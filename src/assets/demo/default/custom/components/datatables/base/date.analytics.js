//== Class definition


var DatatableDataLocalAnalytics = function () {
	var daterangepickerAnalyticsInit = function () {
		if ($('#m_analytics_daterangepicker').length == 0) {
			return;
		}
	
		var picker = $('#m_analytics_daterangepicker');
		var start = moment().subtract(29, 'days');
		var end = moment();
	
		function cb(start, end, label) {
			var title = '';
			var range = '';
	
			if ((end - start) < 100) {
				title = 'Today:';
				range = start.format('MMM D');
			} else if (label == 'Yesterday' || label == 'Today') {
				title = label + ':';
				range = start.format('MMM D');
			} else {
				range = start.format('MMM D') + ' - ' + end.format('MMM D YYYY');
			}
	
			window.localStorage.setItem('analyticsStartDate', start);
			window.localStorage.setItem('analyticsEndDate', end);
			picker.find('.m-subheader__daterange-date').html(range);
			picker.find('.m-subheader__daterange-title').html(title);
		}
	
		picker.daterangepicker({
			startDate: start,
			endDate: end,
			opens: 'left',
			autoApply: true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
				'This year': [moment().startOf('year'), moment().endOf('year')]
			}
		}, cb);
		$(".range_inputs").hide();
		cb(start, end, 'Last 30 Days');
	}
	return {
		//== Public functions
		init: function () {
			// init dmeo
			daterangepickerAnalyticsInit();
		}
	};
	}();




jQuery(document).ready(function () {
	DatatableDataLocalAnalytics.init();	
});