import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { ConsumablesService } from '../consumables.service';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./consumables-manage.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class ConsumablesManageComponent implements OnInit, AfterViewInit {
    testList: any[];
    consumableTests: any[];
    sortType: string;
    sort: boolean;
    user: any;
    isAdmin: any;
    userRole: any;
    constructor(private _ngZone: NgZone, private _consumablesService: ConsumablesService, private _script: ScriptLoaderService, private _router: Router) {
        this.testList = [
            { type: "physical", name: "Physical", tests: [{ name: "BLOOD_PRESSURE", type: "BLOOD_PRESSURE" }, { name: "PULSE_OXIMETER", type: "PULSE_OXIMETER" }, { name: "TEMPERATURE", type: "TEMPERATURE" }, { name: "HEIGHT", type: "Height" }, { name: "WEIGHT", type: "Weight" }, { name: "BMI", type: "BMI" }, { name: "ECG", type: "ECG" }, { name: "MID_ARM_CIRCUMFERENCE", type: "MID_ARM_CIRCUMFERENCE" }] },
            { type: "whole_blood_poct", name: "Whole Blood-POCT", tests: [{ name: "GLUCOSE", type: "GLUCOSE" }, { name: "HEMOGLOBIN", type: "HEMOGLOBIN" }, { name: "CHOLESTEROL", type: "CHOLESTEROL" }, { name: "URIC_ACID", type: "URIC_ACID" }] },
            { type: "whole_blood", name: "Whole Blood", tests: [{ name: "BLOOD_GROUPING", type: "BLOOD_GROUPING" }] },
            { type: "whole_blood_rdt", name: "Whole Blood-RDT", tests: [{ name: "MALARIA", type: "Malaria" }, { name: "DENGUE", type: "Dengue" }, { name: "TYPHOID", type: "Typhoid" }, { name: "HEP_B", type: "HEP_B" }, { name: "HEP_C", type: "HEP_C" }, { name: "HIV", type: "HIV" }, { name: "CHIKUNGUNYA", type: "Chikungunya" }, { name: "SYPHILIS", type: "Syphilis" }, { name: "TROPONIN_1", type: "TROPONIN_1" }] },
            { type: "urine_poct", name: "Urine-POCT", tests: [{ name: "PREGNANCY", type: "Pregnancy" }] },
            { type: "urine_rdt", name: "Urine-RDT", tests: [{ name: "URINE_SUGAR", type: "URINE_SUGAR" }, { name: "URINE_PROTEIN", type: "URINE_PROTEIN" }, { name: "LEUKOCYTES", type: "Leukocytes" }, { name: "UROBILINOGEN", type: "Urobilinogen" }, { name: "NITRITE", type: "Nitrite" }, { name: "PH", type: "pH" }, { name: "KETONE", type: "Ketone" }, { name: "BLOOD", type: "Blood" }, { name: "BILIRUBIN", type: "BILIRUBIN" }, { name: "SPECIFIC_GRAVITY", type: "SPECIFIC_GRAVITY" }] }
        ];
        this.consumableTests = [];
        this.testList.forEach(type => {
            type.tests.forEach(test => {
                test.consumables = [];
                test.consumables.push({ id: 'asdd', name: "Disposable Syringes", unit: "ml", manufacturer: "Online Surgicals", count: '100' });
                this.consumableTests.push(test);
            });
        });
        // console.log(this.consumableTests);
    }
    ngOnInit() {
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
        window.my.namespace.consumableUpdatePublicFunc = this.consumableUpdatePublicFunc.bind(this);

        this.user = JSON.parse(window.localStorage.getItem('userInfo'))
        if (this.user) {
            this.roleChecking();
        }
    }
    ngOnDestroy() {
        window.my.namespace.publicFunc = null;
    }
    ngAfterViewInit() {
        this.list();
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/forms/validation/form-controls.js');
    }

    roleChecking() {
        var isadmin = true;
        var isfacilityadmin = false;
        this.user.userRoles.forEach(role => {
            if (role === "OPERATOR" || role === "DOCTOR" || role === "TECHNICIAN")
                isadmin = false;
            if (role === "TECHNICIAN")
                isfacilityadmin = true;
            if (role === "HC_OPERATIONS" || role === "HC_MANAGER")
                this.userRole = "HC_OPS";
            if (role === "HC_ADMIN") {
                this.userRole = "HC_ADMIN";
            }
            if (role === "OPERATOR")
                this.userRole = "OPERATOR";
            if (role === "DOCTOR")
                this.userRole = "DOCTOR";
            if (role === "TECHNICIAN")
                this.userRole = "TECHNICIAN";
        });
        if (!isadmin) {
            this.isAdmin = "Facility";
        }
        else {
            this.isAdmin = "Admin";
        }
    }

    consumableUpdatePublicFunc(id) {
        this._ngZone.run(() => this.goToConsumableEdit(id));
    }

    goToConsumableEdit(id) {
        this._router.navigate(['/consumables/update'], { queryParams: { consumableId: id } });
    }

    list(): void {
        localStorage.setItem('consumableTestsTable', JSON.stringify(this.consumableTests));
        // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
        //     'assets/demo/default/custom/components/datatables/base/data-local.consumablesTest.js');
    }

}