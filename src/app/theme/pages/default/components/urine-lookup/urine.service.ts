import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { environment } from "../../../../../../environments/environment";
import { Helpers } from "../../../../../helpers";
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";





@Injectable()
export class UrineService {

    private headers = new Headers({
        "Content-Type": "application/json",
        Authorization:
            "Bearer " + JSON.parse(localStorage.getItem("currentUser")).token
    });

    private serviceUrl = environment.BaseURL + environment.FileSrvURL;
    private serviceUrlJson = environment.BaseURL + "ezdx-file-srv/api/v1/file";

    constructor(private http: Http, private _script: ScriptLoaderService) {
    }

    getDefaultLookup(): Promise<any> {
        var defaultUrl =
            this.serviceUrl +
            "/search?searchCriteria=FILE_TYPE&fileType=DEFAULT_URINE_LOOKUP";
        return this.http
            .get(defaultUrl, { headers: this.headers })
            .toPromise()
            .then(res => {
                return res.json();
            })
            .catch(this.handleError.bind(this));
    }

    getUrineLookupById(ULookupId): Promise<any> {
        var getByIdUrl = this.serviceUrl + "/" + ULookupId;
        // console.log("url: ", getByIdUrl);
        return this.http
            .get(getByIdUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                // console.log("Res: ", response);
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    downloadFile(id): Promise<any> {
        var downloadUrl = this.serviceUrlJson + "/urinelookup/json/" + id;
        return this.http
            .get(downloadUrl, { headers: this.headers })
            .toPromise()
            .then(res => {
                return res.json();
            })
            .catch(this.handleError.bind(this));
    }


    createRdtJson(rdtLookups): Promise<any> {
        //Helpers.setLoading(true);
        var postUrl = this.serviceUrl + "/urinelookup/" + rdtLookups.fileName;
        return this.http
            .post(postUrl, rdtLookups, { headers: this.headers })
            .toPromise()
            .then(respose => {
                return respose;
            })
            .catch(this.handleError.bind(this));
    }

    updateUrineJson(urineLookup, id): Promise<any> {

        var postUrl = this.serviceUrl + "/urinelookup/" + id;
        return this.http
            .put(postUrl, urineLookup, { headers: this.headers })
            .toPromise()
            .then(respose => {
                return respose;
            })
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load(
                "body",
                "assets/demo/default/custom/components/utils/redirect-cidaas-logout.js"
            );
        }
        return Promise.reject(error.message || error);
    }
}