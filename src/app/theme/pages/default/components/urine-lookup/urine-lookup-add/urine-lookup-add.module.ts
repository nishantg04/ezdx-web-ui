import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { OrderModule } from 'ngx-order-pipe';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DataTableModule, SharedModule, PaginatorModule, DialogModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { UrineLookupAddComponent } from './urine-lookup-add.component';
import { UrineService } from '../urine.service';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": UrineLookupAddComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule, DataTableModule, SharedModule, PaginatorModule, DialogModule,
        TranslateModule.forChild({ isolate: false }),
    ], exports: [
        RouterModule
    ], declarations: [
        UrineLookupAddComponent,
    ],
    providers: [
        FacilitiesService, UrineService, TranslateService
    ]

})

export class UrineLookupAddModule { }


