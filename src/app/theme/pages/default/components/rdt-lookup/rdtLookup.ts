export class RdtLookup {
    assignmentStatus?: string;
    centerId?: string[];
    checksum?: string;
    deleted?: boolean;
    deviceId?: string[];
    facilityId?: string[];
    fileType?: string;
    id?: string;
    path?: string;
    updateTime?: number;
    uploadTime?: number;
    userId?: string;
}