//== Class definition

var DatatableDataLocalDemo = function () {
    //== Private functions

    // demo initializer
    var demo = function () {

        var dataJSONArray = JSON.parse(localStorage.getItem('facilitypatientTable'));

        var datatable = $('#patient_data').mDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: dataJSONArray,
                pageSize: 10,
                saveState: {
                    cookie: false,
                    webstorage: false
                }, // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
            },

            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 450, // datatable's body's fixed height
                footer: false // display/hide footer
            },

            // column sorting(refer to Kendo UI)
            sortable: true,

            // column based filtering(refer to Kendo UI)
            filterable: false,

            pagination: true,

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition

            columns: [{
                field: "",
                title: "Sr no",
                width: 100,
                sortable: false,
                template: function (row) {
                    return '\
						<span>\
                            ' + (((row.getDatatable().getCurrentPage() - 1) * 10) + (row.getIndex() + 1)) + '\
                        </span>\
					';
                }
            }, {
                field: "id",
                title: "Patient ID",
                width: 350,
                sortable: false,
                template: function (row) {
                    return '\
						<span>\
                            ' + (row.id) + '\
                        </span>\
					';
                }
            }, {
                field: "gender",
                title: "Gender",
                width: 250,
                template: function (row) {
                    return '\
						<span>\
                            ' + row.gender + '\
                        </span>\
					';
                }
            }, {
                field: "age",
                title: "Age",
                width: 150,
                template: function (row) {
                    return '\
						<span>\
                            ' + row.age + '\
                        </span>\
					';
                }
            }, {
                field: "userEmail",
                title: "Operator",
                template: function (row) {
                    return '\
						<span>\
                            ' + row.userEmail + '\
                        </span>\
					';
                }
            }, {
                field: "createTimeDate",
                title: "Reg. Date",
                template: function (row) {
                    var partsOfStr = row.createTime.split('(');
                    partsOfStr[1] = partsOfStr[1].slice(0, -1);
                    return '\
						<div>\
                            <i class="fa fa-calendar"></i>  ' + partsOfStr[0] + '\
                        </div>\
						<div>\
							<i class="fa fa-clock-o"></i> ' + partsOfStr[1] + '\
                        </div>\
					';
                }
            }]
        });

        var query = datatable.getDataSourceQuery();

        $('#patient_search').on('keyup', function (e) {
            datatable.search($(this).val().toLowerCase());
        }).val(query.generalSearch);
        datatable.sort('endTimeDate');
        setTimeout(function () {
            datatable.sort('endTimeDate');
        }, 500);

        // $('#m_form_status').on('change', function () {
        // 	datatable.search($(this).val(), 'Status');
        // }).val(typeof query.Status !== 'undefined' ? query.Status : '');

        // $('#m_form_type').on('change', function () {
        // 	datatable.search($(this).val(), 'Type');
        // }).val(typeof query.Type !== 'undefined' ? query.Type : '');

        // $('#m_form_status, #m_form_type').selectpicker();

    };

    return {
        //== Public functions
        init: function () {
            // init dmeo
            demo();
        }
    };
}();

jQuery(document).ready(function () {
    DatatableDataLocalDemo.init();
});