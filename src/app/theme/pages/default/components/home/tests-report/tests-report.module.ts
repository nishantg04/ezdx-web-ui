import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TestsReportComponent } from './tests-report.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { UsersService } from '../../users/users.service';
import { PatientsService } from '../../patients/patients.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DevicesService } from '../../devices/devices.service';
import { VisitsService } from '../../visits/visits.service';
import { TestsReportService } from './tests-report.service'
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { DropdownModule } from 'primeng/primeng';
import { AmchartModule } from '../../../../../amchartcomponent/amchart.module';
import { FacDropdownModule } from './../../../../../fac-dropdown/fac-dropdown.module';
import { ConfigService } from '../../skinconfig/tests.service';


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": TestsReportComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, FacDropdownModule,
        TranslateModule.forChild({ isolate: false }), DropdownModule, AmchartModule
    ], exports: [
        RouterModule
    ], declarations: [
        TestsReportComponent
    ],
    providers: [
        UsersService,
        PatientsService,
        FacilitiesService,
        DevicesService,
        VisitsService,
        TestsReportService,
        TranslateService,
        ConfigService
    ]

})
export class TestsReportModule {



}