import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { FacilitiesService } from './../../theme/pages/default/components/facilities/facilities.service';
import { Facility } from './../../theme/pages/default/components/facilities/facility';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SimpleChange } from '@angular/core/src/change_detection/change_detection_util';
import { UsersService } from './../../theme/pages/default/components/users/users.service';
import { MasterTestService } from './../../theme/pages/default/components/master-test/master-test.service';
import { User } from './../../theme/pages/default/components/users/user';
import { Test } from './../../theme/pages/default/components/master-test/test';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-fac-dropdown',
    templateUrl: "./fac-dropdown.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class FacDropdownComponent implements OnInit {
    list: any[] = [];
    @Input() id: any;
    @Input() label: any;
    @Input() placeH: any;
    @Input() value: Facility;
    @Input() valueUser: any;
    @Input() valueTest: any;
    @Input() reset: any;
    @Input() noLabel: any;
    @Input() FacilityId: any;
    @Input() FacilityName : any;
    @Input() active: boolean = false;
    @Input() noAll: any;
    @Input() hideCus: boolean = false;
    @Input() typeOfSearch: any;
    @Input() selectedFacilityId: any;
    @Input() selectedCenterId: any;
    @Input() clearText: boolean = false;
    @Output() selected: EventEmitter<any> = new EventEmitter<any>();
    @Output() selectedTest: EventEmitter<any> = new EventEmitter<any>();
    @Output() selectedUser: EventEmitter<any> = new EventEmitter<any>();
    text: string;
    test: string;
    user: string;
    searchUSerString: string
    facilities: Facility[] = [];
    allFacility = new Facility();
    testList: Test[] = [];
    alltestList: Test[] = [];
    userList: User[] = [];
    allTest: boolean = false;
    allUser: boolean = false;
    facilityId: any;
    centerId: any;
    selectedFacility : any ={};

    constructor(private facilitiesService: FacilitiesService, private _usersService: UsersService, private _router: Router,
        private _activeroute: ActivatedRoute, private testService: MasterTestService, private translate : TranslateService) { }

    ngOnInit() {
        this.checkUrl();
    }
    ngOnChanges(changes: SimpleChange) {
        if (this.FacilityId != "")
            this.getFaciltyDetail(this.FacilityId);
        if (this.clearText == true)
            this.user = '';
        if(this.FacilityName !=''){
            this.text = this.FacilityName;
        }
    }

    checkUrl() {
        if (this._activeroute.snapshot.queryParams['facilityId'] && this._activeroute.snapshot.queryParams['facilityId'] != 'ALL') {
            this.facilityId = this._activeroute.snapshot.queryParams['facilityId'];
            if (this.facilityId != 'ALL') {
                this.getFaciltyDetail(this.facilityId);
            }
        } else {
            if (!this.noLabel && this.noAll != 'hideAll') {
                this.allFacility.name = "ALL";
                this.allFacility.id = "ALL";
                this.facilities.push(this.allFacility)
                this.text = this.facilities[0].name;
            }
        }
    }

    ngOnDestroy() {

    }


    search(event) {
        if(event.query == "")
           this.text = ''
        this.getFacility(event.query)
    }

    getFaciltyDetail(id) {
        if (id != undefined && id) {
            this.facilitiesService.getFacilityById(id)
                .then((data) => {
                    this.text = data.name;
                    this.value = data;
                    //  this.selected.emit(this.value)
                    this.facilities.push(data);
                })
        }
    }

    getFacility(value) {
        console.log(value)
        this.facilitiesService.searchFacilities('NAME', value, 0, 0, 'name', 'asc')
            .then(response => {
                if (response !== null) {
                    if (this.hideCus == false)
                        this.facilities = response.organizations;
                    else
                        this.specificCustomer(response.organizations)
                    if (!this.noLabel && this.noAll != 'hideAll' && value == '') {
                        let allFacility = new Facility();
                        allFacility.name = "ALL";
                        allFacility.id = "ALL";
                        this.facilities.splice(0, 0, allFacility);
                    }
                }
                else {
                    this.facilities = [];
                }
            })
    }

    searchUser(event) {
        this.searchUSerString = event.query;
        if (this.searchUSerString == '') {
            console.log('if')
            this.user = ''
            if (this.selectedFacilityId == 'ALL' && this.selectedCenterId == undefined) {
                console.log('1st cond')
                this.userAllList(0, 0, 'ALL', 0, 1, 0)
            }
            if (this.selectedFacilityId == 'ALL' && this.selectedCenterId == 'ALL') {
                this.userAllList(0, 0, 'ALL', 0, 1, 0)
           }
            if (this.selectedFacilityId != 'ALL' && this.selectedCenterId == 'ALL') {
                console.log('2nd cond')
                this.userAllList(0, 0, 'FACILITY', this.selectedFacilityId, 1, 0)
            }
            if (this.selectedFacilityId && this.selectedCenterId != 'ALL' && this.selectedCenterId != undefined) {
                console.log('3rd cond')
                this.userAllList(0, 0, 'CENTER', this.selectedCenterId, 1, 0)
            }

        } else {
            console.log('else')
            console.log(this.selectedFacilityId)
            console.log(this.selectedCenterId)
            if (this.selectedFacilityId == 'ALL' && this.selectedCenterId == undefined) {
                this.filterUser(0, 0, 'NAME', 'asc', this.searchUSerString, 1, 0)
            }
            if (this.selectedFacilityId == 'ALL' && this.selectedCenterId == 'ALL') {
                 this.filterUser(0, 0, 'NAME', 'asc', this.searchUSerString, 1, 0)
            }
            if (this.selectedFacilityId != 'ALL' && this.selectedCenterId == 'ALL') {
                this.searchFacilityCenterUser(0, 0, 'NAME', this.selectedFacilityId, 'asc', this.searchUSerString, 1, 0, 'FACILITY')
            }
            if (this.selectedFacilityId && this.selectedCenterId != 'ALL' && this.selectedCenterId != undefined) {
                this.searchFacilityCenterUser(0, 0, 'NAME', this.selectedCenterId, 'asc', this.searchUSerString, 1, 0, 'CENTER')
            }
        }
    }

    searchTest(event) {
        this.searchUSerString = event.query;
        if (this.searchUSerString == '') {           
            this.test = ''
            this.getAllTest();
        } else {
            this.searchByTestName(this.searchUSerString)
        }
    }

    searchByTestName(testName) {
        this.testService.testNameSearching(testName).then((res) => {
            this.alltestList = res;          
            this.specificTest(this.alltestList)
        })
    }

    getAllTest() {
        this.testService.getAllTests().then((res) => {
            this.alltestList = res as Test[];
            this.specificTest(this.alltestList)
        })
    }

    specificTest(alltestList) {
        this.testList = [];
        alltestList.forEach(test => {
            if (test.active && test.testName!= "SYMPTOM_BASED_TEST") {
                this.testList.push(test)
            }
        })
    }


    userAllList(fromDate, toDate, searchcriteria, id, pageno, size): void {
        console.log('userlist')
        this._usersService.getUsersFiltered(fromDate, toDate, searchcriteria, id, pageno, size, '', 'asc')
            .then(users => {
                this.userList = users.users;
            })
    }

    filterUser(from, to, searchCriteria, sortby, searchstring, page, size) {
        console.log('filter user')
        this._usersService.getUsersFiltered(from, to, searchCriteria, '', page, size, searchstring, sortby)
            .then(
            response => {
                this.userList = response.users;
            })
    }

    searchFacilityCenterUser(from, to, searchCriteria, id, sortby, searchstring, page, size, searchby) {
        if (searchby == 'FACILITY') {
            this._usersService.getFaciltyUserSearch(from, to, searchCriteria, id, page, size, searchstring, sortby, searchby)
                .then(response => {
                    this.userList = response.users;
                    console.log(this.userList)
                });

        }
        if (searchby == 'CENTER') {
            this._usersService.getCenterUserSearch(from, to, searchCriteria, id, page, size, searchstring, sortby, searchby)
                .then(response => {
                    this.userList = response.users;
                })
        }
    }
    onSelected(e) {   
        this.value = e;
        this.text = this.value.name;
        this.selected.emit(this.value);       
    }

    onselectedUser(e) {
        this.valueUser = e;        
        this.selectedUser.emit(this.valueUser)
        this.user = this.valueUser.displayName;
    }

    onselectedTest(e) {
        this.valueTest = e;
        this.selectedTest.emit(this.valueTest);
        this.test = this.valueTest.testName;
       // this.test = this.humanize(this.test);
        this.translate.stream(this.test).subscribe((val) => {                             
            this.test = val;
          
       })    
       this.translate.onLangChange.subscribe((event) => {         
          this.test =  event.translations[this.valueTest.testName];              
                      
       }); 
    }

    specificCustomer(customer) {
        this.facilities = [];
        customer.forEach(element => {
            if (element.organizationCode != "HCB" && element.organizationCode != "IAP")
                this.facilities.push(element)
        });
    }

    humanize(str) {
        var frags = str.split('_');
        for (var i = 0; i < frags.length; i++) {
            frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
        }
        return frags.join(' ');
    }




}
