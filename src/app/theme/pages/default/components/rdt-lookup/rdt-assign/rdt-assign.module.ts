import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { OrderModule } from 'ngx-order-pipe';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DataTableModule, SharedModule, PaginatorModule, DialogModule, DropdownModule, MultiSelectModule, CheckboxModule } from 'primeng/primeng';
import { RdtService } from '../rdt.service';
import { RdtAssignComponent } from './rdt-assign.component';
import { DeviceService } from '../device.service';
import { DevicesService } from '../../devices/devices.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { UploadFileService } from '../../upload/upload-file.service';
import { FacDropdownModule } from './../../../../../fac-dropdown/fac-dropdown.module'

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": RdtAssignComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, TranslateModule.forChild({ isolate: false }), FacDropdownModule,
        MultiSelectModule, DropdownModule, OrderModule, DataTableModule, SharedModule, PaginatorModule, DialogModule, CheckboxModule

    ], exports: [
        RouterModule
    ], declarations: [
        RdtAssignComponent
    ],
    providers: [
        FacilitiesService,
        RdtService,
        DeviceService, DevicesService,
        TranslateService,
        UploadFileService
    ]

})
export class RdtAssignModule { }