import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AmchartComponent } from './amchartcomponent.component'
import { AmChartsService } from 'amcharts3-angular2';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule
    ], exports: [
        AmchartComponent
    ], declarations: [
        AmchartComponent
    ],
    providers: [AmChartsService,TranslateService]

})

export class AmchartModule { }
