import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { AnalyticsService } from '../analytics.service';
import { ScriptLoaderService } from './../../../../../../_services/script-loader.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./avgconsumablebasedtest.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class AvgconsumablebasedtestComponent implements OnInit {

    user: any;
    from: string;
    userType : any;
    constructor(private _router: Router, private analytics: AnalyticsService, private _script: ScriptLoaderService, private _activeroute: ActivatedRoute) { 
        this.user=JSON.parse(localStorage.getItem('userInfo'));
        if (window.localStorage.getItem('userType') == 'Facility')
        this.userType = 'Facility';
        else if (window.localStorage.getItem('userType') == 'Center')
            this.userType = 'Center';
        else if (window.localStorage.getItem('userType') == 'User')
            this.userType = 'User';
        else
            this.userType = 'Admin';
    }
    public avgdiagnostic: any = [];
    public avgdiagnosticresp: any = {};
    public avgdiagnosticCount: number = 0;
    public visiblePagination: boolean = false;
    public pageNo: number = 1;
    public limit: number = 10;
    public ranges: any = [];
    public row: number = 10;
    public warning: boolean = false;
    public data: any = [];
    public selectedoption: string = "Consumable based test";
    public selectedItem: string;
    public selectedcenter: string;

    ngOnInit() {
        this._activeroute.queryParams.subscribe(param => {
            this.selectedItem = param.selectedItem;
            this.selectedcenter = param.selectedcenter;
            this.from=param.from;
        });
        this.limit = 10;
        Helpers.setLoading(true);
        this.funcChecking()
        this.ranges = [
            { label: "10", value: 10 },
            { label: "25", value: 25 },
            { label: "50", value: 50 },
            { label: "100", value: 100 },
        ];
    }

    funcChecking(){
        if(this.userType == 'Facility'){
            if (this.from=='user' && this.user.userRoles.includes('TECHNICIAN')) {
                this.getUserWiseavgdiagnosticAndConsumablebasedtesFacility('facility',this.user.facilityId,this.pageNo, this.limit)
            }
            else if(this.from!='user' && this.user.userRoles.includes('TECHNICIAN')){
                this.getavgdiagnosticAndConsumablebasedtestFacility(this.user.facilityId,this.pageNo, this.limit)
            }
        }
        else if(this.userType == 'Center'){
            if (this.from=='user' && this.user.userRoles.includes('CENTER_ADMIN')) {
                this.getUserWiseavgdiagnosticAndConsumablebasedtesFacility('center',this.user.centerId,this.pageNo, this.limit)
            }
            else if(this.from!='user' && this.user.userRoles.includes('CENTER_ADMIN')){
                this.getavgdiagnosticAndConsumablebasedtestFacility(this.user.centerId,this.pageNo, this.limit)
            }
        }
        else{           
            this.getUserWiseavgdiagnosticAndConsumablebasedtestStats(this.pageNo, this.limit)
            this.getavgdiagnosticAndConsumablebasedtest(this.pageNo, this.limit)            
        }
    }

   
    goBack(){
        if(this.from && this.from == 'user'){
            this._router.navigate(['/analytics/usercharts'])
        }else{
         this._router.navigate(['/analytics/charts']) 
        }
     }

    selectedoptionchanged(option) {
        if (option == "Total Diagnostics" && this.from!='user') {
            this._router.navigate(['analytics/avgdiagnostics'], { queryParams: { selectedItem: this.selectedItem, selectedcenter: this.selectedcenter } });
        }else if(option == "Total Diagnostics" && this.from=='user'){
            this._router.navigate(['analytics/avgdiagnostics'], { queryParams: { selectedItem: this.selectedItem, selectedcenter: this.selectedcenter, from: this.from } });
        }
    }

    //get userwise log list
    public getUserWiseavgdiagnosticAndConsumablebasedtestStats(page, limit): void {
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getanalyticsUserSearchPaginate(page, limit).then((res) => {
            Helpers.setLoading(false);
            if (res != undefined) {
                this.visiblePagination = true;
                this.avgdiagnosticresp = res;
                this.avgdiagnostic = this.avgdiagnosticresp.results;
                this.avgdiagnosticCount = this.avgdiagnosticresp.totalHits;
                this.avgdiagnostic.sort((a, b) => { return b.totalRegistrations - a.totalRegistrations })

            } else {
                this.avgdiagnostic = [];
                this.avgdiagnosticCount = 0;
            }
        })
    }

    //get userwise log list with filter 
    public getUserWiseavgdiagnosticAndConsumablebasedtesFacility(type,facility, page, limit): void {
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getanalyticsUserSearchPaginateFacility(type,facility,page, limit).then((res) => {
            Helpers.setLoading(false);
            // console.log(res)
            if (res != undefined) {
                this.visiblePagination = true;
                this.avgdiagnosticresp = res;
                this.avgdiagnostic = this.avgdiagnosticresp.results;
                this.avgdiagnosticCount = this.avgdiagnosticresp.totalHits;
                this.avgdiagnostic.sort((a, b) => { return b.totalRegistrations - a.totalRegistrations })

            } else {
                this.avgdiagnostic = [];
                this.avgdiagnosticCount = 0;
            }
        })
    }

    public getavgdiagnosticAndConsumablebasedtestFacility(faility,page, limit): void {
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getavgdiagnosticAndConsumablebasedtestFacility(faility,page, limit).then((res) => {
            Helpers.setLoading(false);
            // console.log(res)
            if (res != undefined) {
                this.visiblePagination = true;
                this.avgdiagnosticresp = res;
                this.avgdiagnostic = this.avgdiagnosticresp.results;
                this.avgdiagnosticCount = this.avgdiagnosticresp.totalHits;
                this.avgdiagnostic.sort((a, b) => { return b.totalRegistrations - a.totalRegistrations })

            } else {
                this.avgdiagnostic = [];
                this.avgdiagnosticCount = 0;
            }
        })
    }

    

   

    //get diagnostics and consumable based test per registration
    public getavgdiagnosticAndConsumablebasedtest(page, limit): void {
        this.data = [];
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getavgdiagnosticAndConsumablebasedtest(page, limit).then((res) => {
            Helpers.setLoading(false);
            //  console.log(res)
            if (res != undefined) {
                this.visiblePagination = true;
                this.avgdiagnosticresp = res;
                this.avgdiagnostic = this.avgdiagnosticresp.results;
                this.avgdiagnosticCount = this.avgdiagnosticresp.totalHits;
                this.avgdiagnostic.sort((a, b) => { return b.totalRegistrations - a.totalRegistrations })

            } else {
                this.avgdiagnostic = [];
                this.avgdiagnosticCount = 0;
            }
        })
    }








    //when pagination number changed
    paginate(event) {
        this.pageNo = event.page + 1;
        this.limit = parseInt(event.rows);
        this.funcChecking()
    }



    //change items per page
    select(e) {
        this.visiblePagination = false;
        this.limit = e.value;
        this.pageNo = 1;
        this.row = e.value;
        this.funcChecking()
    }




}
