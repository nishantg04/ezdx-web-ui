import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { UsersService } from '../../users/users.service';
import { PatientsService } from '../../patients/patients.service';
import { UserChild } from '../../users/userChild';
import { Facility } from '../../facilities/facility';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DevicesService } from '../../devices/devices.service';
import { VisitsService } from '../../visits/visits.service';
import { Center } from '../../facilities/center';
import { Visit } from '../../visits/visit';
import { Test } from '../../visits/test';
import { TestsReportService } from './tests-report.service';
import { TranslateService } from '@ngx-translate/core';
import * as _ from "lodash";
import { AmChartsService } from "amcharts3-angular2";
import { setTimeout } from 'timers';
import { Skin } from '../../skinconfig/skin';
import { ConfigService } from '../../skinconfig/tests.service';
import { validateConfig } from '@angular/router/src/config';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./tests-report.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class TestsReportComponent implements OnInit, AfterViewInit {

    templow: any = 95;
    temphigh: any = 99.5;
    hemoglobinhigh: any = 18;
    hemoglobinlow: any = 11;
    cholestrollow: any = 0;
    cholestrolhigh: any = 200;
    diastoliclow: any = 60;
    diastolichigh: any = 89;
    systoliclow: any = 90;
    systolichigh: any = 140;
    nonfastingBloodGlucoseDiabetic: any = 200;
    nonfastingBloodGlucoseNormal: any = 139;
    nonfastingBloodGlucosePreDiabetic: any = 89;
    fastingBloodGlucoseDiabetic: any = 126;
    fastingBloodGlucoseNormal: any = 110;
    fastingBloodGlucosePreDiabetic: any = 70;
    config: Skin;
    userType: string;
    userInfo: any;
    //facilities: { label: string, value: Facility }[] = [];
    facilities: Facility[]
    selectedFacility: Facility;
    selectedCenter: Center;
    // centers: { label: String, value: Center }[] = [];
    centers: Center[]
    years: any[];
    selectedYear: any;
    types: any;
    selectedType: any;
    testsForSelectedType: any;
    reports: any[];
    testsForFilteredType: any;
    displayType: any;
    showPiechart: boolean = false;
    msg: any;
    errorPiechart: boolean = false;
    typeLabels: any[] = [];
    private chart: any;
    //array
    BLOOD_PRESSURE_array: any[] = [];
    BLOOD_GLUCOSE_NON_FASTING_array: any[] = [];
    BLOOD_GLUCOSE_FASTING_array: any[] = [];
    CHOLESTEROL_array: any[] = [];
    HEMOGLOBIN_array: any[] = [];
    HEP_B_array: any[] = [];
    MALARIA_array: any[] = [];
    TEMPERATURE_array: any[] = [];
    TYPHOID_array: any[] = [];
    bloodGluUnit : any = 'mg/dL';
    bloodPreUnit : any= 'mmHg';
    cholUnit : any = 'mg/dL';
    hemoUnit : any = 'g/dL';
    tempUnit : any = 'F';
    something: boolean = true;


    constructor(private _configService: ConfigService, private _ngZone: NgZone, private _script: ScriptLoaderService, private _usersService: UsersService, private _patientsService: PatientsService,
        private _router: Router, private _facilitiesService: FacilitiesService, private _devicesService: DevicesService, private _visitsService: VisitsService,
        private _testsReportService: TestsReportService, private translate: TranslateService, private AmCharts: AmChartsService) {
        // $(".amcharts-chart-div a").hide();
        this._router.routeReuseStrategy.shouldReuseRoute = function() {
            return false;
        }
        this.config = new Skin();
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        if (window.localStorage.getItem('userType') == 'Facility')
            this.userType = 'Facility';
        else if (window.localStorage.getItem('userType') == 'Center')
            this.userType = 'Center';
        else if (window.localStorage.getItem('userType') == 'User')
            this.userType = 'User';
        else
            this.userType = 'Admin';
        this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
        this.types = [
            { label: 'ALL', value: 'ALL' },
            { label: 'BLOOD_PRESSURE', value: 'BLOOD_PRESSURE' },
            { label: 'BLOOD_GLUCOSE_NON_FASTING', value: 'BLOOD_GLUCOSE_NON_FASTING' },
            { label: 'BLOOD_GLUCOSE_FASTING', value: 'BLOOD_GLUCOSE_FASTING' },
            { label: 'CHOLESTEROL', value: 'CHOLESTEROL' },
            { label: 'HEAMOGLOBIN', value: 'HEAMOGLOBIN' },
            { label: 'HEP_B', value: 'HEP_B' },
            { label: 'MALARIA', value: 'MALARIA' },
            { label: 'TEMPERATURE', value: 'TEMPERATURE' },
            { label: 'TYPHOID', value: 'TYPHOID' },
            // { label: 'Pulse Oximeter', value: 'PULSE_OXIMETER' }, 
            //{ label: 'Uric Acid', value: 'URIC_ACID' },                    
        ];
        this.displayType = [
            { id: 'temperatureReport', value: 'TEMPERATURE' },
            { id: 'malariaReport', value: 'MALARIA' },
            { id: 'cholesterolReport', value: 'CHOLESTEROL' },
            { id: 'hepbReport', value: 'HEP_B' },
            { id: 'typhoidReport', value: 'TYPHOID' },
            { id: 'hemoglobinReport', value: 'HEAMOGLOBIN' },
            { id: 'bloodPressureReport', value: 'BLOOD_PRESSURE' },
            { id: 'bloodGlucoseFastReport', value: 'BLOOD_GLUCOSE_FASTING' },
            { id: 'bloodGlucoseNonFastReport', value: 'BLOOD_GLUCOSE_NON_FASTING' }
        ]
    }
    ngOnInit() {
        this.getFacilities();
        this.getFullReport();
        let tempArray: any = [];
        this.types.forEach((type) => {
            tempArray.push(type.label);
        });
        this.translate.stream(tempArray).subscribe((val) => {
            //this.typeLabels = [...val];
            this.typeLabels = _.map(val, this.objectToArray);
            // console.log("values: ", val, " labels: ", this.typeLabels);
        })
    }

    rangeCalclulation(facilityId): any {
        let criteria = 'FACILITY_ID&facilityId=' + facilityId;
      //  this.something = true;
        this._configService.getconfigfileForUser(criteria).then(res => {
            this.config = res.configs[0];
            console.log(this.config)
            if (this.config != null || this.config != undefined) {
                this.config.testRanges.map((range) => {
                    if (range.testName == 'BLOOD_GLUCOSE') {
                        this.fastingBloodGlucosePreDiabetic = range.ranges.fasting[0];
                        this.fastingBloodGlucoseNormal = range.ranges.fasting[1];
                        this.fastingBloodGlucoseDiabetic = range.ranges.fasting[2];

                        this.nonfastingBloodGlucosePreDiabetic = range.ranges.post_prandial[0];
                        this.nonfastingBloodGlucoseNormal = range.ranges.post_prandial[1];
                        this.nonfastingBloodGlucoseDiabetic = range.ranges.post_prandial[2];

                    }
                    if (range.testName == 'BLOOD_PRESSURE') {
                        this.systolichigh = range.ranges.systolic[1];
                        this.systoliclow = range.ranges.systolic[0];

                        this.diastolichigh = range.ranges.diastolic[1];
                        this.diastoliclow = range.ranges.diastolic[0];
                    }
                    if (range.testName == 'CHOLESTEROL') {
                        this.cholestrolhigh = range.ranges.result[1];
                        this.cholestrollow = range.ranges.result[0];
                    }
                    if (range.testName == "HEMOGLOBIN") {
                        this.hemoglobinlow = range.ranges.men[0];
                        this.hemoglobinhigh = range.ranges.men[1];
                    }
                    if (range.testName == "TEMPERATURE") {
                        this.temphigh = range.ranges.result[0];
                        this.templow = range.ranges.result[1];
                    }


                })
                this.config.testUnitMapping.map((unit)=>{
                    if (unit.testName == 'BLOOD_GLUCOSE') {
                         this.bloodGluUnit = unit.testUnit;
                    }
                    if (unit.testName == 'BLOOD_PRESSURE') {
                       this.bloodPreUnit = unit.testUnit;
                    }
                    if (unit.testName == 'CHOLESTEROL') {
                       this.cholUnit = unit.testUnit;
                    }
                    if (unit.testName == "HEMOGLOBIN") {
                        this.hemoUnit = unit.testUnit;
                    }
                    if (unit.testName == "TEMPERATURE") {
                       this.tempUnit = unit.testUnit;
                    }
                })
            }
        });


    }

    defaultCal(){
        this._configService.getdefaultconfig()
        .then(res => {
            this.config=res; 
          //  this.something = true;
            if (this.config != null || this.config != undefined) {
                this.config.testRanges.map((range) => {
                    if (range.testName == 'BLOOD_GLUCOSE') {
                        this.fastingBloodGlucosePreDiabetic = range.ranges.fasting[0];
                        this.fastingBloodGlucoseNormal = range.ranges.fasting[1];
                        this.fastingBloodGlucoseDiabetic = range.ranges.fasting[2];

                        this.nonfastingBloodGlucosePreDiabetic = range.ranges.post_prandial[0];
                        this.nonfastingBloodGlucoseNormal = range.ranges.post_prandial[1];
                        this.nonfastingBloodGlucoseDiabetic = range.ranges.post_prandial[2];

                    }
                    if (range.testName == 'BLOOD_PRESSURE') {
                        this.systolichigh = range.ranges.systolic[1];
                        this.systoliclow = range.ranges.systolic[0];

                        this.diastolichigh = range.ranges.diastolic[1];
                        this.diastoliclow = range.ranges.diastolic[0];
                    }
                    if (range.testName == 'CHOLESTEROL') {
                        this.cholestrolhigh = range.ranges.result[1];
                        this.cholestrollow = range.ranges.result[0];
                    }
                    if (range.testName == "HEMOGLOBIN") {
                        this.hemoglobinlow = range.ranges.men[0];
                        this.hemoglobinhigh = range.ranges.men[1];
                    }
                    if (range.testName == "TEMPERATURE") {
                        this.temphigh = range.ranges.result[0];
                        this.templow = range.ranges.result[1];
                    }


                })
                this.config.testUnitMapping.map((unit)=>{
                    if (unit.testName == 'BLOOD_GLUCOSE') {
                         this.bloodGluUnit = unit.testUnit;
                    }
                    if (unit.testName == 'BLOOD_PRESSURE') {
                       this.bloodPreUnit = unit.testUnit;
                    }
                    if (unit.testName == 'CHOLESTEROL') {
                       this.cholUnit = unit.testUnit;
                    }
                    if (unit.testName == "HEMOGLOBIN") {
                        this.hemoUnit = unit.testUnit;
                    }
                    if (unit.testName == "TEMPERATURE") {
                       this.tempUnit = unit.testUnit;
                    }
                })
            }

        })
    }

    objectToArray(value, key) {
        return { "label": value, "value": key };
    }

    getFacilities(): void {
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            } else if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
        if (window.localStorage.getItem('userType') == 'Facility') {
            this.selectedFacility.id = this.userInfo.facilityId;
            this.facilities.forEach(fclt => {
                if (fclt.id == this.userInfo.facilityId) {
                    this.centers = jQuery.extend(true, [], fclt.centers);
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                    this.selectedCenter.id = this.centers[0].id;
                }
            });
        }
    }



    arrayClear() {
        this.BLOOD_GLUCOSE_FASTING_array = [];
        this.BLOOD_GLUCOSE_NON_FASTING_array = [];
        this.HEP_B_array = [];
        this.HEMOGLOBIN_array = [];
        this.MALARIA_array = [];
        this.TEMPERATURE_array = [];
        this.TYPHOID_array = [];
        this.CHOLESTEROL_array = [];
        this.BLOOD_PRESSURE_array = [];
    }

    getFullReport() {
        this.arrayClear();
        Helpers.setLoading(true);
        if (this.userType === 'Admin') {
            this.types.forEach(element => {
                if (element.value !== 'ALL') {
                    this._testsReportService.getTestBasedOnType('ALL', element.value) // to get full reports
                        .then(tests => {
                            this.testsForSelectedType = tests;
                            //  localStorage.setItem(element.value, JSON.stringify(this.testsForSelectedType))
                            this.showPiechart = true;
                            this.getValueForChart(this.testsForSelectedType);
                            // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                            //     'assets/demo/default/custom/components/charts/amcharts/charts.tests.js');
                            //  this.allTest();
                            Helpers.setLoading(false);
                        }, (e: any) => {                            
                            this.errorPiechart = true;
                            this.msg = e.statusText;
                        });
                }
            });
        }
        else if (this.userType === 'Center') {
            this.types.forEach(element => {
                if (element.value !== 'ALL') {
                    var searchBy = 'CENTER';
                    this.selectedFacility.id = this.userInfo.facilityId;
                    this.selectedCenter.id = this.userInfo.centerId;
                    this._testsReportService.getTestBasedOnCenter(searchBy, this.selectedFacility.id, this.selectedCenter.id, element.value) // to get full reports
                        .then(tests => {
                            this.testsForSelectedType = tests;
                            this.showPiechart = true;
                            this.getValueForChart(this.testsForSelectedType);
                            Helpers.setLoading(false);
                        }, (e: any) => {
                            this.errorPiechart = true;
                            this.msg = e.statusText;
                        });
                }
            });
        }
        else if (this.userType === 'User') {
            this.types.forEach(element => {
                if (element.value !== 'ALL') {
                    var searchBy = 'USER';                   
                    this._testsReportService.getTestBasedOnUser(searchBy,this.userInfo.id, element.value) // to get full reports
                        .then(tests => {
                            this.testsForSelectedType = tests;
                            this.showPiechart = true;
                            this.getValueForChart(this.testsForSelectedType);
                            Helpers.setLoading(false);
                        }, (e: any) => {
                            this.errorPiechart = true;
                            this.msg = e.statusText;
                        });
                }
            });
        }
        else {
            var facilityid = this.userInfo.facilityId;
            var searchBy = 'FACILITY';
            this.types.forEach(type => {
                if (type.value !== 'ALL') {
                    this._testsReportService.getTestBasedOnFaclity(searchBy, facilityid, type.value)
                        .then(tests => {
                            this.testsForSelectedType = tests;
                            this.getValueForChart(this.testsForSelectedType);
                            // localStorage.setItem(type.value, JSON.stringify(this.testsForSelectedType))
                            this.showPiechart = true;
                            // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                            //     'assets/demo/default/custom/components/charts/amcharts/charts.tests.js');
                            Helpers.setLoading(false);
                        })
                }
            });
        }
    }

    getValueForChart(test) {
        this.something = true;
        if (test.testName == 'BLOOD_PRESSURE') {
            this.BLOOD_PRESSURE_array.push({ 'title': 'High', 'value': test.high, "color": "#465EEB" },
                { 'title': 'Normal', 'value': test.normal, "color": "#21C4DF" },
                { 'title': 'Low', 'value': test.low, "color": "#F2F3F8" })
            //    this.allTest('pie_chart_blood_pressure', this.BLOOD_PRESSURE_array, 'Blood Pressure')
        }
        if (test.testName == 'HEMOGLOBIN') {
            this.HEMOGLOBIN_array.push({ 'title': 'High', 'value': test.high, "color": "#465EEB" },
                { 'title': 'Normal', 'value': test.normal, "color": "#21C4DF" },
                { 'title': 'Low', 'value': test.low, "color": "#F2F3F8" })
            //    this.allTest('pie_chart_hemoglobin', this.HEMOGLOBIN_array, 'Hemoglobin')
        }
        if (test.testName == 'TEMPERATURE') {
            this.TEMPERATURE_array.push({ 'title': 'High', 'value': test.high, "color": "#465EEB" },
                { 'title': 'Normal', 'value': test.normal, "color": "#21C4DF" },
                { 'title': 'Low', 'value': test.low, "color": "#F2F3F8" })
            //   this.allTest('pie_chart_temperature', this.TEMPERATURE_array, 'Temperature')
        }
        if (test.testName == 'CHOLESTEROL') {
            this.CHOLESTEROL_array.push({ 'title': 'High', 'value': test.high, "color": "#465EEB" },
                { 'title': 'Normal', 'value': test.normal, "color": "#21C4DF" },
                { 'title': 'Low', 'value': test.low, "color": "#F2F3F8" })
            //   this.allTest('pie_chart_cholesterol', this.CHOLESTEROL_array, 'Cholestrol')
        }
        if (test.testName == 'HEP_B') {
            this.HEP_B_array.push({ 'title': 'Positive', 'value': test.positive, "color": "#465EEB" },
                { 'title': 'Negative', 'value': test.negative, "color": "#21C4DF" },
                { 'title': 'Invalid', 'value': test.invalid, "color": "#F2F3F8" })
            //   this.allTest('pie_chart_hepb', this.HEP_B_array, 'Hep-B')
        }
        if (test.testName == 'MALARIA') {
            this.MALARIA_array.push({ 'title': 'Positive', 'value': test.positive, "color": "#465EEB" },
                { 'title': 'Negative', 'value': test.negative, "color": "#21C4DF" },
                { 'title': 'Invalid', 'value': test.invalid, "color": "#F2F3F8" })
            //   this.allTest('pie_chart_malaria', this.MALARIA_array, 'Malaria')
        }
        if (test.testName == 'TYPHOID') {
            this.TYPHOID_array.push({ 'title': 'Positive', 'value': test.positive, "color": "#465EEB" },
                { 'title': 'Negative', 'value': test.negative, "color": "#21C4DF" },
                { 'title': 'Invalid', 'value': test.invalid, "color": "#F2F3F8" })
            //   this.allTest('pie_chart_typoid', this.TYPHOID_array, 'Typhoid')
        }
        if (test.testName == 'BLOOD_GLUCOSE' && test.chartType == 'BLOOD_GLUCOSE_NON_FASTING') {
            this.BLOOD_GLUCOSE_NON_FASTING_array.push({ 'title': 'Diabetic', 'value': test.high, "color": "#465EEB" },
                { 'title': 'Normal', 'value': test.normal, "color": "#21C4DF" },
                { 'title': 'Pre Diabetic', 'value': test.low, "color": "#F2F3F8" });
            //  this.allTest('pie_chart_blood_glucose_non_fast', this.BLOOD_GLUCOSE_NON_FASTING_array, 'Blood Glucose(Non Fasting)')
        }
        if (test.testName == 'BLOOD_GLUCOSE' && test.chartType == 'BLOOD_GLUCOSE_FASTING') {
            this.BLOOD_GLUCOSE_FASTING_array.push({ 'title': 'Diabetic', 'value': test.high, "color": "#465EEB" },
                { 'title': 'Normal', 'value': test.normal, "color": "#21C4DF" },
                { 'title': 'Pre Diabetic', 'value': test.low, "color": "#F2F3F8" })
            //    this.allTest('pie_chart_blood_glucose_fast', this.BLOOD_GLUCOSE_FASTING_array, 'Blood Glucose(Fasting)')
        }



    }

    getAllFaclityReport() {
        var facilityid = this.selectedFacility.id;
        var searchBy = 'FACILITY';
        this.types.forEach(element => {
            if (element.value !== 'ALL') {
                this._testsReportService.getTestBasedOnFaclity(searchBy, facilityid, element.value)
                    .then(tests => {
                        Helpers.setLoading(false);
                        this.testsForSelectedType = tests
                        this.getValueForChart(this.testsForSelectedType)
                        // localStorage.setItem(element.value, JSON.stringify(this.testsForSelectedType))
                        this.showPiechart = true;
                        // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                        //     'assets/demo/default/custom/components/charts/amcharts/charts.tests.js');
                    })
            }
        })
    }

    getAllCenterReport() {
        var centerId = this.selectedCenter.id;
        var searchBy = 'CENTER';
        this.types.forEach(element => {
            if (centerId !== undefined) {
                if (element.value !== 'ALL') {
                    this._testsReportService.getTestBasedOnCenter(searchBy, this.selectedFacility.id, centerId, element.value)
                        .then(tests => {
                            Helpers.setLoading(false);
                            this.testsForSelectedType = tests
                            this.getValueForChart(this.testsForSelectedType)
                            // localStorage.setItem(element.value, JSON.stringify(this.testsForSelectedType))
                            this.showPiechart = true;
                            // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                            //     'assets/demo/default/custom/components/charts/amcharts/charts.tests.js');
                        })
                }
            }
        })
    }

    getAllUserReport(){
        this.types.forEach(element => {
            if (element.value !== 'ALL') {
                var searchBy = 'USER';                   
                this._testsReportService.getTestBasedOnUser(searchBy,this.userInfo.id, element.value) // to get full reports
                    .then(tests => {
                        this.testsForSelectedType = tests;
                        this.showPiechart = true;
                        this.getValueForChart(this.testsForSelectedType);
                        Helpers.setLoading(false);
                    }, (e: any) => {
                        this.errorPiechart = true;
                        this.msg = e.statusText;
                    });
            }
        });
    }

    getFaclityFilter() {
        var facilityid = this.selectedFacility.id;
        var searchBy = 'FACILITY';
        var type = this.selectedType;
        this._testsReportService.getTestBasedOnFaclity(searchBy, facilityid, type)
            .then(tests => {
                Helpers.setLoading(false);
                this.testsForSelectedType = tests
                this.getValueForChart(this.testsForSelectedType)
                //this.loadPieChart();
            })
    }

    getCenterFilter() {
        var centerId = this.selectedCenter.id;
        var searchBy = 'CENTER';
        var type = this.selectedType;
        if (centerId !== undefined) {
            this._testsReportService.getTestBasedOnCenter(searchBy, this.selectedFacility.id, centerId, type)
                .then(tests => {
                    Helpers.setLoading(false);
                    this.testsForSelectedType = tests
                    this.getValueForChart(this.testsForSelectedType)
                    // this.loadPieChart();
                })
        }
    }

    getUserFilter(){
        var searchBy = 'USER';   
        var type = this.selectedType;                
        this._testsReportService.getTestBasedOnUser(searchBy,this.userInfo.id, type) // to get full reports
            .then(tests => {
                this.testsForSelectedType = tests;
                this.showPiechart = true;
                this.getValueForChart(this.testsForSelectedType);
                Helpers.setLoading(false);
            }, (e: any) => {
                this.errorPiechart = true;
                this.msg = e.statusText;
            });
    }

    reportDisplay() {
        this.displayType.forEach(element => {
            if (this.selectedType !== 'ALL') {
                if (element.value === this.selectedType) {
                    document.getElementById(element.id).style.display = "block"
                    document.getElementById(element.id).classList['value'] = "col-lg-12"
                }
                else {
                    document.getElementById(element.id).style.display = "none"
                }
            } else {
                document.getElementById(element.id).style.display = "block"
                document.getElementById(element.id).classList['value'] = "col-lg-4"
            }
        });
    }

    getReportFilter() {       
           this.arrayClear();
           this.something = false;
           if(this.selectedFacility.id == undefined && !this.selectedType){
             this.getFullReport();
           }
           if(this.selectedFacility.id != 'ALL')    
             this.rangeCalclulation(this.selectedFacility.id)
           else
             this.defaultCal()
        if (this.selectedType) {
            Helpers.setLoading(true);
            if (this.selectedType === 'ALL') {    
                if(this.userType == 'User'){
                    this.getAllUserReport();
                } else{
                    if (this.selectedFacility.id !== "ALL" && this.selectedCenter.id === "ALL") {
                        this.getAllFaclityReport();
                    }
                    else if (this.selectedCenter.id !== "ALL" && this.selectedFacility.id) {
                        this.getAllCenterReport();
                    }
                    else{
                        this.getFullReport();
                    }
                }       
               
            }
            this.reportDisplay();
            if (this.userType === 'Admin') {
                if (this.selectedFacility.id !== "ALL" && this.selectedCenter.id === "ALL") {
                    this.getFaclityFilter();
                }
                else if (this.selectedCenter.id !== "ALL" && this.selectedFacility.id) {
                    this.getCenterFilter();
                }
                else {
                    this._testsReportService.getTestBasedOnType('ALL', this.selectedType)
                        .then(tests => {
                            Helpers.setLoading(false);
                            this.testsForSelectedType = tests;
                            this.getValueForChart(this.testsForSelectedType)
                            //this.loadPieChart();
                        })
                }
            } else if (this.userType === 'Facility') {
                this.getCenterFilter();
            }
            else if(this.userType === 'User'){
                this.getUserFilter();
            }
            else if (this.userType === 'Center') {
                this.getCenterFilter();
            }
        } else {
            if(this.userType == 'User'){
                this.getAllUserReport();
            } else{
                if (this.selectedFacility.id !== "ALL" && this.selectedCenter.id === "ALL") {
                    this.getAllFaclityReport();
                }
                if (this.selectedCenter.id !== "ALL" && this.selectedFacility.id) {
                    this.getAllCenterReport();
                }
                if (this.selectedFacility.id === "ALL") {
                    this.getFullReport();
                }
            }
        }
    }

    loadPieChart() {
        if (this.testsForSelectedType) {
            this.testsForSelectedType['type'] = this.selectedType;
            localStorage.setItem(this.selectedType, JSON.stringify(this.testsForSelectedType))
            this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                'assets/demo/default/custom/components/charts/amcharts/charts.tests.js');
        }
    }

    allTest(id, array, title) {
        // console.log(array)
        setTimeout(() => {
            title = this.AmCharts.makeChart(id, {
                "type": "pie",
                "theme": "light",
                "dataProvider": array,
                "titleField": "title",
                "valueField": "value",
                "colorField": "color",
                "labelRadius": 5,
                "radius": "42%",
                "innerRadius": "70%",
                "labelText": "",
                "legend": {
                    "enabled": true,
                    "align": "center",
                    "markerType": "circle"
                },
                "balloon": {
                    "fixedPosition": true
                },
                "export": {
                    "enabled": true
                }
            });

        }, 500)
        if (array[0].value === 0 && array[1].value === 0 && array[2].value === 0) {
            title.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
        }
    }

    ngAfterViewInit() {
        this.rangeCalclulation(this.userInfo.facilityId)
    }


    facilityChange(value) {
        this.selectedFacility = value;
        this.selectedFacility.organizationCode = value.organizationCode;        
        this.centers = value.centers;  
        if(this.selectedFacility.id != 'ALL'){
            if (this.centers[0].id !== "ALL") {
                let allCenter = new Center();
                allCenter.name = "ALL";
                allCenter.id = "ALL";
                this.centers.splice(0, 0, allCenter);
            }
            this.selectedCenter.id = this.centers[0].id;
            this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
        }else{
            this.selectedCenter.id = 'ALL';
        }    
       
    }

}
