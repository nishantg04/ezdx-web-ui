import { Injectable } from '@angular/core';
import { Headers, Http, ResponseContentType } from '@angular/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { Helpers } from './../../../../../helpers';

import 'rxjs/add/operator/toPromise';

import { Facility } from './facility';
import { Country } from './country';
import { environment } from '../../../../../../environments/environment'
import { promise } from 'selenium-webdriver';

@Injectable()
export class FacilitiesService {

    private headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private headerspdf = new Headers({ 'Content-Type': 'application/json', 'X-Content-Type-Options': 'nosniff', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private serviceUrl2 = environment.BaseURL + environment.OrganizationCommandURL; // 'http://dev.ezdx.healthcubed.com/ezdx-organization-command/api/v1/organizations';  // URL to web api

    private serviceUrl = environment.BaseURL + environment.OrganizationQueryURL;// 'http://dev.ezdx.healthcubed.com/ezdx-organization-query/api/v1/organizations';
    private pdfReportUrl = environment.BaseURL + environment.centerweeklyReport;

    private multipartHeader = new Headers({ 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });

    constructor(private http: Http, private _router: Router, private _script: ScriptLoaderService) { }

    getFacilities(pageno, size): Promise<any> {
        return this.http.get(this.serviceUrl + "?page=" + pageno + "&limit=" + size, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as any })
            .catch(this.handleError.bind(this));
    }

    searchFacilities(criteria, criteriaText, pageno, size, order, sortby) {
        return this.http.get(this.serviceUrl + "/search?page=" + pageno + "&limit=" + size + "&searchcriteria=" + criteria + "&name=" + criteriaText + "&order=" + order + "&sort=" + sortby, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as any })
            .catch(this.handleError.bind(this));
    }

    getCountries(): Promise<Country[]> {
        return this.http.get("../../../assets/demo/default/base/countries.json")
            .toPromise()
            .then(response => { return response.json() as Country[] })
            .catch(this.handleError.bind(this));
    }


    getFacility(id: string): Promise<Facility> {
        const url = '${this.serviceUrl}/code/${id}';
        return this.http.get(this.serviceUrl + "/code/" + id, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as Facility })
            .catch(this.handleError.bind(this));
    }

    getFacilityById(id: string): Promise<Facility> {
        return this.http.get(this.serviceUrl + "/id/" + id, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as Facility })
            .catch(this.handleError.bind(this));
    }

    getCenter(id: string): Promise<Facility> {
        const url = '${this.serviceUrl}/code/${id}';
        return this.http.get(this.serviceUrl + "/id/" + id, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    delete(id: number): Promise<void> {
        const url = `${this.serviceUrl}/${id}`;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(() => null)
            .catch(this.handleError.bind(this));
    }

    create(facility: Facility): Promise<Facility> {
        return this.http
            .post(this.serviceUrl2, JSON.stringify(facility), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError.bind(this));
    }

    update(facility: Facility): Promise<Facility> {
        const url = `${this.serviceUrl}/${facility.id}`;
        return this.http
            .put(this.serviceUrl2, JSON.stringify(facility), { headers: this.headers })
            .toPromise()
            .then(() => facility)
            .catch(this.handleError.bind(this));
    }

    createCenter(id, center): Promise<Facility> {
        const url = this.serviceUrl2 + '/' + id + '/centers';
        return this.http
            .put(url, center, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError.bind(this));
    }

    generatePdf(data): Promise<any> {
        return this.http.put(this.pdfReportUrl, data, { headers: this.headerspdf, responseType: ResponseContentType.Blob })
            .toPromise()
            .then(response => {
                return (response.status === 200) ? new Blob([response.blob()], { type: 'application/pdf' }) : response.statusText;
            })
            .catch(this.handleError.bind(this));
    }

    getOrgCode(code): Promise<any> {
        const url = `${this.serviceUrl}/${'code'}/${code}`;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getCenterDetail(id): Promise<any> {
        return this.http.get(this.serviceUrl + "/centers/" + id, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as any })
            .catch(this.handleError.bind(this));
    }

    uploadLogo(code, data): Promise<any> {
        return this.http.put(this.serviceUrl2 + '/' + code + '/logo', data, { headers: this.multipartHeader })
            .toPromise()
            .then(response => { return response.json() as any })
            .catch(this.handleError.bind(this))
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load('body',
                'assets/demo/default/custom/components/utils/redirect-cidaas-logout.js');
        }
        return Promise.reject(error.message || error);
    }

}