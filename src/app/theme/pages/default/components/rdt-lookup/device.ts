
export class Device {
    id: string;
    organizationId: string;
    centerId: string;
    organizationCode: String;
    serialNumber: string = "";
    deviceType: string = "HC_SE";
    activationDate: string = "";
    selected: boolean;
    assignmentStatus: string;
}