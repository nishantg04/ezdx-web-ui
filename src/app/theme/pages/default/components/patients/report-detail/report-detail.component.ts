import { Component, OnInit, ViewEncapsulation, AfterViewInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { NgForm } from '@angular/forms';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./report-detail.component.html",
    styleUrls: ['./report-details.component.scss'],
    encapsulation: ViewEncapsulation.None,
})

export class ReportDetailComponent implements OnInit, AfterViewInit, OnDestroy {

    visit: any;
    testId: string;
    showRejectReport: boolean = false;
    comment: string;
    constructor(private route: ActivatedRoute, private router: Router) { }


    ngOnInit() {
        this.testId = this.route.snapshot.queryParamMap.get('testId');
        // console.log('test id: ', this.testId)
        this.visit = JSON.parse(localStorage.getItem('visit'));
    }


    ngAfterViewInit() { }

    ngOnDestroy(): void {
        localStorage.removeItem('visit');
    }
}