import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { NotFoundComponent } from "./not-found.component";
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": NotFoundComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes),
        LayoutModule, TranslateModule.forChild({ isolate: false })
    ], exports: [
        RouterModule
    ], declarations: [
        NotFoundComponent
    ],
    providers: [
        TranslateService
    ]
})
export class NotFoundModule {
}