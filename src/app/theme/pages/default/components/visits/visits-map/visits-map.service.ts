import { Injectable } from "@angular/core";
import { Http, Response, Headers } from "@angular/http";
import { environment } from "../../../../../../../environments/environment";
import { Helpers } from "../../../../../../helpers";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";
import 'rxjs/add/operator/toPromise';
import { ICenterData } from "./map-center.entity";

@Injectable()
export class VisitsMapService {
    private headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private serviceUrl = environment.BaseURL + environment.PatientQueryURL;
    private serviceanalytics = environment.BaseURL + environment.analyticsCenter;

    constructor(private http: Http, private _script: ScriptLoaderService) { }

    getAllPatients(): Promise<any> {
        // console.log("Call to get all patients");
        var url = this.serviceUrl + "/search";
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getCenterDetails(): Promise<any> {
        var url = this.serviceanalytics + '/analytics';
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json().results as ICenterData[]; })
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);

        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load('body',
                'assets/demo/default/custom/components/utils/redirect-cidaas-logout.js');
        }
        //alert('An error occurred:' + error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}
