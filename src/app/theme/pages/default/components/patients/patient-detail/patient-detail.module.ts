import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { PatientDetailComponent } from './patient-detail.component';
import { LayoutModule } from "../../../../../layouts/layout.module";
import { DefaultComponent } from "../../../default.component";
import { PatientsService } from "../patients.service";
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { VisitsService } from "../../visits/visits.service";


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": PatientDetailComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule,
        TranslateModule.forChild({ isolate: false }),

    ], exports: [
        RouterModule
    ], declarations: [
        PatientDetailComponent
    ],
    providers: [
        PatientsService, VisitsService,
        TranslateService
    ]

})
export class PatientDetailModule { }
