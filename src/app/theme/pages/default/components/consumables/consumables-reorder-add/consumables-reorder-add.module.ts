import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ConsumablesReorderAddComponent } from './consumables-reorder-add.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { ConsumablesService } from '../consumables.service';
import { OrderModule } from 'ngx-order-pipe';
import { FacilitiesService } from "../../facilities/facilities.service";
import { DataTableModule, SharedModule, MessagesModule } from 'primeng/primeng';
import { VisitsService } from '../../visits/visits.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ConsumablesReorderAddComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule, DataTableModule, SharedModule,
        TranslateModule.forChild({ isolate: false }), MessagesModule
    ], exports: [
        RouterModule
    ], declarations: [
        ConsumablesReorderAddComponent
    ],
    providers: [
        ConsumablesService,
        FacilitiesService,
        VisitsService,
        TranslateService
    ]

})
export class ConsumablesReorderAddModule {



}