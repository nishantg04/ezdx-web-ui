import { async, inject, TestBed, tick } from "@angular/core/testing";

import { MockBackend, MockConnection } from "@angular/http/testing";

import {
    HttpModule,
    Http,
    XHRBackend,
    Response,
    ResponseOptions
} from "@angular/http";

import { Observable } from "rxjs/Observable";
import { StateChangeService } from "./state-change.service";
import 'rxjs';
import { UserChild } from "./userChild";
import { HttpBackend } from "@angular/common/http/src/backend";
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";
import { fakeAsync } from "@angular/core/testing";

////////  Tests  /////////////
describe("StateChange-service (mockBackend)", () => {
    let currentUser = { "fullName": "Nishant", "email": "nishant@healthcubed.com", "token": "dummy-token" };
    window.localStorage.setItem('currentUser', JSON.stringify(currentUser));
    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                imports: [HttpModule],
                providers: [
                    StateChangeService,
                    { provide: XHRBackend, useClass: MockBackend },
                    ScriptLoaderService
                ]
            }).compileComponents();
        })
    );

    it("can instantiate service when inject service",
        inject([StateChangeService], (service: StateChangeService) => {
            expect(service instanceof StateChangeService).toBe(true);
        })
    );

    it(
        "can provide the mockBackend as XHRBackend",
        inject([XHRBackend], (backend: MockBackend) => {
            // console.log("be: ", backend);
            expect(backend).not.toBeNull("backend should be provided");
        })
    );

    describe('when setUserActive', () => {
        let backend: MockBackend;
        let service: StateChangeService;
        let script: ScriptLoaderService;
        let fakeUser: UserChild;
        let response: Response;

        beforeEach(inject([Http, XHRBackend, ScriptLoaderService], (http: Http, be: MockBackend, script: ScriptLoaderService) => {
            backend = be;
            script = script;
            script.load = spyOn(script, 'load').and.callThrough();
            // console.log("script: ", script.load);
            service = new StateChangeService(http, script);
            fakeUser = new UserChild();
            fakeUser.id = "d1ed4281-1bf4-4216-a3bc-6391ddf8da15";
            let option = new ResponseOptions({ status: 200, body: { data: fakeUser } });
            response = new Response(option);
            // console.log("fake user: ", fakeUser);
        }))

        it('It should call activate user', async(inject([], () => {
            backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));
            let userId = fakeUser.id;
            service.setUserActive(userId)
                .do(userUpdated => {
                    // console.log("User Updated: ", userUpdated.id)
                    expect(userUpdated).not.toBeNull('shoult have user updated one');
                    expect(userUpdated.id).toBe(userId);
                    expect(userUpdated.active).toBe(true);
                })
        })));

        it('should treat 404 as an Observable error', async(inject([], () => {
            let resp = new Response(new ResponseOptions({ status: 404 }));
            backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));
            let userId = fakeUser.id;
            service.setUserActive(userId)
                .do(userState => {
                    fail('should not respond with user');
                })
                .catch(err => {
                    expect(err).toMatch(/Bad response status/, 'should catch bad response status code');
                    return Observable.of(null); // failure is the expected test result
                })
                .toPromise();
        })));

        xit('should show an user interaction message for the Bad response(except 401 and 403)', async(inject([], () => {

        })))

        it('should logout on 401 and 403', fakeAsync(inject([], () => {
            /*       let resp = new Response(new ResponseOptions({status: 401}));
                  backend.connections.subscribe((c:MockConnection) => c.mockRespond(resp));
                  let userId = fakeUser.id;
            
                  service.setUserActive(userId)
                  .do(userState => {
                    // console.log("user state: ",userState);
                    fail('should be response with 401 status');
                  })
                  .catch(err => {
                    // console.log("err: ", err);
                    expect(err).toMatch(/Bad response status/, 'should catch bad res status code 401');
                    //tick();
            
                    script.load('load', 'another');
                    expect(script.load).toHaveBeenCalled();
                    return Observable.of(null);
                  })
                  .toPromise(); */
        })))
    });
});