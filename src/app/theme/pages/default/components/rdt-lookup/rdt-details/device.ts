export class Device {
    activationDate: number;
    assignmentStatus: string;
    centerId: string;
    createTime: number;
    deviceStatus?: any;
    deviceType: string;
    firmwareDownloadTime?: any;
    id: string;
    organizationCode: string;
    organizationId: string;
    pythonDownloadTime?: number;
    rdtDownloadTime?: number;
    serialNumber: string;
    updateTime: number;
}
