export class Test {
    id: string;
    testType: string;
    testName: string;
    consumableType: string;
    active: boolean;
    label: String;
}