import { Component, OnInit, ViewEncapsulation, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Helpers } from "../../../../../../helpers";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";
import { Device } from "../device";
import { Center } from "../../facilities/center";
import { Facility } from "../../facilities/facility";
import { FacilitiesService } from "../../facilities/facilities.service";
import { CommonService } from '../../../../../../_services/common.service';
import { RdtService } from "../rdt.service";
import { Observable } from "rxjs/Observable";
import { Utils } from "../../utils/utils";
import { RDTEntity } from "../RDTEntity";
import { RdtLookup } from "../rdtLookup";
import { RDTLookupJson } from "../rdtFile";
import { DeviceService } from "../device.service";
import { forEach } from "@angular/router/src/utils/collection";
import { User } from '../../users/user';
import { UsersService } from '../../users/users.service';
import { UploadFileService } from '../../upload/upload-file.service';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./rdt-details.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class RdtDetailsComponent implements OnInit, AfterViewInit {
    facilitiesLabel: any[] = [];
    centersLabel: any[] = [];
    facilityList: any[] = [];
    facilities: any;
    file: any;
    fileObj: any;
    user = new User();
    devices: Device[] = [];
    rdtId: any;
    deviceList: any;
    viewCustomer: boolean;
    selectedFacility: any;
    selectedCenter: any;
    countires: any;
    country_code: any;
    showPython: boolean = false;
    displayListName: any;
    deviceNo: any;
    deviceArray: any[] = [];
    unassign: boolean = false;
    dummyArray: any[] = [];
    deviceData: any[] = [];
    idList: any[] = [];
    countNo: any;
    isSuccessAssgined: boolean = false;

    constructor(private _rdtService: RdtService, private _router: Router, private _activeroute: ActivatedRoute, private CommonService: CommonService,
        private _usersService: UsersService, private _facilitiesService: FacilitiesService, private _deviceService: DeviceService, private UploadFileService: UploadFileService,
    ) {
        this.countNo = 0;
        this.getCountries();
    }
    ngOnInit() {
        this._activeroute.queryParams.subscribe(param => {
            this.rdtId = param.rdtId;
            if (param.from == 'upload-unassign') {
                this.unassign = true;
            } else {
                this.unassign = false;
            }
            if (param.type) {
                if (param.type == 'PYTHON_SCRIPT') {
                    this.showPython = true;
                    this.displayListName = 'Python'
                }
                if (param.type == 'FIRMWARE') {
                    this.showPython = true;
                    this.displayListName = 'FIRMWARE'
                }
                if (param.type == "RL") {
                    this.displayListName = "RDT";
                }
                if (param.type == "UL") {
                    this.displayListName = "Urine";
                }
            }
        });
        this.getFileDetails();
        this.getFacilities();
    }

    getCountries(): void {
        this.CommonService.getCountries().then(
            countries => {
                this.countires = countries
            }
        );
    }

    getFlag() {
        this.countires.forEach(item => {
            if (item.name == this.user.user.addressInfo.country)
                this.country_code = item.code;
        })
    }

    redirect() {
        if (this.displayListName == 'Python')
            this._router.navigate(['upload/python'])
        if (this.displayListName == 'FIRMWARE')
            this._router.navigate(['upload/firmware'])
        if (this.displayListName == 'RDT')
            this._router.navigate(['rdt/list'], { queryParams: { type: 'RL' } });
        if (this.displayListName == 'Urine')
            this._router.navigate(['rdt/list'], { queryParams: { type: 'UL' } });
    }

    redirectAssign() {
        var type;
        if (this.displayListName == 'Python') {
            type = 'PYTHON_SCRIPT'
            this._router.navigate(['rdt/assign'], { queryParams: { rdtId: this.rdtId, from: 'upload', type: type } })
        }
        if (this.displayListName == 'FIRMWARE') {
            type = 'FIRMWARE'
            this._router.navigate(['rdt/assign'], { queryParams: { rdtId: this.rdtId, from: 'upload', type: type } })
        }
        if (this.displayListName == 'RDT') {
            this._router.navigate(['rdt/assign'], { queryParams: { rdtId: this.rdtId, type: 'RL' } });
        }
        if (this.displayListName == 'Urine') {
            this._router.navigate(['rdt/assign'], { queryParams: { rdtId: this.rdtId, type: 'UL' } });
        }
    }

    ngAfterViewInit() { }

    getFileDetails() {
        this._rdtService.fileDetails(this.rdtId)
            .then(res => {
                this.file = res;
                this.fileObj = res.file;
                this.deviceList = res.deviceList;
                if (this.fileObj.devices)
                    this.deviceNo = this.fileObj.devices.length;
                else {
                    this.deviceNo = 0;
                    if (this.displayListName == 'Python') {
                        this._router.navigate(['upload/python'])
                    }
                    if (this.displayListName == 'FIRMWARE') {
                        this._router.navigate(['upload/firmware'])
                    }
                    if (this.displayListName == 'RDT') {
                        this._router.navigate(['rdt/list'], { queryParams: { type: 'RL' } });
                    }
                    if (this.displayListName == 'Urine') {
                        this._router.navigate(['rdt/list'], { queryParams: { type: 'UL' } });
                    }
                }

                this.mapDeviceFacility();
                if (this.fileObj.userId) {
                    this.getUserDetails();
                }
            });
    }

    redirectHistory() {
        var type;
        if (this.displayListName == 'Python') {
            type = 'PYTHON_SCRIPT'
            this._router.navigate(['rdt/history'], { queryParams: { rdtId: this.rdtId, from: 'upload', type: type } })
        }
        if (this.displayListName == 'FIRMWARE') {
            type = 'FIRMWARE'
            this._router.navigate(['rdt/history'], { queryParams: { rdtId: this.rdtId, from: 'upload', type: type } })
        }
        if (this.displayListName == 'RDT') {
            this._router.navigate(['rdt/history'], { queryParams: { rdtId: this.rdtId } })
        }
    }

    getUserDetails() {
        Helpers.setLoading(true);
        this._usersService.getUser(this.fileObj.userId)
            .then(user => this.user.user = user)
            .then(() => {
                Helpers.setLoading(false);
                this.getFlag();
            })
    }

    getFacilities() {
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        // console.log('list of facilities: ', this.facilities);
    }

    mapDeviceFacility() {
        var devices = [];
        let facilityList: any[] = [];
        var facilityArray: string[] = [];
        var centerArray: string[] = [];
        this.facilitiesLabel = [];
        this.facilityList = [];
        this.centersLabel = [];

        this.fileObj.devices.forEach(element => {
            this.deviceArray.push(element.deviceId)
            facilityArray.push(element.facilityId);
            centerArray.push(element.centerId);
        });

        var facilities = this.facilities;

        // devices.forEach( device => {
        //     facilityArray.push(device.organizationId);
        //     centerArray.push(device.centerId);
        // });
        let filteredFacilityIds = facilityArray.sort().reduce((init, current) => {
            if (init.length === 0 || init[init.length - 1] !== current) {
                init.push(current);
            }
            return init;
        }, []);

        let filteredCenterIds = centerArray.sort().reduce((init, current) => {
            if (init.length === 0 || init[init.length - 1] !== current) {
                init.push(current);
            }
            return init;
        }, []);
        filteredFacilityIds.forEach(facilityE => {
            var existedFacility = this.facilities.find(fElement => {
                if (fElement.id == facilityE) return fElement;
            });
            let centerObj = [];
            existedFacility.centers.forEach(center => {
                filteredCenterIds.forEach(id => {
                    if (id == center.id) centerObj.push(center);
                });
            });
            existedFacility.centers = centerObj;
            facilityList.push(existedFacility);
        });
        this.facilityList = facilityList;
        this.facilityList.forEach(facility => {
            this.facilitiesLabel.push({ 'label': facility.name, 'value': facility });
        })
        this.selectedFacility = this.facilitiesLabel[0].value;
        this.selectedFacility.centers.forEach(center => {
            this.centersLabel.push({ 'label': center.name, 'value': center });
        });
        this.selectedCenter = this.centersLabel[0].value;

        this.renderDevices();
        //this.getDeviceList();

    }

    onDeviceCheck(device): void {
        device.selected = !device.selected;
        if (device.selected == true) {
            this.dummyArray.push(device.id)
        }
        if (device.selected == false) {
            var index = this.dummyArray.indexOf(device.id);
            this.dummyArray.splice(index, 1);
            if (this.deviceData.length != 0) {
                this.deviceData.forEach(data => {
                    if (data.deviceId == device.id) {
                        var i = this.deviceData.indexOf(data)
                        // console.log('index', i)
                        this.deviceData.splice(i, 1)
                    }
                })
            }
        }

        this.idList = this.dummyArray;
        if (this.idList.length != 0) {
            this.countNo = this.idList.length;
        } else
            this.countNo = 0;

        this.devices.forEach(dev => {
            this.idList.forEach(id => {
                if (dev.id == id) {
                    this.deviceData.push({ 'deviceId': dev.serialNumber, 'facilityId': this.selectedFacility.id, 'centerId': this.selectedCenter.id, 'successTime': null, 'failureTime': null })
                }
            })
        })
        var fac = [];
        this.deviceData = this.deviceData.reduceRight(function(r, a) {
            r.some(function(b) {
                if (a.deviceId === b.deviceId) {
                    fac.push(a);
                    return fac;
                }
            }) || r.push(a); return r;

        }, []);
    }

    getDeviceList() {
        this.fileObj.deviceList.forEach(device => {
            if (device.centerId == this.selectedCenter.id) {
                var deviceObj = new Device();
                deviceObj = device as Device;
                this.devices.push(deviceObj);
            }
        });
    }

    renderDevices() {
        this.devices = [];
        this.deviceList.forEach(device => {
            if (device.centerId == this.selectedCenter.id) {
                this.devices.push(device);
            }
        });
        this.devices.forEach(device => {
            if (device.deviceType.includes('_'))
                device.deviceType = device.deviceType.replace('_', ' ')
            if (device.deviceType.includes('_'))
                device.deviceType = device.deviceType.replace('_', ' ')
        })

    }

    onFacilityChange() {
        this.centersLabel = [];
        this.selectedFacility.centers.forEach(center => {
            this.centersLabel.push({ 'label': center.name, 'value': center });
        });
        this.selectedCenter = this.centersLabel[0].value;
        this.onCenterChange();
    }

    onCenterChange() {
        this.renderDevices();
    }

    unAssign() {
        let self = this;
        Helpers.setLoading(true);
        setTimeout(function() {
            self.UploadFileService.uploadDeviceUnassign(self.deviceData, self.fileObj.id).then(data => {
                Helpers.setLoading(false);
                self.countNo = 0;
                self.isSuccessAssgined = true
                self.deviceData = [];
                self.idList = [];
                self.deviceList = [];
                self.fileObj.devices = [];
                self.dummyArray = [];
                self.getFileDetails();
                setTimeout(function() {
                    self.isSuccessAssgined = false;
                }, 3000);
            })
        })
    }

}
