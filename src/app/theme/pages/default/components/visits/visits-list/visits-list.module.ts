import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { VisitsListComponent } from './visits-list.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { VisitsService } from '../visits.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { UsersService } from '../../users/users.service';
import { DataTableModule, SharedModule, PaginatorModule, DialogModule, GMapModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { MapLoaderService } from '../../utils/map-loader.service';
import { DropdownModule } from 'primeng/primeng';
import { FacDropdownModule } from '../../../../../fac-dropdown/fac-dropdown.module'

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": VisitsListComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, FacDropdownModule, DialogModule, GMapModule, DropdownModule,
        DataTableModule, SharedModule, PaginatorModule,
        TranslateModule.forChild({ isolate: false }),

    ], exports: [
        RouterModule
    ], declarations: [
        VisitsListComponent
    ],
    providers: [
        MapLoaderService,
        VisitsService,
        FacilitiesService,
        UsersService,
        TranslateService
    ]

})
export class VisitsListModule {



}