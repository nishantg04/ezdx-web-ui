import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { ConfigService } from '../tests.service';



@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./skinconfig-list.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SkinconfigListComponent implements OnInit {

    constructor(private _testsService: ConfigService) { }

    public listofconfig: Array<any> = [];
    public totalcount: any;
    public row: number = 10;
    public page: number = 1;
    public limit: number = 10;
    public pageranges: any = [{ label: "10", value: 10 },
    { label: "25", value: 25 },
    { label: "50", value: 50 },
    { label: "100", value: 100 }];
    public visiblepagination: boolean = false;

    ngOnInit() {
        this.getconfigfiles(this.page, this.limit);
    }

    getconfigfiles(page, limit) {
        Helpers.setLoading(true);
        this._testsService
            .getconfigfile('ALL', page, limit)
            .then(res => {
                // console.log(res);
                this.listofconfig = res.configs;
                this.totalcount = res.count;
                this.visiblepagination = true;

                Helpers.setLoading(false);
            });
    }
    select(e) {
        this.visiblepagination = false;
        this.limit = e.value;
        this.page = 1;
        this.row = e.value;
        this.getconfigfiles(this.page, this.limit);
    }

    paginate(event) {
        this.page = event.page + 1;
        this.getconfigfiles(this.page, this.limit);

    }

}
