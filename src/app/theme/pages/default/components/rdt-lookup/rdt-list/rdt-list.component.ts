import {
    Component,
    OnInit,
    ViewEncapsulation,
    AfterViewInit,
    NgZone,
    OnDestroy
} from "@angular/core";
import { Router, ActivatedRoute, ParamMap, NavigationEnd } from "@angular/router";
import { Helpers } from "../../../../../../helpers";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";

import { Device } from "../device";
import { DateFormat } from "../dateFormat";
import { RdtService } from "../rdt.service";
import { Facility } from "../../facilities/facility";
import { FacilitiesService } from "../../facilities/facilities.service";
import { Center } from "../../facilities/center";
import { RdtLookup } from "../rdtLookup";
import { RDTEntity } from "../RDTEntity";
import { UrineService } from "../../urine-lookup/urine.service";
import { UrineEntity } from "../../urine-lookup/urine.entity";

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./rdt-list.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class RdtListComponent implements OnInit, AfterViewInit {
    urineDisplay: boolean = false;
    urineLookup: UrineEntity[];
    type: string;
    rdtJsonLookup: RDTEntity[];
    rdtId: any;
    uCount: number;
    aCount: number;
    aLimit: string = "10";
    aPageNo: number = 1;
    uLimit: string = "10";
    uPageNo: number = 1;
    default: any;
    assignedRDT: RdtLookup[] = [];
    showAssign: boolean = false;
    unassignedRDT: RdtLookup[] = [];
    display: boolean = false;
    fileTypeDefault: string = "";
    constructor(
        private _rdtService: RdtService,
        private _urineService: UrineService,
        private _router: Router,
        private _activeroute: ActivatedRoute
    ) {
        /*         this._router.routeReuseStrategy.shouldReuseRoute = function() {
                    return false;
                }
        
                this._router.events.subscribe((evt) => {
                    if (evt instanceof NavigationEnd) {
                        // trick the Router into believing it's last link wasn't previously loaded
                        this._router.navigated = false;
                        // if you need to scroll back to top, here is the right place
                        window.scrollTo(0, 0);
                    }
                }); */

        // let param=  _activeroute.snapshot.params['type'];
        // if (param == "RL")
        //  this.type = "RDT_LOOKUP";
        // else if (param == "UL")
        //     this.type = "URINE_LOOKUP";
        // else
        //     this._router.navigate(["404"]);
    }

    ngOnInit() {
        this._activeroute.queryParams.subscribe(param => {
            // console.log("params: ", param);
            if (param.type == "RL")
                this.type = "RDT_LOOKUP";
            else if (param.type == "UL")
                this.type = "URINE_LOOKUP";
            else
                this._router.navigate(["404"]);

        });
        this.getUnassignedRdt();
        this.getAssignedRdt();
        this.getDefaultLookup();
    }

    ngAfterViewInit() { }

    assignedDetails(id) {
        if (this.type === "RDT_LOOKUP")
            this._router.navigate(["/rdt/details"], { queryParams: { rdtId: id, type: "RL" } });
        else
            this._router.navigate(["/rdt/details"], { queryParams: { rdtId: id, type: "UL" } });
    }
    getUnassignedRdt() {
        Helpers.setLoading(true);
        this._rdtService
            .getLookups(this.type, "UNASSIGNED", this.uPageNo, this.uLimit)
            .then(res => {
                this.unassignedRDT = res.files as RdtLookup[];
                this.uCount = res.count;
                Helpers.setLoading(false);
            });
    }
    getAssignedRdt() {
        Helpers.setLoading(true);
        this._rdtService
            .getLookups(this.type, "ASSIGNED", this.aPageNo, this.aLimit)
            .then(res => {
                this.assignedRDT = res.files as RdtLookup[];
                this.aCount = res.count;
                Helpers.setLoading(false);
            });
    }

    getDefaultLookup() {
        this.fileTypeDefault = "";
        if (this.type == "RDT_LOOKUP") {
            this.fileTypeDefault = "DEFAULT_RDT_LOOKUP";
        } else {
            this.fileTypeDefault = "DEFAULT_URINE_LOOKUP";
        }
        this._rdtService.getDefaultFile(this.fileTypeDefault).then(res => {
            this.default = res.files[0];
            // console.log("res: ", this.default);
        });
    }


    redirectUnAssign(file) {
        this._router.navigate(['rdt/details'], { queryParams: { rdtId: file.id, from: 'upload-unassign' } })
    }

    /*     getUnassignedRdt() {
            Helpers.setLoading(true);
            this._rdtService
                .getUnassignedLookup(this.uPageNo, this.uLimit)
                .then(res => {
                    this.unassignedRDT = res.files as RdtLookup[];
                    this.uCount = res.count;
                    Helpers.setLoading(false);
                });
        }
        getAssignedRdt() {
            Helpers.setLoading(true);
            this._rdtService.getAssignedLookup(this.aPageNo, this.aLimit).then(res => {
                this.assignedRDT = res.files as RdtLookup[];
                this.aCount = res.count;
                Helpers.setLoading(false);
            });
        }
     */
    goToDetails(rdtId) {
        Helpers.setLoading(true);
        if (this.type == "RDT_LOOKUP") {
            this._rdtService.downloadFile(rdtId).then(res => {
                Helpers.setLoading(false);
                // console.log("RDT: ", res);
                this.rdtJsonLookup = res;
                this.display = true;
            });
        }
        if (this.type == "URINE_LOOKUP") {
            this._urineService.downloadFile(rdtId).then(res => {
                Helpers.setLoading(false);
                this.urineLookup = res;
                this.urineDisplay = true;
            })
        }
    }

    paginateUnRDT(event) {
        this.uPageNo = event.page + 1;
        this.getUnassignedRdt();
    }
    paginateRDT(event) {
        this.aPageNo = event.page + 1;
        this.getAssignedRdt();
    }

    goToEdit(rdtId) {
        if (this.type === "RDT_LOOKUP")
            this._router.navigate(["/rdt/update"], { queryParams: { rdtId: rdtId } });
        else
            this._router.navigate(["/urine/add"], { queryParams: { lookupId: rdtId } });
    }

    assignLookup(rdtId) {
        this.showAssign = true;
        if (this.type === "RDT_LOOKUP")
            this._router.navigate(["/rdt/assign"], {
                queryParams: { rdtId: rdtId, type: "RL" }
            });
        else
            this._router.navigate(["/rdt/assign"], {
                queryParams: { rdtId: rdtId, type: "UL" }
            });
    }
}
