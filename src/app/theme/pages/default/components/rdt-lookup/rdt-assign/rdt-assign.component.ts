import {
    Component,
    OnInit,
    ViewEncapsulation,
    AfterViewInit
} from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Helpers } from "../../../../../../helpers";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";

import { Device } from "../device";
import { Center } from "../../facilities/center";
import { Facility } from "../../facilities/facility";
import { FacilitiesService } from "../../facilities/facilities.service";
import { Observable } from "rxjs/Observable";
import { Utils } from "../../utils/utils";
import { RdtService } from '../rdt.service';
import { RDTEntity } from "../RDTEntity";
import { RdtLookup } from "../rdtLookup";
import { RDTLookupJson } from "../rdtFile";
import { DeviceService } from "../device.service";
import { UploadFileService } from '../../upload/upload-file.service';
import { DevicesService } from '../../devices/devices.service';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./rdt-assign.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class RdtAssignComponent implements OnInit, AfterViewInit {
    isSuccessAssgined: boolean = false;
    deviceCount: any;
    filename: string;
    errorMsgSelectDevice: boolean = false;
    selectedDevices: { label: string; value: Device }[] = [];
    devicesLabel: any[];
    devices: Device[];
    checkAll: boolean = false;
    facilities: Facility[];
    centers: Center[];
    facilitiesLabel: { label: string; value: Facility }[] = [];
    rdtLookup: RdtLookup;
    selectedFacility: Facility;
    centersLabel: { label: string; value: Center }[] = [];
    selectedCenter: Center;
    rdtId: any;
    errorMsg: boolean = false;
    pageNo: number = 1;
    limit: number = 10;
    textBody: boolean = false;
    textTemplate: boolean = true;
    showPython: boolean = true;
    displayListName: any;
    selectedId: any[];
    selectedFile: any;
    fileVersion: any;
    deviceNo: any;
    idList: any[] = [];
    dummyArray: any[] = [];
    deviceData: any[] = [];
    fullDevice: any[] = [];
    showMsg: boolean = false;
    countNo: any = 0;
    assignDone: boolean = false;
    searchString: any;
    searchby: any;
    showTable: boolean = false;
    show : boolean = true;
    visiblePagination : boolean = false;
    constructor(private _rdtService: RdtService, private _facilitiesService: FacilitiesService, private _router: Router, private _activeroute: ActivatedRoute,
        private _deviceService: DeviceService, private UploadFileService: UploadFileService, private devicesService: DevicesService
    ) {
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
    }
    ngOnInit() {
        this._activeroute.queryParams.subscribe(param => {
            this.rdtId = param.rdtId;
            if (param.type) {
                if (param.type == 'PYTHON_SCRIPT') {
                    this.showPython = true;
                    this.displayListName = 'Python'
                    this.getFileDetail();
                }
                if (param.type == 'FIRMWARE') {
                    this.showPython = true;
                    this.displayListName = 'FIRMWARE'
                    this.getFileDetail();
                }
                if (param.type == "RL") {
                    // console.log("Inside RDT");
                    this.displayListName = 'RDT'
                    this.getFileDetail();
                }
                if (param.type == "UL") {
                    // console.log("Inside Urine");
                    this.displayListName = "Urine Lookup"
                    this.getFileDetail();
                }
            }


        });

        //  this.getFacilities();
        //this.getCenters();
        if(this.selectedCenter.id !=undefined)
          this.getDevices();
    }

    ngAfterViewInit() { }

    getRdtDetails() {
        this._rdtService.getRdtLookupById(this.rdtId).then(res => {
            this.filename = res.filename;
            this.fileVersion = res.version;
            if (res.deviceId)
                this.deviceNo = res.deviceId.length;
            else
                this.deviceNo = 0;
            this.rdtLookup = res as RdtLookup;
            // console.log("RDT lookup: ", this.rdtLookup);
        });
    }

    getFileDetail() {
        this.UploadFileService.getFileDetails(this.rdtId).then(file => {
            // console.log(file);
            this.selectedFile = file;
            this.filename = this.selectedFile.filename;
            this.fileVersion = this.selectedFile.version;
            if (this.selectedFile.devices) {
                this.deviceData = this.selectedFile.devices;
                this.deviceNo = this.selectedFile.devices.length;
            }
            else
                this.deviceNo = 0;
        })
    }



    oncheckAll(): void {
        this.checkAll = !this.checkAll;
        // this.devices.forEach((device, i) => {
        //     device.selected = this.checkAll;
        // });
        if (this.checkAll == true) {
            this.getFullList();
            this.fullDevice.forEach((device, i) => {
                device.selected = true;
                this.dummyArray.push(device.id)
            });
        }
        if (this.checkAll == false) {
            this.fullDevice.forEach((device, i) => {
                device.selected = false;
                this.dummyArray = [];
                this.deviceData = [];
            });
        }

        this.idList = this.dummyArray;

        this.fullDevice.forEach(dev => {
            this.idList.forEach(id => {
                if (dev.id == id) {
                    this.deviceData.push({ 'deviceId': dev.serialNumber, 'facilityId': dev.organizationId, 'centerId': dev.centerId, 'successTime': null, 'failureTime': null })
                }
            })
        })
        var fac = [];
        this.deviceData = this.deviceData.reduceRight(function(r, a) {
            r.some(function(b) {
                if (a.deviceId === b.deviceId) {
                    fac.push(a);
                    return fac;
                }
            }) || r.push(a); return r;

        }, []);

    }

    getFullList() {
        this._deviceService.getDeviceByCenter(this.selectedCenter.id, 0, 0).then(res => {
            if (res != null) {
                this.fullDevice = res.devices;
            }
        })
    }

    onDeviceCheck(device): void {
        // // console.log('Device: ', device);
        // let allDevicesSelected = true;
        // this.devices.forEach((device, i) => {
        //     if (!device.selected) allDevicesSelected = false;
        // });
        // if (allDevicesSelected) this.checkAll = true;
        // else this.checkAll = false;        
        device.selected = !device.selected;
        if (device.selected == true) {
            this.dummyArray.push(device.id)
        }
        if (device.selected == false) {
            var index = this.dummyArray.indexOf(device.id);
            this.dummyArray.splice(index, 1);
            if (this.deviceData.length != 0) {
                this.deviceData.forEach(data => {
                    if (data.deviceId == device.id) {
                        var i = this.deviceData.indexOf(data)
                        // console.log('index', i)
                        this.deviceData.splice(i, 1)
                    }
                })
            }
        }

        this.idList = this.dummyArray;
        this.countNo = this.idList.length;

        this.devices.forEach(dev => {
            this.idList.forEach(id => {
                if (dev.id == id) {
                    var facilityId;
                    var centerId;
                    if(this.selectedFacility.id && this.selectedCenter.id){
                        facilityId = this.selectedFacility.id
                        centerId = this.selectedCenter.id
                    }else{
                        facilityId = dev['organizationId']
                        centerId = dev['centerId']
                    }
                    this.deviceData.push({ 'deviceId': dev.serialNumber, 'facilityId': facilityId, 'centerId': centerId, 'successTime': null, 'failureTime': null })
                }
            })
        })
        var fac = [];
        this.deviceData = this.deviceData.reduceRight(function(r, a) {
            r.some(function(b) {
                if (a.deviceId === b.deviceId) {
                    fac.push(a);
                    return fac;
                }
            }) || r.push(a); return r;

        }, []);
    }

    redirect(displayListName) {
        // console.log("display name: ", displayListName);
        if (displayListName == 'Python')
            this._router.navigate(['upload/python'])
        if (displayListName == 'FIRMWARE')
            this._router.navigate(['upload/firmware'])
        if (displayListName == 'RDT')
            this._router.navigate(['rdt/list'], { queryParams: { type: 'RL' } });
        //this._router.navigate(['rdt/list/RL']);
        if (displayListName == "Urine Lookup") {
            this._router.navigate(['rdt/list'], { queryParams: { type: 'UL' } });
            //this._router.navigate(['rdt/list/UL']);
        }
    }

    onFacilityChange() {
        this.centersLabel = [];
        this.selectedCenter;
        if (this.selectedFacility.centers.length > 0) {
            this.selectedFacility.centers.forEach(center => {
                this.centersLabel = [
                    ...this.centersLabel,
                    { label: center.name, value: center }
                ];
            });
            this.selectedCenter = this.centersLabel[0].value;
            this.getDevices();
        }
    }

    facilityChange(value) {
        this.centers = [];
        this.selectedFacility = value;
        this.selectedFacility.organizationCode = value.organizationCode;
        this.selectedFacility.id = value.id;
        this.centers = value.centers;
        this.searchString = '';
        if (this.selectedFacility.centers) {
            this.centers = this.selectedFacility.centers;
            this.selectedCenter.id = this.centers[0].id;
        } else {
            this.centers = null;
            this.selectedCenter.id = null;
        }
        if (!this.searchString)
            this.getDevices();
        else
            this.searchingHub(this.searchString)
    }

    onCenterChange() {
        if (!this.searchString)
            this.getDevices();
        else
            this.searchingHub(this.searchString)
    }

    searchingHub(event) {
        this.searchString = event;
        if (this.searchString == '') {
            this.getDevices();
        }
        var page; var size;
        if (this.searchString.length >= 3) {
            this.showTable = true;
            Helpers.setLoading(true);
            this.devices = [];

            page = this.pageNo;
            size = this.limit;

            if (!this.selectedFacility.id) {
                this.searchby = 'DEVICE_SERIAL_NUMBER_WITH_STATUS'
                this.getAllHub(page, size)
            }
            if (this.selectedFacility.id != 'ALL' && this.selectedCenter.id && this.selectedCenter.id != 'ALL') {
                this.searchby = 'DEVICE_SERIAL_NUMBER_WITH_STATUS_AND_CENTER'
                this.getCenterHub(page, size, this.selectedCenter.id)
            }
            if (this.selectedFacility.id && this.selectedCenter.id == 'ALL') {
                this.searchby = 'DEVICE_SERIAL_NUMBER_WITH_STATUS_AND_FACILITY'
                this.getCenterHub(page, size, this.selectedFacility.id)
            }

        }
    }

    clearClick() {
        if (this.selectedCenter.id) {
            this.getDevices();
        } else {
            this.devices = [];
            this.showTable = false;
        }
    }

    getCenterHub(page, size, id) {
        this.devices = [];
        this.devicesService.getSearchByCenter(this.searchby, this.searchString, 'ASSIGNED', id, page, size)
            .then(devices => {
                if (devices != null) {
                    if (devices.devices) {
                        this.devices = devices.devices;
                        this.deviceCount = devices.count;
                    }
                }
                else {
                    this.devices = [];
                    this.deviceCount = 0;
                }

                Helpers.setLoading(false);

            })
    }

    getAllHub(page, size) {
        this.devices = [];
        this.devicesService.getSearch(this.searchby, this.searchString, 'ASSIGNED', page, size)
            .then(devices => {
                if (devices != null) {
                    if (devices.devices) {
                        this.devices = devices.devices;
                        this.deviceCount = devices.count;
                    }
                }
                else {
                    this.devices = [];
                    this.deviceCount = 0;
                }
                Helpers.setLoading(false);
            })
    }


    getDevices() {
        Helpers.setLoading(true);
        this._deviceService.getDeviceByCenter(this.selectedCenter.id, this.pageNo, this.limit).then(res => {
            if (res != null) {
                this.devices = res.devices;
                this.showTable = true;
                this.devices.forEach(device => {
                    if (this.idList.length != 0) {
                        this.idList.forEach(id => {
                            if (device.id == id)
                                device.selected = true;
                        })
                    } else
                        device.selected = false;
                    if (device.deviceType.includes('_'))
                        device.deviceType = device.deviceType.replace('_', ' ')
                    if (device.deviceType.includes('_'))
                        device.deviceType = device.deviceType.replace('_', ' ')
                })
                this.deviceCount = res.count;
                Helpers.setLoading(false);
            } else {
                this.devices = [];
                this.deviceCount = 0;
                Helpers.setLoading(false);
            }
        });
    }

    paginate(event) {
        // console.log("event: ", event);
        this.pageNo = event.page + 1;
        if(this.searchString.length >= 3){
            this.searchingHub(this.searchString)
        }else{
            this.getDevices();
        }        
    }

    getIdArray(objArray: any[]): string[] {
        var arrayOfId: string[] = [];
        objArray.forEach(obj => {
            arrayOfId = [...arrayOfId, obj.id];
        });
        return arrayOfId;
    }

    check() {
        if (this.displayListName == 'Python' || this.displayListName == 'FIRMWARE') {
            this.reassign();
        } else {
            this.reassign();
        }
    }

    redirectAssign() {
        var type;
        if (this.displayListName == 'Python') {
            type = 'PYTHON_SCRIPT'
            this._router.navigate(['rdt/details'], { queryParams: { rdtId: this.rdtId, from: 'upload', type: type } })
        }
        if (this.displayListName == 'FIRMWARE') {
            type = 'FIRMWARE'
            this._router.navigate(['rdt/details'], { queryParams: { rdtId: this.rdtId, from: 'upload', type: type } })
        }
        if (this.displayListName == 'RDT') {
            this._router.navigate(['rdt/details'], { queryParams: { rdtId: this.rdtId, type: "RL" } })
        }
        if (this.displayListName == 'Urine Lookup') {
            this._router.navigate(['rdt/details'], { queryParams: { rdtId: this.rdtId, type: "UL" } })
        }
    }

    assignRDT() {
        Helpers.setLoading(true);
        // console.log("Object to be sent: ", this.rdtLookup);
        var selectedDevices = this.devices.filter(device => device.selected);
        // console.log("Devices: ", selectedDevices);
        if (this.rdtLookup.deviceId === null) {
            this.rdtLookup.deviceId = [];
            this.rdtLookup.deviceId = this.getIdArray(selectedDevices);
        } else {
            let arrayAssign = [...this.rdtLookup.deviceId, ...this.getIdArray(selectedDevices)];
            // console.log(this.rdtLookup.deviceId);
            this.rdtLookup.deviceId = arrayAssign.sort().reduce((init, current) => {
                if (init.length === 0 || init[init.length - 1] !== current) {
                    init.push(current);
                }
                return init;
            }, []);
        }

        // console.log("device id repeating? ", this.rdtLookup.deviceId);
        this._rdtService.assignRdtLookup(this.rdtLookup).then(res => {
            Helpers.setLoading(false);
            this.isSuccessAssgined = true;
            setTimeout(() => { this.isSuccessAssgined = false }, 800);
            
        });
    }

    reassign() {
        Helpers.setLoading(true);
        console.log(this.deviceData)          
        this.UploadFileService.uploadDeviceId(this.deviceData, this.selectedFile.id).then(data => {
            Helpers.setLoading(false);
            this.searchString='';
            this.isSuccessAssgined = true
            setTimeout(() => { this.isSuccessAssgined = false }, 800);
            this.selectedFacility = new Facility();
            this.selectedCenter = new Center();   
            this.show = false;
            setTimeout(()=>{ this.show = true},0) 
            this.deviceData = [];
            this.dummyArray = [];
            this.idList = [];
            this.countNo = this.idList.length;
            this.showTable  = false;
            this.assignDone = true;
            this.getFileDetail();
        })
    }



    prepareData() {
        let self = this;
        self.selectedId = [];
        let newCenter = [];
        let newFacility = [];
        var dummy = [];
        var dummyArray = [];
        var fac = [];
        var dummyFacArray = [];
        var deviceSerial = [];
        var tempDevice = [];
        tempDevice = self.devices.filter(device => device.selected);

        tempDevice.forEach(device => {
            deviceSerial.push(device.serialNumber)
        })

        self.selectedId = deviceSerial;

        if (self.selectedFile.deviceId) {
            dummyArray = self.selectedId.concat(self.selectedFile.deviceId)
        }

        if (dummyArray.length != 0) {
            var names_array_new = dummyArray.reduceRight(function(r, a) {
                r.some(function(b) {
                    if (a === b) {
                        dummy.push(a);
                        return dummy;
                    }
                }) || r.push(a); return r;

            }, []);
            // console.log('array', names_array_new)
            self.selectedId = names_array_new;
        }

        if (self.selectedFile.facilityId) {
            self.selectedFile.facilityId.forEach(id => {
                newFacility.push(id);
            });
        }

        if (self.selectedFile.centerId) {
            self.selectedFile.centerId.forEach(id => {
                if (id != self.selectedCenter.id) {
                    newFacility.push(self.selectedFacility.id)
                    newCenter.push(id)
                }
                //  else{
                //     self.selectedFile.facilityId.forEach(faci => {
                //             if(faci == self.selectedFacility.id) 
                //             newFacility.splice(self.selectedFacility.id)   
                //     });                            
                // }              
            });
        }

        dummyFacArray = newFacility;

        if (dummyFacArray.length != 0) {
            var names_array_fac = dummyFacArray.reduceRight(function(r, a) {
                r.some(function(b) {
                    if (a === b) {
                        fac.push(a);
                        return fac;
                    }
                }) || r.push(a); return r;

            }, []);
            newFacility = names_array_fac;
        }
        if (self.selectedCenter.id && self.selectedFacility.id && self.selectedId) {
            newCenter.push(self.selectedCenter.id)
            newFacility.push(self.selectedFacility.id)
        }


        let parsedData = self.selectedFile;
        parsedData.centerId = newCenter;
        parsedData.facilityId = newFacility;
        parsedData.deviceId = self.selectedId;
        parsedData.assignmentStatus = "ASSIGNED";
        return parsedData;
    }

    pageOne(type){
        if(type == 'search'){
            if(this.searchString.length >=3){
                this.pageNo=1;
                this.visiblePagination = false;       
                setTimeout(() => this.visiblePagination = true, 0);
            }
        }else{
            this.pageNo=1;
            this.visiblePagination = false;       
            setTimeout(() => this.visiblePagination = true, 0);
        }
        
    }

    remove(){
        this.selectedFacility.name = ''
        this.selectedFacility = new Facility;
        this.devices = [];       
        this.showTable = false;
    }

}
