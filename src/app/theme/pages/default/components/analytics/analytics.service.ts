import { Injectable } from '@angular/core';
import { Headers, Http } from "@angular/http";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";
import "rxjs/add/operator/toPromise";
import { Helpers } from "./../../../../../helpers";

import { environment } from "../../../../../../environments/environment";

@Injectable()
export class AnalyticsService {

    private headers = new Headers({
        "Content-Type": "application/json",
        Authorization:
            "Bearer " + JSON.parse(localStorage.getItem("currentUser")).token
    });
    private serviceUrlUser = environment.BaseURL + environment.UserQueryURL; //'http://dev.ezdx.healthcubed.com/ezdx-device-command/api/v1/users';  // URL to web api
    private serviceUrlOrg = environment.BaseURL + environment.OrganizationQueryURL; //'http://dev.ezdx.healthcubed.com/ezdx-device-command/api/v1/organisation';  // URL to web api
    private serviceUrlCenter = environment.BaseURL + environment.CenterQueryURL;
    private serviceanalytics = environment.BaseURL + environment.analyticsCenter;
    private serviceanalyticsuser = environment.BaseURL + environment.analyticsUser;
    private servicePatient= environment.BaseURL + environment.PatientQueryURL;
    private serviceVisits = environment.BaseURL + environment.repeatVisits;
    constructor(
        private http: Http,
        private _router: Router,
        private _script: ScriptLoaderService
    ) { }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load(
                "body",
                "assets/demo/default/custom/components/utils/redirect-cidaas-logout.js"
            );
        }
        return Promise.reject(error.message || error);
    }

    getvisitLogs(from, to): Promise<any[]> {
        return this.http
            .get(this.serviceUrlUser + '/stats?from=' + from + '&to=' + to, {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getAlluserAnalytics(): Promise<any[]> {
        return this.http
            .get(this.serviceanalyticsuser + '/analytics/all', {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getuserAnalyticsSearch(type,facility): Promise<any[]> {
        return this.http
            .get(this.serviceanalyticsuser + '/analytics/search?'+type+'='+facility+'&page=1&limit=10000', {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }


    getAlluserRevisits(from,to): Promise<any[]> {
        return this.http
            .get(this.serviceVisits+'?from=' + from + '&to=' + to, {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getAlluserPatientCount(from,to): Promise<any[]> {
        return this.http
            .get(this.servicePatient+'/aggregation/user?from=' + from + '&to=' + to, {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getCenterWiseRegistrationStats(from, to): Promise<any[]> {
        return this.http
            .get(this.serviceUrlOrg + '/centers/stats?page=' + from + '&limit=' + to, {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getCenterAnalyticsSearch(facilityId): Promise<any[]> {
        return this.http
            .get(this.serviceanalytics + '/analytics/search?facility=' + facilityId, {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getCenterWiseRegistrationStatsCenterWise(centerid): Promise<any[]> {
        return this.http
            .get(this.serviceUrlCenter + '/' + centerid + '/stats', {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getavgdiagnosticAndConsumablebasedtest(from, to): Promise<any[]> {
        return this.http
            .get(this.serviceanalytics + '/analytics/search?page=' + from + '&limit=' + to, {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getavgdiagnosticAndConsumablebasedtestFacility(facility, from, to): Promise<any[]> {
        return this.http
            .get(this.serviceanalytics + '/analytics/search?facility='+facility+'&page='+from + '&limit=' + to, {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getanalytics(): Promise<any[]> {
        return this.http
            .get(this.serviceanalytics + '/analytics', {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getanalyticsUserSearchPaginate(from, to): Promise<any[]> {
        return this.http
        .get(this.serviceanalyticsuser + '/analytics/search?page='+from + '&limit=' + to, {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getanalyticsUserSearchPaginateFacility(type,facility, from, to): Promise<any[]> {
        return this.http
        .get(this.serviceanalyticsuser + '/analytics/search?'+type +'='+facility+'&page='+from + '&limit=' + to, {
                headers: this.headers
            })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }
   




}
