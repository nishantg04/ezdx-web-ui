import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';

import { User } from '../user';
import { UsersService } from '../users.service';
import { Role } from '../role';
import { Facility } from '../../facilities/facility';
import { FacilitiesService } from '../../facilities/facilities.service';
import { Center } from '../../facilities/center';
import { parse } from 'path';
import { StateChangeService } from '../state-change.service';
import { log } from 'util';
import { UserChild } from '../../users/userChild';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./users-list.component.html",
    styleUrls: ["../../../primeng.component.scss", "./users-list.component.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class UsersListComponent implements OnInit, AfterViewInit {
    userToDeactivate: any;
    userType: string;
    userInfo: UserChild;
    users: any[];
    hcRoles: Role[];
    facilityRoles: Role[];
    facilities: Facility[];
    selectedFacility: Facility;
    selectedCenter: Center;
    centers: Center[];
    years: any[];
    ranges: any[];
    selectedYear: any;
    fromDate: string;
    toDate: string;
    facilityId: string;
    centerId: string;
    roles: any;
    isOperator: boolean = false;
    pageNo: number;
    row: number;
    pageSize: number;
    userCount: string;
    SearchBylist: any;
    sortBylist: any;
    selectedSearchBy: any;
    selectedBySort: any;
    searchString: string;
    isSearched: boolean;
    displayDialog: boolean = false;
    visiblepagination: boolean = false;
    hideFilter: boolean = false;
    isDoctor : boolean = false;

    constructor(private _ngZone: NgZone, private _usersService: UsersService, private _script: ScriptLoaderService, private _router: Router, private _activeroute: ActivatedRoute, private _facilitiesService: FacilitiesService, private _stateService: StateChangeService) {

        this.searchString = ''
        this.isSearched = false;
        this.row = 10;
        this.hcRoles = [
            { name: 'Admin', type: 'HC_ADMIN', selected: false },
            { name: 'Operations', type: 'HC_OPERATIONS', selected: false },
            { name: 'Manager', type: 'HC_MANAGER', selected: false }
        ];
        this.facilityRoles = [
            { name: 'Healthworker/Operator', type: 'OPERATOR', selected: false },
            { name: 'Doctor', type: 'DOCTOR', selected: false },
            { name: 'Center Admin', type: 'CENTER_ADMIN', selected: false },
            { name: 'Facility Admin/Manager/Technician', type: 'TECHNICIAN', selected: false }
        ];
        this.roles = [
            { name: 'HC Admin', type: 'HC_ADMIN' },
            { name: 'HC Operations', type: 'HC_OPERATIONS' },
            { name: 'HC Manager', type: 'HC_MANAGER' },
            { name: 'Operator', type: 'OPERATOR' },
            { name: 'Doctor', type: 'DOCTOR' },
            { name: 'Technician', type: 'TECHNICIAN' },
            { name: 'Center Admin', type: 'CENTER_ADMIN' },
            { name: 'Clinical User', type: 'CLINICAL_USER' },
            { name: 'Pathologist', type: 'PATHOLOGIST' }
        ];
        this.ranges = [
            { label: "10", value: 10 },
            { label: "25", value: 25 },
            { label: "50", value: 50 },
            { label: "100", value: 100 },
        ];
        this.SearchBylist = [
            { id: '0', name: 'Name', keytoSend: 'NAME' },
            { id: '1', name: 'Email', keytoSend: 'EMAIL' }  // Key to send should be match with service call
        ]
        this.sortBylist = [
            { id: '0', name: 'Ascending', keytoSend: 'asc' },
            { id: '1', name: 'Descending', keytoSend: 'desc' }
        ]
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        if (window.localStorage.getItem('userType') == 'Facility')
            this.userType = 'Facility';
        else if (window.localStorage.getItem('userType') == 'Center')
            this.userType = 'Center';
        else
            this.userType = 'Admin';

        this.pageNo = 1;
        this.pageSize = 10;

        this.selectedSearchBy = this.SearchBylist[0];
        this.selectedBySort = this.sortBylist[0];
    }


    ngOnInit() {
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
        window.my.namespace.userUpdatePublicFunc = this.userUpdatePublicFunc.bind(this);
        window.my.namespace.userViewPublicFunc = this.userViewPublicFunc.bind(this);
        let roles = JSON.parse(window.localStorage.getItem('userInfo')).userRoles;
        roles.filter((role) => role === "TECHNICIAN").length > 0 ? this.isOperator = true : this.isOperator = false;
        this.getAllFacilities();
        
    }
    ngOnDestroy() {
        window.my.namespace.publicFunc = null;
    }

    select(e) {
        // console.log(e);
        this.pageSize = e.value;
        this.pageNo = 1;
        this.row = e.value;
       // this.onFilterClick();
       if (this.isSearched) {
        this.filterUser(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), this.selectedSearchBy.keytoSend, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.pageSize)
    }
    else {
        if (this.selectedFacility.id != "ALL" && this.selectedFacility.id != undefined) {
            if (this.selectedCenter.id != "ALL" && this.selectedCenter.id !=undefined) {
                this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedCenter.id, this.pageNo, this.pageSize);
            }
            else {
                this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "FACILITY", this.selectedFacility.id, this.pageNo, this.pageSize);
            }
        }
        else {
            var endDate = new Date();
            endDate.setDate(endDate.getDate() + 1);
            endDate.setHours(23);
            endDate.setMinutes(23);
            endDate.setSeconds(23);
            endDate.setMilliseconds(0);
            this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0, this.pageNo, this.pageSize);
        }
    }

    }

    userUpdatePublicFunc(userId) {
        this._ngZone.run(() => this.goToUserEdit(userId));
    }

    userViewPublicFunc(userId) {
        this._ngZone.run(() => this.goToUserView(userId));
    }

    goToUserView(userId) {
        this._router.navigate(['/users/details'], { queryParams: { userId: userId } });
    }

    goToUserEdit(userId) {
        this._router.navigate(['/users/update'], { queryParams: { userId: userId } });
    }
    ngAfterViewInit() {
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/datatables/base/date.patients.js');
        window.localStorage.removeItem('patientStartDate');
        window.localStorage.removeItem('patientEndDate');
        this.fromDate = this._activeroute.snapshot.queryParams['from'];
        this.toDate = this._activeroute.snapshot.queryParams['to'];
        this.selectedFacility.id = this._activeroute.snapshot.queryParams['facilityId'];
        this.selectedCenter.id = this._activeroute.snapshot.queryParams['centerId'];
        this.getFacilities();
        if (this.fromDate && this.toDate) {
            window.localStorage.setItem('patientStartDate', this.fromDate);
            window.localStorage.setItem('patientEndDate', this.toDate);
            this.onFilterClick();
        }
        
        // var facilitiesJSON = '[{"name":"Darshan Hospital","email":"admin@darshan.co.in"}]';
        // localStorage.setItem('facilityTable', facilitiesJSON);
        // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
        //     'assets/demo/default/custom/components/datatables/base/data-local.1.js');
    }

    onFilterClick(): void {
        this.searchString='';
        this.isSearched = false;
        this.visiblepagination = false;
        setTimeout(() => this.visiblepagination = true, 0);
        this.selectedSearchBy = this.SearchBylist[0];
        this.selectedBySort = this.sortBylist[0];
        if (this.userType == 'Center') {
            this.selectedCenter.id = this.userInfo.centerId;
            this.selectedFacility.id = this.userInfo.facilityId;
        }
        if (this.selectedFacility.id != "ALL" && this.selectedFacility.id != undefined) {
            if (this.selectedCenter.id != "ALL") {
                this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedCenter.id, this.pageNo, this.pageSize);
            }
            else {
                this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "FACILITY", this.selectedFacility.id, this.pageNo, this.pageSize);
            }
        }
        else {
            var endDate = new Date();
            endDate.setDate(endDate.getDate() + 1);
            endDate.setHours(23);
            endDate.setMinutes(23);
            endDate.setSeconds(23);
            endDate.setMilliseconds(0);
            this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0, this.pageNo, this.pageSize);
            
        }
    }

    getFacilities(): void {
        // Helpers.setLoading(true);
        // this._facilitiesService.getFacilities(1,10)
        //     .then(facilities => this.facilities = facilities.organizations)
        //     .then(() => {
        //         Helpers.setLoading(false);        
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            } else if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - 29);
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
        startDate.setMilliseconds(0);
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        endDate.setHours(23);
        endDate.setMinutes(23);
        endDate.setSeconds(23);
        endDate.setMilliseconds(0);
        if (!this.fromDate && !this.toDate) {
            this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
            if (window.localStorage.getItem('userType') == 'Facility') {
                this.selectedFacility.id = this.userInfo.facilityId;
                this.facilities.forEach(fclt => {
                    if (fclt.id == this.userInfo.facilityId) {
                        this.centers = jQuery.extend(true, [], fclt.centers);
                        let allCenter = new Center();
                        allCenter.name = "ALL";
                        allCenter.id = "ALL";
                        this.centers.splice(0, 0, allCenter);
                        this.selectedCenter.id = this.centers[0].id;
                        console.log(this.selectedCenter.id)
                    }
                });
            }
            else {
                //this.list(0, new Date(endDate).getTime(), "ALL", 0);
                let allFacility = new Facility();
                allFacility.name = "ALL";
                allFacility.id = "ALL";
                this.facilities.splice(0, 0, allFacility);
                this.selectedFacility.id = this.facilities[0].id;
                this.centers = this.facilities[0].centers;
                let allCenter = new Center();
                allCenter.name = "ALL";
                allCenter.id = "ALL";
                // this.centers.splice(0, 0, allCenter);
                // this.selectedCenter.id = this.centers[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
            let searchcriteria = "ALL";
            if (window.localStorage.getItem('userType') === "Facility") {
                searchcriteria = "FACILITY";
                this.list(0, new Date(endDate).getTime(), searchcriteria, this.selectedFacility.id, this.pageNo, this.pageSize);
            }
            else if (window.localStorage.getItem('userType') === "Center") {
                searchcriteria = "CENTER";
                this.selectedFacility.id = this.userInfo.facilityId;
                this.selectedCenter.id = this.userInfo.centerId;
                this.hideFilter = true;
                this.list(0, new Date(endDate).getTime(), searchcriteria, this.selectedCenter.id, this.pageNo, this.pageSize);
            }
            else {
                this.list(0, new Date(endDate).getTime(), searchcriteria, 0, this.pageNo, this.pageSize);
            }
        }
        else {
            let allFacility = new Facility();
            allFacility.name = "ALL";
            allFacility.id = "ALL";
            this.facilities.splice(0, 0, allFacility);
            if (this.selectedFacility.id != "ALL") {
                this.facilities.forEach(fac => {
                    if (fac.id === this.selectedFacility.id) {
                        this.centers = fac.centers;
                        let allCenter = new Center();
                        allCenter.name = "ALL";
                        allCenter.id = "ALL";
                        this.centers.splice(0, 0, allCenter);
                    }
                });
            }
        }
        // });
    }



    statusChange(event, user) {
        this.userToDeactivate = user;
        if (event) {
            Helpers.setLoading(true);
            this._stateService.setUserActive(user.id).then(() => {
                Helpers.setLoading(false);
            });
        } else {
            this.displayDialog = true;
        }
    }


    deactivate() {
        this.displayDialog = false;
        Helpers.setLoading(true);
        this._stateService.setUserDeactive(this.userToDeactivate.id)
            .then(() => {
                Helpers.setLoading(false);
            });
    }

    deactivationCancel() {
        this.users.forEach((user) => {
            if (user.id === this.userToDeactivate.id)
                user.active = !this.userToDeactivate.active;
        });
        this.displayDialog = false;
    }

    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers[0].id !== "ALL") {
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                }
                this.selectedCenter.id = this.centers[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }

    list(fromDate, toDate, searchcriteria, id, pageno, size): void {
        Helpers.setLoading(true);
        this._usersService.getUsersFiltered(fromDate, toDate, searchcriteria, id, pageno, size, '', 'asc')
            .then(response => {
                Helpers.setLoading(false);
                this.visiblepagination = true;
                if (response != null) {
                    this.users = response.users;
                    this.userCount = response.count.toString();
                    this.getCustomerCenterRole();
                } else {
                    this.users = [];
                    this.userCount = '0'
                }
            })
    }

    getAllFacilities() {
        this.facilities = [];
        this._facilitiesService.getFacilities(0, 0)  // for get All facility we should pass page and size as zero
            .then(facilities => {
                localStorage.setItem('facilityTable', JSON.stringify(facilities.organizations)); // for save all facility details              
            })
    }


    getCustomerCenterRole() {

        this.users.forEach(user => {
            user.firstName = user.firstName.toLowerCase();
            user.lastName = user.lastName.toLowerCase();
        });  //HC Role Specification Checking
        this.users.forEach(user => {
            user.userRoles.sort(function(a, b) {
                if (a > b) { return -1; } else if (a < b) { return 1; } return 0;
            });

            var userrole = user.userRoles.toString();
            if (userrole.includes('HC')) {
                if (user.userRoles[0] != 'HC_OPERATIONS') {
                    if (user.centerId)
                        delete user['centerId'];
                    if (user.facilityId) {
                        delete user['facilityId'];
                    }
                    if (user.facilityCode) {
                        delete user['facilityCode'];
                    }
                }
            }

            if (user.centerId) {
                this.facilities.forEach(fct => {
                    if (fct.centers) {
                        fct.centers.forEach(cnt => {
                            if (cnt.id === user.centerId)
                                user.centerName = cnt.name;
                        });
                    }
                });
            }
            user.userRoles.forEach(role => {
                this.roles.forEach(element => {
                    if (role == element.type) {
                        var index = user.userRoles.indexOf(role)
                        user.userRoles[index] = element.name;
                        //user.userRoles.toString().replace(user.userRoles.toString(), element.name)   
                        //// console.log('role', user.userRoles[role])                              
                    }
                });
            });
        });
        var dataJSONArray = this.users;
        var facilityJSONArray = JSON.parse(localStorage.getItem('facilityTable'))
        var roleString = '';
        for (var j = 0; j < dataJSONArray.length; j++) {
            roleString = '';
            if (dataJSONArray[j].userRoles) {
                for (var i = 0; i < dataJSONArray[j].userRoles.length; i++) {
                    if (i != 0)
                        roleString += ', ' + dataJSONArray[j].userRoles[i];
                    else
                        roleString = dataJSONArray[j].userRoles[i];
                }
            }
            dataJSONArray[j].roleString = roleString;
        }

        for (var i = 0; i < dataJSONArray.length; i++) {
            if (dataJSONArray[i].facilityId !== undefined) {
                for (var k = 0; k < facilityJSONArray.length; k++) {
                    if (facilityJSONArray[k].id === dataJSONArray[i].facilityId) {
                        dataJSONArray[i].facilityName = facilityJSONArray[k].name;
                    }
                    if (dataJSONArray[i].centerId !== undefined && facilityJSONArray[k].centers !== undefined) {
                        for (var j = 0; j < facilityJSONArray[k].centers.length; j++) {
                            if (facilityJSONArray[k].centers[j].id === dataJSONArray[i].centerId[0]) {
                                dataJSONArray[i].centerName = facilityJSONArray[k].centers[j].name;
                            }

                        }
                    }
                }
            }


        }
        this.users = dataJSONArray;
        localStorage.setItem('userTable', JSON.stringify(this.users));
        if (this._router['currentRouterState'].snapshot.url === "/users/list") {
            // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            //     'assets/demo/default/custom/components/datatables/base/data-local.users.js');
        }
        Helpers.setLoading(false);
        // let self = this;
        // setTimeout(() => {
        //     jQuery.each(jQuery('.edit-facility'), function (i, val) {
        //         jQuery(val).click(function () {
        //             self._router.navigate(['/facilities/update'], { queryParams: { facilityId: jQuery(val).attr('id') } });
        //         });
        //     });
        // }, 1000);
    }


    paginate(event) {
        this.pageNo = event.page + 1;
        this.pageSize = parseInt(event.rows);
        if (this.isSearched) {
            this.filterUser(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), this.selectedSearchBy.keytoSend, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.pageSize)
        }
        else {
            if (this.selectedFacility.id != "ALL" && this.selectedFacility.id != undefined) {
                if (this.selectedCenter.id != "ALL" && this.selectedCenter.id !=undefined) {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedCenter.id, this.pageNo, this.pageSize);
                }
                else {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "FACILITY", this.selectedFacility.id, this.pageNo, this.pageSize);
                }
            }
            else {
                var endDate = new Date();
                endDate.setDate(endDate.getDate() + 1);
                endDate.setHours(23);
                endDate.setMinutes(23);
                endDate.setSeconds(23);
                endDate.setMilliseconds(0);
                this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0, this.pageNo, this.pageSize);
            }
        }

    }

    searchUser(event) {
        this.searchString = event.target.value;
        if(this.userType == 'Center'){
            this.selectedFacility.id = this.userInfo.facilityId;
            this.selectedCenter.id = this.userInfo.centerId;
        }
        if(this.userType == 'Facility'){
            this.selectedFacility.id = this.userInfo.facilityId;  
            this.selectedCenter.id = 'ALL'         
        }
        
        if (this.searchString == '') {
            this.onFilterClick();
        }
        if (this.selectedFacility.id == 'ALL' || this.selectedCenter.id == undefined) {
            if (this.searchString.length >= 3) {
                this.isSearched = true;
                this.filterUser(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), this.selectedSearchBy.keytoSend, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.pageSize)
            }
        }
        
        if (this.selectedFacility.id && this.selectedCenter.id == 'ALL') {
            var from = new Date(window.localStorage.getItem('patientStartDate')).getTime()
            var to = new Date(window.localStorage.getItem('patientEndDate')).getTime()
            if (this.searchString.length >= 3) {
                this.isSearched = true;
                this.searchFacilityCenterUser(from, to, this.selectedSearchBy.keytoSend, this.selectedFacility.id, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.pageSize, 'FACILITY')
            }
        }
        if (this.selectedFacility.id && this.selectedCenter.id != 'ALL' && this.selectedCenter.id != undefined) {
            var from = new Date(window.localStorage.getItem('patientStartDate')).getTime()
            var to = new Date(window.localStorage.getItem('patientEndDate')).getTime()
            if (this.searchString.length >= 3) {
                this.isSearched = true;
                this.searchFacilityCenterUser(from, to, this.selectedSearchBy.keytoSend, this.selectedCenter.id, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.pageSize, 'CENTER')
            }
        }


    }

    searchFacilityCenterUser(from, to, searchCriteria, id, sortby, searchstring, page, size, searchby) {
        Helpers.setLoading(true);
        if (searchby == 'FACILITY') {
            this._usersService.getFaciltyUserSearch(from, to, searchCriteria, id, page, size, searchstring, sortby, searchby)
                .then(response => {
                    Helpers.setLoading(false);
                    if (response != undefined || response != null) {
                        this.users = response.users;
                        this.userCount = response.count.toString();
                    } else {
                        this.users = [];
                        this.userCount = '0';
                    }

                    this.visiblepagination = true;
                    if (response != null) {
                        this.users = response.users;
                        this.userCount = response.count.toString();
                        this.getCustomerCenterRole();
                    } else {
                        this.users = [];
                        this.userCount = '0'
                    }
                })

        }
        if (searchby == 'CENTER') {
            this._usersService.getCenterUserSearch(from, to, searchCriteria, id, page, size, searchstring, sortby, searchby)
                .then(response => {
                    Helpers.setLoading(false);
                    this.visiblepagination = true;
                    if (response != null) {
                        this.users = response.users;
                        this.userCount = response.count.toString();
                        this.getCustomerCenterRole();
                    } else {
                        this.users = [];
                        this.userCount = '0'
                    }

                })

        }


    }

    filterUser(from, to, searchCriteria, sortby, searchstring, page, size) {
        Helpers.setLoading(true);
        this._usersService.getUsersFiltered(from, to, searchCriteria, '', page, size, searchstring, sortby)
            .then(
            response => {
                Helpers.setLoading(false);
                if (response != undefined || response != null) {
                    this.users = response.users;
                    this.userCount = response.count.toString();
                } else {
                    this.users = [];
                    this.userCount = '0';
                }
                this.visiblepagination = true;
                if (response != null) {
                    this.users = response.users;
                    this.userCount = response.count.toString();
                    this.getCustomerCenterRole();
                } else {
                    this.users = [];
                    this.userCount = '0'
                }
            })

    }

    onChangeSeachBy(event) {
        //console.log()
        this.pageOne('change');
        var from = new Date(window.localStorage.getItem('patientStartDate')).getTime()
        var to = new Date(window.localStorage.getItem('patientEndDate')).getTime()
        if (this.selectedFacility.id == 'ALL' && this.selectedCenter.id == undefined)
            this.filterUser(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), this.selectedSearchBy.keytoSend, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.pageSize)
        if (this.selectedFacility.id && this.selectedCenter.id == 'ALL') {
            this.searchFacilityCenterUser(from, to, this.selectedSearchBy.keytoSend, this.selectedFacility.id, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.pageSize, 'FACILITY')
        }
        if (this.selectedFacility.id && this.selectedCenter.id != 'ALL' && this.selectedCenter.id != undefined) {
            this.searchFacilityCenterUser(from, to, this.selectedSearchBy.keytoSend, this.selectedCenter.id, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.pageSize, 'CENTER')
        }
        if (this.selectedFacility.id==undefined) {
            this.filterUser(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), this.selectedSearchBy.keytoSend, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.pageSize)
        }
        
    }

    onChangeSortBy(event) {
        this.pageOne('change');
        var from = new Date(window.localStorage.getItem('patientStartDate')).getTime()
        var to = new Date(window.localStorage.getItem('patientEndDate')).getTime()
        if (this.selectedFacility.id == 'ALL' && this.selectedCenter.id == undefined)
            this.filterUser(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), this.selectedSearchBy.keytoSend, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.pageSize)
        if (this.selectedFacility.id && this.selectedCenter.id == 'ALL') {
            this.searchFacilityCenterUser(from, to, this.selectedSearchBy.keytoSend, this.selectedFacility.id, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.pageSize, 'FACILITY')
        }
        if (this.selectedFacility.id && this.selectedCenter.id != 'ALL' && this.selectedCenter.id != undefined) {
            this.searchFacilityCenterUser(from, to, this.selectedSearchBy.keytoSend, this.selectedCenter.id, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.pageSize, 'CENTER')
        }
    }

    facilityChange(value) {
        this.selectedFacility = value;
        this.selectedFacility.organizationCode = value.organizationCode;
        this.centers = value.centers;
        if (this.centers[0].id !== "ALL") {
            let allCenter = new Center();
            allCenter.name = "ALL";
            allCenter.id = "ALL";
            this.centers.splice(0, 0, allCenter);
        }
        this.selectedCenter.id = this.centers[0].id;
        this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
    }

    pageOne(type){    
        if(type == 'user')   {
            if(this.searchString.length >= 3){
                this.pageNo=1;
                this.visiblepagination = false;       
                setTimeout(() => this.visiblepagination = true, 0);
            }
        } else{
            this.pageNo=1;
            this.visiblepagination = false;       
            setTimeout(() => this.visiblepagination = true, 0);            
        }         
    }
}