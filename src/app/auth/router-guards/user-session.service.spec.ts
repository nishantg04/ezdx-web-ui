import { UserSessionService } from "./user-session.service";

describe('UserSessionService::', () => {
    let userSessionService: UserSessionService;

    describe('isUserLoggedIn: ', () => {
        beforeEach(() => {
            userSessionService = new UserSessionService();
        })


        it('Should return false if the User session is not available', () => {
            localStorage.clear();
            expect(userSessionService.isUserLoggedIn()).toBeFalsy();
        })

        it('Should return true if the user session is available', () => {
            let currentUser = {
                'email': 'vardhaman@widas.in',
                'fullName': 'Vardhaman',
                'token': 'abcd'
            };
            localStorage.setItem('currentUser', JSON.stringify(currentUser));
            expect(userSessionService.isUserLoggedIn()).toBeTruthy();
        })

    })

    describe('getUserRoles: ', () => {
        it('Should return empty array if the users roles are not defined', () => {
            localStorage.clear();
            let rolesFetched = userSessionService.getUserRoles();
            // console.log('Roles fetched: ', rolesFetched);
            expect(userSessionService.getUserRoles()).toBeUndefined();
        })

        it('Should return no-empty array if the users roles defined', () => {
            let user = {
                userRoles: ['HC_ADMIN', 'HC_MANAGER']
            }
            localStorage.setItem('userInfo', JSON.stringify(user));
            let gotRole = userSessionService.getUserRoles();
            expect(gotRole).toContain('HC_MANAGER');
        })
    })
})