import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { ConsumablesService } from '../consumables.service';
import { Facility } from "../../facilities/facility";
import { Center } from "../../facilities/center";
import { FacilitiesService } from "../../facilities/facilities.service";


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./consumables-reorder.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ConsumablesReorderComponent implements OnInit, AfterViewInit {
    currMonth: Date;
    msgsAllocate: { severity: string; summary: string; detail: string; }[];
    consumables: any[];
    enableAssignMode: boolean;
    enableShipMode: boolean;
    sortType: string;
    sort: boolean;
    reorders: any[];
    checkAll: boolean;
    sortTypeCons: string;
    sortCons: boolean;
    facilities: any[];
    selectedFacility: any;
    centers: any[];
    selectedCenter: any;
    isShipped: boolean;
    reorderRequest: any;
    selectedOrder: any;
    errMsg: string;
    loading: boolean;
    isValid: boolean;
    user: any;
    userRole: any;
    msgs: any[];
    userType: any;
    constructor(private _facilitiesService: FacilitiesService, private _ngZone: NgZone, private _script: ScriptLoaderService, private _consumablesService: ConsumablesService, private _router: Router, private _activeroute: ActivatedRoute) {
        this.consumables = [
            { id: 'asdd', name: "Cholesterol strips", lotno: "H17006057", manufacturer: "Online Surgicals", expirydt: "08/2018", sku: "SYNG3004", test: "Cholesterol", testdt: "20/12/2017", qntpack: 200, qntperpack: 10, totalunits: 2000, note: "", customer: "Healthcubed", center: "Bangalore", selected: false },
            { id: '123', name: "Hemoglobin strips", lotno: "H17006057", manufacturer: "Online Surgicals", expirydt: "08/2018", sku: "SYNG3004", test: "Hemoglobin", testdt: "20/12/2017", qntpack: 100, qntperpack: 10, totalunits: 1000, note: "", customer: "Healthcubed", center: "Bangalore", selected: false },
            { id: 'as44dd', name: "Urine strips", lotno: "H17006057", manufacturer: "Online Surgicals", expirydt: "08/2018", sku: "SYNG3004", test: "Urine Sugar", testdt: "20/12/2017", qntpack: 50, qntperpack: 10, totalunits: 500, note: "", customer: "Healthcubed", center: "Bangalore", selected: false }
        ];
        this.enableAssignMode = false;
        this.enableShipMode = false;
        this.sortType = "name";
        this.sort = true;
        this.sortTypeCons = "name";
        this.sortCons = false;
        this.checkAll = false;
        this.isShipped = false;
        this.loading = false;
        this.isValid = true;
        this.errMsg = "";
        this.msgs = [];
        // this.reorders = [
        //     { name: 'Pavan G', orgName: 'Healthcubed', center: 'Bangalore', quantity: 500, consumableName: 'Cholesterol strips', notes: 'Need it ASAP', datetime: "08/12/2017 10 AM" },
        //     { name: 'Hemant', orgName: 'Healthcubed', center: 'Ahmedabad', quantity: 100, consumableName: 'Urine strips', notes: 'Send invoice as well', datetime: "08/12/2017 4 PM" }
        // ];
        this.reorderRequest = {
            facilityId: "123",
            centerId: "123",
            orderedBy: "Ramu",
            createTime: "18/01/2018",
            orderItems: [
                {
                    id: "string",
                    testType: "PHYSICAL",
                    testName: "BLOOD_PRESSURE",
                    quantity: 200,
                    orderItemStatus: "ORDER_PLACED",
                    fulfilledQuantity: 0
                }
            ]
        }
    }
    ngOnInit() {

        this.currMonth = new Date();
        this.currMonth.setDate(1);

        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
        window.my.namespace.consumableUpdatePublicFunc = this.consumableUpdatePublicFunc.bind(this);
        this.user = JSON.parse(window.localStorage.getItem('userInfo'))
        this.userType = window.localStorage.getItem('userType');
    }
    ngOnDestroy() {
        window.my.namespace.publicFunc = null;
    }
    ngAfterViewInit() {
        this.list();
        this.getFacilities();
        if (this.user) {
            this.roleChecking();
        }
        // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
        //     'assets/demo/default/custom/components/forms/validation/form-controls.js');
    }

    roleChecking() {
        var isadmin = true;
        var isfacilityadmin = false;
        this.user.userRoles.forEach(role => {
            if (role === "OPERATOR" || role === "DOCTOR" || role === "TECHNICIAN")
                isadmin = false;
            if (role === "TECHNICIAN")
                isfacilityadmin = true;
            if (role === "HC_OPERATIONS" || role === "HC_MANAGER")
                this.userRole = "HC_OPS";
            if (role === "HC_ADMIN")
                this.userRole = "HC_ADMIN";
            if (role === "OPERATOR")
                this.userRole = "OPERATOR";
            if (role === "DOCTOR")
                this.userRole = "DOCTOR";
            if (role === "TECHNICIAN")
                this.userRole = "TECHNICIAN";
        });
    }

    humanize(str) {
        var frags = str.split('_');
        for (var i = 0; i < frags.length; i++) {
            frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
        }
        return frags.join(' ');
    }
    oncheckAll(): void {
        this.checkAll = !this.checkAll;
        this.consumables.forEach((cons, i) => {
            cons.selected = this.checkAll;
        });
    }

    consumableUpdatePublicFunc(id) {
        this._ngZone.run(() => this.goToConsumableEdit(id));
    }

    goToConsumableEdit(id) {
        //this._router.navigate(['/consumables/update'], { queryParams: { consumableId: id } });
        this.enableAssignMode = true;
    }

    toggleShipMode(selectedReroder) {
        this.reorderRequest = selectedReroder;
        this.enableAssignMode = !this.enableAssignMode;
        this.enableShipMode = !this.enableShipMode;
    }

    toggleAssignMode() {
        this.isShipped = false;
        this.enableShipMode = false;
        this.enableAssignMode = !this.enableAssignMode;
        // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
        //     'assets/demo/default/custom/components/datatables/base/data-local.consumables.js');
    }

    toggleShipped() {
        this.isShipped = !this.isShipped;
    }

    list(): void {
        localStorage.setItem('consumablesTable', JSON.stringify(this.consumables));
        // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
        //     'assets/demo/default/custom/components/datatables/base/data-local.consumables.js');
    }

    getFacilities(): void {

        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            } else if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        Helpers.setLoading(false);
        this.facilities.sort(function(a, b) {
            return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
        });
        this.loadRoleBasedOrder();
        if (window.localStorage.getItem('userType') == 'Facility') {
            this.facilities.forEach(fclt => {
                if (fclt.id == this.user.facilityId) {
                    this.selectedFacility = jQuery.extend(true, {}, fclt);
                }
            });
        }
        else {

            this.selectedFacility = jQuery.extend(true, {}, this.facilities[0]);
        }
        this.centers = this.selectedFacility.centers;
        if (this.centers) {
            this.centers.sort(function(a, b) {
                return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
            });
            this.selectedCenter = jQuery.extend(true, {}, this.centers[0]);
        }
    }

    loadRoleBasedOrder(): void {
        if (window.localStorage.getItem('userType') == 'Facility') {
            this.listOrder(1, 1000, "FACILITY", this.user.facilityId, "updateTime", "desc");
        }
        else {
            this.listOrder(1, 1000, "ALL", "", "updateTime", "desc");
        }
    }

    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers) {
                    this.centers.sort(function(a, b) {
                        return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
                    });
                    this.selectedCenter = jQuery.extend(true, {}, this.centers[0]);
                }
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }

    onCenterChange(centerId: string): void {
    }

    onQuantityChange(): void {
        // console.log(this.consumables);
    }

    listConsumable(pageno, pagesize, search, searchterm, orderby, sortby): void {
        Helpers.setLoading(true);
        this.consumables = [];
        this._consumablesService.list(pageno, pagesize, search, searchterm, orderby, sortby)  // for get All facility we should pass page and size as zero
            .then(consumables => {
                Helpers.setLoading(false);
                this.consumables = consumables.masterInventory;
            })
            .then(() => {
                Helpers.setLoading(false);
                this.consumables.forEach(consumable => {
                    consumable.fulfilledQuantity = 0;
                    if (this.reorderRequest.orderShipments.length > 0) {
                        this.reorderRequest.orderShipments.forEach(ordr => {
                            if (ordr.lotNumber === consumable.lotNumber) {
                                consumable.allocatedQuantity = ordr.quantity;
                            }
                            else {
                                consumable.allocatedQuantity = 0;
                            }
                        });
                    }
                });
            })
    }

    openOrderModal(selectedOrder) {
        this.listConsumable(1, 1000, "TEST_NAME", selectedOrder.testName, "testName", "asc");
        this.selectedOrder = selectedOrder;
    }


    listOrder(pageno, pagesize, search, searchterm, orderby, sortby): void {
        Helpers.setLoading(true);
        this._consumablesService.listOrder(pageno, pagesize, search, searchterm, orderby, sortby)  // for get All facility we should pass page and size as zero
            .then(reorders => {
                Helpers.setLoading(false);
                this.reorders = reorders.orders;
            })
            .then(() => {
                this.reorders.forEach(req => {
                    var d = new Date(req.createTime),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                    req.createTimeString = [day, month, year].join('-') + ' (' + d.getHours() + ':' + d.getMinutes() + ')';
                    this.facilities.forEach(fct => {
                        if (fct.id === req.facilityId) {
                            req.facilityName = fct.name;
                            fct.centers.forEach(cnt => {
                                if (cnt.id === req.centerId)
                                    req.centerName = cnt.name;
                            });
                        }
                    });

                    req.orderItems.forEach(ord => {
                        ord.testNameString = this.humanize(ord.testName);
                    });
                });
            })
    }

    fulfillOrder(): void {
        this.reorderRequest.orderShipments = [];
        this.isValid = true;
        var consumableFulfilledValid = false;
        // console.log("consumables: ", this.consumables, " order request: ", this.reorderRequest);
        this.consumables.forEach((consumable) => {
            if (consumable.fulfilledQuantity > 0) {
                consumableFulfilledValid = true;
            }
        });
        if (!consumableFulfilledValid) {
            this.msgsAllocate = [{ severity: 'error', summary: 'Invalid allocation', detail: 'Please add valid packs!' }];
            setTimeout(() => {
                this.msgsAllocate = [];
            }, 5 * 1000);
            return;
        }

        this.consumables.forEach(consm => {
            if (consm.fulfilledQuantity > 0) {
                if (consm.fulfilledQuantity > consm.quantity) {
                    this.isValid = false;
                }
                if (this.isValid) {
                    var lotAlreadyExist = false;
                    // console.log("order: ", this.reorderRequest);
                    this.reorderRequest.orderShipments.forEach(ordr => {
                        if (ordr.lotNumber === consm.lotNumber) {
                            lotAlreadyExist = true;
                            ordr.quantity += consm.fulfilledQuantity;
                            ordr.items = ordr.quantity * ordr.itemsPerPackage;
                        }
                    });
                    if (!lotAlreadyExist) {
                        this.reorderRequest.orderShipments.push(
                            {
                                lotNumber: consm.lotNumber,
                                lotCode: consm.lotCode,
                                facilityId: this.reorderRequest.facilityId,
                                centerId: this.reorderRequest.centerId,
                                packaging: consm.packaging,
                                quantity: consm.fulfilledQuantity,
                                itemsPerPackage: consm.itemsPerPackage,
                                items: consm.fulfilledQuantity * consm.itemsPerPackage,
                                testName: consm.testName,
                                testType: consm.testType,
                                consumableType: consm.consumableType
                            }
                        );
                    }
                    this.reorderRequest.orderItems.forEach(order => {
                        if (order.testName === consm.testName) {
                            order.fulfilledQuantity += consm.fulfilledQuantity;
                        }
                    });
                }
            }

        });
        this.reorderRequest.orderItems.forEach(order => {
            if (order.fulfilledQuantity > order.quantity) {
                this.isValid = false;
                order.fulfilledQuantity = 0;
            }
        });
        if (this.isValid) {
            this.isValid = true;
            var fulfilledqnt = 0, requiredqnt = 0;
            this.reorderRequest.orderItems.forEach(order => {
                fulfilledqnt += order.fulfilledQuantity;
                requiredqnt += order.quantity;
            });
            if (fulfilledqnt > 0) {
                if (fulfilledqnt === requiredqnt)
                    this.reorderRequest.orderStatus = "FULFILLED";
                else
                    this.reorderRequest.orderStatus = "PARTIALLY_ALLOCATED";
            }
            this.allocateOrder(false);
            jQuery('#close_order_modal').click();
        } else {
            // console.log("inside else...")
            this.msgsAllocate = [{ severity: 'error', summary: 'Invalid allocation', detail: 'Please add valid packs!' }];
            setTimeout(() => {
                this.msgsAllocate = [];
            }, 5 * 1000);
            return;
        }
    }

    getTotalToBeAllocated(): any {
        var totalToBeAllocated = 0;
        this.consumables.forEach(consm => {
            totalToBeAllocated += consm.fulfilledQuantity;
        });
        return totalToBeAllocated;
    }

    updateReorder(isShip): void {
        if (!this.loading) {
            Helpers.setLoading(true);
            // var time = this.reorderRequest.createTimeString;
            // var facility = this.reorderRequest.facilityName;
            // var center = this.reorderRequest.centerName;
            var requestToSend = jQuery.extend(true, {}, this.reorderRequest);
            requestToSend.createTimeString = undefined;
            requestToSend.facilityName = undefined;
            requestToSend.centerName = undefined;
            requestToSend.orderItems.forEach(order => {
                order.testNameString = undefined;
            });
            if (isShip)
                requestToSend.orderStatus = "DISPATCHED";
            this._consumablesService.updateReorder(requestToSend)
                .then((data) => console.log(data))
                .then(() => {
                    // this.reorderRequest.createTimeString = time;
                    // this.reorderRequest.facilityName = facility;
                    // this.reorderRequest.centerName = center;
                    Helpers.setLoading(false);
                    this.loading = false;
                    if (isShip) {
                        this.toggleShipMode(null);
                        this.msgs = [{ severity: 'success', summary: 'Success', detail: 'Reorder request successfully marked as Shipped !' }];
                        setTimeout(function() {
                            this.msgs.splice(0, 1);
                        }, 5000);
                    }
                    else {
                        this.msgs = [{ severity: 'success', summary: 'Success', detail: 'Consumables successfully allocated !' }];
                        setTimeout(() => {
                            this.msgs.splice(0, 1);
                        }, 5000);
                    }
                    this.loadRoleBasedOrder();

                }, (e: any) => {
                    // console.log(e);
                    let res = JSON.parse(e._body);
                    if (res.error) {
                        this.errMsg = res.error;
                    }
                    Helpers.setLoading(false);
                    this.loading = false
                });
        }
    }

    allocateOrder(isShip): void {
        if (!this.loading) {
            Helpers.setLoading(true);
            // var time = this.reorderRequest.createTimeString;
            // var facility = this.reorderRequest.facilityName;
            // var center = this.reorderRequest.centerName;
            var requestToSend = jQuery.extend(true, {}, this.reorderRequest);
            requestToSend.createTimeString = undefined;
            requestToSend.facilityName = undefined;
            requestToSend.centerName = undefined;
            requestToSend.orderNumber = undefined;
            requestToSend.orderItems.forEach(order => {
                order.testNameString = undefined;
            });
            if (isShip)
                requestToSend.orderStatus = "DISPATCHED";
            this._consumablesService.allocateOrder(requestToSend)
                .then((data) => console.log(data))
                .then(() => {
                    // this.reorderRequest.createTimeString = time;
                    // this.reorderRequest.facilityName = facility;
                    // this.reorderRequest.centerName = center;
                    Helpers.setLoading(false);
                    this.loading = false;
                    if (isShip) {
                        this.toggleShipMode(null);
                        this.msgs = [{ severity: 'success', summary: 'Success', detail: 'Reorder request successfully marked as Shipped !' }];
                        setTimeout(function() {
                            this.msgs.splice(0, 1);
                        }, 5000);
                    }
                    else {
                        this.msgs = [{ severity: 'success', summary: 'Success', detail: 'Consumables successfully allocated !' }];
                        setTimeout(() => {
                            this.msgs.splice(0, 1);
                        }, 5000);
                    }
                    this.loadRoleBasedOrder();

                }, (e: any) => {
                    // console.log(e);
                    let res = JSON.parse(e._body);
                    if (res.error) {
                        this.errMsg = res.error;
                    }
                    Helpers.setLoading(false);
                    this.loading = false
                });
        }
    }

    acceptOrder(): void {
        this._consumablesService.acceptOrder(this.reorderRequest)
            .then((data) => console.log(data))
            .then(() => {
                // this.reorderRequest.createTimeString = time;
                // this.reorderRequest.facilityName = facility;
                // this.reorderRequest.centerName = center;
                Helpers.setLoading(false);
                this.loading = false;
                //this.toggleShipMode(null);
                this.msgs = [{ severity: 'success', summary: 'Success', detail: 'Order successfully marked as accepted !' }];
                this._router.navigate(['/consumables/center']);
                setTimeout(() => {
                    this.msgs.splice(0, 1);
                }, 5000);
                this.loadRoleBasedOrder();

            }, (e: any) => {
                // console.log(e);
                let res = JSON.parse(e._body);
                if (res.error) {
                    this.errMsg = res.error;
                }
                Helpers.setLoading(false);
                this.loading = false
            });
    }

    rejectOrder(): void {
        this._consumablesService.rejectOrder(this.reorderRequest)
            .then((data) => console.log(data))
            .then(() => {
                // this.reorderRequest.createTimeString = time;
                // this.reorderRequest.facilityName = facility;
                // this.reorderRequest.centerName = center;
                Helpers.setLoading(false);
                this.loading = false;
                this.msgs = [{ severity: 'success', summary: 'Success', detail: 'Order successfully marked as rejected !' }];
                setTimeout(() => {
                    this.msgs.splice(0, 1);
                }, 5000);
                this.loadRoleBasedOrder();

            }, (e: any) => {
                // console.log(e);
                let res = JSON.parse(e._body);
                if (res.error) {
                    this.errMsg = res.error;
                }
                Helpers.setLoading(false);
                this.loading = false
            });
    }

}