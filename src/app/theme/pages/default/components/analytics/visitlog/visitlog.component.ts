import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { AnalyticsService } from '../analytics.service';
import { ScriptLoaderService } from './../../../../../../_services/script-loader.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./visitlog.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class VisitlogComponent implements OnInit, AfterViewInit {

    revisitLog: any=[];
    user: any;
    visitcount: any[];
    patientcount: any[];
    public dummyids:any=[];
    userType : any;
    constructor(private _activeroute: ActivatedRoute, private analytics: AnalyticsService, private _script: ScriptLoaderService) { 
        this.user=JSON.parse(localStorage.getItem('userInfo'));
        if (window.localStorage.getItem('userType') == 'Facility')
        this.userType = 'Facility';
        else if (window.localStorage.getItem('userType') == 'Center')
            this.userType = 'Center';
        else if (window.localStorage.getItem('userType') == 'User')
            this.userType = 'User';
        else
            this.userType = 'Admin';
    }

    public visitorlog: any = [];
    public visitorlogresp: any = {};
    public analyticsUsersresp: any = {};
    public visitorlogCount: number = 0;
    public from: any;
    public to: any;
    public row: number = 10;
    public selectedItem: string;
    public analyticsUser: any=[];
    ngOnInit() {
        this._activeroute.queryParams.subscribe(param => {
            this.selectedItem = param.selectedItem;
            this.from = param.fromdate;
            this.to = param.todate;
        });
        Helpers.setLoading(true);
    }


    ngAfterViewInit(): void {
        window.localStorage.removeItem('analyticsStartDate');
        window.localStorage.removeItem('analyticsEndDate');
        var date = new Date();
        if (this.from && this.to) {
            window.localStorage.setItem('analyticsStartDate', this.from);
            window.localStorage.setItem('analyticsEndDate', this.to);
            if(this.userType == 'Facility'){
                if (this.user.userRoles.includes('TECHNICIAN')) {
                    this.useranalyticsCustomerAdmin('facility',this.user.facilityId,new Date(this.from).getTime(), new Date(this.to).getTime())
                }
            }else if(this.userType == 'Center'){
                if ((this.user.userRoles).includes('CENTER_ADMIN')) {
                    this.useranalyticsCustomerAdmin('center',this.user.centerId,new Date(this.from).getTime(), new Date(this.to).getTime())
                }
            }
            else{
                this.useranalytics(new Date(this.from).getTime(), new Date(this.to).getTime());
            }           

        } else {
            this.to = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            this.from = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 29, 0);
            if(this.userType == 'Facility'){
                if (this.user.userRoles.includes('TECHNICIAN')) {
                    this.useranalyticsCustomerAdmin('facility',this.user.facilityId,new Date(this.from).getTime(), new Date(this.to).getTime())
                }
            } else if(this.userType == 'Center'){
                if ((this.user.userRoles).includes('CENTER_ADMIN')) {
                    this.useranalyticsCustomerAdmin('center',this.user.centerId,new Date(this.from).getTime(), new Date(this.to).getTime())
                }
            }    
              else{
                this.useranalytics(new Date(this.from).getTime(), new Date(this.to).getTime());
              }
        }
        //console.log(new Date(this.from), new Date(this.to))
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/datatables/base/date.analytics.visits.js');
    }

    //get analytics first

    /**
     * useranalytics
     */
    public useranalytics(from,to) {
        this.analytics.getAlluserAnalytics().then((res) => {
            Helpers.setLoading(false);
            if (res!=undefined) {
                this.analyticsUsersresp=res;   
                this.analyticsUser=this.analyticsUsersresp.results;
            }else{
                return false;
            }
            
        })
        .then(()=>{
            this.visitsAndPatientCount(from,to)
        })
    }

    /**
     * useranalytics customer admin
     */
    public useranalyticsCustomerAdmin(type,facility,from,to) {
        this.analytics.getuserAnalyticsSearch(type,facility).then((res) => {
            Helpers.setLoading(false);
           // console.log(res)
            if (res!=undefined) {
                this.analyticsUsersresp=res;   
                this.analyticsUser=this.analyticsUsersresp.results;
            }else{
                return false;
            }
            
        })
        .then(()=>{
            this.visitsAndPatientCount(from,to)
        })
    }

    /**
     * visitsAndPatientCount
     */
    public visitsAndPatientCount(from , to) {
        this.analytics.getAlluserPatientCount(from,to).then((res) => {
            Helpers.setLoading(false);
          //  console.log(res);
            if (res!=undefined) {
                this.patientcount=res;
            }
            
        }).then(()=>{
            this.analytics.getAlluserRevisits(from,to).then((res) => {
                Helpers.setLoading(false);
             //   console.log(res);
                if (res!=undefined) {
                    this.visitorlog = [];
                    this.revisitLog=[];
                    this.visitcount=res;
                if (this.patientcount.length>this.visitcount.length) {
                    this.patientcount.map((patient)=>{
                        this.visitcount.map((visit,i)=>{
                            if (patient._id==visit._id) {
                                this.visitorlog.push({id:visit._id,patientCount:patient.patientCount,visitCount:visit.revisitCount})
                            }
                        })
                    })
                    for (let i = 0; i < this.visitcount.length; i++) {
                        if(!JSON.stringify(this.visitorlog).includes(this.visitcount[i]._id)){
                            this.visitorlog.push({id:this.visitcount[i]._id,patientCount:0,visitCount:this.visitcount[i].revisitCount})
                           
                        }                       
                    }
                    
                }
                
                else if (this.visitcount.length>this.patientcount.length) {
                    this.visitcount.map((patient)=>{
                        this.patientcount.map((visit)=>{
                            if (patient._id==visit._id) {
                                this.visitorlog.push({id:visit._id,patientCount:patient.patientCount,visitCount:visit.revisitCount})
                            }
                        })
                       
                    })
                    for (let i = 0; i < this.patientcount.length; i++) {
                        if(!JSON.stringify(this.visitorlog).includes(this.patientcount[i]._id)){
                            this.visitorlog.push({id:this.patientcount[i]._id,patientCount:this.patientcount[i].patientCount,visitCount:0})
                           
                        }                       
                    }
                }
                this.analyticsUser.map((analytic)=>{
                    this.visitorlog.map((countlog)=>{
                        if (countlog.id==analytic.id) {
                            countlog.displayName=analytic.displayName;
                        }
                    })
                })
                this.visitorlog.sort((a, b) => { return b.patientCount - a.patientCount })

                this.visitorlog.map((visits)=>{
                    if (visits.displayName!=undefined) {
                        this.revisitLog.push(visits)
                    }                    
                })

                console.log(this.revisitLog)
            }
            })
        })
        
    }

    //get visitor log list
    // public getallvisitorLog(from, to): void {

    //     Helpers.setLoading(true);
    //     window.scroll({ top: 0, left: 0, behavior: 'smooth' });
    //     this.analytics.getvisitLogs(from, to).then((res) => {
    //         Helpers.setLoading(false);

    //         if (res != undefined) {
    //             this.visitorlogresp = res;
    //             this.visitorlog = this.visitorlogresp.userStatsView;
    //             this.visitorlogCount = this.visitorlogresp.count;
    //             this.visitorlog.sort((a, b) => { return b.patientCount - a.patientCount })
    //         } else {
    //             this.visitorlog = [];
    //             this.visitorlogCount = 0;
    //         }
    //     })
    // }

    /**
     * when clicked on apply filter button
     */
    public applyFilter() {
        this.from = new Date(localStorage.getItem('analyticsStartDate')).getTime();
        this.to = new Date(localStorage.getItem('analyticsEndDate')).getTime();
        this.visitsAndPatientCount(this.from, this.to)
    }




}
