import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { Helpers } from '../../../../../../helpers';

import { environment } from '../../../../../../../environments/environment'
@Injectable()
export class TestsReportService {

    private headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private serviceUrl = environment.BaseURL + environment.VisitTestURL; //'http://dev.ezdx.healthcubed.com/ezdx-visit-query/api/v1/tests';

    constructor(private http: Http, private _router: Router, private _script: ScriptLoaderService) { }

    getTestBasedOnType(searchcriteria, type) {
        if (type !== 'ALL') {
            if (type === 'BLOOD_GLUCOSE_FASTING' || type === "BLOOD_GLUCOSE_NON_FASTING") {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&testName=' + 'BLOOD_GLUCOSE' + '&chartType=' + type;
            }
            else if (type === 'HEAMOGLOBIN') {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&testName=' + 'HEMOGLOBIN' + '&chartType=' + 'HEMOGLOBIN';
            } else if (type === 'MALARIA' || type === 'HEP_B' || type === 'TYPHOID') {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&testName=' + type + '&chartType=' + 'RDT';
            }
            else
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&testName=' + type + '&chartType=' + type;
            return this.http.get(getUrl, { headers: this.headers })
                .toPromise()
                .then(response => { return response.json() })
                .catch(this.handleError.bind(this));
        }
    }

    getTestBasedOnFaclity(searchcriteria, facilityId, type) {
        if (type !== 'ALL') {
            if (type === 'BLOOD_GLUCOSE_FASTING' || type === "BLOOD_GLUCOSE_NON_FASTING") {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&facilityid=' + facilityId + '&testName=' + 'BLOOD_GLUCOSE' + '&chartType=' + type;
            }
            else if (type === 'HEAMOGLOBIN') {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&facilityid=' + facilityId + '&testName=' + 'HEMOGLOBIN' + '&chartType=' + 'HEMOGLOBIN';
            }
            else if (type === 'MALARIA' || type === 'HEP_B' || type === 'TYPHOID') {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&facilityid=' + facilityId + '&testName=' + type + '&chartType=' + 'RDT';
            }
            else
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&facilityid=' + facilityId + '&testName=' + type + '&chartType=' + type;
            return this.http.get(getUrl, { headers: this.headers })
                .toPromise()
                .then(response => { return response.json() })
                .catch(this.handleError.bind(this));
        }
    }

    getTestBasedOnCenter(searchcriteria, facilityId, centerId, type) {
        if (type !== 'ALL') {
            if (type === 'BLOOD_GLUCOSE_FASTING' || type === "BLOOD_GLUCOSE_NON_FASTING") {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&centerid=' + centerId + '&facilityid=' + facilityId + '&testName=' + 'BLOOD_GLUCOSE' + '&chartType=' + type;
            } else if (type === 'HEAMOGLOBIN') {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&centerid=' + centerId + '&facilityid=' + facilityId + '&testName=' + 'HEMOGLOBIN' + '&chartType=' + 'HEMOGLOBIN';
            }
            else if (type === 'MALARIA' || type === 'HEP_B' || type === 'TYPHOID') {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&centerid=' + centerId + '&facilityid=' + facilityId + '&testName=' + type + '&chartType=' + 'RDT';
            }
            else
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&centerid=' + centerId + '&facilityid=' + facilityId + '&testName=' + type + '&chartType=' + type;
            return this.http.get(getUrl, { headers: this.headers })
                .toPromise()
                .then(response => { return response.json() })
                .catch(this.handleError.bind(this));
        }
    }

    getTestBasedOnUser(searchcriteria,id, type) {
        if (type !== 'ALL') {
            if (type === 'BLOOD_GLUCOSE_FASTING' || type === "BLOOD_GLUCOSE_NON_FASTING") {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&userid=' + id + '&testName=' + 'BLOOD_GLUCOSE' + '&chartType=' + type;
            } else if (type === 'HEAMOGLOBIN') {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&userid=' + id + '&testName=' + 'HEMOGLOBIN' + '&chartType=' + 'HEMOGLOBIN';
            }
            else if (type === 'MALARIA' || type === 'HEP_B' || type === 'TYPHOID') {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&userid=' + id + '&testName=' + type + '&chartType=' + 'RDT';
            }
            else
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&userid=' + id + '&testName=' + type + '&chartType=' + type;
            return this.http.get(getUrl, { headers: this.headers })
                .toPromise()
                .then(response => { return response.json() })
                .catch(this.handleError.bind(this));
        }
    }

    getTestBasedOnCenterForReport(searchcriteria, facilityId, centerId, type, from, to) {
        if (type !== 'ALL') {
            if (type === 'BLOOD_GLUCOSE_FASTING' || type === "BLOOD_GLUCOSE_NON_FASTING") {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&centerid=' + centerId + '&facilityid=' + facilityId + '&testName=' + 'BLOOD_GLUCOSE' + '&chartType=' + type + '&from=' + from + '&to=' + to;
            } else if (type === 'HEAMOGLOBIN') {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&centerid=' + centerId + '&facilityid=' + facilityId + '&testName=' + 'HEMOGLOBIN' + '&chartType=' + 'HEMOGLOBIN' + '&from=' + from + '&to=' + to;
            }
            else if (type === 'MALARIA' || type === 'HEP_B' || type === 'TYPHOID') {
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&centerid=' + centerId + '&facilityid=' + facilityId + '&testName=' + type + '&chartType=' + 'RDT' + '&from=' + from + '&to=' + to;
            }
            else
                var getUrl = this.serviceUrl + '/count/type/classification?' + 'searchcriteria=' + searchcriteria + '&centerid=' + centerId + '&facilityid=' + facilityId + '&testName=' + type + '&chartType=' + type + '&from=' + from + '&to=' + to;
            return this.http.get(getUrl, { headers: this.headers })
                .toPromise()
                .then(response => { return response.json() })
                .catch(this.handleError.bind(this));
        }
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            alert('Session has expired or invalid we will redirect you to login page');
            this._script.load('body',
                'assets/demo/default/custom/components/utils/redirect-cidaas-logout.js');
        }
        return Promise.reject(error.message || error);
    }

}
