import { UrineEntity } from "./urine.entity";

export class UrineLookupJson {
    fileName: string;
    facilityId: string[];
    centerId: string[];
    deviceId: string[];
    urineLookups: UrineEntity[];
    reason: string;
}