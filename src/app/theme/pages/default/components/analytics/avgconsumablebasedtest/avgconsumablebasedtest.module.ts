import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { AvgconsumablebasedtestComponent } from './avgconsumablebasedtest.component';
import { InputTextModule, InputTextareaModule, DataTableModule, SharedModule, PaginatorModule, GrowlModule } from 'primeng/primeng';
import { AnalyticsService } from '../analytics.service';
import { AmchartModule } from '../../../../../amchartcomponent/amchart.module';

import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": AvgconsumablebasedtestComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, InputTextModule, InputTextareaModule, DataTableModule, SharedModule, PaginatorModule, GrowlModule,
        TranslateModule.forChild({ isolate: false }), AmchartModule
    ], exports: [
        RouterModule
    ], declarations: [
        AvgconsumablebasedtestComponent
    ],
    providers: [
        TranslateService,
        AnalyticsService
    ]

})
export class AvgconsumablebasedtestModule { }
