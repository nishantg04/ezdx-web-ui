export interface Test {
    centerId: String;
    consumableInventoryId: String;
    consumableQuantity: number;
    currency: String;
    endTime: Number;
    geopoint: { lat: Number; lon: Number };
    hcDeviceId: String;
    id: String;
    imagePath: String;
    imageUrl: String;
    lotNumber: String | Number;
    name: String;
    operatorId: String;
    organizationId: String;
    panelId: String;
    patientId: String;
    result: Number | String;
    startTime: Number;
    tabletId: String;
    testCost: Number;
    testDuration: Number;
    testExpenseConsumable: Number;
    type: String;
    unit: String;
    userInput: any;
    visitId: String;
}
