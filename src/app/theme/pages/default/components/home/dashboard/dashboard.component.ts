import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { UsersService } from '../../users/users.service';
import { PatientsService } from '../../patients/patients.service';
import { UserChild } from '../../users/userChild';
import { Facility } from '../../facilities/facility';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DevicesService } from '../../devices/devices.service';
import { VisitsService } from '../../visits/visits.service';
import { Center } from '../../facilities/center';
import { Visit } from '../../visits/visit';
import { Test } from '../../visits/test';
import * as _ from 'lodash';
import { element } from 'protractor';
import { Response } from '@angular/http/src/static_response';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./dashboard.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class DashboardComponent implements OnInit, AfterViewInit {
    user: UserChild;
    patientCount: number;
    deviceCount: any;
    visitCount: number;
    testCount: number;
    visitCountTotal: number;
    testCountTotal: any;
    totalVists: any[]
    facilities: Facility[];
    facilityCount: number;
    selectedFacility: Facility;
    selectedCenter: Center;
    selectedFacility2: Facility;
    selectedCenter2: Center;
    centers: Center[];
    centers2: Center[];
    years: any[];
    selectedYear: any;
    testList: any;
    selectedTestType: any;
    selectedTestList: any;
    selectedTestType2: any;
    selectedTestList2: any;
    patientCountByMonth: any[];
    selectedYearPatientCountByMonth: any[];
    testCountByMonth: any[];
    selectedYearTestCountByMonth: any[];
    selectedYearPerTestCountByMonth: any[];
    devicesTotalCount: any;
    patientsTotalCount: any;
    visits: Visit[];
    tests: Test[];
    testsForMonth: any[];
    testsMonth: any[];
    userType: string;
    userInfo: any;
    patientCountByWeek: any[];
    usersTotal: any[];
    testCountByWeek: any[];
    patientCountJson: any;
    hcOperations: boolean = false;
    hidePatient: boolean = true;
    hideDignostic: boolean = false;
    graphValues: any[] = [];
    patientChart: any[] = [];
    testChartValue: any[] = [];
    showDigLoad: boolean = false;
    showPatLoad: boolean = false;
    hover: boolean = true;
    testMapCount: any;
    constructor(private _ngZone: NgZone, private _script: ScriptLoaderService, private _router: Router, private _usersService: UsersService, private _patientsService: PatientsService, private _facilitiesService: FacilitiesService, private _devicesService: DevicesService, private _visitsService: VisitsService) {
        this.userType = null;
        this.selectedFacility = new Facility();
        this.selectedFacility2 = new Facility();
        this.selectedCenter = new Center();
        this.selectedCenter2 = new Center();
        let currentYear = new Date().getFullYear();
        this.tests = [];
        this.years = [];
        this.years.push("ALL");
        for (var index = 2014; index <= currentYear; index++) {
            this.years.push(index);
        }
        this.selectedYear = this.years[this.years.length - 1];

        this.testList = [
            { type: "physical", name: "Physical", color: '#71bf44', tests: [{ name: "Blood Pressure", type: "BLOOD_PRESSURE" }, { name: "Pulse Oximeter", type: "PULSE_OXIMETER" }, { name: "%O2 Saturation", type: "%O2 Saturation" }, { name: "Temperature", type: "TEMPERATURE" }, { name: "Height", type: "Height" }, { name: "Weight", type: "Weight" }, { name: "BMI", type: "BMI" }, { name: "ECG", type: "ECG" }, { name: "Mid Arm Circumference", type: "Mid Arm Circumference" }] },
            { type: "whole_blood_poct", color: '#21C4DF', name: "Whole Blood-POCT", tests: [{ name: "Glucose", type: "BLOOD_GLUCOSE" }, { name: "Hemoglobin", type: "HEAMOGLOBIN" }, { name: "Cholesterol", type: "CHOLESTEROL" }, { name: "Uric Acid", type: "URIC_ACID" }] },
            { type: "whole_blood", name: "Whole Blood", color: '#02abcb', tests: [{ name: "Blood Grouping", type: "BLOOD_GROUPING" }] },
            { type: "whole_blood_rdt", name: "Whole Blood-RDT", color: '#465EEB', tests: [{ name: "Malaria", type: "Malaria" }, { name: "Dengue", type: "Dengue" }, { name: "Typhoid", type: "Typhoid" }, { name: "Hep-B", type: "Hep-B" }, { name: "Hep-C", type: "Hep-C" }, { name: "HIV", type: "HIV" }, { name: "Chikungunya", type: "Chikungunya" }, { name: "Syphilis", type: "Syphilis" }, { name: "Troponin-I", type: "Troponin-I" }] },
            { type: "urine_poct", name: "Urine-POCT", color: '#DA70D6', tests: [{ name: "Pregnancy", type: "Pregnancy" }] },
            { type: "urine_rdt", name: "Urine-RDT", color: '#B22222', tests: [{ name: "Urine Sugar", type: "Urine Sugar" }, { name: "Urine Protein", type: "Urine Protein" }, { name: "Leukocytes", type: "Leukocytes" }, { name: "Urobilinogen", type: "Urobilinogen" }, { name: "Nitrite", type: "Nitrite" }, { name: "pH", type: "pH" }, { name: "Ketone", type: "Ketone" }, { name: "Blood", type: "Blood" }, { name: "Bilurubin", type: "Bilurubin" }, { name: "Specific Gravity", type: "Specific Gravity" }] }
        ];
        this.selectedTestType = this.testList[0].type;
        this.selectedTestList = this.testList[0].tests;
        this.selectedTestType2 = this.testList[0].type;
        this.selectedTestList2 = this.testList[0].tests;
    }
    ngOnInit() {
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
        window.my.namespace.publicFunc = this.publicFunc.bind(this);
        window.my.namespace.publicFuncPatientWeek = this.publicFuncPatientWeek.bind(this);
        window.my.namespace.publicFuncTestWeek = this.publicFuncTestWeek.bind(this);
    }

    ngAfterViewInit() {
        Helpers.setLoading(true);
        if (window.localStorage.getItem('userChanged') === "true") {

            this._usersService.getCurrentUser()
                .then(user => this.user = user)
                .then(() => {
                    window.localStorage.setItem('userInfo', JSON.stringify(this.user));
                    window.localStorage.setItem('userChanged', "false");
                    this.setUserDetails();
                });
        }
        else {
            this.user = JSON.parse(window.localStorage.getItem('userInfo'));
            this.setUserDetails();
        }
    }

    setUserDetails(): void {
        if (this.user && this.user.userRoles) {
            let isAdmin = false;
            let isCenter = false;
            this.user.userRoles = this.user.userRoles.sort();
            this.user.userRoles.forEach((role, i) => {
                if (role === "HC_ADMIN") {
                    isAdmin = true;
                }
                if (role === "HC_MANAGER") {
                    isAdmin = true;
                }
                if (role === "HC_OPERATIONS") {
                    isAdmin = true;
                    this.hcOperations = true;
                }
            });

            if (!isAdmin) {
                let isFirst = true;
                let customer_admin = false;
                let center_admin = false;
                let operator_user = false;
                // this.userInfo.userRoles.includes('CENTER_ADMIN')
                // if (this.user.userRoles[0] == 'CENTER_ADMIN' && this.user.userRoles.length == 1) {
                //     window.localStorage.setItem('userType', 'Center');
                //     this.hover = false;
                // }
                this.user.userRoles.forEach((role, i) => {
                    if (role === "CENTER_ADMIN") {
                        center_admin = true;
                        operator_user = false;
                    }
                    if (role === "DOCTOR") {
                        customer_admin = true;
                        operator_user = false;
                        center_admin = false;
                    }
                    if (role === "TECHNICIAN") {
                        customer_admin = true;  
                        operator_user = false;  
                        center_admin = false;                   
                    }
                    if (role === "OPERATOR") {
                        operator_user = true;                       
                    }
                });
                this.user.userRoles.sort();
                var center_admin_check = this.user.userRoles.toString();
                if(center_admin_check == 'CENTER_ADMIN,DOCTOR'){
                    center_admin = true;
                    customer_admin = false;
                }   
                if(center_admin_check == 'CENTER_ADMIN,DOCTOR,OPERATOR'){
                    center_admin = true;
                    customer_admin = false;
                }                    
                if (this.user.userRoles[0] == 'OPERATOR' && this.user.userRoles.length == 1 && operator_user) {
                    window.localStorage.setItem('userType', 'User');
                    this.hover = false;
                }
                else if(center_admin){
                    window.localStorage.setItem('userType', 'Center');
                    this.hover = false;  
                }
                else {
                    isFirst = false;
                    this.hover = true;
                    window.localStorage.setItem('userType', 'Facility');
                }

            }
            else {
                let isFirst = true;
                this.hover = true;
                if (window.localStorage.getItem('userType')) {
                    if (window.localStorage.getItem('userType') == 'Admin')
                        isFirst = false;
                }
                window.localStorage.setItem('userType', 'Admin');
            }
            if (window.localStorage.getItem('userType') == 'Facility')
                this.userType = 'Facility';
            else if (window.localStorage.getItem('userType') == 'Center')
                this.userType = 'Center';
            else if (window.localStorage.getItem('userType') == 'User')
                this.userType = 'User';
            else
                this.userType = 'Admin';

            this.getFacilities();
            // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            //     'assets/app/js/trends.center.js');
            // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            //     'assets/demo/default/custom/components/datatables/base/date.dashboard.js');


        }
        else {
            this._router.navigate(['/login']);
        }
    }

    getFacilities(): void {
        Helpers.setLoading(true);
        if (!localStorage.getItem('facilityTable')) {
            this._facilitiesService.getFacilities(0, 0)  // for get All facility we should pass page and size as zero
                .then(facilities => {
                    this.facilities = facilities.organizations;
                    this.facilityCount = facilities.count;
                    localStorage.setItem('facilityTable', JSON.stringify(this.facilities)); // for save all facility details                                

                })
                .then(() => {
                    Helpers.setLoading(false);
                    this.facilities.sort(function(a, b) { return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0); });
                    let allFacility = new Facility();
                    allFacility.name = "ALL";
                    allFacility.id = "ALL";
                    this.facilities.splice(0, 0, allFacility);
                    this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
                    if (window.localStorage.getItem('userType') == 'Facility') {
                        this.selectedFacility.id = this.userInfo.facilityId;
                        this.selectedFacility2.id = this.userInfo.facilityId;
                        this.facilities.forEach(fclt => {
                            if (fclt.id == this.userInfo.facilityId) {
                                this.centers = jQuery.extend(true, [], fclt.centers);
                                this.centers2 = jQuery.extend(true, [], fclt.centers);
                                let allCenter = new Center();
                                allCenter.name = "ALL";
                                allCenter.id = "ALL";
                                this.centers.splice(0, 0, allCenter);
                                this.centers2.splice(0, 0, allCenter);
                                this.selectedCenter.id = this.centers[0].id;
                                this.selectedCenter2.id = this.centers2[0].id;
                            }
                        });
                    }
                    else {
                        this.selectedFacility.id = this.facilities[0].id;
                        this.selectedFacility2.id = this.facilities[0].id;
                        this.centers = this.facilities[0].centers;
                        this.centers2 = this.facilities[0].centers;
                        // this.selectedCenter.id = this.centers[0].id;
                        // this.selectedCenter2.id = this.centers2[0].id;
                        this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
                        this.selectedFacility2.organizationCode = this.facilities[0].organizationCode;
                    }
                    var startDate = new Date();
                    startDate.setDate(startDate.getDate() - 29);
                    startDate.setHours(0);
                    startDate.setMinutes(0);
                    startDate.setSeconds(0);
                    startDate.setMilliseconds(0);
                    var endDate = new Date();
                    //endDate.setDate(endDate.getDate() + 1);
                    endDate.setHours(23);
                    endDate.setMinutes(23);
                    endDate.setSeconds(23);
                    endDate.setMilliseconds(0);
                    var startDate2 = new Date();
                    startDate2.setMonth(startDate2.getMonth() - 3);
                    // startDate2.setDate(startDate2.getDate() - 29);
                    startDate2.setHours(0);
                    startDate2.setMinutes(0);
                    startDate2.setSeconds(0);
                    startDate2.setMilliseconds(0);
                    var endDate2 = new Date();
                    //endDate.setDate(endDate.getDate() + 1);
                    endDate2.setHours(23);
                    endDate2.setMinutes(23);
                    endDate2.setSeconds(23);
                    endDate2.setMilliseconds(0);
                    let searchcriteria = "ALL";
                    if (window.localStorage.getItem('userType') === "Facility") {
                        searchcriteria = "FACILITY";
                        this.getPatientCount(startDate, endDate, searchcriteria, this.selectedFacility.id, 0);
                        this.getNewDeviceCount(startDate, endDate, searchcriteria, this.selectedFacility.id, 0);
                        this.getNewVisitCount(startDate, endDate, searchcriteria, this.selectedFacility.id, 0);
                        //this.getPatientCountByMonth(searchcriteria, this.selectedFacility.id, 0);
                        // this.getTestCountByMonth(searchcriteria, this.selectedFacility.id, 0);
                        this.getDeviceCount();
                        this.getUserCount();
                        //this.getTotalPatientCount();
                        this.getTotalPatientCounts();
                        this.getVisits();
                        this.getDiagnostics();
                        //   // console.log(startDate2, endDate2)
                        //this.getTestsForMonth(searchcriteria, this.selectedFacility.id);
                        this.getPatientCountByWeek(startDate2, endDate2, searchcriteria, this.selectedFacility.id, 0);
                        this.getTestCountByWeek(startDate2, endDate2, searchcriteria, this.selectedFacility.id, 0);
                    }
                    else if (window.localStorage.getItem('userType') === "Center") {
                        this.selectedCenter.id = this.user.centerId;
                        this.selectedFacility.id = this.user.facilityId;
                        this.selectedFacility.organizationCode = this.user.facilityCode;
                        this.getPatientCount(0, 0, "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                        this.getNewDeviceCount(0, 0, "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                        this.getNewVisitCount(0, 0, "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                        this.getPatientCountByWeek(startDate2, endDate2, "CENTER", this.selectedFacility.id, this.selectedCenter.id)
                        this.getTestCountByWeek(startDate2, endDate2, "CENTER", this.selectedFacility.id, this.selectedCenter.id)
                        this.getNewTestCount(0, 0, "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                    }
                    else if (window.localStorage.getItem('userType') === "User") {
                        this.getPatientCountUser();
                        this.getDigCountUser();
                        this.getVisitCountByUser(this.user.id, 0, 0)
                        this.getTestMapped();
                        this.getTestCountByWeekUser(startDate2, endDate2)
                        this.getPatientCountByWeekUser('USER',startDate2,endDate2,this.user.id)
                    }
                    else {
                        this.getDashboardCounts();
                        this.getPatientCountByWeek(startDate2, endDate2, searchcriteria, 0, 0);
                        this.getTestCountByWeek(startDate2, endDate2, searchcriteria, 0, 0);
                    }
                });
        }
        else {
            this.facilities = JSON.parse(window.localStorage.getItem('facilityTable'));
            this.facilities.sort(function(a, b) { return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0); });
            let allFacility = new Facility();
            allFacility.name = "ALL";
            allFacility.id = "ALL";
            this.facilities.splice(0, 0, allFacility);
            this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
            if (window.localStorage.getItem('userType') == 'Facility') {
                this.selectedFacility.id = this.userInfo.facilityId;
                this.selectedFacility2.id = this.userInfo.facilityId;
                this.facilities.forEach(fclt => {
                    if (fclt.id == this.userInfo.facilityId) {
                        this.centers = jQuery.extend(true, [], fclt.centers);
                        this.centers2 = jQuery.extend(true, [], fclt.centers);
                        let allCenter = new Center();
                        allCenter.name = "ALL";
                        allCenter.id = "ALL";
                        this.centers.splice(0, 0, allCenter);
                        this.centers2.splice(0, 0, allCenter);
                        this.selectedCenter.id = this.centers[0].id;
                        this.selectedCenter2.id = this.centers2[0].id;
                    }
                });
            }
            else {
                this.selectedFacility.id = this.facilities[0].id;
                this.selectedFacility2.id = this.facilities[0].id;
                this.centers = this.facilities[0].centers;
                this.centers2 = this.facilities[0].centers;
                // this.selectedCenter.id = this.centers[0].id;
                // this.selectedCenter2.id = this.centers2[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
                this.selectedFacility2.organizationCode = this.facilities[0].organizationCode;
            }
            var startDate = new Date();
            startDate.setDate(startDate.getDate() - 29);
            startDate.setHours(0);
            startDate.setMinutes(0);
            startDate.setSeconds(0);
            startDate.setMilliseconds(0);
            var endDate = new Date();
            //endDate.setDate(endDate.getDate() + 1);
            endDate.setHours(23);
            endDate.setMinutes(23);
            endDate.setSeconds(23);
            endDate.setMilliseconds(0);
            var startDate2 = new Date();
            startDate2.setMonth(startDate2.getMonth() - 3);
            // startDate2.setDate(startDate2.getDate() - 29);
            startDate2.setHours(0);
            startDate2.setMinutes(0);
            startDate2.setSeconds(0);
            startDate2.setMilliseconds(0);
            var endDate2 = new Date();
            //endDate.setDate(endDate.getDate() + 1);
            endDate2.setHours(23);
            endDate2.setMinutes(23);
            endDate2.setSeconds(23);
            endDate2.setMilliseconds(0);
            let searchcriteria = "ALL";
            if (window.localStorage.getItem('userType') === "Facility") {
                searchcriteria = "FACILITY";
                this.getPatientCount(startDate, endDate, searchcriteria, this.selectedFacility.id, 0);
                this.getNewDeviceCount(startDate, endDate, searchcriteria, this.selectedFacility.id, 0);
                this.getNewVisitCount(startDate, endDate, searchcriteria, this.selectedFacility.id, 0);
                //this.getPatientCountByMonth(searchcriteria, this.selectedFacility.id, 0);
                // this.getTestCountByMonth(searchcriteria, this.selectedFacility.id, 0);
                this.getDeviceCount();
                this.getUserCount();
                //this.getTotalPatientCount();
                this.getTotalPatientCounts();
                this.getVisits();
                this.getDiagnostics();
                //this.getTestsForMonth(searchcriteria, this.selectedFacility.id);
                this.getPatientCountByWeek(startDate2, endDate2, searchcriteria, this.selectedFacility.id, 0);
                this.getTestCountByWeek(startDate2, endDate2, searchcriteria, this.selectedFacility.id, 0);
            }
            else if (window.localStorage.getItem('userType') === "Center") {
                searchcriteria = "CENTER";
                this.selectedCenter.id = this.user.centerId;
                this.selectedFacility.id = this.user.facilityId;
                this.selectedFacility.organizationCode = this.user.facilityCode;

                this.getPatientCount(0, 0, "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                this.getNewDeviceCount(0, 0, "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                this.getNewVisitCount(0, 0, "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                this.getPatientCountByWeek(startDate2, endDate2, "CENTER", this.selectedFacility.id, this.selectedCenter.id)
                this.getTestCountByWeek(startDate2, endDate2, "CENTER", this.selectedFacility.id, this.selectedCenter.id)
                this.getNewTestCount(0, 0, "CENTER", this.selectedFacility.id, this.selectedCenter.id);
            }
            else if (window.localStorage.getItem('userType') === "User") {
                this.getPatientCountUser();
                this.getDigCountUser();
                this.getTestMapped();
                this.getVisitCountByUser(this.user.id, 0, 0)
                this.getTestCountByWeekUser(startDate2, endDate2)
                this.getPatientCountByWeekUser('USER',startDate2,endDate2,this.user.id)
            }
            else {
                this.getDashboardCounts();
                // this.getPatientCount(startDate, endDate, searchcriteria, 0);
                // this.getNewDeviceCount(startDate, endDate, searchcriteria, 0, 0);
                // this.getNewVisitCount(startDate, endDate, searchcriteria, 0, 0);
                // //this.getPatientCountByMonth(searchcriteria, 0, 0);
                // //  this.getTestCountByMonth(searchcriteria, 0, 0);
                // this.getDeviceCount();
                // this.getUserCount();
                // //this.getTotalPatientCount();
                // this.getTotalPatientCounts();
                // this.getVisits();
                // this.getDiagnostics();
                // //this.getTestsForMonth(searchcriteria, 0);
                this.getPatientCountByWeek(startDate2, endDate2, searchcriteria, 0, 0);
                this.getTestCountByWeek(startDate2, endDate2, searchcriteria, 0, 0);
            }
        }
    }

    ngOnDestroy() {
        window.my.namespace.publicFunc = null;
        window.my.namespace.publicFuncPatientWeek = null;
        window.my.namespace.publicFuncTestWeek = null;
    }

    publicFunc() {
        this._ngZone.run(() => this.refreshDiagnosisForMonth());
    }

    publicFuncPatientWeek(week, year, type) {
        var weekTosend = week.split("\n")
        this._ngZone.run(() => this.gotToPatientList(weekTosend[0], year, type));
    }

    publicFuncTestWeek(week, year) {
        var weekTosend = week.split("\n")
        this._ngZone.run(() => this.gotToTestList(weekTosend[0], year));
    }

    getDateOfISOWeek(w, y) {
        var simple = new Date(y, 0, 1 + (w - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay());
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        return ISOweekStart;
    }

    gotToPatientList(week, year, type) {
        var frmdt = this.getDateOfISOWeek(week, year);
        frmdt.setFullYear(year);
        frmdt.setDate(frmdt.getDate() - frmdt.getDay());
        var todt = new Date(frmdt);
        todt.setDate(todt.getDate() + 6);
        todt.setFullYear(year);
        todt.setHours(23);
        todt.setMinutes(23);
        todt.setSeconds(23);
        todt.setMilliseconds(0);
        if (frmdt.getDate() > todt.getDate() && week === "1") { //Special check for week #1 year bug
            frmdt.setFullYear(frmdt.getFullYear() - 1);
        }
        if (type === "patient") {
            this._router.navigate(['/patients/stats'], { queryParams: { from: frmdt, to: todt, facilityId: this.selectedFacility.id, centerId: this.selectedCenter.id } });
        }
        else {
            this._router.navigate(['/visits/stats'], { queryParams: { from: frmdt, to: todt, facilityId: this.selectedFacility.id, centerId: this.selectedCenter.id } });
        }
    }

    gotToTestList(week, year) {
        var frmdt = this.getDateOfISOWeek(week, year);
        frmdt.setDate(frmdt.getDate() - frmdt.getDay());
        var todt = new Date(frmdt);
        todt.setDate(todt.getDate() + 7);
        todt.setHours(23);
        todt.setMinutes(23);
        todt.setSeconds(23);
        todt.setMilliseconds(0);
        if (frmdt.getDate() > todt.getDate() && week === "1") { //Special check for week #1 year bug
            frmdt.setFullYear(frmdt.getFullYear() - 1);
        }
        this._router.navigate(['/visits/list'], { queryParams: { from: frmdt, to: todt, facilityId: this.selectedFacility.id, centerId: this.selectedCenter.id } });
    }

    refreshDiagnosisForMonth() {
        // do private stuff
        this.testsMonth = [];
        this.testsForMonth.forEach(test => {
            if ((test.year === window.localStorage.getItem('SelectedYearDiagnosis') || test.year === window.localStorage.getItem('SelectedYearDiagnosis').substring(2)) && this.getMonthName(test.month) == window.localStorage.getItem('SelectedMonthDiagnosis')) {
                var testType = "";
                this.testList.forEach(test2 => {
                    test2.tests.forEach(test3 => {
                        if (test3.type === test.testType)
                            test.testType = test3.name;
                    });
                });
                this.testsMonth.push({ "Tests": test.count, "Type": test.testType });
            }
        });
        window.localStorage.setItem('testsForMonth', JSON.stringify(this.testsMonth));
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/charts/amcharts/charts.testformonth.js');
    }

    onPatientCountClick(): void {
        this._router.navigate(['/patients/list'], { queryParams: { from: window.localStorage.getItem('dashStartDate'), to: window.localStorage.getItem('dashEndDate'), facilityId: this.selectedFacility.id, centerId: this.selectedCenter.id } });
    }

    onTestCountClick(): void {
        this._router.navigate(['/visits/list'], { queryParams: { from: window.localStorage.getItem('dashStartDate'), to: window.localStorage.getItem('dashEndDate'), facilityId: this.selectedFacility.id, centerId: this.selectedCenter.id } });
    }

    onDeviceCountClick(): void {
        this._router.navigate(['/devices/list'], { queryParams: { from: window.localStorage.getItem('dashStartDate'), to: window.localStorage.getItem('dashEndDate'), facilityId: this.selectedFacility.id, centerId: this.selectedCenter.id } });
    }

    filterReports(): void {
        if (this.selectedFacility.id != "ALL") {
            if (this.selectedCenter.id != "ALL") {
                this.getPatientCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                this.getNewDeviceCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                this.getNewVisitCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "CENTER", this.selectedFacility.id, this.selectedCenter.id);
            }
            else {
                this.getPatientCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "FACILITY", this.selectedFacility.id, 0);
                this.getNewDeviceCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "FACILITY", this.selectedFacility.id, 0);
                this.getNewVisitCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "FACILITY", this.selectedFacility.id, 0);
            }
        }
        else {
            this.getDashboardCounts();
            // this.getPatientCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "ALL", 0);
            // this.getNewDeviceCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "ALL", 0, 0);
            // this.getNewVisitCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "ALL", 0, 0);
        }
    }

    onYearChange(year) {
        if (year === "ALL") {

        }
    }

    getVisits(): void {
        Helpers.setLoading(true);
        // if (window.localStorage.getItem('userType') === "Facility") {
        this.visitCount = null;
        this.testCount = null;
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var searchcriteria = "ALL";
        if (window.localStorage.getItem('userType') === "Facility") {
            var searchcriteria = "FACILITY";
            this._visitsService.getVisitCounts(0, 0, searchcriteria, this.selectedFacility.id, 0)
                .then(responseData => {
                    if (responseData != null) {
                        this.visits = responseData.visits;
                        this.visitCountTotal = responseData.count
                    } else {
                        this.visitCountTotal = 0;
                    }
                });
        }
        if (window.localStorage.getItem('userType') === "Center") {
            var searchcriteria = "CENTER";
            this._visitsService.getVisitCounts(0, 0, searchcriteria, this.selectedFacility.id, this.selectedCenter.id)
                .then(responseData => {
                    if (responseData != null) {
                        this.visits = responseData.visits;
                        this.visitCountTotal = responseData.count
                    } else {
                        this.visitCountTotal = 0;
                    }
                });

        }

    }

    getDiagnostics(): void {
        Helpers.setLoading(true);
        this.testCount = null;
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var searchcriteria = "ALL";
        if (window.localStorage.getItem('userType') === "Facility") {
            var searchcriteria = "FACILITY";
            this._visitsService.getDiagnosisDate(0, 0, searchcriteria, this.selectedFacility.id, 0, 1, 1)
                .then(responseData => {
                    if (responseData.length != 0) {
                        this.testCountTotal = responseData.totalHits;
                        localStorage.setItem('diagnosticCount',this.testCountTotal)
                    } else {
                        this.testCountTotal = 0;
                    }

                });
        }
        if (window.localStorage.getItem('userType') === "Center") {
            var searchcriteria = "CENTER";
            this._visitsService.getDiagnosisDate(0, 0, searchcriteria, this.selectedCenter.id, this.selectedFacility.id, 1, 1)
                .then(responseData => {
                    if (responseData.length != 0) {
                        this.testCountTotal = responseData.totalHits
                        localStorage.setItem('diagnosticCount',this.testCountTotal)
                    } else {
                        this.testCountTotal = 0;
                    }
                });
        }

    }

    getNewTestCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        this.testCountTotal = null;
        Helpers.setLoading(true);
        this._visitsService.getTestCounts(0, 0, searchcriteria, facilityid, centerid)
            .then(data => {
                if (data != null) {
                    //this.totalTest = data
                    this.testCountTotal = data.totalHits.toString();
                    localStorage.setItem('diagnosticCount',this.testCountTotal)
                }
                else
                    this.testCountTotal = 0;
                //this.testCountTotal = this.testCountTotal.toString();
            })
            .then(() => {
                Helpers.setLoading(false);
            });
    }

    getDeviceCount(): void {
        Helpers.setLoading(true);
        if (window.localStorage.getItem('userType') === "Facility") {
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            this._devicesService.getDeviceCountDashboard(0, tomorrow.getTime(), "FACILITY", this.selectedFacility.id)
                .then(count => this.devicesTotalCount = count)
                .then(() => {
                    Helpers.setLoading(false);
                });
        }
        else {
            this._devicesService.getDeviceCountDashboard(0, 0, "ALL", 0)
                .then(count => this.devicesTotalCount = count)
                .then(() => {
                    Helpers.setLoading(false);
                });
        }
    }

    getUserCount(): void {
        Helpers.setLoading(true);
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        if (window.localStorage.getItem('userType') === "Facility") {
            this._usersService.getUserCountDashboard(0, tomorrow.getTime(), "FACILITY", this.selectedFacility.id)
                .then(users => this.usersTotal = users)
                .then(() => {
                    Helpers.setLoading(false);
                });
        }
        else {
            this._usersService.getUserCountDashboard(0, 0, "ALL", 0)
                .then(users => this.usersTotal = users)
                .then(() => {
                    Helpers.setLoading(false);
                });
        }
    }

    getTotalPatientCounts(): void {
        Helpers.setLoading(true);
        var searchBy;
        if (window.localStorage.getItem('userType') === "Facility") {
            searchBy = 'FACILITY'
            this._patientsService.getPatientFaclityCount(searchBy, this.selectedFacility.id)
                .then(count => { this.patientsTotalCount = count })
                .then(() => {
                    Helpers.setLoading(false);
                });
        }
        else {
            this._patientsService.getPatientCountDashboard()
                .then(count => { this.patientsTotalCount = count })
                .then(() => {
                    Helpers.setLoading(false);
                });
        }
    }

    getTotalPatientCount(): void {
        Helpers.setLoading(true);
        if (window.localStorage.getItem('userType') === "Facility") {
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            this._patientsService.getPatientCount(0, tomorrow.getTime(), "FACILITY", this.selectedFacility.id)
                .then((count) => {
                    this.patientsTotalCount = count; 
                    //console.log(this.patientsTotalCount)
                    localStorage.setItem('patientcount',this.patientsTotalCount)
                })
                .then(() => {
                    
                    Helpers.setLoading(false);
                });
        }
        else {
            this._patientsService.getTotalPatientCount()
                .then(count => this.patientsTotalCount = count)
                .then(() => {
                    Helpers.setLoading(false);
                });
        }
    }

    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers[0].id !== "ALL") {
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                }
                this.selectedCenter.id = this.centers[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }

    onOrgChange2(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility2.organizationCode = org.organizationCode;
                this.centers2 = org.centers;
                if (this.centers2[0].id !== "ALL") {
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers2.splice(0, 0, allCenter);
                }
                this.selectedCenter2.id = this.centers2[0].id;
                this.selectedFacility2.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }

    getPatientCount(startDate, endDate, searchcriteria, facilityId, centerId): void {
        Helpers.setLoading(true);
        this.patientCount = null;
        this._patientsService.getPatientFilterCount(0, 0, searchcriteria, facilityId, centerId)
            .then(count => {this.patientsTotalCount = count            
                console.log(this.patientsTotalCount)
                localStorage.setItem('patientcount',this.patientsTotalCount)
            })
            .then(() => {
                Helpers.setLoading(false);
            });
    }

    getNewDeviceCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        Helpers.setLoading(true);
        this.deviceCount = null;
        this._devicesService.getNewDeviceCount(0, 0, searchcriteria, facilityid, centerid)
            .then(data => {
                if (data != null)
                    this.devicesTotalCount = data
                else
                    this.devicesTotalCount = 0
            })
            .then(() => {
                this.deviceCount = this.deviceCount.toString();
                Helpers.setLoading(false);

            });
    }
    getDashboardCounts(): void {
        Helpers.setLoading(true);
        this.deviceCount = null;
        this.facilityCount = null;
        this.devicesTotalCount = null;
        this.patientsTotalCount = null;
        this.usersTotal = null;
        this.testCountTotal = null;
        this.visitCountTotal = null;
        this._visitsService.getDashboardCounts()
            .then(data => {
                //console.log('dashboard count', data)
                this.facilityCount = data.organizations;
                this.devicesTotalCount = data.devices;
                this.patientsTotalCount = data.patients;
                this.testCountTotal = data.tests;
                this.visitCountTotal = data.visits;
                this.usersTotal = data.users;
                localStorage.setItem('dashboardcount', JSON.stringify(data));
            })
            .then(() => {
                //this.deviceCount = this.deviceCount.toString();
                Helpers.setLoading(false);

            });
    }

    getNewVisitCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        this.visitCountTotal = null;
        Helpers.setLoading(true);
        this._visitsService.getVisitCounts(new Date(startDate).getTime(), new Date(endDate).getTime(), searchcriteria, facilityid, centerid)
            .then(data => {
                if (data != null) {
                    this.totalVists = data
                    this.visitCountTotal = data.count.toString();
                }
                else
                    this.visitCountTotal = 0;
                // this.visitCountTotal = this.visitCountTotal.toString();
            })
            .then(() => {
                Helpers.setLoading(false);
            });
    }

    getMonthName(month): any {
        var monthString = "Jan";
        if (month == "1")
            monthString = "Jan";
        else if (month == "2")
            monthString = "Feb";
        else if (month == "3")
            monthString = "Mar";
        else if (month == "4")
            monthString = "Apr";
        else if (month == "5")
            monthString = "May";
        else if (month == "6")
            monthString = "Jun";
        else if (month == "7")
            monthString = "Jul";
        else if (month == "8")
            monthString = "Aug";
        else if (month == "9")
            monthString = "Sep";
        else if (month == "10")
            monthString = "Oct";
        else if (month == "11")
            monthString = "Nov";
        else
            monthString = "Dec";

        return monthString;
    }

    getPatientCountByWeek(from, to, searchcriteria, facilityid, centerId): void {
        Helpers.setLoading(true);
        this.patientCountByWeek = [];
        this.patientChart = [];
        this.showPatLoad = true;
        this._patientsService.getPatientCountByWeek(searchcriteria, new Date(from).getTime(), new Date(to).getTime(), facilityid, centerId)
            .then(count => this.patientCountByWeek = count)
            .then(() => {
                //  // console.log('patient by week', this.patientCountByWeek)
                var patientCountByWeekForChart = [];
                // for (var index = 1; index <= 12; index++) {
                //     this.selectedYearPatientCountByMonth.push({ "Patients": 0, "Month": this.getMonthName(index.toString()) });
                // }

                var tempData;
                var dummy = [];

                var names_array_new = this.patientCountByWeek.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.id.week === b.id.week) {
                            dummy.push(a);
                            return dummy;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.forEach(element => {
                    for (var i = 0; i < dummy.length; i++) {
                        if (element.id.week == dummy[i].id.week) {
                            element.count = dummy[i].count + element.count
                        }
                    }
                })

                // names_array_new.sort(function(a, b) {
                //     if (a.id.week > b.id.week) {
                //         return 1;
                //     } else if (a.id.week < b.id.week) {
                //         return -1;
                //     }
                //     return 0;
                // });

                var dummyArray = [];

                names_array_new = names_array_new.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.id.year > b.id.year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);

                var currentYear = new Date().getFullYear()

                names_array_new.forEach(element => {
                    if (element.id.year == currentYear) {
                        element.id.week = parseInt(element.id.week) + 1;
                    }
                });

                dummyArray.forEach(data => {
                    if (data.id.year == currentYear) {
                        data.id.week = parseInt(data.id.week) + 1
                    }
                })

                names_array_new = names_array_new.concat(dummyArray)

                this.patientCountByWeek = names_array_new;

                this.patientCountByWeek.forEach(data => {
                    patientCountByWeekForChart.push({ "Patients": data.count, "Week": data.id.week, "Year": data.id.year, "Week_Year": data.id.week + "\n" + '(' + data.id.year + ')' });
                });
                this.patientChart = patientCountByWeekForChart

                Helpers.setLoading(false);
                this.showPatLoad = false;

            });
    }

    getPatientCountByWeekUser(searchcriteria,from, to, id): void {
        Helpers.setLoading(true);
        this.patientCountByWeek = [];
        this.patientChart = [];
        this.showPatLoad = true;
        this._patientsService.getPatientCountByWeekForUser(searchcriteria, new Date(from).getTime(), new Date(to).getTime(),id)
            .then(count => this.patientCountByWeek = count)
            .then(() => {
                //  // console.log('patient by week', this.patientCountByWeek)
                var patientCountByWeekForChart = [];
                // for (var index = 1; index <= 12; index++) {
                //     this.selectedYearPatientCountByMonth.push({ "Patients": 0, "Month": this.getMonthName(index.toString()) });
                // }

                var tempData;
                var dummy = [];

                var names_array_new = this.patientCountByWeek.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.id.week === b.id.week) {
                            dummy.push(a);
                            return dummy;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.forEach(element => {
                    for (var i = 0; i < dummy.length; i++) {
                        if (element.id.week == dummy[i].id.week) {
                            element.count = dummy[i].count + element.count
                        }
                    }
                })

                // names_array_new.sort(function(a, b) {
                //     if (a.id.week > b.id.week) {
                //         return 1;
                //     } else if (a.id.week < b.id.week) {
                //         return -1;
                //     }
                //     return 0;
                // });

                var dummyArray = [];

                names_array_new = names_array_new.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.id.year > b.id.year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);

                var currentYear = new Date().getFullYear()

                names_array_new.forEach(element => {
                    if (element.id.year == currentYear) {
                        element.id.week = parseInt(element.id.week) + 1;
                    }
                });

                dummyArray.forEach(data => {
                    if (data.id.year == currentYear) {
                        data.id.week = parseInt(data.id.week) + 1
                    }
                })

                names_array_new = names_array_new.concat(dummyArray)

                this.patientCountByWeek = names_array_new;

                this.patientCountByWeek.forEach(data => {
                    patientCountByWeekForChart.push({ "Patients": data.count, "Week": data.id.week, "Year": data.id.year, "Week_Year": data.id.week + "\n" + '(' + data.id.year + ')' });
                });
                this.patientChart = patientCountByWeekForChart

                Helpers.setLoading(false);
                this.showPatLoad = false;

            });
    }

    formateDate(d): string {
        var m_names = new Array("Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec");

        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();
        return m_names[curr_month] + " " + curr_date + " " + curr_year;
    }
    getDateRangeOfWeek(w, y): string {
        var simple = new Date(y, 0, 1 + (w - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay());
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        var ISOweekEnd = new Date(ISOweekStart);
        ISOweekEnd.setDate(ISOweekEnd.getDate() + 6);
        ISOweekEnd.setHours(23);
        ISOweekEnd.setMinutes(23);
        ISOweekEnd.setSeconds(23);
        ISOweekEnd.setMilliseconds(0);
        return this.formateDate(ISOweekStart) + " to " + this.formateDate(ISOweekEnd);
    };

    getTestCountByWeek(from, to, searchcriteria, facilityid, centerId): void {
        Helpers.setLoading(true);
        this.testCountByWeek = [];
        this.testChartValue = [];
        this.showDigLoad = true;
        this._visitsService.getTestsCountByWeek(searchcriteria, new Date(from).getTime(), new Date(to).getTime(), facilityid, centerId)
            .then(count => this.testCountByWeek = count)
            .then(() => {
                this.showDigLoad = false;
                var testCountByWeekForChart = [];
                // for (var index = 1; index <= 12; index++) {
                //     this.selectedYearPatientCountByMonth.push({ "Patients": 0, "Month": this.getMonthName(index.toString()) });
                // }
                this.testCountByWeek.forEach(data => {
                    var isWeekCreated = false;
                    testCountByWeekForChart.forEach(week => {
                        if (week.Week === data.week)
                            isWeekCreated = true;
                    });
                    if (!isWeekCreated) {
                        var objectToPush = {};
                        objectToPush['Week'] = data.week;
                        objectToPush[data.testType] = data.count;
                        objectToPush['Year'] = '20' + data.year;
                        objectToPush['Week_Year'] = data.week + "\n" + '(' + '20' + data.year + ')';
                        testCountByWeekForChart.push(objectToPush);
                        testCountByWeekForChart.sort(function(a, b) {
                            if (a.Year > b.Year) { return -1; } else if (a.Year < b.Year) { return 1; }
                            return 0;
                        });
                        //    // console.log(testCountByWeekForChart)

                    }
                    else {
                        testCountByWeekForChart.forEach(week => {
                            if (week.Week === data.week) {
                                week[data.testType] = data.count;
                            }
                        });
                    }
                });

                testCountByWeekForChart.forEach(testWeek => {
                    testWeek.Week = Number(testWeek.Week)
                })

                var dummyArray = [];

                var names_array_new = testCountByWeekForChart.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.Year > b.Year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                dummyArray.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                names_array_new = names_array_new.concat(dummyArray)

                testCountByWeekForChart = names_array_new;
                this.testChartValue = testCountByWeekForChart;
                this.graphValues = [];

                Helpers.setLoading(false);

            });
    }

    // User/ operator part

    getPatientCountUser() {
        this._usersService.getUserPatientCount(this.user.id).then(data => {
            this.patientsTotalCount = data.toString();
        })
    }

    getDigCountUser() {
        this._usersService.getDignosticbyUser(this.user.id).then(data => {
            this.testCountTotal = data.toString();
        })
    }

    getTestMapped() {
        this.testMapCount = this.user.testMapping.length
    }

    getVisitCountByUser(userId, from, to) {
        this._visitsService.getVisitByUser(userId, from, to).then(data => {
            if (data != null)
                this.visitCountTotal = data.count.toString();
            else
                this.visitCountTotal = 0;
        })
    }

    getTestCountByWeekUser(from, to): void {
        Helpers.setLoading(true);
        this.testCountByWeek = [];
        this.testChartValue = [];
        this.showDigLoad = true;
        this._usersService.getDignosticTestbyUser(new Date(from).getTime(), new Date(to).getTime(), this.user.id)
            .then(count => {
                if (count != null) {
                    this.testCountByWeek = count
                }
                else
                    this.testCountByWeek = [];
            })
            .then(() => {
                var testCountByWeekForChart = [];
                this.testCountByWeek.forEach(data => {
                    var isWeekCreated = false;
                    testCountByWeekForChart.forEach(week => {
                        if (week.Week === data.week)
                            isWeekCreated = true;
                    });
                    if (!isWeekCreated) {
                        var objectToPush = {};
                        objectToPush['Week'] = data.week;
                        objectToPush[data.testType] = data.count;
                        objectToPush['Year'] = '20' + data.year;
                        objectToPush['Week_Year'] = data.week + "\n" + '(20' + data.year + ')';
                        testCountByWeekForChart.push(objectToPush);
                    }
                    else {
                        testCountByWeekForChart.forEach(week => {
                            if (week.Week === data.week) {
                                week[data.testType] = data.count;
                            }
                        });
                    }
                });

                testCountByWeekForChart.forEach(testWeek => {
                    testWeek.Week = Number(testWeek.Week)
                })
                var dummyArray = [];

                var names_array_new = testCountByWeekForChart.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.Year > b.Year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                dummyArray.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                names_array_new = names_array_new.concat(dummyArray)

                testCountByWeekForChart = names_array_new;


                this.graphValues = [];
                this.testList.forEach(test => {
                    this.graphValues.push({
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>week [[category]]: <b>[[value]] diagnostics done</b></span>",
                        "fillAlphas": 1.8,
                        "labelText": "[[value]]",
                        "lineAlpha": 1.0,
                        "title": test.name,
                        "type": "column",
                        "color": "#000000",
                        "showHandOnHover": true,
                        "valueField": test.type.toLowerCase(),
                        "colorField": test.color,
                        "fillColors": test.color,
                        "lineColor": test.color
                    });
                });
                this.testChartValue = testCountByWeekForChart
                this.showDigLoad = false;
                window.localStorage.setItem("selectedYear", this.selectedYear.toString());

                Helpers.setLoading(false);

            });
    }

}