import { Component, OnInit, ViewEncapsulation, NgZone, OnDestroy } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { AnalyticsService } from '../analytics.service';
import { ScriptLoaderService } from './../../../../../../_services/script-loader.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./userwise-stats.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})

export class UserwiseStatsComponent implements OnInit, OnDestroy {    
    user: any;
    visitcount: any[];
    patientcount: any[];
    analyticsUser: any;
    analyticsUsersresp: any={};
    userrole:any;
    userType : any;
    constructor(private _router: Router, private analytics: AnalyticsService, private _script: ScriptLoaderService, private _ngZone: NgZone) {
        this.user=JSON.parse(localStorage.getItem('userInfo'));
        if (window.localStorage.getItem('userType') == 'Facility')
        this.userType = 'Facility';
        else if (window.localStorage.getItem('userType') == 'Center')
            this.userType = 'Center';
        else if (window.localStorage.getItem('userType') == 'User')
            this.userType = 'User';
        else
            this.userType = 'Admin';
     }

    public analyticsRespArr: any = [];
    public avgdiagnosticresp: any = {};
    public dataTESTPerReg: any = [];
    public selectedoptionTEST: string = 'Consumable based test'
    public showDigLoad: boolean = true;
    public TESTselectedbaloon: string = 'Consumable based test / Registration';
    public selectedoptionCCWise: string = 'Registration';
    public dataVisitStat: any = [];
    public visitorlog: any = [];
    public ccRegTest: any = [];
    public dataforRegStat: any = [];
    public dataforTestStat: any = [];
    public visitorlogresp: any = {};
    public ccRegTestresp: any = {};
    public visitorlogCount: number = 0;
    public from: any;
    public to: any;
    public totalpatientsReg: number;
    public totaldiagnosticsmade: number;


    ngOnInit() {
        Helpers.setLoading(true);
        
        window.my = window.my || {};
        window.my.namespace1 = window.my.namespace1 || {};
        window.my.namespace1.gotodesiredpage = this.gotodesiredpage.bind(this);
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/datatables/base/date.analytics.js');
        var date = new Date();
        this.to = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        this.from = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 29, 0);
        // console.log(this.from, this.to)
        if(this.userType == 'Facility'){
            if ((this.user.userRoles).includes('TECHNICIAN')) {
                this.userSpecificStats('facility',this.user.facilityId);
                this.getallvisitorLogCustomerAdmin('facility',this.user.facilityId,this.from.getTime(), this.to.getTime());
            }
            else {
                this.userSpecificStats('center',this.user.centerId);
                this.getallvisitorLogCustomerAdmin('center',this.user.centerId,this.from.getTime(), this.to.getTime());
            }
        }else if(this.userType == 'Center'){
            if ((this.user.userRoles).includes('CENTER_ADMIN')) {
                this.userSpecificStats('center',this.user.centerId);
                this.getallvisitorLogCustomerAdmin('center',this.user.centerId,this.from.getTime(), this.to.getTime());
            }     
        }
        else{
            this.getavgdiagnosticAndConsumablebasedtest();
            this.getallvisitorLog(this.from.getTime(), this.to.getTime());
        }       

    }

    ngOnDestroy() {
        window.my.namespace1 = null;
    }


    userSpecificStats(type,id): any {
        this.dataTESTPerReg = [];
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getuserAnalyticsSearch(type,id).then((res) => {
            Helpers.setLoading(false);
              console.log(res)
            if (res != undefined) {
                let patientcountarr = [];
                let diagnosticscountarr = [];
                this.avgdiagnosticresp = res;
                localStorage.setItem('analytics_res', JSON.stringify(this.avgdiagnosticresp));
                this.analyticsRespArr = this.avgdiagnosticresp.results;
                //console.log(this.analyticsRespArr)
                this.analyticsRespArr.map((item) => {
                    diagnosticscountarr.push(item.totalTests);
                    patientcountarr.push(item.totalRegistrations);
                    this.dataTESTPerReg.push({
                        "y": item.totalConsumableTests,
                        "x": item.totalRegistrations,
                        "value": (item.consumableTestsPerRegistration.toFixed(2)),
                        "facility": item.displayName,
                        "center": item.centerName
                    });
                });
                
                console.log(this.dataTESTPerReg)
                /**
   * gettotaldiagnostics
   */
                let patientData = JSON.parse(localStorage.getItem('patientcount'));
                let diagnosticCount = JSON.parse(localStorage.getItem('diagnosticCount'));
                console.log(patientData)
                this.totalpatientsReg = patientData;
                this.totaldiagnosticsmade = diagnosticCount;
                this.analyticsRespArr.map((item) => {
                    //console.log(item)
                    if (((item.totalRegistrations / this.totalpatientsReg) * 100) > 1) {
                        this.dataforRegStat.push({
                            "percentage": ((item.totalRegistrations / this.totalpatientsReg) * 100).toFixed(2),
                            "patient registered": item.totalRegistrations,
                            "customer": item.displayName,
                            "center": item.centerName,
                            "registration": true
                        });
                    }
                    if (((item.totalTests / this.totaldiagnosticsmade) * 100) > 1) {
                        this.dataforTestStat.push({
                            "percentage": ((item.totalTests / this.totaldiagnosticsmade) * 100).toFixed(2),
                            "diagnostics": item.totalTests,
                            "customer": item.displayName,
                            "center": item.centerName,
                            "registration": false
                        })
                    }

                });
                console.log(this.dataforTestStat)
                console.log(this.dataforRegStat)
                setTimeout(() => {
                    this.showDigLoad = false;
                }, 1500);
            } else {
                this.analyticsRespArr = [];
                this.showDigLoad = false;
            }
        })
    }

    gotodesiredpage(param, dataContext, type) {
        this._ngZone.run(() => {
            if (type == "test") {
                if (param === "Diagnostics") {
                    this._router.navigate(['/analytics/avgdiagnostics'], { queryParams: { selectedItem: dataContext.facility, selectedcenter: dataContext.center, from:'user' } });
                } else {
                    this._router.navigate(['/analytics/avgconsumablebasedtest'], { queryParams: { selectedItem: dataContext.facility, selectedcenter: dataContext.center, from:'user' } });
                }
            }
            if (type == "devices") {
                this._router.navigate(['/analytics/permachinedata'], { queryParams: { selectedparam: param, selectedItem: dataContext.facility, selectedcenter: dataContext.center, from:'user' } });
            }
            if (param == 'visitstats') {
                this._router.navigate(['/analytics/visitlog'], { queryParams: { selectedItem: dataContext.User, fromdate: localStorage.getItem('analyticsStartDate'), todate: localStorage.getItem('analyticsEndDate') } });
            }
            if (type == "ccwisedata") {
                if (param == "registration") {
                    this._router.navigate(['/analytics/centerwiseregistration'], { queryParams: { selectedItem: dataContext.customer, selectedcenter: dataContext.center, from:'user' } });
                } else {
                    this._router.navigate(['/analytics/centerwisetest'], { queryParams: { selectedItem: dataContext.customer, selectedcenter: dataContext.center, from:'user' } });

                }
            }

        });
    }

    //get diagnostics and consumable based test per registration
    public getavgdiagnosticAndConsumablebasedtest(): void {
        this.dataTESTPerReg = [];
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getAlluserAnalytics().then((res) => {
            Helpers.setLoading(false);
            //  console.log(res)
            if (res != undefined) {
                let patientcountarr = [];
                let diagnosticscountarr = [];
                this.avgdiagnosticresp = res;
                localStorage.setItem('analytics_res', JSON.stringify(this.avgdiagnosticresp));
                this.analyticsRespArr = this.avgdiagnosticresp.results;
                this.analyticsRespArr.map((item) => {
                    diagnosticscountarr.push(item.totalTests);
                    patientcountarr.push(item.totalRegistrations);
                    this.dataTESTPerReg.push({
                        "y": item.totalConsumableTests,
                        "x": item.totalRegistrations,
                        "value": (item.consumableTestsPerRegistration.toFixed(2)),
                        "facility": item.displayName,
                        "center": item.centerName
                    });
                });

                /**
   * gettotaldiagnostics
   */
                let countdata = JSON.parse(localStorage.getItem('dashboardcount'));
                this.totalpatientsReg = countdata.patients;
                this.totaldiagnosticsmade = countdata.tests;
                this.analyticsRespArr.map((item) => {
                    //console.log(item)
                    if (((item.totalRegistrations / this.totalpatientsReg) * 100) > 1) {
                        this.dataforRegStat.push({
                            "percentage": ((item.totalRegistrations / this.totalpatientsReg) * 100).toFixed(2),
                            "patient registered": item.totalRegistrations,
                            "customer": item.displayName,
                            "center": item.centerName,
                            "registration": true
                        });
                    }
                    if (((item.totalTests / this.totaldiagnosticsmade) * 100) > 1) {
                        this.dataforTestStat.push({
                            "percentage": ((item.totalTests / this.totaldiagnosticsmade) * 100).toFixed(2),
                            "diagnostics": item.totalTests,
                            "customer": item.displayName,
                            "center": item.centerName,
                            "registration": false
                        })
                    }

                });

                setTimeout(() => {
                    this.showDigLoad = false;
                }, 1500);
            } else {
                this.analyticsRespArr = [];
                this.showDigLoad = false;
            }
        })
    }

    //selected test per reg dropdown
    selectedoptionTESTchanged(option) {
        Helpers.setLoading(true);
        this.selectedoptionTEST = option;
        this.TESTselectedbaloon = this.selectedoptionTEST + ' / Registration';
        this.dataTESTPerReg = [];
        this.analyticsRespArr.map((item) => {
            if (option == 'Consumable based test') {
                this.dataTESTPerReg.push({
                    "y": item.totalConsumableTests,
                    "x": item.totalRegistrations,
                    "value": (item.consumableTestsPerRegistration).toFixed(2),
                    "facility": item.displayName,
                    "center": item.centerName
                });
            }
            else if (option == 'Diagnostics') {
                this.dataTESTPerReg.push({
                    "y": item.totalTests,
                    "x": item.totalRegistrations,
                    "value": (item.testsPerRegistration).toFixed(2),
                    "facility": item.displayName,
                    "center": item.centerName,
                    "color": "#b22222"
                })
            }
        });
        setTimeout(() => {
            Helpers.setLoading(false);
        }, 1500);
    }

    

 
    /**
     * useranalytics
     */
    public getallvisitorLog(from,to) {
        this.analytics.getAlluserAnalytics().then((res) => {
            Helpers.setLoading(false);
            if (res!=undefined) {
                this.analyticsUsersresp=res;   
                this.analyticsUser=this.analyticsUsersresp.results;
            }else{
                return false;
            }
            
        })
        .then(()=>{
            this.visitsAndPatientCount(from,to)
        })
    }

    /**
     * useranalytics for customer admin
     */
    public getallvisitorLogCustomerAdmin(type,id,from,to) {
        this.analytics.getuserAnalyticsSearch(type,id).then((res) => {
            Helpers.setLoading(false);
            if (res!=undefined) { 
                this.analyticsUser=this.analyticsRespArr;
            }else{
                return false;
            }            
        })
        .then(()=>{
            this.visitsAndPatientCount(from,to)
        })
    }

    /**
     * visitsAndPatientCount
     */
    public visitsAndPatientCount(from , to) {
        this.analytics.getAlluserPatientCount(from,to).then((res) => {
            Helpers.setLoading(false);
            if (res!=undefined) {
                this.patientcount=res;
            }
            
        }).then(()=>{
            this.analytics.getAlluserRevisits(from,to).then((res) => {
                Helpers.setLoading(true);
                if (res!=undefined) {
                    this.visitorlog = [];
                    this.dataVisitStat=[];
                    this.visitcount=res;
                if (this.patientcount.length>this.visitcount.length) {
                    this.patientcount.map((patient)=>{
                        this.visitcount.map((visit,i)=>{
                            if (patient._id==visit._id) {
                                this.visitorlog.push({id:visit._id,patientCount:patient.patientCount,visitCount:visit.revisitCount})
                            }
                        })
                    })
                    for (let i = 0; i < this.visitcount.length; i++) {
                        if(!JSON.stringify(this.visitorlog).includes(this.visitcount[i]._id)){
                            this.visitorlog.push({id:this.visitcount[i]._id,patientCount:0,visitCount:this.visitcount[i].revisitCount})
                           
                        }                       
                    }
                    
                }
                
                else if (this.visitcount.length>this.patientcount.length) {
                    this.visitcount.map((patient)=>{
                        this.patientcount.map((visit)=>{
                            if (patient._id==visit._id) {
                                this.visitorlog.push({id:visit._id,patientCount:patient.patientCount,visitCount:visit.revisitCount})
                            }
                        })
                       
                    })
                    for (let i = 0; i < this.patientcount.length; i++) {
                        if(!JSON.stringify(this.visitorlog).includes(this.patientcount[i]._id)){
                            this.visitorlog.push({id:this.patientcount[i]._id,patientCount:this.patientcount[i].patientCount,visitCount:0})
                           
                        }                       
                    }
                }
                this.analyticsUser.map((analytic)=>{
                    this.visitorlog.map((countlog)=>{
                        if (countlog.id==analytic.id) {
                            countlog.displayName=analytic.displayName;
                        }
                    })
                })
                this.visitorlog.sort((a, b) => { return b.patientCount - a.patientCount })
            }
            this.visitorlog.map((item) => {
                if (item.displayName!=undefined) {
                    this.dataVisitStat.push({
                        "User": item.displayName,
                        "Patient Registered": item.patientCount,
                        "Re-visits": item.visitCount
                                })
                }                
            });
            console.log(this.dataVisitStat)
            })
            
        })
        this.visitorlog.sort((a, b) => { return b.patientCount - a.patientCount })
        setTimeout(() => {
            Helpers.setLoading(false);
        }, 2300);
    }
    /**
      * when clicked on apply filter button
      */
    public applyFilter() {
        Helpers.setLoading(true);
        this.from = new Date(localStorage.getItem('analyticsStartDate')).getTime();
        this.to = new Date(localStorage.getItem('analyticsEndDate')).getTime();
        this.visitsAndPatientCount(this.from, this.to);

        setTimeout(() => {
            Helpers.setLoading(false);
        }, 1500);
    }

}
