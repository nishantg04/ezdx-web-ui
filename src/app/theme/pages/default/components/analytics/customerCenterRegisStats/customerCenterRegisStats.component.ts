import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { AnalyticsService } from '../analytics.service';
import { ScriptLoaderService } from './../../../../../../_services/script-loader.service';
import { Facility } from '../../facilities/facility';
import { Center } from '../../facilities/center';
import { ActivatedRoute, Router } from '@angular/router';
import { VisitsService } from '../../visits/visits.service';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./customerCenterRegisStats.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CustomerCenterRegisStatsComponent implements OnInit {

    user: any;
    userType : any;
    constructor(private _visitsService: VisitsService, private analytics: AnalyticsService, private _script: ScriptLoaderService, private _activeroute: ActivatedRoute) {
        this.user=JSON.parse(localStorage.getItem('userInfo'));
        if (window.localStorage.getItem('userType') == 'Facility')
        this.userType = 'Facility';
        else if (window.localStorage.getItem('userType') == 'Center')
            this.userType = 'Center';
        else if (window.localStorage.getItem('userType') == 'User')
            this.userType = 'User';
        else
            this.userType = 'Admin';
     }

    public ccWisereg: any = [];
    public ccWiseregresp: any = {};
    public ccWiseregCount: number = 0;
    public visiblePagination: boolean = false;
    public pageNo: number = 1;
    public limit: number = 10;
    public ranges: any = [];
    public row: number = 10;
    public facilities: any = [];
    public centers: any = [];
    public warning: boolean = false;
    public selectedItem: string;
    public selectedcenter: string;
    public patientsTotalCount: any = 0;
    public from: any;

    ngOnInit() {
        this._activeroute.queryParams.subscribe(param => {
            this.selectedItem = param.selectedItem;
            this.selectedcenter = param.selectedcenter;
            this.from = param.from;
        });
        Helpers.setLoading(true);

        if(this.userType == 'Admin'){        
          this.gettotaldiagnosticsandcenterwisedata(this.pageNo, this.limit);
        }else{
            this.patientsTotalCount = JSON.parse(localStorage.getItem('patientcount'))
            this.funcChecking(this.pageNo,this.limit)
        }
        
        
        this.ranges = [
            { label: "10", value: 10 },
            { label: "25", value: 25 },
            { label: "50", value: 50 },
            { label: "100", value: 100 },
        ];
    }

    /**
     * getpateintcount
     */
    public gettotaldiagnosticsandcenterwisedata(page, limit) {
        this._visitsService.getDashboardCounts()
            .then(data => {
                console.log('dashboard count', data)
                this.patientsTotalCount = data.patients;
            }).then(data => {
                 this.funcChecking(page,limit);                
            })
    }

    funcChecking(page,limit){
        if(this.userType == 'Facility'){
            if (this.from=='user' && this.user.userRoles.includes('TECHNICIAN')) {
                this.getUserWiseRegistrationStatsFacility('facility',this.user.facilityId,page, limit)
            }
            if(this.from!='user' && this.user.userRoles.includes('TECHNICIAN')){
                this.getCenterWiseRegistrationStatsFacility(this.user.facilityId,page, limit)
            }
        }else if(this.userType == 'Center'){
            if (this.from=='user' && this.user.userRoles.includes('CENTER_ADMIN')) {
                this.getUserWiseRegistrationStatsFacility('center',this.user.centerId,page, limit)
            }
        }else{
            this.getCenterWiseRegistrationStats(page, limit) 
            this.getUserWiseRegistrationStats(page, limit)
        }
    }

    //get centerwise log list
    public getCenterWiseRegistrationStatsFacility(faility,page, limit): void {
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getavgdiagnosticAndConsumablebasedtestFacility(faility,page, limit).then((res) => {
            Helpers.setLoading(false);
            // console.log(res)
            if (res != undefined) {
                this.visiblePagination = true;
                this.ccWiseregresp = res;
                this.ccWisereg = this.ccWiseregresp.results;
                this.ccWiseregCount = this.ccWiseregresp.totalHits;
                this.ccWisereg.sort((a, b) => { return b.totalRegistrations - a.totalRegistrations })
            } else {
                this.ccWisereg = [];
                this.ccWiseregCount = 0;
            }
        })
    }

    //get centerwise log list
    public getCenterWiseRegistrationStats(page, limit): void {
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getavgdiagnosticAndConsumablebasedtest(page, limit).then((res) => {
            Helpers.setLoading(false);
            // console.log(res)
            if (res != undefined) {
                this.visiblePagination = true;
                this.ccWiseregresp = res;
                this.ccWisereg = this.ccWiseregresp.results;
                this.ccWiseregCount = this.ccWiseregresp.totalHits;
                this.ccWisereg.sort((a, b) => { return b.totalRegistrations - a.totalRegistrations })
            } else {
                this.ccWisereg = [];
                this.ccWiseregCount = 0;
            }
        })
    }

    //get userwise log list
    public getUserWiseRegistrationStats(page, limit): void {
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getanalyticsUserSearchPaginate(page, limit).then((res) => {
            Helpers.setLoading(false);
            // console.log(res)
            if (res != undefined) {
                this.visiblePagination = true;
                this.ccWiseregresp = res;
                this.ccWisereg = this.ccWiseregresp.results;
                this.ccWiseregCount = this.ccWiseregresp.totalHits;
                this.ccWisereg.sort((a, b) => { return b.totalRegistrations - a.totalRegistrations })
            } else {
                this.ccWisereg = [];
                this.ccWiseregCount = 0;
            }
        })
    }

    //get userwise log list
    public getUserWiseRegistrationStatsFacility(type,facility, page, limit): void {
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getanalyticsUserSearchPaginateFacility(type,facility,page, limit).then((res) => {
            Helpers.setLoading(false);
            // console.log(res)
            if (res != undefined) {
                this.visiblePagination = true;
                this.ccWiseregresp = res;
                this.ccWisereg = this.ccWiseregresp.results;
                this.ccWiseregCount = this.ccWiseregresp.totalHits;
                this.ccWisereg.sort((a, b) => { return b.totalRegistrations - a.totalRegistrations })
            } else {
                this.ccWisereg = [];
                this.ccWiseregCount = 0;
            }
        })
    }

    
    goBack(){
        window.history.go(-1);
    }


    //when pagination number changed
    paginate(event) {
        this.pageNo = event.page + 1;
        this.limit = parseInt(event.rows);
         this.funcChecking(this.pageNo,this.limit)
    }

    

    //change items per page
    select(e) {
        this.visiblePagination = false;
        this.limit = e.value;
        this.pageNo = 1;
        this.row = e.value;
        this.funcChecking(this.pageNo,this.limit)
    }

}
