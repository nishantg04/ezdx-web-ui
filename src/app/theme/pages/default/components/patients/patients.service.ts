import { Injectable } from '@angular/core';
import { Headers, Http, ResponseContentType } from '@angular/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { Helpers } from './../../../../../helpers';

import 'rxjs/add/operator/toPromise';

import { Patient } from './patient';
import { promise } from 'selenium-webdriver';
import { environment } from '../../../../../../environments/environment'

@Injectable()
export class PatientsService {

    private headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private headerspdf = new Headers({ 'Content-Type': 'application/json', 'X-Content-Type-Options': 'nosniff', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private serviceUrl2 = environment.BaseURL + environment.PatientCommandURL; //'http: //dev.ezdx.healthcubed.com/ezdx-patient-command/api/v1/patients';  // URL to web api

    private serviceUrl = environment.BaseURL + environment.PatientQueryURL;//'http://dev.ezdx.healthcubed.com/ezdx-patient-query/api/v1/patients';
    private serviceTestUrl = environment.BaseURL + environment.VisitTestURL;// 'http://dev.ezdx.healthcubed.com/ezdx-visit-query/api/v1/tests';
    private patientCountUrl = environment.BaseURL + environment.PatientQueryURL + '/count/date';
    private patientMrnUrl = environment.BaseURL + environment.patientsSvrMrn;
    private patientSearchbyUser = environment.BaseURL + environment.patientSearch;
    private patientMrnUserUrl = environment.BaseURL + environment.PatientQueryURL;
    private patientCenterSearchByUser = environment.BaseURL + environment.patientFacSearch;

    constructor(private http: Http, private _router: Router, private _script: ScriptLoaderService) { }

    getPdf(patientsIds: string[], creatorId: string, creatorName: string): any {
        var putUrl = this.serviceUrl2 + "/healthcard";
        var bodyobj = { "operatorId": creatorId, "operatorName": creatorName, "patientIds": patientsIds }
        return this.http.put(putUrl, bodyobj, { headers: this.headerspdf, responseType: ResponseContentType.Blob })
            .toPromise()
            .then(response => {
                console.log("status : ", response.statusText);
                return (response.status === 201) ? new Blob([response.blob()], { type: 'application/pdf' }) : response.statusText;
            })
            .catch(this.handleError.bind(this));
    }
    getPatients(fromDate, toDate, searchcriteria, id): Promise<Patient[]> {
        var getUrl = this.serviceUrl + "/search/date?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/search/date?searchcriteria=FACILITY&facility=" + id + "&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/search/date?searchcriteria=CENTER&center=" + id + "&from=" + fromDate + "&to=" + toDate;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json().patients as Patient[] : [] })
            .catch(this.handleError.bind(this));
    }

    getPatientsList(fromDate, toDate, searchcriteria, id, pageNo, limit): Promise<any> {
        var getUrl = this.serviceUrl + "/search/date?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate + "&page=" + pageNo + "&limit=" + limit;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/search/date?searchcriteria=FACILITY&facility=" + id + "&from=" + fromDate + "&to=" + toDate + "&page=" + pageNo + "&limit=" + limit;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/search/date?searchcriteria=CENTER&center=" + id + "&from=" + fromDate + "&to=" + toDate + "&page=" + pageNo + "&limit=" + limit;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status < 400) ? response.json() : [] })
            .catch(this.handleError.bind(this));
    }

    getTotalPatientCount(): Promise<any> {
        return this.http.get(this.serviceUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json().count })
            .catch(this.handleError.bind(this));
    }

    getPatientById(patientId: String): Promise<any> {
        let url = this.serviceUrl + "/" + patientId;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getPatientCountDashboard(): Promise<any> {
        return this.http.get(this.patientCountUrl + "?searchcriteria=ALL&from=0&to=0", { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    getPatientFaclityCount(searchby, id): Promise<any> {
        var url;
        if (searchby == 'FACILITY')
            url = this.patientCountUrl + "?searchcriteria=FACILITY&facility=" + id + "&from=0&to=0"
        else
            url = this.patientCountUrl + "?searchcriteria=CENTER&center=" + id + "&from=0&to=0"
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }


    getPatientCount(fromDate, toDate, searchcriteria, id): Promise<number> {
        var getUrl = this.serviceUrl + "/search/date?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/search/date?searchcriteria=FACILITY&facility=" + id + "&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/search/date?searchcriteria=CENTER&center=" + id + "&from=" + fromDate + "&to=" + toDate;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json().count : 0 })
            .catch(this.handleError.bind(this));
    }

    getPatientFilterCount(fromDate, toDate, searchcriteria, facilityId, centerId): Promise<any> {
        var getUrl = this.patientCountUrl + "?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "FACILITY")
            getUrl = this.patientCountUrl + "?searchcriteria=FACILITY&facility=" + facilityId + "&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "CENTER")
            getUrl = this.patientCountUrl + "?searchcriteria=CENTER&center=" + centerId + "&facility=" + facilityId + "&from=" + fromDate + "&to=" + toDate;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() : 0 })
            .catch(this.handleError.bind(this))
    }

    getPatientCenterCount(fromDate, toDate, searchcriteria, facilityid, centerid): Promise<any> {
        var getUrl = this.patientCountUrl + "?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "FACILITY")
            getUrl = this.patientCountUrl + "?searchcriteria=FACILITY&facility=" + facilityid + "&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "CENTER")
            getUrl = this.patientCountUrl + "?searchcriteria=CENTER&facility=" + facilityid + "&center=" + centerid + "&from=" + fromDate + "&to=" + toDate;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() : 0 })
            .catch(this.handleError.bind(this))
    }

    getTestCountByMonth(searchcriteria, facilityid, centerid): Promise<any[]> {
        var getUrl = this.serviceTestUrl + "/count/month?searchcriteria=ALL";
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceTestUrl + "/count/month?searchcriteria=FACILITY&facilityid=" + facilityid;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceTestUrl + "/count/month?searchcriteria=CENTER&facilityid=" + facilityid + "&centerid=" + centerid;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any[] : [] })
            .catch(this.handleError.bind(this));
    }

    getPatientCountByMonth(searchcriteria, facilityid, centerId): Promise<any[]> {
        var getUrl = this.serviceUrl + "/count/search?searchcriteria=ALL";
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/count/search?searchcriteria=FACILITY&facilityid=" + facilityid;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/count/search?searchcriteria=CENTER&facilityid=" + facilityid + "&centerid=" + centerId;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any[] : [] })
            .catch(this.handleError.bind(this));
    }

    getPatientStatisticsForAge(searchcriteria, facilityid, centerId): Promise<any[]> {
        // console.log("Params:: Search Criteria: ", searchcriteria, " facility: ", facilityid, " center: ", centerId)

        let getUrl = this.serviceUrl + "/stats/age?searchcriteria=";
        if (searchcriteria === "CENTER") {
            getUrl = getUrl + searchcriteria + "&facility=" + facilityid + "&center=" + centerId;
        } else if (searchcriteria === "FACILITY") {
            getUrl = getUrl + searchcriteria + "&facility=" + facilityid;
        } else {
            getUrl = getUrl + searchcriteria;
        }

        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any[] : [] })
            .catch(this.handleError.bind(this));
    }
    getPatientStatisticsForGender(searchcriteria, facilityid, centerId): Promise<any> {
        var getUrl = this.serviceUrl + "/stats/gender?searchcriteria=ALL";
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/stats/gender?searchcriteria=FACILITY&facility=" + facilityid;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/stats/gender?searchcriteria=CENTER&facility=" + facilityid + "&center=" + centerId;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any : {}; })
            .catch(this.handleError.bind(this));
    }

    getPatientStatsForGenderByFilter(searchcriteria, fromDate, toDate, facilityid, centerId): Promise<any> {
        var getUrl = this.serviceUrl + "/stats/gender?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate;;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/stats/gender?searchcriteria=FACILITY&from=" + fromDate + "&to=" + toDate + "&facility=" + facilityid;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/stats/gender?searchcriteria=CENTER&from=" + fromDate + "&to=" + toDate + "&facility=" + facilityid + "&center=" + centerId;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getPatientStatsForAgeByFilter(searchcriteria, fromDate, toDate, facilityid, centerId): Promise<any> {
        var getUrl = this.serviceUrl + "/stats/age?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate;;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/stats/age?searchcriteria=FACILITY&from=" + fromDate + "&to=" + toDate + "&facility=" + facilityid;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/stats/age?searchcriteria=CENTER&from=" + fromDate + "&to=" + toDate + "&facility=" + facilityid + "&center=" + centerId;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getPatientCountByWeek(searchcriteria, fromDate, toDate, facilityid, centerId): Promise<any[]> {
        var getUrl = this.serviceUrl + "/count/week?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/count/week?searchcriteria=FACILITY&facilityid=" + facilityid + '&from=' + + fromDate + "&to=" + toDate;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/count/week?searchcriteria=CENTER&facilityid=" + facilityid + "&centerid=" + centerId + '&from=' + + fromDate + "&to=" + toDate;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any[] : [] })
            .catch(this.handleError.bind(this));
    }

    getPatientCountByWeekForUser(searchcriteria, fromDate, toDate, id,): Promise<any[]> {
        var getUrl;
        if (searchcriteria == "USER")
           getUrl = this.serviceUrl + "/count/week?searchcriteria=USER&operator=" + id + '&from=' + + fromDate + "&to=" + toDate;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any[] : [] })
            .catch(this.handleError.bind(this));
    }

    getPatientCountByTril(searchcriteria, fromDate, toDate, facilityid, centerId): Promise<any[]> {
        var getUrl = this.serviceUrl + "/count/week?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/count/week?searchcriteria=FACILITY&from=" + fromDate + "&to=" + toDate + "&facilityid=" + facilityid;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/count/week?searchcriteria=CENTER&from=" + fromDate + "&to=" + toDate + "&facilityid=" + facilityid + "&centerid=" + centerId;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any[] : [] })
            .catch(this.handleError.bind(this));
    }

    getPatientList(searchcriteria: string, facilityid: string, centerId: string, operatorId: string, page: number, limit: number): Promise<any> {
        var getUrl = this.serviceUrl + "/search?searchcriteria=ALL&page=" + page + "&limit=" + limit;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/search?searchcriteria=FACILITY&facility=" + facilityid + "&page=" + page + "&limit=" + limit;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/search?searchcriteria=CENTER&facility=" + facilityid + "&center=" + centerId + "&page=" + page + "&limit=" + limit;
        if (searchcriteria == "OPERATOR")
            getUrl = this.serviceUrl + "/search?searchcriteria=OPERATOR&operator=" + operatorId + "&page=" + page + "&limit=" + limit;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                return (response.status === 200) ? response.json() as any[] : [];
            })
            .catch(this.handleError.bind(this));
    }

    getPatientByMrn(searchcriteria: String, mrn: String, pageNo: Number, limit: Number): Promise<any> {
        let url = this.serviceUrl + '/search?searchcriteria=' + searchcriteria + "&mrn=" + mrn + "&page=" + pageNo + "&limit=" + limit;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(res => {
                return res.status < 400 ? res.json() : [];
            })
            .catch(this.handleError.bind(this));
    }
    getPatientOfMrnByCenter(facilityId: String, centerId: String, mrn: String, pageNo, limit): Promise<any> {
        let url = this.patientMrnUrl;
        if (centerId != null) {
            url = url + 'facility/' + facilityId + '/center/' + centerId + '/patients/search?searchcriteria=MRN&mrn=' + mrn + '&facility=' + facilityId + '&center=' + centerId + "&page=" + pageNo + "&limit=" + limit;
        } else {
            url = url + 'facility/' + facilityId + '/patients/search?searchcriteria=MRN&mrn=' + mrn + '&facility=' + facilityId + "&page=" + pageNo + "&limit=" + limit;
        }
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(res => { return res.status < 400 ? res.json() : ''; })
            .catch(this.handleError.bind(this));
    }

    getPatientMrnByUser(id,mrn,pageNo,limit){
        let url = this.patientMrnUserUrl + '/search?searchcriteria=MRN_OPR&operator='+ id + "&mrn=" + mrn + "&page=" + pageNo + "&limit=" + limit;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getAllPatients(): Promise<any> {
        var url = this.serviceUrl + "/search?searchcriteria=ALL";
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getPatientByUserAll(id, page, limit) {
        var url = this.patientSearchbyUser + "?searchcriteria=OPERATOR&operator=" + id + "&page=" + page + "&limit=" + limit
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getPatientByUserFacCenter(id, facilityId, centerId, page, limit) {
        // GET /api/v1/facility/{facilityId}/center/{centerId}/patients/search
        var url;
        if (facilityId != 'ALL' && centerId == 'ALL')
            url = this.patientCenterSearchByUser + "/facility/" + facilityId + "/patients/search?searchcriteria=OPERATOR" + "&operator=" + id + "&facilityId="+ facilityId + "&page=" + page + "&limit=" + limit
        else
            url = this.patientCenterSearchByUser + "/facility/" + facilityId + "/center/" + centerId + "/patients/search" + "?searchcriteria=OPERATOR&operator=" + id + "&page=" + page + "&limit=" + limit
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load('body',
                'assets/demo/default/custom/components/utils/redirect-cidaas-logout.js');
        }
        return Promise.reject(error.message || error);
    }
}