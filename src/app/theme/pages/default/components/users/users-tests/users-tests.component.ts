import {
    Component,
    OnInit,
    ViewEncapsulation,
    AfterViewInit
} from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Helpers } from "../../../../../../helpers";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";

import { User } from "../user";
import { Facility } from "../../facilities/facility";
import { Center } from "../../facilities/center";
import { FacilitiesService } from "../../facilities/facilities.service";
import { UsersService } from "../users.service";
import { VisitsService } from "../../visits/visits.service";
import { TestsService } from "./tests.service";
import { concat } from "rxjs/operators/concat";

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./users-tests.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class UsersTestsComponent implements OnInit, AfterViewInit {
    isCentersListEmpty: boolean = false;
    users: any[];
    allUsers: any[];
    facilityUser: any[];
    facilities: Facility[] = [];
    selectedFacility: Facility;
    selectedCenter: Center;
    centers: Center[] = [];
    testList: any;
    tests: any[];
    checkAll: boolean;
    checkAllTest: boolean;
    multi: boolean;
    canMultiselect: boolean;
    showTests: boolean;
    sortTypeTest: any;
    sortTest: any = false;
    sortType: any;
    newtests: any[];
    sort: any;
    msg: any;
    userSelect: string;
    textBody: boolean = false;
    textTemplate: boolean = true;
    userTestMappedCount: string;
    alltestList: any[]
    roles: any;
    pageNo: any;
    pageSize: any;
    userCount: any;
    dummyArray: any[] = [];
    idList: any[] = [];
    selectUser: any;
    selectLabel: boolean = false;
    userRowCount: any;
    dummyId: any[] = [];
    testCount: any[] = [];
    chosen: any;
    visiblePagination : boolean = false;
    constructor(
        private _usersService: UsersService, private _facilitiesService: FacilitiesService, private _visitsService: VisitsService,
        private _script: ScriptLoaderService, private _router: Router, private _activeroute: ActivatedRoute, private _testsService: TestsService
    ) {
        this.selectUser = null;
        this.tests = [];
        this.sortTypeTest = "testName";
        this.sortTest = false;
        this.sortType = "firstName";
        this.sort = false;
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        this.allUsers = [];
        this.multi = true;
        this.showTests = true;
        this.pageNo = 1;
        this.pageSize = 10;
        this.userRowCount = 10;
        this.roles = [
            { name: 'HC Admin', type: 'HC_ADMIN' },
            { name: 'HC Operations', type: 'HC_OPERATIONS' },
            { name: 'HC Manager', type: 'HC_MANAGER' },
            { name: 'Operator', type: 'OPERATOR' },
            { name: 'Doctor', type: 'DOCTOR' },
            { name: 'Center Admin', type: 'CENTER_ADMIN' },
            { name: 'Technician', type: 'TECHNICIAN' },
            { name: 'Clinical User', type: 'CLINICAL_USER' },
            { name: 'Pathologist', type: 'PATHOLOGIST' }
        ];
    }
    ngOnInit() { }
    ngAfterViewInit() {
        //this.list();
        //this.getFacilities();
        this.getAllTests();

    }
    oncheckAll(): void {
        this.checkAll = !this.checkAll;
        this.facilityUser.forEach((user, i) => {
            user.selected = this.checkAll;
        });
    }

    testChecked() {
        let count = 0;
        this.newtests.forEach(test => {
            if (test.selected) count++;
        })
        if (this.newtests.length === count) {
            this.checkAllTest = true;
        } else {
            this.checkAllTest = false;
        }
    }

    back() {
        this.pageNo = 1;
        this.pageSize = 10;
        document.getElementById('checksum_id').style.display = 'block'
        document.getElementById('drag_drop').style.display = 'none'
        document.getElementById('next_map').style.display = 'none'
        document.getElementById('step_one').style.backgroundColor = '#ffb822'
        document.getElementById('step_two').style.backgroundColor = 'gray'
        document.getElementById('step_three').style.backgroundColor = 'gray'
        // document.getElementById('step_four').style.backgroundColor = 'gray'
        this.userList('CENTER', this.selectedCenter.id, this.pageNo, this.pageSize)
    }

    paginate(event) {
        // console.log('event', event)
        this.pageNo = event.page + 1;
        this.pageSize = event.rows;
        var searchcriteria = "CENTER";
        this.userList(searchcriteria, this.selectedCenter.id, this.pageNo, this.pageSize)
    }

    getAllTests() {
        this.newtests = [];
        this._testsService.getAllTests().then(res => {
            this.alltestList = res;
            this.alltestList.forEach(test => {
                if (test.active) {
                    this.newtests.push(test)
               }
            })
            this.newtests.forEach(test => {
                test.testIconUrl = "./assets/app/media/img/test/" + test.testName + ".png"
            })
            this.newtests.forEach(test => {
                if (test.testName.includes('_')) {
                    test.testName = test.testName.replace('_', ' ')
                }
                if (test.testName.includes('_')) {
                    test.testName = test.testName.replace('_', ' ')
                }
                if (test.testName == 'URINE') {
                    test.testName = 'URINE 2P'
                }
                if (test.testName == 'DENGUE') {
                    test.testName = 'DENGUE ANTIGEN'
                }
                if (test.testType.includes('_')) {
                    test.testType = test.testType.replace('_', ' ')
                }
                if (test.testType.includes('_')) {
                    test.testType = test.testType.replace('_', ' ')
                }
            })
            this.newtests.forEach(element => {
                element.selected = false;
            });
        });
    }

    getFacilities(): void {
        var facilities = JSON.parse(localStorage.getItem('facilityTable'))
        // facilities.forEach(facility => {
        //     this.facilities.push({ label: facility.name, value: facility });
        // });
        // this.facilities.sort(function(a, b) {
        //     if (a.label > b.label) {
        //         return 1;
        //     } else if (a.label < b.label) {
        //         return -1;
        //     }
        //     return 0;
        // });
    }

    onOrgChange(facility): void {
        this.checkAll = false;
        this.checkAllTest = false;
        this.canMultiselect = true;
        this.centers = [];
        this.facilityUser = [];
        this.isCentersListEmpty = true;
        this.newtests.forEach(element => (element.selected = false));
        // if (this.selectedFacility.centers.length > 0) {
        //     this.selectedFacility.centers.forEach(center => {
        //         this.centers.push({ label: center.name, value: center });
        //     });
        //     this.isCentersListEmpty = false;
        //     this.centers.sort(function(a, b) {
        //         return a.label > b.label ? 1 : b.label > a.label ? -1 : 0;
        //     });
        // }
        // this.selectedCenter = this.centers[0].value;
        document.getElementById('step_one').style.backgroundColor = '#34bfa3'
        document.getElementById('step_two').style.backgroundColor = '#ffb822'
        this.userList("CENTER", this.selectedCenter.id, this.pageNo, this.pageSize)
        // if (this.isCentersListEmpty) {
        //     this.allUsers.forEach(user => {
        //         user.selected = false;
        //         if (user.facilityId === this.selectedFacility.id) this.facilityUser.push(user);
        //         if (user.testMapping.length > 0) this.canMultiselect = false;
        //     });
        // } else {
        //     this.allUsers.forEach(user => {
        //         user.selected = false;
        //         if (this.selectedCenter.id === user.centerId) this.facilityUser.push(user);
        //         if (user.testMapping.length > 0) this.canMultiselect = false;
        //     })
        // }
    }

    // this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedCenter.id, this.pageNo, this.pageSize);

    userList(searchcriteria, centerId, pageno, size) {
        this.facilityUser = [];
        this._usersService.getUsersFiltered(0, 0, searchcriteria, centerId, pageno, size, '', 'asc')
            .then(users => {
                if(users){
                    this.facilityUser = users.users;
                    this.userCount = users.count.toString();
                }else{
                    this.facilityUser = [];
                    this.userCount = '0'
                }
               
            })
            .then(() => {
                this.getCustomerCenterRole();
            });

    }

    onCenterChange(): void {
        this.checkAll = false;
        this.checkAllTest = false;
        this.canMultiselect = true;
        this.newtests.forEach(element => (element.selected = false));
        this.userList("CENTER", this.selectedCenter.id, this.pageNo, this.pageSize)
        // this.facilityUser = [];
        // this.allUsers.forEach(user => {
        //     user.selected = false;
        //     if (user.centerId === this.selectedCenter.id) {
        //         this.facilityUser.push(user);
        //         if (user.testMapping.length > 0 && user.centerId === this.selectedCenter.id) this.canMultiselect = false;
        //     }
        // });
        /*         } else {
                    this.facilityUser = [];
                    this.allUsers.forEach(user => {
                        user.selected = false;
                        if (user.facilityId === this.selectedFacility.id) {
                            this.facilityUser.push(user);
                            if (user.testMapping.length > 0 && user.centerId === this.selectedCenter.id) this.canMultiselect = false;
                        }
                    });
                } */
    }


    onTestMap(): void {
        this.users.forEach(user => {
            var userId = null;
            if (user.selected) userId = user.id;
            var selectedTests = [];
            this.newtests.forEach(test => {
                if (test.selected) selectedTests.push(test.id);
            });
            selectedTests = this.idList;
            if (userId !== null) {
                this.userSelect = null;
            } else {
                this.userSelect =
                    "CHOOSE_THE_USER_AND_THEN_TRY_TO_ASSIGN/UNASSIGN_TEST";
            }
            if (userId) {
                this.userSelect = null;
                Helpers.setLoading(true);
                this._usersService
                    .updateTestMap("set", userId, selectedTests)
                    .then(() => {
                        this.userSelect = null;
                        this.users.forEach(user => {
                            if (user.id === userId) user.testMapping = selectedTests;
                        });
                        this.msg = "TEST_MAPPED_SUCCESSFULLY";
                        setTimeout(() => {
                            this.msg = "";
                        }, 5000);
                        Helpers.setLoading(false);
                    });
            }
        });
        this.canMultiselect = true;
        this.allUsers.forEach(user => {
            if (user.centerId === this.selectedCenter.id && user.testMapping.length > 0) this.canMultiselect = false;
        })
    }

    next() {
        document.getElementById('checksum_id').style.display = 'none'
        document.getElementById('drag_drop').style.display = 'block'
        document.getElementById('step_three').style.background = '#ffb822'
    }

    selectingUser(Suser, event) {


        this.facilityUser.forEach(user => {
            if (event.target.checked == true) {
                document.getElementById('next_map').style.display = 'block'
                document.getElementById('step_two').style.backgroundColor = '#34bfa3'
            } else {
                document.getElementById('next_map').style.display = 'none'
                document.getElementById('step_two').style.backgroundColor = '#ffb822'
            }
            if (event.target.checked) {
                if (user.id === Suser.id) {
                    user.selected = event.target.checked;

                } else {
                    user.selected = false;
                }
            } else {
                user.selected = false;
            }
        });

        this.checkAllTest = false;
        this.newtests.forEach(test => (test.selected = false));

        if (Suser.selected) {
            this.userTestMappedCount = Suser.testMapping.length;
            Suser.testMapping.forEach(element => {
                this.newtests.forEach(test => {
                    if (element === test.id) {
                        test.selected = true;
                    }
                });
            });
        }
        this.selectUser = Suser;
        
        if (this.newtests.length == this.selectUser.testMapping.length && this.selectUser.testMapping) {
            this.checkAllTest = true;
            this.selectLabel = true
        } else {
            this.selectLabel = false;
            this.checkAllTest = false;
        }

    }

    sTest(testvalue) {
        testvalue.selected = !testvalue.selected;
        this.newtests.forEach(test => {
            if (test.id == testvalue.id) {
                test.selected = testvalue.selected;
            }
        })
    }

    //not in use
    allTest(event) {
        this.checkAllTest = event.target.checked;
        if (this.checkAllTest == true) {
            this.newtests.forEach(test => {
                test.selected == true
            })
        }
        if (this.checkAllTest == false) {
            this.newtests.forEach(test => {
                test.selected == false
            })
        }
    }

    checkSave() {
        var arrayV = []
        this.selectUser.testMapping = [];
        this.newtests.forEach(test => {
            if (test.selected == true) {
                arrayV.push(test.id)
            }
        })
        this.selectUser.testMapping = arrayV;
        if (this.selectUser.testMapping.length != this.newtests.length) {
            this.checkAllTest = false;
        } else {
            this.checkAllTest = true;
        }
        document.getElementById('step_three').style.backgroundColor = '#34bfa3'

        this._usersService.updateTestMap("set", this.selectUser.id, this.selectUser.testMapping)
            .then(() => {
                //   document.getElementById('step_four').style.backgroundColor = '#34bfa3'
                this.msg = "TEST_MAPPED_SUCCESSFULLY";

                setTimeout(() => {
                    this.msg = null;
                    this.back();
                }, 1000);
                Helpers.setLoading(false);
            });
    }

    oncheckAllTest(event): void {
        this.selectUser.testMapping = [];
        this.userSelect = null;
        this.checkAllTest = event.target.checked;
        this.newtests.forEach((test, i) => {
            test.selected = event.target.checked;
            if (test.selected == true)
                this.dummyArray.push(test.id)
            if (test.selected == false) {
                var index = this.dummyArray.indexOf(test.id);
                this.dummyArray.splice(index, 1);
            }
        });
        this.idList = this.dummyArray;
        if (this.idList.length != 0)
            document.getElementById('step_three').style.backgroundColor = '#ffb822'
        else
            document.getElementById('step_three').style.backgroundColor = '#ffb822'
        //  document.getElementById('step_four').style.backgroundColor = 'gray'    
    }

    //not in use

    onTestCheck(test): void {
        this.userSelect = null;
        test.selected = !test.selected;
        let allTestsSelected = true;
        this.tests.forEach((test, i) => {
            if (!test.selected) allTestsSelected = false;
        });
        if (allTestsSelected) this.checkAllTest = true;
        else this.checkAllTest = false;
    }

    //not in use
    getTests(): void {
        Helpers.setLoading(true);
        var testArray = [];
        this._visitsService
            .getTests()
            .then(tests => (testArray = tests))
            .then(() => {
                testArray.forEach(test => {
                    this.testList.forEach(test2 => {
                        if (test.testType.toLowerCase() === test2.type.toLowerCase()) {
                            test2.tests.forEach(test3 => {
                                if (test.testName.toLowerCase() === test3.type.toLowerCase()) {
                                    test3.testId = test.id;
                                }
                            });
                        }
                    });
                });
                this.testList.forEach(type => {
                    type.tests.forEach(test => {
                        this.tests.push(test);
                    });
                });
                this.tests.sort(function(a, b) {
                    return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
                });
                Helpers.setLoading(false);
            });
    }

    //not in use
    list(): void {
        Helpers.setLoading(true);
        this.facilityUser = [];
        this._usersService
            .getUsers()
            .then(users => (this.users = users))
            .then(() => {
                this.users.sort(function(a, b) {
                    return a.firstName > b.firstName
                        ? 1
                        : b.firstName > a.firstName ? -1 : 0;
                });
                this.users.forEach(user => {
                    if (user.facilityId) {
                        if (user.facilityId == this.selectedFacility.id) {
                            user.selected = false;
                            this.facilityUser.push(user);
                        }
                    }
                    this.allUsers.push(user);
                });
                this.getTests();
                this.getCustomerCenterRole();
                Helpers.setLoading(false);
            });
    }

    getCustomerCenterRole() {

        this.facilityUser.forEach(user => {
            user.userRoles.sort(function(a, b) {
                if (a > b) { return -1; } else if (a < b) { return 1; } return 0;
            });

            var userrole = user.userRoles.toString();

            user.userRoles.forEach(role => {
                this.roles.forEach(element => {
                    if (role == element.type) {
                        var index = user.userRoles.indexOf(role)
                        user.userRoles[index] = element.name;
                        //user.userRoles.toString().replace(user.userRoles.toString(), element.name)   
                        //// console.log('role', user.userRoles[role])                              
                    }
                });
            });
        });
        var dataJSONArray = this.facilityUser;

        var roleString = '';
        for (var j = 0; j < dataJSONArray.length; j++) {
            roleString = '';
            if (dataJSONArray[j].userRoles) {
                for (var i = 0; i < dataJSONArray[j].userRoles.length; i++) {
                    if (i != 0)
                        roleString += ', ' + dataJSONArray[j].userRoles[i];
                    else
                        roleString = dataJSONArray[j].userRoles[i];
                }
            }
            dataJSONArray[j].roleString = roleString;
            // console.log(dataJSONArray[j].roleString)
        }
        this.facilityUser = dataJSONArray;

        Helpers.setLoading(false);

    }

    facilityChange(value) {
        this.centers = [];
        this.selectedFacility = value;
        this.selectedFacility.organizationCode = value.organizationCode;
        this.centers = value.centers;
        if (this.selectedFacility.centers) {
            this.centers = this.selectedFacility.centers;
            this.selectedCenter.id = this.centers[0].id;
        } else {
            this.centers = null;
            this.selectedCenter.id = null;
        }
        document.getElementById('step_one').style.backgroundColor = '#34bfa3'
        document.getElementById('step_two').style.backgroundColor = '#ffb822'
        this.userList("CENTER", this.selectedCenter.id, this.pageNo, this.pageSize)
    }

    pageOne(){
        this.pageNo=1;
        this.visiblePagination = false;       
        setTimeout(() => this.visiblePagination = true, 0);
    }



}
