//== Class definition

var BootstrapDatepicker = function () {
    var today = new Date();
    var lastDateOfMonth = new Date(today.getFullYear(), today.getMonth() + 1, 1);
    // console.log("Month last date: ", lastDateOfMonth);
    //== Private functions
    var demos = function () {
        // minimum setup
        var datePicker = $('#consumable_expirydate').datepicker({
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            todayBtn: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            },
            startDate: lastDateOfMonth,
            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months"
        }).on('changeDate', function (e) {
            window.localStorage.setItem('consumableExpiryDate', e.date);
        });
        if (window.localStorage.getItem('consumableExpiryDate'))
            $("#consumable_expirydate").datepicker("update", new Date(window.localStorage.getItem('consumableExpiryDate')));
        else
            $("#consumable_expirydate").datepicker("update", new Date());

        }

    return {
        // public functions
        init: function () {
            demos();
        }
    };
}();

jQuery(document).ready(function () {
    BootstrapDatepicker.init();
});