import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { UsersTestsComponent } from './users-tests.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { UsersService } from '../users.service';
import { OrderModule } from 'ngx-order-pipe';
import { VisitsService } from '../../visits/visits.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { TestsService } from '../users-tests/tests.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { DropdownModule, DataTableModule, SharedModule, PaginatorModule } from 'primeng/primeng';
import { FacDropdownModule } from './../../../../../fac-dropdown/fac-dropdown.module'

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": UsersTestsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule, DataTableModule, SharedModule, PaginatorModule,
        TranslateModule.forChild({ isolate: false }), DropdownModule, FacDropdownModule
    ], exports: [
        RouterModule
    ], declarations: [
        UsersTestsComponent
    ],
    providers: [
        UsersService,
        VisitsService,
        TestsService,
        FacilitiesService,
        TranslateService
    ]

})
export class UsersTestsModule { }