
export class Address {
    address: string = "";
    city: string = "";
    country: string = "";
    district: string = "";
    postcode: string = "";
    state: string = "";
}