//== Class definition

var BootstrapDatepicker = function () {

    //== Private functions
    var demos = function () {
        // minimum setup
        var datePicker = $('#device_activationdate').datepicker({
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true,
            todayBtn: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }).on('changeDate', function (e) {
            window.localStorage.setItem('deviceActivationDate', e.date);
        });
        if (window.localStorage.getItem('deviceActivationDate'))
            $("#device_activationdate").datepicker("update", new Date(window.localStorage.getItem('deviceActivationDate')));
        else
            $("#device_activationdate").datepicker("update", new Date());
    }

    return {
        // public functions
        init: function () {
            demos();
        }
    };
}();

jQuery(document).ready(function () {
    BootstrapDatepicker.init();
});