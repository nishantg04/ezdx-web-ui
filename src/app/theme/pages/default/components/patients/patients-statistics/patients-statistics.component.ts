import { Component, OnInit, ViewEncapsulation, AfterViewInit } from "@angular/core";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";
import { PatientsService } from "../patients.service";
import { SelectItem } from "primeng/primeng";
import { FacilitiesService } from "../../facilities/facilities.service";
import { Facility } from "../../facilities/facility";
import { Helpers } from "../../../../../../helpers";
import { Center } from "../../facilities/center";
import { Input } from "@angular/core/src/metadata/directives";
import { PACKAGE_ROOT_URL } from "@angular/core/src/application_tokens";
import { validateConfig } from "@angular/router/src/config";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UserChild } from '../../users/userChild';
import { UsersService } from '../../users/users.service';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./patients-statistics.component.html",
    encapsulation: ViewEncapsulation.None
})
export class PatientsStatisticsComponent implements OnInit, AfterViewInit {
    isAdmin: boolean = false;
    showPiechart: boolean;
    selectedFacility: Facility;
    selectedCenter: Center;
    centers: Center[];
    userInfo: any;
    user: UserChild;
    searchCriteria: string = "ALL";
    facilities: Facility[];
    chosen: any;
    isFirstTimeStatFetched: boolean = false;
    userType: string;
    genderArray: any[];
    ageArray: any[];
    ageJson: any;
    lessThanfive: any;
    fiveTotwelve: any;
    twelveTofourty: any;
    fourtyToSixty: any;
    aboveSixty: any;
    private chart: any;
    ageList: any[] = [];
    hcOperations: boolean = false;
    hideFilter: boolean = true;

    constructor(private _script: ScriptLoaderService, private _patientsService: PatientsService, private _router: Router,
         private _facilitiesService: FacilitiesService, private _activeroute: ActivatedRoute, private usersService : UsersService
    ) {
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
    }

    ngOnInit(): void {
        this.getFacilities();
        this.getView();
    }

    ngAfterViewInit(): void { }

    getView() {
        Helpers.setLoading(true);
        if (this.selectedFacility.id != "ALL") {
            this.facilities.forEach(fac => {
                if (fac.id === this.selectedFacility.id) {
                    this.centers = fac.centers;
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                }
            });
        }
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/datatables/base/date.patients.js');

        this.user = JSON.parse(window.localStorage.getItem('userInfo'))
        if (this.user && this.user.userRoles) {
            let isAdmin = false;
            this.user.userRoles.forEach((role, i) => {
                if (role === "HC_ADMIN") {
                    isAdmin = true;
                }
                if (role === "HC_OPERATIONS") {
                    this.hcOperations = true;
                    isAdmin = true;
                }
            });
            if (window.localStorage.getItem('userType') == 'Facility') {
                this.userType = 'Facility';
                this.filterReports();
            }
            else if (window.localStorage.getItem('userType') == 'Center') {
                this.selectedCenter.id = this.user.centerId;
                this.selectedFacility.id = this.user.facilityId;
                this.selectedFacility.organizationCode = this.user.facilityCode;
                this.hideFilter = false;
                this.userType = 'Center';
                this.filterReports();
            }else if (window.localStorage.getItem('userType') == 'User') {
                this.userType = 'User';
                this.hideFilter = false;
                this.userReport();
            }
            else {
                this.userType = 'Admin';
                this.getFilterPatientGenderCount();
                this.getFilterPatientAgeCount();
            }
        }
        else {
            this._router.navigate(['/login']);
        }
        Helpers.setLoading(false);

    }

    getFacilities() {
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        // console.log('fac', this.facilities)
        this.facilities.sort(function(a, b) { if (a.name > b.name) { return 1; } else if (a.name < b.name) { return -1; } return 0; });
        let allFacility = new Facility();
        allFacility.name = "ALL";
        allFacility.id = "ALL";
        this.facilities.splice(0, 0, allFacility);
        let searchcriteria = "ALL";
        if (window.localStorage.getItem('userType') == 'Facility') {
            // console.log('type', window.localStorage.getItem('userType'))
            this.selectedFacility.id = this.userInfo.facilityId;
            this.facilities.forEach(fclt => {
                if (fclt.id == this.userInfo.facilityId) {
                    this.centers = jQuery.extend(true, [], fclt.centers);
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                    this.selectedCenter.id = this.centers[0].id;
                }
            });
        }
        else {
            this.selectedFacility.id = this.facilities[0].id;
            this.centers = this.facilities[0].centers;
            this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
        }

    }

    filterReports() {
        var searchcriteria;
        if (this.selectedFacility.id == "ALL") {
            searchcriteria = "ALL"
        }
        if (this.selectedFacility.id != "ALL" && this.selectedCenter.id == "ALL") {
            searchcriteria = "FACILITY"
        }
        if (this.selectedCenter.id != "ALL") {
            searchcriteria = "CENTER"
        }
        this.getFilterPatientGenderCount();
        this.getFilterPatientAgeCount();
    }

    getFilterPatientGenderCount() {
        var searchcriteria;
        var tempArray;
        this.genderArray = [];
        if (this.selectedFacility.id == "ALL" && this.selectedCenter.id == undefined) {
            searchcriteria = "ALL"
        }
        if (this.selectedFacility.id != "ALL" && this.selectedCenter.id == "ALL") {
            searchcriteria = "FACILITY"
        }
        if (this.selectedCenter.id != "ALL" && this.selectedCenter.id != undefined) {
            searchcriteria = "CENTER"
        }
        this._patientsService.getPatientStatsForGenderByFilter(searchcriteria, 0,
            0, this.selectedFacility.id, this.selectedCenter.id).then(data => {
                tempArray = data;
                this.genderArray.push({ 'title': 'Male', 'value': tempArray.male, "color": "#465EEB" },
                    { 'title': 'Female', 'value': tempArray.female, "color": "#21C4DF" },
                    { 'title': 'Others', 'value': tempArray.other, "color": "#F2F3F8" }
                )
            })
    }

    getFilterPatientAgeCount() {
        this.lessThanfive = 0;
        this.fiveTotwelve = 0;
        this.twelveTofourty = 0;
        this.fourtyToSixty = 0;
        this.aboveSixty = 0;
        var searchcriteria;
        this.ageList = []
        if (this.selectedFacility.id == "ALL" && this.selectedCenter.id == undefined) {
            searchcriteria = "ALL"
        }
        if (this.selectedFacility.id != "ALL" && this.selectedCenter.id == "ALL") {
            searchcriteria = "FACILITY"
        } if (this.selectedCenter.id != "ALL" && this.selectedCenter.id != undefined) {
            searchcriteria = "CENTER"
        }
        this._patientsService.getPatientStatsForAgeByFilter(searchcriteria, 0,
            0, this.selectedFacility.id, this.selectedCenter.id)
            .then(data => {
                this.ageArray = data
                // console.log('data age', this.ageArray)
                this.ageArray.forEach(age => {
                    if (age._id >= 1 && age._id < 5) {
                        this.lessThanfive = this.lessThanfive + age.count;
                    } if (age._id >= 5 && age._id < 12) {
                        this.fiveTotwelve = this.fiveTotwelve + age.count;
                    } if (age._id >= 12 && age._id < 40) {
                        this.twelveTofourty = this.twelveTofourty + age.count;
                    } if (age._id >= 40 && age._id < 60) {
                        this.fourtyToSixty = this.fourtyToSixty + age.count;
                    } if (age._id >= 60 && age._id < 150) {
                        this.aboveSixty = this.aboveSixty + age.count;
                    }
                })

                this.ageList.push({ 'title': '< 5', 'value': this.lessThanfive, "color": "#F2F3F8" },
                    { 'title': '5-12', 'value': this.fiveTotwelve, "color": "#465EEB" },
                    { 'title': '13-40', 'value': this.twelveTofourty, "color": "#21C4DF" },
                    { 'title': '41-60', 'value': this.fourtyToSixty, "color": "#8800F3" },
                    { 'title': '> 60', 'value': this.aboveSixty, "color": "#6AC1E5" }
                )
            })
    }

    userReport(){
       this.getUserPatientAgeCount()
       this.getUserPatientGenderCount()       
    }


    getUserPatientGenderCount() {
        var searchcriteria;
        var tempArray;
        this.genderArray = [];      
        this.usersService.getUserPatientbyGender(this.userInfo.id).then(data => {
                tempArray = data;
                this.genderArray.push({ 'title': 'Male', 'value': tempArray.male, "color": "#465EEB" },
                    { 'title': 'Female', 'value': tempArray.female, "color": "#21C4DF" },
                    { 'title': 'Others', 'value': tempArray.other, "color": "#F2F3F8" }
                )
            })
    }


    getUserPatientAgeCount() {
        this.lessThanfive = 0;
        this.fiveTotwelve = 0;
        this.twelveTofourty = 0;
        this.fourtyToSixty = 0;
        this.aboveSixty = 0;
        var searchcriteria;
        this.ageList = []     
        this.usersService.getUserPatientbyAge(this.userInfo.id)
            .then(data => {
                this.ageArray = data
                // console.log('data age', this.ageArray)
                this.ageArray.forEach(age => {
                    if (age._id >= 1 && age._id < 5) {
                        this.lessThanfive = this.lessThanfive + age.count;
                    } if (age._id >= 5 && age._id < 12) {
                        this.fiveTotwelve = this.fiveTotwelve + age.count;
                    } if (age._id >= 12 && age._id < 40) {
                        this.twelveTofourty = this.twelveTofourty + age.count;
                    } if (age._id >= 40 && age._id < 60) {
                        this.fourtyToSixty = this.fourtyToSixty + age.count;
                    } if (age._id >= 60 && age._id < 150) {
                        this.aboveSixty = this.aboveSixty + age.count;
                    }
                })

                this.ageList.push({ 'title': '< 5', 'value': this.lessThanfive, "color": "#F2F3F8" },
                    { 'title': '5-12', 'value': this.fiveTotwelve, "color": "#465EEB" },
                    { 'title': '13-40', 'value': this.twelveTofourty, "color": "#21C4DF" },
                    { 'title': '41-60', 'value': this.fourtyToSixty, "color": "#8800F3" },
                    { 'title': '> 60', 'value': this.aboveSixty, "color": "#6AC1E5" }
                )
            })
    }


    



    facilityChange(value) {
        this.selectedFacility = value;
        this.selectedFacility.organizationCode = value.organizationCode;
        this.centers = value.centers;
        if (this.centers[0].id !== "ALL") {
            let allCenter = new Center();
            allCenter.name = "ALL";
            allCenter.id = "ALL";
            this.centers.splice(0, 0, allCenter);
        }
        this.selectedCenter.id = this.centers[0].id;
        this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
    }
}
