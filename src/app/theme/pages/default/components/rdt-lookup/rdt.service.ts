import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';

import 'rxjs/add/operator/toPromise';
import { Helpers } from './../../../../../helpers';

import { Device } from './device';
import { environment } from '../../../../../../environments/environment'
import { RDTEntity } from './RDTEntity';
import { RDTLookupJson } from './rdtFile';
import { RdtLookup } from './rdtLookup';
import { stat } from 'fs';

@Injectable()
export class RdtService {
    jsonObj: RDTEntity[] = [];

    private headers = new Headers({"Content-Type": "application/json",Authorization:"Bearer " + JSON.parse(localStorage.getItem("currentUser")).token
    });
    private serviceUrl2 = environment.BaseURL + environment.DeviceCommandURL; //'http://dev.ezdx.healthcubed.com/ezdx-device-command/api/v1/devices';  // URL to web api
    private serviceUrlRdt = environment.BaseURL + environment.FileSrvURL;
    private serviceUrlFileDetails = environment.BaseURL + environment.DetailFileSrvURL;
    private serviceUrlRdtJson = environment.BaseURL + "ezdx-file-srv/api/v1/file";

    constructor(
        private http: Http,
        private _router: Router,
        private _script: ScriptLoaderService
    ) { }

    getDefaultLookup(): Promise<any> {
        var defaultRdtUrl =
            this.serviceUrlRdt +
            "/search?searchCriteria=FILE_TYPE&fileType=DEFAULT_RDT_LOOKUP";
        return this.http
            .get(defaultRdtUrl, { headers: this.headers })
            .toPromise()
            .then(res => {
                return res.json();
            })
            .catch(this.handleError.bind(this));
    }

    downloadFile(id): Promise<any> {
        var downloadUrl = this.serviceUrlRdtJson + "/json/" + id;
        return this.http
            .get(downloadUrl, { headers: this.headers })
            .toPromise()
            .then(res => {
                return res.json();
            })
            .catch(this.handleError.bind(this));
    }

    downloadUrineLookupFile(id): Promise<any> {
        var downloadUrl = this.serviceUrlRdtJson + "/urinelookup/json/" + id;
        return this.http
            .get(downloadUrl, { headers: this.headers })
            .toPromise()
            .then(res => {
                return res.json();
            })
            .catch(this.handleError.bind(this));
    }


    fileDetails(fileId): Promise<any> {
        var fileDetailsUrl = this.serviceUrlFileDetails + "/" + fileId;
        return this.http.get(fileDetailsUrl, { headers: this.headers })
            .toPromise()
            .then(res => {
                return res.json();
            })
            .catch(this.handleError.bind(this))
    }

    getDefaultFile(type): Promise<any> {
        var defaultLookupUrl = this.serviceUrlRdt + "/search?searchCriteria=FILE_TYPE&fileType=" + type;
        return this.http.get(defaultLookupUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError.bind(this));
    }

    getLookups(type, state, page, limit): Promise<any> {
        var defaultRdtUrl = this.serviceUrlRdt + "/search?searchCriteria=FILE_TYPE_AND_ASSIGNMENT_STATUS&fileType=" + type + "&assignmentStatus=" + state + "&page=" +
            page + "&limit=" + limit;

        return this.http
            .get(defaultRdtUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError.bind(this));
    }

    getUnassignedLookup(page, limit): Promise<any> {
        var defaultRdtUrl =
            this.serviceUrlRdt +
            "/rdtlookup/search?assignmentStatus=UNASSIGNED&page=" +
            page +
            "&limit=" +
            limit;
        return this.http
            .get(defaultRdtUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError.bind(this));
    }

    getJsonRdtLookup(id): Promise<any> {
        var donloadUrl = this.serviceUrlRdt + "/" + id + "/download";
        return this.http
            .get(donloadUrl, { headers: this.headers })
            .toPromise()
            .then(respose => {
                // console.log("console json file:", respose);
                return respose.json();
            })
            .catch(this.handleError.bind(this));
    }

    /*   getJsonObj(): RDTEntity[] {
      //return this._http.get()
      return this.RDTFactory();
    } */

    /*   RDTFactory() {
      this.arrayRDLList.forEach(rdt => {
        // console.log("RDT: ", rdt);
        var rdtObj = new RDTEntity();
        rdtObj.testName = rdt;
        this.jsonObj = [...this.jsonObj, rdtObj];
      });
      return this.jsonObj;
    } */

    createRdtJson(rdtLookups: RDTLookupJson): Promise<any> {
        //Helpers.setLoading(true);
        var postUrl = this.serviceUrlRdt + "/rdtlookup/" + rdtLookups.fileName;
        return this.http
            .post(postUrl, rdtLookups, { headers: this.headers })
            .toPromise()
            .then(respose => {
                return respose;
            })
            .catch(this.handleError.bind(this));
    }

    getRDTList(): Promise<any> {
        var rdtListUrl =
            this.serviceUrlRdt +
            "/search?searchCriteria=FILE_TYPE&fileType=RDT_LOOKUP";
        return this.http
            .get(rdtListUrl, { headers: this.headers })
            .toPromise()
            .then(res => {
                return res.json();
            })
            .catch(this.handleError.bind(this));
    }

    updateRdtJson(id: string, rdtLookups: RDTLookupJson): Promise<any> {
        //Helpers.setLoading(true);
        var postUrl = this.serviceUrlRdt + "/rdtlookup/" + id;
        return this.http
            .put(postUrl, rdtLookups, { headers: this.headers })
            .toPromise()
            .then(respose => {
                return respose;
            })
            .catch(this.handleError.bind(this));
    }

    getRdtLookupById(rdtId): Promise<any> {
        var getByIdUrl = this.serviceUrlRdt + "/" + rdtId;
        // console.log("url: ", getByIdUrl);
        return this.http
            .get(getByIdUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                // console.log("Res: ", response);
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    assignRdtLookup(rdt): Promise<any> {
        var getByIdUrl = this.serviceUrlRdt + "/" + rdt.id + "/assign";
        return this.http
            .put(getByIdUrl, rdt, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load(
                "body",
                "assets/demo/default/custom/components/utils/redirect-cidaas-logout.js"
            );
        }
        return Promise.reject(error.message || error);
    }
}