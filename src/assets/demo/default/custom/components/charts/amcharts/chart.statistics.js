//== Class definition
var amChartsChartsDemo = function() {
    AmCharts.addInitHandler(function(chart) {
        if (chart.dataProvider[0]['column-1'] === 0 && chart.dataProvider[1]['column-1'] === 0 && chart.dataProvider[2]['column-1'] === 0) {
            // add label to let users know the chart is empty
            chart.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
        }

    }, ["pie"]);

    var pieChartPatientsStisticsAge = function() {
        var PATIENTS_STAT = JSON.parse(localStorage.getItem('PATIENTS_STISTICS_AGE'))
        AmCharts.makeChart("pie_chart_age_statistics", {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "colorField": "color",
            "titleField": "category",
            "valueField": "column-1",
            "theme": "light",
            "labelsEnabled": false,
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Patient Statistics ( Age )"
            }],
            "dataProvider": [{
                    "category": "< 5",
                    "column-1": parseInt(PATIENTS_STAT.first)
                },
                {
                    "category": "5-12",
                    "column-1": parseInt(PATIENTS_STAT.second)
                },
                {
                    "category": "13-40",
                    "column-1": parseInt(PATIENTS_STAT.third),
                },
                {
                    "category": "41-60",
                    "column-1": parseInt(PATIENTS_STAT.fourth),
                },
                {
                    "category": "> 60",
                    "column-1": parseInt(PATIENTS_STAT.fifth),
                }
            ]
        });
    }
    var pieChartPatientsStisticsGender = function() {
        var PATIENTS_STAT = JSON.parse(localStorage.getItem('PATIENTS_STISTICS_GENDER'));
        //// console.log('PATIENTS_STAT: ', PATIENTS_STAT);
        AmCharts.makeChart("pie_chart_gender_statistics", {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "colorField": "color",
            "titleField": "category",
            "valueField": "column-1",
            "theme": "light",
            "labelsEnabled": false,
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Patient Statistics (Gender)"
            }],
            "dataProvider": [{
                    "category": "Male",
                    "column-1": parseInt(PATIENTS_STAT.male),
                    "color": "blue"
                },
                {
                    "category": "Female",
                    "column-1": parseInt(PATIENTS_STAT.female),
                    "color": "pink"
                },
                {
                    "category": "Other",
                    "column-1": parseInt(PATIENTS_STAT.other),
                    "color": "yellow"
                }
            ]
        });
    }

    return {
        // public functions
        init: function() {
            pieChartPatientsStisticsAge();
            pieChartPatientsStisticsGender();
        }
    };
}();

jQuery(document).ready(function() {
    amChartsChartsDemo.init();
    $("a").removeAttr("href");
    $(".amcharts-chart-div a").hide();
});