import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FiltersComponent } from './filters.component';
import { LayoutModule } from './../../../../layouts/layout.module';
import { DefaultComponent } from './../../default.component';
import { UsersService } from './../users/users.service';
import { PatientsService } from './../patients/patients.service';
import { FacilitiesService } from './../facilities/facilities.service';
import { DevicesService } from './../devices/devices.service';
import { VisitsService } from './../visits/visits.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { AmChartsModule, AmChartsService } from "amcharts3-angular2";
import { AmchartModule } from '../../../../amchartcomponent/amchart.module';
import { FacDropdownModule } from '../../../../fac-dropdown/fac-dropdown.module'


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": FiltersComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, AmChartsModule,
        TranslateModule.forChild({ isolate: false }), FacDropdownModule,
        AmchartModule
    ], exports: [
        RouterModule
    ], declarations: [
        FiltersComponent
    ],
    providers: [
        AmChartsService,
        UsersService,
        PatientsService,
        FacilitiesService,
        DevicesService,
        VisitsService,
        TranslateService
    ]

})
export class FiltersModule { }
