import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { AccessDeniedComponent } from './access-denied.component';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": AccessDeniedComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes),
        LayoutModule, TranslateModule.forChild({ isolate: false })
    ], exports: [
        RouterModule
    ], declarations: [
        AccessDeniedComponent
    ],
    providers: [
        TranslateService
    ]
})
export class AccessDeniedModule {
}