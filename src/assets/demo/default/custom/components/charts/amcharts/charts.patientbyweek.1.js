//== Class definition
var amChartsChartsDemo = function() {


    return {
        // public functions
        init: function() {
            patientsChart();
        }
    };
}();

jQuery(document).ready(function() {
    //amChartsChartsDemo.init();
    var formateDate = function(d) {
        var m_names = new Array("Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec");

        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();
        return m_names[curr_month] + " " + curr_date + " " + curr_year;
    }
    var getDateRangeOfWeek = function(w, y) {
        var week = w.split("\n")
        var simple = new Date(y, 0, 1 + (week[0] - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay());
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        var ISOweekEnd = new Date(ISOweekStart);
        ISOweekEnd.setDate(ISOweekEnd.getDate() + 6);
        ISOweekEnd.setHours(23);
        ISOweekEnd.setMinutes(23);
        ISOweekEnd.setSeconds(23);
        ISOweekEnd.setMilliseconds(0);
        ISOweekEnd.setFullYear(y);
        return formateDate(ISOweekStart) + " to " + formateDate(ISOweekEnd);
    };
    AmCharts.addInitHandler(function(chart) {
       // // console.log('data not', chart.dataProvider)
        if (chart.dataProvider.length === 0 || chart.dataProvider === undefined) {
            // add label to let users know the chart is empty
            chart.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
        }

    }, ["serial"]);

    //== Private functions
    // var patientsChart = function () {
    var patientCountByWeek = JSON.parse(window.localStorage.getItem("patientCountByWeek"));
    var chart = AmCharts.makeChart("patients_chart_week", {
        "type": "serial",
        "categoryField": "Week_Year",
        "maxSelectedTime": -2,
        "angle": 30,
        "depth3D": 30,
        "startDuration": 1,
        "startEffect": "easeOutSine",
        "processCount": 998,
        "processTimeout": -2,
        "theme": "light",
        "categoryAxis": {
            "gridPosition": "start",
            "title": "Week"
        },
        "trendLines": [],
        "graphs": [{
            "balloonFunction": function(graphDataItem, graph) {
                return "<span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " patients</b></span><br/><span>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
            },
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "No of Patient Registrations",
            "type": "column",
            "valueField": "Patients",
            "labelText": "[[value]]",
            "showHandOnHover": false
        }],
        "guides": [],
        "valueAxes": [{
            "id": "ValueAxis-1",
            "stackType": "regular",
            "title": "Patients"
        }],
        "allLabels": [],
        "balloon": {},
        "legend": {
            "enabled": false,
            "useGraphSettings": true
        },
        "titles": [{
            "id": "Title-1",
            "size": 15,
            "text": "Patients Registration based on filtered date"
        }],
        "dataProvider": patientCountByWeek,
        "listeners": [{
            "event": "clickGraphItem",
            "method": function(event) {
                //window.localStorage.setItem("SelectedMonthDiagnosis", event.item.category);
                my.namespace.publicFuncPatientWeek(event.item.category, "patient");
            }
        }]
    });
    //}
});