export class Geopoint {
    lat: number;
    lon: number;
    alt: number;
}
