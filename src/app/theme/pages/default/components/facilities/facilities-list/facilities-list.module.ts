import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FacilitiesListComponent } from './facilities-list.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { FacilitiesService } from '../facilities.service';
import { DataTableModule, SharedModule, PaginatorModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { DropdownModule } from 'primeng/primeng';


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": FacilitiesListComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, DataTableModule, SharedModule, PaginatorModule,
        TranslateModule.forChild({ isolate: false }), DropdownModule
    ], exports: [
        RouterModule
    ], declarations: [
        FacilitiesListComponent
    ],
    providers: [
        FacilitiesService, TranslateService
    ]

})
export class FacilitiesListModule {



}