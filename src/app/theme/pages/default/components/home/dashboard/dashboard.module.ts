import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { UsersService } from '../../users/users.service';
import { PatientsService } from '../../patients/patients.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DevicesService } from '../../devices/devices.service';
import { VisitsService } from '../../visits/visits.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { AmchartModule } from '../../../../../amchartcomponent/amchart.module';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": DashboardComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule,
        TranslateModule.forChild({ isolate: false }),
        AmchartModule
    ], exports: [
        RouterModule
    ], declarations: [
        DashboardComponent
    ],
    providers: [
        UsersService,
        PatientsService,
        FacilitiesService,
        DevicesService,
        VisitsService,
        TranslateService
    ]

})
export class DashboardModule {



}