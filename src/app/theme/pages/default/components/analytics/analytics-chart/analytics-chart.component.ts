import { Component, OnInit, ViewEncapsulation, NgZone, OnDestroy } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { AnalyticsService } from '../analytics.service';
import { ScriptLoaderService } from './../../../../../../_services/script-loader.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./analytics-chart.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})

export class AnalyticsChartComponent implements OnInit, OnDestroy {


    
    user: any;
    visitcount: any[];
    patientcount: any[];
    analyticsUser: any;
    analyticsUsersresp: any={};
    userrole:any;
    constructor(private _router: Router, private analytics: AnalyticsService, private _script: ScriptLoaderService, private _ngZone: NgZone) {
        this.user=JSON.parse(localStorage.getItem('userInfo'));
     }

    public analyticsRespArr: any = [];
    public avgdiagnosticresp: any = {};
    public dataTESTPerReg: any = [];
    public datapermachinedata: any = [];
    public selectedoptionPMD: string = 'Patient registered';
    public selectedoptionTEST: string = 'Consumable based test'
    public showDigLoad: boolean = true;
    public PMDselectedbaloon: string = 'Patient registration / Device';
    public TESTselectedbaloon: string = 'Consumable based test / Registration';
    public xTitleforPMD: string = 'Patient registered';
    public selectedoptionCCWise: string = 'Registration';
    public dataVisitStat: any = [];
    public visitorlog: any = [];
    public ccRegTest: any = [];
    public dataforRegStat: any = [];
    public dataforTestStat: any = [];
    public visitorlogresp: any = {};
    public ccRegTestresp: any = {};
    public visitorlogCount: number = 0;
    public from: any;
    public to: any;
    public totalpatientsReg: number;
    public totaldiagnosticsmade: number;
    role:string='admin';


    ngOnInit() {
        Helpers.setLoading(true);
        
        window.my = window.my || {};
        window.my.namespace1 = window.my.namespace1 || {};
        window.my.namespace1.gotodesiredpage = this.gotodesiredpage.bind(this);
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/datatables/base/date.analytics.js');
        var date = new Date();
        this.to = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        this.from = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 29, 0);
        // console.log(this.from, this.to)
        if ((this.user.userRoles).includes('TECHNICIAN')) {
            this.role='user';
            this.userSpecificStats();
        }else{
            this.getavgdiagnosticAndConsumablebasedtest();
        }
        

    }

    ngOnDestroy() {
        window.my.namespace1 = null;
    }


    userSpecificStats(): any {
        this.dataTESTPerReg = [];
        this.datapermachinedata = [];
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getCenterAnalyticsSearch(this.user.facilityId).then((res) => {
            Helpers.setLoading(false);
              console.log(res)
            if (res != undefined) {
                let patientcountarr = [];
                let diagnosticscountarr = [];
                this.avgdiagnosticresp = res;
                localStorage.setItem('analytics_res', JSON.stringify(this.avgdiagnosticresp));
                this.analyticsRespArr = this.avgdiagnosticresp.results;
                //console.log(this.analyticsRespArr)
                this.analyticsRespArr.map((item) => {
                    diagnosticscountarr.push(item.totalTests);
                    patientcountarr.push(item.totalRegistrations);
                    this.dataTESTPerReg.push({
                        "y": item.totalConsumableTests,
                        "x": item.totalRegistrations,
                        "value": (item.consumableTestsPerRegistration.toFixed(2)),
                        "facility": item.facilityName,
                        "center": item.centerName
                    });
                    this.datapermachinedata.push({
                        "y": item.totalDevices,
                        "x": item.totalRegistrations,
                        "value": item.totalDevices != 0 ? (item.registrationPerDevice).toFixed(2) : 0,
                        "facility": item.facilityName,
                        "center": item.centerName
                    })
                });
                
                console.log(this.dataTESTPerReg)
                console.log(this.datapermachinedata)
                /**
   * gettotaldiagnostics
   */
                let patientData = JSON.parse(localStorage.getItem('patientcount'));
                let diagnosticCount = JSON.parse(localStorage.getItem('diagnosticCount'));
                console.log(patientData)
                this.totalpatientsReg = patientData;
                this.totaldiagnosticsmade = diagnosticCount;
                this.analyticsRespArr.map((item) => {
                    //console.log(item)
                    if (((item.totalRegistrations / this.totalpatientsReg) * 100) > 1) {
                        this.dataforRegStat.push({
                            "percentage": ((item.totalRegistrations / this.totalpatientsReg) * 100).toFixed(2),
                            "patient registered": item.totalRegistrations,
                            "customer": item.facilityName,
                            "center": item.centerName,
                            "registration": true
                        });
                    }
                    if (((item.totalTests / this.totaldiagnosticsmade) * 100) > 1) {
                        this.dataforTestStat.push({
                            "percentage": ((item.totalTests / this.totaldiagnosticsmade) * 100).toFixed(2),
                            "diagnostics": item.totalTests,
                            "customer": item.facilityName,
                            "center": item.centerName,
                            "registration": false
                        })
                    }

                });
                console.log(this.dataforTestStat)
                console.log(this.dataforRegStat)
                setTimeout(() => {
                    this.showDigLoad = false;
                }, 1500);
            } else {
                this.analyticsRespArr = [];
                this.showDigLoad = false;
            }
        })
    }

    gotodesiredpage(param, dataContext, type) {
        this._ngZone.run(() => {
            if (type == "test") {
                if (param === "Diagnostics") {
                    this._router.navigate(['/analytics/avgdiagnostics'], { queryParams: { selectedItem: dataContext.facility, selectedcenter: dataContext.center } });
                } else {
                    this._router.navigate(['/analytics/avgconsumablebasedtest'], { queryParams: { selectedItem: dataContext.facility, selectedcenter: dataContext.center } });
                }
            }
            if (type == "devices") {
                this._router.navigate(['/analytics/permachinedata'], { queryParams: { selectedparam: param, selectedItem: dataContext.facility, selectedcenter: dataContext.center } });
            }
            if (param == 'visitstats') {
                this._router.navigate(['/analytics/visitlog'], { queryParams: { selectedItem: dataContext.User, fromdate: localStorage.getItem('analyticsStartDate'), todate: localStorage.getItem('analyticsEndDate') } });
            }
            if (type == "ccwisedata") {
                if (param == "registration") {
                    this._router.navigate(['/analytics/centerwiseregistration'], { queryParams: { selectedItem: dataContext.customer, selectedcenter: dataContext.center } });
                } else {
                    this._router.navigate(['/analytics/centerwisetest'], { queryParams: { selectedItem: dataContext.customer, selectedcenter: dataContext.center } });

                }
            }

        });
    }

    //get diagnostics and consumable based test per registration
    public getavgdiagnosticAndConsumablebasedtest(): void {
        this.dataTESTPerReg = [];
        this.datapermachinedata = [];
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getanalytics().then((res) => {
            Helpers.setLoading(false);
              console.log(res)
            if (res != undefined) {
                let patientcountarr = [];
                let diagnosticscountarr = [];
                this.avgdiagnosticresp = res;
                localStorage.setItem('analytics_res', JSON.stringify(this.avgdiagnosticresp));
                this.analyticsRespArr = this.avgdiagnosticresp.results;
                this.analyticsRespArr.map((item) => {
                    diagnosticscountarr.push(item.totalTests);
                    patientcountarr.push(item.totalRegistrations);
                    this.dataTESTPerReg.push({
                        "y": item.totalConsumableTests,
                        "x": item.totalRegistrations,
                        "value": (item.consumableTestsPerRegistration.toFixed(2)),
                        "facility": item.facilityName,
                        "center": item.centerName
                    });
                    this.datapermachinedata.push({
                        "y": item.totalDevices,
                        "x": item.totalRegistrations,
                        "value": item.totalDevices != 0 ? (item.registrationPerDevice).toFixed(2) : 0,
                        "facility": item.facilityName,
                        "center": item.centerName
                    })
                });

                /**
   * gettotaldiagnostics
   */
                let countdata = JSON.parse(localStorage.getItem('dashboardcount'));
                this.totalpatientsReg = countdata.patients;
                this.totaldiagnosticsmade = countdata.tests;
                this.analyticsRespArr.map((item) => {
                    //console.log(item)
                    if (((item.totalRegistrations / this.totalpatientsReg) * 100) > 1) {
                        this.dataforRegStat.push({
                            "percentage": ((item.totalRegistrations / this.totalpatientsReg) * 100).toFixed(2),
                            "patient registered": item.totalRegistrations,
                            "customer": item.facilityName,
                            "center": item.centerName,
                            "registration": true
                        });
                    }
                    if (((item.totalTests / this.totaldiagnosticsmade) * 100) > 1) {
                        this.dataforTestStat.push({
                            "percentage": ((item.totalTests / this.totaldiagnosticsmade) * 100).toFixed(2),
                            "diagnostics": item.totalTests,
                            "customer": item.facilityName,
                            "center": item.centerName,
                            "registration": false
                        })
                    }

                });

                setTimeout(() => {
                    this.showDigLoad = false;
                }, 1500);
            } else {
                this.analyticsRespArr = [];
                this.showDigLoad = false;
            }
        })
    }

    //selected test per reg dropdown
    selectedoptionTESTchanged(option) {
        Helpers.setLoading(true);
        this.selectedoptionTEST = option;
        this.TESTselectedbaloon = this.selectedoptionTEST + ' / Registration';
        this.dataTESTPerReg = [];
        this.analyticsRespArr.map((item) => {
            if (option == 'Consumable based test') {
                this.dataTESTPerReg.push({
                    "y": item.totalConsumableTests,
                    "x": item.totalRegistrations,
                    "value": (item.consumableTestsPerRegistration).toFixed(2),
                    "facility": item.facilityName,
                    "center": item.centerName
                });
            }
            else if (option == 'Diagnostics') {
                this.dataTESTPerReg.push({
                    "y": item.totalTests,
                    "x": item.totalRegistrations,
                    "value": (item.testsPerRegistration).toFixed(2),
                    "facility": item.facilityName,
                    "center": item.centerName,
                    "color": "#b22222"
                })
            }
        });
        setTimeout(() => {
            Helpers.setLoading(false);
        }, 1500);
    }

    //dropdown changed for per machine data
    selectedoptionPMDchanged(option) {
        Helpers.setLoading(true);
        this.selectedoptionPMD = option;
        this.PMDselectedbaloon = this.selectedoptionPMD + ' / Device';
        this.xTitleforPMD = this.selectedoptionPMD;
        this.datapermachinedata = [];
        this.analyticsRespArr.map((item) => {
            if (option == 'Patient registered') {
                this.datapermachinedata.push({
                    "y": item.totalDevices,
                    "x": item.totalRegistrations,
                    "value": item.totalDevices != 0 ? (item.registrationPerDevice).toFixed(2) : 0,
                    "facility": item.facilityName,
                    "center": item.centerName
                })
            }
            else if (option == 'Consumable based test') {
                this.datapermachinedata.push({
                    "y": item.totalDevices,
                    "x": item.totalConsumableTests,
                    "value": item.totalDevices != 0 ? (item.consumableTestsPerDevice).toFixed(2) : 0,
                    "facility": item.facilityName,
                    "center": item.centerName,
                    "color": "#71bf44",
                })
            }
            else if (option == 'Probe based test') {
                this.datapermachinedata.push({
                    "y": item.totalDevices,
                    "x": item.totalProbeTests,
                    "value": item.totalDevices != 0 ? ((item.totalProbeTests) / (item.totalDevices)).toFixed(2) : 0,
                    "facility": item.facilityName,
                    "center": item.centerName,
                    "color": "#da70d6",
                })
            }
        });
        setTimeout(() => {
            Helpers.setLoading(false);
        }, 1700);
    }

    //get visitor log list
    // public getallvisitorLog(from, to): void {
    //     this.analytics.getvisitLogs(from, to).then((res) => {
    //         this.dataVisitStat = [];
    //         if (res != undefined) {
    //             this.visitorlogresp = res;
    //             this.visitorlog = this.visitorlogresp.userStatsView;
    //             this.visitorlog.map((item) => {
    //                 this.dataVisitStat.push({
    //                     "User": item.displayName,
    //                     "Patient Registered": item.patientCount,
    //                     "Re-visits": item.visitCount
    //                 })
    //             });
    //         } else {
    //             this.visitorlog = [];
    //             this.visitorlogCount = 0;
    //         }
    //     })
    // }
   

}
