import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FacilitiesAddComponent } from './facilities-add.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { FacilitiesService } from '../facilities.service';
import { CommonService } from '../../../../../../_services/common.service'
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { Ng4FilesModule } from "angular4-files-upload"
import { MapLoaderService } from '../../utils/map-loader.service';
import { AutoCompleteModule, GMapModule, DialogModule } from 'primeng/primeng'
import { DataTableModule, SharedModule, PaginatorModule } from 'primeng/primeng';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": FacilitiesAddComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, Ng4FilesModule, GMapModule, AutoCompleteModule, DialogModule,
        TranslateModule.forChild({ isolate: false }), PaginatorModule, DataTableModule, SharedModule,
    ], exports: [
        RouterModule
    ], declarations: [
        FacilitiesAddComponent
    ],
    providers: [
        MapLoaderService,
        FacilitiesService, CommonService,
        TranslateService
    ]

})
export class FacilitiesAddModule {



}