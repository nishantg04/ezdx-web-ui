import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DeviceAddComponent } from './devices-add.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { DevicesService } from '../devices.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { OrderModule } from 'ngx-order-pipe';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { FacDropdownModule } from '../../../../../fac-dropdown/fac-dropdown.module'


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": DeviceAddComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule, FacDropdownModule,
        TranslateModule.forChild({ isolate: false }),

    ], exports: [
        RouterModule
    ], declarations: [
        DeviceAddComponent
    ],
    providers: [
        DevicesService,
        FacilitiesService,
        TranslateService
    ]

})
export class DevicesAddModule {



}