import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { environment } from "../../../../../../environments/environment";
import { Helpers } from "../../../../../helpers";
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";

@Injectable()
export class StateChangeService {

    private headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private serviceUrl = environment.BaseURL + environment.UserCommandURL

    constructor(private _http: Http, private _script: ScriptLoaderService) { }

    setUserActive(userId: string): Promise<any> {
        let activateUrl = this.serviceUrl + "/activate/" + userId;
        return this._http
            .put(activateUrl, null, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    setUserDeactive(userId: string): Promise<any> {
        let activateUrl = this.serviceUrl + "/deactivate/" + userId;
        return this._http
            .put(activateUrl, null, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load('body',
                'assets/demo/default/custom/components/utils/redirect-cidaas-logout.js');
        }
        //alert('An error occurred:' + error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}