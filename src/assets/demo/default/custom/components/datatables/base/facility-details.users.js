//== Class definition

var DatatableDataLocalDemo = function() {
    //== Private functions

    // demo initializer
    var demo = function() {

        //Refresh table by remove html tag
        // $("#user_data").remove();
        // var $newdiv1 = $("<div class='m_datatable id='user_data'></div>");
        // $(".m-portlet__body").append($newdiv1);
        // var dataJSONArray = JSON.parse(localStorage.getItem('facilityuserTable'));
        // var roleString = '';
        // for (j = 0; j < dataJSONArray.length; j++) {
        //     roleString = '';
        //     if (dataJSONArray[j].userRoles) {
        //         for (var i = 0; i < dataJSONArray[j].userRoles.length; i++) {
        //             if (i != 0)
        //                 roleString += ', ' + dataJSONArray[j].userRoles[i];
        //             else
        //                 roleString = dataJSONArray[j].userRoles[i];
        //         }
        //     }
        //     dataJSONArray[j].roleString = roleString;
        // }



        // var datatable = $('#user_data').mDatatable({
        //     // datasource definition
        //     data: {
        //         type: 'local',
        //         source: dataJSONArray,
        //         pageSize: 10,
        //         saveState: {
        //             cookie: false,
        //             webstorage: false
        //         }, // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
        //     },

        //     // layout definition
        //     layout: {
        //         theme: 'default', // datatable theme
        //         class: '', // custom wrapper class
        //         scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        //         height: 450, // datatable's body's fixed height
        //         footer: false // display/hide footer
        //     },

        //     // column sorting(refer to Kendo UI)
        //     sortable: true,

        //     // column based filtering(refer to Kendo UI)
        //     filterable: false,

        //     pagination: true,

        //     // inline and bactch editing(cooming soon)
        //     // editable: false,

        //     // columns definition

        //     columns: [{
        //         field: "",
        //         title: "Sr no",
        //         width: 50,
        //         sortable: false,
        //         template: function (row) {
        //             return '\
        // 				<span>\
        //                     ' + (((row.getDatatable().getCurrentPage() - 1) * 10) + (row.getIndex() + 1)) + '\
        //                 </span>\
        // 			';
        //         }
        //     }, {
        //         field: "firstName",
        //         title: "Name",
        //         width: 300,
        //         sortable: true,
        //         template: function (row) {
        //             return '\
        // 				<div>\
        // 				<i class="la la-user" style="color: #656565;"></i>&nbsp;&nbsp;' + row.firstName + ' ' + row.lastName + '\
        // 				</div><div> <i class="fa fa-envelope-o" style="color: #656565; font-size: 14px"></i>&nbsp;&nbsp;' + row.email + '\
        //                 </div>\
        // 			';
        //         }
        //     }, {
        //         field: "userRole",
        //         title: "Role",
        //         sortable: true,
        //         template: function (row) {
        //             return '\
        // 				<span>\
        //                     ' + row.roleString + '\
        //                 </span>\
        // 			';
        //         }
        //     }
        //     ]
        // });

        // var query = datatable.getDataSourceQuery();
        // datatable.sort('firstName');

        // $('#m_user_search').on('keyup', function (e) {
        //     datatable.search($(this).val().toLowerCase());
        // }).val(query.generalSearch);

        // $('#m_form_status').on('change', function () {
        // 	datatable.search($(this).val(), 'Status');
        // }).val(typeof query.Status !== 'undefined' ? query.Status : '');

        // $('#m_form_type').on('change', function () {
        // 	datatable.search($(this).val(), 'Type');
        // }).val(typeof query.Type !== 'undefined' ? query.Type : '');

        // $('#m_form_status, #m_form_type').selectpicker();

    };

    return {
        //== Public functions
        init: function() {
            // init dmeo
            demo();
        }
    };
}();

jQuery(document).ready(function() {
    //DatatableDataLocalDemo.init();
    //Refresh table by remove html tag
    // $("#user_data").remove();
    // var $newdiv1 = $("<div class='m_datatable id='user_data'></div>");
    // $(".m-portlet__body").append($newdiv1);
    var dataJSONArray = JSON.parse(localStorage.getItem('facilityuserTable'));
    var roleString = '';
    for (j = 0; j < dataJSONArray.length; j++) {
        roleString = '';
        if (dataJSONArray[j].userRoles) {
            for (var i = 0; i < dataJSONArray[j].userRoles.length; i++) {
                if (i != 0)
                    roleString += ', ' + dataJSONArray[j].userRoles[i];
                else
                    roleString = dataJSONArray[j].userRoles[i];
            }
        }
        dataJSONArray[j].roleString = roleString;
    }



    var datatable = $('#user_data').mDatatable({
        // datasource definition
        data: {
            type: 'local',
            source: dataJSONArray,
            pageSize: 10,
            saveState: {
                cookie: false,
                webstorage: false
            }, // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
        },

        // layout definition
        layout: {
            theme: 'default', // datatable theme
            class: '', // custom wrapper class
            scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
            height: 450, // datatable's body's fixed height
            footer: false // display/hide footer
        },

        // column sorting(refer to Kendo UI)
        sortable: true,

        // column based filtering(refer to Kendo UI)
        filterable: false,

        pagination: true,

        // inline and bactch editing(cooming soon)
        // editable: false,

        // columns definition

        columns: [{
            field: "",
            title: "Sr no",
            width: 50,
            sortable: false,
            template: function(row) {
                return '\
						<span>\
                            ' + (((row.getDatatable().getCurrentPage() - 1) * 10) + (row.getIndex() + 1)) + '\
                        </span>\
					';
            }
        }, {
            field: "firstName",
            title: "Name",
            width: 300,
            sortable: true,
            template: function(row) {
                return '\
						<div>\
						<i class="la la-user" style="color: #656565;"></i>&nbsp;&nbsp;' + row.firstName + ' ' + row.lastName + '\
						</div><div> <i class="fa fa-envelope-o" style="color: #656565; font-size: 14px"></i>&nbsp;&nbsp;' + row.email + '\
                        </div>\
					';
            }
        }, {
            field: "userRole",
            title: "Role",
            sortable: true,
            template: function(row) {
                return '\
						<span>\
                            ' + row.roleString + '\
                        </span>\
					';
            }
        }, {
            field: "Actions",
            width: 110,
            title: "Actions",
            sortable: false,
            overflow: 'visible',
            template: function(row) {
                return '\
                <a id="' + row.id + '" onclick="my.namespace.userViewPublicFunc(\'' + row.id + '\')" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill edit-facility" title="View ">\
                <i class="la la-eye"></i>\
            </a>\
               \
            ';
            }
        }]
    });

    var query = datatable.getDataSourceQuery();
    datatable.sort('firstName');

    $('#m_user_search').on('keyup', function(e) {
        datatable.search($(this).val().toLowerCase());
    }).val(query.generalSearch);
});