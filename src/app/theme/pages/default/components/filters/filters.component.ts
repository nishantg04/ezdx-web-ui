import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from './../../../../../helpers';
import { ScriptLoaderService } from './../../../../../_services/script-loader.service';
import { UsersService } from './../users/users.service';
import { PatientsService } from './../patients/patients.service';
import { UserChild } from './../users/userChild';
import { Facility } from './../facilities/facility';
import { FacilitiesService } from './../facilities/facilities.service';
import { DevicesService } from './../devices/devices.service';
import { VisitsService } from './../visits/visits.service';
import { Center } from './../facilities/center';
import { Visit } from './../visits/visit';
import { Test } from './../visits/test';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./filters.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class FiltersComponent implements OnInit {
    user: UserChild;
    patientCount: any;
    deviceCount: any;
    visitCount: number;
    testCount: number;
    visitCountTotal: number;
    testCountTotal: number;
    testList: any;
    userType: string;
    totalVists: any;
    totalTest: any;
    facilities: Facility[];
    centers: Center[];
    selectedFacility: Facility;
    selectedCenter: Center;
    userInfo: any;
    visitsCount: any;
    selectedTestType: any;
    selectedTestList: any;
    testsCount: any;
    patientCountByWeek: any[];
    years: any[];
    selectedYear: any;
    testCountByWeek: any[];
    hideDignostic: boolean = false;
    hidePatient: boolean = false;
    hcOperations: boolean = false;
    graphValues: any[] = [];
    patientChart: any[] = [];
    testChartValue: any[] = [];
    showPatLoad: boolean = false;
    showDigLoad: boolean = false;
    something: boolean = true;
    testMapCount: any;

    constructor(private _ngZone: NgZone, private _script: ScriptLoaderService, private _router: Router,
        private _usersService: UsersService, private _patientsService: PatientsService, private _facilitiesService: FacilitiesService,
        private _devicesService: DevicesService, private _visitsService: VisitsService) {
        this.testsCount = null;
        let currentYear = new Date().getFullYear();
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        // this.tests = [];
        this.years = [];
        this.years.push("ALL");
        for (var index = 2014; index <= currentYear; index++) {
            this.years.push(index);
        }
        this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
        this.user = JSON.parse(window.localStorage.getItem('userInfo'));
        this.selectedYear = this.years[this.years.length - 1];
        this.testList = [
            { type: "physical", name: "Physical", color: '#71bf44', tests: [{ name: "Blood Pressure", type: "BLOOD_PRESSURE" }, { name: "Pulse Oximeter", type: "PULSE_OXIMETER" }, { name: "%O2 Saturation", type: "%O2 Saturation" }, { name: "Temperature", type: "TEMPERATURE" }, { name: "Height", type: "Height" }, { name: "Weight", type: "Weight" }, { name: "BMI", type: "BMI" }, { name: "ECG", type: "ECG" }, { name: "Mid Arm Circumference", type: "Mid Arm Circumference" }] },
            { type: "whole_blood_poct", color: '#21C4DF', name: "Whole Blood-POCT", tests: [{ name: "Glucose", type: "BLOOD_GLUCOSE" }, { name: "Hemoglobin", type: "HEAMOGLOBIN" }, { name: "Cholesterol", type: "CHOLESTEROL" }, { name: "Uric Acid", type: "URIC_ACID" }] },
            { type: "whole_blood", name: "Whole Blood", color: '#02abcb', tests: [{ name: "Blood Grouping", type: "BLOOD_GROUPING" }] },
            { type: "whole_blood_rdt", name: "Whole Blood-RDT", color: '#465EEB', tests: [{ name: "Malaria", type: "Malaria" }, { name: "Dengue", type: "Dengue" }, { name: "Typhoid", type: "Typhoid" }, { name: "Hep-B", type: "Hep-B" }, { name: "Hep-C", type: "Hep-C" }, { name: "HIV", type: "HIV" }, { name: "Chikungunya", type: "Chikungunya" }, { name: "Syphilis", type: "Syphilis" }, { name: "Troponin-I", type: "Troponin-I" }] },
            { type: "urine_poct", name: "Urine-POCT", color: '#DA70D6', tests: [{ name: "Pregnancy", type: "Pregnancy" }] },
            { type: "urine_rdt", name: "Urine-RDT", color: '#B22222', tests: [{ name: "Urine Sugar", type: "Urine Sugar" }, { name: "Urine Protein", type: "Urine Protein" }, { name: "Leukocytes", type: "Leukocytes" }, { name: "Urobilinogen", type: "Urobilinogen" }, { name: "Nitrite", type: "Nitrite" }, { name: "pH", type: "pH" }, { name: "Ketone", type: "Ketone" }, { name: "Blood", type: "Blood" }, { name: "Bilurubin", type: "Bilurubin" }, { name: "Specific Gravity", type: "Specific Gravity" }] }
        ];
        this.selectedTestType = this.testList[0].type;
        this.selectedTestList = this.testList[0].tests;
    }

    ngOnInit() {
        this.getUser();
        this.getFacilities();
    }



    getUser() {
        Helpers.setLoading(true);
        this._usersService.getCurrentUser()
            .then(user => this.user = user)
            .then(() => {
                window.localStorage.setItem('userInfo', JSON.stringify(this.user));
                if (this.user && this.user.userRoles) {
                    let isAdmin = false;
                    this.user.userRoles.forEach((role, i) => {
                        if (role === "HC_ADMIN") {
                            isAdmin = true;
                        }
                        if (role === "HC_OPERATIONS") {
                            this.hcOperations = true;
                            document.getElementById('patient_filter').style.cursor = "pointer";
                            isAdmin = true;
                        } else {
                            document.getElementById('patient_filter').style.cursor = "auto";
                        }
                    });
                    if (!isAdmin) {
                        let isFirst = true;
                        let customer_admin = false;
                        let center_admin = false;
                        let operator_user = false;
                        // this.userInfo.userRoles.includes('CENTER_ADMIN')
                        // if (this.user.userRoles[0] == 'CENTER_ADMIN' && this.user.userRoles.length == 1) {
                        //     window.localStorage.setItem('userType', 'Center');
                        //     this.hover = false;
                        // }
                        this.user.userRoles.forEach((role, i) => {
                            if (role === "CENTER_ADMIN") {
                                center_admin = true;
                                operator_user = false;
                            }
                            if (role === "DOCTOR") {
                                customer_admin = true;
                                operator_user = false;
                                center_admin = false;
                            }
                            if (role === "TECHNICIAN") {
                                customer_admin = true;  
                                operator_user = false;  
                                center_admin = false;                   
                            }
                            if (role === "OPERATOR") {
                                operator_user = true;                       
                            }
                        });
                        if (this.user.userRoles[0] == 'OPERATOR' && this.user.userRoles.length == 1 && operator_user) {
                            window.localStorage.setItem('userType', 'User');
                           
                        }
                        else if(center_admin){
                            window.localStorage.setItem('userType', 'Center');                           
                        }
                        else {
                            isFirst = false;                           
                            window.localStorage.setItem('userType', 'Facility');
                        }        
                    }
                    else {
                        let isFirst = true;
                        if (window.localStorage.getItem('userType')) {
                            if (window.localStorage.getItem('userType') == 'Admin')
                                isFirst = false;
                        }
                        window.localStorage.setItem('userType', 'Admin');
                    }
                    if (window.localStorage.getItem('userType') == 'Facility')
                        this.userType = 'Facility';
                    else if (window.localStorage.getItem('userType') == 'Center')
                        this.userType = 'Center';
                    else if (window.localStorage.getItem('userType') == 'User')
                        this.userType = 'User';
                    else
                        this.userType = 'Admin';

                    //this.getFacilities();
                    // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                    //     'assets/app/js/trends.center.js');
                    // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                    //     'assets/demo/default/custom/components/datatables/base/date.dashboard.js');

                }
                else {
                    this._router.navigate(['/login']);
                }
                Helpers.setLoading(false);
            });
    }

    getFacilities() {
        // console.log('get facilities')
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        // console.log('fac', this.facilities)
        this.facilities.sort(function(a, b) { if (a.name > b.name) { return 1; } else if (a.name < b.name) { return -1; } return 0; });
        let allFacility = new Facility();
        allFacility.name = "ALL";
        allFacility.id = "ALL";
        this.facilities.splice(0, 0, allFacility);
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - 29);
        startDate.setHours(0); startDate.setMinutes(0); startDate.setSeconds(0); startDate.setMilliseconds(0);
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        endDate.setHours(23); endDate.setMinutes(23); endDate.setSeconds(23); endDate.setMilliseconds(0);
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/datatables/base/date.dashboard.js');
        let searchcriteria = "ALL";
        if (window.localStorage.getItem('userType') == 'Facility') {
            this.selectedFacility.id = this.userInfo.facilityId;
            this.facilities.forEach(fclt => {
                if (fclt.id == this.userInfo.facilityId) {
                    this.centers = jQuery.extend(true, [], fclt.centers);
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                    this.selectedCenter.id = this.centers[0].id;

                }
            });
        }
        else {
            this.selectedFacility.id = this.facilities[0].id;
            this.centers = this.facilities[0].centers;
            this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
        }
        if (window.localStorage.getItem('userType') === "Facility") {
            searchcriteria = "FACILITY";
            this.getPatientCount(startDate, endDate, searchcriteria, this.selectedFacility.id, 0);
            this.getNewDeviceCount(startDate, endDate, searchcriteria, this.selectedFacility.id, 0);
            this.getNewVisitCount(startDate, endDate, searchcriteria, this.selectedFacility.id, 0);
            this.getNewTestCount(startDate, endDate, searchcriteria, this.selectedFacility.id, 0);
            this.getPatientCountByWeek(startDate, endDate, searchcriteria, this.selectedFacility.id, 0);
            this.getTestCountByWeek(startDate, endDate, searchcriteria, this.selectedFacility.id, 0);
        }
        else if (window.localStorage.getItem('userType') === "Center") {
            searchcriteria = 'CENTER'
            this.selectedCenter.id = this.userInfo.centerId;
            this.selectedFacility.id = this.userInfo.facilityId;
            this.selectedFacility.organizationCode = this.userInfo.facilityCode;
            this.getPatientCount(startDate, endDate, "CENTER", this.selectedFacility.id, this.selectedCenter.id);
            this.getNewDeviceCount(startDate, endDate, "CENTER", this.selectedFacility.id, this.selectedCenter.id);
            this.getNewVisitCount(startDate, endDate, "CENTER", this.selectedFacility.id, this.selectedCenter.id);
            this.getPatientCountByWeek(startDate, endDate, "CENTER", this.selectedFacility.id, this.selectedCenter.id)
            this.getTestCountByWeek(startDate, endDate, "CENTER", this.selectedFacility.id, this.selectedCenter.id)
            this.getNewTestCount(startDate, endDate, "CENTER", this.selectedFacility.id, this.selectedCenter.id);
        }
        else if (window.localStorage.getItem('userType') === "User") {
            this.getPatientCountUser(startDate, endDate);
            this.getDigCountUser(this.user.id, startDate, endDate);
            this.getVisitCountByUser(this.user.id, startDate, endDate)
            //  this.getTestMapped();
            this.getTestCountByWeekUser( startDate, endDate)
            this.getPatientCountByWeekUser('USER', startDate, endDate,this.user.id)
        }
        else {
            this.getPatientCount(startDate, endDate, searchcriteria, 0, 0);
            this.getNewDeviceCount(startDate, endDate, searchcriteria, 0, 0);
            this.getNewVisitCount(startDate, endDate, searchcriteria, 0, 0);
            this.getNewTestCount(startDate, endDate, searchcriteria, 0, 0);
            this.getPatientCountByWeek(startDate, endDate, searchcriteria, 0, 0);
            this.getTestCountByWeek(startDate, endDate, searchcriteria, 0, 0);
        }

    }

    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers[0].id !== "ALL") {
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                }
                this.selectedCenter.id = this.centers[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }

    filterReports(): void {
        this.something = false;
        if (window.localStorage.getItem('userType') === "User") {
            this.getPatientCountUser(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'));
            this.getDigCountUser(this.user.id, window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'));
            this.getVisitCountByUser(this.user.id, window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'))
            this.getTestMapped();
            this.getTestCountByWeekUser(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'))
            this.getPatientCountByWeekUser('USER', window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'),this.user.id)
        } else {
            if (this.selectedFacility.id != "ALL") {
                if (this.selectedCenter.id != "ALL") {
                    this.getPatientCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                    this.getNewDeviceCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                    this.getNewVisitCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                    this.getPatientCountByWeek(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "CENTER", this.selectedFacility.id, this.selectedCenter.id)
                    this.getTestCountByWeek(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "CENTER", this.selectedFacility.id, this.selectedCenter.id)
                    this.getNewTestCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "CENTER", this.selectedFacility.id, this.selectedCenter.id);
                }
                else {
                    this.getPatientCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "FACILITY", this.selectedFacility.id, 0);
                    this.getNewDeviceCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "FACILITY", this.selectedFacility.id, 0);
                    this.getNewVisitCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "FACILITY", this.selectedFacility.id, 0);
                    this.getPatientCountByWeek(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "FACILITY", this.selectedFacility.id, 0)
                    this.getTestCountByWeek(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "FACILITY", this.selectedFacility.id, 0)
                    this.getNewTestCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "FACILITY", this.selectedFacility.id, 0);
                }
            }
            else {
                this.getPatientCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "ALL", 0, 0);
                this.getNewDeviceCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "ALL", 0, 0);
                this.getNewVisitCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "ALL", 0, 0);
                this.getPatientCountByWeek(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "ALL", 0, 0)
                this.getTestCountByWeek(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "ALL", 0, 0)
                this.getNewTestCount(window.localStorage.getItem('dashStartDate'), window.localStorage.getItem('dashEndDate'), "ALL", 0, 0);
            }
        }

    }

    getPatientCount(startDate, endDate, searchcriteria, facilityId, centerId): void {
        Helpers.setLoading(true);
        this.patientCount = null;
        this._patientsService.getPatientFilterCount(new Date(startDate).getTime(), new Date(endDate).getTime(), searchcriteria, facilityId, centerId)
            .then(count => this.patientCount = count)
            .then(() => {
                this.patientCount = this.patientCount.toString();
                Helpers.setLoading(false);
            });
    }

    getNewDeviceCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        Helpers.setLoading(true);
        this.deviceCount = null;
        this._devicesService.getNewDeviceCount(new Date(startDate).getTime(), new Date(endDate).getTime(), searchcriteria, facilityid, centerid)
            .then(data => {
                // console.log('count hub', data)
                if (data != null) {
                    this.deviceCount = data;
                    this.deviceCount = this.deviceCount.toString();
                } else {
                    this.deviceCount = 0;
                    this.deviceCount = this.deviceCount.toString();
                }
            })
            .then(() => {
                Helpers.setLoading(false);
            });
    }

    getNewVisitCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        this.visitsCount = null;
        Helpers.setLoading(true);
        this._visitsService.getVisitCounts(new Date(startDate).getTime(), new Date(endDate).getTime(), searchcriteria, facilityid, centerid)
            .then(data => {
                if (data != null) {
                    this.totalVists = data
                    this.visitsCount = this.totalVists.count.toString();
                }
                else
                    this.visitsCount = 0;
                this.visitsCount = this.visitsCount.toString();
            })
            .then(() => {
                Helpers.setLoading(false);
            });
    }

    getNewTestCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        this.testsCount = null;
        Helpers.setLoading(true);
        this._visitsService.getTestCounts(new Date(startDate).getTime(), new Date(endDate).getTime(), searchcriteria, facilityid, centerid)
            .then(data => {
                if (data != null) {
                    this.totalTest = data
                    this.testsCount = this.totalTest.totalHits.toString();
                }
                else
                    this.testsCount = 0;
                this.testsCount = this.testsCount.toString();
            })
            .then(() => {
                Helpers.setLoading(false);
            });
    }

    getPatientCountByWeek(from, to, searchcriteria, facilityid, centerId): void {
        // console.log('entered patient')
        Helpers.setLoading(true);
        this.patientCountByWeek = [];
        this.patientChart = [];
        this.showPatLoad = true;
        this._patientsService.getPatientCountByTril(searchcriteria, new Date(from).getTime(), new Date(to).getTime(), facilityid, centerId)
            .then(count => this.patientCountByWeek = count)
            .then(() => {
                console.log('count')
                this.something = true;
                var patientCountByWeekForChart = [];

                var tempData;
                var dummy = [];

                var names_array_new = this.patientCountByWeek.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.id.week === b.id.week) {
                            dummy.push(a);
                            return dummy;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.forEach(element => {
                    for (var i = 0; i < dummy.length; i++) {
                        if (element.id.week == dummy[i].id.week) {
                            element.count = dummy[i].count + element.count
                        }
                    }
                })


                var dummyArray = [];

                names_array_new = names_array_new.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.id.year > b.id.year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);


                var currentYear = new Date().getFullYear()

                names_array_new.forEach(element => {
                    if (element.id.year == currentYear) {
                        element.id.week = parseInt(element.id.week) + 1;
                    }
                });


                dummyArray.forEach(data => {
                    if (data.id.year == currentYear) {
                        data.id.week = parseInt(data.id.week) + 1
                    }
                })

                names_array_new = names_array_new.concat(dummyArray)

                this.patientCountByWeek = names_array_new;


                this.patientCountByWeek.forEach(data => {
                    patientCountByWeekForChart.push({ "Patients": data.count, "Week": data.id.week, "Year": data.id.year, "Week_Year": data.id.week + "\n" + '(' + data.id.year + ')' });
                });

                if (patientCountByWeekForChart.length >= 10) {
                    document.getElementById('dignostic_charts_byfilter').classList['value'] = "col-lg-12";
                    document.getElementById('patient_chart_byfilter').classList['value'] = "col-lg-12";

                } else {
                    document.getElementById('dignostic_charts_byfilter').classList['value'] = "col-lg-6";
                    document.getElementById('patient_chart_byfilter').classList['value'] = "col-lg-6";
                }
                this.patientChart = patientCountByWeekForChart

                this.showPatLoad = false;
                Helpers.setLoading(false);

            });
    }

    getPatientCountByWeekUser(searchcriteria,from, to, id): void {
        Helpers.setLoading(true);
        this.patientCountByWeek = [];
        this.patientChart = [];
        this.showPatLoad = true;
        this._patientsService.getPatientCountByWeekForUser(searchcriteria, new Date(from).getTime(), new Date(to).getTime(),id)
            .then(count => this.patientCountByWeek = count)
            .then(() => {
                //  // console.log('patient by week', this.patientCountByWeek)
                var patientCountByWeekForChart = [];
                // for (var index = 1; index <= 12; index++) {
                //     this.selectedYearPatientCountByMonth.push({ "Patients": 0, "Month": this.getMonthName(index.toString()) });
                // }

                var tempData;
                var dummy = [];

                var names_array_new = this.patientCountByWeek.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.id.week === b.id.week) {
                            dummy.push(a);
                            return dummy;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.forEach(element => {
                    for (var i = 0; i < dummy.length; i++) {
                        if (element.id.week == dummy[i].id.week) {
                            element.count = dummy[i].count + element.count
                        }
                    }
                })

                // names_array_new.sort(function(a, b) {
                //     if (a.id.week > b.id.week) {
                //         return 1;
                //     } else if (a.id.week < b.id.week) {
                //         return -1;
                //     }
                //     return 0;
                // });

                var dummyArray = [];

                names_array_new = names_array_new.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.id.year > b.id.year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);

                var currentYear = new Date().getFullYear()

                names_array_new.forEach(element => {
                    if (element.id.year == currentYear) {
                        element.id.week = parseInt(element.id.week) + 1;
                    }
                });

                dummyArray.forEach(data => {
                    if (data.id.year == currentYear) {
                        data.id.week = parseInt(data.id.week) + 1
                    }
                })

                names_array_new = names_array_new.concat(dummyArray)

                this.patientCountByWeek = names_array_new;

                this.patientCountByWeek.forEach(data => {
                    patientCountByWeekForChart.push({ "Patients": data.count, "Week": data.id.week, "Year": data.id.year, "Week_Year": data.id.week + "\n" + '(' + data.id.year + ')' });
                });
                this.patientChart = patientCountByWeekForChart

                Helpers.setLoading(false);
                this.showPatLoad = false;

            });
    }

    getTestCountByWeek(from, to, searchcriteria, facilityid, centerId): void {
        Helpers.setLoading(true);
        this.testCountByWeek = [];
        this.testChartValue = [];
        this.showDigLoad = true;
        this._visitsService.getTestsCountByWeek(searchcriteria, new Date(from).getTime(), new Date(to).getTime(), facilityid, centerId)
            .then(count => this.testCountByWeek = count)
            .then(() => {

                var testCountByWeekForChart = [];
                // for (var index = 1; index <= 12; index++) {
                //     this.selectedYearPatientCountByMonth.push({ "Patients": 0, "Month": this.getMonthName(index.toString()) });
                // }
                this.testCountByWeek.forEach(data => {
                    var isWeekCreated = false;
                    testCountByWeekForChart.forEach(week => {
                        if (week.Week === data.week)
                            isWeekCreated = true;
                    });
                    if (!isWeekCreated) {
                        var objectToPush = {};
                        objectToPush['Week'] = data.week;
                        objectToPush[data.testType] = data.count;
                        objectToPush['Year'] = '20' + data.year;
                        objectToPush['Week_Year'] = data.week + "\n" + '(20' + data.year + ')';
                        testCountByWeekForChart.push(objectToPush);
                    }
                    else {
                        testCountByWeekForChart.forEach(week => {
                            if (week.Week === data.week) {
                                week[data.testType] = data.count;
                            }
                        });
                    }

                });

                testCountByWeekForChart.forEach(testWeek => {
                    testWeek.Week = Number(testWeek.Week)
                })
                var dummyArray = [];

                var names_array_new = testCountByWeekForChart.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.Year > b.Year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                dummyArray.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                names_array_new = names_array_new.concat(dummyArray)

                testCountByWeekForChart = names_array_new;

                this.graphValues = [];
                this.testList.forEach(test => {
                    this.graphValues.push({
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>week [[category]]: <b>[[value]] diagnostics done</b></span>",
                        "fillAlphas": 1.8,
                        "labelText": "[[value]]",
                        "lineAlpha": 1.0,
                        "title": test.name,
                        "type": "column",
                        "color": "#000000",
                        "showHandOnHover": false,
                        "valueField": test.type.toLowerCase(),
                        "colorField": test.color,
                        "fillColors": test.color,
                        "lineColor": test.color
                    });
                })

                this.testChartValue = testCountByWeekForChart;
                this.showDigLoad = false;
                // window.localStorage.setItem("graphValuesTestCountByWeek", JSON.stringify(graphValues));
                // window.localStorage.setItem("testCountByWeek", JSON.stringify(testCountByWeekForChart));
                // window.localStorage.setItem("selectedYear", this.selectedYear.toString());
                // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                //     'assets/demo/default/custom/components/charts/amcharts/charts.testbyweek.1.js');
                Helpers.setLoading(false);

            });
    }

    onTestCountClick(): void {
        if (this.userType != 'User')
            this._router.navigate(['/visits/list'], { queryParams: { from: window.localStorage.getItem('dashStartDate'), to: window.localStorage.getItem('dashEndDate'), facilityId: this.selectedFacility.id, centerId: this.selectedCenter.id } });
        else
            this._router.navigate(['/visits/list'], { queryParams: { from: window.localStorage.getItem('dashStartDate'), to: window.localStorage.getItem('dashEndDate'), userId: this.user.id } });

    }

    onDeviceCountClick(): void {
        this._router.navigate(['/devices/list'], { queryParams: { from: window.localStorage.getItem('dashStartDate'), to: window.localStorage.getItem('dashEndDate'), facilityId: this.selectedFacility.id, centerId: this.selectedCenter.id } });
    }

    ///patients/statistics

    // onPatientCountClick(): void {
    //     this._router.navigate(['patients/chart'], { queryParams: { from: window.localStorage.getItem('dashStartDate'), to: window.localStorage.getItem('dashEndDate'), facilityId: this.selectedFacility.id, centerId: this.selectedCenter.id } });
    // }

    onPatientCountClick(): void {
        this._router.navigate(['patients/chart'], { queryParams: { from: window.localStorage.getItem('dashStartDate'), to: window.localStorage.getItem('dashEndDate'), facilityId: this.selectedFacility.id, centerId: this.selectedCenter.id } })
    }

    facilityChange(value) {
        this.selectedFacility = value;
        this.selectedFacility.organizationCode = value.organizationCode;
        this.centers = value.centers;
        if (this.centers[0].id !== "ALL") {
            let allCenter = new Center();
            allCenter.name = "ALL";
            allCenter.id = "ALL";
            this.centers.splice(0, 0, allCenter);
        }
        this.selectedCenter.id = this.centers[0].id;
        this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
    }

    getPatientCountUser(from, to) {
        this._usersService.getUserPatientCountFilter(this.user.id, new Date(from).getTime(), new Date(to).getTime()).then(data => {
            if (data != null)
                this.patientCount = data.toString();
            else
                this.patientCount = '0'
        })
    }

    getDigCountUser(id, from, to) {
        this._usersService.getDignosticbyUserFilter(id, new Date(from).getTime(), new Date(to).getTime()).then(data => {
            if (data != null)
                this.testsCount = data.toString();
            else
                this.testsCount = '0';
        })
    }

    getTestMapped() {
        this.testMapCount = this.user.testMapping.length
    }

    getVisitCountByUser(userId, from, to) {
        this._visitsService.getVisitByUser(userId, new Date(from).getTime(), new Date(to).getTime()).then(data => {
            if (data != null)
                this.visitsCount = data.count.toString();
            else
                this.visitsCount = '0'
        })
    }

    getTestCountByWeekUser(from, to): void {
        Helpers.setLoading(true);
        this.testCountByWeek = [];
        this.testChartValue = [];
        this.showDigLoad = true;
        this._usersService.getDignosticTestbyUser(new Date(from).getTime(), new Date(to).getTime(), this.user.id)
            .then(count => {
                this.something = true;
                if (count != null) {
                    this.testCountByWeek = count
                }
                else
                    this.testCountByWeek = [];
            })
            .then(() => {
                var testCountByWeekForChart = [];
                this.testCountByWeek.forEach(data => {
                    var isWeekCreated = false;
                    testCountByWeekForChart.forEach(week => {
                        if (week.Week === data.week)
                            isWeekCreated = true;
                    });
                    if (!isWeekCreated) {
                        var objectToPush = {};
                        objectToPush['Week'] = data.week;
                        objectToPush[data.testType] = data.count;
                        objectToPush['Year'] = '20' + data.year;
                        objectToPush['Week_Year'] = data.week + "\n" + '(20' + data.year + ')';
                        testCountByWeekForChart.push(objectToPush);
                    }
                    else {
                        testCountByWeekForChart.forEach(week => {
                            if (week.Week === data.week) {
                                week[data.testType] = data.count;
                            }
                        });
                    }
                });

                testCountByWeekForChart.forEach(testWeek => {
                    testWeek.Week = Number(testWeek.Week)
                })
                var dummyArray = [];

                var names_array_new = testCountByWeekForChart.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.Year > b.Year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                dummyArray.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                names_array_new = names_array_new.concat(dummyArray)

                testCountByWeekForChart = names_array_new;


                this.graphValues = [];
                this.testList.forEach(test => {
                    this.graphValues.push({
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>week [[category]]: <b>[[value]] diagnostics done</b></span>",
                        "fillAlphas": 1.8,
                        "labelText": "[[value]]",
                        "lineAlpha": 1.0,
                        "title": test.name,
                        "type": "column",
                        "color": "#000000",
                        "showHandOnHover": true,
                        "valueField": test.type.toLowerCase(),
                        "colorField": test.color,
                        "fillColors": test.color,
                        "lineColor": test.color
                    });
                });
                this.testChartValue = testCountByWeekForChart
                this.showDigLoad = false;
                window.localStorage.setItem("selectedYear", this.selectedYear.toString());

                Helpers.setLoading(false);

            });
    }

}

