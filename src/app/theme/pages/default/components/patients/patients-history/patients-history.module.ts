import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
import { LayoutModule } from "../../../../../layouts/layout.module";
import { DefaultComponent } from "../../../default.component";
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from "@ngx-translate/core";
import { PatientsHistoryComponent } from "./patients-history.component";
import { DataTableModule, PaginatorModule, DialogModule } from "primeng/primeng";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";
import { VisitsService } from "../../visits/visits.service";
import { PatientVisitCardComponent } from "../patient-visit-card/patient-visit-card.component";
import { OrderModule } from 'ngx-order-pipe';
import { PatientsService } from "../patients.service";

const routes: Routes = [
    {
        path: "",
        component: DefaultComponent,
        children: [
            {
                path: "",
                component: PatientsHistoryComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        LayoutModule,
        TranslateModule.forChild({ isolate: false }),
        DataTableModule,
        PaginatorModule,
        DialogModule,
        OrderModule
    ],
    exports: [RouterModule],
    declarations: [PatientsHistoryComponent, PatientVisitCardComponent],
    providers: [
        TranslateService,
        ScriptLoaderService,
        VisitsService,
        PatientsService
    ]
})
export class PatientsHistoryModule { }
