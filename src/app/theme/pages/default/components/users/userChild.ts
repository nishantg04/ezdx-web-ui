import { Address } from '../facilities/address';
export class UserChild {
    id: string;
    facilityId: string;
    facilityCode: string;
    centerId: string;
    centerCode: string;
    qualification: String = "";
    firstName: string = "";
    lastName: string = "";
    age: number;
    gender: string = "MALE";
    email: string=null;
    phone: string = "";
    dial_code: string = "";
    userType: string = "EZDX";
    userRoles: string[];
    profilePicture: any[];
    profilePictureUrl: string = '';
    addressInfo: Address;
    testMapping: any[];
    displayName: string = '';
    active: boolean = true;
}