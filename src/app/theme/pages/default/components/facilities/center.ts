import { Address } from './address';
import { Geopoint } from './geopoint'

export class Center {
    id: string;
    name: string = "";
    description: string = "";
    centerCode: string;
    createTime: number;
    updateTime: number;
    address: Address;
    geopoint: Geopoint;

}