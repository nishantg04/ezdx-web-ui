import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { PatientsChartComponent } from './patients-chart.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { UsersService } from '../../users/users.service';
import { PatientsService } from '../../patients/patients.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { AmchartModule } from '../../../../../amchartcomponent/amchart.module';
import { FacDropdownModule } from './../../../../../fac-dropdown/fac-dropdown.module'



const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": PatientsChartComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule,
        TranslateModule.forChild({ isolate: false }), AmchartModule, FacDropdownModule

    ], exports: [
        RouterModule
    ], declarations: [
        PatientsChartComponent
    ],
    providers: [
        UsersService,
        PatientsService,
        FacilitiesService,
        TranslateService
    ]

})
export class PatientsChartModule { }
