import { Injectable } from '@angular/core';

import { Headers, Http } from '@angular/http';
import { Country } from '../theme/pages/default/components/facilities/country';
import { Helpers } from '../helpers'
@Injectable()
export class CommonService {
    constructor(private http: Http) { }

    getCountries(): Promise<Country[]> {
        return this.http.get('./assets/app/media/json/country.json')
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getTestType(): Promise<Country[]> {
        return this.http.get('./assets/app/media/json/testType.json')
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        //alert('An error occurred:' + error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}
