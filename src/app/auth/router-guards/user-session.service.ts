import { Injectable } from "@angular/core";
import * as _ from "lodash";

@Injectable()
export class UserSessionService {
    constructor() { }

    // returns if true if the user
    isUserLoggedIn(): Boolean {
        let user = undefined;
        try {
            user = _.get(JSON.parse(localStorage.getItem("currentUser")), "token");
        } catch (error) {
            console.error("ERROR::", error.message);
        }
        return user != undefined;
    }

    // gives the roles user has.
    getUserRoles(): any {
        let roles = [];
        try {
            roles = _.get(JSON.parse(localStorage.getItem("userInfo")), "userRoles");
        } catch (error) {
            console.error("ERROR:: ", error.message);
        }
        return roles;
    }
}
