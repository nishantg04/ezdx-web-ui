import { Component, OnInit, ViewEncapsulation, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Helpers } from "../../../../../../helpers";
import { UploadFileService } from '../../upload/upload-file.service';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./rdt-history.component.html",
    encapsulation: ViewEncapsulation.None,

})
export class RdtHistoryComponent implements OnInit {
    rdtId: any;
    showPython: boolean = false;
    displayListName: any;

    constructor(private _router: Router, private _activeroute: ActivatedRoute, private _uploadFileService: UploadFileService) { }

    ngOnInit() {
        this.urlChecking();

    }

    urlChecking() {
        this._activeroute.queryParams.subscribe(param => {
            this.rdtId = param.rdtId;
            if (param.type) {
                if (param.type == 'PYTHON_SCRIPT') {
                    this.showPython = true;
                    this.displayListName = 'Python'
                }
                if (param.type == 'FIRMWARE') {
                    this.showPython = true;
                    this.displayListName = 'FIRMWARE'
                }
            } else {
                this.displayListName = 'RDT'
            }
        });
    }

}
