import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkinconfigEditComponent } from './skinconfig-edit.component';

import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { InputTextModule, InputTextareaModule, DataTableModule, SharedModule, PaginatorModule, GrowlModule, DialogModule } from 'primeng/primeng';
import { FacDropdownModule } from './../../../../../fac-dropdown/fac-dropdown.module'
import { ConfigService } from '../tests.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": SkinconfigEditComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, InputTextModule, InputTextareaModule, DataTableModule, SharedModule, PaginatorModule, DialogModule, GrowlModule,
        TranslateModule.forChild({ isolate: false }), FacDropdownModule
    ], exports: [
        RouterModule
    ], declarations: [
        SkinconfigEditComponent
    ],
    providers: [
        TranslateService,
        ConfigService
    ]

})
export class SkinconfigEditModule { }


