import {
    Component,
    OnInit,
    ViewEncapsulation,
    AfterViewInit,
    ViewContainerRef
} from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Helpers } from "../../../../../../helpers";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";

import { User } from "../user";
import { UserChild } from "../userChild";
import { Role } from "../role";
import { Center } from "../../facilities/center";
import { Facility } from "../../facilities/facility";
import { Address } from "../../facilities/address";
import { Country } from "../../facilities/country";
import { FacilitiesService } from "../../facilities/facilities.service";
import { UsersService } from "../users.service";
import { Observable } from "rxjs/Observable";
import { ToastsManager } from "ng2-toastr/ng2-toastr";

import { Utils } from "../../utils/utils";
import {
    Ng4FilesStatus,
    Ng4FilesSelected,
    Ng4FilesService,
    Ng4FilesConfig
} from "angular4-files-upload";
import { environment } from "../../../../../../../environments/environment";
import { CommonService } from "../../../../../../_services/common.service";
import { element } from "protractor";
import { Password } from "../password";
import { NgForm } from "@angular/forms";
import { Message } from "primeng/primeng";
import * as _ from 'lodash';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./user-edit.component.html",
    encapsulation: ViewEncapsulation.None
})
export class UserEditComponent implements OnInit, AfterViewInit {
    isPathologist: boolean = false;
    response: any;
    isSignatureUpload: boolean = false;
    invalidFrom: boolean = false;
    currentUserType: string;
    confirmPassword: any;
    userInfo: any;
    user = new User();
    password = new Password();
    changePasswordError: string;
    userId: string;
    userType: string;
    countires: Country[];
    showChangePassword: boolean = false;
    loading: boolean = false;
    msg: string = "";
    wrongPassword: boolean = false;
    errorEmail: string;
    errorPhone: string;
    zeroPhone: string;
    digitLength: string;
    fieldError: string = "";
    successMessage: boolean = false;
    cidaasPassword: boolean = false;
    samePassword: boolean = false;
    country_code: string;
    emailID: boolean = false;
    selectedType: boolean = false;
    profilePictureUrl: any;
    public selectedFiles;
    formData: FormData = new FormData();
    imgLoad: boolean = false;
    submitMsg: boolean = false;
    passcode: String;
    cfpasscode: String;
    signaturemsg: Message[] = [];
    showSignatureUpload: boolean = false;
    file: Blob | String;
    ext: string = "";
    formSign: FormData = new FormData();


    public selectedSignature;
    private signatureImage: Ng4FilesConfig = {
        acceptExtensions: ["jpg", "jpeg", "png"],
        maxFilesCount: 1,
        maxFileSize: 10000000,
        totalFilesSize: 10000000
    };

    constructor(
        private _script: ScriptLoaderService,
        private _usersService: UsersService,
        private _facilitiesService: FacilitiesService,
        private _router: Router,
        private _activeroute: ActivatedRoute,
        vcr: ViewContainerRef,
        private CommonService: CommonService,
        private ng4FilesService: Ng4FilesService,
        private toast: ToastsManager
    ) {
        this.toast.setRootViewContainerRef(vcr);

        this.userInfo = JSON.parse(window.localStorage.getItem("userInfo"));
        this.user.user = new UserChild();
        this.user.user.userRoles = [];
        this.user.user.addressInfo = new Address();
        this.user.user.addressInfo.city = "Bangalore";
        this.user.user.addressInfo.country = "India";
        this.user.user.dial_code = "+91";
        this.country_code = "in";

        this.getCountries();
        this.clearMsg();
        this._script.load(
            ".m-grid__item.m-grid__item--fluid.m-wrapper",
            "assets/demo/default/custom/components/forms/widgets/select2.js"
        );
    }

    ngOnInit() {
        this.getUser();
        this.ng4FilesService.addConfig(this.signatureImage, "sign-config");
    }

    ngAfterViewInit() { }

    clearMsg() {
        this.zeroPhone = "";
        this.digitLength = "";
        this.errorEmail = "";
        this.errorPhone = "";
        this.fieldError = "";
    }

    getCountries(): void {
        this.CommonService.getCountries().then(countries => {
            this.countires = countries;
        });
    }

    getFlag() {
        this.countires.forEach(item => {
            if (item.name == this.user.user.addressInfo.country)
                this.country_code = item.code;
        });
    }

    changePassword() {
        this.showChangePassword = true;
    }

    changeUserPassword(pass) {
        if (
            this.password.oldPassword === undefined ||
            this.password.newPassword === undefined ||
            this.confirmPassword === undefined
        ) {
            this.invalidFrom = true;
            window.setTimeout(() => {
                this.invalidFrom = false;
            }, 4000);
        }
        if (
            this.password.oldPassword === this.password.newPassword &&
            this.password.oldPassword != undefined &&
            this.password.newPassword != undefined
        ) {
            this.samePassword = true;
            window.setTimeout(() => {
                this.samePassword = false;
            }, 4000);
        }
        if (
            this.password.newPassword === this.confirmPassword &&
            this.password.oldPassword != this.password.newPassword &&
            this.password.oldPassword != undefined &&
            this.password.newPassword != undefined &&
            this.confirmPassword != undefined
        ) {
            this._usersService.changeUserPassword(this.password).then(
                res => {
                    this.toast.success("", "Password updated successfully");
                    window.setTimeout(() => {
                        this._router.navigate(["/logout"]);
                    }, 4000);

                    this.showChangePassword = false;
                    this.password = new Password();
                    this.confirmPassword = undefined;
                },
                err => {
                    this.cidaasPassword = true;
                    window.setTimeout(() => {
                        this.cidaasPassword = false;
                    }, 4000);
                    this.password = new Password();
                    this.confirmPassword = undefined;
                }
            );
        }
        if (
            this.password.newPassword != this.confirmPassword &&
            this.password.oldPassword != this.password.newPassword &&
            this.password.oldPassword != undefined &&
            this.password.newPassword != undefined &&
            this.confirmPassword != undefined
        ) {
            this.wrongPassword = true;
            window.setTimeout(() => {
                this.wrongPassword = false;
            }, 4000);
            this.password = new Password();
            this.confirmPassword = undefined;
        }
    }

    pathologistSignature() {
        this.showSignatureUpload = true;
    }

    public signatureUpload(selectedSignature: Ng4FilesSelected): void {
        // console.log("signatuer: ", selectedSignature);
        if (selectedSignature.status === Ng4FilesStatus.STATUS_SUCCESS) {
            this.isSignatureUpload = true;
            this.selectedSignature = Array.from(selectedSignature.files).map(file => {
                this.formSign.append('signature', file);
                let ext = file.name.split('.');
                this.formSign.append('ext', ext[ext.length - 1]);
            });
        } else {
            this.isSignatureUpload = false;
        }

        // console.log("signature: ", this.response.signatuer);
    }


    submitSignature(form: NgForm) {
        Helpers.setLoading(true);
        if (form.valid) {
            this.formSign.append('passcode', form.value.passcode);
            this._usersService.signatureUpload(this.userInfo.id, this.formSign).then(
                res => {
                    this.signaturemsg.push({
                        severity: "success",
                        summary: "Success",
                        detail: "Signature added successfully"
                    });
                    this.clearMessages();
                    this.signatureFormClear();
                    Helpers.setLoading(false);
                },
                rej => {
                    this.signaturemsg.push({
                        severity: "error",
                        summary: "Failure",
                        detail: "failed to save Signature, try again some time."
                    });
                    this.clearMessages();
                    Helpers.setLoading(false);
                }
            );
            this.showSignatureUpload = false;
        }
    }

    signatureFormClear() {
        this.formSign.delete('signature');
        this.formSign.delete('ext');
        this.formSign.delete('passcode');
        this.isSignatureUpload = false;
        this.passcode = '';
        this.cfpasscode = '';
    }

    clearMessages() {
        setTimeout(() => {
            this.signaturemsg = [];
        }, 6000);
    }

    emptyPasswords() {
        this.password = new Password();
        this.confirmPassword = undefined;
    }

    getDialCode() {
        this.countires.forEach(item => {
            if (item.name == this.user.user.addressInfo.country)
                this.user.user.dial_code = item.dial_code;
        });
    }

    countrySelect(event) {
        //alert(JSON.stringify(this.countires[event.target.selectedIndex]))
        var selectedCountryObject = this.countires[event.target.selectedIndex];
        this.user.user.addressInfo.country = selectedCountryObject.name;
        this.user.user.dial_code = selectedCountryObject.dial_code;
        this.country_code = selectedCountryObject.code;
    }

    getUser() {
        Helpers.setLoading(true);
        this._usersService.getUser(this.userInfo.id).then(res => {
            // console.log(res);
            this.user.user = res;
            console.log('user: ', res);
            if (_.indexOf(res.userRoles, 'PATHOLOGIST') >= 0) {
                this.isPathologist = true;
            }
            if (res.age === 0) {
                this.user.user.age = undefined;
            }
            Helpers.setLoading(false);
        });
    }

    private FileConfig: Ng4FilesConfig = {
        acceptExtensions: ["png", "jpeg", "jpg"],
        maxFilesCount: 1,
        maxFileSize: 5120000,
        totalFilesSize: 10120000
    };

    disableFun() {
        if (this._activeroute.snapshot.queryParams["userId"] == undefined)
            this.emailID = true;
        else this.emailID = false;
    }

    valPhone() {
        this.digitLength = "";
        this.fieldError = "";
        if (this.user.user.phone) {
            if (Utils.returnNumbers(this.user.user.phone) === false) {
                this.errorPhone = "ENTER_VALID_PHONE_NUMBER";
            } else this.errorPhone = "";
        } else {
            this.errorPhone = "";
            return true;
        }
    }

    numberChange(Pnumber) {
        if (Pnumber) {
            if (Pnumber.length !== 10) {
                // console.log(Pnumber.length);
                this.digitLength = "ENTER_10_DIGIT_NUMBER";
            } else this.digitLength = "";
            if (!Utils.numberZeroCheck(Pnumber)) {
                this.digitLength = "";
                this.errorPhone = "ENTER_VALID_PHONE_NUMBER";
                document.getElementById("user_phone").focus();
            } else this.errorPhone = "";
        }
    }

    public filesSelect(selectedFiles: Ng4FilesSelected): void {
        var FileUploadPath = selectedFiles.files[0].name;
        var Extension = FileUploadPath.substring(
            FileUploadPath.lastIndexOf(".") + 1
        ).toLowerCase();
        if (
            Extension === "png" ||
            Extension === "jpeg" ||
            Extension === "jpg" ||
            Extension === "gif"
        ) {
            if (selectedFiles.status === Ng4FilesStatus.STATUS_SUCCESS) {
                this.selectedFiles = Array.from(selectedFiles.files).map(file =>
                    this.formData.append("picture", file)
                );
                if (this.user.user.id) this.uploadUserImage();
                else this.submitMsg = true;
            }
        } else {
            alert("upload valid image");
        }
    }

    uploadUserImage() {
        Helpers.setLoading(true);
        if (this.selectedFiles) {
            this._usersService
                .uploadImage(this.user.user.id, this.formData)
                .then(data => {
                    Helpers.setLoading(false);
                    this.user.user.profilePictureUrl = data.profilePictureUrl;
                    Helpers.setLoading(false);
                });
        }
    }

    save() {
        Helpers.setLoading(true);
        this._usersService.update(this.user).then(res => {
            this.toast.success("Profile Updated Successfully");
            Helpers.setLoading(false);
            this._router.navigate(["/index"]);
        });
    }
}
