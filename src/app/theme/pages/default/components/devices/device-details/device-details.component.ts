import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { DevicesService } from '../devices.service';
import { Device } from '../device';
import { Center } from '../../facilities/center';
import { Facility } from '../../facilities/facility';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./device-details.component.html",
    encapsulation: ViewEncapsulation.None,
})

export class DeviceDetailsComponent implements OnInit {
    device = new Device();
    deviceId: string;
    facilities: Facility[];
    centers: Center[];
    facilty: Facility;
    center: Center;
    hideEdit: boolean = false;
    starstate : boolean = false;
    state : any = 'not clicked'

    constructor(private devicesService: DevicesService, private _activeroute: ActivatedRoute) { }

    ngOnInit() {
        if (window.localStorage.getItem('userType') === "Center") {
            this.hideEdit = true;
        }
        this.deviceId = this._activeroute.snapshot.queryParams['deviceId'] || '/';
        if (this.deviceId) {
            this.getDeviceDetails()
        }
    }

    getDeviceDetails() {
        this.device.id = this.deviceId;
        this.devicesService.getDevice(this.device.id)
            .then(device => {
                this.device = device;
                if (this.device.deviceType.includes('_'))
                    this.device.deviceType = this.device.deviceType.replace('_', ' ')
                if (this.device.deviceType.includes('_'))
                    this.device.deviceType = this.device.deviceType.replace('_', ' ')
                this.getFacility()
            })
    }

    getFacility() {
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));

        this.facilities.forEach((org, i) => {
            if (org.id === this.device.organizationId) {
                this.facilty = org;
                this.centers = org.centers;
            }
        });

        this.centers.forEach((center, i) => {
            if (center.id == this.device.centerId) {
                this.center = center;
            }
        })


    }

    clickBut(){
        this.starstate = !this.starstate;
        if(this.starstate)
           this.state = 'clicked'
        else
           this.state = 'not clicked'
    }

}
