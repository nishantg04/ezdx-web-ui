import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { UsersService } from '../users.service';
import { User } from '../user';
import { UserChild } from '../userChild';
import { Helpers } from '../../../../../../helpers';
import { Country } from '../../facilities/country';
import { CommonService } from '../../../../../../_services/common.service'
import { PatientsService } from '../../patients/patients.service';
import { Center } from '../../facilities/center';
import { TestsService } from "./../users-tests/tests.service";
import { concat } from 'rxjs/operator/concat';
import { AmChartsService } from "amcharts3-angular2";
import { setTimeout } from 'timers';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: "./users-details.component.html",
    styleUrls: ["../../../primeng.component.scss", "./users-details.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class UsersDetailsComponent implements OnInit {
    userId: any;
    facilityId: any;
    patientCount: any;
    genderArray: any[];
    ageArray: any[];
    digCount: any;
    lessThanfive: any;
    fiveTotwelve: any;
    twelveTofourty: any;
    fourtyToSixty: any;
    aboveSixty: any;
    ageJson: any;
    genderJson: any;
    user = new User();
    userType: any;
    hideDignostic: boolean = false;
    testCountByWeek: any[];
    countires: Country[];
    years: any;
    selectedYear: any;
    testList: any;
    selectedTestType: any;
    selectedTestList: any;
    country_code: any;
    selectedYearTestCountByMonth: any[];
    testCountByMonth: any[];
    testData: any;
    testTypeArray: any[] = [];
    facilityName: string = '';
    centerName: any;
    facilities: any[] = [];
    centers: any[] = [];
    fLogo: any;
    role: any;
    roles: any;
    alltestList: any[] = [];
    newtests: any[] = [];
    mappedList: any[] = [];
    deviceList: any[] = [];
    mapping: boolean = false;
    testMap: boolean = false;
    checkAllTest: boolean = false;
    dummyArray: any[] = [];
    idList: any[] = [];
    msg: any;
    private chart: any;
    ageList: any[] = [];
    graphValues: any[] = [];
    time: any;
    testChartValue: any[] = [];
    showDelete: boolean = false;
    showDigLoad: boolean = false;
    hideUserEdit: boolean = false;

    constructor(private _router: Router, private _activeroute: ActivatedRoute, private _usersService: UsersService,
        private _script: ScriptLoaderService, private CommonService: CommonService, private _patientsService: PatientsService,
        private _testsService: TestsService, private AmCharts: AmChartsService) {
        this.user.user = new UserChild();
        let currentYear = new Date().getFullYear();
        this.years = [];
        this.years.push("ALL");
        this.country_code = "in"
        for (var index = 2014; index <= currentYear; index++) {
            this.years.push(index);
        }
        this.selectedYear = this.years[this.years.length - 1];
        this.testList = [
            { type: "physical", name: "Physical", color: '#71bf44', tests: [{ name: "Blood Pressure", type: "BLOOD_PRESSURE" }, { name: "Pulse Oximeter", type: "PULSE_OXIMETER" }, { name: "%O2 Saturation", type: "%O2 Saturation" }, { name: "Temperature", type: "TEMPERATURE" }, { name: "Height", type: "Height" }, { name: "Weight", type: "Weight" }, { name: "BMI", type: "BMI" }, { name: "ECG", type: "ECG" }, { name: "Mid Arm Circumference", type: "Mid Arm Circumference" }] },
            { type: "whole_blood_poct", color: '#21C4DF', name: "Whole Blood-POCT", tests: [{ name: "Glucose", type: "BLOOD_GLUCOSE" }, { name: "Hemoglobin", type: "HEAMOGLOBIN" }, { name: "Cholesterol", type: "CHOLESTEROL" }, { name: "Uric Acid", type: "URIC_ACID" }] },
            { type: "whole_blood", name: "Whole Blood", color: '#02abcb', tests: [{ name: "Blood Grouping", type: "BLOOD_GROUPING" }] },
            { type: "whole_blood_rdt", name: "Whole Blood-RDT", color: '#465EEB', tests: [{ name: "Malaria", type: "Malaria" }, { name: "Dengue", type: "Dengue" }, { name: "Typhoid", type: "Typhoid" }, { name: "Hep-B", type: "Hep-B" }, { name: "Hep-C", type: "Hep-C" }, { name: "HIV", type: "HIV" }, { name: "Chikungunya", type: "Chikungunya" }, { name: "Syphilis", type: "Syphilis" }, { name: "Troponin-I", type: "Troponin-I" }] },
            { type: "urine_poct", name: "Urine-POCT", color: '#DA70D6', tests: [{ name: "Pregnancy", type: "Pregnancy" }] },
            { type: "urine_rdt", name: "Urine-RDT", color: '#B22222', tests: [{ name: "Urine Sugar", type: "Urine Sugar" }, { name: "Urine Protein", type: "Urine Protein" }, { name: "Leukocytes", type: "Leukocytes" }, { name: "Urobilinogen", type: "Urobilinogen" }, { name: "Nitrite", type: "Nitrite" }, { name: "pH", type: "pH" }, { name: "Ketone", type: "Ketone" }, { name: "Blood", type: "Blood" }, { name: "Bilurubin", type: "Bilurubin" }, { name: "Specific Gravity", type: "Specific Gravity" }] }
        ];
        this.roles = [
            { name: 'HC Admin', type: 'HC_ADMIN' },
            { name: 'HC Operations', type: 'HC_OPERATIONS' },
            { name: 'HC Manager', type: 'HC_MANAGER' },
            { name: 'Operator', type: 'OPERATOR' },
            { name: 'Doctor', type: 'DOCTOR' },
            { name: 'Technician', type: 'TECHNICIAN' },
            { name: 'Center Admin', type: 'CENTER_ADMIN' },
            { name: 'Clinical User', type: 'CLINICAL_USER' },
            { name: 'Pathologist', type: 'PATHOLOGIST' }
        ];
        this.selectedTestType = this.testList[0].type;
        this.selectedTestList = this.testList[0].tests;
        this.getCountries();
    }

    ngOnInit() {
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
        window.my.namespace.showChartWeek = this.showChartWeek.bind(this);

        this.userId = this._activeroute.snapshot.queryParams['userId'] || '/';
        this.facilityId = this._activeroute.snapshot.queryParams['facilityId'] || '/';
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - 29);
        startDate.setHours(0); startDate.setMinutes(0); startDate.setSeconds(0); startDate.setMilliseconds(0);
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        endDate.setHours(23); endDate.setMinutes(23); endDate.setSeconds(23); endDate.setMilliseconds(0);
        if (this.userId) {
            this.user.user.id = this.userId;
            this.getUserDetails();
            this.getPatientCount();
            this.getRole();
            //this.getDignosticCount();
            this.getHubHistory();
            this.getPatientGenderCount();
            this.getUserPatientbyAge();
            this.getDigCount();
            this.getTestCountByWeek(startDate, endDate);
            this.getTestCountByMonth(startDate, endDate);

        }
    }

    getRole() {
        if (window.localStorage.getItem('userType') == 'Facility')
            this.userType = 'Facility';
        else if (window.localStorage.getItem('userType') == 'Center') {
            this.userType = 'Center';
            this.hideUserEdit = true;
        }
        else if (window.localStorage.getItem('userType') == 'User') {
            this.userType = 'User';            
        }
        else
            this.userType = 'Admin';
    }

    humanize(str) {
        var frags = str.split('_');
        for (var i = 0; i < frags.length; i++) {
            frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
        }
        return frags.join(' ');
    }

    getDateOfISOWeek(w, y) {
        var simple = new Date(y, 0, 1 + (w - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay());
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        //console.log('iso', ISOweekStart)
        return ISOweekStart;
    }

    showChartWeek(event, color) {
        Helpers.setLoading(true);
        var testsWeek = [];
        this.testData = event
        var frmdt = this.getDateOfISOWeek(this.testData.week, this.testData.year);
        frmdt.setFullYear(this.testData.year);
        frmdt.setDate(frmdt.getDate() - frmdt.getDay());
        // console.log(frmdt)
        var todt = new Date(frmdt);
        todt.setDate(todt.getDate() + 6);
        todt.setFullYear(this.testData.year);
        todt.setHours(23);
        todt.setMinutes(23);
        todt.setSeconds(23);
        todt.setMilliseconds(0);
        if (frmdt.getDate() > todt.getDate() && this.testData.week === "1") { //Special check for week #1 year bug
            frmdt.setFullYear(frmdt.getFullYear() - 1);
        }
        this._usersService.getDignosticTestTypebyUser(new Date(frmdt).getTime(), new Date(todt).getTime(), this.userId, this.testData.test.toUpperCase())
            .then(data => {
                this.testTypeArray = data;
                this.testTypeArray.forEach(test => {
                    if (test.testName.includes('_')) {
                        test.testName = test.testName.replace('_', ' ')
                    }
                    test.testName = this.humanize(test.testName)
                    if (test.testName == 'Bmi')
                        test.testName = 'BMI'
                    if (test.testName == 'Hiv')
                        test.testName = 'HIV'
                    if (test.testName == 'Hep b')
                        test.testName = 'Hep B'
                    testsWeek.push({ "Tests": test.count, "Type": test.testName, "color": "#4CAF50" });
                })


                var Stime = this.formateDate(frmdt)
                var Etime = this.formateDate(todt)

                this.time = Stime + ' to ' + Etime

                if (testsWeek) {
                    this.showEachTest(testsWeek, this.time, color)
                    Helpers.setLoading(false);
                    document.getElementById('eachtest').style.display = 'block';
                    window.scroll({ top: 2500, left: 0, behavior: 'smooth' });
                }
            })

    }

    formateDate(d) {
        var m_names = new Array("Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec");

        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();
        return m_names[curr_month] + " " + curr_date + " " + curr_year;
    }

    getDateForm(from, to) {
        var d = new Date(from),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        var sTime = [day, month, year].join('/');

        var e = new Date(to),
            month = '' + (e.getMonth() + 1),
            day = '' + e.getDate(),
            year = e.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        var eTime = [day, month, year].join('/');
        return [sTime, eTime]
    }

    getCountries(): void {
        this.CommonService.getCountries().then(
            countries => {
                this.countires = countries
            }
        );
    }

    getFlag() {
        this.countires.forEach(item => {
            if (item.name == this.user.user.addressInfo.country)
                this.country_code = item.code;
        })
    }

    getDialCode() {
        this.countires.forEach(item => {
            if (item.name == this.user.user.addressInfo.country)
                this.user.user.dial_code = item.dial_code;
        })
    }

    getUserDetails() {
        this._usersService.getUser(this.user.user.id)
            .then(user => this.user.user = user)
            .then(() => {
                //   console.log('tests', this.user.user.testMapping)
                this.getAllTests();
                this.getFlag();
                this.getFacilityDetails()

                if (this.user.user.userRoles) {
                    this.userRole()
                }

            })
    }

    getTestMapped() {

    }

    userRole() {
        if (this.user.user.userRoles) {
            this.user.user.userRoles.forEach(role => {
                this.roles.forEach(element => {
                    if (role == element.type) {
                        var index = this.user.user.userRoles.indexOf(role)
                        this.user.user.userRoles[index] = element.name;
                    }
                });
            });
            this.role = this.user.user.userRoles.toString();

        }
    }

    getFacilityDetails() {
        // console.log('facility id');

        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.forEach(facility => {
            if (facility.id == this.user.user.facilityId) {

                //   console.log(facility)
                this.facilityName = facility.name;
                this.centers = facility.centers;
                this.fLogo = facility.logo;
            }

        })
        if (this.user.user.facilityId == undefined) {
            this.facilityName = 'NA';
        }
        if (this.centers) {
            this.centerDetails()
        } else {
            this.centerName = ""
        }
    }

    centerDetails() {
        this.centers.forEach(center => {
            if (center.id == this.user.user.centerId) {
                this.centerName = center.name
            }
        })
    }

    redirectDetail() {
        this._router.navigate(['/facilities/details'], { queryParams: { facilityId: this.facilityId } })
    }
    redirectList() {
        this._router.navigate(['users/list'])
    }

    getPatientCount() {
        this._usersService.getUserPatientCount(this.user.user.id).then(data => {
            this.patientCount = data.toString();
        })
    }

    getDigCount() {
        this._usersService.getDignosticbyUser(this.user.user.id).then(data => {
            this.digCount = data.toString();
        })
    }

    getPatientGenderCount() {
        var maleCount = 0;
        var femaleCount = 0;
        var otherCount = 0;
        this.genderArray = [];
        this._usersService.getUserPatientbyGender(this.user.user.id)
            .then(data => {
                var tempArray = data;
                this.genderArray.push({ 'title': 'Male', 'value': tempArray.male, "color": "#465EEB" },
                    { 'title': 'Female', 'value': tempArray.female, "color": "#21C4DF" },
                    { 'title': 'Others', 'value': tempArray.other, "color": "#F2F3F8" }
                )
                // console.log(this.genderArray)
                //  this.showGenderChart();
                // localStorage.setItem('PATIENTS_STISTICS_GENDER', JSON.stringify(this.genderArray))
                // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                //     'assets/demo/default/custom/components/charts/amcharts/charts.patientbyage.js');

            })
    }

    getUserPatientbyAge() {
        this.lessThanfive = 0;
        this.fiveTotwelve = 0;
        this.twelveTofourty = 0;
        this.fourtyToSixty = 0;
        this.aboveSixty = 0;
        this.ageList = [];
        this._usersService.getUserPatientbyAge(this.user.user.id)
            .then(data => {
                //<5, 5-12, 12-40,40-60, >60  x >= 0.001 && x <= 0.009
                this.ageArray = data;
                //console.log('age array', this.ageArray)
                this.ageArray.forEach(age => {
                    if (age._id >= 1 && age._id < 5) {
                        this.lessThanfive = this.lessThanfive + age.count;
                    } if (age._id >= 5 && age._id < 12) {
                        this.fiveTotwelve = this.fiveTotwelve + age.count;
                    } if (age._id >= 12 && age._id < 40) {
                        this.twelveTofourty = this.twelveTofourty + age.count;
                    } if (age._id >= 40 && age._id < 60) {
                        this.fourtyToSixty = this.fourtyToSixty + age.count;
                    } if (age._id >= 60 && age._id < 150) {
                        this.aboveSixty = this.aboveSixty + age.count;
                    }
                })
                this.ageList.push({ 'title': '< 5', 'value': this.lessThanfive, "color": "#F2F3F8" },
                    { 'title': '5-12', 'value': this.fiveTotwelve, "color": "#465EEB" },
                    { 'title': '13-40', 'value': this.twelveTofourty, "color": "#21C4DF" },
                    { 'title': '41-60', 'value': this.fourtyToSixty, "color": "#8800F3" },
                    { 'title': '> 60', 'value': this.aboveSixty, "color": "#6AC1E5" }
                )
            })
    }

    getTestCountByWeek(from, to): void {
        Helpers.setLoading(true);
        this.testCountByWeek = [];
        this.testChartValue = [];
        this.showDigLoad = true;
        this._usersService.getDignosticTestbyUser(0, new Date(to).getTime(), this.user.user.id)
            .then(count => {
                if (count != null) {
                    this.testCountByWeek = count
                }
                else
                    this.testCountByWeek = [];
            })
            .then(() => {
                var testCountByWeekForChart = [];
                this.testCountByWeek.forEach(data => {
                    var isWeekCreated = false;
                    testCountByWeekForChart.forEach(week => {
                        if (week.Week === data.week)
                            isWeekCreated = true;
                    });
                    if (!isWeekCreated) {
                        var objectToPush = {};
                        objectToPush['Week'] = data.week;
                        objectToPush[data.testType] = data.count;
                        objectToPush['Year'] = '20' + data.year;
                        objectToPush['Week_Year'] = data.week + "\n" + '(20' + data.year + ')';
                        testCountByWeekForChart.push(objectToPush);
                    }
                    else {
                        testCountByWeekForChart.forEach(week => {
                            if (week.Week === data.week) {
                                week[data.testType] = data.count;
                            }
                        });
                    }
                });

                testCountByWeekForChart.forEach(testWeek => {
                    testWeek.Week = Number(testWeek.Week)
                })
                var dummyArray = [];

                var names_array_new = testCountByWeekForChart.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.Year > b.Year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                dummyArray.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                names_array_new = names_array_new.concat(dummyArray)

                testCountByWeekForChart = names_array_new;


                this.graphValues = [];
                this.testList.forEach(test => {
                    this.graphValues.push({
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>week [[category]]: <b>[[value]] diagnostics done</b></span>",
                        "fillAlphas": 1.8,
                        "labelText": "[[value]]",
                        "lineAlpha": 1.0,
                        "title": test.name,
                        "type": "column",
                        "color": "#000000",
                        "showHandOnHover": true,
                        "valueField": test.type.toLowerCase(),
                        "colorField": test.color,
                        "fillColors": test.color,
                        "lineColor": test.color
                    });
                });
                this.testChartValue = testCountByWeekForChart
                this.showDigLoad = false;
                window.localStorage.setItem("selectedYear", this.selectedYear.toString());
                // console.log(testCountByWeekForChart)
                //  this.showTestChart(testCountByWeekForChart);
                Helpers.setLoading(false);
                // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                //     'assets/demo/default/custom/components/charts/amcharts/charts.testbyweek.3.js');

            });
    }

    getMonthName(month): any {
        var monthString = "Jan";
        if (month == "1")
            monthString = "Jan";
        else if (month == "2")
            monthString = "Feb";
        else if (month == "3")
            monthString = "Mar";
        else if (month == "4")
            monthString = "Apr";
        else if (month == "5")
            monthString = "May";
        else if (month == "6")
            monthString = "Jun";
        else if (month == "7")
            monthString = "Jul";
        else if (month == "8")
            monthString = "Aug";
        else if (month == "9")
            monthString = "Sep";
        else if (month == "10")
            monthString = "Oct";
        else if (month == "11")
            monthString = "Nov";
        else
            monthString = "Dec";

        return monthString;
    }

    getTestCountByMonth(from, to): void {
        Helpers.setLoading(true);
        var pushArray = [];
        var testsMonth = [];
        var dummyArray = [];
        if (this.selectedYear !== "ALL") {
            this._usersService.getDignosticTestbyUser(0, new Date(to).getTime(), this.user.user.id)
                .then(count => {
                    if (count != null)
                        this.testCountByMonth = count
                    else
                        this.testCountByMonth = []
                })
                .then(() => {
                    this.testCountByMonth.forEach(test => {
                        test.year = '20' + test.year
                    })
                    // console.log('test', this.testCountByMonth)
                    this.testCountByMonth.forEach(test => {

                        pushArray.push({ "Tests": test.count, "Type": test.testType });
                    })
                    //console.log('push', pushArray)
                    this.selectedYearTestCountByMonth = [];
                    for (var index = 1; index <= 12; index++) {
                        this.selectedYearTestCountByMonth.push({ "Tests": 0, "Month": this.getMonthName(index.toString()) });
                    }
                    this.testCountByMonth.forEach(month => {
                        if (month.year == this.selectedYear.toString()) {
                            this.selectedYearTestCountByMonth.forEach(element => {
                                if (element.Month == this.getMonthName(month.month)) {
                                    element.Tests = month.count;
                                }
                            });
                        }
                    });
                    this.testCountByMonth.forEach(test => {
                        testsMonth.push({ "Tests": test.count, "Type": test.testType });
                    })

                    var tempTestArray = testsMonth;
                    var physicalValue = 0;
                    var urinepoctValue = 0;
                    var urinerdtValue = 0;
                    var wbcrdtValue = 0;
                    var wbcpoctValue = 0;

                    tempTestArray.forEach(test => {
                        if (test.Type == "physical") {
                            test.Type = "Physical"
                            physicalValue = physicalValue + test.Tests;
                        }
                        if (test.Type == "urine_poct") {
                            test.Type = "Urine Poct"
                            urinepoctValue = urinepoctValue + test.Tests;
                        }
                        if (test.Type == "urine_rdt") {
                            test.Type = "Urine RDT"
                            urinerdtValue = urinerdtValue + test.Tests;
                        }
                        if (test.Type == "whole_blood_rdt") {
                            test.Type = "Whole Blood RDT"
                            wbcrdtValue = wbcrdtValue + test.Tests;
                        }
                        if (test.Type == "whole_blood_poct") {
                            test.Type = "Whole Blood POCT"
                            wbcpoctValue = wbcpoctValue + test.Tests;
                        }
                    })
                    tempTestArray.forEach(data => {
                        if (data.Type == "Physical")
                            data.Tests = physicalValue
                        if (data.Type == "Urine Poct")
                            data.Tests = urinepoctValue
                        if (data.Type == "Whole Blood RDT")
                            data.Tests = wbcrdtValue
                        if (data.Type == "Whole Blood POCT")
                            data.Tests = wbcpoctValue
                        if (data.Type == "Urine RDT")
                            data.Tests = urinerdtValue
                    })

                    var names_array_new = tempTestArray.reduceRight(function(r, a) {
                        r.some(function(b) {
                            if (a.Type == b.Type) {
                                dummyArray.push(a);
                                return dummyArray;
                            }
                        }) || r.push(a); return r;

                    }, []);
                    tempTestArray = names_array_new;
                    if (tempTestArray)
                        window.localStorage.setItem('testsForMonth', JSON.stringify(tempTestArray));
                    //  window.localStorage.setItem("testCountByMonth", JSON.stringify(this.selectedYearTestCountByMonth));
                    // window.localStorage.setItem("selectedYear", this.selectedYear.toString());
                    this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                        'assets/demo/default/custom/components/charts/amcharts/charts.patientbyweek.3.js');
                    Helpers.loadStyles('.m-grid__item.m-grid__item--fluid.m-wrapper', [
                        '//www.amcharts.com/lib/3/plugins/export/export.css']);
                    Helpers.setLoading(false);

                });
        }
    }

    userEdit() {
        this._router.navigate(['/users/update'], { queryParams: { userId: this.user.user.id } })
    }

    getAllTests() {
        //console.log('entered get test')
        this.newtests = [];
        this.alltestList = [];
        Helpers.setLoading(true)
        this._testsService.getAllTests().then(res => {
            Helpers.setLoading(false)
            this.alltestList = res;
            this.alltestList.forEach(test => {
                if (test.active) {
                    this.newtests.push(test)
               }
            })
            // console.log(this.newtests[0])
            this.newtests.forEach(test => {
                test.testIconUrl = "./assets/app/media/img/test/" + test.testName + ".png"
            })
            this.newtests.forEach(test => {
                if (test.testName.includes('_')) {
                    test.testName = test.testName.replace('_', ' ')
                }
                if (test.testName.includes('_')) {
                    test.testName = test.testName.replace('_', ' ')
                }
                if (test.testName == 'URINE') {
                    test.testName = 'URINE 2P'
                }
                if (test.testName == 'DENGUE') {
                    test.testName = 'DENGUE ANTIGEN'
                }
                if (test.testType.includes('_')) {
                    test.testType = test.testType.replace('_', ' ')
                }
                if (test.testType.includes('_')) {
                    test.testType = test.testType.replace('_', ' ')
                }
            })
            var testDataArray = [];
            this.mappedList = [];
            this.newtests.forEach(test => {
                this.user.user.testMapping.forEach(id => {
                    if (id == test.id) {
                        testDataArray.push(test.id)
                    }
                })
            })
            this.mappedList = testDataArray;
            if (this.mappedList) {
                this.checkOpacity();
            }
        });
    }

    getHubHistory() {
        this.deviceList = [];
        this._usersService.getDeviceHistoryByUser(this.user.user.id)
            .then(data => {
                this.deviceList = data.results;
                // console.log('data for device', this.deviceList)
            })
    }

    tableSelectClick(type) {
        if (type == "chart") {
            document.getElementById('m_tabs_6_3').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_4').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_5').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_1').classList['value'] = "tab-pane active show"

            document.getElementById('chart_link').classList['value'] = "nav-link m-tabs__link active show"
            document.getElementById('log_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('test_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('hub_link').classList['value'] = "nav-link m-tabs__link"
        }
        if (type == "log") {
            document.getElementById('m_tabs_6_1').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_4').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_3').classList['value'] = "tab-pane active show"
            document.getElementById('eachtest').style.display = 'none'
            document.getElementById('m_tabs_6_5').classList['value'] = "tab-pane"

            document.getElementById('chart_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('log_link').classList['value'] = "nav-link m-tabs__link active show"
            document.getElementById('test_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('hub_link').classList['value'] = "nav-link m-tabs__link"
        }
        if (type == "details") {
            document.getElementById('m_tabs_6_3').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_4').classList['value'] = "tab-pane active show"
            document.getElementById('m_tabs_6_1').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_5').classList['value'] = "tab-pane"

            document.getElementById('chart_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('log_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('test_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('hub_link').classList['value'] = "nav-link m-tabs__link active show"
        }
        if (type == "test") {
            this.checkOpacity();
            document.getElementById('m_tabs_6_3').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_5').classList['value'] = "tab-pane active show"
            document.getElementById('m_tabs_6_1').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_4').classList['value'] = "tab-pane"

            document.getElementById('chart_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('log_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('hub_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('test_link').classList['value'] = "nav-link m-tabs__link active show"

        }
    }

    checkOpacity() {
        // Helpers.setLoading(true)
        this.newtests.forEach(test => {
            this.mappedList.forEach(id => {
                if (test.id == id) {
                    document.getElementById(test.testName).style.opacity = '0.8'
                }
            })
        })
        //Helpers.setLoading(false)
    }

    sTest(testvalue) {
        testvalue.selected = !testvalue.selected;
        this.newtests.forEach(test => {
            if (test.id == testvalue.id) {
                test.selected = testvalue.selected;
            }
        })
    }

    oncheckAllTest(event): void {
        this.user.user.testMapping = [];
        this.checkAllTest = event.target.checked;
        this.newtests.forEach((test, i) => {
            test.selected = event.target.checked;
            if (test.selected == true)
                this.dummyArray.push(test.id)
            if (test.selected == false) {
                var index = this.dummyArray.indexOf(test.id);
                this.dummyArray.splice(index, 1);
            }
        });
        this.idList = this.dummyArray;

    }

    checkSave() {
        Helpers.setLoading(true);
        var arrayV = []
        this.user.user.testMapping = [];
        this.newtests.forEach(test => {
            if (test.selected == true) {
                arrayV.push(test.id)
            }
        })
        this.user.user.testMapping = arrayV;
        if (this.user.user.testMapping.length != this.newtests.length) {
            this.checkAllTest = false;
        } else {
            this.checkAllTest = true;
        }
        this._usersService.updateTestMap("set", this.user.user.id, this.user.user.testMapping)
            .then((data) => {
                this.user.user.testMapping = [];
                Helpers.setLoading(false);
                if (data['testMapping'])
                    this.user.user.testMapping = data['testMapping'];
                this.msg = "TEST_MAPPED_SUCCESSFULLY";
                setTimeout(() => {
                    this.msg = null;
                }, 1000);
            });
    }

    enableMapping(value) {
        this.mapping = !this.mapping
        if (this.mapping == false) {
            this.userOpacity()
            document.getElementById('noMap').style.display = 'block'
        } else {
            document.getElementById('noMap').style.display = 'none'
            this.testTomap()
        }
    }

    userOpacity() {
        this.newtests.forEach(test => {
            this.user.user.testMapping.forEach(id => {
                if (test.id == id) {
                    document.getElementById(test.testName).style.opacity = '0.8'
                }
            })
        })
    }

    testTomap() {
        this.user.user.testMapping.forEach(element => {
            this.newtests.forEach(test => {
                if (element === test.id) {
                    test.selected = true;
                }
            });
        });
        if (this.newtests.length == this.user.user.testMapping.length) {
            this.checkAllTest = true;
        } else {
            this.checkAllTest = false;
        }
    }



    showEachTest(testsWeek, selectedYear, color) {
        let eachChart = this.AmCharts.makeChart("each_test_chart", {
            "type": "serial",
            "categoryField": "Type",
            "maxSelectedTime": -2,
            "angle": 30,
            "rotate": true,
            "startDuration": 1,
            "startEffect": "easeOutSine",
            "processCount": 998,
            "processTimeout": -2,
            "theme": "light",
            "categoryAxis": {
                "gridPosition": "start",
                "title": "Test Type",
                "gridColor": '#aaa'
            },
            "trendLines": [],
            "graphs": [{
                "balloonFunction": function(graphDataItem, graph) {
                    //  console.log('ballon', graphDataItem)
                    return "<span style='font-size:14px'> " + graphDataItem.category + ": <b>" + graphDataItem.values.value + "</b></span><br/><span>" + selectedYear + "</span>"
                },
                "fillAlphas": 1,
                "id": "AmGraph-1",
                "title": "Diagnostics done for each test type ",
                "type": "column",
                "valueField": "Tests",
                "labelText": "[[value]]",
                "lineColor": color,
                "fillColors": color,
                "showHandOnHover": false
            }],
            "guides": [],
            "valueAxes": [{
                "id": "ValueAxis-1",
                "stackType": "regular",
                "title": "Diagnostics Tests",
                "minimum": 0,
                "gridColor": '#aaa'
            }],
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": false,
                "useGraphSettings": true
            },

            "dataProvider": testsWeek,
            "listeners": [{
                "event": "clickGraphItem",
                "method": function(event) {
                    //window.localStorage.setItem("SelectedMonthDiagnosis", event.item.category);
                    // my.namespace.publicFuncPatientWeek(event.item.category, "patient");
                }
            }]
        });
    }


    getDateRangeOfWeek(w, y) {
        var week = w.split("\n")
        var simple = new Date(y, 0, 1 + (week[0] - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay());
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        var ISOweekEnd = new Date(ISOweekStart);
        ISOweekEnd.setDate(ISOweekEnd.getDate() + 6);
        ISOweekEnd.setHours(23);
        ISOweekEnd.setMinutes(23);
        ISOweekEnd.setSeconds(23);
        ISOweekEnd.setMilliseconds(0);
        return this.formateDate(ISOweekStart) + " to " + this.formateDate(ISOweekEnd);
    }

    deleteUser() {
        Helpers.setLoading(true)
        this._usersService.delete(this.user.user.id)
            .then(data => {
                Helpers.setLoading(false)
                this.showDelete = true;
                setTimeout(() => {
                    this.showDelete = false
                    this._router.navigate(['users/list'])
                }, 1000)

            })
    }

}

