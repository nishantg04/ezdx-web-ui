import { RouteGuard } from "./router.guards";
import { TestBed } from "@angular/core/testing";
import { UserSessionService } from "./user-session.service";
import { Router } from "@angular/router";

describe('Router Guard::', () => {

    beforeEach(() => {

        const routerSpy = jasmine.createSpyObj('Router', ['navigate'])

        TestBed.configureTestingModule({
            providers: [
                UserSessionService,
                { provide: Router, value: routerSpy }

            ]
        })
    })

    describe('canActivate: ', () => {

    })
})