import { Test } from './test';
export class Visit {
    id: string;
    patientId: string;
    userId: string;
    centerId: string;
    status: string;
    syncTime: string;
    tests: Test[];
}