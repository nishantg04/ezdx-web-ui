import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { ConfigService } from '../tests.service';
import { truncate } from 'fs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {UsersService} from '../../users/users.service'
import { Utils } from '../../utils/utils';
import { Facility } from '../../facilities/facility';
import {FacilitiesService} from '../../facilities/facilities.service'
@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./skinconfig.component.html",
    styleUrls: ['../../../primeng.component.scss', './skinconfig.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SkinconfigComponent implements OnInit {

    errormin: any;
    errormax: any;
    mandate: { AADHAR_CARD: boolean; DATE_OF_BIRTH: boolean; CITY_OR_VILLAGE: boolean; POSTAL_CODE: boolean; EMAIL: boolean; ADDRESS: boolean; STATE: boolean; COUNTRY: boolean; };
  idList: any;
  dummyArray: any=[];
  mandatoryArr: any=[];
  checkAllTest: boolean=true;
  public config:any={};
  selectedFacility: Facility;
  public showtests:boolean=false;
  public showunit:boolean=false;
  public showlang:boolean=true;
  public showUpperAndLowerLimit:boolean=false;
  public alltestList:any=[];
  public newtests:any=[];
  public displayDialog:boolean=false;
  public testData:any={};
  public showMandatoryFields:boolean=false;
  public fieldLabelstate:any='';
  chosen : any;
  testRangeArray : any[] = [];
  testUnitRangeArray : any[] = [];
  noCustomer : boolean = true;
  green : string = '#34bfa3';
  yellow : string = '#ffb822';
  gray : string = 'grey';
  isEditable : boolean = true;
  noUser : boolean = false;
  selectedBpRanges : any = {};
  selectedTempRanges : any = {};
  selectedOtherRanges : any = {};
  selectedPORanges : any = {};
  selectedCholRanges : any = {};
  selectedHURanges : any = {};
  selectedBGRanges : any = {};
  selectedUARanges : any = {};
  isEdit : boolean = true;
  confId : any;
  disableCustomer : boolean = false;
  lowLimit : boolean = false;
  upLimit : boolean = false;
  heightoption:any[]=[
      {label:'ft',value:'ft'},
      {label:'cms',value:'cms'},
  ]
  weightoption:any[]=[
    {label:'Kgs',value:'Kgs'},
    {label:'lbs',value:'lbs'},
  ]
    tempOption:any[]=[
        {label:'°F',value:'°F'},
        {label:'°C',value:'°C'}
    ]
    poctOption:any[]=[
        {label:'mg/dL',value:'mg/dL'},
        {label:'mmol/L',value:'mmol/L'},
    ]

    bpConvBool : boolean = false;
    showReport : boolean = false;

  constructor(private _testsService:ConfigService, private _router:Router, private _activeroute:ActivatedRoute,
     private _usersService : UsersService, private facilitiesService : FacilitiesService) { }
  @ViewChild('numberTwo') numberTwo;
  @ViewChild('numberOne') numberOne;
  ngOnInit() {
    this._activeroute.queryParams.subscribe(param => {
        this.confId=param.confId;
     });
    this.config={
      language:'ENGLISH',
      testsMapped:[],
      testUnitMapping:[],
      testRanges:[],
      facilityId : null,
      configType:'CUSTOM',
      customLogo : false,
      customRanges : false
    }
    this.mandate={
        AADHAR_CARD:false,
        DATE_OF_BIRTH:false,
        CITY_OR_VILLAGE:false,
        POSTAL_CODE:false,
        EMAIL:false,
        ADDRESS:false,
        STATE:false,
        COUNTRY:false,
    }
    if(this.confId)
       this.getconfigfiledetail(this.confId)
    else
      this.getdefaultconfig()    
  }

  getconfigfiledetail(id) {
    Helpers.setLoading(true);
    this._testsService
        .getconfigfiledetail(id)
        .then(res => {            
            this.config=res; 
            this.noCustomer=false;   
            if(this.config.facilityId)
               this.disableCustomer = true;
            else 
               this.disableCustomer = false;
            // if(this.config.customLogo == undefined && this.config.customRanges== undefined){
            //     this.config.customLogo = true;
            //     this.config.customRanges = true;
            // }
            if(this.config.facilityId){
                this.getFacility(this.config.facilityId)
            }
            this.fieldLabels.STATE=this.config.fieldLabels.STATE;  
            this.fieldLabels.CITY_OR_VILLAGE=this.config.fieldLabels.CITY_OR_VILLAGE;  
            if (this.config.mandatoryFields!=undefined) {
                this.mandatoryArr= this.config.mandatoryFields;
            }
           
            Object.keys(this.mandate).map((field)=>{
                if (this.mandatoryArr.indexOf(field)>-1) {
                    this.mandate[field]=true;
                }
            });
            Helpers.setLoading(false);
        }).then(()=>{
          this.getAllTests();
        });
}


  getdefaultconfig() {
    Helpers.setLoading(true);
    this._testsService
        .getdefaultconfig()
        .then(res => {            
            this.config=res; 
            this.fieldLabels.STATE=this.config.fieldLabels.STATE;  
            this.fieldLabels.CITY_OR_VILLAGE=this.config.fieldLabels.CITY_OR_VILLAGE;  
            this.mandatoryArr= this.config.mandatoryFields;
            this.config.customLogo = false;
            this.config.customRanges = false;    
            
            delete this.config.id;
            delete this.config.createTime;
            delete this.config.updateTime;
            this.config.configType='CUSTOM';
            console.log(this.config) 
            Object.keys(this.mandate).map((field)=>{
                if (this.mandatoryArr.indexOf(field)>-1) {
                    this.mandate[field]=true;
                }
            });
            Helpers.setLoading(false);
        }).then(()=>{
          this.getAllTests();
        });
}

getFacility(id){
    if (id != undefined && id) {
        this.facilitiesService.getFacilityById(id)
            .then((data) => {
               this.selectedFacility = data;
              
            })
    }
}



  testChecked() {
    let count = 0;
    this.newtests.forEach(test => {
        if (test.selected) count++;
    })
    if (this.newtests.length === count) {
        this.checkAllTest = true;
    } else {
        this.checkAllTest = false;
    }
}

getAllTests() {
    this.newtests = [];
    this.alltestList = [];
    this.testRangeArray = [];
    this.testUnitRangeArray = [];
    this._testsService.getAllTests().then(res => {
        this.alltestList = res;
        this.alltestList.forEach(test => {
            if (test.active) {
                this.newtests.push(test)
           }
        })       
        this.newtests.forEach(test => {
            test.testIconUrl = "./assets/app/media/img/test/" + test.testName + ".png"
        })
        this.newtests.forEach(test => {
            if (test.testName.includes('_')) {
                test.testName = test.testName.split('_').join(' ')
            }
            if (test.testName == 'URINE') {
                test.testName = 'URINE 2P'
            }
            if (test.testName == 'DENGUE') {
                test.testName = 'DENGUE ANTIGEN'
            }
            if (test.testType.includes('_')) {
                test.testType = test.testType.split('_').join(' ')
            }
            this.isEditable = false;
            
        })  
        this.newtests.forEach(test=>{
            if(test.testName == 'PULSE OXIMETER' || test.testName == 'BMI' || test.testName == 'BLOOD GLUCOSE' || test.testName == 'HEMOGLOBIN'
            || test.testName == 'CHOLESTEROL' || test.testName == 'URIC ACID' || test.testName == 'HEIGHT' || test.testName == 'WEIGHT' || test.testName == 'TEMPERATURE' || test.testName == 'BLOOD PRESSURE' || test.testName == 'MID ARM CIRCUMFERENCE' ){               
                    this.testRangeArray.push(test)                
            } 
            if(test.testName == 'PULSE OXIMETER' || test.testName == 'BMI' || test.testName == 'BLOOD GLUCOSE' || test.testName == 'HEMOGLOBIN'
            || test.testName == 'CHOLESTEROL' || test.testName == 'URIC ACID'  || test.testName == 'TEMPERATURE' || test.testName == 'BLOOD PRESSURE' || test.testName == 'MID ARM CIRCUMFERENCE' ){               
                    this.testUnitRangeArray.push(test)                
            } 
        })  
        this.testRangeArray.forEach(test=>{
            if(test.testName == 'MID ARM CIRCUMFERENCE' && test.testName == 'HEMOGLOBIN' && test.testName == 'BMI')
               this.isEdit = false;
            else
                this.isEdit = true;
        })       
        this.newtests.forEach(element => {
            element.selected = false;
            element.unit='NA';
            element.conversionFactor=1;
            this.config.testsMapped.map((testsmapped)=>{
                if(testsmapped==element.id){
                    element.selected = true;
                }
            })
            this.config.testUnitMapping.map((testsunitmapped)=>{
                if((testsunitmapped.testName).split('_').join(' ')==element.testName){
                    element.unit = testsunitmapped.testUnit;
                    element.conversionFactor = testsunitmapped.conversionFactor;
                }
            })
            this.config.testRanges.map((testRange)=>{
                if((testRange.testName).split('_').join(' ')==element.testName){
                    element.ranges = testRange.ranges;
                }
            })
            
        });
    });
}

oncheckAllTest(event): void {
  this.checkAllTest = event.target.checked;
  this.newtests.forEach((test, i) => {
      test.selected = event.target.checked;
      if (test.selected == true)
          this.dummyArray.push(test.id)
      if (test.selected == false) {
          var index = this.dummyArray.indexOf(test.id);
          this.dummyArray.splice(index, 1);
      }
  });
  this.idList = this.dummyArray; 
  this.config.testsMapped=this.idList;
}

sTest(testvalue) {
  testvalue.selected = !testvalue.selected;
  this.newtests.forEach(test => {
      if (test.id == testvalue.id) {
          test.selected = testvalue.selected;
      }
  })
  let arr=[]
  this.newtests.map((item)=>{
      if(item.selected==true){
        arr.push(item.id);
      }
  });
  //console.log(arr);
  this.config.testsMapped=arr;
  
}

//show tests
  nextToTest(lang):void{
    if(this.config.facilityId){
        var searchcriteria = 'FACILITY'
        this. userList(searchcriteria, this.config.facilityId)
    }
   
    //this.getAllTests()       
  //  this.config.language=lang;
  
  }

  userList(searchcriteria, id) {   
    this._usersService.getUsersFiltered(0, 0, searchcriteria, id, 1,10, '', 'asc')
        .then(users => {
            if(users){
                this.showlang=false;
                this.showtests=true;
                if(this.disableCustomer == false && this.confId){
                    this.assignCustomer()
                }else{
                    document.getElementById('step_one').style.backgroundColor = this.green;     
                    document.getElementById('step_two').style.backgroundColor = this.yellow;
                }
               
            }else{
               this.noUser = true;
            }           
        })
    }

    assignCustomer(){
        console.log(this.config)
        Helpers.setLoading(true);
        this._testsService.Assignconfigfile(this.config).then(res=>{
            console.log(res);
            Helpers.setLoading(false);
            this.showlang=false;
            this.showtests=true;
            document.getElementById('step_one').style.backgroundColor = this.green;     
            document.getElementById('step_two').style.backgroundColor = this.yellow;
        })
    }

 
//back to step 1
  backToLang():void{
      console.log(this.config)
    this.showtests=false;
    this.showlang=true;
    document.getElementById('step_two').style.backgroundColor = this.gray;
    document.getElementById('step_one').style.backgroundColor = this.yellow;
  }

//show unit
  nextToUnit():void{
    this.showtests=false;
    this.showunit=true;
    document.getElementById('step_three').style.backgroundColor = this.yellow;
    document.getElementById('step_two').style.backgroundColor = this.green; 
  }

//back to tests (step 2)
  backToTest():void{
    this.showunit=false;
    this.showtests=true;    
    document.getElementById('step_three').style.backgroundColor = this.gray;
    document.getElementById('step_two').style.backgroundColor = this.yellow;
  }

//show range
nextToRange(arr):void{
    console.log(arr)
    console.log(this.config)
  this.showunit=false;
  this.showUpperAndLowerLimit=true;  
  document.getElementById('step_four').style.backgroundColor = this.yellow;
  document.getElementById('step_three').style.backgroundColor = this.green;
  let rangearr=[];
  this.testRangeArray.map((item)=>{
    if (item.testName == 'URINE 2P') {
        item.testName = 'URINE'
    }
    if (item.testName == 'DENGUE ANTIGEN') {
        item.testName = 'DENGUE'
    }
    rangearr.push({"testName":item.testName.split(' ').join('_'),"testUnit":item.unit,"conversionFactor":item.conversionFactor})
     
  });
  this.config.testUnitMapping=rangearr;
  //console.log(this.config)
}

//back to unit (step 3)
backToUnit():void{
  this.showUpperAndLowerLimit=false;
  this.showunit=true;    
  document.getElementById('step_four').style.backgroundColor = this.gray;
  document.getElementById('step_three').style.backgroundColor = this.yellow;  
}

showReportScr() :void{
    this.showReport = true;
    this.showUpperAndLowerLimit=false;
    document.getElementById('step_five').style.backgroundColor = this.yellow;
    document.getElementById('step_four').style.backgroundColor = this.green;
}

//show mandatory fields
showMandatory():void{    
    console.log(this.config)
    this.showReport=false;
    this.showMandatoryFields=true;
  document.getElementById('step_five').style.backgroundColor = this.green;
  document.getElementById('step_six').style.backgroundColor = this.yellow;
    
}

//back to range

backToRange(){
    this.showReport=false;
    this.showUpperAndLowerLimit=true;
  document.getElementById('step_five').style.backgroundColor = 'grey';
  document.getElementById('step_four').style.backgroundColor = this.yellow;
    
}

backToReport(){
    this.showReport = true;
    this.showMandatoryFields = false;
    document.getElementById('step_six').style.backgroundColor = 'grey';
    document.getElementById('step_five').style.backgroundColor = this.yellow;
}


setRangeforTest(test){
    this.lowLimit = false 
    this.upLimit = false; 
    this.testData={};
    this.testData=test;
    console.log(this.testData)
    this.displayDialog=true;
    if (this.testData.testName!='BLOOD PRESSURE' && this.testData.testName!='URIC ACID' && this.testData.testName!='HEMOGLOBIN' && this.testData.testName!='BLOOD GLUCOSE' ) {
       if(this.testData.testName == 'BMI')
        //  this.bmiValue()
        this.commonValue(this.selectedOtherRanges)
       if(this.testData.testName == 'PULSE OXIMETER')
        //  this.poValue()
        this.commonValue(this.selectedPORanges)
       if(this.testData.testName == 'CHOLESTEROL')
        //   this.cholValue()
          this.commonValue(this.selectedCholRanges)
      if(this.testData.testName == 'TEMPERATURE')
          this.commonValue(this.selectedTempRanges)
    }
    else if (this.testData.testName=='URIC ACID' || this.testData.testName=='HEMOGLOBIN') {
        if(this.testData.testName=='URIC ACID')
          this.uricValue()
        if(this.testData.testName=='HEMOGLOBIN')
          this.hemoValue()
    }
    else if (this.testData.testName=='BLOOD PRESSURE') {
       this.bpValue()
    }
    else if (this.testData.testName=='BLOOD GLUCOSE') {
        this.glucoValue()
    }
}

    hemoValue(){
        if(this.selectedHURanges.menlowerlimit)
          this.gen.men.lowerlimit=this.selectedHURanges.menlowerlimit;
        else
         this.gen.men.lowerlimit=this.testData.ranges.men[0];       
            
        if(this.selectedHURanges.menupperlimit)
             this.gen.men.upperlimit=this.selectedHURanges.menupperlimit;
        else
            this.gen.men.upperlimit=this.testData.ranges.men[1];           

        if(this.selectedHURanges.womenlowerlimit)
            this.gen.women.lowerlimit=this.selectedHURanges.womenlowerlimit;
        else
            this.gen.women.lowerlimit=this.testData.ranges.women[0];       
           
        if(this.selectedHURanges.womenupperlimit)
           this.gen.women.upperlimit=this.selectedHURanges.womenupperlimit; 
        else
            this.gen.women.upperlimit=this.testData.ranges.women[1];     
                 
    }

    uricValue(){
        if(this.selectedUARanges.menlowerlimit)
          this.gen.men.lowerlimit=this.selectedUARanges.menlowerlimit;
        else
         this.gen.men.lowerlimit=this.testData.ranges.men[0];       
            
        if(this.selectedUARanges.menupperlimit)
            this.gen.men.upperlimit=this.selectedUARanges.menupperlimit;
        else
            this.gen.men.upperlimit=this.testData.ranges.men[1];                 

        if(this.selectedUARanges.womenlowerlimit)
           this.gen.women.lowerlimit=this.selectedUARanges.womenlowerlimit;
        else
            this.gen.women.lowerlimit=this.testData.ranges.women[0];                  
        if(this.selectedUARanges.womenupperlimit)
            this.gen.women.upperlimit=this.selectedUARanges.womenupperlimit;    
        else
            this.gen.women.upperlimit=this.testData.ranges.women[1];        
    }

    bpValue(){
        if(this.selectedBpRanges.systolicLower)
        this.bpranges.systolicLower=this.selectedBpRanges.systolicLower;
      else
        this.bpranges.systolicLower=this.testData.ranges.systolic[0];
      if(this.selectedBpRanges.systolicUpper)
         this.bpranges.systolicUpper=this.selectedBpRanges.systolicUpper;
      else
        this.bpranges.systolicUpper=this.testData.ranges.systolic[1];
      if(this.selectedBpRanges.diastolicLower)
        this.bpranges.diastolicLower=this.selectedBpRanges.diastolicLower;
      else
        this.bpranges.diastolicLower=this.testData.ranges.diastolic[0];
      if(this.selectedBpRanges.diastolicUpper)
        this.bpranges.diastolicUpper=this.selectedBpRanges.diastolicUpper;
      else
        this.bpranges.diastolicUpper=this.testData.ranges.diastolic[1];
    }

    bmiValue(){
        if(this.selectedOtherRanges.lowerlimit)
        this.gen.lowerlimit=this.selectedOtherRanges.lowerlimit;
      else
        this.gen.lowerlimit=this.testData.ranges.result[0];
      if(this.selectedOtherRanges.upperlimit)
        this.gen.upperlimit=this.selectedOtherRanges.upperlimit;
      else
        this.gen.upperlimit=this.testData.ranges.result[1];
    }

    poValue(){
        if(this.selectedPORanges.lowerlimit)
        this.gen.lowerlimit=this.selectedPORanges.lowerlimit;
      else
        this.gen.lowerlimit=this.testData.ranges.result[0];
      if(this.selectedPORanges.upperlimit)
        this.gen.upperlimit=this.selectedPORanges.upperlimit;
      else
        this.gen.upperlimit=this.testData.ranges.result[1];
    }

    cholValue(){
        if(this.selectedCholRanges.lowerlimit)
        this.gen.lowerlimit=this.selectedCholRanges.lowerlimit;
      else
        this.gen.lowerlimit=this.testData.ranges.result[0];
      if(this.selectedCholRanges.upperlimit)
        this.gen.upperlimit=this.selectedCholRanges.upperlimit;
      else
        this.gen.upperlimit=this.testData.ranges.result[1];
    }

    commonValue(value){
      if(value.lowerlimit)
        this.gen.lowerlimit=value.lowerlimit;
      else
        this.gen.lowerlimit=this.testData.ranges.result[0];
      if(value.upperlimit)
        this.gen.upperlimit=value.upperlimit;
      else
        this.gen.upperlimit=this.testData.ranges.result[1];
    }

    changedMandateValue(value):void{
        let index=this.mandatoryArr.indexOf(value)
        if (index==-1) {
            this.mandatoryArr.push(value)
        }else{
            this.mandatoryArr.splice(index,1)
        }
      //  console.log(this.mandatoryArr)
    }

    gen :any={
        lowerlimit:'',
        upperlimit:'',
        men:{
            lowerlimit:'',
            upperlimit:''
        },
        women:{
            lowerlimit:'',
            upperlimit:''
        },
    }
    bpranges:any={
        diastolicUpper:'',
        systolicUpper:'',
        systolicLower:'',
        diastolicLower:''
    }
    bloodGlucoseRanges:any={
        fastingDiabeticupper:'',
        fastingnormallower:'',
        fastingnormalupper:'',
        postprandialDiabeticupper:'',
        postprandialnormallower:'',
        postprandialnormalupper:'',
        randomDiabeticupper:'',
        randomnormallower:'',
        randomnormalupper:'',
    }
    setRange(){
    console.log('thih');    
    console.log(this.numberOne);
    console.log(this.numberTwo);   
     
    
        if (this.config.testRanges.length==0) {
            if(this.testData.testName=="BLOOD PRESSURE"){
                if (this.bpranges.systolicLower>=this.bpranges.systolicUpper || this.bpranges.diastolicLower>=this.bpranges.diastolicUpper) {
                    this.errormax=true;
                }else{
                        this.errormax=false;
                        this.config.testRanges.push({
                        "testName": this.testData.testName.split(' ').join('_') ,
                        "rangeLabels":["low", "normal", "high"],
                        "ranges": {
                            "systolic": [this.bpranges.systolicLower, this.bpranges.systolicUpper],
                            "diastolic": [this.bpranges.diastolicLower, this.bpranges.diastolicUpper]
                        },
                    });
                    this.clearObj()
                }
            }else if(this.testData.testName=="BLOOD GLUCOSE"){
                if (this.bloodGlucoseRanges.fastingnormallower>=this.bloodGlucoseRanges.fastingnormalupper || this.bloodGlucoseRanges.fastingnormalupper>=this.bloodGlucoseRanges.fastingDiabeticupper || this.bloodGlucoseRanges.postprandialnormallower>=this.bloodGlucoseRanges.postprandialnormalupper || this.bloodGlucoseRanges.postprandialnormalupper>=this.bloodGlucoseRanges.postprandialDiabeticupper || this.bloodGlucoseRanges.randomnormallower>=this.bloodGlucoseRanges.randomnormalupper || this.bloodGlucoseRanges.randomnormalupper>=this.bloodGlucoseRanges.randomDiabeticupper) {
                    this.errormax=true;
                   }else{
                    this.errormax=false;
                    this.config.testRanges.push({
                        "testName": this.testData.testName.split(' ').join('_') ,
                        "rangeLabels": ["pre-diabetic", "normal", "diabetic"],
                        "ranges": {
                            "fasting":[this.bloodGlucoseRanges.fastingnormallower,this.bloodGlucoseRanges.fastingnormalupper,this.bloodGlucoseRanges.fastingDiabeticupper],
                            "post_prandial":[this.bloodGlucoseRanges.postprandialnormallower,this.bloodGlucoseRanges.postprandialnormalupper,this.bloodGlucoseRanges.postprandialDiabeticupper],
                            "random":[this.bloodGlucoseRanges.randomnormallower,this.bloodGlucoseRanges.randomnormalupper,this.bloodGlucoseRanges.randomDiabeticupper]
                        },
                    });
                    this.clearObj()
                }
            }
            else if(this.testData.testName=="URIC ACID" || this.testData.testName=="HEMOGLOBIN"){
                if (this.gen.men.lowerlimit>=this.gen.men.upperlimit || this.gen.women.lowerlimit>=this.gen.women.upperlimit) {
                    this.errormax=true;
                }else{
                        this.errormax=false;
                        this.config.testRanges.push({
                        "testName": this.testData.testName.split(' ').join('_') ,
                        "rangeLabels":["low", "normal", "high"],
                        "ranges": {
                            "men":[this.gen.men.lowerlimit,this.gen.men.upperlimit],
                            "women":[this.gen.women.lowerlimit,this.gen.women.upperlimit],
                        },
                    });
                    this.clearObj()
                }
            }else{
                if (this.gen.lowerlimit>=this.gen.upperlimit) {
                    this.errormax=true;
                }else{
                    this.errormax=false;
                    this.config.testRanges.push({
                        "testName": this.testData.testName.split(' ').join('_') ,
                        "rangeLabels":["low", "normal", "high"],
                        "ranges": {  
                            "result":[this.gen.lowerlimit,this.gen.upperlimit]
                        },
                    });
                    this.clearObj()
                }
                
            }
        }
        else {
            this.config.testRanges.map((rangeItem,i)=>{
                if (rangeItem.testName==this.testData.testName.split(' ').join('_')) {
                    this.config.testRanges.splice(i,1);
                }
            });
            if(this.testData.testName=="BLOOD PRESSURE"){
                if (this.bpranges.systolicLower>=this.bpranges.systolicUpper || this.bpranges.diastolicLower>=this.bpranges.diastolicUpper) {
                    this.errormax=true;
                }else{
                    this.errormax=false;
                    this.config.testRanges.push({
                    "testName": this.testData.testName.split(' ').join('_') ,
                    "rangeLabels":["low", "normal", "high"],
                    "ranges": {
                        "systolic": [this.bpranges.systolicLower, this.bpranges.systolicUpper],
                        "diastolic": [this.bpranges.diastolicLower, this.bpranges.diastolicUpper]
                    },
                });
                this.clearObj()
            }
            }else if(this.testData.testName=="BLOOD GLUCOSE"){
               if (this.bloodGlucoseRanges.fastingnormallower>=this.bloodGlucoseRanges.fastingnormalupper || this.bloodGlucoseRanges.fastingnormalupper>=this.bloodGlucoseRanges.fastingDiabeticupper || this.bloodGlucoseRanges.postprandialnormallower>=this.bloodGlucoseRanges.postprandialnormalupper || this.bloodGlucoseRanges.postprandialnormalupper>=this.bloodGlucoseRanges.postprandialDiabeticupper || this.bloodGlucoseRanges.randomnormallower>=this.bloodGlucoseRanges.randomnormalupper || this.bloodGlucoseRanges.randomnormalupper>=this.bloodGlucoseRanges.randomDiabeticupper) {
                this.errormax=true;
               }else{
                this.errormax=false;
                this.config.testRanges.push({
                    "testName": this.testData.testName.split(' ').join('_') ,
                    "rangeLabels": ["pre-diabetic", "normal", "diabetic"],
                    "ranges": {
                        "fasting":[this.bloodGlucoseRanges.fastingnormallower,this.bloodGlucoseRanges.fastingnormalupper,this.bloodGlucoseRanges.fastingDiabeticupper],
                        "post_prandial":[this.bloodGlucoseRanges.postprandialnormallower,this.bloodGlucoseRanges.postprandialnormalupper,this.bloodGlucoseRanges.postprandialDiabeticupper],
                        "random":[this.bloodGlucoseRanges.randomnormallower,this.bloodGlucoseRanges.randomnormalupper,this.bloodGlucoseRanges.randomDiabeticupper]
                    },
                })
                this.clearObj()
               }
                
            }
            else if(this.testData.testName=="URIC ACID" || this.testData.testName=="HEMOGLOBIN"){
                if (this.gen.men.lowerlimit>=this.gen.men.upperlimit || this.gen.women.lowerlimit>=this.gen.women.upperlimit) {
                    this.errormax=true;
                }else{
                    this.errormax=false;
                        this.config.testRanges.push({
                        "testName": this.testData.testName.split(' ').join('_') ,
                        "rangeLabels":["low", "normal", "high"],
                        "ranges": {
                            "men":[this.gen.men.lowerlimit,this.gen.men.upperlimit],
                            "women":[this.gen.women.lowerlimit,this.gen.women.upperlimit],
                        },
                    })
                    this.clearObj()
                }
            }else{
                if (this.gen.lowerlimit>=this.gen.upperlimit) {
                    this.errormax=true;
                }else{
                    this.errormax=false;
                    this.config.testRanges.push({
                        "testName": this.testData.testName.split(' ').join('_') ,
                        "rangeLabels":["low", "normal", "high"],
                        "ranges": {  
                            "result":[this.gen.lowerlimit,this.gen.upperlimit]
                        },
                    })
                    this.clearObj()
                }
            }
        }
        
    }  

    clearObj(){
        console.log(this.config)
        this.displayDialog=false;
        this.bpranges={
            diastolicUpper:'',
            systolicUpper:'',
            systolicLower:'',
            diastolicLower:''
        },
        this.gen={
            lowerlimit:'',
            upperlimit:'',
            men:{
                lowerlimit:'',
                upperlimit:''
            },
            women:{
                lowerlimit:'',
                upperlimit:''
            },
        }
    }
    
    cancel(){
        this.bpranges={
            diastolicUpper:'',
            systolicUpper:'',
            systolicLower:'',
            diastolicLower:''
        }
        this.gen={
            lowerlimit:'',
            upperlimit:'',
            men:{
                lowerlimit:'',
                upperlimit:''
            },
            women:{
                lowerlimit:'',
                upperlimit:''
            },
        }
        this.displayDialog=false;
    }

    fieldLabels:any={
        STATE:'',
        CITY_OR_VILLAGE:''
    }

    submit(){
        if(this.confId)
         this.pushToServerUpdate()
        else
          this.pushToServer()
    }

  pushToServer() {
    Helpers.setLoading(true);
    this.config.mandatoryFields=this.mandatoryArr;
    this.config.fieldLabels=this.fieldLabels;
    this.config.configType='CUSTOM';
    delete this.config.id;
    console.log('sending',this.config)
    this._testsService.createconfigfile(this.config).then(res => {     
        Helpers.setLoading(false);
        document.getElementById('step_six').style.backgroundColor = this.green;
        setTimeout(() => {
            this._router.navigate(['/skin/list']);         
        }, 500);
    });
}

pushToServerUpdate() {
    console.log(this.config)
  Helpers.setLoading(true);
  this.config.mandatoryFields=this.mandatoryArr;
  this.config.fieldLabels=this.fieldLabels;
  console.log(this.config)
  this._testsService.updateconfigfile(this.config).then(res => {     
      Helpers.setLoading(false);
      document.getElementById('step_six').style.backgroundColor = this.green;
      setTimeout(() => {
          this._router.navigate(['/skin/list']);         
      }, 500);
  });
}

facilityChange(e){
   this.config.facilityId = e.id;
   this.config.facilityName = e.name; 
   this.selectedFacility = e;
   if(this.config.facilityId != ''){
        document.getElementById('step_one').style.backgroundColor = this.green;
        this.noCustomer = false;
        this.noUser = false;
   }      
   else
      this.noCustomer = true;
}

BpRanges(value, type){   
    switch(type){
        case 'systolicLower' :
            this.selectedBpRanges.systolicLower = value
            break;
        case 'systolicUpper' :
            this.selectedBpRanges.systolicUpper = value
            break;
        case 'diastolicLower' :
            this.selectedBpRanges.diastolicLower = value
            break;
        case 'diastolicUpper' :
            this.selectedBpRanges.diastolicUpper = value
            break;  
        default :
            break;        
    }   
}

otherRanges(value,type){ 
    
 if(this.testData.testName == 'BMI')
  //  this.switchBMI(type,value)
  this.switchCommon(type,value,this.selectedOtherRanges)  
 if(this.testData.testName == 'PULSE OXIMETER')
  //  this.switchPO(type,value)
  this.switchCommon(type,value,this.selectedPORanges)  
 if(this.testData.testName == 'CHOLESTEROL')
 //  this.switchChol(type,value)   
   this.switchCommon(type,value,this.selectedCholRanges)  
if(this.testData.testName == 'TEMPERATURE')
   this.switchCommon(type,value,this.selectedTempRanges)   
}

switchCommon(type,value,rangeVal){
    switch(type){
        case 'lowerlimit' :
          rangeVal.lowerlimit = value;
            // if(!Utils.checkLimit(rangeVal.lowerlimit,rangeVal.upperlimit)){
            //     this.lowLimit = true
            //     this.upLimit = false;
            // }      
            // else{
            //     this.lowLimit = false 
            //     this.upLimit = false; 
            // }
          break
        case 'upperlimit' :
            rangeVal.upperlimit = value;
            // if(!Utils.checkLimit(rangeVal.lowerlimit,rangeVal.upperlimit)){
            //     this.lowLimit = false
            //     this.upLimit = true;
            // }      
            // else{
            //     this.lowLimit = false 
            //     this.upLimit = false; 
            // }
            break
        default: 
           break;
    } 
}

switchBMI(type,value){
    switch(type){
        case 'lowerlimit' :
          this.selectedOtherRanges.lowerlimit = value;
          break
        case 'upperlimit' :
            this.selectedOtherRanges.upperlimit = value;
            break
        default: 
           break;
    }
}

switchPO(type,value){
    switch(type){
        case 'lowerlimit' :
          this.selectedPORanges.lowerlimit = value;
          break
        case 'upperlimit' :
            this.selectedPORanges.upperlimit = value;
            break
        default: 
           break;
    }
}

switchChol(type,value){
    switch(type){
        case 'lowerlimit' :
          this.selectedCholRanges.lowerlimit = value;
          break
        case 'upperlimit' :
            this.selectedCholRanges.upperlimit = value;
            break
        default: 
           break;
    }
}

uricHemoRanges(value,type,name){  
    if(name == 'HEMOGLOBIN'){
       this.switchHemo(type,value)
    }
    if(name == 'URIC ACID'){
       this.switchUric(type,value)
    }    
}

bloGluRanges(value,type){ 
    switch(type){
        case 'fastingDiabeticupper':
           this.selectedBGRanges.fastingDiabeticupper = value;
           break;
        case 'fastingnormallower':
           this.selectedBGRanges.fastingnormallower = value;
           break;
        case 'fastingnormalupper':
           this.selectedBGRanges.fastingnormalupper = value;
           break;
        case 'postprandialDiabeticupper':
           this.selectedBGRanges.postprandialDiabeticupper = value;
           break;
        case 'postprandialnormallower':
           this.selectedBGRanges.postprandialnormallower = value;
           break;
        case 'postprandialnormalupper':
           this.selectedBGRanges.postprandialnormalupper = value;
           break;
        case 'randomDiabeticupper':
           this.selectedBGRanges.randomDiabeticupper = value;
           break;
        case 'randomnormallower':
           this.selectedBGRanges.randomnormallower = value;
           break;
        case 'randomnormalupper':
           this.selectedBGRanges.randomnormalupper = value;
           break;
          
    }
}

glucoValue(){
    if(this.selectedBGRanges.fastingDiabeticupper)
      this.bloodGlucoseRanges.fastingDiabeticupper = this.selectedBGRanges.fastingDiabeticupper
    else
      this.bloodGlucoseRanges.fastingDiabeticupper = this.testData.ranges.fasting[2]
    if(this.selectedBGRanges.fastingnormallower)
      this.bloodGlucoseRanges.fastingnormallower = this.selectedBGRanges.fastingnormallower
    else
      this.bloodGlucoseRanges.fastingnormallower = this.testData.ranges.fasting[0]
    if(this.selectedBGRanges.fastingnormalupper)
      this.bloodGlucoseRanges.fastingnormalupper = this.selectedBGRanges.fastingnormalupper
    else
      this.bloodGlucoseRanges.fastingnormalupper = this.testData.ranges.fasting[1]
    if(this.selectedBGRanges.postprandialDiabeticupper)
      this.bloodGlucoseRanges.postprandialDiabeticupper = this.selectedBGRanges.postprandialDiabeticupper
    else
      this.bloodGlucoseRanges.postprandialDiabeticupper = this.testData.ranges.post_prandial[2]
    if(this.selectedBGRanges.postprandialnormallower)
      this.bloodGlucoseRanges.postprandialnormallower = this.selectedBGRanges.postprandialnormallower
    else
      this.bloodGlucoseRanges.postprandialnormallower = this.testData.ranges.post_prandial[0]
    if(this.selectedBGRanges.postprandialnormalupper)
      this.bloodGlucoseRanges.postprandialnormalupper = this.selectedBGRanges.postprandialnormalupper
    else
      this.bloodGlucoseRanges.postprandialnormalupper = this.testData.ranges.post_prandial[1]
    if(this.selectedBGRanges.randomDiabeticupper)
      this.bloodGlucoseRanges.randomDiabeticupper = this.selectedBGRanges.randomDiabeticupper
    else
      this.bloodGlucoseRanges.randomDiabeticupper = this.testData.ranges.random[2]
    if(this.selectedBGRanges.randomnormallower)
      this.bloodGlucoseRanges.randomnormallower = this.selectedBGRanges.randomnormallower
    else
      this.bloodGlucoseRanges.randomnormallower = this.testData.ranges.random[0]
    if(this.selectedBGRanges.randomnormalupper)
      this.bloodGlucoseRanges.randomnormalupper = this.selectedBGRanges.randomnormalupper
    else
      this.bloodGlucoseRanges.randomnormalupper = this.testData.ranges.random[1]       
}

switchHemo(type,value){
    switch(type){
        case 'menlowerlimit' : 
            this.selectedHURanges.menlowerlimit = value;
            break
        case 'menupperlimit' : 
            this.selectedHURanges.menupperlimit = value;
            break
        case 'womenlowerlimit' : 
            this.selectedHURanges.womenlowerlimit = value;
            break
        case 'womenupperlimit' : 
            this.selectedHURanges.womenupperlimit = value;
            break
        default: 
            break;        
    }
}

switchUric(type,value){
    switch(type){
        case 'menlowerlimit' : 
            this.selectedUARanges.menlowerlimit = value;
            break
        case 'menupperlimit' : 
            this.selectedUARanges.menupperlimit = value;
            break
        case 'womenlowerlimit' : 
            this.selectedUARanges.womenlowerlimit = value;
            break
        case 'womenupperlimit' : 
            this.selectedUARanges.womenupperlimit = value;
            break
        default: 
            break;        
    }
}

closeDialog(){  
  this.config.testRanges.map(data=>{
      if(data.testName == 'PULSE_OXIMETER'){
          this.valueChange( this.selectedPORanges,data)       
      }
      if(data.testName == 'BMI'){
        this.valueChange(this.selectedOtherRanges,data)
      }
      if(data.testName == 'TEMPERATURE'){
        this.valueChange(this.selectedTempRanges,data)
      }
      if(data.testName == 'CHOLESTEROL'){
        this.valueChange(this.selectedCholRanges,data)      
      } 
      if(data.testName == 'BLOOD_PRESSURE'){
        this.bpChange(this.selectedBpRanges,data)      
      } 
      if(data.testName == 'HEMOGLOBIN'){
        this.hemoUricChange(this.selectedHURanges,data)      
      } 
      if(data.testName == 'URIC_ACID'){
        this.hemoUricChange(this.selectedUARanges,data)      
      } 
      if(data.testName == 'BLOOD_GLUCOSE'){
        this.bloodglucoChange(this.selectedBGRanges,data)
      }    
   })
   this.displayDialog = false;
}

valueChange(value,data){
    value.lowerlimit = data.ranges.result[0]
    value.upperlimit = data.ranges.result[1]
}

bpChange(value,data){   
    value.systolicLower = data.ranges.systolic[0];
    value.systolicUpper = data.ranges.systolic[1];
    value.diastolicLower = data.ranges.diastolic[0];
    value.diastolicUpper = data.ranges.diastolic[1];
}

hemoUricChange(value,data){   
    value.menlowerlimit = data.ranges.men[0]
    value.menupperlimit = data.ranges.men[1]
    value.womenlowerlimit = data.ranges.women[0]
    value.womenupperlimit = data.ranges.women[1]
}

bloodglucoChange(value,data){    
   value.fastingDiabeticupper = data.ranges.fasting[2];  
   value.fastingnormallower = data.ranges.fasting[0]; 
   value.fastingnormalupper = data.ranges.fasting[1];  
   value.postprandialDiabeticupper = data.ranges.post_prandial[2];  
   value.postprandialnormallower = data.ranges.post_prandial[0];  
   value.postprandialnormalupper = data.ranges.post_prandial[1];   
   value.randomDiabeticupper = data.ranges.random[2]; 
   value.randomnormallower = data.ranges.random[0]; 
   value.randomnormalupper = data.ranges.random[1];    
}

heightChange(test){
  // this.isEdit = false;
   if(test.unit == 'ft')
      test.conversionFactor = 0.033;
  if(test.unit == 'cms')
      test.conversionFactor = 1;
}

weightChange(test){
    if(test.unit == 'lbs')
      test.conversionFactor = 2.204;
    if(test.unit == 'Kgs')
      test.conversionFactor = 1;    
}

tempChange(test){  
    if(test.unit == 'F')
      test.conversionFactor = 1;
    if(test.unit == 'C')
      test.conversionFactor = 1;    
}

poctChange(test){
    if(test.unit == 'mmol/L')
    test.conversionFactor = 0.0555;
  if(test.unit == 'mg/dL')
    test.conversionFactor = 1; 
}

ConversionFactor(test){   
  switch(test.testName){
      case test.testName:
         if(test.conversionFactor  && test.conversionFactor.trim('') && test.conversionFactor != 0)
            test.conversionFactor = test.conversionFactor;
         else{
            this.config.testUnitMapping.map((testsunitmapped)=>{
                if((testsunitmapped.testName).split('_').join(' ')==test.testName){                  
                    test.conversionFactor = testsunitmapped.conversionFactor;
                }
            })  
         }            
          break;
      default :
          break;
  }
}

ConversionUnit(test){   
    switch(test.testName){
        case test.testName:
           if(test.unit  && test.unit.trim(''))
              test.unit = test.unit;
           else{
              this.config.testUnitMapping.map((testsunitmapped)=>{
                  if((testsunitmapped.testName).split('_').join(' ')==test.testName){                  
                      test.unit = testsunitmapped.testUnit;
                  }
              })  
           }            
            break;
        default :
            break;
    }
  }

  redirectCus(){
    this._router.navigate(['/facilities/update'], { queryParams: { facilityId: this.selectedFacility.organizationCode } });
  }

  onlogo(){
      this.config.customLogo = !this.config.customLogo
  }

  onrange(){
      this.config.customRanges = !this.config.customRanges
  }

}