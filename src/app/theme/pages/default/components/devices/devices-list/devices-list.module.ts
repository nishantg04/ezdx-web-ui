import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DevicesListComponent } from './devices-list.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { DevicesService } from '../devices.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { OrderModule } from 'ngx-order-pipe';
import { DataTableModule, SharedModule, PaginatorModule, DialogModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { DropdownModule } from 'primeng/primeng';
import { FacDropdownModule } from '../../../../../fac-dropdown/fac-dropdown.module'

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": DevicesListComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule, FacDropdownModule, DataTableModule, DialogModule, SharedModule, PaginatorModule,
        TranslateModule.forChild({ isolate: false }), DropdownModule

    ], exports: [
        RouterModule
    ], declarations: [
        DevicesListComponent
    ],
    providers: [
        DevicesService,
        FacilitiesService, TranslateService
    ]

})
export class DevicesListModule {



}