import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { UsersService } from '../../pages/default/components/users/users.service';
import { UserChild } from '../../pages/default/components/users/userChild';
import { Router, ActivatedRoute } from '@angular/router';
import { ScriptLoaderService } from '../../../_services/script-loader.service';

declare let mLayout: any;
@Component({
    selector: "app-aside-nav",
    templateUrl: "./aside-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class AsideNavComponent implements OnInit, AfterViewInit {
    user: any;
    isAdmin: string;
    showSidebar: boolean;
    userRole: string;
    fCode: string;
    showUpload: boolean = false;
    constructor(private _usersService: UsersService, private _router: Router, private _script: ScriptLoaderService, private _activeroute: ActivatedRoute) {
        this.isAdmin = null;
        this.showSidebar = true;
        this.userRole = "";
        this.user = { "displayName": "", "email": "", "profilePictureUrl": "" }
    }
    ngOnInit() {
        this.activeContent();
        if (!window.localStorage.getItem('userInfo'))
            this.getCurrentUser();
        else
            this.getCurrentFromLocal()

    }

    getCurrentFromLocal() {
        this.user = JSON.parse(window.localStorage.getItem('userInfo'))
        if (this.user) {
            this.roleChecking();
        }
    }

    getCurrentUser() {
        this._usersService.getCurrentUser()
            .then(user => this.user = user)
            .then(() => {
                if (this.user) {
                    this.roleChecking();
                }
            });
    }

    roleChecking() {
        var isadmin = true;
        var isfacilityadmin = false;
        this.fCode = this.user.facilityCode;

        this.user.userRoles.forEach(role => {
            if (role === "OPERATOR" || role === "DOCTOR" || role === "TECHNICIAN" || role === "CENTER_ADMIN")
                isadmin = false;
            if (role === "TECHNICIAN")
                isfacilityadmin = true;
            if (role === "HC_OPERATIONS")
                this.userRole = "HC_OPS";
            if (role === "HC_MANAGER")
                this.userRole = "HC_MANAGER";
            if (role === "HC_ADMIN") {
                this.showUpload = true;
                this.userRole = "HC_ADMIN";
            }
            if (role === "OPERATOR")
                this.userRole = "OPERATOR";
            if (role === "DOCTOR")
                this.userRole = "DOCTOR";
            if (role === "TECHNICIAN")
                this.userRole = "TECHNICIAN";
            if (role === 'PATHOLOGIST')
                this.userRole = 'PATHOLOGIST'
            if (role === 'CENTER_ADMIN')
                this.userRole = 'CENTER_ADMIN'
        });
        if (!isadmin) {
            this.isAdmin = "Facility";
        }
        else {
            this.isAdmin = "Admin";
        }
        if (window.location.href.indexOf("privacy") !== -1) {
            this.showSidebar = false;
        }
    }
    ngAfterViewInit() {
        mLayout.initAside();
        let menu = (<any>$('#m_aside_left')).mMenu(); let item = $(menu).find('a[href="' + window.location.pathname + '"]').parent('.m-menu__item'); (<any>$(menu).data('menu')).setActiveItem(item);
    }

    deviceListNavigate() {
        document.getElementsByClassName('m-menu__item m-menu__item--submenu')[3].classList['value'] = "m-menu__item m-menu__item--submenu m-menu__item--open"
        document.getElementById('hub-tab').style.display = 'block'
        this._router.navigate(['/devices/list']);
    }

    activeContent() {
        //  console.log('current url', this._activeroute.snapshot['_routerState'].url)
    }


    userListNavigate() {
        this._router.navigate(['/users/list']);
    }

    clientListNavigate() {
        this._router.navigate(['/facilities/list'])
    }

    redirectCustomer(type) {
        if (type == 'list') {
            this._router.navigate(['/facilities/list'])
        }
        if (type == 'add') {
            this._router.navigate(['/facilities/add'])
        }
    }

    hubListNavigate() {
        this._router.navigate(['/devices/list'])
    }

    clientUpdateNavigate() {
        this._router.navigate(['/facilities/update'], { queryParams: { facilityId: this.user.facilityCode } });
    }

    CenterNavigate() {
        this._router.navigate(['center/details'], { queryParams: { center: this.user.centerId, facility: this.user.facilityCode }, })
    }

    rdtRedirect() {
        this._router.navigate(['/rdt/list', { queryParams: { type: "RL" } }])
    }


}