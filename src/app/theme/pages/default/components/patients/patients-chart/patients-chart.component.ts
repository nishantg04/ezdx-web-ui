import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone } from "@angular/core";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";
import { PatientsService } from "../patients.service";
import { SelectItem } from "primeng/primeng";
import { FacilitiesService } from "../../facilities/facilities.service";
import { UsersService } from '../../users/users.service';
import { Facility } from "../../facilities/facility";
import { Helpers } from "../../../../../../helpers";
import { Center } from "../../facilities/center";
import { Input } from "@angular/core/src/metadata/directives";
import { PACKAGE_ROOT_URL } from "@angular/core/src/application_tokens";
import { userInfo } from "os";
import { validateConfig } from "@angular/router/src/config";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UserChild } from '../../users/userChild';
import { AmChartsService } from "amcharts3-angular2";


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./patients-chart.component.html",
    encapsulation: ViewEncapsulation.None
})
export class PatientsChartComponent implements OnInit {
    user: UserChild;
    facilities: Facility[];
    centers: Center[];
    selectedFacility: Facility;
    selectedCenter: Center;
    userInfo: any;
    chosen: string;
    hcOperations: boolean = false;
    userType: any;
    fromDate: string;
    toDate: string;
    genderArray: any[];
    ageArray: any[];
    ageJson: any;
    lessThanfive: any;
    fiveTotwelve: any;
    twelveTofourty: any;
    fourtyToSixty: any;
    aboveSixty: any;
    private chart: any;
    ageList: any[] = [];

    constructor(private _ngZone: NgZone, private _script: ScriptLoaderService, private _router: Router, private _activeroute: ActivatedRoute,
        private _usersService: UsersService, private _patientsService: PatientsService, private _facilitiesService: FacilitiesService,
        private AmCharts: AmChartsService) {
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
        if (window.localStorage.getItem('userType') == 'Facility')
            this.userType = 'Facility';
        else if (window.localStorage.getItem('userType') == 'Center')
            this.userType = 'Center';
        else if (window.localStorage.getItem('userType') == 'User')
            this.userType = 'User';
        else
            this.userType = 'Admin';
    }

    ngOnInit() {
        this.getFacilities();
        this.getView();
    }

    showGenderChart() {

        this.chart = this.AmCharts.makeChart("pie_chart_gender_statistics", {
            "type": "pie",
            "theme": "light",
            "dataProvider": this.genderArray,
            "titleField": "title",
            "valueField": "value",
            "colorField": "color",
            "labelRadius": 5,
            "radius": "42%",
            "innerRadius": "70%",
            "labelText": "",
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "export": {
                "enabled": true
            }
        });
        if (this.genderArray[0].value === 0 && this.genderArray[1].value === 0 && this.genderArray[2].value === 0) {
            this.chart.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
        }
    }

    showAgeChart() {
        this.chart = this.AmCharts.makeChart("pie_chart_age_statistics", {
            "type": "pie",
            "theme": "light",
            "dataProvider": this.ageList,
            "titleField": "title",
            "valueField": "value",
            "colorField": "color",
            "labelRadius": 5,
            "radius": "42%",
            "innerRadius": "70%",
            "labelText": "",
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "export": {
                "enabled": true
            }
        });
        // console.log('age', this.ageList)
        if (this.ageList[0].value === 0 && this.ageList[1].value === 0 && this.ageList[2].value === 0 && this.ageList[3].value === 0 && this.ageList[4].value === 0) {
            this.chart.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
        }
    }

    getFacilities() {
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        // console.log('fac', this.facilities)
        this.facilities.sort(function(a, b) { if (a.name > b.name) { return 1; } else if (a.name < b.name) { return -1; } return 0; });
        let allFacility = new Facility();
        allFacility.name = "ALL";
        allFacility.id = "ALL";
        this.facilities.splice(0, 0, allFacility);
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - 29);
        startDate.setHours(0); startDate.setMinutes(0); startDate.setSeconds(0); startDate.setMilliseconds(0);
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        endDate.setHours(23); endDate.setMinutes(23); endDate.setSeconds(23); endDate.setMilliseconds(0);
        let searchcriteria = "ALL";
        if (window.localStorage.getItem('userType') == 'Facility') {
            // console.log('type', window.localStorage.getItem('userType'))
            this.selectedFacility.id = this.userInfo.facilityId;
            this.facilities.forEach(fclt => {
                if (fclt.id == this.userInfo.facilityId) {
                    this.centers = jQuery.extend(true, [], fclt.centers);
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                    this.selectedCenter.id = this.centers[0].id;
                }
            });
        }
        else if (window.localStorage.getItem('userType') == 'Center') {
            this.selectedCenter.id = this.userInfo.centerId;
            this.selectedFacility.id = this.userInfo.facilityId;
        }
        else {
            this.selectedFacility.id = this.facilities[0].id;
            this.centers = this.facilities[0].centers;
            this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
        }

    }

    ngAfterViewInit() {

    }

    getView() {
        Helpers.setLoading(true);
        window.localStorage.removeItem('patientStartDate');
        window.localStorage.removeItem('patientEndDate');
        this.fromDate = this._activeroute.snapshot.queryParams['from'];
        this.toDate = this._activeroute.snapshot.queryParams['to'];
        this.selectedFacility.id = this._activeroute.snapshot.queryParams['facilityId'];
        this.selectedCenter.id = this._activeroute.snapshot.queryParams['centerId'];
        // this.getFacilities();   
        if (this.fromDate && this.toDate) {
            window.localStorage.setItem('patientStartDate', this.fromDate);
            window.localStorage.setItem('patientEndDate', this.toDate);
        }
        if (this.selectedFacility.id != "ALL") {
            this.facilities.forEach(fac => {
                if (fac.id === this.selectedFacility.id) {
                    this.centers = fac.centers;
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                }
            });
        }
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/datatables/base/date.patients.js');

        this.user = JSON.parse(window.localStorage.getItem('userInfo'))
        if (this.user && this.user.userRoles) {
            let isAdmin = false;
            this.user.userRoles.forEach((role, i) => {
                if (role === "HC_ADMIN") {
                    isAdmin = true;
                }
                if (role === "HC_OPERATIONS") {
                    this.hcOperations = true;
                    isAdmin = true;
                }
            });
            if (window.localStorage.getItem('userType') == 'Facility') {
                this.userType = 'Facility';
                this.filterReports();
            }
            else if (window.localStorage.getItem('userType') == 'Center') {
                this.selectedCenter.id = this.user.centerId;
                this.selectedFacility.id = this.user.facilityId;
                this.selectedFacility.organizationCode = this.user.facilityCode;
                // this.hideFilter = false;
                this.centers = undefined;
                this.userType = 'Center';
                this.filterReports();
            }else if (window.localStorage.getItem('userType') == 'User') {
                this.userType = 'User';
                this.userReport();
            }
            else {
                this.userType = 'Admin';
                this.getFilterPatientGenderCount();
                this.getFilterPatientAgeCount();
            }
        }
        else {
            this._router.navigate(['/login']);
        }
        Helpers.setLoading(false);

    }

    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers[0].id !== "ALL") {
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                }
                this.selectedCenter.id = this.centers[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }

    filter(){
        if(this.userType != 'User'){
            this.filterReports() 
        }else{
            this.userReport()
        }
    }

    filterReports() {
        this.fromDate = window.localStorage.getItem('patientStartDate')
        this.toDate = window.localStorage.getItem('patientEndDate')
        var searchcriteria;
        if (this.selectedFacility.id == "ALL") {
            searchcriteria = "ALL"
        }
        if (this.selectedFacility.id != "ALL" && this.selectedCenter.id == "ALL") {
            searchcriteria = "FACILITY"
        }
        if (this.selectedCenter.id != "ALL") {
            searchcriteria = "CENTER"
        }
        this.getFilterPatientGenderCount();
        this.getFilterPatientAgeCount();
    }


    userReport(){
        this.fromDate = window.localStorage.getItem('patientStartDate')
        this.toDate = window.localStorage.getItem('patientEndDate')
        this.getUserFilterPatientGenderCount()
        this.getUserFilterPatientAgeCount();
    }


    getUserFilterPatientGenderCount() {
        var searchcriteria;
        var tempArray;
        this.genderArray = [];
        var fromTime = new Date(this.fromDate).getTime()
        var toTime = new Date(this.toDate).getTime()        
        this._usersService.getUserPatientbyGenderFilter(this.userInfo.id,fromTime,toTime).then(data => {
                tempArray = data;
                this.genderArray.push({ 'title': 'Male', 'value': tempArray.male, "color": "#465EEB" },
                    { 'title': 'Female', 'value': tempArray.female, "color": "#21C4DF" },
                    { 'title': 'Others', 'value': tempArray.other, "color": "#F2F3F8" }
                )
                // console.log(this.genderArray)
                this.showGenderChart();
                // localStorage.setItem('PATIENTS_STISTICS_GENDER', JSON.stringify(this.genderArray))
                // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                //     'assets/demo/default/custom/components/charts/amcharts/charts.patientbyage.js');
            })
    }

    getUserFilterPatientAgeCount() {
        this.lessThanfive = 0;
        this.fiveTotwelve = 0;
        this.twelveTofourty = 0;
        this.fourtyToSixty = 0;
        this.aboveSixty = 0;
        var searchcriteria;
        this.ageList = []
        var fromTime = new Date(this.fromDate).getTime()
        var toTime = new Date(this.toDate).getTime()
       
        this._usersService.getUserPatientbyAgeFilter(this.userInfo.id, fromTime,toTime)
            .then(data => {
                this.ageArray = data
                // console.log('data age', this.ageArray)
                this.ageArray.forEach(age => {
                    if (age._id >= 1 && age._id < 5) {
                        this.lessThanfive = this.lessThanfive + age.count;
                    } if (age._id >= 5 && age._id < 12) {
                        this.fiveTotwelve = this.fiveTotwelve + age.count;
                    } if (age._id >= 12 && age._id < 40) {
                        this.twelveTofourty = this.twelveTofourty + age.count;
                    } if (age._id >= 40 && age._id < 60) {
                        this.fourtyToSixty = this.fourtyToSixty + age.count;
                    } if (age._id >= 60 && age._id < 150) {
                        this.aboveSixty = this.aboveSixty + age.count;
                    }
                })

                this.ageList.push({ 'title': '< 5', 'value': this.lessThanfive, "color": "#F2F3F8" },
                    { 'title': '5-12', 'value': this.fiveTotwelve, "color": "#465EEB" },
                    { 'title': '13-40', 'value': this.twelveTofourty, "color": "#21C4DF" },
                    { 'title': '41-60', 'value': this.fourtyToSixty, "color": "#8800F3" },
                    { 'title': '> 60', 'value': this.aboveSixty, "color": "#6AC1E5" }
                )
                this.showAgeChart()
                // localStorage.setItem('PATIENTS_STISTICS_AGE', JSON.stringify(this.ageJson))
                // localStorage.setItem('PATIENTS_STISTICS_GENDER', JSON.stringify(this.genderArray))
                // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                //     'assets/demo/default/custom/components/charts/amcharts/charts.patientbyage.js');
            })
    }



    getFilterPatientGenderCount() {
        var searchcriteria;
        var tempArray;
        this.genderArray = [];
        var fromTime = new Date(this.fromDate).getTime()
        var toTime = new Date(this.toDate).getTime()
        if (this.selectedFacility.id == "ALL" && this.selectedCenter.id == undefined) {
            searchcriteria = "ALL"
        }
        if (this.selectedFacility.id != "ALL" && this.selectedCenter.id == "ALL") {
            searchcriteria = "FACILITY"
        }
        if (this.selectedCenter.id != "ALL" && this.selectedCenter.id != undefined) {
            searchcriteria = "CENTER"
        }
        this._patientsService.getPatientStatsForGenderByFilter(searchcriteria, fromTime,
            toTime, this.selectedFacility.id, this.selectedCenter.id).then(data => {
                tempArray = data;
                this.genderArray.push({ 'title': 'Male', 'value': tempArray.male, "color": "#465EEB" },
                    { 'title': 'Female', 'value': tempArray.female, "color": "#21C4DF" },
                    { 'title': 'Others', 'value': tempArray.other, "color": "#F2F3F8" }
                )
                // console.log(this.genderArray)
                this.showGenderChart();
                // localStorage.setItem('PATIENTS_STISTICS_GENDER', JSON.stringify(this.genderArray))
                // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                //     'assets/demo/default/custom/components/charts/amcharts/charts.patientbyage.js');
            })
    }

    getFilterPatientAgeCount() {
        this.lessThanfive = 0;
        this.fiveTotwelve = 0;
        this.twelveTofourty = 0;
        this.fourtyToSixty = 0;
        this.aboveSixty = 0;
        var searchcriteria;
        this.ageList = []
        var fromTime = new Date(this.fromDate).getTime()
        var toTime = new Date(this.toDate).getTime()
        if (this.selectedFacility.id == "ALL" && this.selectedCenter.id == undefined) {
            searchcriteria = "ALL"
        }
        if (this.selectedFacility.id != "ALL" && this.selectedCenter.id == "ALL") {
            searchcriteria = "FACILITY"
        } if (this.selectedCenter.id != "ALL" && this.selectedCenter.id != undefined) {
            searchcriteria = "CENTER"
        }
        this._patientsService.getPatientStatsForAgeByFilter(searchcriteria, fromTime,
            toTime, this.selectedFacility.id, this.selectedCenter.id)
            .then(data => {
                this.ageArray = data
                // console.log('data age', this.ageArray)
                this.ageArray.forEach(age => {
                    if (age._id >= 1 && age._id < 5) {
                        this.lessThanfive = this.lessThanfive + age.count;
                    } if (age._id >= 5 && age._id < 12) {
                        this.fiveTotwelve = this.fiveTotwelve + age.count;
                    } if (age._id >= 12 && age._id < 40) {
                        this.twelveTofourty = this.twelveTofourty + age.count;
                    } if (age._id >= 40 && age._id < 60) {
                        this.fourtyToSixty = this.fourtyToSixty + age.count;
                    } if (age._id >= 60 && age._id < 150) {
                        this.aboveSixty = this.aboveSixty + age.count;
                    }
                })

                this.ageList.push({ 'title': '< 5', 'value': this.lessThanfive, "color": "#F2F3F8" },
                    { 'title': '5-12', 'value': this.fiveTotwelve, "color": "#465EEB" },
                    { 'title': '13-40', 'value': this.twelveTofourty, "color": "#21C4DF" },
                    { 'title': '41-60', 'value': this.fourtyToSixty, "color": "#8800F3" },
                    { 'title': '> 60', 'value': this.aboveSixty, "color": "#6AC1E5" }
                )
                this.showAgeChart()
                // localStorage.setItem('PATIENTS_STISTICS_AGE', JSON.stringify(this.ageJson))
                // localStorage.setItem('PATIENTS_STISTICS_GENDER', JSON.stringify(this.genderArray))
                // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                //     'assets/demo/default/custom/components/charts/amcharts/charts.patientbyage.js');
            })
    }

    selected(value) {
        console.log('got', this.chosen)
    }

    facilityChange(value) {
        this.selectedFacility = value;
        this.selectedFacility.organizationCode = value.organizationCode;
        this.centers = value.centers;
        if (this.centers[0].id !== "ALL") {
            let allCenter = new Center();
            allCenter.name = "ALL";
            allCenter.id = "ALL";
            this.centers.splice(0, 0, allCenter);
        }
        this.selectedCenter.id = this.centers[0].id;
        this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
    }


}
