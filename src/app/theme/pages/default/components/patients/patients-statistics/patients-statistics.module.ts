import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
import { LayoutModule } from "../../../../../layouts/layout.module";
import { DefaultComponent } from "../../../default.component";
import { DropdownModule } from "primeng/primeng";
import { PatientsService } from "../patients.service";
import { FacilitiesService } from "../../facilities/facilities.service";
import { UsersService } from "../../users/users.service";
import { PatientsStatisticsComponent } from "./patients-statistics.component";
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { AmchartModule } from '../../../../../amchartcomponent/amchart.module';
import { FacDropdownModule } from '../../../../../fac-dropdown/fac-dropdown.module'

const routes: Routes = [
    {
        path: "",
        component: DefaultComponent,
        children: [
            {
                path: "",
                component: PatientsStatisticsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, AmchartModule, FormsModule, FacDropdownModule, RouterModule.forChild(routes), LayoutModule,
        DropdownModule, TranslateModule.forChild({ isolate: false })
    ],
    exports: [RouterModule],
    declarations: [PatientsStatisticsComponent
    ],
    providers: [PatientsService, FacilitiesService, UsersService, TranslateService]
})
export class PatientsStatisticsModule { }
