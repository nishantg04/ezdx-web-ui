import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Helpers } from './../../../../../helpers';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';

import { environment } from '../../../../../../environments/environment'
import { promise } from 'selenium-webdriver';
@Injectable()
export class UploadFileService {

    private multipartHeader = new Headers({ 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });

    private serviceUrl = environment.BaseURL + environment.FileSrvURL;// 'http://dev.ezdx.healthcubed.com/ezdx-file-srv/api/v1/files';
    private fileDetailUrl = environment.BaseURL + environment.DetailFileSrvURL;
    private fileHistoryUrl = environment.BaseURL + environment.FileHistory;

    constructor(private http: Http, private _script: ScriptLoaderService) { }


    uploadFile(data, checksum): Promise<any> {
        return this.http.post(this.serviceUrl + "?filetype=PYTHON_SCRIPT&checksum=" + checksum, data, { headers: this.multipartHeader })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    uploadFirmWareFile(data, checksum): Promise<any> {
        return this.http.post(this.serviceUrl + "?filetype=FIRMWARE&checksum=" + checksum, data, { headers: this.multipartHeader })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    uploadDeviceId(data, id): Promise<any> {
        return this.http.put(this.serviceUrl + "/" + id + "/assign", data, { headers: this.headers })
            .toPromise()
            .then(response => { return response })
            .catch(this.handleError.bind(this))
    }

    uploadDeviceUnassign(data, id): Promise<any> {
        return this.http.put(this.serviceUrl + "/" + id + "/unassign", data, { headers: this.headers })
            .toPromise()
            .then(response => { return response })
            .catch(this.handleError.bind(this))
    }


    getUploadedPythonList(page, size): Promise<any> {
        return this.http.get(this.serviceUrl + "/search?page=" + page + "&limit=" + size + "&searchCriteria=FILE_TYPE_AND_ASSIGNMENT_STATUS&fileType=PYTHON_SCRIPT&assignmentStatus=ASSIGNED", { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as any })
            .catch(this.handleError.bind(this));
    }

    getUploadedUnassignedPythonList(page, size): Promise<any> {
        return this.http.get(this.serviceUrl + "/search?page=" + page + "&limit=" + size + "&searchCriteria=FILE_TYPE_AND_ASSIGNMENT_STATUS&fileType=PYTHON_SCRIPT&assignmentStatus=UNASSIGNED", { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as any })
            .catch(this.handleError.bind(this));
    }

    getUploadedFirmwareList(page, size): Promise<any> {
        return this.http.get(this.serviceUrl + "/search?page=" + page + "&limit=" + size + "&searchCriteria=FILE_TYPE_AND_ASSIGNMENT_STATUS&fileType=FIRMWARE&assignmentStatus=ASSIGNED", { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as any })
            .catch(this.handleError.bind(this));
    }

    getUploadedUnassignedFirmwareList(page, size): Promise<any> {
        return this.http.get(this.serviceUrl + "/search?page=" + page + "&limit=" + size + "&searchCriteria=FILE_TYPE_AND_ASSIGNMENT_STATUS&fileType=FIRMWARE&assignmentStatus=UNASSIGNED", { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as any })
            .catch(this.handleError.bind(this));
    }

    getFileDetails(id): Promise<any> {
        return this.http.get(this.serviceUrl + "/" + id, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as any })
            .catch(this.handleError.bind(this))
    }

    getFileFullDetail(id): Promise<any> {
        return this.http.get(this.fileDetailUrl + "/" + id, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as any })
            .catch(this.handleError.bind(this))
    }

    getFileHistory(id): Promise<any> {
        return this.http.get(this.fileHistoryUrl + "/" + id, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as any })
            .catch(this.handleError.bind(this))
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load('body',
                'assets/demo/default/custom/components/utils/redirect-cidaas-logout.js');
        }
        else if (error.status === 400) {
            // console.log(error.statusText)
        }
        return Promise.reject(error.message || error);
    }

}
