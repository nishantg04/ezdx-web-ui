/* export interface MapForCenter {
    "centerId": string,
    "geopoint": {
        lat: number,
        lon: number
    },
    "facilityId": string,
    "facilityName": string,
    "centerName": string,
    "patientCount": number,
    "deviceCount": number,
    "testCount": number
}
 */
export interface ICenterData {
    id?: String;
    facilityName: String;
    centerName: String;
    totalDevices: Number;
    totalRegistrations: Number;
    totalTests: Number;
    totalConsumableTests: Number;
    totalProbeTests: Number;
    testsPerRegistration: Number;
    consumableTestsPerRegistration: Number;
    probeTestsPerRegistration: Number;
    registrationPerDevice: Number;
    testsPerDevice: Number;
    consumableTestsPerDevice: Number;
    address: Iaddress;
    geopoint?: any;
    lastUpdated: Number;
}

export interface Iaddress {
    city: String;
    postcode: String;
    address: String;
    country: String;
    district: String;
    state: String
}