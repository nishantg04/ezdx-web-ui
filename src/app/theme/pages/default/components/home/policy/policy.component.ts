import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./policy.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class PolicyComponent implements OnInit, AfterViewInit {
    constructor(private _script: ScriptLoaderService, private _router: Router) {

    }
    ngOnInit() {
    }
    ngAfterViewInit() {
    }

}