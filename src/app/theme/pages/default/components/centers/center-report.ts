export class CenterReport {
    centerName: string;
    beneficiariesTillDate: any;
    females: any;
    diagnosticsTillDate: number;
    fromDate: any;
    toDate: any;
    registrationCount: any;
    diagnosticsCount: any;
    userCount: number;
    diabetesNormal: any;
    diabetesPre: any;
    diabetesDiabetic: any;
    hypertensionNormal: any;
    hypertensionHigh: any;
    anemiaLow: any;
    anemiaHigh: any;
    anemiaNormal: any;
    malariaPositive: any;
    malariaNegative: any;
    logoUrl: any;
}