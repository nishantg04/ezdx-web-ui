import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkinconfigComponent } from './skinconfig.component';

import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { InputTextModule, InputTextareaModule, DataTableModule, SharedModule, PaginatorModule, GrowlModule, DialogModule } from 'primeng/primeng';
import { FacDropdownModule } from './../../../../../fac-dropdown/fac-dropdown.module'
import { ConfigService } from '../tests.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { CustomMinValidatorDirective } from '../custom-min-validator.directive';
import { CustomMaxValidatorDirective } from '../custom-max-validator.directive';
import {FacilitiesService} from '../../facilities/facilities.service'

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": SkinconfigComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, InputTextModule, InputTextareaModule, DataTableModule, SharedModule, PaginatorModule, DialogModule, GrowlModule,
        TranslateModule.forChild({ isolate: false }), FacDropdownModule
    ], exports: [
        RouterModule
    ], declarations: [
        SkinconfigComponent,
        CustomMaxValidatorDirective,
        CustomMinValidatorDirective

    ],
    providers: [
        TranslateService,
        ConfigService,
        FacilitiesService
    ]

})
export class SkinconfigModule { }


