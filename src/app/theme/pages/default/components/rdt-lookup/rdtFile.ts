import { RDTEntity } from "./RDTEntity";

export class RDTLookupJson {
    fileName: string;
    facilityId: string[];
    centerId: string[];
    deviceId: string[];
    rdtLookups: RDTEntity[];
    reason: string;
}