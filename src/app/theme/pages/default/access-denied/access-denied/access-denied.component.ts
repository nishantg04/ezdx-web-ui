import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector: ".m-wrapper",
    templateUrl: "./access-denied.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class AccessDeniedComponent implements OnInit {

    constructor() { }

    ngOnInit() { }
}