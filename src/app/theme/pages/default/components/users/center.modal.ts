export class CenterModal {
    id: String;
    name: String;
    description: String;
    centerCode: String;
    createTime: Number;
    updateTime: Number;
    address: {
        address: String;
        city: String;
        country: String;
        district: String;
        postcode: String;
        state: String;
    };
    geopoint: {
        lat: number;
        lon: number;
        alt: number;
    };
}
