import {
    Component,
    OnInit,
    ViewEncapsulation,
    AfterViewInit
} from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Helpers } from "../../../../../../helpers";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";
import { ReportDataService } from "../report-data.service";
import { NgForm } from "@angular/forms";

import * as _ from "lodash";
import { Visit } from "../visit.model";

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./ipan-reports.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class IpanReportsComponent implements OnInit, AfterViewInit {
    pdfData: any;
    reportDetails: any;
    selectedTests: any;
    displayCommentDialog: boolean = false;
    pageNo: number = 1;
    testCounts: number = 0;
    comment: string;
    row: number;
    limit: number = 10;
    visits: Visit[] = [];
    showReject: Boolean = true;
    showPdfReport: Boolean = false;
    ranges: any;
    constructor(private service: ReportDataService, private route: Router) {
        this.pageNo = 1;
        this.limit = 10;
    }

    ngOnInit() {
        this.getVisits(this.pageNo, this.limit);
    }
    ngAfterViewInit() { }

    getVisits(pageno, limit) {
        Helpers.setLoading(true);
        this.service
            .getAllTests(this.pageNo, this.limit)
            .then(res => {
                Helpers.setLoading(false);
                this.visits = [];
                this.visits = res.visits;
                console.log(this.visits)
                this.testCounts = res.count;
            }, err => {
                Helpers.setLoading(false);
            });
    }


    paginate(event) {
        this.pageNo = event.page + 1;
        this.limit = parseInt(event.rows);
        this.getVisits(this.pageNo, this.limit);
    }
    rejectReport(form: NgForm) {
        this.service.reject(form.value.comment, this.reportDetails.id).then(
            (res) => {
                this.reportDetails = {};
                this.showPdfReport = false;
                this.pageNo = 1;
                this.limit = 10;
                this.getVisits(this.pageNo, this.limit);
                this.comment = "";
            })
    }
    showPdfDetails(test) {
        Helpers.setLoading(true);
        this.reportDetails = test;
        if (this.reportDetails.rejected) {
            this.showReject = false;
        } else {
            this.showReject = true;
        }
        this.service.getPdfReport(this.reportDetails.patientId)
            .then((res) => {
                Helpers.setLoading(false);
                this.showPdfReport = true;
                this.pdfData = URL.createObjectURL(res);
                //this.pdfData = url.link;
                /*             let dataViewPdf: Uint8Array = new Uint8Array(res);
                            this.pdfData = URL.createObjectURL(dataViewPdf);
                            console.log('array buffer: ', ArrayBuffer.isView(res)); */
                //console.log("Response: ",res)
            }, (rej) => {
                Helpers.setLoading(false)
            })
    }
}
