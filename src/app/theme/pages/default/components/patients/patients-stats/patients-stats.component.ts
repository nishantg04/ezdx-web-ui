import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';

import { Patient } from '../patient';
import { PatientsService } from '../patients.service';
import { Facility } from '../../facilities/facility';
import { FacilitiesService } from '../../facilities/facilities.service';
import { Center } from '../../facilities/center';
import { UsersService } from '../../users/users.service';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./patients-stats.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class PatientsStatsComponent implements OnInit, AfterViewInit {
    userType: string;
    userInfo: any;
    patients: any[];
    facilities: any[];
    selectedFacility: Facility;
    selectedCenter: Center;
    centers: Center[];
    years: any[];
    selectedYear: any;
    fromDate: string;
    toDate: string;
    facilityId: string;
    centerId: string;
    userBulkList: any[];
    patientStats: any[];
    constructor(private _patientsService: PatientsService, private _script: ScriptLoaderService, private _router: Router, private _activeroute: ActivatedRoute, private _facilitiesService: FacilitiesService, private _usersService: UsersService, private _ngZone: NgZone) {
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        if (window.localStorage.getItem('userType') == 'Facility')
            this.userType = 'Facility';
        else
            this.userType = 'Admin';
    }
    ngOnInit() {
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
        window.my.namespace.navFacilityDetails = this.navFacilityDetails.bind(this);
        window.my.namespace.navCenterDetails = this.navCenterDetails.bind(this);
    }


    ngAfterViewInit() {
        Helpers.setLoading(true);
        window.localStorage.removeItem('patientStartDate');
        window.localStorage.removeItem('patientEndDate');
        this.fromDate = this._activeroute.snapshot.queryParams['from'];
        this.toDate = this._activeroute.snapshot.queryParams['to'];
        this.selectedFacility.id = this._activeroute.snapshot.queryParams['facilityId'];
        this.selectedCenter.id = this._activeroute.snapshot.queryParams['centerId'];
        if (this.fromDate && this.toDate) {
            window.localStorage.setItem('patientStartDate', this.fromDate);
            window.localStorage.setItem('patientEndDate', this.toDate);
            //this.onFilterClick();
        }
        this.getFacilities();
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/datatables/base/date.patients.js');
        // var facilitiesJSON = '[{"name":"Darshan Hospital","email":"admin@darshan.co.in"}]';
        // localStorage.setItem('facilityTable', facilitiesJSON);
        // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
        //     'assets/demo/default/custom/components/datatables/base/data-local.1.js');
    }

    onFilterClick(): void {
        Helpers.setLoading(true);
        if (this.selectedFacility.id != "ALL") {
            if (this.selectedCenter.id != "ALL") {
                this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedCenter.id);
            }
            else {
                this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "FACILITY", this.selectedFacility.id);
            }
        }
        else {
            var endDate = new Date();
            endDate.setDate(endDate.getDate() + 1);
            endDate.setHours(23);
            endDate.setMinutes(23);
            endDate.setSeconds(23);
            endDate.setMilliseconds(0);
            this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0);
        }
    }


    getFacilities(): void {
        // Helpers.setLoading(true);
        // this._facilitiesService.getFacilities(1, 10)
        //     .then(facilities => this.facilities = facilities.organizations)
        //     .then(() => {
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        //Helpers.setLoading(false);
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - 29);
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
        startDate.setMilliseconds(0);
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        endDate.setHours(23);
        endDate.setMinutes(23);
        endDate.setSeconds(23);
        endDate.setMilliseconds(0);
        if (!this.fromDate && !this.toDate) {
            this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
            if (window.localStorage.getItem('userType') == 'Facility') {
                let allFacilities = jQuery.extend(true, {}, this.facilities);
                this.facilities = [];
                allFacilities.forEach(fclt => {
                    if (fclt.id == this.userInfo.facilityId) {
                        this.facilities.push(fclt);
                    }
                });
            }
            else {
                //this.list(0, new Date(endDate).getTime(), "ALL", 0);
                let allFacility = new Facility();
                allFacility.name = "ALL";
                allFacility.id = "ALL";
                this.facilities.splice(0, 0, allFacility);
                this.selectedFacility.id = this.facilities[0].id;
                this.centers = this.facilities[0].centers;
                let allCenter = new Center();
                allCenter.name = "ALL";
                allCenter.id = "ALL";
                // this.centers.splice(0, 0, allCenter);
                // this.selectedCenter.id = this.centers[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
            let searchcriteria = "ALL";
            if (window.localStorage.getItem('userType') === "Facility") {
                searchcriteria = "FACILITY";
                this.list(new Date(startDate).getTime(), new Date(endDate).getTime(), searchcriteria, this.selectedFacility.id);
            }
            else {
                this.list(new Date(startDate).getTime(), new Date(endDate).getTime(), searchcriteria, 0);
            }
        }
        else {
            // let allFacility = new Facility();
            // allFacility.name = "ALL";
            // allFacility.id = "ALL";
            // this.facilities.splice(0, 0, allFacility);
            // if (this.selectedFacility.id != "ALL") {
            //     this.facilities.forEach(fac => {
            //         if (fac.id === this.selectedFacility.id) {
            //             this.centers = fac.centers;
            //             let allCenter = new Center();
            //             allCenter.name = "ALL";
            //             allCenter.id = "ALL";
            //             this.centers.splice(0, 0, allCenter);
            //         }
            //     });
            // }
            this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
            if (window.localStorage.getItem('userType') == 'Facility') {
                let allFacilities = jQuery.extend(true, [], this.facilities);
                this.facilities = [];
                allFacilities.forEach(fclt => {
                    if (fclt.id == this.userInfo.facilityId) {
                        this.facilities.push(fclt);
                    }
                });
            }
        }
        this.onFilterClick();
        // });
    }

    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers[0].id !== "ALL") {
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                }
                this.selectedCenter.id = this.centers[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }
    generateArray(obj) {
        return Object.keys(obj).map((key) => { return { key: key, value: obj[key] } });
    }

    list(fromDate, toDate, searchcriteria, id): void {
        //Helpers.setLoading(true);
        this._patientsService.getPatients(fromDate, toDate, searchcriteria, id)
            .then(devices => this.patients = devices)
            .then(() => {
                //Helpers.setLoading(true);
                var userIds = [];
                this.patients.forEach(function(val, i) {
                    userIds.push(val.userId);
                    var d = new Date(val.createTime),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                    val.createTime = [day, month, year].join('-') + ' (' + d.getHours() + ':' + d.getMinutes() + ')';
                    val.createTimeDate = d;
                });
                this.patientStats = [];
                let allFacilities = jQuery.extend(true, [], this.facilities);
                //this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
                allFacilities.forEach(fclt => {
                    var facility = {
                        id: fclt.id,
                        name: fclt.name,
                        facilityId: fclt.organizationCode,
                        count: 0,
                        centers: fclt.centers
                    }
                    this.patients.forEach(patient => {
                        if (patient.organizationId === facility.id) {
                            if (facility.centers) {
                                facility.centers.forEach(cntr => {
                                    if (!cntr.count)
                                        cntr.count = 0;
                                    if (patient.centerId === cntr.id) {
                                        facility.count += 1;
                                        cntr.count += 1;
                                    }
                                });
                            }
                        }
                    });
                    if (facility.centers && facility.centers.length > 0) {
                        facility.centers.sort((a, b) => b.count - a.count);
                        facility.centers.forEach(cntr => {
                            cntr.facilityId = facility.facilityId;
                            if (!cntr.count)
                                cntr.count = 0;
                        });
                    }
                    this.patientStats.push(facility);

                });
                // console.log(this.patientStats)
                this.patientStats.sort((a, b) => b.count - a.count);
                localStorage.setItem('patientsStatsTable', JSON.stringify(this.patientStats));
                this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                    'assets/demo/default/custom/components/datatables/child/data-local.patients.js');
                Helpers.setLoading(false);

            });
    }

    goToFacilityDetails(facilityId) {
        this._router.navigate(['/facilities/details'], { queryParams: { facilityId: facilityId } });
    }



    navCenterDetails(centerId, facilityId) {
        this._ngZone.run(() => this.goToCenterDetails(centerId, facilityId));
    }

    navFacilityDetails(facilityId) {
        this._ngZone.run(() => this.goToFacilityDetails(facilityId));
    }

    goToCenterDetails(centerId, facilityId) {
        this._router.navigate(['/center/details'], { queryParams: { center: String(centerId), facility: String(facilityId) } });

    }


}