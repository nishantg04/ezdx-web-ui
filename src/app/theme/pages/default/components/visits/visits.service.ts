import { Injectable } from '@angular/core';
import { Headers, Http, ResponseContentType } from '@angular/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { Helpers } from './../../../../../helpers';

import 'rxjs/add/operator/toPromise';

import { Visit } from './visit';

import { environment } from '../../../../../../environments/environment'
import { retry } from 'rxjs/operator/retry';
@Injectable()
export class VisitsService {

    private headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private headerspdf = new Headers({ 'Content-Type': 'application/json', 'X-Content-Type-Options': 'nosniff', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private serviceUrl2 = environment.BaseURL + environment.VisitCommandURL; //http://dev.ezdx.healthcubed.com/ezdx-visit-command/api/v1/visits';  // URL to web api
    private serviceUrl3 = environment.BaseURL + environment.VisitTestURL; //'http://dev.ezdx.healthcubed.com/ezdx-visit-query/api/v1/tests';
    private serviceUrlTestMaster = environment.BaseURL + environment.TestMasterURL;//'http://dev.ezdx.healthcubed.com/ezdx-test-master-srv/api/v1/testmaster';
    private serviceUrlDashbaord = environment.BaseURL + environment.DashboardSrvURL;
    private serviceUrl = environment.BaseURL + environment.VisitQueryURL;// 'http://dev.ezdx.healthcubed.com/ezdx-visit-query/api/v1/visits';
    private testSearching = environment.BaseURL + environment.testSearch;
    private testFacilitySearching = environment.BaseURL + environment.facilitytestSearch;

    constructor(private http: Http, private _router: Router, private _script: ScriptLoaderService) { }

    getVisits(): Promise<Visit[]> {
        return this.http.get(this.serviceUrl + "/search?searchcriteria=ALL&page=1&limit=100", { headers: this.headers })
            .toPromise()
            .then(response => { return response.json().visits as Visit[] })
            .catch(this.handleError.bind(this));
    }

    getTests(): Promise<any[]> {
        return this.http.get(this.serviceUrlTestMaster + "/search?searchcriteria=ALL", { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as any[] })
            .catch(this.handleError.bind(this));
    }

    getDashboardCounts(): Promise<any> {
        return this.http.get(this.serviceUrlDashbaord + "/count", { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getVisitsDate(fromDate, toDate, searchcriteria, id): Promise<Visit[]> {
        var getUrl = this.serviceUrl + "/search/date?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/search/date?searchcriteria=FACILITY&facilityId=" + id + "&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/search/date?searchcriteria=CENTER&centerId=" + id + "&from=" + fromDate + "&to=" + toDate;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json().visits as Visit[] : [] })
            .catch(this.handleError.bind(this));
    }

    getDiagnosisDate(fromDate, toDate, searchcriteria, facilityid, centerid, page, size): Promise<any> {
        var getUrl = this.serviceUrl3 + "/search?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate + "&page=" + page + "&limit=" + size;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl3 + "/search?searchcriteria=FACILITY&facilityId=" + facilityid + "&from=" + fromDate + "&to=" + toDate + "&page=" + page + "&limit=" + size;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl3 + "/search?searchcriteria=CENTER&centerId=" + centerid + "&facilityId=" + facilityid + "&from=" + fromDate + "&to=" + toDate + "&page=" + page + "&limit=" + size;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                return (response.status === 200) ? response.json() : []
            })
            .catch(this.handleError.bind(this));
    }


    getTestsForMonth(searchcriteria, id): Promise<any[]> {
        var getUrl = this.serviceUrl3 + "/count/type/month?searchcriteria=ALL";
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl3 + "/count/type/month?searchcriteria=FACILITY&facilityid=" + id;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl3 + "/count/type/month?searchcriteria=CENTER&centerid=" + id;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any[] : [] })
            .catch(this.handleError.bind(this));
    }

    getTestsCountByWeek(searchcriteria, fromDate, toDate, facilityid, centerId): Promise<any[]> {
        var getUrl = this.serviceUrl3 + "/count/type/week?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl3 + "/count/type/week?searchcriteria=FACILITY&from=" + fromDate + "&to=" + toDate + "&facilityid=" + facilityid;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl3 + "/count/type/week?searchcriteria=CENTER&from=" + fromDate + "&to=" + toDate + "&facilityid=" + facilityid + "&centerid=" + centerId;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any[] : [] })
            .catch(this.handleError.bind(this));
    }

    getNewVisitCount(startDate, endDate, searchcriteria, facilityid, centerid): Promise<any> {
        var url = this.serviceUrl + "/search/date?searchcriteria=" + searchcriteria + "&from=" + startDate + "&to=" + endDate + "&page=1&limit=10";
        if (searchcriteria == "FACILITY")
            url += "&facilityId=" + facilityid;
        if (searchcriteria == "CENTER")
            url += "&centerId=" + centerid;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() : response.json() })
            .catch(this.handleError.bind(this));
    }

    getVisitCounts(startDate, endDate, searchcriteria, facilityid, centerid) {
        var url = this.serviceUrl + "/search?searchcriteria=" + searchcriteria + "&from=" + startDate + "&to=" + endDate + "&page=1&limit=10";
        if (searchcriteria == "FACILITY")
            url += "&facilityId=" + facilityid;
        if (searchcriteria == "CENTER")
            url += "&centerId=" + centerid + "&facilityId=" + facilityid;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getNewTestCount(startDate, endDate, searchcriteria, facilityid, centerid): Promise<any> {
        var url = this.serviceUrl + "/search/date?searchcriteria=" + searchcriteria + "&from=" + startDate + "&to=" + endDate + "&page=0&limit=0";
        if (searchcriteria == "FACILITY")
            url += "&facilityId=" + facilityid;
        if (searchcriteria == "CENTER")
            url += "&centerId=" + centerid;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json().count : 0 })
            .catch(this.handleError.bind(this));
    }

    getTestCounts(startDate, endDate, searchcriteria, facilityid, centerid): Promise<any> {
        var url = this.serviceUrl3 + "/search?searchcriteria=" + searchcriteria + "&from=" + startDate + "&to=" + endDate + "&page=1&limit=10";
        if (searchcriteria == "FACILITY")
            url += "&facilityId=" + facilityid;
        if (searchcriteria == "CENTER")
            url += "&centerId=" + centerid + "&facilityId=" + facilityid;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    downloadECG(opt) {
        var url = environment.BaseURL + environment.ecgReport + '/?path=' + opt;
        return this.http.get(url, { headers: this.headerspdf, responseType: ResponseContentType.Blob })
            .toPromise()
            .then(response => {
                return (response.status === 200) ? new Blob([response.blob()], { type: 'application/pdf' }) : response.statusText;
            })
            .catch(this.handleError.bind(this));
    }

    searchByTestBasedOnAll(type, name, pageNo, size, startDate, endDate) {
        var url = this.testSearching + '?searchcriteria=NAME' + '&type=' + type + '&name=' + name + '&page=' + pageNo + '&limit=' + size + "&from=" + startDate + "&to=" + endDate
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() : [] })
            .catch(this.handleError.bind(this));
    }

    searchByTestBasedOnUser(type, name, id, pageNo, size, startDate, endDate) {
        var url = this.testSearching + '?searchcriteria=NAME_OPR' + '&type=' + type + '&name=' + name + '&userId='+id+ '&page=' + pageNo + '&limit=' + size + "&from=" + startDate + "&to=" + endDate
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() : [] })
            .catch(this.handleError.bind(this));
    }

    searchByUserBasedOnAll(userId, pageNo, size, startDate, endDate) {
        var url = this.testSearching + '?searchcriteria=USER&userId=' + userId + '&page=' + pageNo + '&limit=' + size + "&from=" + startDate + "&to=" + endDate
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() : [] })
            .catch(this.handleError.bind(this));
    }

    searchByTestBasedonFacilty(facilityId, type, name, pageNo, size, startDate, endDate) {
        var url = this.testFacilitySearching + '/' + facilityId + '/search?searchcriteria=NAME&type=' + type + '&name=' + name + '&page=' + pageNo + '&limit=' + size + "&from=" + startDate + "&to=" + endDate
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() : [] })
            .catch(this.handleError.bind(this));
    }

    searchByUserBasedonFacilty(facilityId, userId, pageNo, size, startDate, endDate) {
        var url = this.testFacilitySearching + '/' + facilityId + '/search?searchcriteria=USER&userId=' + userId + '&page=' + pageNo + '&limit=' + size + "&from=" + startDate + "&to=" + endDate
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() : [] })
            .catch(this.handleError.bind(this));
    }

    searchByTestBasedonCenter(facilityId, centerId, type, name, pageNo, size, startDate, endDate) {
        var url = this.testFacilitySearching + '/' + facilityId + '/center/' + centerId + '/search?searchcriteria=NAME&type=' + type + '&name=' + name + '&page=' + pageNo + '&limit=' + size + "&from=" + startDate + "&to=" + endDate
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() : [] })
            .catch(this.handleError.bind(this));
    }

    searchByUserBasedonCenter(facilityId, centerId, userId, pageNo, size, startDate, endDate) {
        var url = this.testFacilitySearching + '/' + facilityId + '/center/' + centerId + '/search?searchcriteria=USER&userId=' + userId + '&page=' + pageNo + '&limit=' + size + "&from=" + startDate + "&to=" + endDate
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() : [] })
            .catch(this.handleError.bind(this));
    }

    getVisitByPatientId(patientId: string, page: number, limit: number): Promise<any> {
        return this.http.get(this.serviceUrl + "/search?searchcriteria=PATIENT&patientId=" + patientId + "&page=" + page + "&limit=" + limit, { headers: this.headers })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError.bind(this));
    }

    getVisitByUser(userId, startDate, endDate) {
        var url = this.serviceUrl + "/search?searchcriteria=OPERATOR&operatorId=" + userId + "&from=" + startDate + "&to=" + endDate
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load('body',
                'assets/demo/default/custom/components/utils/redirect-cidaas-logout.js');
        }
        return Promise.reject(error.message || error);
    }
}