import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FacDropdownComponent } from './fac-dropdown.component';
import { AutoCompleteModule } from 'primeng/primeng';
import { FacilitiesService } from './../../theme/pages/default/components/facilities/facilities.service';
import { UsersService } from './../../theme/pages/default/components/users/users.service';
import { MasterTestService } from './../../theme/pages/default/components/master-test/master-test.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';


@NgModule({
    imports: [
        CommonModule, AutoCompleteModule, FormsModule, TranslateModule.forChild({ isolate: false })
    ],
    declarations: [
        FacDropdownComponent
    ],
    exports: [
        FacDropdownComponent
    ],
    providers: [
        FacilitiesService, UsersService, MasterTestService
    ]
})
export class FacDropdownModule { }
