//== Class definition

var FormControls = function () {
    //== Private functions

    var facilityForm = function () {
        $("#m_form_1").validate({
            // define validation rules
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                mobile: {
                    required: true
                },
                organizationCode: {
                    required: true
                },
                type: {
                    required: true
                }
            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
            }
        });

        $("#center_form").validate({
            // define validation rules
            rules: {
                cname: {
                    required: true
                }
            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                var alert = $('#center_form_msg');
                alert.removeClass('m--hide').show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
            }
        });
    }

    var deviceForm = function () {
        $("#m_form_device").validate({
            // define validation rules
            rules: {
                serialNumber: {
                    required: true
                },
                type: {
                    required: true
                }
            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                var alert = $('#m_form_2_msg');
                alert.removeClass('m--hide').show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
            }
        });
    }

    var userForm = function () {
        $("#m_form_user").validate({
            // define validation rules
            rules: {
                firstName: {
                    required: true
                },
                lastName: {
                    required: true
                },
                email: {
                    required: true
                },
                phone: {
                    required: true
                }
            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                var alert = $('#m_form_2_msg');
                alert.removeClass('m--hide').show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
            }
        });
    }

    return {
        // public functions
        init: function () {
            facilityForm();
            deviceForm();
            userForm();
        }
    };
}();

jQuery(document).ready(function () {
    FormControls.init();
});