import { Center } from './center';
import { Address } from './address';
import { Geopoint } from './geopoint'
export class Facility {
    id: string;
    name: string = "";
    organizationCode: string = "";
    phone: string = "";
    mobile: string = "";
    dial_code: string = ""
    email: string = "";
    logo: string = "";
    picture: string = "";
    description: string = "";
    panNumber: string = "";
    gstNumber: string = "";
    managerId: string = "";
    department: string = "";
    type: string = "INDIVIDUAL_HOME";
    createTime: number;
    updateTime: number;
    centers: Center[];
    address: Address;
    geopoint: Geopoint;
}