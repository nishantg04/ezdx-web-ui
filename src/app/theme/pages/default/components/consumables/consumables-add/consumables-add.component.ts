import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { ConsumablesService } from '../consumables.service';
import { VisitsService } from '../../visits/visits.service';
import { element } from 'protractor';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./consumables-add.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class ConsumablesAddComponent implements OnInit, AfterViewInit {
    consumable: any;
    loading: boolean;
    msg: string;
    testList: any[];
    tests: any[];
    constructor(private _script: ScriptLoaderService, private _consumablesService: ConsumablesService, private _visitsService: VisitsService, private _router: Router, private _activeroute: ActivatedRoute) {
        this.loading = false;
        this.testList = [
            { type: "physical", name: "Physical", tests: [{ name: "BLOOD_PRESSURE", type: "BLOOD_PRESSURE" }, { name: "PULSE_OXIMETER", type: "PULSE_OXIMETER" }, { name: "TEMPERATURE", type: "TEMPERATURE" }, { name: "HEIGHT", type: "Height" }, { name: "WEIGHT", type: "Weight" }, { name: "BMI", type: "BMI" }, { name: "ECG", type: "ECG" }, { name: "MID_ARM_CIRCUMFERENCE", type: "MID_ARM_CIRCUMFERENCE" }] },
            { type: "whole_blood_poct", name: "Whole Blood-POCT", tests: [{ name: "GLUCOSE", type: "GLUCOSE" }, { name: "HEMOGLOBIN", type: "HEMOGLOBIN" }, { name: "CHOLESTEROL", type: "CHOLESTEROL" }, { name: "URIC_ACID", type: "URIC_ACID" }] },
            { type: "whole_blood", name: "Whole Blood", tests: [{ name: "BLOOD_GROUPING", type: "BLOOD_GROUPING" }] },
            { type: "whole_blood_rdt", name: "Whole Blood-RDT", tests: [{ name: "MALARIA", type: "Malaria" }, { name: "DENGUE", type: "Dengue" }, { name: "TYPHOID", type: "Typhoid" }, { name: "HEP_B", type: "HEP_B" }, { name: "HEP_C", type: "HEP_C" }, { name: "HIV", type: "HIV" }, { name: "CHIKUNGUNYA", type: "Chikungunya" }, { name: "SYPHILIS", type: "Syphilis" }, { name: "TROPONIN_1", type: "TROPONIN_1" }] },
            { type: "urine_poct", name: "Urine-POCT", tests: [{ name: "PREGNANCY", type: "Pregnancy" }] },
            { type: "urine_rdt", name: "Urine-RDT", tests: [{ name: "URINE_SUGAR", type: "URINE_SUGAR" }, { name: "URINE_PROTEIN", type: "URINE_PROTEIN" }, { name: "LEUKOCYTES", type: "Leukocytes" }, { name: "UROBILINOGEN", type: "Urobilinogen" }, { name: "NITRITE", type: "Nitrite" }, { name: "PH", type: "pH" }, { name: "KETONE", type: "Ketone" }, { name: "BLOOD", type: "Blood" }, { name: "BILIRUBIN", type: "BILIRUBIN" }, { name: "SPECIFIC_GRAVITY", type: "SPECIFIC_GRAVITY" }] }
        ];

        var tempTest = [];
        this.testList.forEach(type => {
            if (type.type != "physical") {
                type.tests.forEach(test => {
                    tempTest.push(test);
                });
            }
        });
        // console.log(' tempTest', tempTest)
        this.tests = [];
        tempTest.forEach(test => {
            if (test.name != 'BILIRUBIN' || test.name != 'BLOOD' || test.name != 'HEP_B' || test.name != 'BLOOD' ||
                test.name != 'KETONE' || test.name != 'LEUKOCYTES' || test.name != 'NITRITE' || test.name != 'PH' ||
                test.name != 'SPECIFIC_GRAVITY' || test.name != 'TROPONIN_1' || test.name != 'TYPHOID' || test.name != 'URINE' ||
                test.name != 'UROBILINOGEN') {
                this.tests.push(test)
            }

        })
        // console.log('test list', this.tests)
        this.tests.sort(function(a, b) { return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0); });
        this.consumable = {
            id: null,
            quantity: 0,
            manufacturer: null,
            description: '',
            testType: null,
            testName: null,
            itemsPerPackage: 1,
            items: 0,
            expiryDate: null,
            lotNumber: null,
            consumableType: null,
            lotCode: null
        }
        this.consumable.testType = this.tests[0].type;
        this.consumable.testName = this.tests[0].name;
        this.getTests();
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/forms/validation/form-controls.js');
        let dateToday = new Date();
        this.consumable.expiryDate = new Date(dateToday.getFullYear(), dateToday.getMonth() + 1, 1);
        window.localStorage.setItem('consumableExpiryDate', this.consumable.expiryDate);
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/forms/widgets/consumable-datepicker.js');
    }

    onQuantityChange() {
        this.consumable.items = this.consumable.quantity * this.consumable.itemsPerPackage;
    }

    selectDate(event) {
        window.localStorage.setItem('lotExpiryDate', event.target.value);
    }

    onTestChange() {
        //Getting selected test type to display
        this.testList.forEach(test => {
            if (test.testName === this.consumable.testName) {
                this.consumable.consumableType = test.consumableType;
                this.consumable.testType = test.testType;
            }
        });
    }

    getTests() {
        Helpers.setLoading(true);
        this.loading = true;
        this._visitsService.getTests()
            .then((data) => {
                this.testList = data;
            })
            .then(() => {
                this.tests = [];
                var tempTest = [];
                this.testList.forEach(test => {
                    if (test.testType != "PHYSICAL") {
                        tempTest.push(test);
                    } else {
                        // console.log('physi', test)
                    }
                });
                tempTest.sort(function(a, b) { return (a.testName > b.testName) ? 1 : ((b.testName > a.testName) ? -1 : 0); });
                // console.log('all test', tempTest)
                tempTest.forEach(test => {
                    // if (test.testName == 'BLOOD_GLUCOSE') {
                    //     test.testName = 'Blood Glucose'
                    // }
                    //  if(test.testName != 'BILIRUBIN' && test.testName != 'BLOOD' &&  test.testName != 'BLOOD' &&
                    //   test.testName !='KETONE' && test.testName != 'LEUKOCYTES' && test.testName != 'NITRITE' && test.testName != 'PH' &&
                    // test.testName != 'SPECIFIC_GRAVITY'   && test.testName != 'URINE'&&
                    // test.testName != 'UROBILINOGEN'){
                    //      this.tests.push(test)
                    //  }
                    if (test.active)
                        this.tests.push(test);
                })

                if (window.localStorage.getItem('selectedConsumable') && window.location.pathname == "/consumables/update") {
                    this.consumable = JSON.parse(window.localStorage.getItem('selectedConsumable'));
                    var d = new Date(this.consumable.expiryDate),
                        month = '' + (d.getMonth() + 1),
                        day = '1',
                        year = d.getFullYear();
                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                    this.consumable.expiryDate = [month, day, year].join('/');
                    window.localStorage.setItem('consumableExpiryDate', this.consumable.expiryDate);
                    this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                        'assets/demo/default/custom/components/forms/widgets/consumable-datepicker.js');
                    this.tests.forEach(tst => {
                        if (tst.testName === this.consumable.testName) {
                            this.consumable.testType = tst.testType;
                            this.consumable.consumableType = tst.consumableType;
                        }
                    });
                }
                else {
                    this.consumable.testType = this.tests[0].testType;
                    this.consumable.testName = this.tests[0].testName;
                    this.consumable.consumableType = this.tests[0].consumableType;
                }
                Helpers.setLoading(false);
                this.loading = false;
                //this._router.navigate(['/devices/list']);
            }, (e: any) => {
                // console.log(e);
                Helpers.setLoading(false);
                this.loading = false
            });
    }

    save(): void {
        this.msg = null;

        //Getting selected test name to save
        // this.testList.forEach(test => {
        //     if (test.testType === this.consumable.testType) {
        //         this.consumable.testName = test.testName;
        //         this.consumable.consumableType = test.consumableType;
        //     }
        // });

        //Getting date from datepicker control
        this.consumable.expiryDate = new Date(window.localStorage.getItem('consumableExpiryDate')).toJSON();

        if (!this.loading) {
            Helpers.setLoading(true);
            this.loading = true;
            if (this.consumable.id) {
                this.consumable.isExpired = undefined;
                this._consumablesService.update(this.consumable)
                    .then((data) => console.log(data))
                    .then(() => {
                        Helpers.setLoading(false);
                        this.loading = false;
                        this._router.navigate(['/consumables/list']);
                    }, (e: any) => {
                        // console.log(e);
                        let res = JSON.parse(e._body);
                        if (res.error) {
                            this.msg = res.error;
                        }
                        Helpers.setLoading(false);
                        this.loading = false
                    });

            } else {
                this._consumablesService.create(this.consumable)
                    .then((data) => console.log(data))
                    .then(() => {
                        Helpers.setLoading(false);
                        this.loading = false;
                        this._router.navigate(['/consumables/list']);
                    }, (e: any) => {
                        // console.log(e);
                        let res = JSON.parse(e._body);
                        if (res.error) {
                            this.msg = res.error;
                        }
                        Helpers.setLoading(false);
                        this.loading = false
                    });
            }
        }
    }

}