import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { OrderModule } from 'ngx-order-pipe';
import { RdtAddComponent } from './rdt-add.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DataTableModule, SharedModule, PaginatorModule, DialogModule } from 'primeng/primeng';
import { RdtService } from '../rdt.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": RdtAddComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule, DataTableModule, SharedModule, PaginatorModule, DialogModule,
        TranslateModule.forChild({ isolate: false }),
    ], exports: [
        RouterModule
    ], declarations: [
        RdtAddComponent,
    ],
    providers: [
        RdtService, TranslateService
    ]

})
export class RdtAddModule {



}