import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { VisitsStatsComponent } from './visits-stats.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { VisitsService } from '../visits.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { UsersService } from '../../users/users.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": VisitsStatsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, TranslateModule.forChild({ isolate: false }), FormsModule, RouterModule.forChild(routes), LayoutModule
    ], exports: [
        RouterModule
    ], declarations: [
        VisitsStatsComponent
    ],
    providers: [
        VisitsService,
        FacilitiesService,
        UsersService,
        TranslateService
    ]

})
export class VisitsStatsModule {



}