import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { ConsumablesService } from '../consumables.service';
import { Facility } from "../../facilities/facility";
import { Center } from "../../facilities/center";
import { FacilitiesService } from "../../facilities/facilities.service";


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./consumables-list.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ConsumablesListComponent implements OnInit, AfterViewInit {
    consumables: any[];
    enableAssignMode: boolean;
    enableShipMode: boolean;
    sortType: string;
    sort: boolean;
    reorders: any[];
    checkAll: boolean;
    sortTypeCons: string;
    sortCons: boolean;
    facilities: any[];
    selectedFacility: any;
    centers: any[];
    selectedCenter: any;
    isShipped: boolean;
    consumableCount: any;
    pageno: any;
    size: any;
    constructor(private _facilitiesService: FacilitiesService, private _ngZone: NgZone, private _script: ScriptLoaderService, private _consumablesService: ConsumablesService, private _router: Router, private _activeroute: ActivatedRoute) {
        // this.consumables = [
        //     { id: 'asdd', name: "Cholesterol strips", lotno: "H17006057", manufacturer: "Online Surgicals", expirydt: "08/2018", sku: "SYNG3004", test: "Cholesterol", testdt: "20/12/2017", qntpack: 200, qntperpack: 10, totalunits: 2000, note: "", customer: "Healthcubed", center: "Bangalore", selected: false },
        //     { id: '123', name: "Hemoglobin strips", lotno: "H17006057", manufacturer: "Online Surgicals", expirydt: "08/2018", sku: "SYNG3004", test: "Hemoglobin", testdt: "20/12/2017", qntpack: 100, qntperpack: 10, totalunits: 1000, note: "", customer: "Healthcubed", center: "Bangalore", selected: false },
        //     { id: 'as44dd', name: "Urine strips", lotno: "H17006057", manufacturer: "Online Surgicals", expirydt: "08/2018", sku: "SYNG3004", test: "Urine Sugar", testdt: "20/12/2017", qntpack: 50, qntperpack: 10, totalunits: 500, note: "", customer: "Healthcubed", center: "Bangalore", selected: false }
        // ];
        this.enableAssignMode = false;
        this.enableShipMode = false;
        this.sortType = "name";
        this.sort = true;
        this.sortTypeCons = "name";
        this.sortCons = false;
        this.checkAll = false;
        this.isShipped = false;
        this.reorders = [
            { name: 'Pavan G', orgName: 'Healthcubed', center: 'Bangalore', quantity: 500, consumableName: 'Cholesterol strips', notes: 'Need it ASAP', datetime: "08/12/2017 10 AM" },
            { name: 'Hemant', orgName: 'Healthcubed', center: 'Ahmedabad', quantity: 100, consumableName: 'Urine strips', notes: 'Send invoice as well', datetime: "08/12/2017 4 PM" }
        ];
        this.pageno = 1;
        this.size = 10;
    }
    ngOnInit() {
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
        window.my.namespace.consumableUpdatePublicFunc = this.consumableUpdatePublicFunc.bind(this);
    }
    ngOnDestroy() {
        window.my.namespace.publicFunc = null;
    }
    ngAfterViewInit() {
        this.list(this.pageno, this.size, "ALL", "", "consumableType", "asc");
        this.getFacilities();
        // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
        //     'assets/demo/default/custom/components/forms/validation/form-controls.js');
    }
    oncheckAll(): void {
        this.checkAll = !this.checkAll;
        this.consumables.forEach((cons, i) => {
            cons.selected = this.checkAll;
        });
    }

    editCosumable(selectedConsumable): void {
        window.localStorage.setItem("selectedConsumable", JSON.stringify(selectedConsumable));
        this._router.navigate(['/consumables/update']);
    }

    consumableUpdatePublicFunc(id) {
        this._ngZone.run(() => this.goToConsumableEdit(id));
    }

    goToConsumableEdit(id) {
        //this._router.navigate(['/consumables/update'], { queryParams: { consumableId: id } });
        this.enableAssignMode = true;
    }

    toggleShipMode() {
        this.enableAssignMode = !this.enableAssignMode;
        this.enableShipMode = !this.enableShipMode;
    }

    toggleAssignMode() {
        this.isShipped = false;
        this.enableShipMode = false;
        this.enableAssignMode = !this.enableAssignMode;
        // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
        //     'assets/demo/default/custom/components/datatables/base/data-local.consumables.js');
    }

    toggleShipped() {
        this.isShipped = !this.isShipped;
    }

    list(pageno, pagesize, search, searchterm, orderby, sortby): void {
        Helpers.setLoading(true);
        this._consumablesService.list(pageno, pagesize, search, searchterm, orderby, sortby)  // for get All facility we should pass page and size as zero
            .then(consumables => {
                Helpers.setLoading(false);
                this.consumables = consumables.masterInventory;
                this.consumableCount = consumables.count;
            })
            .then(() => {
                Helpers.setLoading(false);
                this.consumables.forEach(consumable => {
                    consumable.expiryDate = new Date(consumable.expiryDate);
                    var d = new Date(consumable.expiryDate),
                        month = '' + (d.getMonth() + 1),
                        day = '1',
                        year = d.getFullYear();


                    d = new Date(d.getFullYear(), d.getMonth() + 1, 0);
                    var today = new Date();
                    today = new Date(today.getFullYear(), today.getMonth() + 1, 0);
                    if (d < today) {
                        consumable.isExpired = true;
                    }
                    else {
                        consumable.isExpired = false;
                    }/* 
                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day; */
                    //consumable.expiryDate = [month, year].join('-');
                    // if (consumable.testName.includes('_'))
                    //     consumable.testName = consumable.testName.replace('_', ' ')
                });
            })
    }

    paginateConsumable(event) {
        this.pageno = event.page + 1;
        this.list(this.pageno, this.size, "ALL", "", "consumableType", "asc");
    }

    getFacilities(): void {
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            } else if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        this.facilities.sort(function(a, b) {
            return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
        });

        this.selectedFacility = jQuery.extend(true, {}, this.facilities[0]);
        this.centers = this.selectedFacility.centers;
        if (this.centers) {
            this.centers.sort(function(a, b) {
                return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
            });
            this.selectedCenter = jQuery.extend(true, {}, this.centers[0]);
        }
    }

    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers) {
                    this.centers.sort(function(a, b) {
                        return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
                    });
                    this.selectedCenter = jQuery.extend(true, {}, this.centers[0]);
                }
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }

    onCenterChange(centerId: string): void {
    }
}