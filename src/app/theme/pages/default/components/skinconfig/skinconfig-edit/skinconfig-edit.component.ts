import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { ConfigService } from '../tests.service';
import { truncate } from 'fs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { validateConfig } from '@angular/router/src/config';
import {UsersService} from '../../users/users.service'

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./skinconfig-edit.component.html",
    styleUrls: ['../../../primeng.component.scss', './skinconfig-edit.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SkinconfigEditComponent implements OnInit {

  confId: any;
  idList: any;
  dummyArray: any=[];
  mandatoryArr: any=[];
  checkAllTest: boolean=true;
  mandate:any={};
  testRangeArray: any[] = [];
  public config:any={};
  public showtests:boolean=false;
  public showunit:boolean=false;
  public showlang:boolean=true;
  public showUpperAndLowerLimit:boolean=false;
  public alltestList:any=[];
  public newtests:any=[];
  public displayDialog:boolean=false;
  public testData:any={};
  public showMandatoryFields:boolean=false;
  public fieldLabelstate:any='';
  disableCustomer : boolean = true;
  noUser : boolean = false;
  selectedBpRanges : any= {};
  heightoption:any[]=[
    {label:'Feet',value:'ft'},
    {label:'cms',value:'cms'},
]
weightoption:any[]=[
  {label:'Kgs',value:'Kgs'},
  {label:'lbs',value:'lbs'},
]

  noCustomer : boolean = true;
  green : string = '#34bfa3';
  yellow : string = '#ffb822';
  gray : string = 'grey';
  isEditable : boolean = true;

  constructor(private _testsService:ConfigService, private _router:Router, private _activeroute:ActivatedRoute,private _usersService : UsersService  ) { }

  ngOnInit() {
    this._activeroute.queryParams.subscribe(param => {
        this.confId=param.confId;
     });
    this.config={
      language:'ENGLISH',
      testsMapped:[],
      testUnitMapping:[],
      testRanges:[],
      facilityId : '',
      configType:'CUSTOM'
    }
    this.mandate={
        AADHAR_CARD:false,
        DATE_OF_BIRTH:false,
        CITY_OR_VILLAGE:false,
        POSTAL_CODE:false,
        EMAIL:false,
        ADDRESS:false,
        STATE:false,
        COUNTRY:false,
    }
    this.getconfigfiledetail(this.confId);
    //this.getAllTests();
    
  }

  getconfigfiledetail(id) {
    Helpers.setLoading(true);
    this._testsService
        .getconfigfiledetail(id)
        .then(res => {            
            this.config=res; 
            this.noCustomer=false;   
            if(!this.config.facilityId && !this.config.facilityName)
               this.disableCustomer = false;
               
            this.fieldLabels.STATE=this.config.fieldLabels.STATE;  
            this.fieldLabels.CITY_OR_VILLAGE=this.config.fieldLabels.CITY_OR_VILLAGE;  
            if (this.config.mandatoryFields!=undefined) {
                this.mandatoryArr= this.config.mandatoryFields;
            }
           
            Object.keys(this.mandate).map((field)=>{
                if (this.mandatoryArr.indexOf(field)>-1) {
                    this.mandate[field]=true;
                }
            });
            Helpers.setLoading(false);
        }).then(()=>{
          this.getAllTests();
        });
}

  testChecked() {
    let count = 0;
    this.newtests.forEach(test => {
        if (test.selected) count++;
    })
    if (this.newtests.length === count) {
        this.checkAllTest = true;
    } else {
        this.checkAllTest = false;
    }
}

  getAllTests() {
    this.newtests = [];
    this.alltestList = [];
    this.testRangeArray = [];
    this._testsService.getAllTests().then(res => {
        this.alltestList = res;
        this.alltestList.forEach(test => {
            if (test.active) {
                this.newtests.push(test)
           }
        })       
        this.newtests.forEach(test => {
            test.testIconUrl = "./assets/app/media/img/test/" + test.testName + ".png"
        })
        this.newtests.forEach(test => {
            if (test.testName.includes('_')) {
                test.testName = test.testName.split('_').join(' ')
            }
            if (test.testName == 'URINE') {
                test.testName = 'URINE 2P'
            }
            if (test.testName == 'DENGUE') {
                test.testName = 'DENGUE ANTIGEN'
            }
            if (test.testType.includes('_')) {
                test.testType = test.testType.split('_').join(' ')
            }
            this.isEditable = false;
        })   
        this.newtests.forEach(test=>{
            if(test.testType != 'WHOLE BLOOD POCT' && test.testType != 'WHOLE BLOOD RDT'){
                if(test.testName != 'BLOOD GROUPING' && test.testName !='ECG' && test.testName !='SYMPTOM BASED TEST')
                  this.testRangeArray.push(test)               
            }
        })  
          
        this.testRangeArray.forEach(element => {
            element.selected = false;
            element.unit='NA';
            element.conversionFactor=1;
            this.config.testsMapped.map((testsmapped)=>{
                if(testsmapped==element.id){
                    element.selected = true;
                }
            })
            this.config.testUnitMapping.map((testsunitmapped)=>{
                if((testsunitmapped.testName).split('_').join(' ')==element.testName){
                    element.unit = testsunitmapped.testUnit;
                    element.conversionFactor = testsunitmapped.conversionFactor;
                }
            })
            this.config.testRanges.map((testRange)=>{
                if((testRange.testName).split('_').join(' ')==element.testName){
                    element.ranges = testRange.ranges;
                }
            })
            this.isEditable = false; 
        });
        
    });
}

oncheckAllTest(event): void {
  this.checkAllTest = event.target.checked;
  this.newtests.forEach((test, i) => {
      test.selected = event.target.checked;
      if (test.selected == true)
          this.dummyArray.push(test.id)
      if (test.selected == false) {
          var index = this.dummyArray.indexOf(test.id);
          this.dummyArray.splice(index, 1);
      }
  });
  this.idList = this.dummyArray; 
  this.config.testsMapped=this.idList;
  
}

sTest(testvalue) {
  testvalue.selected = !testvalue.selected;
  this.newtests.forEach(test => {
      if (test.id == testvalue.id) {
          test.selected = testvalue.selected;
      }
  })
  let arr=[]
  this.newtests.map((item)=>{
      if(item.selected==true){
        arr.push(item.id);
      }
  });
  //console.log(arr);
  this.config.testsMapped=arr;
        
  
}


  //show tests
  nextToTest(lang):void{   
   
    if(this.config.facilityId){
        var searchcriteria = 'FACILITY'
        this. userList(searchcriteria, this.config.facilityId)
    }
   
    //this.getAllTests()       
  //  this.config.language=lang;
  
  }

  userList(searchcriteria, id) {   
    this._usersService.getUsersFiltered(0, 0, searchcriteria, id, 1,10, '', 'asc')
        .then(users => {
            if(users){
                this.showlang=false;
                this.showtests=true;
                if(this.disableCustomer == false){
                    this.assignCustomer()
                }
               
            }else{
               this.noUser = true;
            }           
        })
    }

//back to step 1
  backToLang():void{
    this.showtests=false;
    this.showlang=true;
    document.getElementById('step_two').style.backgroundColor = this.gray;
    document.getElementById('step_one').style.backgroundColor = this.yellow;
  }

//show unit
  nextToUnit():void{
    this.showtests=false;
    this.showunit=true;
    document.getElementById('step_three').style.backgroundColor = this.yellow;
    document.getElementById('step_two').style.backgroundColor = this.green; 
  }

//back to tests (step 2)
  backToTest():void{
    this.showunit=false;
    this.showtests=true;    
    document.getElementById('step_three').style.backgroundColor = this.gray;
    document.getElementById('step_two').style.backgroundColor = this.yellow;
  }

//show range
nextToRange(arr):void{
   // console.log(arr)
  this.showunit=false;
  this.showUpperAndLowerLimit=true;  
  document.getElementById('step_four').style.backgroundColor = this.yellow;
  document.getElementById('step_three').style.backgroundColor = this.green;
  let rangearr=[];
  this.testRangeArray.map((item)=>{
    if (item.testName == 'URINE 2P') {
        item.testName = 'URINE'
    }
    if (item.testName == 'DENGUE ANTIGEN') {
        item.testName = 'DENGUE'
    }
    rangearr.push({"testName":item.testName.split(' ').join('_'),"testUnit":item.unit,"conversionFactor":item.conversionFactor})
      
  });
  this.config.testUnitMapping=rangearr;
  //console.log(this.config)
}

//back to unit (step 3)
backToUnit():void{
  this.showUpperAndLowerLimit=false;
  this.showunit=true;    
  document.getElementById('step_four').style.backgroundColor = this.gray;
  document.getElementById('step_three').style.backgroundColor = this.yellow;  
}

//show mandatory fields
showMandatory():void{
    this.showUpperAndLowerLimit=false;
    this.showMandatoryFields=true;
  document.getElementById('step_four').style.backgroundColor = this.green;
  document.getElementById('step_five').style.backgroundColor = this.yellow;
    
}

//back to range

backToRange(){
    this.showMandatoryFields=false;
    this.showUpperAndLowerLimit=true;
  document.getElementById('step_five').style.backgroundColor = 'grey';
  document.getElementById('step_four').style.backgroundColor = this.yellow;
    
}


    setRangeforTest(test){
        this.testData={};
        this.testData=test;        
        this.displayDialog=true;
        if (this.testData.testName!='BLOOD PRESSURE' && this.testData.testName!='URIC ACID' && this.testData.testName!='HEMOGLOBIN' && this.testData.testName!='BLOOD GLUCOSE' ) {
            this.gen.lowerlimit=this.testData.ranges.result[0];
            this.gen.upperlimit=this.testData.ranges.result[1];
        }
        else if (this.testData.testName=='URIC ACID' || this.testData.testName=='HEMOGLOBIN') {
            this.gen.men.lowerlimit=this.testData.ranges.men[0];
            this.gen.men.upperlimit=this.testData.ranges.men[1];
            this.gen.women.lowerlimit=this.testData.ranges.women[0];
            this.gen.women.upperlimit=this.testData.ranges.women[1];
        }
        else if (this.testData.testName=='BLOOD PRESSURE') {      
            this.bpranges.systolicLower=this.testData.ranges.systolic[0];           
            this.bpranges.systolicUpper=this.testData.ranges.systolic[1];
            this.bpranges.diastolicLower=this.testData.ranges.diastolic[0];
            this.bpranges.diastolicUpper=this.testData.ranges.diastolic[1];
        }
        else if (this.testData.testName=='BLOOD GLUCOSE') {
            this.bloodGlucoseRanges={
                fastingDiabeticupper:this.testData.ranges.fasting[2],
                fastingnormallower:this.testData.ranges.fasting[0],
                fastingnormalupper:this.testData.ranges.fasting[1],
                postprandialDiabeticupper:this.testData.ranges.post_prandial[2],
                postprandialnormallower:this.testData.ranges.post_prandial[0],
                postprandialnormalupper:this.testData.ranges.post_prandial[1],
                randomDiabeticupper:this.testData.ranges.random[2],
                randomnormallower:this.testData.ranges.random[0],
                randomnormalupper:this.testData.ranges.random[1],
            }
        }
    }

    changedMandateValue(value):void{
        console.log(value)
        console.log(this.mandatoryArr)
        let index=this.mandatoryArr.indexOf(value)
        console.log(index)
        if (index==-1) {
            this.mandatoryArr.push(value)
        }else{
            this.mandatoryArr.splice(index,1)
        }
        console.log(this.mandatoryArr)
    }

    gen :any={
        lowerlimit:'',
        upperlimit:'',
        men:{
            lowerlimit:'',
            upperlimit:''
        },
        women:{
            lowerlimit:'',
            upperlimit:''
        },
    }
    bpranges:any={
        diastolicUpper:'',
        systolicUpper:'',
        systolicLower:'',
        diastolicLower:''
    }
    bloodGlucoseRanges:any={
        fastingDiabeticupper:'',
        fastingnormallower:'',
        fastingnormalupper:'',
        postprandialDiabeticupper:'',
        postprandialnormallower:'',
        postprandialnormalupper:'',
        randomDiabeticupper:'',
        randomnormallower:'',
        randomnormalupper:'',
    }
    setRange(){
        let rangearray=[];
        if (this.config.testRanges.length==0) {
            if(this.testData.testName=="BLOOD PRESSURE"){
                this.config.testRanges.push({
                    "testName": this.testData.testName.split(' ').join('_') ,
                    "rangeLabels":["low", "normal", "high"],
                    "ranges": {
                        "systolic": [this.bpranges.systolicLower, this.bpranges.systolicUpper],
                        "diastolic": [this.bpranges.diastolicLower, this.bpranges.diastolicUpper]
                    },
                })
            }else if(this.testData.testName=="BLOOD GLUCOSE"){
                this.config.testRanges.push({
                    "testName": this.testData.testName.split(' ').join('_') ,
                    "rangeLabels": ["pre-diabetic", "normal", "diabetic"],
                    "ranges": {
                        "fasting":[this.bloodGlucoseRanges.fastingnormallower,this.bloodGlucoseRanges.fastingnormalupper,this.bloodGlucoseRanges.fastingDiabeticupper],
                        "post_prandial":[this.bloodGlucoseRanges.postprandialnormallower,this.bloodGlucoseRanges.postprandialnormalupper,this.bloodGlucoseRanges.postprandialDiabeticupper],
                        "random":[this.bloodGlucoseRanges.randomnormallower,this.bloodGlucoseRanges.randomnormalupper,this.bloodGlucoseRanges.randomDiabeticupper]
                    },
                })
            }
            else if(this.testData.testName=="URIC ACID" || this.testData.testName=="HEMOGLOBIN"){
                this.config.testRanges.push({
                    "testName": this.testData.testName.split(' ').join('_') ,
                    "rangeLabels":["low", "normal", "high"],
                    "ranges": {
                        "men":[this.gen.men.lowerlimit,this.gen.men.upperlimit],
                        "women":[this.gen.women.lowerlimit,this.gen.women.upperlimit],
                    },
                })
            }else{
                this.config.testRanges.push({
                    "testName": this.testData.testName.split(' ').join('_') ,
                    "rangeLabels":["low", "normal", "high"],
                    "ranges": {  
                        "result":[this.gen.lowerlimit,this.gen.upperlimit]
                    },
                })
            }
        }
        else {
            this.config.testRanges.map((rangeItem,i)=>{
                if (rangeItem.testName==this.testData.testName.split(' ').join('_')) {
                    this.config.testRanges.splice(i,1);
                }
            });
            if(this.testData.testName=="BLOOD PRESSURE"){
                this.config.testRanges.push({
                    "testName": this.testData.testName.split(' ').join('_') ,
                    "rangeLabels":["low", "normal", "high"],
                    "ranges": {
                        "systolic": [this.bpranges.systolicLower, this.bpranges.systolicUpper],
                        "diastolic": [this.bpranges.diastolicLower, this.bpranges.diastolicUpper]
                    },
                })
            }else if(this.testData.testName=="BLOOD GLUCOSE"){
                this.config.testRanges.push({
                    "testName": this.testData.testName.split(' ').join('_') ,
                    "rangeLabels": ["pre-diabetic", "normal", "diabetic"],
                    "ranges": {
                        "fasting":[this.bloodGlucoseRanges.fastingnormallower,this.bloodGlucoseRanges.fastingnormalupper,this.bloodGlucoseRanges.fastingDiabeticupper],
                        "post_prandial":[this.bloodGlucoseRanges.postprandialnormallower,this.bloodGlucoseRanges.postprandialnormalupper,this.bloodGlucoseRanges.postprandialDiabeticupper],
                        "random":[this.bloodGlucoseRanges.randomnormallower,this.bloodGlucoseRanges.randomnormalupper,this.bloodGlucoseRanges.randomDiabeticupper]
                    },
                })
            }
            else if(this.testData.testName=="URIC ACID" || this.testData.testName=="HEMOGLOBIN"){
                this.config.testRanges.push({
                    "testName": this.testData.testName.split(' ').join('_') ,
                    "rangeLabels":["low", "normal", "high"],
                    "ranges": {
                        "men":[this.gen.men.lowerlimit,this.gen.men.upperlimit],
                        "women":[this.gen.women.lowerlimit,this.gen.women.upperlimit],
                    },
                })
            }else{
                this.config.testRanges.push({
                    "testName": this.testData.testName.split(' ').join('_') ,
                    "rangeLabels":["low", "normal", "high"],
                    "ranges": {  
                        "result":[this.gen.lowerlimit,this.gen.upperlimit]
                    },
                })
            }
        }
       

        this.displayDialog=false;
        this.bpranges={
            diastolicUpper:'',
            systolicUpper:'',
            systolicLower:'',
            diastolicLower:''
        },
        this.gen={
            lowerlimit:'',
            upperlimit:'',
            men:{
                lowerlimit:'',
                upperlimit:''
            },
            women:{
                lowerlimit:'',
                upperlimit:''
            },
        }
        
    }  
    
    cancel(){
        
        this.displayDialog=false;
    }

    fieldLabels:any={
        STATE:'',
        CITY_OR_VILLAGE:''
    }

  pushToServer() {
      console.log(this.config)
    Helpers.setLoading(true);
    this.config.mandatoryFields=this.mandatoryArr;
    this.config.fieldLabels=this.fieldLabels;
    console.log(this.config)
    this._testsService.updateconfigfile(this.config).then(res => {     
        Helpers.setLoading(false);
        document.getElementById('step_five').style.backgroundColor = this.green;
        setTimeout(() => {
            this._router.navigate(['/skin/list']);         
        }, 500);
    });
}

facilityChange(e){
   this.config.facilityId = e.id;
   this.config.facilityName = e.name; 
   if(this.config.facilityId != ''){
        this.noCustomer = false;
        this.noUser = false;
   }      
   else
      this.noCustomer = true;
}

assignCustomer(){
    console.log(this.config)
    Helpers.setLoading(true);
    this._testsService.Assignconfigfile(this.config).then(res=>{
        console.log(res);
        Helpers.setLoading(false);
        this.showlang=false;
        this.showtests=true;
        document.getElementById('step_one').style.backgroundColor = this.green;     
        document.getElementById('step_two').style.backgroundColor = this.yellow;
    })
}

sysLower(value){
    console.log('bgg',value)
  this.selectedBpRanges.systolicLower = value;
  console.log(this.selectedBpRanges.systolicLower)
}

sysUpper(value){
    this.selectedBpRanges.systolicUpper = value;
}

}