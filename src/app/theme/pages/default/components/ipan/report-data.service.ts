import { Injectable } from "@angular/core";
import { Http, Headers, ResponseContentType } from "@angular/http";
import { environment } from "../../../../../../environments/environment";
import { Helpers } from "../../../../../helpers";
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";
import { Visit } from "./visit.model";

@Injectable()
export class ReportDataService {
    private headers = new Headers({
        "Content-Type": "application/json",
        Authorization:
            "Bearer " + JSON.parse(localStorage.getItem("currentUser")).token
    });
    private headerspdf = new Headers({
        "Content-Type": "application/json",
        "X-Content-Type-Options": "nosniff",
        Authorization:
            "Bearer " + JSON.parse(localStorage.getItem("currentUser")).token
    });
    private serviceUrl = environment.BaseURL + environment.IpanService; //'http: //dev.ezdx.healthcubed.com/ezdx-patient-command/api/v1/patients';  // URL to web api

    constructor(private http: Http, private _script: ScriptLoaderService) { }

    getAllTests(pageNo: number, limit: number) {
        //var url = this.serviceUrl + "/search?page=" + pageNo + "&limit=" + limit;
        var url =
            this.serviceUrl +
            "/search?searchCriteria=ALL&page=" +
            pageNo +
            "&limit=" +
            limit;
        return this.http
            .get(url, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError.bind(this));
    }

    reject(comment: string, visitId: String) {
        var url = this.serviceUrl + "/" + visitId + "/reject";
        return this.http
            .put(url, { comment: comment }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError.bind(this));
    }

    getPdfReport(patinetID: string) {
        let url = this.serviceUrl + "/" + patinetID + "/report";
        return this.http
            .get(url, {
                headers: this.headerspdf,
                responseType: ResponseContentType.Blob
            })
            .toPromise()
            .then(res => {
                // console.log("res : ", new Blob([res.arrayBuffer()]));
                return (res.status >= 200 && res.status <= 399)
                    ? new Blob([res.blob()], { type: 'application/pdf' })
                    : res.statusText;
            })
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load(
                "body",
                "assets/demo/default/custom/components/utils/redirect-cidaas-logout.js"
            );
        }
        return Promise.reject(error.message || error);
    }
}
