import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { Facility } from '../facility';
import { FacilitiesService } from '../facilities.service';
import { UsersService } from '../../users/users.service';
import { VisitsService } from '../../visits/visits.service';
import { DevicesService } from '../../devices/devices.service';
import { PatientsService } from '../../patients/patients.service';
import { Observable } from 'rxjs/Observable';
import { Center } from '../center';
import { MapLoaderService } from '../../utils/map-loader.service';
import { AmChartsService } from "amcharts3-angular2";
import { setTimeout } from 'timers';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./facilities-details.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class FacilitiesDetailsComponent implements OnInit, AfterViewInit {
    map: google.maps.Map;
    options: any = {
        center: { lat: 36.890257, lng: 30.707417 },
        zoom: 12,
    };
    overlays: any[] = [];
    facility = new Facility();
    center = new Center();
    facilityId: string;
    loading: boolean;
    users: any[];
    roles: any[];
    tests: any[];
    visits: any[];
    userBulkList: any[];
    patients: any[];
    devices: any[];
    centers: any[];
    devicesWithOrgName: any[];
    currentRole: any;
    hasTechnicianRole: boolean = false;
    pageNo: number;
    size: number;
    visitCount: number;
    patientCount: any;
    testsCount: any;
    totalTest: any;
    userCount: any;
    deviceCount: any;
    centerscount: any;
    pageNoHub: any;
    userRowCount: any;
    centerRowCount: any;
    hubRowCount: any;
    endDate: any;
    searchString: string;
    isSearched: boolean;
    selectedSearchBy: any;
    selectedBySort: any;
    SearchBylist: any;
    sortBylist: any;
    showCenter: boolean = false;
    testCountByWeek: any[] = [];
    patientCountByWeek: any[] = [];
    testList: any[] = [];
    selectedYear: any;
    years: any[];
    icon: any;
    noGeo: boolean = false;
    testChartValue: any[] = [];
    graphValues: any[] = [];
    patientChart: any[] = [];

    constructor(private _ngZone: NgZone, private _script: ScriptLoaderService, private _devicesService: DevicesService, private mapLoaderService: MapLoaderService,
        private _patientsService: PatientsService, private _visitsService: VisitsService, private _facilitiesService: FacilitiesService, private _usersService: UsersService, private _router: Router, private _activeroute: ActivatedRoute) {
        this.loading = false;
        this.SearchBylist = [
            { id: '0', name: 'Name', keytoSend: 'NAME' },
            { id: '1', name: 'Email', keytoSend: 'EMAIL' }  // Key to send should be match with service call
        ]
        this.sortBylist = [
            { id: '0', name: 'Ascending', keytoSend: 'asc' },
            { id: '1', name: 'Descending', keytoSend: 'desc' }
        ]
        this.selectedSearchBy = this.SearchBylist[0];
        this.selectedBySort = this.sortBylist[0];
        this.roles = [
            { name: 'HC Admin', type: 'HC_ADMIN' },
            { name: 'HC Operations', type: 'HC_OPERATIONS' },
            { name: 'HC Manager', type: 'HC_MANAGER' },
            { name: 'Operator', type: 'OPERATOR' },
            { name: 'Doctor', type: 'DOCTOR' },
            { name: 'Technician', type: 'TECHNICIAN' },
            { name: 'Clinical User', type: 'CLINICAL_USER' },
            { name: 'Pathologist', type: 'PATHOLOGIST' }
        ];
        this.years = [];
        this.years.push("ALL");
        let currentYear = new Date().getFullYear();
        for (var index = 2014; index <= currentYear; index++) {
            this.years.push(index);
        }
        this.selectedYear = this.years[this.years.length - 1];
        this.testList = [
            { type: "physical", name: "Physical", color: '#71bf44', tests: [{ name: "Blood Pressure", type: "BLOOD_PRESSURE" }, { name: "Pulse Oximeter", type: "PULSE_OXIMETER" }, { name: "%O2 Saturation", type: "%O2 Saturation" }, { name: "Temperature", type: "TEMPERATURE" }, { name: "Height", type: "Height" }, { name: "Weight", type: "Weight" }, { name: "BMI", type: "BMI" }, { name: "ECG", type: "ECG" }, { name: "Mid Arm Circumference", type: "Mid Arm Circumference" }] },
            { type: "whole_blood_poct", color: '#21C4DF', name: "Whole Blood-POCT", tests: [{ name: "Glucose", type: "BLOOD_GLUCOSE" }, { name: "Hemoglobin", type: "HEAMOGLOBIN" }, { name: "Cholesterol", type: "CHOLESTEROL" }, { name: "Uric Acid", type: "URIC_ACID" }] },
            { type: "whole_blood", name: "Whole Blood", color: '#02abcb', tests: [{ name: "Blood Grouping", type: "BLOOD_GROUPING" }] },
            { type: "whole_blood_rdt", name: "Whole Blood-RDT", color: '#465EEB', tests: [{ name: "Malaria", type: "Malaria" }, { name: "Dengue", type: "Dengue" }, { name: "Typhoid", type: "Typhoid" }, { name: "Hep-B", type: "Hep-B" }, { name: "Hep-C", type: "Hep-C" }, { name: "HIV", type: "HIV" }, { name: "Chikungunya", type: "Chikungunya" }, { name: "Syphilis", type: "Syphilis" }, { name: "Troponin-I", type: "Troponin-I" }] },
            { type: "urine_poct", name: "Urine-POCT", color: '#DA70D6', tests: [{ name: "Pregnancy", type: "Pregnancy" }] },
            { type: "urine_rdt", name: "Urine-RDT", color: '#B22222', tests: [{ name: "Urine Sugar", type: "Urine Sugar" }, { name: "Urine Protein", type: "Urine Protein" }, { name: "Leukocytes", type: "Leukocytes" }, { name: "Urobilinogen", type: "Urobilinogen" }, { name: "Nitrite", type: "Nitrite" }, { name: "pH", type: "pH" }, { name: "Ketone", type: "Ketone" }, { name: "Blood", type: "Blood" }, { name: "Bilurubin", type: "Bilurubin" }, { name: "Specific Gravity", type: "Specific Gravity" }] }
        ];
    }
    ngOnInit() {
        this.pageNo = 1;
        this.pageNoHub = 1;
        this.size = 10;
        this.userRowCount = 10;
        this.centerRowCount = 10;
        this.hubRowCount = 10;
        if (localStorage.getItem('userType')) {
            this.currentRole = localStorage.getItem('userType')
        }
        if (localStorage.getItem("userInfo")) {
            let userRoles: string[] = JSON.parse(localStorage.getItem("userInfo")).userRoles;
            let isTechnician = userRoles.filter(role => role == "TECHNICIAN");
            if (isTechnician[0] === "TECHNICIAN")
                this.hasTechnicianRole = true;
        }
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
        window.my.namespace.userUpdatePublicFunc = this.userUpdatePublicFunc.bind(this);
        window.my.namespace.userViewPublicFunc = this.userViewPublicFunc.bind(this);
        window.my.namespace.centerViewPublicFunc = this.centerViewPublicFunc.bind(this);
        this.facilityId = this._activeroute.snapshot.queryParams['facilityId'] || '/';
        document.getElementById('testchart').style.display = 'none'
        document.getElementById('patientchart').style.display = 'none'
    }

    setMap(event) {
        this.map = event.map;
    }

    ngOnDestroy() {
        window.my.namespace.publicFunc = null;
    }

    userViewPublicFunc(userId) {
        this._ngZone.run(() => this.goToUserView(userId));
    }

    userUpdatePublicFunc(userId) {
        this._ngZone.run(() => this.goToUserEdit(userId));
    }

    centerViewPublicFunc(centerId) {
        this._ngZone.run(() => this.goToCenterDetails(centerId));
    }

    goToCenterDetails(centerId) {
        this._router.navigate(['/center/details'], { queryParams: { center: centerId, facility: this.facilityId } });
    }

    goToUserView(userId) {
        this._router.navigate(['/users/details'], { queryParams: { userId: userId, facilityId: this.facilityId } });
    }

    goToUserEdit(userId) {
        this._router.navigate(['/users/update'], { queryParams: { userId: userId } });
    }

    visitRoute() {
        this._router.navigate(['/visits/list'], { queryParams: { from: 0, to: 0, facilityId: this.facility.id, centerId: 'ALL' } });
    }

    ngAfterViewInit() {
        MapLoaderService.load().then(() => {
        })
        if (this.facilityId != '/') {
            Helpers.setLoading(true);
            this.facility.id = this.facilityId;
            this._facilitiesService.getFacility(this.facility.id)
                .then(facility => this.facility = facility)
                .then(() => {
                    if (!this.facility.centers) {
                        this.facility.centers = [];
                        this.centerscount = '0';
                    }
                    else {
                        this.centers = this.facility.centers;
                        this.centerscount = this.centers.length;
                    }
                    Helpers.setLoading(false);
                    this.endDate = new Date();
                    this.endDate.setDate(this.endDate.getDate() + 1);
                    this.endDate.setHours(23);
                    this.endDate.setMinutes(23);
                    this.endDate.setSeconds(23);
                    this.endDate.setMilliseconds(0);
                    var searchcriteria = "FACILITY";
                    this.getPatientCountByWeek(0, new Date(this.endDate).getTime(), searchcriteria, this.facility.id, 0)
                    this.getTestCountByWeek(0, new Date(this.endDate).getTime(), searchcriteria, this.facility.id, 0)
                    this.getUsers(0, new Date(this.endDate).getTime(), searchcriteria, this.facility.id, this.pageNo, this.size);
                    this.getDevices(0, new Date(this.endDate).getTime(), searchcriteria, this.facility.id, this.pageNoHub, this.size, "ASSIGNED");
                    this.getPatientCount(0, new Date(this.endDate).getTime(), searchcriteria, this.facility.id, 0)
                    this.getNewTestCount(0, new Date(this.endDate).getTime(), searchcriteria, this.facility.id, 0)
                });
        }
    }

    getCenterLocation(centers) {
        this.overlays = [];
        let bounds = new google.maps.LatLngBounds();
        centers.forEach((center, i) => {
            if (center.geopoint) {
                var value = i + 1;
                this.icon = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + value + '|FE6256|000000';
                this.overlays.push(new google.maps.Marker({ position: { lat: center.geopoint.lat, lng: center.geopoint.lon }, map: this.map, icon: this.icon, title: center.name }));
            } else {
                this.noGeo = true;
                this.getLocation();
            }
        });

        if (this.overlays.length != 0) {
            this.overlays.forEach(marker => {
                bounds.extend(marker.getPosition());
                // console.log(bounds)
            });

            setTimeout(() => { // map will need some time to load               
                this.map.fitBounds(bounds); // Map object used directly
            }, 300);

            this.overlays.forEach(overlay => {
                overlay.setAnimation(google.maps.Animation.BOUNCE);
                setTimeout(() => {
                    overlay.setAnimation(5);
                }, 1000)
            })

            this.overlays.forEach((marker, i) => {
                var infoWindow = new google.maps.InfoWindow();
                let title = marker.title;
                var content = `<div id="index${i}">               
                <span>${title}</span>               
                </div>`
                infoWindow.setContent(content);
                infoWindow.open(this.map, marker)
            })
        }
    }

    getLocation() {
        this.overlays = [];
        if (navigator.geolocation) {
            // console.log('Getting location');
            navigator.geolocation.getCurrentPosition(position => {
                // console.log(position);
                this.options = {
                    center: { lng: position.coords.longitude, lat: position.coords.latitude },
                    zoom: 14
                };
                setTimeout(() => {
                    // this.overlays.push(new google.maps.Marker({ position: { lat: position.coords.latitude, lng: position.coords.longitude }, map: this.map, draggable: true, icon: this.icon }));
                    this.map.panTo(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
                }, 500)
            });
        };
    }

    getPatientCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        Helpers.setLoading(true);
        this.patientCount = null;
        this._patientsService.getPatientCenterCount(0, new Date(endDate).getTime(), searchcriteria, facilityid, centerid)
            .then(count => this.patientCount = count)
            .then(() => {
                this.patientCount = this.patientCount.toString();
                Helpers.setLoading(false);
            });
    }

    getNewTestCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        this.testsCount = null;
        Helpers.setLoading(true);
        this._visitsService.getTestCounts(0, new Date(endDate).getTime(), searchcriteria, facilityid, centerid)
            .then(data => {
                if (data != null) {
                    this.totalTest = data
                    this.testsCount = this.totalTest.totalHits.toString();
                }
                else
                    this.testsCount = 0;
                this.testsCount = this.testsCount.toString();
            })
            .then(() => {
                Helpers.setLoading(false);
            });
    }

    generateArray(obj) {
        return Object.keys(obj).map((key) => { return { key: key, value: obj[key] } });
    }

    getDevices(fromDate, toDate, searchcriteria, id, pageno, size, assign): void {
        Helpers.setLoading(true);
        this.devicesWithOrgName = [];
        this.devices = [];
        this._devicesService.getDevicesDatePaginated(fromDate, toDate, searchcriteria, id, pageno, size, assign)
            .then(devices => {
                if (devices.length != 0) {
                    this.deviceCount = devices.count
                    this.devices = devices.devices
                    this.deviceCount = this.deviceCount.toString();
                } else {
                    this.deviceCount = '0';
                    this.deviceCount = this.deviceCount.toString();
                }
            })
            .then(() => {
                Helpers.setLoading(false);
                this.devices.forEach(device => {
                    if (device.deviceType.includes('_'))
                        device.deviceType = device.deviceType.replace('_', ' ')
                    if (device.deviceType.includes('_'))
                        device.deviceType = device.deviceType.replace('_', ' ')
                })
            });
    }

    redirect() {
        this._router.navigate(['/facilities/update'], { queryParams: { facilityId: this.facility.organizationCode } });
    }

    getUsers(fromDate, toDate, searchcriteria, id, pageno, size): void {
        Helpers.setLoading(true);
        this._usersService.getUsersFiltered(fromDate, toDate, searchcriteria, id, pageno, size, '', 'asc')
            .then(users => {
                Helpers.setLoading(false);
                if (users != null) {
                    this.users = users.users;
                    this.userCount = users.count.toString();
                } else {
                    this.users = [];
                    this.userCount = '0';
                }


            })
            .then(() => {
                if (this.users.length != 0)
                    this.getUserDetail(this.users)
            })
    }

    getUserDetail(dataJSONArray) {
        this.users.forEach(user => {
            user.firstName = user.firstName.toLowerCase();
            user.lastName = user.lastName.toLowerCase();
            this.centers.forEach(center => {
                if (center.id == user.centerId) {
                    user['centerName'] = center.name;
                }
            })
            user.userRoles.forEach(role => {
                this.roles.forEach(element => {
                    if (role == element.type) {
                        var index = user.userRoles.indexOf(role)
                        user.userRoles[index] = element.name;
                        //user.userRoles.toString().replace(user.userRoles.toString(), element.name)   
                        //// console.log('role', user.userRoles[role])                              
                    }
                });
            });
        });

        var roleString = '';
        for (var j = 0; j < dataJSONArray.length; j++) {
            roleString = '';
            if (dataJSONArray[j].userRoles) {
                for (var i = 0; i < dataJSONArray[j].userRoles.length; i++) {
                    if (i != 0)
                        roleString += ', ' + dataJSONArray[j].userRoles[i];
                    else
                        roleString = dataJSONArray[j].userRoles[i];
                }
            }
            dataJSONArray[j].roleString = roleString;
        }
    }

    paginate(event) {
        // console.log('event', event)
        this.pageNo = event.page + 1;
        this.size = event.rows;
        var searchcriteria = "FACILITY";
        this.getUsers(0, new Date(this.endDate).getTime(), searchcriteria, this.facility.id, this.pageNo, this.size)
    }

    paginateUnDevices(event) {
        this.pageNoHub = event.page + 1;
        var searchcriteria = "FACILITY";
        this.getDevices(0, new Date(this.endDate).getTime(), searchcriteria, this.facility.id, this.pageNoHub, this.size, "ASSIGNED")
    }

    onChangeSeachBy(event) {
        this.filterUser(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), this.selectedSearchBy.keytoSend, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.size)
    }

    onChangeSortBy(event) {
        this.filterUser(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), this.selectedSearchBy.keytoSend, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.size)
    }

    searchUser(event) {
        this.searchString = event.target.value;
        if (this.searchString.length >= 3) {
            this.isSearched = true;
            this.filterUser(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), this.selectedSearchBy.keytoSend, this.selectedBySort.keytoSend, this.searchString, this.pageNo, this.size)
        }

    }

    getPatientCountByWeek(from, to, searchcriteria, facilityid, centerId): void {
        Helpers.setLoading(true);
        this.patientCountByWeek = [];
        this.patientChart = [];
        this._patientsService.getPatientCountByTril(searchcriteria, 0, new Date(to).getTime(), facilityid, centerId)
            .then(count => this.patientCountByWeek = count)
            .then(() => {
                // console.log(' pateint chart data', this.patientCountByWeek)
                var patientCountByWeekForChart = [];

                var tempData;
                var dummy = [];

                var names_array_new = this.patientCountByWeek.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.id.week === b.id.week) {
                            dummy.push(a);
                            return dummy;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.forEach(element => {
                    for (var i = 0; i < dummy.length; i++) {
                        if (element.id.week == dummy[i].id.week) {
                            element.count = dummy[i].count + element.count
                        }
                    }
                })
                // names_array_new.sort(function(a, b) {
                //     if (a.id.week > b.id.week) {
                //         return 1;
                //     } else if (a.id.week < b.id.week) {
                //         return -1;
                //     }
                //     return 0;
                // });

                var dummyArray = [];

                names_array_new = names_array_new.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.id.year > b.id.year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);

                // names_array_new.sort(function(a, b) {
                //     if (a.id.week > b.id.week) { return 1; } else if (a.id.week < b.id.week) { return -1; }
                //     return 0;
                // });

                var currentYear = new Date().getFullYear()

                names_array_new.forEach(element => {
                    if (element.id.year == currentYear) {
                        element.id.week = parseInt(element.id.week) + 1;
                    }
                });


                // dummyArray.sort(function(a, b) {
                //     if (a.id.week > b.id.week) { return 1; } else if (a.id.week < b.id.week) { return -1; }
                //     return 0;
                // });

                dummyArray.forEach(data => {
                    if (data.id.year == currentYear) {
                        data.id.week = parseInt(data.id.week) + 1
                    }
                })

                names_array_new = names_array_new.concat(dummyArray)

                this.patientCountByWeek = names_array_new;

                this.patientCountByWeek.forEach(data => {
                    patientCountByWeekForChart.push({
                        "Patients": data.count, "Week": data.id.week, "Year": data.id.year, "Week_Year": data.id.week + "\n"
                            + '(' + data.id.year + ')'
                    });
                });

                this.patientChart = patientCountByWeekForChart
                Helpers.setLoading(false);
            });
    }


    getTestCountByWeek(from, to, searchcriteria, facilityid, centerId): void {
        Helpers.setLoading(true);
        this.testCountByWeek = [];
        this.testChartValue = [];
        this._visitsService.getTestsCountByWeek(searchcriteria, 0, new Date(to).getTime(), facilityid, centerId)
            .then(count => this.testCountByWeek = count)
            .then(() => {
                var testCountByWeekForChart = [];
                this.testCountByWeek.forEach(data => {
                    var isWeekCreated = false;
                    testCountByWeekForChart.forEach(week => {
                        if (week.Week === data.week)
                            isWeekCreated = true;
                    });
                    if (!isWeekCreated) {
                        var objectToPush = {};
                        objectToPush['Week'] = data.week;
                        objectToPush[data.testType] = data.count;
                        objectToPush['Year'] = '20' + data.year;
                        objectToPush['Week_Year'] = data.week + "\n" + '(20' + data.year + ')';
                        testCountByWeekForChart.push(objectToPush);
                    }
                    else {
                        testCountByWeekForChart.forEach(week => {
                            if (week.Week === data.week) {
                                week[data.testType] = data.count;
                            }
                        });
                    }
                });

                testCountByWeekForChart.forEach(testWeek => {
                    testWeek.Week = Number(testWeek.Week)
                })

                var dummyArray = [];

                var names_array_new = testCountByWeekForChart.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.Year > b.Year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                dummyArray.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                names_array_new = names_array_new.concat(dummyArray)

                testCountByWeekForChart = names_array_new;
                this.testChartValue = testCountByWeekForChart;

                this.graphValues = [];
                this.testList.forEach(test => {
                    this.graphValues.push({
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>week [[category]]: <b>[[value]] diagnostics done</b></span>",
                        "fillAlphas": 1.8,
                        "labelText": "[[value]]",
                        "lineAlpha": 1.0,
                        "title": test.name,
                        "type": "column",
                        "color": "#000000",
                        "showHandOnHover": false,
                        "valueField": test.type.toLowerCase(),
                        "colorField": test.color,
                        "fillColors": test.color,
                        "lineColor": test.color
                    });
                });
                // window.localStorage.setItem("graphValuesTestCountByWeek", JSON.stringify(graphValues));
                // window.localStorage.setItem("testCountByWeek", JSON.stringify(testCountByWeekForChart));
                // window.localStorage.setItem("selectedYear", this.selectedYear.toString());
                // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                //     'assets/demo/default/custom/components/charts/amcharts/charts.testbyweek.4.js');
                Helpers.setLoading(false);

            });
    }

    filterUser(from, to, searchCriteria, sortby, searchstring, page, size) {
        Helpers.setLoading(true);
        this._usersService.getUsersFiltered(from, to, searchCriteria, '', page, size, searchstring, sortby)
            .then(
            response => {
                Helpers.setLoading(false);
                this.users = response.users;
                this.userCount = response.count.toString();
            }
            )
            .then(() => {
                var roleString = '';
                for (var j = 0; j < this.users.length; j++) {
                    roleString = '';
                    if (this.users[j].userRoles) {
                        for (var i = 0; i < this.users[j].userRoles.length; i++) {
                            if (i != 0)
                                roleString += ', ' + this.users[j].userRoles[i];
                            else
                                roleString = this.users[j].userRoles[i];
                        }
                    }
                    this.users[j].roleString = roleString;
                }
            });
    }

    tableSelectClick(type) {
        document.getElementById('main_ui').style.display = 'block'
        document.getElementById('testchart').style.display = 'none'
        document.getElementById('patientchart').style.display = 'none'
        if (type == "user") {
            document.getElementById('m_tabs_6_3').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_4').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_1').classList['value'] = "tab-pane active show"
            document.getElementById('m_tabs_6_5').classList['value'] = "tab-pane"
            //link li
            document.getElementById('user_link').classList['value'] = "nav-link m-tabs__link active show"
            document.getElementById('hub_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('center_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('map_link').classList['value'] = "nav-link m-tabs__link"
        }
        if (type == "center") {
            document.getElementById('m_tabs_6_1').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_4').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_3').classList['value'] = "tab-pane active show"
            document.getElementById('m_tabs_6_5').classList['value'] = "tab-pane"

            document.getElementById('center_link').classList['value'] = "nav-link m-tabs__link active show"
            document.getElementById('hub_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('user_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('map_link').classList['value'] = "nav-link m-tabs__link"
        }
        if (type == "hub") {
            document.getElementById('m_tabs_6_3').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_4').classList['value'] = "tab-pane active show"
            document.getElementById('m_tabs_6_1').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_5').classList['value'] = "tab-pane"

            document.getElementById('hub_link').classList['value'] = "nav-link m-tabs__link active show"
            document.getElementById('center_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('user_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('map_link').classList['value'] = "nav-link m-tabs__link"

        }
        if (type == "map") {
            document.getElementById('m_tabs_6_3').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_4').classList['value'] = "tab-pane"
            document.getElementById('m_tabs_6_5').classList['value'] = "tab-pane active show"
            document.getElementById('m_tabs_6_1').classList['value'] = "tab-pane"
            this.getCenterLocation(this.centers)

            document.getElementById('hub_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('center_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('user_link').classList['value'] = "nav-link m-tabs__link"
            document.getElementById('map_link').classList['value'] = "nav-link m-tabs__link active show"

        }
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });

    }

    patientClick(type) {
        if (type == 'view') {
            document.getElementById('main_ui').style.display = 'none'
            document.getElementById('patientchart').style.display = 'block'
            document.getElementById('testchart').style.display = 'none'
        }
        if (type == 'back') {
            document.getElementById('main_ui').style.display = 'block'
            document.getElementById('patientchart').style.display = 'none'
            document.getElementById('testchart').style.display = 'none'
        }

    }

    diagnosticClick(type) {
        if (type == 'view') {
            document.getElementById('main_ui').style.display = 'none'
            document.getElementById('testchart').style.display = 'block'
            document.getElementById('patientchart').style.display = 'none'
        }
        if (type == 'back') {
            document.getElementById('main_ui').style.display = 'block'
            document.getElementById('testchart').style.display = 'none'
            document.getElementById('patientchart').style.display = 'none'
        }
    }

    formateDate(d) {
        var m_names = new Array("Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec");

        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();
        return m_names[curr_month] + " " + curr_date + " " + curr_year;
    }

    getDateRangeOfWeek(w, y) {
        var week = w.split("\n")
        var simple = new Date(y, 0, 1 + (week[0] - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay());
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        var ISOweekEnd = new Date(ISOweekStart);
        ISOweekEnd.setDate(ISOweekEnd.getDate() + 6);
        ISOweekEnd.setHours(23);
        ISOweekEnd.setMinutes(23);
        ISOweekEnd.setSeconds(23);
        ISOweekEnd.setMilliseconds(0);
        ISOweekEnd.setFullYear(y);
        return this.formateDate(ISOweekStart) + " to " + this.formateDate(ISOweekEnd);
    };




}