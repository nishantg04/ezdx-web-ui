import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { PatientsService } from "../patients.service";
import * as _ from 'lodash';
import { element } from 'protractor';
import { Response } from '@angular/http/src/static_response';
import { Console } from '@angular/core/src/console';
import { VisitsService } from "../../visits/visits.service";
import { TranslateService } from '@ngx-translate/core';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./patient-detail.component.html",
    styleUrls: ["./patient-detail.component.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class PatientDetailComponent implements OnInit {

    visits: any[] = [];
    id: any;
    user: any;
    country_code: any;
    testCount: any;
    tests: any[] = [];

    constructor(private usersService: VisitsService, private patientsService: PatientsService,private translate: TranslateService, private activatedRoute: ActivatedRoute) {
        // this.getCountries();
    }


    ngOnInit() {
        this.id = this.activatedRoute.snapshot.queryParams['patientId'] || '/';
        if (this.id != '/') {
            this.getPatient();
        }
    }

    getPatientVisit() {
        this.visits = [];
        Helpers.setLoading(true);
        this.usersService.getVisitByPatientId(this.id, 1, 0)
            .then(data => {
                Helpers.setLoading(false);
                if (data) {
                    this.visits = data.visits;
                } else {
                    this.visits = [];
                    this.testCount = 0;
                }

            })
            .then(() => {
                var dia_result = '';
                var duration: any;
                this.visits.sort(function(a, b) {
                    if (a.createTime > b.createTime) {
                        return -1;
                    } else if (a.createTime < b.createTime) {
                        return 1;
                    }
                    return 0;
                });

                if (this.visits.length != 0) {
                    this.visits.forEach((visit, i) => {

                        var tests = visit.tests
                        tests.forEach((test, index) => {

                            var dia_result = '';
                            var duration: any;

                            test.testIconUrl = "./assets/app/media/img/test/" + test.name + ".png"


                            if (test.type == "WHOLE_BLOOD_RDT") {
                                if (test.mismatched) {
                                    if (test.machineResultValid) {
                                        dia_result = test.rdtResult
                                    } else {
                                        dia_result = test.userRdtResult
                                    }
                                } else {
                                    dia_result = test.rdtResult
                                }
                                test.diaResults = dia_result;
                            }

                            if (test.type == "URINE_POCT") {
                                if (test.mismatched) {
                                    if (test.machineResultValid) {
                                        dia_result = test.rdtResult
                                    } else {
                                        dia_result = test.userRdtResult
                                    }
                                } else {
                                    dia_result = test.rdtResult
                                }
                                test.diaResults = dia_result;
                            }

                            if (test.name == "TEMPERATURE") { dia_result = test.result + ' ' + (test.unit ? test.unit : '') }
                            else if (test.name == "BMI") { dia_result = test.bmiValue }
                            else if (test.name == "PULSE_OXIMETER") { 
                               // dia_result = 'SpO2: ' + test.spoValue + ' ' + test.spoUnit + '<br/>Pulse: ' + test.pulseRate + ' ' + test.pulseRateUnit 
                               var tempArray = [];
                               tempArray.push('PULSE_RATE',test.pulseRateUnit)
                               this.translate.stream(tempArray).subscribe((val) => {                        
                                    dia_result = 'SpO2 : ' + test.spoValue + ' ' + test.spoUnit + '<br/>' + val.PULSE_RATE + ' : ' + test.pulseRate + ' '+  val[test.pulseRateUnit];
                               })    
                               this.translate.onLangChange.subscribe((event) => {                
                                   dia_result = 'SpO2 : ' + test.spoValue + ' ' + test.spoUnit + '<br/>' +  event.translations.PULSE_RATE + ' : ' + test.pulseRate + ' '+   event.translations[test.pulseRateUnit];               
                                   test.diaResults = dia_result;             
                               }); 
                            }
                            else if (test.name == "BLOOD_PRESSURE") {
                              //  dia_result = 'DIA: ' + test.dia + ' ' + test.unit + '<br/>SYS: ' + test.sys + ' ' + test.unit;
                              var tempArray = [];
            tempArray.push('DIA','SYS',test.unit)
            this.translate.stream(tempArray).subscribe((val) => {                        
                 dia_result = val.DIA + ' : ' + test.dia + ' ' + val[test.unit] + '<br/>'+ val.SYS + ' ' + test.sys + ' ' + val[test.unit];               
            })    
            this.translate.onLangChange.subscribe((event) => {                        
                dia_result  =  event.translations.DIA + test.dia+ ' ' + event.translations[test.unit] + '<br/>'+ event.translations.SYS+ ' ' + test.sys + ' ' + event.translations[test.unit];                
                test.diaResults = dia_result;             
            }); 
            if(test.heartRate && test.heartRate != 0){
               // dia_result = dia_result + '<br/>PULSE RATE:' + test.heartRate;
                var tempArrays = [];
                tempArrays.push('PULSE_RATE')
                var heartLable = '';
                this.translate.stream(tempArrays).subscribe((val) => {                        
                    heartLable = val.PULSE_RATE + ':' + test.heartRate;
                    dia_result = dia_result + '<br/>' + heartLable
                })    
                this.translate.onLangChange.subscribe((event) => {                 
                    test.diaResults = dia_result;               
                });             
                 
            }
                } else if (test.name == "DENGUE") {
                    test.name = "DENGUE ANTIGEN"
                }
                else if (test.name == "BLOOD_GROUPING") {
                    // test.name = 'BLOOD GROUPING'
                    dia_result = test.bloodGroupResult;
                }
                else if (test.name == "ECG") {
                    dia_result = ' ';
                }
                else if (test.name == "URINE") {
                    test.name = "URINE 2P"
                    //dia_result = 'Protein : ' + test.rdtResultProtein + '<br/>' + 'Glucose : ' + test.rdtResultGlucose;
                    if (test.mismatched) {
                        if (test.machineGlucoseResultValid) {
                            var glucose = test.rdtResultGlucose
                        } else {
                            glucose = test.userRdtResultGlucose
                        }
                        if (test.machineProteinResultValid) {
                            var protein = test.rdtResultProtein
                        } else {
                            protein = test.userRdtResultProtein;
                        }
                        if (protein == "Plus1") protein = "+"
                        if (protein == "Plus3") protein = "+++"
                        if (protein == "Plus2") protein = "++"
                        if (protein == "Plus4") protein = "++++"
                        if (test.userRdtResultGlucose == "Plus1") test.userRdtResultGlucose = "+"
                        if (test.userRdtResultGlucose == "Plus3") test.userRdtResultGlucose = "+++"
                        if (test.userRdtResultGlucose == "Plus2") test.userRdtResultGlucose = "++"
                        if (test.userRdtResultGlucose == "Plus4") test.userRdtResultGlucose = "++++"
                       // dia_result = 'Protein : ' + protein + '<br/>Glucose : ' + test.userRdtResultGlucose;
                       var tempArray = [];
                       tempArray.push('Protein', 'Glucose', protein,test.userRdtResultGlucose)
                       this.translate.stream(tempArray).subscribe((val) => {       
                                
                            dia_result = val.Protein + ' : ' + val[protein] + '<br/>' 
                                        + val.Glucose + ' : ' +  val[test.userRdtResultGlucose];           
                       })    
                       this.translate.onLangChange.subscribe((event) => {                        
                           dia_result  =  event.translations.Protein + ' : ' + event.translations[protein] + '<br/>'  
                                          + event.translations.Glucose + ' : ' +  event.translations[test.userRdtResultGlucose];         
                           test.diaResults = dia_result;             
                       }); 
                    } else {
                        if (protein == "Plus1") protein = "+"
                        if (protein == "Plus3") protein = "+++"
                        if (protein == "Plus2") protein = "++"
                        if (protein == "Plus4") protein = "++++"
                        if (test.rdtResultGlucose == "Plus1") test.rdtResultGlucose = "+"
                        if (test.rdtResultGlucose == "Plus3") test.rdtResultGlucose = "+++"
                        if (test.rdtResultGlucose == "Plus2") test.rdtResultGlucose = "++"
                        if (test.rdtResultGlucose == "Plus4") test.rdtResultGlucose = "++++"
                       // dia_result = 'Protein : ' + this.valPlusConver(test.rdtResultProtein) + '<br/>Glucose : ' + test.rdtResultGlucose;
                       var valProtien = this.valPlusConver(test.rdtResultProtein)
                       var tempArray = [];
                       tempArray.push('Protein', 'Glucose',valProtien,test.rdtResultGlucose)
                       this.translate.stream(tempArray).subscribe((val) => {          
                            var typeOf = test.dietType       
                            dia_result = val.Protein + ' : ' + val[valProtien] + '<br/>' 
                                        + val.Glucose + ' : ' +  val[test.rdtResultGlucose];           
                       })    
                       this.translate.onLangChange.subscribe((event) => {                        
                           dia_result  =  event.translations.Protein +  ' : ' +event.translations[valProtien] + '<br/>'  
                                          + event.translations.Glucose + ' : ' + event.translations[test.rdtResultGlucose];         
                           test.diaResults = dia_result;             
                       });
                    }
                    test.diaResults = dia_result;
                }
                else if (test.name === "HEMOGLOBIN") {
                   // dia_result = 'Count: ' + test.result + ' ' + test.unit;
                   var tempArray = [];
                   tempArray.push('COUNT')
                   this.translate.stream(tempArray).subscribe((val) => {     
                               
                        dia_result = val.COUNT + ' : ' + test.result + ' ' + test.unit;               
                   })    
                   this.translate.onLangChange.subscribe((event) => {                        
                       dia_result  =  event.translations.COUNT + test.result+ ' ' + test.unit               
                       test.diaResults = dia_result;             
                   }); 
                } else if (test.name === "BLOOD_GLUCOSE") {
                   // dia_result = 'Type: ' + test.dietType + '<br/> Count: ' + test.result + ' ' + test.unit;
                   var tempArray = [];
                   tempArray.push('TYPE',test.dietType,'COUNT')
                   this.translate.stream(tempArray).subscribe((val) => {          
                         
                        dia_result = val.TYPE + ' : ' + val[test.dietType] + '<br/>' + val.COUNT + ' : ' + test.result + ' ' + test.unit;               
                   })    
                   this.translate.onLangChange.subscribe((event) => {                          
                       dia_result  = event.translations.TYPE + ' : ' + event.translations[test.dietType] + '<br/>'  + event.translations.COUNT + ' : ' + test.result+ ' ' + test.unit               
                       test.diaResults = dia_result;             
                   }); 
                }
                else { dia_result = test.result + ' ' + test.unit }
                if (test.type != "WHOLE_BLOOD_RDT" && test.type != "URINE_POCT" && test.name != "URINE") {
                    test.diaResults = dia_result;
                }
                this.tests.push(test);
                if (this.tests.length != 0) {
                    this.testCount = this.tests.length;
                    this.remove()
                } else {
                    this.testCount = 0;
                }
                        });

                    })
                }

            })


    }

    

    valPlusConver(protein) {
        if (protein == "Plus1") protein = "+"
        if (protein == "Plus3") protein = "+++"
        if (protein == "Plus2") protein = "++"
        if (protein == "Plus4") protein = "++++"
        return protein;
    }

    remove() {
        this.tests.forEach(test => {
            if (test.name.includes('_')) {
                test.name = test.name.replace('_', ' ')
            }
        })
        setTimeout(() => {
            this.expand(0)
        }, 500)
    }

    getPatient() {
        this.patientsService.getPatientById(this.id)
            .then(user => this.user = user)
            .then(() => {
                this.getPatientVisit();
            });
    }


    expand(id) {
        //console.log('id', id)
        document.getElementById('visit' + id).style.display = 'block'
        document.getElementById('exicon' + id).style.display = 'none'
        document.getElementById('compicon' + id).style.display = 'block'
    }

    compress(id) {
        document.getElementById('visit' + id).style.display = 'none'
        document.getElementById('exicon' + id).style.display = 'block'
        document.getElementById('compicon' + id).style.display = 'none'

    }

}
