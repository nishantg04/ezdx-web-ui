import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ConsumablesCenterComponent } from './consumables-center.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { ConsumablesService } from '../consumables.service';
import { OrderModule } from 'ngx-order-pipe';
import { FacilitiesService } from "../../facilities/facilities.service";
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { DataTableModule, SharedModule, PaginatorModule } from 'primeng/primeng';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ConsumablesCenterComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule,
        TranslateModule.forChild({ isolate: false }), DataTableModule, SharedModule, PaginatorModule
    ], exports: [
        RouterModule
    ], declarations: [
        ConsumablesCenterComponent
    ],
    providers: [
        ConsumablesService,
        FacilitiesService,
        TranslateService
    ]

})
export class ConsumablesCenterModule {



}