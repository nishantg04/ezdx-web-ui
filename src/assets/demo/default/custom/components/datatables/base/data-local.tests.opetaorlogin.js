//== Class definition

var DatatableDataLocalDemo = function() {
    //== Private functions

    // demo initializer
    var demo = function() {

        var dataJSONArray = JSON.parse(localStorage.getItem('diagnosisTable'));
        //Refresh table by remove html tag
        $(".m_datatable").remove();
        var $newdiv1 = $("<div class='m_datatable'></div>");
        $(".m-portlet__body").append($newdiv1);

        var datatable = $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: dataJSONArray,
                pageSize: 10,
                saveState: {
                    cookie: false,
                    webstorage: false
                }, // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
            },

            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 450, // datatable's body's fixed height
                footer: false // display/hide footer
            },

            // column sorting(refer to Kendo UI)
            sortable: true,

            // column based filtering(refer to Kendo UI)
            filterable: false,

            pagination: true,

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition

            columns: [{
                    field: "",
                    title: "Sr no",
                    sortable: false,
                    width: 40,
                    template: function(row) {
                        return '\
						<span>\
                            ' + (((row.getDatatable().getCurrentPage() - 1) * 10) + (row.getIndex() + 1)) + '\
                        </span>\
					';
                    }
                },

                {
                    field: "endTimeDate",
                    title: "Time",
                    width: 150,
                    template: function(row) {
                        var partsOfStr = row.endTime.split('(');
                        partsOfStr[1] = partsOfStr[1].slice(0, -1);
                        return '\
						<div>\
                            <i class="fa fa-calendar"></i>  ' + partsOfStr[0] + '\
                        </div>\
						<div>\
							<i class="fa fa-clock-o"></i> ' + partsOfStr[1] + '\
                        </div>\
					';
                    }
                },
                {
                    title: "Duration",
                    width: 100,
                    template: function(row) {
                        return '\
						<span>' + row.duration + '\</span>\
					';
                    }
                }, {
                    field: "type",
                    title: "Type & Result",
                    width: 180,
                    template: function(row) {
                        return '\
						<span style="color: #656565; font-weight: 500">\
							' + row.type + '\
						</span><br/>\
						<span>\
                            ' + row.diaResults + '\
                        </span>\
					';
                    }
                },
                {
                    field: "patientId",
                    title: "PatientID and MRN",
                    width: 280,
                    template: function(row) {
                        return '\
						<div>\
                            <i class="fa fa-user-o" style="color: #656565; font-weight: 500"></i>  ' + row.patientId + '\
                        </div>\
						<div>\
							<i class="fa fa-list-ol" style="color: #656565; font-size: 14px"></i> ' + row.mrn + '\
                        </div>\
					';
                    }
                },
                {
                    field: "centerName",
                    title: "Centers",
                    template: function(row) {
                        return '\
					<span>' + '</span>\
					<span>' + row.centerName + '</span>\
					'
                    }
                },
                {
                    field: "serialNumber",
                    title: "Serial Number, IMEI, Operator",
                    template: function(row) {
                        return '\
						<span>\
                            ' + row.serialNumber + '\
						</span><br/>\
						<span>\
						' + row.imei + '\
					</span><br/>\
					<span>\
					' + row.operatorName + '\
				</span>\
					';
                    }
                }
            ]
        });

        var query = datatable.getDataSourceQuery();

        $('#m_form_search').on('keyup', function(e) {
            datatable.search($(this).val().toLowerCase());
        }).val(query.generalSearch);
        datatable.sort('endTimeDate');
        setTimeout(function() {
            datatable.sort('endTimeDate');
        }, 500);

        // $('#m_form_status').on('change', function () {
        // 	datatable.search($(this).val(), 'Status');
        // }).val(typeof query.Status !== 'undefined' ? query.Status : '');

        // $('#m_form_type').on('change', function () {
        // 	datatable.search($(this).val(), 'Type');
        // }).val(typeof query.Type !== 'undefined' ? query.Type : '');

        // $('#m_form_status, #m_form_type').selectpicker();

    };

    return {
        //== Public functions
        init: function() {
            // init dmeo
            demo();
        }
    };
}();

jQuery(document).ready(function() {
    DatatableDataLocalDemo.init();
});