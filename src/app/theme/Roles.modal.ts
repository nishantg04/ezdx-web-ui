export const ALL = "ALL";

/* HC ROLES */
export const HC_ADMIN = "HC_ADMIN";
export const HC_MANAGER = "HC_MANAGER";
export const HC_OPERATION = "HC_OPERATIONS";

/* CUSTOMER ROLES */
export const OPERATOR = "OPERATOR";
export const DOCTOR = "DOCTOR";
export const TECHNICIAN = "TECHNICIAN";
export const CENTER_ADMIN = "CENTER_ADMIN"

/* IPAN PATHOLOGIST */

export const PATHOLOGIST = "PATHOLOGIST"
