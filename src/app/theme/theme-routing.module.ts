import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from "../auth/_guards/auth.guard";
import { RouteGuard } from '../auth/router-guards/router.guards';

import * as ROLE from './Roles.modal';
import { Role } from './pages/default/components/users/role';

const routes: Routes = [
    {
        "path": "",
        "component": ThemeComponent,
        "canActivate": [AuthGuard],
        "children": [
            {
                "path": "index",
                "loadChildren": ".\/pages\/default\/components\/home\/dashboard\/dashboard.module#DashboardModule"
            },
            {
                "path": "report\/tests",
                "loadChildren": ".\/pages\/default\/components\/home\/tests-report\/tests-report.module#TestsReportModule"
            },
            {
                "path": "privacy",
                "loadChildren": ".\/pages\/default\/components\/home\/policy\/policy.module#PolicyModule"
            },
            {
                "path": "facilities\/add",
                "loadChildren": ".\/pages\/default\/components\/facilities\/facilities-add\/facilities-add.module#FacilitiesAddModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "filters\/all",
                "loadChildren": ".\/pages\/default\/components\/filters\/filters.module#FiltersModule"
            },
            {
                "path": "facilities\/update",
                "loadChildren": ".\/pages\/default\/components\/facilities\/facilities-add\/facilities-add.module#FacilitiesAddModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.TECHNICIAN] }
            },
            {
                "path": "facilities\/details",
                "loadChildren": ".\/pages\/default\/components\/facilities\/facilities-details\/facilities-details.module#FacilitiesDetailsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.HC_MANAGER, ROLE.TECHNICIAN] }
            },
            {
                "path": "facilities\/list",
                "loadChildren": ".\/pages\/default\/components\/facilities\/facilities-list\/facilities-list.module#FacilitiesListModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_MANAGER, ROLE.HC_OPERATION] }
            },
            {
                "path": "consumables\/add",
                "loadChildren": ".\/pages\/default\/components\/consumables\/consumables-add\/consumables-add.module#ConsumablesAddModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN] }
            },
            {
                "path": "consumables\/update",
                "loadChildren": ".\/pages\/default\/components\/consumables\/consumables-add\/consumables-add.module#ConsumablesAddModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN] }
            },
            {
                "path": "center\/details",
                "loadChildren": ".\/pages\/default\/components\/centers\/center-details\/center-details.module#CenterDetailsModule"
            },
            {
                "path": "consumables\/list",
                "loadChildren": ".\/pages\/default\/components\/consumables\/consumables-list\/consumables-list.module#ConsumablesListModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN] }
            },
            {
                "path": "consumables\/center",
                "loadChildren": ".\/pages\/default\/components\/consumables\/consumables-center\/consumables-center.module#ConsumablesCenterModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.TECHNICIAN] }
            },
            {
                "path": "consumables\/reorder",
                "loadChildren": ".\/pages\/default\/components\/consumables\/consumables-reorder\/consumables-reorder.module#ConsumablesReorderModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.TECHNICIAN] }
            },
            {
                "path": "consumables\/reorder\/add",
                "loadChildren": ".\/pages\/default\/components\/consumables\/consumables-reorder-add\/consumables-reorder-add.module#ConsumablesReorderAddModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.TECHNICIAN] }
            },
            {
                "path": "consumables\/shipment",
                "loadChildren": ".\/pages\/default\/components\/consumables\/consumables-shipment\/consumables-shipment.module#ConsumablesShipmentModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.TECHNICIAN] }
            },
            {
                "path": "consumables\/map",
                "loadChildren": ".\/pages\/default\/components\/consumables\/consumables-map\/consumables-map.module#ConsumablesMapModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN] }
            },
            {
                "path": "consumables\/manage",
                "loadChildren": ".\/pages\/default\/components\/consumables\/consumables-manage\/consumables-manage.module#ConsumablesManageModule"
            },
            {
                "path": "users\/add",
                "loadChildren": ".\/pages\/default\/components\/users\/users-add\/users-add.module#UsersAddModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.TECHNICIAN] }
            },
            {
                "path": "users\/update",
                "loadChildren": ".\/pages\/default\/components\/users\/users-add\/users-add.module#UsersAddModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.TECHNICIAN,ROLE.DOCTOR] }
            },
            {
                "path": "users\/edit",
                "loadChildren": ".\/pages\/default\/components\/users\/user-edit\/user-edit.module#UserEditModule"
            },
            {
                "path": "users\/details",
                "loadChildren": ".\/pages\/default\/components\/users\/users-details\/users-details.module#UsersDetailsModule"
            },
            {
                "path": "users\/list",
                "loadChildren": ".\/pages\/default\/components\/users\/users-list\/users-list.module#UsersListModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.CENTER_ADMIN, ROLE.DOCTOR, ROLE.TECHNICIAN] }
            },
            {
                "path": "users\/tests",
                "loadChildren": ".\/pages\/default\/components\/users\/users-tests\/users-tests.module#UsersTestsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "devices\/add",
                "loadChildren": ".\/pages\/default\/components\/devices\/devices-add1\/devices-add1.module#DevicesAdd1Module",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "devices\/assign",
                "loadChildren": ".\/pages\/default\/components\/devices\/devices-add1\/devices-add1.module#DevicesAdd1Module",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.TECHNICIAN] }

            },
            {
                "path": "devices\/update",
                "loadChildren": ".\/pages\/default\/components\/devices\/devices-add\/devices-add.module#DevicesAddModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.TECHNICIAN] }
            },
            {
                "path": "devices\/details",
                "loadChildren": ".\/pages\/default\/components\/devices\/device-details\/device-details.module#DeviceDetailsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.CENTER_ADMIN, ROLE.TECHNICIAN] }
            },
            {
                "path": "devices\/list",
                "loadChildren": ".\/pages\/default\/components\/devices\/devices-list\/devices-list.module#DevicesListModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.TECHNICIAN, ROLE.CENTER_ADMIN] }
            },
            /* RDT related */
            {
                "path": "rdt\/add",
                "loadChildren": ".\/pages\/default\/components\/rdt-lookup\/rdt-add\/rdt-add.module#RdtAddModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "rdt\/assign",
                "loadChildren": ".\/pages\/default\/components\/rdt-lookup\/rdt-assign\/rdt-assign.module#RdtAssignModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "rdt\/update",
                "loadChildren": ".\/pages\/default\/components\/rdt-lookup\/rdt-edit\/rdt-edit.module#RdtEditModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "rdt\/list",
                "loadChildren": ".\/pages\/default\/components\/rdt-lookup\/rdt-list\/rdt-list.module#RdtListModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "rdt\/details",
                "loadChildren": ".\/pages\/default\/components\/rdt-lookup\/rdt-details\/rdt-details.module#RdtDetailsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "urine\/add",
                "loadChildren": ".\/pages\/default\/components\/urine-lookup\/urine-lookup-add\/urine-lookup-add.module#UrineLookupAddModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "patients\/list",
                "loadChildren": ".\/pages\/default\/components\/patients\/patients-list\/patients-list.module#PatientsListModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.OPERATOR, ROLE.TECHNICIAN, ROLE.CENTER_ADMIN] }
            },
            {
                "path": "patients\/stats",
                "loadChildren": ".\/pages\/default\/components\/patients\/patients-stats\/patients-stats.module#PatientsStatsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.DOCTOR, ROLE.TECHNICIAN] }
            },
            {
                "path": "patients\/statistics",
                "loadChildren": ".\/pages\/default\/components\/patients\/patients-statistics\/patients-statistics.module#PatientsStatisticsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.CENTER_ADMIN, ROLE.HC_OPERATION, ROLE.DOCTOR, ROLE.TECHNICIAN,ROLE.OPERATOR] }
            },
            {
                "path": "patients\/chart",
                "loadChildren": ".\/pages\/default\/components\/patients\/patients-chart\/patients-chart.module#PatientsChartModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.CENTER_ADMIN, ROLE.DOCTOR, ROLE.TECHNICIAN,ROLE.OPERATOR] }
            },
            {
                "path": "patients\/log",
                "loadChildren": ".\/pages\/default\/components\/patients\/patient-detail\/patient-detail.module#PatientDetailModule"
            },
            {
                "path": "visits\/list",
                "loadChildren": ".\/pages\/default\/components\/visits\/visits-list\/visits-list.module#VisitsListModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.OPERATOR, ROLE.DOCTOR, ROLE.CENTER_ADMIN, ROLE.TECHNICIAN] }
            },
            {
                "path": "visits\/stats",
                "loadChildren": ".\/pages\/default\/components\/visits\/visits-stats\/visits-stats.module#VisitsStatsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.DOCTOR, ROLE.TECHNICIAN] }
            },
            {
                "path": "visits\/map",
                "loadChildren": ".\/pages\/default\/components\/visits\/visits-map\/visits-map.module#VisitsMapModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "upload\/python",
                "loadChildren": ".\/pages\/default\/components\/upload\/upload-file.module#UploadFileModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "upload\/firmware",
                "loadChildren": ".\/pages\/default\/components\/upload\/upload-file.module#UploadFileModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "file\/details",
                "loadChildren": ".\/pages\/default\/components\/upload\/file-details\/file-details.module#FileDetailsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "tests\/update",
                "loadChildren": ".\/pages\/default\/components\/master-test\/master-test-edit\/master-test-edit.module#MasterTestEditModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "visit\/logdetail",
                "loadChildren": ".\/pages\/default\/components\/patients\/report-detail\/report-detail.module#ReportDetailModule"
            },
            {
                "path": "ipan\/reports",
                "loadChildren": ".\/pages\/default\/components\/ipan\/ipan-reports\/ipan-reports.module#IpanReportsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION, ROLE.PATHOLOGIST] }
            },
            {
                "path": "analytics\/charts",
                "loadChildren": ".\/pages\/default\/components\/analytics\/analytics-chart\/analytics-chart.module#AnalyticsChartModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION,ROLE.TECHNICIAN] }
            },
            {
                "path": "analytics\/usercharts",
                "loadChildren": ".\/pages\/default\/components\/analytics\/userwise-stats\/userwise-stats.module#UserwiseStatsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION,ROLE.TECHNICIAN,ROLE.CENTER_ADMIN] }
            },
            {
                "path": "analytics\/visitlog",
                "loadChildren": ".\/pages\/default\/components\/analytics\/visitlog\/visitlog.module#VisitlogModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION,ROLE.TECHNICIAN,ROLE.CENTER_ADMIN] }
            },
            {
                "path": "analytics\/centerwiseregistration",
                "loadChildren": ".\/pages\/default\/components\/analytics\/customerCenterRegisStats\/customerCenterRegisStats.module#CustomerCenterRegisStatsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION,ROLE.TECHNICIAN,ROLE.CENTER_ADMIN] }
            },
            {
                "path": "analytics\/centerwisetest",
                "loadChildren": ".\/pages\/default\/components\/analytics\/customer-center-test-stats\/customer-center-test-stats.module#CustomerCenterTestStatsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION,ROLE.TECHNICIAN,ROLE.CENTER_ADMIN] }
            },
            {
                "path": "analytics\/avgdiagnostics",
                "loadChildren": ".\/pages\/default\/components\/analytics\/avgdiagnostics\/avgdiagnostics.module#AvgdiagnosticsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION,ROLE.TECHNICIAN, ROLE.CENTER_ADMIN] }
            },
            {
                "path": "analytics\/avgconsumablebasedtest",
                "loadChildren": ".\/pages\/default\/components\/analytics\/avgconsumablebasedtest\/avgconsumablebasedtest.module#AvgconsumablebasedtestModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION,ROLE.TECHNICIAN, ROLE.CENTER_ADMIN] }
            },
            {
                "path": "analytics\/permachinedata",
                "loadChildren": ".\/pages\/default\/components\/analytics\/permachinedata\/permachinedata.module#PermachinedataModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION,ROLE.TECHNICIAN] }
            },
            {
                "path": "skinconfig",
                "loadChildren": ".\/pages\/default\/components\/skinconfig\/skinconfigbuild\/skinconfig.module#SkinconfigModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "skin/skinconfigedit",
                "loadChildren": ".\/pages\/default\/components\/skinconfig\/skinconfig-edit\/skinconfig-edit.module#SkinconfigEditModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "skin\/list",
                "loadChildren": ".\/pages\/default\/components\/skinconfig\/skinconfig-list\/skinconfig-list.module#SkinconfigListModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "skin\/details",
                "loadChildren": ".\/pages\/default\/components\/skinconfig\/skinconfig-details\/skinconfig-details.module#SkinconfigDetailsModule",
                "canActivate": [RouteGuard],
                "data": { 'roles': [ROLE.HC_ADMIN, ROLE.HC_OPERATION] }
            },
            {
                "path": "404",
                "loadChildren": ".\/pages\/default\/not-found\/not-found\/not-found.module#NotFoundModule"
            },
            {
                "path": "401",
                "loadChildren": ".\/pages\/default\/access-denied\/access-denied\/access-denied.module#AccessDeniedModule"
            },
            {
                "path": "",
                "redirectTo": "index",
                "pathMatch": "full"
            }
        ]
    },
    {
        "path": "**",
        "redirectTo": "404",
        "pathMatch": "full"
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ThemeRoutingModule { }