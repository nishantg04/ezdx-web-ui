import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FileDetailsComponent } from '../file-details/file-details.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { UploadFileService } from '../upload-file.service'
import { FacilitiesService } from '../../facilities/facilities.service';
import { UsersService } from '../../users/users.service';
import { CommonService } from '../../../../../../_services/common.service';
import { DevicesService } from '../../devices/devices.service';
import { DataTableModule, SharedModule, PaginatorModule } from 'primeng/primeng';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": FileDetailsComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule,
        DataTableModule, SharedModule, PaginatorModule
    ], exports: [
        RouterModule
    ],
    declarations: [FileDetailsComponent],
    providers: [
        UploadFileService,
        FacilitiesService,
        UsersService,
        CommonService,
        DevicesService
    ]
})
export class FileDetailsModule { }
