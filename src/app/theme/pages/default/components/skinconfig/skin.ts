
export class Skin {
    configType: string;
    facilityId: string;
    facilityName: string;
    fieldLabels: any = {};
    id: string;
    mandatoryFields: Array<any> = [];
    testRanges: Array<any> = [];
    testUnitMapping: Array<any> = [];
    testsMapped: Array<any> = [];
    updateTime: string;
}