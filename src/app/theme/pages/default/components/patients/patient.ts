import { Address } from '../facilities/address';
export class Patient {
    id: string;
    userId: string;
    firstName: string;
    lastName: string;
    gender: string;
    age: number;
    address: Address;
    email: string;
    phone: string;
    profilePicture: string;
    mrn: string;
    organizationId: string;
    syncTime: string;
    createTime: string;
    createTimeDate: Date;
    geopoint?: { lat: number, lon: number, alt: number }
}