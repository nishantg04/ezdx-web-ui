import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { UploadFileComponent } from '../upload/upload-file.component';
import { LayoutModule } from './../../../../layouts/layout.module';
import { DefaultComponent } from './../../default.component';
import { Ng4FilesModule } from "../../../../../../../node_modules/angular4-files-upload/src/app/ng4-files"
import { AutoCompleteModule, ButtonModule, DialogModule, GrowlModule, TabViewModule, InputMaskModule, InputTextModule } from 'primeng/primeng';
import { FacilitiesService } from '../facilities/facilities.service';
import { DevicesService } from '../devices/devices.service'
import { UploadFileService } from './upload-file.service'
import { CommonService } from '../../../../../_services/common.service'
import { DataTableModule, SharedModule, PaginatorModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": UploadFileComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, Ng4FilesModule, TranslateModule.forChild({ isolate: false }),
        AutoCompleteModule, DataTableModule, SharedModule, ButtonModule, DialogModule, GrowlModule, TabViewModule, InputMaskModule, InputTextModule, PaginatorModule
    ], exports: [
        RouterModule
    ],
    declarations: [UploadFileComponent],
    providers: [
        FacilitiesService,
        DevicesService,
        UploadFileService,
        CommonService,
        TranslateService
    ]

})
export class UploadFileModule { }
