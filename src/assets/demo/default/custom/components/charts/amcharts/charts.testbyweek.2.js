//== Class definition
var amChartsChartsDemo = function() {



    return {
        // public functions
        init: function() {
            testsChart();
        }
    };
}();

jQuery(document).ready(function() {
    //amChartsChartsDemo.init();
    var formateDate = function(d) {
        var m_names = new Array("Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec");

        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();
        return m_names[curr_month] + " " + curr_date + " " + curr_year;
    }
    var getDateRangeOfWeek = function(w, y) {
        var week = w.split("\n")
        var simple = new Date(y, 0, 1 + (week[0] - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay());
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        var ISOweekEnd = new Date(ISOweekStart);
        ISOweekEnd.setDate(ISOweekEnd.getDate() + 6);
        ISOweekEnd.setHours(23);
        ISOweekEnd.setMinutes(23);
        ISOweekEnd.setSeconds(23);
        ISOweekEnd.setMilliseconds(0);
        return formateDate(ISOweekStart) + " to " + formateDate(ISOweekEnd);
    };
    AmCharts.addInitHandler(function(chart) {
       // // console.log('data not', chart.dataProvider)
        if (chart.dataProvider.length === 0 || chart.dataProvider === undefined) {
            // add label to let users know the chart is empty
            chart.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
        }

    }, ["serial"]);
    //== Private functions
    var testsChart = function() {
        var testCountByWeek = JSON.parse(window.localStorage.getItem("testCountByWeek"));
        var selectedYear = window.localStorage.getItem("selectedYear");
        var graphValuesTestCountByWeek = JSON.parse(window.localStorage.getItem("graphValuesTestCountByWeek"));
        graphValuesTestCountByWeek.forEach(function(week) {
            week['balloonFunction'] = function(graphDataItem, graph) {
                return "<b>" + graphDataItem.graph.title + "</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b></span><br/><span>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
            }
        }, this);
        var selectedYear = window.localStorage.getItem("selectedYear");
        var chart = AmCharts.makeChart("tests_chart_week", {
            "type": "serial",
            "theme": "light",
            "depth3D": 20,
            "angle": 30,
            "legend": {
                "horizontalGap": 0,
                "verticalGap": 0,
                "markerSize": 10,
                "valueWidth": 20
            },
            "dataProvider": testCountByWeek,
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.5,
                "title": "Diagnostics done"
            }],
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Diagnostics done based on center"
            }],
            "graphs": graphValuesTestCountByWeek,
            "categoryField": "Week_Year",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "position": "left",
                "title": "Week"
            },
            "listeners": [{
                "event": "clickGraphItem",
                "method": function(event) {
                    //window.localStorage.setItem("SelectedMonthDiagnosis", event.item.category);
                    my.namespace.publicFuncPatientWeek(event.item.category, "test");
                }
            }]
        });
    }
    testsChart();
});