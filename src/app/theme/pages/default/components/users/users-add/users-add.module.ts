import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { UsersAddComponent } from './users-add.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { Ng4FilesModule } from "angular4-files-upload"
import { UsersService } from '../users.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { CommonService } from '../../../../../../_services/common.service';
import { FilterOrganizationByCodePipe } from './filter-organization-by-code.pipe'
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { StateChangeService } from '../state-change.service';
import { DropdownModule, DialogModule } from 'primeng/primeng';
import { FacDropdownModule } from './../../../../../fac-dropdown/fac-dropdown.module'


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": UsersAddComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, Ng4FilesModule, FacDropdownModule,
        TranslateModule.forChild({ isolate: false }), DropdownModule, DialogModule
    ], exports: [
        RouterModule
    ], declarations: [
        UsersAddComponent,
        FilterOrganizationByCodePipe
    ],
    providers: [
        UsersService,
        FacilitiesService,
        CommonService, TranslateService, StateChangeService
    ]

})
export class UsersAddModule { }