import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, RouterLink } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { Test } from '../test';
import { MasterTestService } from '../master-test.service';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./master-test-edit.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class MasterTestEditComponent implements OnInit, AfterViewInit {
    tests: Test[] = [];
    alert = [];
    testCount: any;
    constructor(private _script: ScriptLoaderService, private testService: MasterTestService, private _router: Router, private _activeroute: ActivatedRoute) {

    }

    ngOnInit() {
        Helpers.setLoading(true);
        this.testService.getAllTests().then((res) => {
            Helpers.setLoading(false);
            this.tests = res as Test[];
            this.testCount = this.tests.length;
        })
    }

    ngAfterViewInit() {

    }

    updateTest(test) {
        Helpers.setLoading(true);
        // console.log("test: ", test);
        this.testService.updateTest(test).then((res) => {
            // console.log("TEST: updated res: ", res);
            Helpers.setLoading(false);
            this.addSingle();
            window.setTimeout(() => {
                this.alert = [];
            }, 3000);
        });
    }

    addSingle() {
        this.alert.push({ severity: 'success', summary: 'Success', detail: 'Test updated successfully.' });
    }

}