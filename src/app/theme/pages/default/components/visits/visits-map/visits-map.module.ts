import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { VisitsMapComponent } from './visits-map.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { VisitsService } from '../visits.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { UsersService } from '../../users/users.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { GMapModule } from 'primeng/primeng';
import { PatientsService } from '../../patients/patients.service';
import { VisitsMapService } from './visits-map.service';
import { MapLoaderService } from '../../utils/map-loader.service';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": VisitsMapComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, TranslateModule.forChild({ isolate: false }), FormsModule, RouterModule.forChild(routes), LayoutModule, GMapModule
    ], exports: [
        RouterModule
    ], declarations: [
        VisitsMapComponent
    ],
    providers: [
        PatientsService,
        VisitsService,
        TranslateService,
        MapLoaderService,
        VisitsMapService
    ]

})
export class VisitsMapModule { }