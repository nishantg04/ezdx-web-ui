import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { ConfigService } from '../tests.service';
import { Router, ActivatedRoute } from "@angular/router";




@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./skinconfig-details.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SkinconfigDetailsComponent implements OnInit {

    showcontent: boolean = false;
    mappedList: any[] = [];
    alltestList: any[];
    newtests: any[];
    testRangeArray : any[] =[];
    mandatoryFields : any[] = [];
    constructor(private _testsService: ConfigService, private _activeroute: ActivatedRoute) { }

    public config: any = {};
    public totalcount: any;
    public confId: string = '';

    ngOnInit() {
        this._activeroute.queryParams.subscribe(param => {
            this.confId = param.confId;
        });
        this.getconfigfiledetail(this.confId);

    }

    tabClick(a) {
        if (a == 'range') {
            setTimeout(() => {
                //this.expand(0)
            }, 500);
        }
    }

    expand(id) {
        //console.log('id', id)
        document.getElementById('visit' + id).style.display = 'block'
        document.getElementById('exicon' + id).style.display = 'none'
        document.getElementById('compicon' + id).style.display = 'block'
    }

    getAllTests() {
        //console.log('entered get test')
        this.newtests = [];
        this.alltestList = [];
        Helpers.setLoading(true)
        this._testsService.getAllTests().then(res => {
            Helpers.setLoading(false)
            this.alltestList = res;
            this.alltestList.forEach(test => {
                if (test.active) {
                    this.newtests.push(test)
                }
            })
            // console.log(this.newtests[0])
            this.newtests.forEach(test => {
                test.testIconUrl = "./assets/app/media/img/test/" + test.testName + ".png"
            })
            this.newtests.forEach(test => {

                if (test.testName.includes('_')) {
                    test.testName = test.testName.split('_').join(' ')
                }
                if (test.testName == 'URINE') {
                    test.testName = 'URINE 2P'
                }
                if (test.testName == 'DENGUE') {
                    test.testName = 'DENGUE ANTIGEN'
                }
                if (test.testType.includes('_')) {
                    test.testType = test.testType.split('_').join(' ');
                }
            })
            var testDataArray = [];
            this.mappedList = [];
            this.newtests.forEach(test => {
                this.config.testsMapped.forEach(id => {
                    if (id == test.id) {
                        testDataArray.push(test)
                    }
                })
            })
            this.mappedList = testDataArray;
            this.newtests.forEach(test=>{
                if(test.testType != 'WHOLE BLOOD POCT' && test.testType != 'WHOLE BLOOD RDT'){
                    if(test.testName != 'BLOOD GROUPING' && test.testName !='ECG' && test.testName !='SYMPTOM BASED TEST')
                      this.testRangeArray.push(test)               
                }
            })  
            setTimeout(() => {
                this.showcontent = true;
            }, 1500);
        });
    }


    compress(id) {
        document.getElementById('visit' + id).style.display = 'none'
        document.getElementById('exicon' + id).style.display = 'block'
        document.getElementById('compicon' + id).style.display = 'none'

    }

    getconfigfiledetail(id) {
        Helpers.setLoading(true);
        this.mandatoryFields = [];
        this._testsService
            .getconfigfiledetail(id)
            .then(res => {                
                this.config = res;  
                this.config.mandatoryFields.forEach(element => {
                    if(element !== 'DATE_OF_BIRTH')
                       this.mandatoryFields.push(element)
                });   


            }).then(() => {
                this.getAllTests();
            });
    }
}
