//== Class definition
var amChartsChartsDemo = function() {


    return {
        // public functions
        init: function() {
            patientsChart();
        }
    };
}();

jQuery(document).ready(function() {
    //amChartsChartsDemo.init();
    var sDate = window.localStorage.getItem('dashStartDate');
    var time = new Date(sDate);
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    var date1 = time.getDate();
    var hour = time.getHours();
    var minutes = time.getMinutes();
    var seconds = time.getSeconds();
    var eDate = window.localStorage.getItem('dashEndDate');
    var etime = new Date(sDate);
    var eyear = time.getFullYear();
    var emonth = time.getMonth() + 1;
    var edate1 = time.getDate();
    var ehour = time.getHours();
    var eminutes = time.getMinutes();
    var eseconds = time.getSeconds();
    var formateDate = function(d) {
        var m_names = new Array("Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec");

        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();
        return m_names[curr_month] + " " + curr_date + " " + curr_year;
    }
    var getDateRangeOfWeek = function(w, y) {
        var week = w.split("\n")
        var simple = new Date(y, 0, 1 + (week[0] - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        var ISOweekEnd = new Date(ISOweekStart);
        ISOweekEnd.setDate(ISOweekEnd.getDate() + 6);
        ISOweekEnd.setHours(23);
        ISOweekEnd.setMinutes(23);
        ISOweekEnd.setSeconds(23);
        ISOweekEnd.setMilliseconds(0);
        return formateDate(ISOweekStart) + " to " + formateDate(ISOweekEnd);
    };

    AmCharts.addInitHandler(function(chart) {
      //  // console.log('data hori', chart)
        if (chart.dataProvider.length === 0 || chart.dataProvider === undefined) {
            // add label to let users know the chart is empty
            chart.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
        }

    }, ["serial"]);

    //== Private functions
    // var patientsChart = function () {
    var testsForMonth = JSON.parse(window.localStorage.getItem("testsForMonth"));
    var testColor = JSON.parse(window.localStorage.getItem('testColor'));
    var selectedYear = JSON.parse(window.localStorage.getItem('selectedYear'))
    var chart = AmCharts.makeChart("each_test_chart", {
        "type": "serial",
        "categoryField": "Type",
        "maxSelectedTime": -2,
        "angle": 20,
        "rotate": true,
        "depth3D": 20,
        "startDuration": 1,
        "startEffect": "easeOutSine",
        "processCount": 998,
        "processTimeout": -2,
        "theme": "light",
        "categoryAxis": {
            "gridPosition": "start",
            "title": "Test Type"
        },
        "trendLines": [],
        "graphs": [{
            "balloonFunction": function(graphDataItem, graph) {
                // console.log('ballon', graphDataItem)
                return "<span style='font-size:14px'> " + graphDataItem.category + ": <b>" + graphDataItem.values.value + "</b></span><br/><span>" + selectedYear + "</span>"
            },
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "Diagnostics done for each test type ",
            "type": "column",
            "valueField": "Tests",
            "labelText": "[[value]]",
            "lineColor": testColor,
            "fillColors": testColor,
            "showHandOnHover": false
        }],
        "guides": [],
        "valueAxes": [{
            "id": "ValueAxis-1",
            "stackType": "regular",
            "title": "Diagnostics Tests",
            "minimum": 0
        }],
        "allLabels": [],
        "balloon": {},
        "legend": {
            "enabled": false,
            "useGraphSettings": true
        },
        "titles": [{
            "id": "Title-1",
            "size": 15,
            "text": "Diagnostics done for each test from" + " " + selectedYear
        }],
        "dataProvider": testsForMonth,
        "listeners": [{
            "event": "clickGraphItem",
            "method": function(event) {
                //window.localStorage.setItem("SelectedMonthDiagnosis", event.item.category);
                // my.namespace.publicFuncPatientWeek(event.item.category, "patient");
            }
        }]
    });
    //}
});