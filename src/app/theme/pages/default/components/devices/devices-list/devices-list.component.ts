import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';

import { Device } from '../device';
import { DateFormat } from '../dateFormat';
import { DevicesService } from '../devices.service';
import { Facility } from '../../facilities/facility';
import { FacilitiesService } from '../../facilities/facilities.service';
import { Center } from '../../facilities/center';
import { setTimeout } from 'timers';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./devices-list.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class DevicesListComponent implements OnInit, AfterViewInit {
    device = new Device();
    userType: string;
    userInfo: any = {};
    devices: Device[];
    undevices: Device[];
    allDevices: Device[];
    assignedDevices: Device[];
    unAssignedDevices: Device[];
    devicesWithOrgName: any[];
    facilities: Facility[];
    selectedFacility: Facility;
    selectedCenter: Center;
    centers: Center[];
    years: any[];
    selectedYear: any;
    fromDate: string;
    toDate: string;
    facilityId: string;
    centerId: string;
    noClient: boolean = false;
    unAssignedFacility: Facility;
    unAssignedCenter: Center;
    unCenters: Center[];
    successAssignMsg: string;
    checkAll: boolean;
    sortType: string;
    sort: boolean;
    loading: boolean;
    msg: string;
    showOrgCenter: boolean;
    noRecord: boolean = false;
    centerSelect: boolean = false;
    chooseHub: String;
    noHub: boolean = false;
    successMsg: string;
    sucessAssignCus: string;
    pageNo: number;
    pageSize: number;
    row: number;
    devicesCount: string;
    unDevicesCount: string;
    visiblePaginationAssigned: boolean = false;
    visiblePaginationUnassigned: boolean = false;
    searchString: string;
    selectedSearchBy: any;
    SearchBylist: any;
    searchby: any;
    status: any;
    ranges: any[];
    idList: any[] = [];
    countNo: any = 0;
    id: any;
    showDialog: boolean = false;
    statusDev: any;
    deviceType: any;
    unAssignedCenterid: any = '';
    chosen: string;
    unAssignedFacilityid: any = '';
    hideFilter : boolean = false;
    constructor(private _ngZone: NgZone, private _devicesService: DevicesService, private _facilitiesService: FacilitiesService, private _script: ScriptLoaderService, private _router: Router, private _activeroute: ActivatedRoute) {
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        this.unAssignedFacility = new Facility();
        this.unAssignedCenter = new Center();
        this.loading = false;
        this.sortType = "serialNumber";
        this.sort = false;
        this.showOrgCenter = false;
        this.checkAll = false;
        this.status = 'ASSIGNED'
        this.searchString = ''
        if (window.localStorage.getItem('userType') == 'Facility')
            this.userType = 'Facility';
        else if (window.localStorage.getItem('userType') == 'Center'){
            this.hideFilter = true;
            this.userType = 'Center';
        }            
        else
            this.userType = 'Admin';
        this.ranges = [
            { label: "10", value: 10 },
            { label: "25", value: 25 },
            { label: "50", value: 50 },
            { label: "100", value: 100 },
        ];
        this.pageNo = 1;
        this.pageSize = 10;
        this.row = 10;

    }
    ngOnInit() {
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
        window.my.namespace.deviceUpdatePublicFunc = this.deviceUpdatePublicFunc.bind(this);        
        if (window.localStorage.getItem('userType')=='Admin') {
            this.unAssignedlist();
        } 
        document.getElementById('prime_search').style.display = "none";      
        this.clearMsg();
    }

    clearMsg() {
        this.successAssignMsg = null;
        this.successMsg = null;
    }
   

    ngOnDestroy() {
        window.my.namespace.publicFunc = null;
    }

    deviceUpdatePublicFunc(deviceId) {
        // this._ngZone.run(() => this.goToDeviceEdit(deviceId));
    }

    goToDeviceEdit(deviceId, type) {
        this._router.navigate(['/devices/update'], { queryParams: { deviceId: deviceId, from: type } });
    }

    goToDeviceView(deviceId) {
        this._router.navigate(['/devices/details'], { queryParams: { deviceId: deviceId } });
    }

    ngAfterViewInit() {
        window.localStorage.removeItem('patientStartDate');
        window.localStorage.removeItem('patientEndDate');
        this.fromDate = this._activeroute.snapshot.queryParams['from'];
        this.toDate = this._activeroute.snapshot.queryParams['to'];
        this.selectedFacility.id = this._activeroute.snapshot.queryParams['facilityId'];
        this.selectedCenter.id = this._activeroute.snapshot.queryParams['centerId'];        
        if (this.fromDate && this.toDate) {
            window.localStorage.setItem('patientStartDate', this.fromDate);
            window.localStorage.setItem('patientEndDate', this.toDate);
            this.onFilterClick();
        }
        else {
            var endDate = new Date();
            endDate.setDate(endDate.getDate() + 1);
            endDate.setHours(23);
            endDate.setMinutes(23);
            endDate.setSeconds(23);
            endDate.setMilliseconds(0);
            window.localStorage.setItem('patientEndDate', endDate.toString());
        }
        this.getFacilities();

        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/datatables/base/date.patients.js');
    }

    onFilterClick(): void {
        this.visiblePaginationAssigned = false;
        setTimeout(() => this.visiblePaginationAssigned = true, 0);
        this.assignedDevices = [];
        this.devices = [];
        this.devicesWithOrgName = [];
        this.allDevices = [];
        this.searchString = ''
        if (this.status == 'ASSIGNED') {
            if (this.userType == 'Center') {
                this.selectedCenter.id = this.userInfo.centerId;
                this.selectedFacility.id = this.userInfo.facilityId;
            }
            if (this.selectedFacility.id != "ALL") {
                if (this.selectedCenter.id != "ALL") {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedCenter.id);
                }
                else {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "FACILITY", this.selectedFacility.id);
                }
            }
            else {
                this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0);
            }
        }
        if (this.status == 'UNASSIGNED') {
            this.unAssignedlist();
        }
    }


    getFacilities(): void {
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            } else if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - 29);
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
        startDate.setMilliseconds(0);
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        endDate.setHours(23);
        endDate.setMinutes(23);
        endDate.setSeconds(23);
        endDate.setMilliseconds(0);
        if (!this.fromDate && !this.toDate) {
            this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
            if (window.localStorage.getItem('userType') == 'Facility') {
                this.selectedFacility.id = this.userInfo.facilityId;
                this.facilities.forEach(fclt => {
                    if (fclt.id == this.userInfo.facilityId) {
                        this.centers = jQuery.extend(true, [], fclt.centers);
                        let allCenter = new Center();
                        allCenter.name = "ALL";
                        allCenter.id = "ALL";
                        this.centers.splice(0, 0, allCenter);
                        this.selectedCenter.id = this.centers[0].id;
                    }
                });
            }
            else if (window.localStorage.getItem('userType') == 'Center') {
                this.selectedFacility.id = this.userInfo.facilityId;
                this.selectedCenter.id = this.userInfo.centerId;
                this.centers = null;
            }
            else {
                let allFacility = new Facility();
                allFacility.name = "ALL";
                allFacility.id = "ALL";
                this.facilities.splice(0, 0, allFacility);
                this.selectedFacility.id = this.facilities[0].id;
                this.centers = this.facilities[0].centers;
                let allCenter = new Center();
                allCenter.name = "ALL";
                allCenter.id = "ALL";
                //this.centers.splice(0, 0, allCenter);
                //this.selectedCenter.id = this.centers[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
            let searchcriteria = "ALL";
            if (window.localStorage.getItem('userType') === "Facility") {
                searchcriteria = "FACILITY";
                this.list(0, new Date(endDate).getTime(), searchcriteria, this.selectedFacility.id);
            }
            else if (window.localStorage.getItem('userType') === "Center") {
                searchcriteria = "CENTER";
                this.list(0, new Date(endDate).getTime(), searchcriteria, this.selectedCenter.id);
            }
            else {
                this.list(0, new Date(endDate).getTime(), searchcriteria, 0);
            }
        }
        else {
            let allFacility = new Facility();
            allFacility.name = "ALL";
            allFacility.id = "ALL";
            this.facilities.splice(0, 0, allFacility);
            if (this.selectedFacility.id != "ALL") {
                this.facilities.forEach(fac => {
                    if (fac.id === this.selectedFacility.id) {
                        this.centers = fac.centers;
                        let allCenter = new Center();
                        allCenter.name = "ALL";
                        allCenter.id = "ALL";
                        this.centers.splice(0, 0, allCenter);
                    }
                });
            }
            this.onFilterClick();
        }
        // });
    }

    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers[0].id !== "ALL") {
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                }
                this.selectedCenter.id = this.centers[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }
    // onOrgUnAssignChange(orgId: string): void {
    //     this.centerSelect = false;
    //     this.sucessAssignCus = null;
    //     this.successAssignMsg = null;
    //     this.facilities.forEach((org, i) => {
    //         if (org.id === orgId) {
    //             this.device.organizationCode = org.organizationCode;
    //             this.unAssignedFacility.organizationCode = org.organizationCode;
    //             this.unCenters = org.centers;
    //             this.device.centerId = this.unCenters[0].id;
    //         }
    //     });
    // }

    onOrgUnAssignChange(value): void {
        this.centerSelect = false;
        this.sucessAssignCus = null;
        this.successAssignMsg = null;
        this.device.organizationCode = value.organizationCode;
        this.unAssignedFacilityid = value.id;
        this.unAssignedFacility.organizationCode = value.organizationCode;
        this.unCenters = value.centers;
        this.device.centerId = this.unCenters[0].id;
    }

    searchingHub(event) {
        this.searchString = event;
        if (this.searchString == '') {
            this.assignedDevices = []
            this.assignedDevices = [];
            this.devices = [];
            this.devicesWithOrgName = [];
            this.allDevices = [];
            if (this.status == 'ASSIGNED') {
                if (this.selectedFacility.id != "ALL") {
                    if (this.selectedCenter.id != "ALL") {
                        this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedCenter.id);
                    }
                    else {
                        this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "FACILITY", this.selectedFacility.id);
                    }
                }
                else {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0);
                }
            }
            if (this.status == 'UNASSIGNED') {
                this.unAssignedlist();
            }
        }
        var page; var size;
        if (this.searchString.length >= 3) {
            Helpers.setLoading(true);
            this.devices = [];
            this.undevices = [];
            if (this.status == 'UNASSIGNED') {
                page = this.pageNo;
                size = this.pageSize;
                this.searchby = 'DEVICE_SERIAL_NUMBER_WITH_STATUS'
                this.getAllHub(page, size)
            }
            if (this.status == 'ASSIGNED') {
                page = this.pageNo;
                size = this.pageSize;
                if (this.selectedFacility.id == 'ALL') {
                    this.searchby = 'DEVICE_SERIAL_NUMBER_WITH_STATUS'
                    this.getAllHub(page, size)
                }
                if (this.selectedFacility.id != 'ALL' && this.selectedCenter.id && this.selectedCenter.id != 'ALL') {
                    this.searchby = 'DEVICE_SERIAL_NUMBER_WITH_STATUS_AND_CENTER'
                    this.getCenterHub(page, size, this.selectedCenter.id)
                }
                if (this.selectedFacility.id && this.selectedCenter.id == 'ALL') {
                    this.searchby = 'DEVICE_SERIAL_NUMBER_WITH_STATUS_AND_FACILITY'
                    this.getCenterHub(page, size, this.selectedFacility.id)
                }
            }

        }
    }

    getAllHub(page, size) {
        this._devicesService.getSearch(this.searchby, this.searchString, this.status, page, size)
            .then(devices => {
                if (this.status == 'ASSIGNED') {
                    if (devices != null) {
                        if (devices.devices) {
                            this.devices = devices.devices;
                            this.devicesCount = devices.count.toString();
                            this.visiblePaginationAssigned = true;
                            this.getCustomerForDevice();
                        }
                    }
                    else {
                        this.devices = [];
                        this.devicesCount = '0';
                        this.visiblePaginationAssigned = true;
                        this.assignedDevices = [];
                    }
                }
                if (this.status == 'UNASSIGNED') {
                    if (devices != null) {
                        if (devices.devices) {
                            this.undevices = devices.devices;
                            this.unDevicesCount = devices.count.toString();
                            this.visiblePaginationUnassigned = true;
                            this.undevices.forEach(val=>{
                                if (val.deviceStatus == 'ACTIVE') {
                                    val.status = true
                                }
                                if (val.deviceStatus == 'INACTIVE') {
                                    val.status = false
                                }
                            })
                        }
                    } else {
                        this.undevices = [];
                        this.unDevicesCount = '0';
                        this.visiblePaginationUnassigned = true;
                    }
                }
                Helpers.setLoading(false);

            })
    }

    getCenterHub(page, size, id) {
        this._devicesService.getSearchByCenter(this.searchby, this.searchString, this.status, id, page, size)
            .then(devices => {
                if (this.status == 'ASSIGNED') {
                    if (devices != null) {
                        if (devices.devices) {
                            this.devices = devices.devices;
                            this.devicesCount = devices.count.toString();
                            this.visiblePaginationAssigned = true;
                            this.getCustomerForDevice();
                        }
                    }
                    else {
                        this.devices = [];
                        this.devicesCount = '0';
                        this.visiblePaginationAssigned = true;
                        this.assignedDevices = [];
                    }
                }
                if (this.status == 'UNASSIGNED') {
                    if (devices != null) {
                        if (devices.devices) {
                            this.undevices = devices.devices;
                            this.unDevicesCount = devices.count.toString();
                            this.visiblePaginationUnassigned = true;                        
                        }
                    } else {
                        this.undevices = [];
                        this.unDevicesCount = '0';
                        this.visiblePaginationUnassigned = true;
                    }
                }
                Helpers.setLoading(false);

            })
    }

    list(fromDate, toDate, searchcriteria, id): void {
        Helpers.setLoading(true);
        this.devicesWithOrgName = [];
        this.devices = [];
        this.allDevices = [];
        this.undevices = [];
        this._devicesService.getDevicesDatePaginated(fromDate, toDate, searchcriteria, id, this.pageNo, this.pageSize, "ASSIGNED")
            .then(devices => {
                Helpers.setLoading(false);
                if (devices.length != 0) {
                    this.allDevices = devices.devices;
                    this.devicesCount = devices.count.toString();
                    this.visiblePaginationAssigned = true;
                } else {
                    this.assignedDevices = [];
                    this.devicesCount = '0';
                    this.visiblePaginationAssigned = true;
                }
            })

            .then(() => {
                this.allDevices.forEach((device, i) => {
                    if (device.organizationId) {
                        this.devices.push(device);
                    }
                });
                this.getCustomerForDevice()

            });
    }

    getCustomerForDevice() {
        this.assignedDevices = [];
        this.devicesWithOrgName = [];
        if (this.devices) {
            this.devices.forEach((val, i) => {
                var d = new Date(val.activationDate),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;
                val.activationDate = [day, month, year].join('-');
                if (val.organizationId) {
                    val.assignmentStatus = "ASSIGNED"
                }
                if (val.deviceStatus == 'ACTIVE') {
                    val.status = true
                }
                if (val.deviceStatus == 'INACTIVE') {
                    val.status = false
                }
                val.assignmentStatus = (val.assignmentStatus === "ASSIGNED") ? "assigned" : "unassigned";
                let currentDevice: any = val;
                currentDevice.organizationName = "N/A";
                this.facilities.forEach(fac => {
                    if (fac.id === currentDevice.organizationId)
                        currentDevice.organizationName = fac.name;
                });
                this.devicesWithOrgName.push(currentDevice);
            });

            for (var i = 0; i < this.devicesWithOrgName.length; i++) {
                if (this.devicesWithOrgName[i].centerId !== undefined) {
                    for (var k = 0; k < this.facilities.length; k++) {
                        if (this.facilities[k].centers !== undefined) {
                            for (var j = 0; j < this.facilities[k].centers.length; j++) {
                                if (this.facilities[k].centers[j].id === this.devicesWithOrgName[i].centerId) {
                                    this.devicesWithOrgName[i].centerName = this.facilities[k].centers[j].name;
                                }
                            }
                        }
                    }
                }

            }

            this.assignedDevices = this.devicesWithOrgName;
            if (this.assignedDevices.length > 10) {
                this.assignedDevices.reduceRight
            }
            // console.log(this.assignedDevices)

        } else {
            this.assignedDevices = [];
        }
    }

    tableSelectClick(type) {
        if (type === 'assign') {
            // if(this.fliterPresent === false)
            this.status = 'ASSIGNED'
            this.searchString = '';
            this.assignedDevices = [];
            this.devices = [];
            this.devicesWithOrgName = [];
            this.allDevices = [];
            this.getFacilities();
            this.pageTab('ASSIGNED')
            document.getElementById('m_form_search').style.display = 'block';
            document.getElementById('search_icon').style.display = 'block';
            document.getElementById('prime_search').style.display = "none";
            this.noClient = false;
            // this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0);
        } else if (type === 'unassign') {
            this.noClient = true;
            this.searchString = '';
            this.sortType = "serialNumber";
            this.status = 'UNASSIGNED'
            this.sort = false;
            this.pageTab('UNASSIGNED');
            document.getElementById('m_form_search').style.display = 'none';
            document.getElementById('search_icon').style.display = 'none';
            document.getElementById('noRecord').style.display = 'none';
            document.getElementById('prime_search').style.display = "block";
            document.getElementsByClassName('ui-datatable-emptymessage')
            this.undevices = [];

            this.unAssignedlist();
            this.getUnHubFacilities();

        }
    }

    getUnHubFacilities(): void {
        this.centerSelect = true;
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) { return 1; } else if (a.name < b.name) { return -1; } return 0;
        });
        // })

        // .then(() => {
        Helpers.setLoading(false);
        // this.unAssignedFacility.id = this.facilities[0].id;
        // this.unCenters = this.facilities[0].centers;
        // this.unAssignedCenter.id = this.unCenters[0].id;
        // this.unAssignedFacility.organizationCode = this.facilities[0].organizationCode;
        if (this.device.id === "") {
            this.device.organizationId = this.facilities[0].id;
            this.centers = this.facilities[0].centers;
            this.device.centerId = this.centers[0].id;
        }
        else {
            if (this.device.organizationId && this.device.centerId) {
                this.facilities.forEach((org, i) => {
                    if (org.id === this.device.organizationId) {
                        this.centers = org.centers;
                    }
                });
            }
        }
        // });
    }

    oncheckAll(): void {
        this.noHub = false;
        this.successAssignMsg = null;
        this.checkAll = !this.checkAll;
        this.undevices.forEach((device, i) => {
            device.selected = this.checkAll;
        })
    }

    onDeviceCheck(device): void {
        this.noHub = false;
        this.successAssignMsg = null
        device.selected = !device.selected;
        let allDevicesSelected = true;
        if (device.selected == true) {
            this.idList.push(device);
        }
        if (device.selected == false) {
            var index = this.idList.indexOf(device.id);
            this.idList.splice(index, 1);
        }
        if (this.idList.length != 0) {
            this.countNo = this.idList.length;
        } else
            this.countNo = 0;
        this.undevices.forEach((device, i) => {
            if (!device.selected)
                allDevicesSelected = false;
        });
        if (allDevicesSelected)
            this.checkAll = true;
        else
            this.checkAll = false;
    }

    unAssignedlist(): void {
        Helpers.setLoading(true);
        this.devices = [];
        this.allDevices = [];
        this.undevices = [];
        this.idList = [];
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        endDate.setHours(23);
        endDate.setMinutes(23);
        endDate.setSeconds(23);
        endDate.setMilliseconds(0);
        this._devicesService.getDevicesDatePaginated(0, new Date(endDate).getTime(), "ALL", 0, this.pageNo, this.pageSize, "UNASSIGNED")
            .then(devices => {
                Helpers.setLoading(false);
                if (devices.devices) {
                    this.undevices = devices.devices;
                    this.unDevicesCount = devices.count.toString();
                    this.visiblePaginationUnassigned = true;
                } else {
                    this.undevices = [];
                    this.unDevicesCount = '0';
                    this.visiblePaginationUnassigned = true;
                }

            })
            .then(() => {
                if (this.undevices != undefined && this.undevices.length != 0) {
                    this.undevices.forEach((device, i) => {
                        this.idList.forEach(dev => {
                            if (dev.id == device.id) {
                                if (dev.selected == true)
                                    device.selected = true;
                                else
                                    device.selected = false;
                            }
                        })
                    });
                    this.undevices.forEach(function(val, i) {
                        var d = new Date(val.activationDate),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();
                        if (val.deviceStatus == 'ACTIVE') {
                            val.status = true
                        }
                        if (val.deviceStatus == 'INACTIVE') {
                            val.status = false
                        }

                        if (month.length < 2) month = '0' + month;
                        if (day.length < 2) day = '0' + day;
                        val.activationDate = [month, day, year].join('/');
                    });
                } else {
                    this.undevices = [];
                }

                localStorage.setItem('unAssingedDeviceTable', JSON.stringify(this.undevices));
                if (localStorage.getItem('unAssingedDeviceTable')) {
                    this.undevices = JSON.parse(localStorage.getItem('unAssingedDeviceTable'))
                }
            });
    }

    // assignDevice(): void {
    //     this.successAssignMsg = null;
    //     this.sucessAssignCus = null;
    //     if (this.idList.length == 0 || !this.unAssignedCenter.id || !this.unAssignedFacility.id) {
    //         this.successAssignMsg = "SELECT_BOTH_CUSTOMER_AND_HUB_THEN_TRY_TO_ASSIGN";
    //     }
    //     this.idList.forEach((device, i) => {
    //         if (device.selected) {
    //             this.successMsg = null;
    //             this.successAssignMsg = null;
    //             this.noHub = false;
    //             device.organizationId = this.unAssignedFacility.id;
    //             device.organizationCode = this.unAssignedFacility.organizationCode;
    //             device.centerId = this.unAssignedCenter.id;
    //             device.assignmentStatus = "ASSIGNED";
    //             delete device["selected"]
    //             if (device.activationDate != "NaN") {
    //                 var myDate = new Date(device.activationDate);
    //                 var offset = myDate.getTimezoneOffset() * 60 * 1000;
    //                 var withOffset = myDate.getTime();
    //                 var withoutOffset = withOffset - offset;
    //                 device.activationDate = withOffset.toString();
    //             } else {
    //                 var myDate = new Date();
    //                 var offset = myDate.getTimezoneOffset() * 60 * 1000;
    //                 var withOffset = myDate.getTime();
    //                 var withoutOffset = withOffset - offset;
    //                 device.activationDate = myDate.getTime().toString();
    //             }
    //             if (device.organizationId && device.centerId) {
    //                 if (device.activationDate == "NaN") {
    //                     var myDate = new Date();
    //                     var offset = myDate.getTimezoneOffset() * 60 * 1000;
    //                     var withOffset = myDate.getTime();
    //                     var withoutOffset = withOffset - offset;
    //                     device.activationDate = myDate.getTime().toString();
    //                 }
    //                 this._devicesService.deviceAssign(device)
    //                     .then((data) => // console.log(data))
    //                     .then(() => {
    //                         this.successAssignMsg = null;
    //                         this.successMsg = "HUB_ASSIGNED_SUCCESSFULLY";
    //                         this.undevices = [];
    //                         this.idList = [];
    //                         this.countNo = 0;
    //                         this.pageNo1 = 1;
    //                         this.unAssignedlist();
    //                         setTimeout(() => {
    //                             this.successMsg = null;
    //                         }, 5000);
    //                     }),
    //                     (e: any) => {
    //                         // console.log(e);
    //                         if (e._body) {
    //                             let res = JSON.parse(e._body);
    //                             // console.log(res)
    //                             if (res.error) {
    //                                 this.successAssignMsg = "Hub assiging Failed";
    //                                 setTimeout(() => {
    //                                     this.successAssignMsg = null;
    //                                 }, 5000);
    //                             }
    //                         }
    //                     }

    //             } else {
    //                 device.selected = true;
    //                 this.successAssignMsg = "SELECT_BOTH_CUSTOMER_AND_HUB_THEN_TRY_TO_ASSIGN";
    //             }


    //         } else {
    //             this.noHub = true;
    //             //  this.successAssignMsg = "SELECT_BOTH_CUSTOMER_AND_HUB_THEN_TRY_TO_ASSIGN";
    //         }
    //     });
    // }

    assignDevice(customer, center): void {
        this.successAssignMsg = null;
        this.sucessAssignCus = null;
        console.log(this.idList.length,customer,center)
        if (this.idList.length == 0 || customer == '' || center == '') {
            this.successAssignMsg = "SELECT_BOTH_CUSTOMER_AND_HUB_THEN_TRY_TO_ASSIGN";
        }
        else {
            Helpers.setLoading(true);
            let deviceIds = []
            this.idList.map((device, i) => {
                if (device.selected) {
                    this.successMsg = null;
                    this.successAssignMsg = null;
                    this.noHub = false;
                    deviceIds.push(device.id);
                }
            })
            let assignObj = {
                deviceIds: deviceIds,
                facilityId: customer,
                centerId: center
            }
            // console.log(assignObj);
            this._devicesService.deviceAssign(assignObj)
                .then((data) => console.log(data))
                .then(() => {
                    Helpers.setLoading(false);
                   // this.unAssignedFacilityid = '';
                    this.unAssignedCenterid = '';
                    this.successAssignMsg = null;
                    this.successMsg = "HUB_ASSIGNED_SUCCESSFULLY";
                    this.undevices = [];
                    this.idList = [];
                    this.countNo = 0;
                    this.pageNo = 1;
                    this.unAssignedlist();
                    setTimeout(() => {
                        this.successMsg = null;
                    }, 5000);
                }),
                (e: any) => {
                    // console.log(e);
                    if (e._body) {
                        let res = JSON.parse(e._body);
                        // console.log(res)
                        if (res.error) {
                            this.successAssignMsg = "Hub assiging Failed";
                            setTimeout(() => {
                                this.successAssignMsg = null;
                            }, 5000);
                        }
                    }
                }
        }
    }

    onDeviceRowclick(selectedDevice): void {
        selectedDevice.selected = !selectedDevice.selected;
    }

    select(e) {
        this.visiblePaginationAssigned = false;
        this.visiblePaginationUnassigned = false;
        this.pageSize = e.value;
        this.pageNo = 1;
        this.row = e.value;
      //  this.onFilterClick();
      if (this.status == 'ASSIGNED') {
        this.searchingHub(this.searchString);
        } else {
            this.assignedDevices = []
            this.assignedDevices = [];
            this.devices = [];
            this.devicesWithOrgName = [];
            this.allDevices = [];
            if (this.status == 'ASSIGNED') {
                if (this.selectedFacility.id != "ALL") {
                    if (this.selectedCenter.id != "ALL") {
                        this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedCenter.id);
                    }
                    else {
                        this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "FACILITY", this.selectedFacility.id);
                    }
                }
                else {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0);
                }
            }
            if (this.status == 'UNASSIGNED') {
                this.unAssignedlist();
            }
        }    

    }

    selectUn(e){
        this.visiblePaginationAssigned = false;
        this.visiblePaginationUnassigned = false;
        this.pageSize = e.value;
        this.pageNo = 1;
        this.row = e.value;
        if (this.status == 'UNASSIGNED') {
            this.searchingHub(this.searchString);
        } else
            this.unAssignedlist();
    }

    paginateUnDevices(event) {
        this.pageNo = event.page + 1;
        this.pageSize = parseInt(event.rows);
        if (this.status == 'UNASSIGNED') {
            this.searchingHub(this.searchString);
        } else
            this.unAssignedlist();
    }
    paginateDevices(event) {
        this.pageNo = event.page + 1;
        this.pageSize = parseInt(event.rows);
        if (this.status == 'ASSIGNED') {
            this.searchingHub(this.searchString);
        } else {
            this.assignedDevices = []
            this.assignedDevices = [];
            this.devices = [];
            this.devicesWithOrgName = [];
            this.allDevices = [];
            if (this.status == 'ASSIGNED') {
                if (this.selectedFacility.id != "ALL") {
                    if (this.selectedCenter.id != "ALL") {
                        this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedCenter.id);
                    }
                    else {
                        this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "FACILITY", this.selectedFacility.id);
                    }
                }
                else {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0);
                }
            }
            if (this.status == 'UNASSIGNED') {
                this.unAssignedlist();
            }
        }

    }

    statusChange(device) {
        this.device = device;
        this.device.status = !this.device.status;
        if (this.device.deviceType.includes('_'))
            this.deviceType = this.device.deviceType.replace('_', ' ')
        if (this.device.deviceType.includes('_'))
            this.deviceType = this.device.deviceType.replace('_', ' ')
        if (this.device.status == false) {
            this.statusDev = 'Deactivate'
        }
        if (this.device.status == true) {
            this.statusDev = 'Activate'
        }
        this.showDialog = true;

    }

    conformStatus() {
        Helpers.setLoading(true)

        if (this.device.status == false) {
            this.device.deviceStatus = 'INACTIVE'
            delete this.device["status"]
        }
        if (this.device.status == true) {
            this.device.deviceStatus = 'ACTIVE'
            this.device.status = true
            //delete this.device["status"]
        }
        this._devicesService.updateDeviceStatus(this.device, this.statusDev)
            .then(data => {
                Helpers.setLoading(false)
                this.showDialog = false;

            })
    }

    close() {
        this.showDialog = false;
        this.device.status = !this.device.status
    }

    facilityChange(value) {
        this.selectedFacility = value;
        this.selectedFacility.organizationCode = value.organizationCode;
        this.centers = value.centers;
        if (this.centers[0].id !== "ALL") {
            let allCenter = new Center();
            allCenter.name = "ALL";
            allCenter.id = "ALL";
            this.centers.splice(0, 0, allCenter);
        }
        this.selectedCenter.id = this.centers[0].id;
        this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
    }

    pageOne(type){    
        if(type == 'UNASSIGNED')   {
            if(this.searchString.length >= 3){
                this.pageNo=1;
                this.visiblePaginationUnassigned = false;       
                setTimeout(() => this.visiblePaginationUnassigned = true, 0);
            }
        } else{
            if(this.searchString.length >= 3){
                this.pageNo=1;
                this.visiblePaginationAssigned = false;       
                setTimeout(() => this.visiblePaginationAssigned = true, 0); 
            }                       
        }         
    }

    pageTab(type){    
        if(type == 'UNASSIGNED')   {         
                this.pageNo=1;
                this.visiblePaginationUnassigned = false;       
                setTimeout(() => this.visiblePaginationUnassigned = true, 0);            
        } else{            
            this.pageNo=1;
            this.visiblePaginationAssigned = false;       
            setTimeout(() => this.visiblePaginationAssigned = true, 0);                                    
        }         
    }
    
}