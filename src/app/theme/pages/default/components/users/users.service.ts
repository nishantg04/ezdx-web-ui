import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { Helpers } from '../../../../../helpers';

import 'rxjs/add/operator/toPromise';

import { User } from './user';
import { UserChild } from './userChild';
import { environment } from '../../../../../../environments/environment'
import { Password } from './password';

@Injectable()
export class UsersService {

    private headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private headers2 = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token, 'api_key': 'secretkey' });
    private serviceUrl2 = environment.BaseURL + environment.UserCommandURL + "/register"; //'http://dev.ezdx.healthcubed.com/ezdx-user-command/api/v1/users/register';  // URL to web api
    private serviceUrl3 = environment.BaseURL + environment.UserCommandURL; // 'http://dev.ezdx.healthcubed.com/ezdx-user-command/api/v1/users';  // URL to web api

    private serviceUrl = environment.BaseURL + environment.UserQueryURL;// 'http://dev.ezdx.healthcubed.com/ezdx-user-query/api/v1/users';

    private patientCountbyUser = environment.BaseURL + environment.PatientCountbyUserURL;//"http://dev.ezdx.healthcubed.com/ezdx-patient-query/api/v1/patients/count/date";
    private patientCountbyGenderAge = environment.BaseURL + environment.PatientCountbyUserAgeGenderURL; //"http://dev.ezdx.healthcubed.com/ezdx-patient-query/api/v1/patients/stats/";

    private dignosticCount = environment.BaseURL + environment.DignosticCountbyUser;

    private dignosticTestbyUserID = environment.BaseURL + environment.DignosticTestbyUser;

    private dignosticTestType = environment.BaseURL + environment.DignosticTestTypeOfUser;

    private DeviceHistoryUrl = environment.BaseURL + environment.DeviceHistory

    private changePassword = environment.BaseURL + "ezdx-user-command/api/v1/users/self/password";

    private generatePassword = environment.BaseURL + environment.GeneratePassword;

    private userUrl = environment.BaseURL + environment.UserCommandURL; //"http://dev.ezdx.healthcubed.com/ezdx-user-command/api/v1/users/"

    private multipartHeader = new Headers({ 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });

    private cidaas_secret_key = "4749858986000300743";

    constructor(private http: Http, private _router: Router, private _script: ScriptLoaderService) { }

    getUsers(): Promise<User[]> {
        return this.http.get(this.serviceUrl + "/?page=1&limit=1000", { headers: this.headers })
            .toPromise()
            .then(response => { return response.json().users as User[] })
            .catch(this.handleError.bind(this));
    }
    getUsersFiltered(fromDate, toDate, searchcriteria, id, pageno, size, searchString, sortby): Promise<any> {
        //  var getUrl = this.serviceUrl + "/search?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate;
        var getUrl = this.serviceUrl + "/search?searchcriteria=ALL&page=" + pageno + "&limit=" + size + "&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/search?searchcriteria=FACILITY&page=" + pageno + "&limit=" + size + "&facility=" + id + "&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/search?searchcriteria=CENTER&page=" + pageno + "&limit=" + size + "&center=" + id + "&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria === "NAME") {
            getUrl = this.serviceUrl + "/search?searchcriteria=NAME&sort=" + sortby + "&page=" + pageno + "&limit=" + size + "&name=" + searchString + "&from=" + fromDate + "&to=" + toDate;
        }
        if (searchcriteria === "EMAIL") {
            getUrl = this.serviceUrl + "/search?searchcriteria=EMAIL&sort=" + sortby + "&page=" + pageno + "&limit=" + size + "&email=" + searchString + "&from=" + fromDate + "&to=" + toDate;
        }
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any : null })
            .catch(this.handleError.bind(this));
    }

    getSortedUser(){
        var getUrl = this.serviceUrl + "/search?searchcriteria=ALL&sort=" + 'acs&order=displayName'  + "&page=" + 1 + "&limit=" + 0  + "&from=" + 0 + "&to=" + 0;
        return this.http.get(getUrl, { headers: this.headers })
        .toPromise()
        .then(response => { return (response.status === 200) ? response.json() as any : null })
        .catch(this.handleError.bind(this));
    }

    userExistsSearch(type, searchString) {
        var getUrl = this.serviceUrl + "/search?searchcriteria=FIELDS&field=" + type + '&value=' + searchString
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any : null })
            .catch(this.handleError.bind(this));
    }

    getFaciltyUserSearch(fromDate, toDate, searchcriteria, id, pageno, size, searchString, sortby, searchby) {
        var getUrl = this.serviceUrl + "/search/"
        if (searchby == 'FACILITY') {
            if (searchcriteria == 'NAME') {
                var url = getUrl + "facility/" + id + "?searchcriteria=NAME&sort=" + sortby + "&name=" + searchString + "&from=" + fromDate + "&to=" + toDate;
            }
            if (searchcriteria == 'EMAIL') {
                var url = getUrl + "facility/" + id + "?searchcriteria=EMAIL&sort=" + sortby + "&email=" + searchString + "&from=" + fromDate + "&to=" + toDate;
            }
        }
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any : null })
            .catch(this.handleError.bind(this));

    }

    getCenterUserSearch(fromDate, toDate, searchcriteria, id, pageno, size, searchString, sortby, searchby) {
        var getUrl = this.serviceUrl + "/search/"
        if (searchby == 'CENTER') {
            if (searchcriteria == 'NAME') {
                var url = getUrl + "center/" + id + "?searchcriteria=NAME&sort=" + sortby + "&name=" + searchString + "&from=" + fromDate + "&to=" + toDate;
            }
            if (searchcriteria == 'EMAIL') {
                var url = getUrl + "center/" + id + "?searchcriteria=EMAIL&sort=" + sortby + "&email=" + searchString + "&from=" + fromDate + "&to=" + toDate;
            }
        }
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() as any : null })
            .catch(this.handleError.bind(this));
    }

    getCurrentUser(): Promise<UserChild> {
        return this.http.get(this.serviceUrl + "/self", { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as UserChild })
            .catch(this.handleError.bind(this));
    }


    getUser(id: string): Promise<UserChild> {
        const url = '${this.serviceUrl}/code/${id}';
        return this.http.get(this.serviceUrl + "/" + id, { headers: this.headers })
            .toPromise()
            .then(response => {
                // console.log('service check', response.json());
                return response.json() as UserChild
            })
            .catch(this.handleError.bind(this));
    }

    getUserCountDashboard(from, to, search, id): Promise<any> {
        var url = this.serviceUrl + "/count?searchcriteria=ALL&from=" + from + "&to=" + to;
        if (search === "FACILITY")
            url = this.serviceUrl + "/count?searchcriteria=FACILITY&from=" + from + "&to=" + to + "&facilityId=" + id;
        if (search === "CENTER")
            url = this.serviceUrl + "/count?searchcriteria=CENTER&from=" + from + "&to=" + to + "&centerId=" + id;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    getUserBulk(ids: string[]): Promise<any> {
        return this.http
            .post(this.serviceUrl + "/id/bulk", JSON.stringify(ids), { headers: this.headers })
            .toPromise()
            .then(res => res.json() as any[])
            .catch(this.handleError.bind(this));
    }

    getUserPatientCount(id): Promise<any> {
        var url = this.patientCountbyUser + "?searchcriteria=USER&user=" + id;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    getUserPatientCountFilter(id, from, to): Promise<any> {
        var url = this.patientCountbyUser + "?searchcriteria=USER&user=" + id + "&from=" + from + "&to=" + to;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    getUserPatientbyGender(id): Promise<any> {
        var url = this.patientCountbyGenderAge + "gender?searchcriteria=USER&user=" + id;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    getUserPatientbyAge(id): Promise<any> {
        var url = this.patientCountbyGenderAge + "age?searchcriteria=USER&user=" + id;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    getUserPatientbyGenderFilter(id,from,to): Promise<any> {
        var url = this.patientCountbyGenderAge + "gender?searchcriteria=USER&user=" + id+ "&from=" + from + "&to=" + to;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    getUserPatientbyAgeFilter(id,from,to): Promise<any> {
        var url = this.patientCountbyGenderAge + "age?searchcriteria=USER&user=" + id+ "&from=" + from + "&to=" + to;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    getDignosticbyUser(id): Promise<any> {
        var url = this.dignosticCount + id;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    getDignosticbyUserFilter(id, from, to): Promise<any> {
        var url = this.dignosticCount + id + "?from=" + from + "&to=" + to;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    getDignosticTestbyUser(from, to, id): Promise<any> {
        var url = this.dignosticTestbyUserID + "?searchcriteria=USER&userid=" + id + "&from=" + from + "&to=" + to;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    getDignosticTestTypebyUser(from, to, id, type): Promise<any> {
        var url = this.dignosticTestType + "/" + id + "?testType=" + type + "&from=" + from + "&to=" + to;;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    delete(id): Promise<void> {
        const url = `${this.serviceUrl3}/${id}`;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(() => null)
            .catch(this.handleError.bind(this));
    }

    create(facility: User): Promise<User> {
        return this.http
            .post(this.serviceUrl2, JSON.stringify(facility), { headers: this.headers2 })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError.bind(this));
    }

    update(user: User): Promise<User> {
        const url = `${this.serviceUrl}/${user.user.id}`;
        // console.log("user service", user)
        return this.http
            .put(this.serviceUrl3 + "/" + user.user.id, JSON.stringify(user.user), { headers: this.headers2 })
            .toPromise()
            .then(() => user)
            .catch(this.handleError.bind(this));
    }


    changeUserPassword(pass: Password): Promise<Password> {
        return this.http
            .put(this.changePassword, JSON.stringify(pass), { headers: this.headers2 })
            .toPromise()
            .then(() => pass)
            .catch(this.handleError);
    }


    updateTestMap(operation: any, userId: any, testIds: any[]): Promise<User> {
        return this.http
            .put(this.serviceUrl3 + "/" + userId + "/testmapping?operation=" + operation, JSON.stringify(testIds), { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    uploadImage(id, data): Promise<any> {
        return this.http.put(this.userUrl + '/' + id + '/profilepicture', data, { headers: this.multipartHeader })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getDeviceHistoryByUser(id): Promise<any> {
        var url = this.DeviceHistoryUrl + id;
        return this.http.get(url, { headers: this.multipartHeader })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getPassword() {
        var url = this.generatePassword + '?minLength=' + 7 + '&maxLength=' + 7
        return this.http.get(url, { headers: this.multipartHeader })
            .toPromise()
            .then(response => {
                // console.log("res: ", response)
                return response['_body'];
            })
            .catch(this.handleError.bind(this));
    }

    signatureUpload(id, data) {
        var url = this.userUrl + '/' + id + '/signature'
        return this.http.put(url, data, { headers: this.multipartHeader })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load('body',
                'assets/demo/default/custom/components/utils/redirect-cidaas-logout.js');
        }
        //alert('An error occurred:' + error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}