import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { DefaultComponent } from "../../../default.component";
import { IpanReportsComponent } from "./ipan-reports.component";
import { LayoutModule } from "../../../../../layouts/layout.module";
import { ReportDataService } from "../report-data.service";
import { DataTableModule, PaginatorModule, DialogModule, DropdownModule } from "primeng/primeng";
import { PdfViewerModule } from "ng2-pdf-viewer";

const routes: Routes = [
    {
        path: "",
        component: DefaultComponent,
        children: [
            {
                path: "",
                component: IpanReportsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        LayoutModule,
        DataTableModule,
        PaginatorModule,
        DialogModule,
        TranslateModule.forChild({ isolate: false }),
        PdfViewerModule
    ],
    exports: [RouterModule],
    declarations: [IpanReportsComponent],
    providers: [TranslateService, ReportDataService]
})
export class IpanReportsModule { }
