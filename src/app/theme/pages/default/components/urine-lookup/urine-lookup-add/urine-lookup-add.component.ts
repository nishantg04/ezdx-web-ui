import {
    Component,
    OnInit,
    ViewEncapsulation,
    AfterViewInit
} from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Helpers } from "../../../../../../helpers";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";
import { UrineService } from "../urine.service";
import { Center } from "../../facilities/center";
import { Facility } from "../../facilities/facility";
import { FacilitiesService } from "../../facilities/facilities.service";
import { Observable } from "rxjs/Observable";
import { UrineLookupJson } from "../urineLookup.entity";

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./urine-lookup-add.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class UrineLookupAddComponent implements OnInit {
    actionProcess: string;
    hasUnderscore: boolean;
    urineLookup: any;
    ULID: string;
    urineLookupJson: UrineLookupJson;
    textBody: boolean = false;
    textTemplate: boolean = true;

    constructor(
        private _urineService: UrineService,
        private _router: Router,
        private _activeroute: ActivatedRoute
    ) { }

    ngOnInit() {
        this.ULID = this._activeroute.snapshot.queryParams["lookupId"];
        // console.log("ULID: ", this.ULID);
        this.urineLookupJson = new UrineLookupJson();
        this.getRdtLookup();
    }

    ngAfterViewInit() { }

    getRdtLookup() {
        if (this.ULID == 'new') {
            this._urineService.getDefaultLookup().then(res => {
                this._urineService.downloadFile(res.files[0].id).then(resDownload => {
                    // console.log("json file downloaded: ", res);
                    this.urineLookup = resDownload;
                });
            });
        } else {
            this.textTemplate = false;
            this._urineService.getUrineLookupById(this.ULID).then(res => {
                this.urineLookupJson.fileName = res.filename;
                this.urineLookupJson.centerId = res.centerId;
                this.urineLookupJson.facilityId = res.facilityId;
                this.urineLookupJson.deviceId = res.deviceId;
                this._urineService.downloadFile(this.ULID).then(resDownload => {
                    // console.log("json file downloaded: ", resDownload);
                    this.urineLookup = resDownload;
                });
            });
        }
    }

    pushToServer() {
        this.urineLookupJson.urineLookups = this.urineLookup;
        this._urineService.createRdtJson(this.urineLookupJson).then(res => {
            // console.log("res: ", res);
            Helpers.setLoading(false);
            this._router.navigate(['rdt/list'], { queryParams: { type: 'UL' } });
        });
    }

    update() {
        this.urineLookupJson.urineLookups = this.urineLookup;
        this._urineService.updateUrineJson(this.urineLookupJson, this.ULID)
            .then(res => {
                this._router.navigate(['rdt/list'], { queryParams: { type: 'UL' } });
            });
    }

    onKeyUpEvent() {
        if (RegExp(/_/).test(this.urineLookupJson.fileName)) {
            this.hasUnderscore = true;
        } else {
            this.hasUnderscore = false;
        }
    }
}
