import {
    Component,
    OnInit,
    ViewEncapsulation,
    AfterViewInit,
    NgZone,
    Input
} from "@angular/core";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { PatientsService } from "../patients.service";
import { VisitsService } from "../../visits/visits.service";
import { Visit } from "../../ipan/visit.model";

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./patients-history.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class PatientsHistoryComponent implements OnInit {

    patientData: any;
    imageUrl: String;
    showImage: boolean = false;
    visitsCount: number = 0;
    visits: any[];
    limit: number = 10;
    pageNo: number = 1;
    patientId: string;
    orderOn: string | string[] = 'createTime';

    constructor(private _router: Router, private _activeroute: ActivatedRoute, private visitsService: VisitsService, private patientService: PatientsService) { }

    ngOnInit() {
        this.patientId = this._activeroute.snapshot.queryParams['patientId'];
        //console.log('patient ID: ', this.patientId);
        this.getPatientDetails();
        this.getPatientsVisitDetails();
    }

    ngAfterViewInit() { }

    onShowImage(ImageUrl) {
        //console.log('image Url: ', ImageUrl);
        this.imageUrl = ImageUrl;
        this.showImage = true;
    }

    getPatientsVisitDetails() {
        this.visitsService.getVisitByPatientId(this.patientId, this.pageNo, this.limit).then(res => {
            //console.log('visits: ', res);
            this.visits = res.visits;
            this.visitsCount = res.count;
            //console.log('visits',this.visits, ' counts: ', this.visitsCount);
        },
            rej => {

            });
    }

    getPatientDetails() {
        this.patientService.getPatientById(this.patientId).then(res => {
            //console.log('res: ', res);
            this.patientData = res;
        },
            req => {

            })
    }

    paginate(event) {
        this.pageNo = event.page + 1;
        this.limit = parseInt(event.rows);
        this.getPatientsVisitDetails();
    }

    navigateToDetails(visit: Visit) {
        localStorage.setItem('visit', JSON.stringify(visit));
        this._router.navigate(['/visit/logdetail']);
    }
}
