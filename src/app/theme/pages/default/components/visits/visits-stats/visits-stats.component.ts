import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';

import { Visit } from '../visit';
import { Test } from '../test';
import { VisitsService } from '../visits.service';
import { Facility } from '../../facilities/facility';
import { FacilitiesService } from '../../facilities/facilities.service';
import { UsersService } from '../../users/users.service';
import { Center } from '../../facilities/center';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./visits-stats.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class VisitsStatsComponent implements OnInit, AfterViewInit {
    userType: string;
    userInfo: any;
    visits: any[];
    tests: any[];
    facilities: any[];
    selectedFacility: Facility;
    selectedCenter: Center;
    centers: Center[];
    years: any[];
    selectedYear: any;
    fromDate: string;
    toDate: string;
    facilityId: string;
    centerId: string;
    userBulkList: any[];
    testStats: any[];
    testTypeList: any[];
    pageNo: number;
    size: number;
    visitCount: number;
    constructor(private _visitsService: VisitsService, private _script: ScriptLoaderService, private _router: Router, private _activeroute: ActivatedRoute, private _facilitiesService: FacilitiesService, private _usersService: UsersService, private _ngZone: NgZone) {
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        if (window.localStorage.getItem('userType') == 'Facility')
            this.userType = 'Facility';
        else
            this.userType = 'Admin';

        this.testTypeList = [
            { type: "PHYSICAL", name: "Physical", tests: [{ name: "Blood Pressure", type: "BLOOD_PRESSURE" }, { name: "Pulse Oximeter", type: "PULSE_OXIMETER" }, { name: "Temperature", type: "TEMPERATURE" }, { name: "Height", type: "HEIGHT" }, { name: "Weight", type: "WEIGHT" }, { name: "BMI", type: "BMI" }, { name: "ECG", type: "ECG" }, { name: "Mid Arm Circumference", type: "MID_ARM_CIRCUMFERENCE" }] },
            { type: "WHOLE_BLOOD_POCT", name: "Whole Blood-POCT", tests: [{ name: "Blood Grouping", type: "BLOOD_GROUPING" },{ name: "Hemoglobin", type: "HEMOGLOBIN" }, { name: "Cholesterol", type: "CHOLESTEROL" },{ name: "Glucose", type: "BLOOD_GLUCOSE" }, { name: "Uric Acid", type: "URIC_ACID" }] },
          //  { type: "WHOLE_BLOOD_POCT", name: "Whole Blood", tests: [{ name: "Blood Grouping", type: "BLOOD_GROUPING" }] },
            { type: "WHOLE_BLOOD_RDT", name: "Whole Blood-RDT", tests: [{ name: "Malaria", type: "MALARIA" }, { name: "Dengue", type: "DENGUE" }, { name: "Typhoid", type: "TYPHOID" }, { name: "Hep-B", type: "HEP_B" }, { name: "Hep-C", type: "HEP_C" }, { name: "HIV", type: "HIV" }, { name: "Chikungunya", type: "CHIKUNGUNYA" }, { name: "Syphilis", type: "SYPHILIS" }, { name: "Troponin-I", type: "TROPONIN_1" }] },
            { type: "URINE_POCT", name: "Urine-POCT", tests: [{ name: "Pregnancy", type: "PREGNANCY" }] },
            { type: "URINE_RDT", name: "Urine-RDT", tests: [{ name: "Urine-2P", type: "URINE" }, { name: "Leukocytes", type: "LEUKOCYTES" }, { name: "Urobilinogen", type: "UROBILINOGEN" }, { name: "Nitrite", type: "NITRITE" }, { name: "pH", type: "PH" }, { name: "Ketone", type: "KETONE" }, { name: "Blood", type: "BLOOD" }, { name: "Bilurubin", type: "BILIRUBIN" }, { name: "Specific Gravity", type: "SPECIFIC_GRAVITY" }] }
        ];
    }
    ngOnInit() {
        this.pageNo = 1;
        this.size = 10000;
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
        window.my.namespace.navFacilityDetails = this.navFacilityDetails.bind(this);
        window.my.namespace.navCenterDetails = this.navCenterDetails.bind(this);
    }
    ngAfterViewInit() {
        window.localStorage.removeItem('patientStartDate');
        window.localStorage.removeItem('patientEndDate');
        this.fromDate = this._activeroute.snapshot.queryParams['from'];
        this.toDate = this._activeroute.snapshot.queryParams['to'];
        this.selectedFacility.id = this._activeroute.snapshot.queryParams['facilityId'];
        this.selectedCenter.id = this._activeroute.snapshot.queryParams['centerId'];
        if (this.fromDate && this.toDate) {
            window.localStorage.setItem('patientStartDate', this.fromDate);
            window.localStorage.setItem('patientEndDate', this.toDate);

        }
        this.getFacilities();
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/datatables/base/date.patients.js');
        // var facilitiesJSON = '[{"name":"Darshan Hospital","email":"admin@darshan.co.in"}]';
        // localStorage.setItem('facilityTable', facilitiesJSON);
        // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
        //     'assets/demo/default/custom/components/datatables/base/data-local.1.js');
    }

    onFilterClick(): void {
        if (this.selectedFacility.id != "ALL") {
            if (this.selectedCenter.id != "ALL") {
                this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedFacility.id, this.selectedCenter.id);
            }
            else {
                this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "FACILITY", this.selectedFacility.id, 0);
            }
        }
        else {
            var endDate = new Date();
            endDate.setDate(endDate.getDate() + 1);
            endDate.setHours(23);
            endDate.setMinutes(23);
            endDate.setSeconds(23);
            endDate.setMilliseconds(0);
            this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0, 0);
        }
    }

    getFacilities(): void {

        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            } else if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - 29);
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
        startDate.setMilliseconds(0);
        var endDate = new Date();
        //endDate.setDate(endDate.getDate() + 1);
        endDate.setHours(23);
        endDate.setMinutes(23);
        endDate.setSeconds(23);
        endDate.setMilliseconds(0);
        if (!this.fromDate && !this.toDate) {
            this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
            if (window.localStorage.getItem('userType') == 'Facility') {
                this.selectedFacility.id = this.userInfo.facilityId;
                this.facilities.forEach(fclt => {
                    if (fclt.id == this.userInfo.facilityId) {
                        this.centers = jQuery.extend(true, [], fclt.centers);
                        let allCenter = new Center();
                        allCenter.name = "ALL";
                        allCenter.id = "ALL";
                        this.centers.splice(0, 0, allCenter);
                        this.selectedCenter.id = this.centers[0].id;
                    }
                });
            }
            else {
                // let allFacility = new Facility();
                // allFacility.name = "ALL";
                // allFacility.id = "ALL";
                // this.facilities.splice(0, 0, allFacility);
                this.selectedFacility.id = this.facilities[0].id;
                if (this.facilities[0].centers !== undefined) {
                    this.centers = this.facilities[0].centers;
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                    this.selectedCenter.id = this.centers[0].id;
                }
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        }
        else {
            // let allFacility = new Facility();
            // allFacility.name = "ALL";
            // allFacility.id = "ALL";
            // this.facilities.splice(0, 0, allFacility);
            // if (this.selectedFacility.id != "ALL") {
            //     this.facilities.forEach(fac => {
            //         if (fac.id === this.selectedFacility.id) {
            //             this.centers = fac.centers;
            //             let allCenter = new Center();
            //             allCenter.name = "ALL";
            //             allCenter.id = "ALL";
            //             this.centers.splice(0, 0, allCenter);
            //         }
            //     });
            // }
            this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
            if (window.localStorage.getItem('userType') == 'Facility') {
                let allFacilities = jQuery.extend(true, [], this.facilities);
                this.facilities = [];
                allFacilities.forEach(fclt => {
                    if (fclt.id == this.userInfo.facilityId) {
                        this.facilities.push(fclt);
                    }
                });
            }
        }
        this.onFilterClick();
        let searchcriteria = "ALL";
        if (!this._activeroute.snapshot.queryParams['from'] && !this._activeroute.snapshot.queryParams['to']) {
            if (window.localStorage.getItem('userType') === "Facility") {
                searchcriteria = "FACILITY";
                this.list(new Date(localStorage.getItem('patientStartDate')).getTime(), new Date(localStorage.getItem('patientEndDate')).getTime(), searchcriteria, this.selectedFacility.id, 0);
            }
            else {
                this.list(new Date(localStorage.getItem('patientStartDate')).getTime(), new Date(localStorage.getItem('patientEndDate')).getTime(), searchcriteria, 0, 0);
            }
        }
    }

    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers[0].id !== "ALL") {
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                }
                this.selectedCenter.id = this.centers[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }

    goToFacilityDetails(facilityId) {
        this._router.navigate(['/facilities/details'], { queryParams: { facilityId: facilityId } });
    }



    navCenterDetails(centerId, facilityId) {
        this._ngZone.run(() => this.goToCenterDetails(centerId, facilityId));
    }

    navFacilityDetails(facilityId) {
        this._ngZone.run(() => this.goToFacilityDetails(facilityId));
    }

    goToCenterDetails(centerId, facilityId) {
        this._router.navigate(['/center/details'], { queryParams: { center: String(centerId), facility: String(facilityId) } });

    }

    generateArray(obj) {
        return Object.keys(obj).map((key) => { return { key: key, value: obj[key] } });
    }
    list(fromDate, toDate, searchcriteria, facilityid, centerId): void {
        Helpers.setLoading(true);
        this.tests = [];
        this._visitsService.getDiagnosisDate(fromDate, toDate, searchcriteria, facilityid, centerId, this.pageNo, this.size)
            .then(response => {
                // console.log(response); 
                this.visits = response.results; this.visitCount = response.totalHits
            })
            .then(() => {
                var userIds = [];
                this.visits.forEach((val, i) => {
                    userIds.push(val.test.visitId);
                    val.imei = (val.imei !== null) ? val.imei : 'N/A'
                    val.serialNumber = (val.serialNumber !== null) ? val.serialNumber : 'N/A'
                    var newObject = val;
                    var test = val.test;
                    test.visitId = val.test.visitId;
                    var d = new Date(test.startTime),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                    test.startTime = [day, month, year].join('-') + ' (' + d.getHours() + ':' + d.getMinutes() + ')';
                    var d1 = new Date(test.endTime),
                        month = '' + (d1.getMonth() + 1),
                        day = '' + d1.getDate(),
                        year = d1.getFullYear();

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                    newObject.endTime = [day, month, year].join('-') + ' (' + d.getHours() + ':' + d.getMinutes() + ')';
                    newObject.endTimeDate = d;


                    var dia_result = '';

                    if (test.name == "TEMPERATURE") { dia_result = test.result + ' ' + (test.unit ? test.unit : '') }
                    else if (test.name == "BMI") { dia_result = test.bmiValue }
                    else if (test.name == "PULSE_OXIMETER") { dia_result = 'SPO: ' + test.spoValue + ' ' + test.spoUnit + '<br/>' + ' Pulse: ' + test.pulseRate + ' ' + test.pulseRateUnit }
                    else if (test.name == "BLOOD_PRESSURE") {
                        dia_result = 'DIA: ' + test.dia + ' ' + test.unit + '</br>' + ' SYS: ' + test.sys + ' ' + test.unit;
                    }
                    else if (test.name === "CHIKUNGUNYA" || test.name === "PREGNANCY" || test.name === "MALARIA" || test.name === "HIV") {
                        dia_result = 'Res : ' + test.rdtResult;
                    }
                    else if (test.name === "HEMOGLOBIN") {
                        dia_result = 'Count: ' + test.result + ' ' + test.unit;
                    } else if (test.name === "BLOOD_GLUCOSE") {
                        dia_result = 'Type: ' + test.dietType + '<br/>' + 'Count: ' + test.result + ' ' + test.unit;
                    }
                    else { dia_result = test.result + ' ' + test.unit }
                    newObject.diaResults = dia_result;

                    newObject.type = test.name;
                    if (newObject.type.includes('_'))
                        newObject.type = newObject.type.replace('_', ' ')
                    //newObject.test = test;
                    this.tests.push(newObject);
                });
                // this._usersService.getUserBulk(userIds)
                //     .then(users => this.userBulkList = users)
                //     .then(() => {
                //         this.userBulkList = this.generateArray(this.userBulkList);
                //         this.tests.forEach(test => {
                //             this.userBulkList.forEach((user) => {
                //                 if (user.key.toString() === test.test.userId)
                //                     test.userEmail = user.value;
                //             });
                //         });
                //         localStorage.setItem('testTable', JSON.stringify(this.tests));
                //         this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                //             'assets/demo/default/custom/components/datatables/base/data-local.tests.js');
                //     });
                this.testStats = [];
                // this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
                let allFacilities = jQuery.extend(true, [], this.facilities);
                allFacilities.forEach(fclt => {
                    var facility = {
                        id: fclt.id,
                        name: fclt.name,
                        count: 0,
                        centers: fclt.centers,
                        facilityId: fclt.organizationCode
                    }
                    this.tests.forEach(visit => {
                        if (visit.test.organizationId === facility.id) {
                            if (facility.centers) {
                                facility.centers.forEach(cntr => {
                                    cntr.facilityId = facility.facilityId;
                                    if (!cntr.count)
                                        cntr.count = 0;
                                    if (visit.test.centerId === cntr.id) {
                                        cntr.count += 1;
                                        facility.count += 1;
                                        if (!cntr.tests) {
                                            cntr.tests = [];
                                            this.testTypeList.forEach(type => {
                                                var testType = {
                                                    id: cntr.id + "_" + type.type,
                                                    type: type.type,
                                                    name: type.name,
                                                    tests: type.tests,
                                                    count: 0
                                                }
                                                testType.tests.forEach(test => {
                                                    test.id = cntr.id + "_" + test.type;
                                                    test.count = 0;
                                                });
                                                cntr.tests.push(jQuery.extend(true, {}, testType));
                                            });
                                        }
                                        cntr.tests.forEach(testType => {
                                            if (testType.type === visit.test.type) {
                                                // console.log(testType.type + '-' + visit.test.type)
                                                testType.count += 1;
                                                testType.tests.forEach(test => {
                                                    if (test.type.toLowerCase() === visit.test.name.toLowerCase()) {
                                                        test.count += 1;
                                                    }
                                                });
                                            }

                                        });
                                    }
                                });
                            }
                        }
                    });
                    if (facility.centers && facility.centers.length > 0) {
                        facility.centers.sort((a, b) => b.count - a.count);
                        facility.centers.forEach(cntr => {
                            if (!cntr.count)
                                cntr.count = 0;
                            if (cntr.tests && cntr.tests.length > 0) {
                                cntr.tests.sort((a, b) => b.count - a.count);
                                cntr.tests.forEach(tst => {
                                    if (tst.tests && tst.tests.length > 0)
                                        tst.tests.sort((a, b) => b.count - a.count);
                                });
                            }
                        });
                    }
                    this.testStats.push(facility);
                });
                // console.log(this.testStats)
                this.testStats.sort((a, b) => b.count - a.count);
                localStorage.setItem('diagnosisStatsTable', JSON.stringify(this.testStats));
                this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                    'assets/demo/default/custom/components/datatables/child/data-local.tests.js');
                Helpers.setLoading(false);

            });
    }

}