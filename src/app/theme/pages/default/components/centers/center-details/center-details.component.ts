import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { UsersService } from '../../users/users.service';
import { PatientsService } from '../../patients/patients.service';
import { UserChild } from '../../users/userChild';
import { Facility } from '../../facilities/facility';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DevicesService } from '../../devices/devices.service';
import { VisitsService } from '../../visits/visits.service';
import { TestsReportService } from '../../home/tests-report/tests-report.service';
import { Center } from '../../facilities/center';
import { Visit } from '../../visits/visit';
import { Test } from '../../visits/test';
import * as uuid from 'uuid';
import { CenterReport } from '../center-report';
import { error } from 'util';
import { setTimeout } from 'timers';
import { fail } from 'assert';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./center-details.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CenterDetailsComponent implements OnInit {
    centerId: any;
    facilityId: any;
    facilityCode: any;
    reportData: CenterReport;
    user: UserChild;
    patientCount: any;
    deviceCount: any;
    visitCount: number;
    testCount: number;
    visitCountTotal: number;
    testCountTotal: number;
    testList: any;
    userType: string;
    totalVists: any;
    totalTest: any;
    facilities: Facility[];
    centers: Center[];
    selectedFacility: Facility;
    selectedCenter: Center;
    userInfo: any;
    visitsCount: any;
    selectedTestType: any;
    selectedTestList: any;
    userCount: any;
    testsCount: any;
    patientCountByWeek: any[];
    years: any[];
    selectedYear: any;
    testCountByWeek: any[];
    hideDignostic: boolean = false;
    hidePatient: boolean = false;
    types: any;
    showPdfModel: boolean = false;
    objectURL: any;
    pdfHead: string;
    femalePercent: string;
    msg: boolean = false;
    graphValues: any[] = [];
    patientChart: any[] = [];
    testChartValue: any[] = [];
    showDigLoad: boolean = false;
    showPatLoad: boolean = false;
    hideLink: boolean = false;

    constructor(private _ngZone: NgZone, private _script: ScriptLoaderService, private _router: Router, private _activeroute: ActivatedRoute, private _testsReportService: TestsReportService,
        private _visitsService: VisitsService, private _patientsService: PatientsService, private _facilitiesService: FacilitiesService,
        private _devicesService: DevicesService, private _usersService: UsersService) {
        let currentYear = new Date().getFullYear();
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        this.reportData = new CenterReport();
        // this.tests = [];
        this.years = [];
        this.years.push("ALL");
        for (var index = 2014; index <= currentYear; index++) {
            this.years.push(index);
        }
        this.selectedYear = this.years[this.years.length - 1];
        this.testList = [
            { type: "physical", name: "Physical", color: '#71bf44', tests: [{ name: "Blood Pressure", type: "BLOOD_PRESSURE" }, { name: "Pulse Oximeter", type: "PULSE_OXIMETER" }, { name: "%O2 Saturation", type: "%O2 Saturation" }, { name: "Temperature", type: "TEMPERATURE" }, { name: "Height", type: "Height" }, { name: "Weight", type: "Weight" }, { name: "BMI", type: "BMI" }, { name: "ECG", type: "ECG" }, { name: "Mid Arm Circumference", type: "Mid Arm Circumference" }] },
            { type: "whole_blood_poct", color: '#21C4DF', name: "Whole Blood-POCT", tests: [{ name: "Glucose", type: "BLOOD_GLUCOSE" }, { name: "Hemoglobin", type: "HEAMOGLOBIN" }, { name: "Cholesterol", type: "CHOLESTEROL" }, { name: "Uric Acid", type: "URIC_ACID" }] },
            { type: "whole_blood", name: "Whole Blood", color: '#02abcb', tests: [{ name: "Blood Grouping", type: "BLOOD_GROUPING" }] },
            { type: "whole_blood_rdt", name: "Whole Blood-RDT", color: '#465EEB', tests: [{ name: "Malaria", type: "Malaria" }, { name: "Dengue", type: "Dengue" }, { name: "Typhoid", type: "Typhoid" }, { name: "Hep-B", type: "Hep-B" }, { name: "Hep-C", type: "Hep-C" }, { name: "HIV", type: "HIV" }, { name: "Chikungunya", type: "Chikungunya" }, { name: "Syphilis", type: "Syphilis" }, { name: "Troponin-I", type: "Troponin-I" }] },
            { type: "urine_poct", name: "Urine-POCT", color: '#DA70D6', tests: [{ name: "Pregnancy", type: "Pregnancy" }] },
            { type: "urine_rdt", name: "Urine-RDT", color: '#B22222', tests: [{ name: "Urine Sugar", type: "Urine Sugar" }, { name: "Urine Protein", type: "Urine Protein" }, { name: "Leukocytes", type: "Leukocytes" }, { name: "Urobilinogen", type: "Urobilinogen" }, { name: "Nitrite", type: "Nitrite" }, { name: "pH", type: "pH" }, { name: "Ketone", type: "Ketone" }, { name: "Blood", type: "Blood" }, { name: "Bilurubin", type: "Bilurubin" }, { name: "Specific Gravity", type: "Specific Gravity" }] }
        ];
        this.types = [
            { label: 'Blood Pressure', value: 'BLOOD_PRESSURE' },
            { label: 'Blood Glucose Postprandial', value: 'BLOOD_GLUCOSE_NON_FASTING' },
            { label: 'Heamoglobin', value: 'HEAMOGLOBIN' },
            { label: 'Malaria', value: 'MALARIA' },
        ];
        this.selectedTestType = this.testList[0].type;
        this.selectedTestList = this.testList[0].tests;
    }

    ngOnInit() {
        this.centerId = this._activeroute.snapshot.queryParams['center'];
        this.facilityCode = this._activeroute.snapshot.queryParams['facility'];
        if (window.localStorage.getItem('userType') == 'Center') {
            this.hideLink = true;
        }
        this.getFacilities();
    }

    ngAfterViewInit() {
        Helpers.setLoading(true);

        this.user = JSON.parse(window.localStorage.getItem('userInfo'))
        if (this.user && this.user.userRoles) {
            let isAdmin = false;
            this.user.userRoles.forEach((role, i) => {
                if (role === "HC_ADMIN") {
                    isAdmin = true;
                }
                if (role === "HC_OPERATIONS") {
                    isAdmin = true;
                }
            });
            if (window.localStorage.getItem('userType') == 'Facility')
                this.userType = 'Facility';
            else
                this.userType = 'Admin';
        }
        else {
            this._router.navigate(['/login']);
        }
        Helpers.setLoading(false);

    }

    getFacilities() {
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        // console.log('fac', this.facilities)
        this.facilities.sort(function(a, b) { if (a.name > b.name) { return 1; } else if (a.name < b.name) { return -1; } return 0; });
        let allFacility = new Facility();
        allFacility.name = "ALL";
        allFacility.id = "ALL";
        this.facilities.splice(0, 0, allFacility);
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - 29);
        startDate.setHours(0); startDate.setMinutes(0); startDate.setSeconds(0); startDate.setMilliseconds(0);
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        endDate.setHours(23); endDate.setMinutes(23); endDate.setSeconds(23); endDate.setMilliseconds(0);
        let searchcriteria = "CENTER";
        this.facilities.forEach(facilty => {
            if (facilty.organizationCode == this.facilityCode) {
                this.selectedFacility = facilty;
                this.facilityId = facilty.id;
                // if(this.selectedFacility.logo)
                // this.reportData.logoUrl = this.selectedFacility.logo
            }
        })
        this.selectedFacility.centers.forEach(center => {
            if (center.id == this.centerId)
                this.selectedCenter = center
        });
        this.reportData.centerName = this.selectedCenter.name;
        if (this.facilityId) {
            this.getLogo()
            this.getTestCountByWeek(startDate, endDate, searchcriteria, this.facilityId, this.centerId);
            this.getPatientCountByWeek(startDate, endDate, searchcriteria, this.facilityId, this.centerId);
            this.getPatientCount(startDate, endDate, searchcriteria, this.facilityId, this.centerId);
            this.getNewDeviceCount(startDate, endDate, searchcriteria, this.facilityId, this.centerId);
            this.getNewVisitCount(startDate, endDate, searchcriteria, this.facilityId, this.centerId);
            this.getNewTestCount(startDate, endDate, searchcriteria, this.facilityId, this.centerId);
            this.getNewUserCount(startDate, endDate, searchcriteria, this.facilityId, this.centerId);
            this.getPatientFemaleCount(startDate, endDate, searchcriteria, this.facilityId, this.centerId);
            this.getWeeklyPatient();
            this.getWeeklyUser();
            this.getWeeklyTest();
            this.getTestReport();
            this.getReportWeek();
        }


    }

    getLogo() {
        Helpers.setLoading(true);
        this._facilitiesService.getFacility(this.facilityCode)
            .then(facility => {
                Helpers.setLoading(false);
                if (facility.logo) {
                    this.reportData.logoUrl = facility.logo
                }
            })
    }

    getPatientCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        Helpers.setLoading(true);
        this.patientCount = null;
        this._patientsService.getPatientCenterCount(0, new Date(endDate).getTime(), searchcriteria, facilityid, centerid)
            .then(count => this.patientCount = count)
            .then(() => {
                this.patientCount = this.patientCount.toString();
                this.reportData.beneficiariesTillDate = this.patientCount;
                Helpers.setLoading(false);
            });
    }

    getNewDeviceCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        Helpers.setLoading(true);
        this.deviceCount = null;
        this._devicesService.getNewDeviceCount(0, new Date(endDate).getTime(), searchcriteria, facilityid, centerid)
            .then(data => {
                if (data != null) {
                    this.deviceCount = data;
                    this.deviceCount = this.deviceCount.toString();
                } else {
                    this.deviceCount = 0;
                    this.deviceCount = this.deviceCount.toString();
                }
            })
            .then(() => {
                Helpers.setLoading(false);
            });
    }

    getNewVisitCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        this.visitsCount = null;
        Helpers.setLoading(true);
        this._visitsService.getVisitCounts(0, new Date(endDate).getTime(), searchcriteria, facilityid, centerid)
            .then(data => {
                // console.log('visit data', data)
                if (data != null) {
                    this.totalVists = data;
                    this.visitsCount = this.totalVists.count.toString();
                }
                else {
                    this.visitsCount = 0;
                    this.visitsCount = this.visitsCount.toString();
                }
            })
            .then(() => {
                Helpers.setLoading(false);
            });
    }

    getNewTestCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        this.testsCount = null;
        Helpers.setLoading(true);
        this._visitsService.getTestCounts(0, new Date(endDate).getTime(), searchcriteria, facilityid, centerid)
            .then(data => {
                if (data != null) {
                    this.totalTest = data
                    this.testsCount = this.totalTest.totalHits.toString();
                    this.reportData.diagnosticsTillDate = this.testsCount;
                }
                else {
                    this.testsCount = 0;
                    this.reportData.diagnosticsTillDate = 0;
                    this.testsCount = this.testsCount.toString();
                }

            })
            .then(() => {
                Helpers.setLoading(false);
            });
    }

    getNewUserCount(startDate, endDate, searchcriteria, facilityid, centerid): void {
        Helpers.setLoading(true);
        this._usersService.getUsersFiltered(0, new Date(endDate).getTime(), searchcriteria, centerid, 1, 10, '', 'asc')
            .then(users => {
                Helpers.setLoading(false);
                if (users !== null) {
                    this.userCount = users.count.toString();
                    // this.reportData.beneficiariesTillDate = users.count;
                }
                else {
                    // this.reportData.beneficiariesTillDate = 0;
                    this.userCount = 0;
                    this.userCount = this.userCount.toString();
                }
            })
    }


    getPatientCountByWeek(from, to, searchcriteria, facilityid, centerId): void {
        Helpers.setLoading(true);
        this.patientCountByWeek = [];
        this.patientChart = [];
        this.showPatLoad = true;
        this._patientsService.getPatientCountByTril(searchcriteria, 0, new Date(to).getTime(), facilityid, centerId)
            .then(count => this.patientCountByWeek = count)
            .then(() => {
                // console.log(' pateint chart data', this.patientCountByWeek)
                var patientCountByWeekForChart = [];

                var tempData;
                var dummy = [];

                var names_array_new = this.patientCountByWeek.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.id.week === b.id.week) {
                            dummy.push(a);
                            return dummy;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.forEach(element => {
                    for (var i = 0; i < dummy.length; i++) {
                        if (element.id.week == dummy[i].id.week) {
                            element.count = dummy[i].count + element.count
                        }
                    }
                })
                // names_array_new.sort(function(a, b) {
                //     if (a.id.week > b.id.week) {
                //         return 1;
                //     } else if (a.id.week < b.id.week) {
                //         return -1;
                //     }
                //     return 0;
                // });

                var dummyArray = [];

                names_array_new = names_array_new.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.id.year > b.id.year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);

                // names_array_new.sort(function(a, b) {
                //     if (a.id.week > b.id.week) { return 1; } else if (a.id.week < b.id.week) { return -1; }
                //     return 0;
                // });

                var currentYear = new Date().getFullYear()

                names_array_new.forEach(element => {
                    if (element.id.year == currentYear) {
                        element.id.week = parseInt(element.id.week) + 1;
                    }
                });


                // dummyArray.sort(function(a, b) {
                //     if (a.id.week > b.id.week) { return 1; } else if (a.id.week < b.id.week) { return -1; }
                //     return 0;
                // });

                dummyArray.forEach(data => {
                    if (data.id.year == currentYear) {
                        data.id.week = parseInt(data.id.week) + 1
                    }
                })

                names_array_new = names_array_new.concat(dummyArray)

                this.patientCountByWeek = names_array_new;

                this.patientCountByWeek.forEach(data => {
                    patientCountByWeekForChart.push({
                        "Patients": data.count, "Week": data.id.week, "Year": data.id.year, "Week_Year": data.id.week + "\n"
                            + '(' + data.id.year + ')'
                    });
                });

                if (patientCountByWeekForChart.length >= 10) {
                    document.getElementById('dignostic_charts_by_center').classList['value'] = "col-lg-12";
                    document.getElementById('patient_chart_by_center').classList['value'] = "col-lg-12";

                } else {
                    document.getElementById('dignostic_charts_by_center').classList['value'] = "col-lg-6 padlr5";
                    document.getElementById('patient_chart_by_center').classList['value'] = "col-lg-6 padlr5";
                }

                this.patientChart = patientCountByWeekForChart
                Helpers.setLoading(false);
                this.showPatLoad = false;
            });
    }


    getTestCountByWeek(from, to, searchcriteria, facilityid, centerId): void {
        Helpers.setLoading(true);
        this.testCountByWeek = [];
        this.testChartValue = [];
        this.showDigLoad = true;
        this._visitsService.getTestsCountByWeek(searchcriteria, 0, new Date(to).getTime(), facilityid, centerId)
            .then(count => this.testCountByWeek = count)
            .then(() => {
                var testCountByWeekForChart = [];
                this.testCountByWeek.forEach(data => {
                    var isWeekCreated = false;
                    testCountByWeekForChart.forEach(week => {
                        if (week.Week === data.week)
                            isWeekCreated = true;
                    });
                    if (!isWeekCreated) {
                        var objectToPush = {};
                        objectToPush['Week'] = data.week;
                        objectToPush[data.testType] = data.count;
                        objectToPush['Year'] = '20' + data.year;
                        objectToPush['Week_Year'] = data.week + "\n" + '(20' + data.year + ')';
                        testCountByWeekForChart.push(objectToPush);
                    }
                    else {
                        testCountByWeekForChart.forEach(week => {
                            if (week.Week === data.week) {
                                week[data.testType] = data.count;
                            }
                        });
                    }
                });

                testCountByWeekForChart.forEach(testWeek => {
                    testWeek.Week = Number(testWeek.Week)
                })

                var dummyArray = [];

                var names_array_new = testCountByWeekForChart.reduceRight(function(r, a) {
                    r.some(function(b) {
                        if (a.Year > b.Year) {
                            dummyArray.push(a);
                            return dummyArray;
                        }
                    }) || r.push(a); return r;

                }, []);

                names_array_new.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                dummyArray.sort(function(a, b) {
                    if (a.Week > b.Week) { return 1; } else if (a.Week < b.Week) { return -1; }
                    return 0;
                });

                names_array_new = names_array_new.concat(dummyArray)

                testCountByWeekForChart = names_array_new;
                this.testChartValue = testCountByWeekForChart;

                this.graphValues = [];
                this.testList.forEach(test => {

                    this.graphValues.push({
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>week [[category]]: <b>[[value]] diagnostics done</b></span>",
                        "fillAlphas": 1.8,
                        "labelText": "[[value]]",
                        "lineAlpha": 1.0,
                        "title": test.name,
                        "type": "column",
                        "color": "#000000",
                        "showHandOnHover": false,
                        "valueField": test.type.toLowerCase(),
                        "colorField": test.color,
                        "fillColors": test.color,
                        "lineColor": test.color
                    });
                });
                this.showDigLoad = false;
                // window.localStorage.setItem("graphValuesTestCountByWeek", JSON.stringify(graphValues));
                // window.localStorage.setItem("testCountByWeek", JSON.stringify(testCountByWeekForChart));
                // window.localStorage.setItem("selectedYear", this.selectedYear.toString());
                // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                //     'assets/demo/default/custom/components/charts/amcharts/charts.testbyweek.4.js');
                Helpers.setLoading(false);

            });
    }

    getPatientFemaleCount(startDate, endDate, searchcriteria, facilityId, centerId) {
        this._patientsService.getPatientStatsForGenderByFilter(searchcriteria, 0, new Date(endDate).getTime(),
            facilityId, centerId).then(data => {
                // console.log('data', data)
                var totalGenderCount = data.female + data.male + data.other
                if (totalGenderCount != 0) {
                    var femalePercent = ((data.female * 100) / totalGenderCount);
                    femalePercent = Math.round(femalePercent)
                }
                else
                    femalePercent = 0;
                this.reportData.females = femalePercent;
                this.femalePercent = femalePercent.toString();
            })
    }

    getWeeklyPatient() {
        var weekCount = 0;
        this._patientsService.getPatientCenterCount(this.getWeek()[0].getTime(), this.getWeek()[1].getTime(), 'CENTER', this.facilityId, this.centerId)
            .then(count => weekCount = count)
            .then(() => {
                //  this.patientCount = this.patientCount.toString();                     
                this.reportData.registrationCount = weekCount;
                Helpers.setLoading(false);
            });
    }

    getNewPatient() {
        var weekCount = 0;
        this._patientsService.getPatientCenterCount(0, new Date().getTime(), 'CENTER', this.facilityId, this.centerId)
            .then(count => weekCount = count)
            .then(() => {
                this.reportData.beneficiariesTillDate = weekCount;
                Helpers.setLoading(false);
            });
    }

    getWeeklyUser() {
        Helpers.setLoading(true);
        this._usersService.getUsersFiltered(this.getWeek()[0].getTime(), this.getWeek()[1].getTime(), 'CENTER', this.centerId, 1, 10, '', 'asc')
            .then(users => {
                Helpers.setLoading(false);
                this.reportData.userCount = users.count;
                // this.userCount = users.count.toString();
            })
    }

    getWeeklyTest() {
        this.testsCount = null;
        Helpers.setLoading(true);
        this._visitsService.getTestCounts(this.getWeek()[0].getTime(), this.getWeek()[1].getTime(), 'CENTER', this.facilityId, this.centerId)
            .then(data => {
                if (data != null) {
                    this.reportData.diagnosticsCount = data.totalHits
                    // console.log('dig count', this.reportData.diagnosticsCount)
                }
                else
                    this.reportData.diagnosticsCount = 0;
            })
            .then(() => {
                Helpers.setLoading(false);
            });
    }

    getTestReport() {
        var from = this.getWeek()[0].getTime();
        var to = this.getWeek()[1].getTime();
        Helpers.setLoading(true);
        this.types.forEach(test => {
            this._testsReportService.getTestBasedOnCenterForReport('CENTER', this.facilityId, this.selectedCenter.id, test.value, from, to)
                .then(data => {
                    Helpers.setLoading(false);
                    if (data.testName == "BLOOD_PRESSURE") {
                        this.reportData.hypertensionNormal = data.normal;
                        this.reportData.hypertensionHigh = data.high;
                    }
                    if (data.testName == "BLOOD_GLUCOSE") {
                        this.reportData.diabetesNormal = data.normal;
                        this.reportData.diabetesDiabetic = data.high;
                        this.reportData.diabetesPre = data.low;
                    }
                    if (data.testName == "HEMOGLOBIN") {
                        this.reportData.anemiaHigh = data.high;
                        this.reportData.anemiaLow = data.low;
                        this.reportData.anemiaNormal = data.normal;
                    }
                    if (data.testName == "MALARIA") {
                        this.reportData.malariaPositive = data.positive;
                        this.reportData.malariaNegative = data.negative;
                    }
                })
        });

    }

    getReportWeek() {
        var from = this.getWeek()[0].getTime();
        var to = this.getWeek()[1].getTime();
        var d = new Date(from),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        var sTime = [day, month, year].join('/');

        var e = new Date(to),
            month = '' + (e.getMonth() + 1),
            day = '' + e.getDate(),
            year = e.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        var eTime = [day, month, year].join('/');
        this.reportData.fromDate = sTime;
        this.reportData.toDate = eTime;
        this.reportData.fromDate = this.reportData.fromDate.toString();
        this.reportData.toDate = this.reportData.toDate.toString();
    }

    getWeek() {
        var startDate2 = new Date();
        startDate2.setDate(startDate2.getDate() - startDate2.getDay() + 1);
        startDate2.setHours(0);
        startDate2.setMinutes(0);
        startDate2.setSeconds(0);
        startDate2.setMilliseconds(0);
        var endDate2 = new Date();
        endDate2.setDate(startDate2.getDate() + 6);
        endDate2.setHours(23);
        endDate2.setMinutes(23);
        endDate2.setSeconds(23);
        endDate2.setMilliseconds(0);
        return [startDate2, endDate2]
    }

    download() {
        this.msg = false;
        Helpers.setLoading(true);
        var from = this.getWeek()[0].getTime();
        var to = this.getWeek()[1].getTime();
        var d = new Date(from),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        var sTime = [day, month, year].join('/');

        var e = new Date(to),
            month = '' + (e.getMonth() + 1),
            day = '' + e.getDate(),
            year = e.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        var eTime = [day, month, year].join('/');

        this.pdfHead = this.reportData.centerName + ' ' + 'Weekly report between' + ' ' + sTime + ' - ' + eTime;

        // console.log('sending data', this.reportData)

        this._facilitiesService.generatePdf(this.reportData)
            .then(res => {
                Helpers.setLoading(false);
                // console.log('data', res)
                this.objectURL = URL.createObjectURL(res);
                this.showPdfModel = true;
                //window.open(this.objectURL)

                // var iframe = document.createElement('iframe');                
                // iframe.setAttribute('id', 'printf'); // assign an id
                // iframe.setAttribute('name', 'printf');
                // iframe.setAttribute('height', '650px');
                // iframe.setAttribute('width', '550px');
                // iframe.src = this.objectURL;

                // var refractionPrintWindow = window.open();
                // refractionPrintWindow.document.title = this.pdfHead;
                // refractionPrintWindow.document.body.appendChild(iframe);
                // refractionPrintWindow.frames["printf"].print(iframe);

            }, error => {
                this.msg = true;
                setTimeout(() => {
                    this.msg = false;
                }, 3000);
            })

    }

    downloadPdfLocal() {
        var a = document.createElement("a");
        a.href = this.objectURL;
        a.download = this.pdfHead + ".pdf";
        document.body.appendChild(a);
        a.click();
        this.showPdfModel = false;
        // var iframe = document.createElement('iframe');
        // // iframe.className = 'sample-iframe';
        // iframe.setAttribute('id', 'printf'); // assign an id
        // iframe.setAttribute('name', 'printf');
        // iframe.setAttribute('height', '650px');
        // iframe.setAttribute('width', '550px');
        // iframe.src = this.objectURL;

        // var refractionPrintWindow = window.open();
        // refractionPrintWindow.document.title = 'Referral Print';
        // refractionPrintWindow.document.body.appendChild(iframe);
        // refractionPrintWindow.frames["printf"].print(iframe);
    }

}
