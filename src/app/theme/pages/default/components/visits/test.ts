export class Test {
    id: string;
    visitId: string;
    patientId: string;
    startTime: string;
    endTime: string;
    hcDeviceId: string;
    tabletId: string;
    panelId: string;
    type: string;
    userInput: string;
    consumableInventoryId: string;
    consumableQuantity: number;
    testCost: number;
    testExpenseConsumable: number;
    currency: string;
    endTimeDate: Date;
}