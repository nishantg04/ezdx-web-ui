import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../../../../helpers';
import { AnalyticsService } from '../analytics.service';
import { ScriptLoaderService } from './../../../../../../_services/script-loader.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./permachinedata.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class PermachinedataComponent implements OnInit {

    constructor(private analytics: AnalyticsService, private _script: ScriptLoaderService, private _router: Router,private _activeroute: ActivatedRoute) {
        this.user=JSON.parse(localStorage.getItem('userInfo'));
     }
    public avgdiagnostic: any = [];
    public avgdiagnosticresp: any = {};
    public avgdiagnosticCount: number = 0;
    public visiblePagination: boolean = false;
    public pageNo: number = 1;
    public limit: number = 10;
    public ranges: any = [];
    public row: number = 10;
    public warning: boolean = false;
    public selectedoption: string = 'Patient registered';
    public selectedItem: string;
    public selectedcenter: string;
    user:any;

    ngOnInit() {
        this._activeroute.queryParams.subscribe(param => {
            this.selectedoption = param.selectedparam == undefined || param.selectedparam == null || param.selectedparam == '' ? 'Patient registered' : param.selectedparam;
            this.selectedItem = param.selectedItem;
            this.selectedcenter = param.selectedcenter;
        });
        Helpers.setLoading(true);
        if (this.user.userRoles.includes('TECHNICIAN')) {
            this.getpermachinedataFacilitybased(this.user.facilityId,this.pageNo, this.limit);
        }else{
            this.getpermachinedatabased(this.pageNo, this.limit);
        }
       
        this.ranges = [
            { label: "10", value: 10 },
            { label: "25", value: 25 },
            { label: "50", value: 50 },
            { label: "100", value: 100 },
        ];

    }

    //get diagnostics and consumable based test per registration
    public getpermachinedatabased(page, limit): void {
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getavgdiagnosticAndConsumablebasedtest(page, limit).then((res) => {
            Helpers.setLoading(false);
            //  console.log(res)
            if (res != undefined) {
                this.visiblePagination = true;
                this.avgdiagnosticresp = res;
                this.avgdiagnostic = this.avgdiagnosticresp.results;
                this.avgdiagnosticCount = this.avgdiagnosticresp.totalHits;
                this.avgdiagnostic.sort((a, b) => { return b.totalRegistrations - a.totalRegistrations })


            } else {
                this.avgdiagnostic = [];
                this.avgdiagnosticCount = 0;
            }
        })
    }

    //pmd facility based
    public getpermachinedataFacilitybased(facility,page, limit): void {
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        this.analytics.getavgdiagnosticAndConsumablebasedtestFacility(facility,page, limit).then((res) => {
            Helpers.setLoading(false);
            //  console.log(res)
            if (res != undefined) {
                this.visiblePagination = true;
                this.avgdiagnosticresp = res;
                this.avgdiagnostic = this.avgdiagnosticresp.results;
                this.avgdiagnosticCount = this.avgdiagnosticresp.totalHits;
                this.avgdiagnostic.sort((a, b) => { return b.totalRegistrations - a.totalRegistrations })

            } else {
                this.avgdiagnostic = [];
                this.avgdiagnosticCount = 0;
            }
        })
    }

    goBack(){
        this._router.navigate(['/analytics/charts']) 
    }

    //changed selectedoption
    selectedoptionchanged(option) {
        this.selectedoption = option
        if (this.selectedoption == 'patient registration') {
            this.avgdiagnostic.sort((a, b) => { return b.totalRegistrations - a.totalRegistrations })
        } else if (this.selectedoption == 'consumable based test') {
            this.avgdiagnostic.sort((a, b) => { return b.totalConsumableTests - a.totalConsumableTests })
        } else if (this.selectedoption == 'probe based test') {
            this.avgdiagnostic.sort((a, b) => { return b.totalProbeTests - a.totalProbeTests })
        } else {

        }
    }


    lookupRowStyleClass(rowData: any) {
        return rowData.facilityName == this.selectedItem ? 'selectedRowClass' : '';
    }


    //when pagination number changed
    paginate(event) {
        this.pageNo = event.page + 1;
        this.limit = parseInt(event.rows);
        if (this.user.userRoles.includes('TECHNICIAN')) {
            this.getpermachinedataFacilitybased(this.user.facilityId,this.pageNo, this.limit);
        }else{
            this.getpermachinedatabased(this.pageNo, this.limit);
        }
    }



    //change items per page
    select(e) {
        this.visiblePagination = false;
        this.limit = e.value;
        this.pageNo = 1;
        this.row = e.value;
        if (this.user.userRoles.includes('TECHNICIAN')) {
            this.getpermachinedataFacilitybased(this.user.facilityId,this.pageNo, this.limit);
        }else{
            this.getpermachinedatabased(this.pageNo, this.limit);
        }
    }




}
