import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterOrganizationByCode'
})
export class FilterOrganizationByCodePipe implements PipeTransform {

    transform(value: any, args?: any): any {
        return value.filter(data => {
            return data.organizationCode !== args;
        });

    }
}
