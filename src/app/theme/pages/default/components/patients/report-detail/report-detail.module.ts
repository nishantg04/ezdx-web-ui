import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from "@ngx-translate/core";
import { ReportDetailComponent } from "./report-detail.component";
import { DefaultComponent } from "../../../default.component";
import { LayoutModule } from "../../../../../layouts/layout.module";
import { DialogModule } from "primeng/primeng";

const routes: Routes = [
    {
        path: "",
        component: DefaultComponent,
        children: [
            {
                path: "",
                component: ReportDetailComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        LayoutModule,
        TranslateModule.forChild({ isolate: false }),
        DialogModule
    ],
    exports: [RouterModule],
    declarations: [ReportDetailComponent],
    providers: [TranslateService]
})
export class ReportDetailModule { }
