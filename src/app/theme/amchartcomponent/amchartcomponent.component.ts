import { Component, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';

import { AmChartsModule, AmChartsService } from "amcharts3-angular2";
@Component({
    selector: 'app-amchartcomponent',
    templateUrl: './amchartcomponent.component.html',
    styles: ['.amcharts-label,amcharts-title { font-family: Roboto !important; }', '.amcharts-pie-slice {cursor:pointer}']
})
export class AmchartComponent implements OnChanges, OnDestroy {

    constructor(private AmCharts: AmChartsService) {
    }
    @Input() data: any = [];
    @Input() type;
    @Input() id;
    @Input() title;
    @Input() height;
    @Input() category;
    @Input() graphData: any = [];
    @Input() xTitle: String;
    @Input() yTitle: String;
    @Input() baloonvalue: String;
    @Input() for: String;
    @Input() from: String;
    hovering: boolean = true;
    clickType: string = 'clickGraphItem';

    private chart: any;


    ngOnChanges(changes: SimpleChanges) {

        if (window.localStorage.getItem('userType') == 'Center' || window.localStorage.getItem('userType') == 'User') {
            this.hovering = false;
            this.clickType = 'clickGraph'
        } else {
            this.hovering = true;
            this.clickType = 'clickGraphItem'
        }

        if (this.type == 'pie') {
            this.showPieChart(changes)
        }
        if (this.type == 'serial') {
            this.showSerialChart(changes)
        }
        if (this.type == 'patient') {
            this.showPatientChart(changes)
        }
        if (this.type == 'dashboardserial') {
            this.showDashboardSerialChart(changes)
        }
        if (this.type == 'dashboardpatient') {
            this.showDashboardPatientChart(changes)
        }
        if (this.type == 'userserial') {
            this.showUserSerialChart(changes)
        }
        if (this.type == 'xy') {
            this.showBubbleChart(changes)
        }
        if (this.type == 'colLine') {
            this.showcolLineMix(changes)
        }
        if (this.type == 'basicpie') {
            this.showbasicPie(changes)
        }
    }

    ngOnDestroy() {
        if (this.title != undefined) {
            //   this.AmCharts.destroyChart(this.title);            
        }
    }

    showPieChart(changes) {
        // console.log(changes)
        setTimeout(() => {
            this.title = this.AmCharts.makeChart(this.id, {
                "type": this.type,
                "theme": "light",
                "dataProvider": changes.data.currentValue,
                "titleField": "title",
                "valueField": "value",
                "colorField": "color",
                "labelRadius": 5,
                "radius": "42%",
                "innerRadius": "70%",
                "labelText": "",
                "legend": {
                    "enabled": true,
                    "align": "center",
                    "markerType": "circle"
                },
                "balloon": {
                    "fixedPosition": true
                },
                "export": {
                    "enabled": true
                }
            });

            if (this.category == 'gender') {
                if (this.data[0].value === 0 && this.data[1].value === 0 && this.data[2].value === 0) {
                    this.title.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
                }
            }
            else if (this.category == 'age') {
                if (this.data[0].value === 0 && this.data[1].value === 0 && this.data[2].value === 0 && this.data[3].value === 0 && this.data[4].value === 0) {
                    this.title.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
                }
            }
            else {
                if (this.data[0].value === 0 && this.data[1].value === 0 && this.data[2].value === 0) {
                    this.title.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
                }
            }



        }, 1700);
    }

    showSerialChart(changes) {
        var formateDate = function(d) {
            var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");

            var curr_date = d.getDate();
            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
            return m_names[curr_month] + " " + curr_date + " " + curr_year;
        }
        var getDateRangeOfWeek = function(w, y) {
            var week = w.split("\n")
            var simple = new Date(y, 0, 1 + (week[0] - 1) * 7);
            var dow = simple.getDay();
            var ISOweekStart = simple;
            if (dow <= 4)
                ISOweekStart.setDate(simple.getDate() - simple.getDay());
            else
                ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
            var ISOweekEnd = new Date(ISOweekStart);
            ISOweekEnd.setDate(ISOweekEnd.getDate() + 6);
            ISOweekEnd.setHours(23);
            ISOweekEnd.setMinutes(23);
            ISOweekEnd.setSeconds(23);
            ISOweekEnd.setMilliseconds(0);
            ISOweekEnd.setFullYear(y);
            return formateDate(ISOweekStart) + " to " + formateDate(ISOweekEnd);
        };
        setTimeout(() => {
            this.title = this.AmCharts.makeChart(this.id, {
                "type": "serial",
                "theme": "light",
                "angle": 30,
                "depth3D": 0,
                "legend": {
                    "horizontalGap": 0,
                    "verticalGap": 0,
                    "markerSize": 10,
                    "valueWidth": 30
                },
                "dataProvider": changes.data.currentValue,
                "valueAxes": [{
                    "stackType": "regular",
                    "title": "Diagnostics done",
                    "gridColor": '#aaa'
                }],
                "graphs": [{
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Physical</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Physical",
                    "type": "column",
                    "color": "#71bf44",
                    "colorField": '#71bf44',
                    "fillColors": '#71bf44',
                    "lineColor": '#71bf44',
                    "valueField": "physical"
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Whole Blood-POCT</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Whole Blood-POCT",
                    "type": "column",
                    "color": "#21C4DF",
                    "colorField": '#21C4DF',
                    "fillColors": '#21C4DF',
                    "lineColor": '#21C4DF',
                    "valueField": "whole_blood_poct"
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Whole Blood</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Whole Blood",
                    "type": "column",
                    "color": "#02abcb",
                    "colorField": '#02abcb',
                    "fillColors": '#02abcb',
                    "lineColor": '#02abcb',
                    "valueField": "whole_blood"
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Whole Blood-RDT</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Whole Blood-RDT",
                    "type": "column",
                    "color": "#465EEB",
                    "colorField": '#465EEB',
                    "fillColors": '#465EEB',
                    "lineColor": '#465EEB',
                    "valueField": "whole_blood_rdt"
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Urine-POCT</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Urine-POCT",
                    "type": "column",
                    "color": "#DA70D6",
                    "colorField": '#DA70D6',
                    "fillColors": '#DA70D6',
                    "lineColor": '#DA70D6',
                    "valueField": "urine_poct"
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Urine-RDT</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Urine-RDT",
                    "type": "column",
                    "color": "#B22222",
                    "colorField": '#B22222',
                    "fillColors": '#B22222',
                    "lineColor": '#B22222',
                    "valueField": "urine_rdt"
                }],
                "categoryField": "Week_Year",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "position": "left",
                    "title": "Week",
                    "gridColor": '#aaa'
                }

            });
            if (this.data.length === 0) {
                this.title.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
            }

        }, 1000)
    }

    //dashboard serial chart
    showDashboardSerialChart(changes) {
        var formateDate = function(d) {
            var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");

            var curr_date = d.getDate();
            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
            return m_names[curr_month] + " " + curr_date + " " + curr_year;
        }
        var getDateRangeOfWeek = function(w, y) {
            var week = w.split("\n")
            var simple = new Date(y, 0, 1 + (week[0] - 1) * 7);
            var dow = simple.getDay();
            var ISOweekStart = simple;
            if (dow <= 4)
                ISOweekStart.setDate(simple.getDate() - simple.getDay());
            else
                ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
            var ISOweekEnd = new Date(ISOweekStart);
            ISOweekEnd.setDate(ISOweekEnd.getDate() + 6);
            ISOweekEnd.setHours(23);
            ISOweekEnd.setMinutes(23);
            ISOweekEnd.setSeconds(23);
            ISOweekEnd.setMilliseconds(0);
            ISOweekEnd.setFullYear(y);
            return formateDate(ISOweekStart) + " to " + formateDate(ISOweekEnd);
        };
        setTimeout(() => {
            this.title = this.AmCharts.makeChart(this.id, {
                "type": "serial",
                "theme": "light",
                "angle": 30,
                "depth3D": 0,
                "legend": {
                    "horizontalGap": 0,
                    "verticalGap": 0,
                    "markerSize": 10,
                    "valueWidth": 30
                },
                "dataProvider": changes.data.currentValue,
                "valueAxes": [{
                    "stackType": "regular",
                    "title": "Diagnostics done",
                    "gridColor": '#aaa'
                }],
                "graphs": [{
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Physical</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Physical",
                    "type": "column",
                    "color": "#71bf44",
                    "colorField": '#71bf44',
                    "fillColors": '#71bf44',
                    "lineColor": '#71bf44',
                    "valueField": "physical",
                    "showHandOnHover": this.hovering
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Whole Blood-POCT</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Whole Blood-POCT",
                    "type": "column",
                    "color": "#21C4DF",
                    "colorField": '#21C4DF',
                    "fillColors": '#21C4DF',
                    "lineColor": '#21C4DF',
                    "valueField": "whole_blood_poct",
                    "showHandOnHover": this.hovering
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Whole Blood</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Whole Blood",
                    "type": "column",
                    "color": "#02abcb",
                    "colorField": '#02abcb',
                    "fillColors": '#02abcb',
                    "lineColor": '#02abcb',
                    "valueField": "whole_blood",
                    "showHandOnHover": this.hovering
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Whole Blood-RDT</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Whole Blood-RDT",
                    "type": "column",
                    "color": "#465EEB",
                    "colorField": '#465EEB',
                    "fillColors": '#465EEB',
                    "lineColor": '#465EEB',
                    "valueField": "whole_blood_rdt",
                    "showHandOnHover": this.hovering
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Urine-POCT</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Urine-POCT",
                    "type": "column",
                    "color": "#DA70D6",
                    "colorField": '#DA70D6',
                    "fillColors": '#DA70D6',
                    "lineColor": '#DA70D6',
                    "valueField": "urine_poct",
                    "showHandOnHover": this.hovering
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Urine-RDT</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Urine-RDT",
                    "type": "column",
                    "color": "#B22222",
                    "colorField": '#B22222',
                    "fillColors": '#B22222',
                    "lineColor": '#B22222',
                    "valueField": "urine_rdt",
                    "showHandOnHover": this.hovering
                }],
                "categoryField": "Week_Year",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "position": "left",
                    "title": "Week",
                    "gridColor": '#aaa'
                },

                "listeners": [{
                    "event": this.clickType,
                    "method": function(event) {
                        //window.localStorage.setItem("SelectedMonthDiagnosis", event.item.category);
                        if (event.type == 'clickGraphItem')
                            window.my.namespace.publicFuncPatientWeek(event.item.category, event.item.dataContext.Year, "test");
                    }
                }]
            });
            if (this.data.length === 0) {
                this.title.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
            }

        }, 1000)
    }
    //user serial chart
    showUserSerialChart(changes) {
        var formateDate = function(d) {
            var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");

            var curr_date = d.getDate();
            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
            return m_names[curr_month] + " " + curr_date + " " + curr_year;
        }
        var getDateRangeOfWeek = function(w, y) {
            var week = w.split("\n")
            var simple = new Date(y, 0, 1 + (week[0] - 1) * 7);
            var dow = simple.getDay();
            var ISOweekStart = simple;
            if (dow <= 4)
                ISOweekStart.setDate(simple.getDate() - simple.getDay());
            else
                ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
            var ISOweekEnd = new Date(ISOweekStart);
            ISOweekEnd.setDate(ISOweekEnd.getDate() + 6);
            ISOweekEnd.setHours(23);
            ISOweekEnd.setMinutes(23);
            ISOweekEnd.setSeconds(23);
            ISOweekEnd.setMilliseconds(0);
            ISOweekEnd.setFullYear(y);
            return formateDate(ISOweekStart) + " to " + formateDate(ISOweekEnd);
        };
        setTimeout(() => {
            this.title = this.AmCharts.makeChart(this.id, {
                "type": "serial",
                "theme": "light",
                "angle": 30,
                "depth3D": 0,
                "legend": {
                    "horizontalGap": 0,
                    "verticalGap": 0,
                    "markerSize": 10,
                    "valueWidth": 30
                },
                "dataProvider": changes.data.currentValue,
                "valueAxes": [{
                    "stackType": "regular",
                    "title": "Diagnostics done",
                    "gridColor": '#aaa'
                }],
                "graphs": [{
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Physical</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Physical",
                    "type": "column",
                    "color": "#71bf44",
                    "colorField": '#71bf44',
                    "fillColors": '#71bf44',
                    "lineColor": '#71bf44',
                    "showHandOnHover": true,
                    "valueField": "physical"
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Whole Blood-POCT</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Whole Blood-POCT",
                    "type": "column",
                    "color": "#21C4DF",
                    "colorField": '#21C4DF',
                    "fillColors": '#21C4DF',
                    "lineColor": '#21C4DF',
                    "showHandOnHover": true,
                    "valueField": "whole_blood_poct"
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Whole Blood</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Whole Blood",
                    "type": "column",
                    "color": "#02abcb",
                    "colorField": '#02abcb',
                    "fillColors": '#02abcb',
                    "lineColor": '#02abcb',
                    "showHandOnHover": true,
                    "valueField": "whole_blood"
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Whole Blood-RDT</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Whole Blood-RDT",
                    "type": "column",
                    "color": "#465EEB",
                    "colorField": '#465EEB',
                    "fillColors": '#465EEB',
                    "lineColor": '#465EEB',
                    "showHandOnHover": true,
                    "valueField": "whole_blood_rdt"
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Urine-POCT</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Urine-POCT",
                    "type": "column",
                    "color": "#DA70D6",
                    "colorField": '#DA70D6',
                    "fillColors": '#DA70D6',
                    "lineColor": '#DA70D6',
                    "valueField": "urine_poct",
                    "showHandOnHover": true
                }, {
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<b>Urine-RDT</b><br><span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " diagnostics done</b><br/>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Urine-RDT",
                    "type": "column",
                    "color": "#B22222",
                    "colorField": '#B22222',
                    "fillColors": '#B22222',
                    "lineColor": '#B22222',
                    "valueField": "urine_rdt",
                    "showHandOnHover": true
                }],
                "categoryField": "Week_Year",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "position": "left",
                    "title": "Week",
                    "gridColor": '#aaa'
                },

                "listeners": [{
                    "event": "clickGraphItem",
                    "method": function(event) {
                        var valueJson = {
                            "week": event.item.dataContext.Week,
                            "test": event.target.valueField,
                            "year": event.item.dataContext.Year
                        }
                        window.my.namespace.showChartWeek(valueJson, event.item.graph.bulletColorR);
                    }
                }]
            });
            if (this.data.length === 0) {
                this.title.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
            }

        }, 1000)
    }

    //general patient chart
    showPatientChart(changes) {
        var formateDate = function(d) {
            var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");

            var curr_date = d.getDate();
            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
            return m_names[curr_month] + " " + curr_date + " " + curr_year;
        }
        var getDateRangeOfWeek = function(w, y) {
            var week = w.split("\n")
            var simple = new Date(y, 0, 1 + (week[0] - 1) * 7);
            var dow = simple.getDay();
            var ISOweekStart = simple;
            if (dow <= 4)
                ISOweekStart.setDate(simple.getDate() - simple.getDay());
            else
                ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
            var ISOweekEnd = new Date(ISOweekStart);
            ISOweekEnd.setDate(ISOweekEnd.getDate() + 6);
            ISOweekEnd.setHours(23);
            ISOweekEnd.setMinutes(23);
            ISOweekEnd.setSeconds(23);
            ISOweekEnd.setMilliseconds(0);
            ISOweekEnd.setFullYear(y);
            return formateDate(ISOweekStart) + " to " + formateDate(ISOweekEnd);
        };


        setTimeout(() => {
            this.title = this.AmCharts.makeChart(this.id, {
                "type": "serial",
                "categoryField": "Week_Year",
                "maxSelectedTime": -2,
                "angle": 30,
                "startDuration": 1,
                "startEffect": "easeOutSine",
                "processCount": 998,
                "processTimeout": -2,
                "theme": "light",
                "categoryAxis": {
                    "gridPosition": "start",
                    "title": "Week"
                },
                "trendLines": [],
                "graphs": [{
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " patients</b></span><br/><span>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1,
                    "id": "AmGraph-1",
                    "title": "No of Patient Registrations",
                    "type": "column",
                    "valueField": "Patients",
                    "labelText": "[[value]]",
                    "lineColor": "#71bf44",
                    "fillColors": "#71bf44",
                    "showHandOnHover": false
                }],
                "guides": [],
                "valueAxes": [{
                    "id": "ValueAxis-1",
                    "stackType": "regular",
                    "title": "Patients"
                }],
                "allLabels": [],
                "balloon": {},
                "legend": {
                    "enabled": false,
                    "useGraphSettings": true
                },

                "dataProvider": changes.data.currentValue

            });
            if (this.data.length === 0) {
                this.title.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
            }
        }, 1000)
    }

    //dashboard patient
    showDashboardPatientChart(changes) {
        var formateDate = function(d) {
            var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");

            var curr_date = d.getDate();
            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
            return m_names[curr_month] + " " + curr_date + " " + curr_year;
        }
        var getDateRangeOfWeek = function(w, y) {
            var week = w.split("\n")
            var simple = new Date(y, 0, 1 + (week[0] - 1) * 7);
            var dow = simple.getDay();
            var ISOweekStart = simple;
            if (dow <= 4)
                ISOweekStart.setDate(simple.getDate() - simple.getDay());
            else
                ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
            var ISOweekEnd = new Date(ISOweekStart);
            ISOweekEnd.setDate(ISOweekEnd.getDate() + 6);
            ISOweekEnd.setHours(23);
            ISOweekEnd.setMinutes(23);
            ISOweekEnd.setSeconds(23);
            ISOweekEnd.setMilliseconds(0);
            ISOweekEnd.setFullYear(y);
            return formateDate(ISOweekStart) + " to " + formateDate(ISOweekEnd);
        };


        setTimeout(() => {
            this.title = this.AmCharts.makeChart(this.id, {
                "type": "serial",
                "categoryField": "Week_Year",
                "maxSelectedTime": -2,
                "angle": 30,
                "startDuration": 1,
                "startEffect": "easeOutSine",
                "processCount": 998,
                "processTimeout": -2,
                "theme": "light",
                "categoryAxis": {
                    "gridPosition": "start",
                    "title": "Week",
                    "gridColor": '#aaa'

                },
                "trendLines": [],
                "graphs": [{
                    "balloonFunction": function(graphDataItem, graph) {
                        return "<span style='font-size:14px'>week " + graphDataItem.category + ": <b>" + graphDataItem.values.value + " patients</b></span><br/><span>" + getDateRangeOfWeek(graphDataItem.category, graphDataItem.dataContext.Year) + "</span>"
                    },
                    "fillAlphas": 1,
                    "id": "AmGraph-1",
                    "title": "No of Patient Registrations",
                    "type": "column",
                    "valueField": "Patients",
                    "labelText": "[[value]]",
                    "lineColor": "#71bf44",
                    "fillColors": "#71bf44",
                    "showHandOnHover": this.hovering
                }],
                "guides": [],
                "valueAxes": [{
                    "id": "ValueAxis-1",
                    "stackType": "regular",
                    "title": "Patients",
                    "gridColor": '#aaa'

                }],
                "allLabels": [],
                "balloon": {},
                "legend": {
                    "enabled": false,
                    "useGraphSettings": true
                },

                "dataProvider": changes.data.currentValue,
                "listeners": [{
                    "event": this.clickType,
                    "method": function(event) {
                        //window.localStorage.setItem("SelectedMonthDiagnosis", event.item.category);
                        if (event.type == 'clickGraphItem')
                            window.my.namespace.publicFuncPatientWeek(event.item.category, event.item.dataContext.Year, "patient");
                    }
                }]
            });
            if (this.data.length === 0) {
                this.title.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
            }
        }, 1000)
    }

    //show bubble chart

    showBubbleChart(changes) {
        setTimeout(() => {
            this.title = this.AmCharts.makeChart(this.id, {
                "type": "xy",
                "theme": "light",
                "marginTop": 17,
                "dataProvider": changes.data.currentValue,
                "valueAxes": [{
                    "position": "bottom",
                    "axisAlpha": 0,
                    "title": this.xTitle,
                    "gridColor": '#aaa'
                }, {
                    "minMaxMultiplier": 1.2,
                    "axisAlpha": 0,
                    "position": "left",
                    "title": this.yTitle,
                    "gridColor": '#aaa'
                }],
                "pathToImages": "https://cdn.amcharts.com/lib/3/images/", // required for grips
                "chartScrollbar": {},
                "startDuration": 1.5,
                "graphs": [{
                    "balloonText": "<b>[[facility]]</b> <br/><b>Center: [[center]]</b> <br/>" + this.xTitle + " :<b>[[x]]</b><br/> " + this.yTitle + " :<b>[[y]]</b><br>" + this.baloonvalue + ":<b>[[value]]</b>",
                    "bullet": "bubble",
                    "lineAlpha": 0,
                    "valueField": "value",
                    "xField": "x",
                    "yField": "y",
                    "fillAlphas": 0,
                    "bulletBorderAlpha": 0.2,
                    "bulletBorderColor": "#ddd",
                    "maxBulletSize": 80,
                    "colorField": "color",
                    "showHandOnHover": true
                }],
                "listeners": [{
                    "event": "clickGraphItem",
                    "method": function(event) {
                        if (event.item.graph.valueAxis.title == 'Devices') {
                            window.my.namespace1.gotodesiredpage(event.item.graph.xAxis.title, event.item.dataContext, "devices");
                        } else {
                            window.my.namespace1.gotodesiredpage(event.item.graph.valueAxis.title, event.item.dataContext, "test");
                        }

                    }
                }],
                "marginBottom": 35,
                "balloon": {
                    "fixedPosition": true
                },
                "export": {
                    "enabled": false
                }
            });
            if (this.data.length === 0) {
                this.title.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
            }
        }, 1600)

    }

    //show coloumn and line mix chart
    showcolLineMix(changes) {
        var up = false;

        function formatLabel(value, valueString, axis) {
            if (up) {
                axis.labelOffset = 0;
            }
            else {
                axis.labelOffset = 25;
            }
            up = !up;

            return value;
        }
        setTimeout(() => {
            this.title = this.AmCharts.makeChart(this.id, {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "autoMargins": true,
                "marginTop": 20,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },

                "dataProvider": changes.data.currentValue,
                "valueAxes": [{
                    "axisAlpha": 0,
                    "position": "left",
                    "gridColor": '#aaa',
                    "title": 'Patient Registered'
                }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[title]] by [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "Patient Registered",
                    "type": "column",
                    "valueField": "Patient Registered",
                    "dashLengthField": "dashLengthColumn",
                    "showHandOnHover": true
                }, {
                    "id": "graph2",
                    "balloonText": "<span style='font-size:12px;'>[[title]] for [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "bullet": "round",
                    "lineThickness": 3,
                    "bulletSize": 7,
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "useLineColorForBulletBorder": true,
                    "bulletBorderThickness": 3,
                    "fillAlphas": 0,
                    "lineAlpha": 1,
                    "title": "Re-visits",
                    "valueField": "Re-visits",
                    "dashLengthField": "dashLengthLine",
                    "showHandOnHover": true
                }],
                "categoryField": "User",
                "pathToImages": "https://cdn.amcharts.com/lib/3/images/", // required for grips
                "chartScrollbar": {},
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "gridColor": '#aaa',
                    "title": 'Users',
                    //"labelRotation": 45,
                    "labelFunction": formatLabel
                },
                "listeners": [{
                    "event": "clickGraphItem",
                    "method": function(event) {
                        window.my.namespace1.gotodesiredpage('visitstats', event.item.dataContext);

                    }
                }],
                "export": {
                    "enabled": false
                }
            });
            if (this.data.length === 0) {
                this.title.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
            }
        }, 1600)

    }

    //basic pie chart
    //show coloumn and line mix chart
    showbasicPie(changes) {

        function labelFunction(info) {
            var data = info.dataContext;

            if (info.index != null && data.customer) {
                //   return data.customer + "("+data.center+") : " + data.percentage + "%";
                return data.customer + " : " + data.percentage + "%";
            } else {
                return "";
            }
        }
        setTimeout(() => {
            this.title = this.AmCharts.makeChart(this.id, {
                "type": "pie",
                "theme": "light",
                "dataProvider": changes.data.currentValue,
                "valueField": "percentage",
                "titleField": "customer",
                "labelFunction": labelFunction,
                "showHandOnHover": true,
                "balloon": {
                    "fixedPosition": true
                },
                "balloonFunction": function(graphDataItem, graph) {
                    if (graphDataItem.dataContext.registration == true) {
                        return "<span >" + graphDataItem.title + "(" + graphDataItem.dataContext.center + ") <br/><b>" + graphDataItem.dataContext['patient registered'] + "</b> Patients registered</span>"
                    } else {
                        return "<span >" + graphDataItem.title + "(" + graphDataItem.dataContext.center + ") <br/> <b>" + graphDataItem.dataContext['diagnostics'] + "</b> Diagnostics done</span>"
                    }
                },
            });

            this.title.addListener("clickSlice", graphClicked);
            function graphClicked(e) {
                console.log(e)
                if (e.dataItem.dataContext.registration == true) {
                    window.my.namespace1.gotodesiredpage('registration', e.dataItem.dataContext, 'ccwisedata');
                } else {
                    window.my.namespace1.gotodesiredpage('diagnostics', e.dataItem.dataContext, 'ccwisedata');
                }
            }
            if (this.data.length === 0) {
                this.title.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
            }
        }, 1500)

    }


}
