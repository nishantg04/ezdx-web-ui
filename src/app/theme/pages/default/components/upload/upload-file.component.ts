import { Component, OnInit, ViewEncapsulation, Self } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Ng4FilesStatus, Ng4FilesSelected, Ng4FilesService, Ng4FilesConfig } from "../../../../../../../node_modules/angular4-files-upload/src/app/ng4-files"
import { FacilitiesService } from '../facilities/facilities.service';
import { DevicesService } from '../devices/devices.service'
import { from } from 'rxjs/observable/from';
import { UploadFileService } from './upload-file.service'
import { CommonService } from '../../../../../_services/common.service'
import { Message } from 'primeng/components/common/api';
import { Facility } from './../facilities/facility';
import { Center } from './../facilities/center';
import { Utils } from './../utils/utils'
import { Helpers } from './../../../../../helpers';
import { Files } from './file-details/file-details';
import { setTimeout } from 'timers';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./upload-file.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['../../primeng.component.scss'],
})
export class UploadFileComponent implements OnInit {
    pageNo: number;
    pageNoFirm: number;
    size: number;
    filteredFacility: any[];
    filteredCenter: any[];
    filteredTestType: any[];
    filteredRAType: any[];
    facilities: any[];
    centers: any[];
    selectedFacility: any;
    selectedCenter: any;
    selectedId: any;
    selectedTestType: any;
    selectedRAType: any;
    deviceList: any[];
    uploadedList: any[];
    uploadedFirmwareList: any[];
    checkAll: boolean;
    displayUploadModal: boolean = false;
    displayRDTModal: boolean = false;
    msgs: Message[] = [];
    testType: any[];
    raType: any[];
    testTypeList: any[];
    thresholdValue: number;
    pythonCount: any;
    firmWareCount: any;
    msgSucess: boolean = false;
    hubMsg: boolean = false;
    errorMsg: boolean = false;
    pythonRowCount: any;
    hubRowCount: any;
    userRowCount: any;
    noHub: boolean = false;
    showDevices: boolean = false;
    deviceDetails: any;
    centerMsg: boolean = false;
    uploadMsg: string = null;
    checkedFile: boolean = false;
    selectedFile: any;
    assign: boolean = false;
    unassignedList: any;
    unassignedCount: any;
    checksum: any;
    currentUrl: any;
    pageunNo: any;
    unCount: any = 0;
    asCount: any = 0;
    firmwareAlert: boolean = false;
    errorCheck: boolean = false;
    lengthCheck: boolean = false;
    showPython: boolean = false;
    showFirmware: boolean = false;
    dataTosend: any;
    extension: any;
    pyAlert: boolean = false;
    fileWarning: boolean = false;
    fileName: any;

    constructor(private ng4FilesService: Ng4FilesService, private FacilitiesService: FacilitiesService, private DevicesService: DevicesService,
        private UploadFileService: UploadFileService, private CommonService: CommonService, private _router: Router, private _activeroute: ActivatedRoute) {
        this.pythonRowCount = 7;
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        this.selectedFile = new Files();
    }

    ngOnInit() {
        this.currentUrl = this._activeroute.snapshot['_routerState'].url;
        this.pageNo = 1;
        this.pageNoFirm = 1;
        this.size = 10;
        this.userRowCount = 5;
        this.thresholdValue = 100;
        this.hubRowCount = 7;
        this.pageunNo = 1;
        this.ng4FilesService.addConfig(this.FileConfig);
        this.getFaclity();
        if (this.currentUrl == '/upload/python') {
            this.showPython = true;
            this.getUnassignedPython(this.pageunNo, this.size);
            this.getPythonUploadFile(this.pageNo, this.size);
        }
        else {
            this.showFirmware = true;
            this.getUploadedFirmware(this.pageNoFirm, this.size);
            this.getUploadedunFirmware(this.pageunNo, this.size);
        }

        this.testType = [{ "testId": "1", "testName": "Pregnancy" }, { "testId": "2", "testName": "Malaria" }, { "testId": "3", "testName": "Hepatitis" }, { "testId": "4", "testName": "Dengue" }, { "testId": "5", "testName": "Chikungunya" }, { "testId": "6", "testName": "HIV" }, { "testId": "7", "testName": "Typhoid" }, { "testId": "8", "testName": "Troponin" }, { "testId": "9", "testName": "Syphilis" }, { "testId": "10", "testName": "HCV" }];
        this.raType = [{ "raId": "1", "raName": "RA2" }, { "raId": "2", "raName": "RA3" }];
        this.deviceDetails = { "facilityName": "", "centerName": "", "fileName": "", "organizationCode": '', "centerCode": "", "deviceSerial": [] }
        this.selectedTestType = this.testType[0];
        this.selectedRAType = this.raType[0];
    }


    getFaclity() {
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) { return 1; } else if (a.name < b.name) { return -1; }
            return 0;
        });
    }

    private FileConfig: Ng4FilesConfig = {
        acceptExtensions: [],
        maxFilesCount: 1,
        maxFileSize: 5120000,
        totalFilesSize: 10120000
    };

    public selectedFiles;


    paginatePythonList(event) {
        this.pageNo = event.page + 1;
        if (this.currentUrl == '/upload/python')
            this.getPythonUploadFile(this.pageNo, this.size);
        else
            this.getUploadedFirmware(this.pageNo, this.size);
    }

    paginateUnPythonList(event) {
        this.pageunNo = event.page + 1;
        if (this.currentUrl == '/upload/python')
            this.getUnassignedPython(this.pageunNo, this.size);
        else
            this.getUploadedunFirmware(this.pageunNo, this.size);
    }

    getPythonUploadFile(pageNo, size) {
        Helpers.setLoading(true);
        var tempArray = [];
        var tempUnassigned = [];
        var tempAssigned = [];
        this.unassignedList = [];
        this.uploadedList = [];
        this.UploadFileService.getUploadedPythonList(pageNo, size).then(data => {
            Helpers.setLoading(false);
            tempArray = data.files;
            this.uploadedList = tempArray;
            this.asCount = data.count;

        })
    }

    getUnassignedPython(pageunNo, size) {
        Helpers.setLoading(true);
        var tempArray = [];
        var tempUnassigned = [];
        var tempAssigned = [];
        this.unassignedList = [];
        this.uploadedList = [];
        this.UploadFileService.getUploadedUnassignedPythonList(pageunNo, size).then(data => {
            Helpers.setLoading(false);
            tempArray = data.files;
            this.unCount = data.count;
            this.unassignedList = tempArray

        })
    }

    redirectAssign(file) {
        this._router.navigate(['rdt/assign'], { queryParams: { rdtId: file.id, from: 'upload', type: file.fileType } })
    }

    redirectUnAssign(file) {
        this._router.navigate(['rdt/details'], { queryParams: { rdtId: file.id, from: 'upload-unassign', type: file.fileType } })
    }


    getUploadedFirmware(pageNo, size) {
        Helpers.setLoading(true);
        var tempArray = [];
        var tempUnassigned = [];
        var tempAssigned = [];
        this.unassignedList = [];
        this.uploadedList = [];
        this.UploadFileService.getUploadedFirmwareList(pageNo, size).then(data => {
            Helpers.setLoading(false);
            tempArray = data.files;
            this.asCount = data.count;
            this.uploadedList = tempArray;

        })
    }

    getUploadedunFirmware(pageunNo, size) {
        Helpers.setLoading(true);
        var tempArray = [];
        var tempUnassigned = [];
        var tempAssigned = [];
        this.unassignedList = [];
        this.uploadedList = [];
        this.UploadFileService.getUploadedUnassignedFirmwareList(pageunNo, size).then(data => {
            Helpers.setLoading(false);
            tempArray = data.files;
            this.unassignedList = tempArray
            this.unCount = data.count;
        })
    }

    showDevice(file) {
        this._router.navigate(['rdt/details'], { queryParams: { rdtId: file.id, from: 'upload', type: file.fileType } });
    }

    onOrgChange(orgId: string): void {
        this.deviceList = [];
        this.noHub = false;
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers[0].id !== "ALL") {
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                }
                this.selectedCenter.id = this.centers[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
        if (this.selectedFacility.id) {
            this.getFacilitydevices()
        }
    }

    centerChange(centerId) {
        this.deviceList = [];
        if (centerId) {
            this.centerMsg = false
            this.selectedCenter.id = centerId;
            // console.log('center', this.selectedCenter.id)
            this.getdevices()
        }
        if (centerId == 'ALL')
            this.getFacilitydevices();
    }

    checkLength(text) {
        if (text.length == 32) {
            this.lengthCheck = false;
        }
        else
            this.lengthCheck = true;
    }

    public filesSelect(selectedFiles: Ng4FilesSelected): void {
        this.firmwareAlert = false;
        this.pyAlert = false;
        this.fileWarning = false;
        if (selectedFiles.status === Ng4FilesStatus.STATUS_MAX_FILES_COUNT_EXCEED) {
            this.uploadMsg = 'File Count Exceed';
        }
        if (selectedFiles.status === Ng4FilesStatus.STATUS_MAX_FILES_TOTAL_SIZE_EXCEED) {
            this.uploadMsg = 'Max Total File Size Exceed';
        }
        if (selectedFiles.status === Ng4FilesStatus.STATUS_MAX_FILE_SIZE_EXCEED) {
            this.uploadMsg = 'Max File Size Exceed';
        }
        if (selectedFiles.status === Ng4FilesStatus.STATUS_NOT_MATCH_EXTENSIONS) {
            this.uploadMsg = 'Extension Not Matched';
        }
        if (selectedFiles.status === Ng4FilesStatus.STATUS_SUCCESS) {
            // Helpers.setLoading(true);
            let formData: FormData = new FormData();
            this.selectedFiles = Array.from(selectedFiles.files).map(file =>
                formData.append('upfile', file)
            );
            if (formData) {
                this.dataTosend = formData;
            }
            if (selectedFiles.files[0].name) {
                // console.log(selectedFiles.files)
                this.fileName = selectedFiles.files[0].name
            }
            if (this.checksum && this.checksum.length == 32) {
                if (this.currentUrl == '/upload/python') {
                    if (Utils.getExtension(selectedFiles.files[0].name) == 'py') {
                        document.getElementById('step_two').style.backgroundColor = '#ffb822'
                        this.extension = 'Python'
                    } else {
                        Helpers.setLoading(false);
                        document.getElementById('step_two').style.backgroundColor = 'gray'
                        this.pyAlert = true;
                    }
                }

                if (this.currentUrl == '/upload/firmware') {
                    if (Utils.getExtension(selectedFiles.files[0].name) == '') {
                        document.getElementById('step_two').style.backgroundColor = '#ffb822'
                        this.extension = 'FIRMWARE'

                    } else {
                        Helpers.setLoading(false);
                        document.getElementById('step_two').style.backgroundColor = 'gray'
                        this.firmwareAlert = true;
                    }
                }
            } else {
                Helpers.setLoading(false);
                this.errorCheck = true;
            }

        }
    }

    nextTab() {
        document.getElementById('drag_drop').style.display = 'block'
        document.getElementById('checksum_id').style.display = 'none'
        document.getElementById('step_one').style.backgroundColor = '#34bfa3'
    }

    preTab() {
        document.getElementById('drag_drop').style.display = 'none'
        document.getElementById('checksum_id').style.display = 'block'
    }

    closeModal() {
        this.displayUploadModal = false;
        this.checksum = null;
        this.dataTosend = null;
        this.fileName = null;
        let formData: FormData = new FormData();
    }

    uploadCheck() {
        if (this.dataTosend) {
            if (this.extension == 'Python') {
                this.assignPython(this.dataTosend)
            }
            if (this.extension == 'FIRMWARE') {
                this.assignFirmWare(this.dataTosend)
            }
        } else {
            this.fileWarning = true;
        }

    }


    assignPython(data) {
        Helpers.setLoading(true);
        this.UploadFileService.uploadFile(data, this.checksum).then(data => {
            Helpers.setLoading(false);
            this.pageunNo = 1;
            this.size = 10;
            document.getElementById('step_two').style.backgroundColor = '#34bfa3'
            this.checksum = "";
            setTimeout(() => {
                this.displayUploadModal = false;
                this.closeModal();
            }, 1000)
            this.msgSucess = true;
            setTimeout(() => {
                this.msgSucess = false;
            }, 3000);
            if (data) {
                this.getUnassignedPython(this.pageunNo, this.size);
            }

        }, error => {
            this.displayUploadModal = false;
            this.checksum = "";
            Helpers.setLoading(false);
            if (error.status == 400) {
                this.errorMsg = true;
                setTimeout(() => {
                    this.errorMsg = false;
                }, 3000);
            }

        });
    }

    tabClick(type) {
        if (type == 'assigned') {
            if (this.currentUrl == '/upload/python')
                this.getPythonUploadFile(this.pageNo, this.size);
            else
                this.getUploadedFirmware(this.pageNoFirm, this.size);
        } else {
            if (this.currentUrl == '/upload/python')
                this.getUnassignedPython(this.pageunNo, this.size);
            else
                this.getUploadedunFirmware(this.pageunNo, this.size);
        }
    }

    assignFirmWare(formData) {
        Helpers.setLoading(true);
        this.UploadFileService.uploadFirmWareFile(formData, this.checksum).then(data => {
            Helpers.setLoading(false);
            document.getElementById('step_two').style.backgroundColor = '#34bfa3'
            this.checksum = "";
            setTimeout(() => {
                this.displayUploadModal = false;
                this.closeModal();
            }, 1000)
            this.msgSucess = true;
            setTimeout(() => {
                this.msgSucess = false;
            }, 3000);
            if (data) {
                this.getUploadedunFirmware(this.pageunNo, this.size);
            }

        }, error => {
            this.displayUploadModal = false;
            this.checksum = "";
            Helpers.setLoading(false);
            if (error.status == 400) {
                this.errorMsg = true;
                setTimeout(() => {
                    this.errorMsg = false;
                }, 3000);
            }

        });
    }

    paginate(event) {
        // console.log('paginate', event)
    }


    filterTestType(event) {
        this.filteredTestType = this.testType.filter(x => (x.testName).toLowerCase().indexOf((event.query).toLowerCase()) === 0);
    }

    filterRAType(event) {
        this.filteredRAType = this.raType.filter(x => (x.raName).toLowerCase().indexOf((event.query).toLowerCase()) === 0);
    }


    filterFacility(event) {
        this.filteredFacility = [];
        this.filteredCenter = [];
        this.selectedCenter = ''
        this.deviceList = [];
        this.checkAll = false;
        this.filteredFacility = this.facilities.filter(x => (x.name).toLowerCase().indexOf((event.query).toLowerCase()) === 0)
        // console.log('fa', this.filteredFacility)

    }
    filterCenter(event) {
        this.checkAll = false;
        this.filteredCenter = [];
        this.deviceList = [];
        this.filteredCenter = this.centers.filter(x => (x.name).toLowerCase().indexOf((event.query).toLowerCase()) === 0)
    }

    getCenters() {
        this.centers = this.selectedFacility.centers;
    }

    getdevices() {
        this.deviceList = [];
        this.noHub = false;
        Helpers.setLoading(true);
        this.DevicesService.getDevicesDate(0, new Date().getTime(), "CENTER", this.selectedCenter.id).then(response => {
            Helpers.setLoading(false);
            if (response.length != 0)
                this.deviceList = response.devices;
            else {
                this.noHub = true;
                document.getElementById('hub_table').style.height = '770px'
            }

        })
    }

    getFacilitydevices() {
        this.deviceList = [];
        this.noHub = false;
        Helpers.setLoading(true);
        this.DevicesService.getDevicesDate(0, new Date().getTime(), "FACILITY", this.selectedFacility.id).then(response => {
            Helpers.setLoading(false);
            if (response.length != 0)
                this.deviceList = response.devices;
            else {
                document.getElementById('hub_table').style.height = '770px'
                this.noHub = true;
            }
        })
    }

    oncheckAll(): void {
        this.checkAll = !this.checkAll;
        this.deviceList.forEach((device, i) => {
            device.selected = this.checkAll;
        })
    }

    dataPaginate(event) {
        // console.log('data co', event.srcElement['computedName'])
        this.pageNo = event.srcElement['computedName']
        this.getPythonUploadFile(this.pageNo, this.size)
    }

    paginateFirmwareList(event) {
        this.pageNo = event.page + 1;
        this.getUploadedFirmware(this.pageNo, this.size)
    }

    onDeviceCheck(device): void {
        this.hubMsg = false;
        document.getElementById('hub_table').style.height = '730px'
        device.selected = !device.selected;
        let allDevicesSelected = true;
        this.deviceList.forEach((device, i) => {
            if (!device.selected)
                allDevicesSelected = false;
        });
        if (allDevicesSelected)
            this.checkAll = true;
        else
            this.checkAll = false;
    }

    UploadAction() {
        this.selectedId = [];
        this.deviceList.forEach((device, i) => {
            if (device.selected) {
                this.selectedId.push(device.id)
            }
        });
        if (this.selectedCenter.id != 'ALL') {
            if (this.selectedId.length) {
                this.displayUploadModal = true;
            }
            else {
                this.msgs = [];
                this.hubMsg = true;
            }
        } else
            this.centerMsg = true;

    }

    uploadActions() {
        this.displayUploadModal = true;
        this.errorCheck = false;
        document.getElementById('step_one').style.backgroundColor = '#ffb822'
        document.getElementById('step_two').style.backgroundColor = 'gray'
        this.preTab();
    }

    onFileSelected(file) {
        this.assign = true;
        // console.log('file', file)
        this.selectedFile = file;
        // console.log('conactted', this.selectedFile);
    }


    prepareData() {
        let self = this;
        self.selectedId = [];
        let newCenter = [];
        let newFacility = [];
        var dummy = [];
        var dummyArray = [];
        var fac = [];
        var dummyFacArray = [];
        self.deviceList.forEach((device, i) => {
            if (device.selected) {
                self.selectedId.push(device.id)
            }
        });
        if (self.selectedFile.deviceId) {
            dummyArray = self.selectedId.concat(self.selectedFile.deviceId)
        }

        if (dummyArray.length != 0) {
            var names_array_new = dummyArray.reduceRight(function(r, a) {
                r.some(function(b) {
                    if (a === b) {
                        dummy.push(a);
                        return dummy;
                    }
                }) || r.push(a); return r;

            }, []);
            // console.log('array', names_array_new)
            self.selectedId = names_array_new;
        }

        if (self.selectedFile.facilityId) {
            self.selectedFile.facilityId.forEach(id => {
                newFacility.push(id);
            });
        }

        if (self.selectedFile.centerId) {
            self.selectedFile.centerId.forEach(id => {
                if (id != self.selectedCenter.id) {
                    newFacility.push(self.selectedFacility.id)
                    newCenter.push(id)
                }
                //  else{
                //     self.selectedFile.facilityId.forEach(faci => {
                //             if(faci == self.selectedFacility.id) 
                //             newFacility.splice(self.selectedFacility.id)   
                //     });                            
                // }              
            });
        }

        dummyFacArray = newFacility;

        if (dummyFacArray.length != 0) {
            var names_array_fac = dummyFacArray.reduceRight(function(r, a) {
                r.some(function(b) {
                    if (a === b) {
                        fac.push(a);
                        return fac;
                    }
                }) || r.push(a); return r;

            }, []);
            newFacility = names_array_fac;
        }
        if (self.selectedCenter.id && self.selectedFacility.id && self.selectedId) {
            newCenter.push(self.selectedCenter.id)
            newFacility.push(self.selectedFacility.id)
        }


        let parsedData = self.selectedFile;
        parsedData.centerId = newCenter;
        parsedData.facilityId = newFacility;
        parsedData.deviceId = self.selectedId;
        parsedData.assignmentStatus = "ASSIGNED";
        return parsedData;
    }

    AddRDT() {
        this.CommonService.getTestType().then(testTypeData => this.testTypeList = testTypeData)
            .then(() => this.displayRDTModal = true)
    }

    charCheck(event) {
        return !Utils.specialCharacter(event.key);
    }

    check(event) {
        this.errorCheck = false
        this.checksum = event.target.value.replace(/ /g, '')
    }
}