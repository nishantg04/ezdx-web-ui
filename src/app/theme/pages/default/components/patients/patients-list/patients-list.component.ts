import { Component, OnInit, ViewEncapsulation, AfterViewInit } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Helpers } from "../../../../../../helpers";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";
import { TranslateService } from '@ngx-translate/core';
import { Patient } from "../patient.modal";
import { PatientsService } from "../patients.service";
import { Facility } from "../../facilities/facility";
import { FacilitiesService } from "../../facilities/facilities.service";
import { Center } from "../../facilities/center";
import { UsersService } from "../../users/users.service";
import { concat } from "rxjs/observable/concat";
import * as constant from '../constants.enum';
import { fail } from "assert";

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./patients-list.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None
})

export class PatientsListComponent implements OnInit, AfterViewInit {

    centersLabel: { label: string, value: any }[] = [];
    endDate: Date;
    startDate: Date;
    searchKey: string = '';
    errMsgLimitCard: boolean = false;
    errMsgSelectPatient: boolean = false;
    objectURL: string = "";
    fileBlob: any;
    showPdfModel: boolean = false;
    patientCount: number = 0;
    visibilePagination: boolean = false;
    today: Date = new Date();
    userType: string;
    userInfo: any;
    pageNo: number = 1;
    limit: number;
    patients: Patient[];
    facilities: Facility[];
    selectedFacility: Facility;
    selectedCenter: Center;
    centers: Center[];
    years: any[];
    selectedYear: any;
    fromDate: string;
    toDate: string;
    userBulkList: any[];
    idList: any = [];
    dummyArray: any = [];
    selectedPatient: any;
    ranges: { label: String, value: Number }[];
    row: number;
    pageSize: number;
    facilitiesLabel: { label: string, value: any }[] = [];
    SearchBylist: any;
    selectedSearchBy: any;
    showType: boolean = false;
    showUser: boolean = false;
    searchUser: any;
    isFilter: boolean = false;
    hideFilter: boolean = false;
    searchKeyUser: any;
    visiblepagination : boolean = false;
    dropChange : boolean = false;
    iskey : boolean = false;
    isOperator : boolean = false;

    constructor(private _patientsService: PatientsService, private _script: ScriptLoaderService, private _router: Router, private translate: TranslateService,
        private _activeroute: ActivatedRoute, private _domSanitizer: DomSanitizer, private _facilitiesService: FacilitiesService, private _usersService: UsersService) {
        this.selectedFacility = new Facility();
        this.ranges = [
            { label: "10", value: 10 },
            { label: "25", value: 25 },
            { label: "50", value: 50 },
            { label: "100", value: 100 },
        ];
        this.row = 10;
        this.pageNo = 1;
        this.limit = 10;
        this.selectedCenter = new Center();
        this.SearchBylist = [
            { id: '0', name: 'ALL', keytoSend: 'ALL' },
            { id: '1', name: 'USER', keytoSend: 'USER' },
            { id: '2', name: 'MRN', keytoSend: 'TYPE' }  // Key to send should be match with service call
        ]
        this.selectedSearchBy = this.SearchBylist[0];
        // this.showUser = true;
        if (window.localStorage.getItem("userType") == "Facility")
            this.userType = "Facility";
        else if (window.localStorage.getItem('userType') == 'Center')
            this.userType = 'Center';
        else if (window.localStorage.getItem('userType') == 'User') {
            this.hideFilter = true;
            this.userType = 'User';
            this.SearchBylist = [
                { id: '0', name: 'ALL', keytoSend: 'ALL' },
                { id: '1', name: 'MRN', keytoSend: 'TYPE' }
            ]
            this.selectedSearchBy = this.SearchBylist[0];
        }
        else
            this.userType = "Admin";
    }

    ngOnInit() {
        this.getFacilities();
    }

    ngAfterViewInit() {

    }

    select(e) {
        this.visibilePagination = false;
        this.limit = e.value;
        this.pageNo = 1;
        this.row = e.value;
        this.dropChange = true
        this.patientsListRefresh();
    }


    patientsListRefresh(): void {
       // Helpers.setLoading(true);  
        this.visiblepagination = false;       
        setTimeout(() => this.visiblepagination = true, 0);     
        if(!this.dropChange && !this.iskey){
            this.selectedSearchBy = this.SearchBylist[0];
            this.showUser = false;
            this.showType = false;           
        }      
        this.functionChecking();
    }

    functionChecking(){
        if (this.searchKey && this.searchKeyUser == undefined) {
            if (this.selectedCenter.id && this.selectedCenter.id != "ALL" && this.selectedFacility.id) {
                this.getPatientsOnMrnOfCenter(this.searchKey, this.selectedFacility.id, this.selectedCenter.id);
            } else if (this.selectedFacility.id && this.selectedFacility.id != "ALL") {
                this.getPatientsOnMrnOfFacility(this.searchKey, this.selectedFacility.id);
            } else if (this.userType === 'Admin') {
                this.getPatientsOnMrn(this.searchKey);
            }else if(this.userType === 'User'){
                var user = JSON.parse(window.localStorage.getItem('userInfo'))
                this.getPatientsOnMrnOfUser(this.searchKey,user.id)
            }
        }
        else if (this.searchKeyUser && !this.searchKey) {
            this.userChange(this.searchKeyUser);
        }
        else {
            if (this.selectedCenter.id && this.selectedCenter.id != "ALL" && this.selectedFacility.id) {
                this.getPatientsOfCenter();
            } else if (this.selectedFacility.id && this.selectedFacility.id != "ALL") {
                this.getPatientsOfFacility();
            } else if (this.userType === 'Admin') {
                this.getAllPatients();
            }
            else if (this.userType === 'User') {
                var user = JSON.parse(window.localStorage.getItem('userInfo'))
                this.getPatientUserAll(user.id)
            }
        }
    }

    setDateLimits() {
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - 29);
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
        startDate.setMilliseconds(0);
        this.startDate = startDate;
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        endDate.setHours(23);
        endDate.setMinutes(23);
        endDate.setSeconds(23);
        endDate.setMilliseconds(0);
        this.endDate = endDate;
    }

    getFacilities(): void {
        this.facilities = JSON.parse(localStorage.getItem("facilityTable"));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            } else if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        if (this.userType == 'Admin') {
            this.facilitiesLabel = [];
            this.centersLabel = [];
            this.facilities.forEach(facility => {
                this.facilitiesLabel = [
                    ...this.facilitiesLabel,
                    { label: facility.name, value: facility }
                ];
            });
            let facilityAll = new Facility();
            facilityAll.id = "ALL";
            facilityAll.name = "ALL";
            this.facilitiesLabel.unshift({ label: facilityAll.name, value: facilityAll });
            this.selectedFacility = this.facilitiesLabel[0].value;
            this.centersLabel = [];
            let centerAll = new Center();
            centerAll.id = "ALL";
            centerAll.name = "ALL";
            this.centersLabel.unshift({ label: centerAll.name, value: centerAll });
            this.selectedCenter = this.centersLabel[0].value;
            this.getAllPatients();
        }
        else if (this.userType == 'Center') {
            var user = JSON.parse(window.localStorage.getItem('userInfo'))
            this.selectedCenter.id = user.centerId;
            this.selectedFacility.id = user.facilityId;
            this.hideFilter = true;
            this.patientsListRefresh();
        }
        else if (this.userType == 'User') {
            var user = JSON.parse(window.localStorage.getItem('userInfo'))
            this.getPatientUserAll(user.id)
        }
        else {
            const userFacility = JSON.parse(window.localStorage.getItem('userInfo')).facilityId;
            this.facilities.forEach(facility => {
                if (facility.id === userFacility) {
                    this.selectedFacility = facility;
                    this.centers = jQuery.extend(true, [], facility.centers);
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                    this.selectedCenter.id = this.centers[0].id;
                    if (this.selectedFacility.centers.length != 0) {
                        this.selectedFacility.centers.forEach(center => {
                            this.centersLabel = [
                                ...this.centersLabel,
                                { label: center.name, value: center }
                            ];
                        });
                        let centerAll = new Center();
                        centerAll.id = "ALL";
                        centerAll.name = "ALL";
                        this.centersLabel.unshift({ label: centerAll.name, value: centerAll });
                        this.selectedCenter = this.centersLabel[0].value;
                    }
                }
            });
            this.patientsListRefresh();
        }
    }


    onFacilityChange() {
        this.centersLabel = [];
        this.selectedCenter;
        if (this.selectedFacility.centers.length > 0) {
            this.selectedFacility.centers.forEach(center => {
                this.centersLabel = [
                    ...this.centersLabel,
                    { label: center.name, value: center }
                ];
            });
            let centerAll = new Center();
            centerAll.id = "ALL";
            centerAll.name = "ALL";
            this.centersLabel.unshift({ label: centerAll.name, value: centerAll });
            this.selectedCenter = this.centersLabel[0].value;
        }
    }

    paginate(event) {
        //Helpers.setLoading(true);       
        this.pageNo = event.page + 1;
        this.limit = parseInt(event.rows);
       // this.patientsListRefresh();
       this.functionChecking();
    }

    onFilterClick() {
        this.pageNo = 1;
        this.isFilter = true;
        this.patientsListRefresh();
    }

    onFilterClickchanged() {
        this.pageNo = 1;
        this.searchKeyUser = undefined;
        this.searchKey = undefined;
        this.patientsListRefresh();
    }


    downloadPDF() {
        if (this.idList.length != 0) {
            this.getRegistrationCards(this.idList)
        } else {
            this.errMsgLimitCard = true;
            setTimeout(() => {
                this.errMsgLimitCard = false;
            }, 3000);
        }
    }

    onCheck(event, patient) {
        event.preventDefault();
        patient.selected = event.target.checked;
        if (patient.selected == true)
            this.dummyArray.push(patient.id)
        if (patient.selected == false) {
            var index = this.dummyArray.indexOf(patient.id);
            this.dummyArray.splice(index, 1);
        }
        this.idList = this.dummyArray;
    }

    onKeyupPatientSearch(event) {
        if (this.searchKey.length >= 3 || this.searchKey.length == 0){
            this.iskey = true
            this.patientsListRefresh();
        }
            
    }

    downloadCopy() {
        var a = document.createElement("a");
        a.href = this.objectURL;
        a.download = "Health-cards.pdf";
        document.body.appendChild(a);
        a.click();
        this.showPdfModel = false;
    }

    getAllPatients(): void {
        this._patientsService.getPatientList(constant.ALL, null, null, null, this.pageNo, this.limit).then(
            res => {
                Helpers.setLoading(false);
                if (res != null) {
                    this.setPrintHistory(res.patients);
                    this.patientCount = res.count;
                    this.visiblepagination = true;
                } else {
                    this.patientCount = 0;
                    this.patients = [];
                }
            })
    }

    setPrintHistory(patients: any[]): void {
        this.patients = [];
        if (patients) {
            patients.forEach(patient => {
                if (patient.printHistory)
                    patient.printHistory = patient.printHistory[patient.printHistory.length - 1].timeofPrinting
                this.patients.push(patient);
            });
        } else {
            this.patients = [];
        }
        Helpers.setLoading(false);
    }

    getPatientsOfCenter(): void {
        this._patientsService.getPatientList(constant.CENTER, this.selectedFacility.id, this.selectedCenter.id, null, this.pageNo, this.limit).then(
            res => {
                Helpers.setLoading(false);
                if (res != null) {
                    this.setPrintHistory(res.patients);
                    this.patientCount = res.count;
                    this.visiblepagination = true;
                } else {
                    this.patientCount = 0;
                    this.patients = [];
                }
            })
    }

    getPatientsOfFacility(): void {
        this._patientsService.getPatientList(constant.FACILITY, this.selectedFacility.id, null, null, this.pageNo, this.limit).then(
            res => {
                Helpers.setLoading(false);
                if (res != null) {
                    this.setPrintHistory(res.patients);
                    this.patientCount = res.count;
                    this.visiblepagination = true;
                } else {
                    this.patientCount = 0;
                    this.patients = [];
                }
            })
    }

    getPatientsOnMrn(mrn): void {
        this._patientsService.getPatientByMrn('MRN', mrn, this.pageNo, this.limit).then(
            res => {
                Helpers.setLoading(false);
                if (res != null) {
                    this.setPrintHistory(res.patients);
                    this.patientCount = res.count;
                    this.visiblepagination = true;
                } else {
                    this.patientCount = 0;
                    this.patients = [];
                }
            })
    }

    getPatientsOnMrnOfFacility(mrn, facilityId): void {
        this._patientsService.getPatientOfMrnByCenter(facilityId, null, mrn, this.pageNo, this.limit).then(res => {
            Helpers.setLoading(false);
            if (res != null) {
                this.setPrintHistory(res.patients);
                this.patientCount = res.count;
                this.visiblepagination = true;
            } else {
                this.patientCount = 0;
                this.patients = [];
            }
        })
    }

    getPatientsOnMrnOfUser(mrn, userId): void {
        this._patientsService.getPatientMrnByUser(userId, mrn, this.pageNo, this.limit).then(res => {
            Helpers.setLoading(false);
            if (res != null) {
                this.setPrintHistory(res.patients);
                this.patientCount = res.count;
                this.visiblepagination = true;
            } else {
                this.patientCount = 0;
                this.patients = [];
            }
        })
    }


    getPatientsOnMrnOfCenter(mrn, facilityId, centerId): void {
        this._patientsService.getPatientOfMrnByCenter(facilityId, centerId, mrn, this.pageNo, this.limit).then(res => {
            Helpers.setLoading(false);
            if (res != null) {
                this.setPrintHistory(res.patients);
                this.patientCount = res.count;
                this.visiblepagination = true;
            } else {
                this.patientCount = 0;
                this.patients = [];
            }
        }
        )
    }

    getRegistrationCards(patientsIds): void {
        let creator: any = JSON.parse(window.localStorage.getItem("userInfo"));
        Helpers.setLoading(true);
        let idGenPatients = [];
        idGenPatients = this.idList;
        if (idGenPatients.length != 0) {
            this._patientsService.getPdf(idGenPatients, creator.id, creator.firstName)
                .then(res => {
                    this.objectURL = URL.createObjectURL(res);
                    this.showPdfModel = true;
                    this.idList = [];
                    this.dummyArray = [];
                    this.patientsListRefresh();
                    Helpers.setLoading(false);
                });
        }
    }

    getfolder(e) {
        var files = e.target.files;
        var path = files[0].webkitRelativePath;
        var Folder = path.split("/");
    }

    showPatientsVisitsLog(patientId): void {
        this._router.navigate(['/patients/log'], { queryParams: { patientId: patientId } });
    }

    facilityChange(value) {
        this.selectedFacility = value;
        this.selectedFacility.organizationCode = value.organizationCode;
        this.dropChange = false;
        this.centers = value.centers;
        if(this.selectedFacility.id != 'ALL'){
            if (this.centers[0].id !== "ALL") {
                let allCenter = new Center();
                allCenter.name = "ALL";
                allCenter.id = "ALL";
                this.centers.splice(0, 0, allCenter);
            }
            this.selectedCenter.id = this.centers[0].id;
            this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
        }else{
            this.selectedCenter.id = undefined;
        }
    }

    onChangeSeachBy(event) {
        this.searchKey = undefined;
        this.searchKeyUser = undefined;
        if (event.keytoSend == 'TYPE') {
            this.showType = true
            this.showUser = false;
            this.iskey = false;           
        }
        else if (event.keytoSend == 'ALL') {
            this.showUser = false;
            this.showType = false;
            this.iskey = false;
            this.pageOne('ALL')
            this.patientsListRefresh();
        }
        else {
            this.showType = false
            this.showUser = true;            
        }
    }

    userChange(event) {
        // this.pageNo = 1;
        if (this.selectedFacility.id == 'ALL' && this.selectedCenter.id == 'ALL')
            this.getPatientUserAll(event.id)
        else
            this.getPatientByCenterFac(event.id)
        this.searchKeyUser = event
    }

    pageOne(type){   
        if(type == 'MRN')    {
            if(this.searchKey.length >=3){
                this.pageNo=1;
                this.visiblepagination = false;       
                setTimeout(() => this.visiblepagination = true, 0);
            }
        }else{
            this.pageNo=1;
            this.visiblepagination = false;       
            setTimeout(() => this.visiblepagination = true, 0);
        }        
    }

    getPatientUserAll(id) {
        this._patientsService.getPatientByUserAll(id, this.pageNo, this.limit)
            .then((res) => {
                if (res != null) {
                    this.setPrintHistory(res.patients);
                    this.patientCount = res.count;
                    this.visiblepagination = true;
                } else {
                    this.patientCount = 0;
                    this.patients = [];
                }
            })
    }

    getPatientByCenterFac(id) {
        this._patientsService.getPatientByUserFacCenter(id, this.selectedFacility.id, this.selectedCenter.id, this.pageNo, this.limit)
            .then((res) => {
                if (res != null) {
                    this.setPrintHistory(res.patients);
                    this.patientCount = res.count;
                    this.visiblepagination = true;
                } else {
                    this.patientCount = 0;
                    this.patients = [];
                }
            })
    }

}
