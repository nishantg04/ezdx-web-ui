import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';

import { Device } from '../device';
import { Center } from '../../facilities/center';
import { Facility } from '../../facilities/facility';
import { RdtService } from '../rdt.service';
import { Observable } from 'rxjs/Observable';
import { Utils } from '../../utils/utils'
import { RDTLookupJson } from '../rdtFile';
import { RDTEntity } from '../RDTEntity';
import { RdtLookup } from '../rdtLookup';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./rdt-add.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})

export class RdtAddComponent implements OnInit, AfterViewInit {
    hasUnderscore: boolean = false;
    RDTId: string;
    creatingCustome: boolean = false;
    assignHub: boolean;
    assignurl: any;
    listOfRdtFiles: RdtLookup[];
    rdts: RDTEntity[];
    rdtLookupJson: RDTLookupJson;
    textBody: boolean = false;
    textTemplate: boolean = true;

    constructor(private _rdtService: RdtService, private _router: Router, private _activeroute: ActivatedRoute) { }
    ngOnInit() {
        this.RDTId = this._activeroute.snapshot.queryParams['rdtId'];
        this.rdtLookupJson = new RDTLookupJson();
        //this.listLookupFiles();
        this.getRdtLookup();
    }
    ngAfterViewInit() { }

    getRdtLookup() {
        this._rdtService.getDefaultLookup().then(res => {
            this._rdtService.downloadFile(res.files[0].id).then(resDownload => {
                // console.log("json file downloaded: ", res);
                this.rdts = resDownload;
            });
            // console.log("RDTs: ", this.rdts);
        })
    }

    pushToServer() {
        this.rdtLookupJson.rdtLookups = this.rdts;
        this._rdtService.createRdtJson(this.rdtLookupJson).then((res) => {
            // console.log("res: ", res);
            Helpers.setLoading(false);
            //this._router.navigate(['rdt/list/RL']);
            this._router.navigate(['rdt/list'], { queryParams: { type: 'RL' } });
        });
    }

    onKeyUpEvent() {
        if (RegExp(/_/).test(this.rdtLookupJson.fileName)) {
            this.hasUnderscore = true;
        } else {
            this.hasUnderscore = false;
        }
    }
}