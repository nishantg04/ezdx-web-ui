#!/bin/bash
set -x #echo on
VERSION=$1
PROJECT=$2
FILE=docker-compose.yml
USERNAME=hcadmin
HOSTS="dev-ezdx.healthcubed.com"
SCRIPT="sudo sed -i 's/nexus.widas.de:18444\/$PROJECT:.*/nexus.widas.de:18444\/$PROJECT:'$VERSION'/' $FILE; sudo docker login -u=devel -p=widas-devel nexus.widas.de:18444; sudo docker-compose up -d"
for HOSTNAME in ${HOSTS} ; do
    ssh -i ./id_rsa -o StrictHostKeyChecking=no -l ${USERNAME} ${HOSTNAME} "${SCRIPT}"
done
