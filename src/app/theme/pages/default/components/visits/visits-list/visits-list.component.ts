import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { Visit } from '../visit';
import { Test } from '../test';
import { VisitsService } from '../visits.service';
import { Facility } from '../../facilities/facility';
import { FacilitiesService } from '../../facilities/facilities.service';
import { UsersService } from '../../users/users.service';
import { Center } from '../../facilities/center';
import { AnimationKeyframesSequenceMetadata } from '@angular/core/src/animation/dsl';
import { concat } from 'rxjs/observable/concat';
import { MapLoaderService } from '../../utils/map-loader.service';
import * as _ from "lodash";

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./visits-list.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ["../../../primeng.component.scss"],
})
export class VisitsListComponent implements OnInit, AfterViewInit {
    userType: string;
    userInfo: any = {};
    visits: any[];
    tests: any[];
    facilities: Facility[];
    selectedFacility: Facility;
    selectedCenter: Center;
    centers: Center[];
    years: any[];
    selectedYear: any;
    fromDate: string;
    toDate: string;
    facilityId: string;
    centerId: string;
    userBulkList: any[];
    pageNo: number;
    size: number;
    visitCount: number;
    visiblepagination: boolean = false;
    filterClicked: false;
    showDialog: boolean = false;
    testData: any;
    testIconUrl: any;
    showResult: any;
    options: any;
    overlays: any[];
    map: google.maps.Map;
    marker: any;
    bounds: any;
    ranges: any[];
    row: number;
    showLocation: boolean = false;
    types: any[] = [];
    typeLabels: any[] = [];
    selectedType: any;
    chosen: string;
    testChoosen: string;
    userChoosen: string;
    showSpinner: boolean = false;
    showsucc: boolean = false;
    SearchBylist: any;
    selectedSearchBy: any;
    showType: boolean = false;
    showUser: boolean = false;
    searchUser: any;
    isFilter: boolean = false;
    resNull: boolean = false;
    searching: boolean = false;
    searchTestKey: any;
    searchNameKey: any;
    perPage : any[] =[];
    dropChange : boolean = false;

    constructor(private _visitsService: VisitsService, private _script: ScriptLoaderService, private _router: Router, private mapLoaderService: MapLoaderService,
        private _activeroute: ActivatedRoute, private _facilitiesService: FacilitiesService, private _usersService: UsersService, private translate: TranslateService) {
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        this.ranges = [
            { label: "10", value: 10 },
            { label: "25", value: 25 },
            { label: "50", value: 50 },
            { label: "100", value: 100 },
        ];
        this.perPage.push(10,25,50,100)
        this.types = [
            { label: 'ALL', value: 'ALL' },
            { label: 'BLOOD_PRESSURE', value: 'BLOOD_PRESSURE' },
            { label: 'BLOOD_GLUCOSE_NON_FASTING', value: 'BLOOD_GLUCOSE_NON_FASTING' },
            { label: 'BLOOD_GLUCOSE_FASTING', value: 'BLOOD_GLUCOSE_FASTING' },
            { label: 'CHOLESTEROL', value: 'CHOLESTEROL' },
            { label: 'HEAMOGLOBIN', value: 'HEAMOGLOBIN' },
            { label: 'HEP_B', value: 'HEP_B' },
            { label: 'MALARIA', value: 'MALARIA' },
            { label: 'TEMPERATURE', value: 'TEMPERATURE' },
            { label: 'TYPHOID', value: 'TYPHOID' }
        ];
        this.SearchBylist = [
            { id: '0', name: 'ALL', keytoSend: 'ALL' },
            { id: '1', name: 'USER', keytoSend: 'USER' },
            { id: '1', name: 'Diagnostic Test', keytoSend: 'TYPE' }  // Key to send should be match with service call
        ]
        this.selectedSearchBy = this.SearchBylist[0];
        //  this.showUser = true;
        this.row = 10;
        this.pageNo = 1;
        this.size = 10;
        if (window.localStorage.getItem('userType') == 'Facility')
            this.userType = 'Facility';
        else if (window.localStorage.getItem('userType') == 'Center')
            this.userType = 'Center';
        else if (window.localStorage.getItem('userType') == 'User') {
            this.userType = 'User';
            this.SearchBylist = [
                { id: '0', name: 'All', keytoSend: 'ALL' },
                { id: '1', name: 'Diagnostic Test', keytoSend: 'TYPE' }
            ]
            this.selectedSearchBy = this.SearchBylist[0];
        }
        else
            this.userType = 'Admin';
    }

    setMap(event) {
        this.map = event.map;
    }

    ngOnInit() {
        MapLoaderService.load().then(() => {
        })
        let tempArray: any = [];
        this.types.forEach((type) => {
            tempArray.push(type.label);
        });
        this.translate.stream(tempArray).subscribe((val) => {
            this.typeLabels = _.map(val, this.objectToArray);
        })
        this.options = {
            center: { lat: 36.890257, lng: 30.707417 },
            zoom: 12
        };
    }

    objectToArray(value, key) {
        return { "label": value, "value": key };
    }

    ngAfterViewInit() {
        window.localStorage.removeItem('patientStartDate');
        window.localStorage.removeItem('patientEndDate');
        this.fromDate = this._activeroute.snapshot.queryParams['from'];
        this.toDate = this._activeroute.snapshot.queryParams['to'];
        if (this._activeroute.snapshot.queryParams['facilityId'])
            this.selectedFacility.id = this._activeroute.snapshot.queryParams['facilityId'];
        if (this._activeroute.snapshot.queryParams['centerId'])
            this.selectedCenter.id = this._activeroute.snapshot.queryParams['centerId'];
        if (this._activeroute.snapshot.queryParams['userId']) {
            this.userInfo.id = this._activeroute.snapshot.queryParams['userId']
            this.userType = 'User';
        }
        if (this.fromDate != null && this.toDate != null) {
            window.localStorage.setItem('patientStartDate', this.fromDate);
            window.localStorage.setItem('patientEndDate', this.toDate);
            this.onFilterClick();
        }
        this.getFacilities();
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/datatables/base/date.patients.js');
    }

    onFilterClick(): void {
        //  this.pageNo = 1;
        // this.size = 10;
        this.isFilter = true;
        this.visiblepagination = false;       
        setTimeout(() => this.visiblepagination = true, 0);
        if(!this.dropChange){
            this.selectedSearchBy = this.SearchBylist[0];
            this.showUser = false;
            this.showType = false;
        }     
        if (this.userType == 'User') {
            this.searchUserByAll(this.userInfo.id, this.pageNo, this.size, new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime());
        } else {
            if (this.selectedFacility.id != "ALL") {
                if (this.selectedCenter.id != "ALL") {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedFacility.id, this.selectedCenter.id, this.pageNo, this.size);
                }
                else {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "FACILITY", this.selectedFacility.id, this.selectedCenter.id, this.pageNo, this.size);
                }
            }
            else {
                var endDate = new Date();
                endDate.setDate(endDate.getDate() + 1);
                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
                endDate.setMilliseconds(0);
                this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0, 0, this.pageNo, this.size);
            }
        }
    }


    select(e) {
       // this.visiblepagination = false;
        this.size = e.value;
        this.pageNo = 1;
        this.row = e.value;
        this.dropChange = true
        //this.onFilterClick();
        if (this.searchNameKey == undefined && this.searchTestKey == undefined) {
            if (this.userType == 'User') {
                this.searchUserByAll(this.userInfo.id, this.pageNo, this.size, new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime());
            }
            if (this.selectedFacility.id != "ALL") {
                if (this.selectedCenter.id != "ALL") {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedFacility.id, this.selectedCenter.id, this.pageNo, this.size);
                }
                else {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "FACILITY", this.selectedFacility.id, this.selectedCenter.id, this.pageNo, this.size);
                }
            }
            else {
                if (this.userType != 'User') {
                var endDate = new Date();
                endDate.setDate(endDate.getDate() + 1);
                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
                endDate.setMilliseconds(0);
                this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0, 0, this.pageNo, this.size);
                }
            }
        }
        if (this.searchNameKey == undefined && this.searchTestKey) {
            this.testChange(this.searchTestKey)
        }
        if (this.searchNameKey && this.searchTestKey == undefined) {
            this.userChange(this.searchNameKey)
        }
    }

    

    getFacilities(): void {

        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            } else if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - 29);
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
        startDate.setMilliseconds(0);
        var endDate = new Date();
        endDate.setHours(23);
        endDate.setMinutes(59);
        endDate.setSeconds(59);
        endDate.setMilliseconds(0);
        if (!this.fromDate && !this.toDate) {
            this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
            if (window.localStorage.getItem('userType') == 'Facility') {
                this.selectedFacility.id = this.userInfo.facilityId;
                this.facilities.forEach(fclt => {
                    if (fclt.id == this.userInfo.facilityId) {
                        this.centers = jQuery.extend(true, [], fclt.centers);
                        let allCenter = new Center();
                        allCenter.name = "ALL";
                        allCenter.id = "ALL";
                        this.centers.splice(0, 0, allCenter);
                        this.selectedCenter.id = this.centers[0].id;
                    }
                });
            }
            else if (window.localStorage.getItem('userType') == 'Center') {
                this.selectedFacility.id = this.userInfo.facilityId;
                this.selectedCenter.id = this.userInfo.centerId;
                this.centers = undefined;
            }
            else {
                let allFacility = new Facility();
                allFacility.name = "ALL";
                allFacility.id = "ALL";
                this.facilities.splice(0, 0, allFacility);
                this.selectedFacility.id = this.facilities[0].id;
                if (this.facilities[0].centers !== undefined) {
                    this.centers = this.facilities[0].centers;
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                    this.selectedCenter.id = this.centers[0].id;
                }
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        }
        else {
            let allFacility = new Facility();
            allFacility.name = "ALL";
            allFacility.id = "ALL";
            this.facilities.splice(0, 0, allFacility);
            if (window.localStorage.getItem('userType') != "Center") {
                if (this.selectedFacility.id != "ALL") {
                    this.facilities.forEach(fac => {
                        if (fac.id === this.selectedFacility.id) {
                            this.centers = fac.centers;
                            let allCenter = new Center();
                            allCenter.name = "ALL";
                            allCenter.id = "ALL";
                            this.centers.splice(0, 0, allCenter);
                        }
                    });
                }
            }
        }
        let searchcriteria = "ALL";
        if (!this._activeroute.snapshot.queryParams['from'] && !this._activeroute.snapshot.queryParams['to']) {
            this.fromDate = startDate.toString()
            this.toDate = endDate.toString()
            window.localStorage.setItem('patientStartDate', this.fromDate);
            window.localStorage.setItem('patientEndDate', this.toDate);
            if (window.localStorage.getItem('userType') === "Facility") {
                searchcriteria = "FACILITY";
                this.selectedFacility.id = this.userInfo.facilityId
                this.list(new Date(startDate).getTime(), new Date(endDate).getTime(), searchcriteria, this.selectedFacility.id, 0, this.pageNo, this.size);
            }
            else if (window.localStorage.getItem('userType') === "Center") {
                searchcriteria = "CENTER";
                this.selectedFacility.id = this.userInfo.facilityId;
                this.selectedCenter.id = this.userInfo.centerId;
                this.centers = undefined;
                this.list(new Date(startDate).getTime(), new Date(endDate).getTime(), searchcriteria, this.selectedFacility.id, this.selectedCenter.id, this.pageNo, this.size);
            }
            else if (window.localStorage.getItem('userType') == 'User') {
                this.searchUserByAll(this.userInfo.id, this.pageNo, this.size, new Date(startDate).getTime(), new Date(endDate).getTime())
            }
            else {
                this.list(new Date(startDate).getTime(), new Date(endDate).getTime(), searchcriteria, 0, 0, this.pageNo, this.size);
            }
        }
    }
    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers[0].id !== "ALL") {
                    let allCenter = new Center();
                    allCenter.name = "ALL";
                    allCenter.id = "ALL";
                    this.centers.splice(0, 0, allCenter);
                }
                this.selectedCenter.id = this.centers[0].id;
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }
    generateArray(obj) {
        return Object.keys(obj).map((key) => { return { key: key, value: obj[key] } });
    }
    list(fromDate, toDate, searchcriteria, facilityid, centerid, pageNo, size): void {
        this.visitCount = 0;
        Helpers.setLoading(true);
        this.tests = [];
        this.resNull = false;
        this._visitsService.getDiagnosisDate(fromDate, toDate, searchcriteria, facilityid, centerid, this.pageNo, this.size)
            .then(visits => {
                if (visits.length == 0) {
                    this.resNull = true;
                }else{
                    this.resNull = false
                }
                if (visits.results) {
                    this.visits = visits.results;
                    this.visitCount = visits.totalHits;
                    this.visiblepagination = true;
                } else {
                    this.visits = [];
                    this.visitCount = 0;
                }
            })
            .then(() => {
                var userIds = [];
                if (this.visits !== undefined && this.visits !== null && this.visits.length > 0) {
                    this.visits.forEach((val, i) => {
                        userIds.push(val.test.visitId);
                        // val.imei = (val.imei !== null) ? val.imei : 'No Data'
                        // val.serialNumber = (val.serialNumber !== null) ? val.serialNumber : 'No Data'
                        var newObject = val;
                        this.testResultCalculation(newObject)
                    });
                    console.log(this.tests)
                    this.tests = [...this.tests]
                }
                Helpers.setLoading(false);
            });
    }


    testResultCalculation(newObject) {
        var test = newObject.test;
        var sTime = newObject.test.startTime;
        var eTime = newObject.test.endTime;
        test.visitId = newObject.test.visitId;
        var d = new Date(test.startTime),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        test.startTime = [day, month, year].join('-') + ' (' + d.getHours() + ':' + d.getMinutes() + ')';
        var d1 = new Date(test.endTime),
            month = '' + (d1.getMonth() + 1),
            day = '' + d1.getDate(),
            year = d1.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        newObject.endTime = [day, month, year].join('-') + ' (' + d.getHours() + ':' + d.getMinutes() + ')';
        newObject.endTimeDate = d;
        newObject.sTime = sTime;
        newObject.eTime = eTime;

        var dia_result = '';
        var duration: any;

        if (test.type == "WHOLE_BLOOD_RDT") {
            if (test.mismatched) {
                if (test.machineResultValid) {
                    dia_result = test.rdtResult
                } else {
                    dia_result = test.userRdtResult
                }
            } else {
                dia_result = test.rdtResult
            }
            newObject.diaResults = dia_result;
        }

        if (test.type == "URINE_POCT") {
            if (test.mismatched) {
                if (test.machineResultValid) {
                    dia_result = test.rdtResult
                } else {
                    dia_result = test.userRdtResult
                }
            } else {
                dia_result = test.rdtResult
            }
            newObject.diaResults = dia_result;
        }

        if (newObject.sTime && newObject.eTime) {
            newObject.duration = newObject.eTime - newObject.sTime;
            newObject.duration = this.fmtMSS(newObject.duration)
        }

        if (test.name == "TEMPERATURE") { dia_result = test.result + ' ' + (test.unit ? test.unit : '') }
        else if (test.name == "BMI") { dia_result = test.bmiValue }
        else if (test.name == "PULSE_OXIMETER") { 
            //dia_result = 'SpO2: ' + test.spoValue + ' ' + test.spoUnit + '<br/>' + test.pulseRate + ' ' + test.pulseRateUnit
            var tempArray = [];
            tempArray.push('PULSE_RATE',test.pulseRateUnit)
            this.translate.stream(tempArray).subscribe((val) => {                        
                 dia_result = 'SpO2 : ' + test.spoValue + ' ' + test.spoUnit + '<br/>' + val.PULSE_RATE + ' : ' + test.pulseRate + ' '+  val[test.pulseRateUnit];
            })    
            this.translate.onLangChange.subscribe((event) => {                
                dia_result = 'SpO2 : ' + test.spoValue + ' ' + test.spoUnit + '<br/>' +  event.translations.PULSE_RATE + ' : ' + test.pulseRate + ' '+   event.translations[test.pulseRateUnit];               
                newObject.diaResults = dia_result;             
            }); 
        }
        else if (test.name == "BLOOD_PRESSURE") {
          //  dia_result = 'DIA: ' + test.dia + ' ' + test.unit + '<br/> SYS: ' + test.sys + ' ' + test.unit;
            var tempArray = [];
            tempArray.push('DIA','SYS',test.unit)
            this.translate.stream(tempArray).subscribe((val) => {                        
                 dia_result = val.DIA + ' : ' + test.dia + ' ' + val[test.unit] + '<br/>'+ val.SYS + ' ' + test.sys + ' ' + val[test.unit];               
            })    
            this.translate.onLangChange.subscribe((event) => {                        
                dia_result  =  event.translations.DIA + test.dia+ ' ' + event.translations[test.unit] + '<br/>'+ event.translations.SYS+ ' ' + test.sys + ' ' + event.translations[test.unit];                
                newObject.diaResults = dia_result;             
            }); 
            if(test.heartRate && test.heartRate != 0){
               // dia_result = dia_result + '<br/>PULSE RATE:' + test.heartRate;
                var tempArrays = [];
                tempArrays.push('PULSE_RATE')
                var heartLable = '';
                this.translate.stream(tempArrays).subscribe((val) => {                        
                    heartLable = val.PULSE_RATE + ':' + test.heartRate;
                    dia_result = dia_result + '<br/>' + heartLable
                })    
                this.translate.onLangChange.subscribe((event) => { 
                  
                    newObject.diaResults = dia_result;   
            
                });             
                 
            }
               
        } else if (test.name == "DENGUE") {
            test.name = "DENGUE ANTIGEN"
        }
        else if (test.name == "BLOOD_GROUPING") {
            // test.name = 'BLOOD GROUPING'
            dia_result = test.bloodGroupResult;
        }
        else if (test.name == "ECG") {
            dia_result = ' ';
        }
        else if (test.name == "URINE") {
            test.name = "URINE 2P"
            var protein
            //dia_result = 'Protein : ' + test.rdtResultProtein + '<br/>' + 'Glucose : ' + test.rdtResultGlucose;
            if (test.mismatched) {
                if (test.machineGlucoseResultValid) {
                    var glucose = test.rdtResultGlucose
                } else {
                    glucose = test.userRdtResultGlucose
                }
                if (test.machineProteinResultValid) {
                    protein = test.rdtResultProtein
                } else {
                    protein = test.userRdtResultProtein;
                }
                if (protein == "Plus1") protein = "+"
                if (protein == "Plus3") protein = "+++"
                if (protein == "Plus2") protein = "++"
                if (protein == "Plus4") protein = "++++"
                if (test.userRdtResultGlucose == "Plus1") test.userRdtResultGlucose = "+"
                if (test.userRdtResultGlucose == "Plus3") test.userRdtResultGlucose = "+++"
                if (test.userRdtResultGlucose == "Plus2") test.userRdtResultGlucose = "++"
                if (test.userRdtResultGlucose == "Plus4") test.userRdtResultGlucose = "++++"
               // dia_result = 'Protein : ' + protein + '<br/> Glucose : ' + test.userRdtResultGlucose;
               var tempArray = [];
               tempArray.push('Protein', 'Glucose', protein,test.userRdtResultGlucose)
               this.translate.stream(tempArray).subscribe((val) => {       
                        
                    dia_result = val.Protein + ' : ' + val[protein] + '<br/>' 
                                + val.Glucose + ' : ' +  val[test.userRdtResultGlucose];           
               })    
               this.translate.onLangChange.subscribe((event) => {                        
                   dia_result  =  event.translations.Protein + ' : ' + event.translations[protein] + '<br/>'  
                                  + event.translations.Glucose + ' : ' +  event.translations[test.userRdtResultGlucose];         
                   newObject.diaResults = dia_result;             
               }); 
            } else {
                if (protein == "Plus1") protein = "+"
                if (protein == "Plus3") protein = "+++"
                if (protein == "Plus2") protein = "++"
                if (protein == "Plus4") protein = "++++"
                if (test.rdtResultGlucose == "Plus1") test.rdtResultGlucose = "+"
                if (test.rdtResultGlucose == "Plus3") test.rdtResultGlucose = "+++"
                if (test.rdtResultGlucose == "Plus2") test.rdtResultGlucose = "++"
                if (test.rdtResultGlucose == "Plus4") test.rdtResultGlucose = "++++"
              //  dia_result = 'Protein : ' + this.valPlusConver(test.rdtResultProtein) + '<br/> Glucose : ' + test.rdtResultGlucose;
                var valProtien = this.valPlusConver(test.rdtResultProtein)
                var tempArray = [];
                tempArray.push('Protein', 'Glucose',valProtien,test.rdtResultGlucose)
                this.translate.stream(tempArray).subscribe((val) => {          
                     var typeOf = test.dietType       
                     dia_result = val.Protein + ' : ' + val[valProtien] + '<br/>' 
                                 + val.Glucose + ' : ' +  val[test.rdtResultGlucose];           
                })    
                this.translate.onLangChange.subscribe((event) => {                        
                    dia_result  =  event.translations.Protein +  ' : ' +event.translations[valProtien] + '<br/>'  
                                   + event.translations.Glucose + ' : ' + event.translations[test.rdtResultGlucose];         
                    newObject.diaResults = dia_result;             
                }); 
            }
            newObject.diaResults = dia_result;
        }
        else if (test.name === "HEMOGLOBIN") {
           // dia_result = 'Count: ' + test.result + ' ' + test.unit;
            var tempArray = [];
            tempArray.push('COUNT')
            this.translate.stream(tempArray).subscribe((val) => {     
                        
                 dia_result = val.COUNT + ' : ' + test.result + ' ' + test.unit;               
            })    
            this.translate.onLangChange.subscribe((event) => {                        
                dia_result  =  event.translations.COUNT + test.result+ ' ' + test.unit               
                newObject.diaResults = dia_result;             
            }); 
        } else if (test.name === "BLOOD_GLUCOSE") {
           // dia_result = 'Type: ' + test.dietType + '<br/> Count: ' + test.result + ' ' + test.unit;
           var tempArray = [];
            tempArray.push('TYPE',test.dietType,'COUNT')
            this.translate.stream(tempArray).subscribe((val) => {          
                  
                 dia_result = val.TYPE + ' : ' + val[test.dietType] + '<br/>' + val.COUNT + ' : ' + test.result + ' ' + test.unit;               
            })    
            this.translate.onLangChange.subscribe((event) => {                          
                dia_result  = event.translations.TYPE + ' : ' + event.translations[test.dietType] + '<br/>'  + event.translations.COUNT + ' : ' + test.result+ ' ' + test.unit               
                newObject.diaResults = dia_result;             
            });   
        }
        else { dia_result = test.result + ' ' + test.unit }
        if (test.type != "WHOLE_BLOOD_RDT" && test.type != "URINE_POCT" && test.name != "URINE") {
            newObject.diaResults = dia_result;
        }

        newObject.type = test.name;

        if (newObject.type.includes('_'))
            newObject.type = newObject.type.replace('_', ' ')
            
        if (newObject.type.includes('_'))
            newObject.type = newObject.type.replace('_', ' ')
            
        //newObject.test = test;
        // if (newObject.patientId !== null && newObject.mrn !== null)
        this.tests.push(newObject);

    }

    valPlusConver(protein) {
        if (protein == "Plus1") protein = "+"
        if (protein == "Plus3") protein = "+++"
        if (protein == "Plus2") protein = "++"
        if (protein == "Plus4") protein = "++++"
        return protein;
    }

    fmtMSS(duration) {
        var milliseconds = ((duration % 1000) / 100) | 0
        var seconds = ((duration / 1000) % 60) | 0
        var minutes = (duration / (1000 * 60) % 60) | 0
        var hours = (duration / (1000 * 60 * 60) % 24) | 0;
        return ((hours < 10) ? "0" + hours : hours) + ":" + ((minutes < 10) ? "0" + minutes : minutes) + ":" + ((seconds < 10) ? "0" + seconds : seconds);
    }

    paginate(event) {
        this.pageNo = event.page + 1;
        this.size = parseInt(event.rows);
        //this.onFilterClick()       
        if (this.searchNameKey == undefined && this.searchTestKey == undefined) {
            if (this.userType == 'User') {
                this.searchUserByAll(this.userInfo.id, this.pageNo, this.size, new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime());
            }
            if (this.selectedFacility.id != "ALL") {
                if (this.selectedCenter.id != "ALL") {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "CENTER", this.selectedFacility.id, this.selectedCenter.id, this.pageNo, this.size);
                }
                else {
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "FACILITY", this.selectedFacility.id, this.selectedCenter.id, this.pageNo, this.size);
                }
            }
            else {
                if (this.userType != 'User') {
                    var endDate = new Date();
                    endDate.setDate(endDate.getDate() + 1);
                    endDate.setHours(23);
                    endDate.setMinutes(59);
                    endDate.setSeconds(59);
                    endDate.setMilliseconds(0);
                    this.list(new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime(), "ALL", 0, 0, this.pageNo, this.size);
                }
            }
        }
        if (this.searchNameKey == undefined && this.searchTestKey) {
            this.testChange(this.searchTestKey)
        }
        if (this.searchNameKey && this.searchTestKey == undefined) {
            this.userChange(this.searchNameKey)
        }

    }

    humanize(str) {
        var frags = str.split('_');
        for (var i = 0; i < frags.length; i++) {
            frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
        }
        return frags.join(' ');
    }


    showTestDetail(test) {
        var testName;
        this.testData = {};
        this.testData = test;
        this.showsucc = false;

        this.showResult = this.testData.diaResults;

        if (this.testData.test.name == 'URINE 2P')
            testName = 'URINE'
        else if (this.testData.test.name == 'DENGUE ANTIGEN')
            testName = 'DENGUE'
        else if (this.testData.test.name == 'BLOOD PRESSURE')
            testName = 'BLOOD_PRESSURE'
        else
            testName = this.testData.test.name

        this.showDialog = true;


        this.testIconUrl = "./assets/app/media/img/test/" + testName + ".png"
        if (this.testData.test.geopoint != null) {
            if (this.testData.test.geopoint.lat == 0 && this.testData.test.geopoint.lon == 0) {
                this.showLocation = true;
            } else {
                this.showLocation = false;
                this.options.center.lat = this.testData.test.geopoint.lat
                this.options.center.lng = this.testData.test.geopoint.lon
                this.overlays = [];
                this.overlays.push(new google.maps.Marker({
                    position: { lat: this.options.center.lat, lng: this.options.center.lng },
                    map: this.map, animation: google.maps.Animation.DROP, title: 'Data'
                }))
                this.map.setCenter(this.options.center)
                this.overlays[0].setAnimation(google.maps.Animation.BOUNCE);
                setTimeout(() => {
                    this.overlays[0].setAnimation(5);
                }, 700)
            }
        } else {
            // console.log(this.testData.test)
            this.showLocation = true;
        }

        if (this.testData.test.name.includes('_'))
            this.testData.test.name = this.testData.test.name.replace('_', ' ')
        // if (this.testData.test.type.includes('_'))
        //     this.testData.test.type = this.testData.test.type.replace('_', ' ')
        // if (this.testData.test.type.includes('_'))
        //     this.testData.test.type = this.testData.test.type.replace('_', ' ')   




    }

    conString(result) {
        var data = result.toString();
        return data;
    }

    reduce(str) {
        if (str.includes('_'))
            str = str.replace('_', ' ')
        if (str.includes('_'))
            str = str.replace('_', ' ')
        return str;
    }

    trump(str) {
        var trumped = "";  // default return for invalid string and pattern
        var pattern = '_'
        if (str && str.length && str.includes('_')) {
            trumped = str;
            if (pattern && pattern.length) {
                var idx = str.indexOf(pattern);
                if (idx != -1) {
                    trumped = str.replace(pattern, ' ');
                }
            }
        }
        return (trumped);
    }

    userChange(event) {
        this.searching = true
        // this.visiblepagination = false;     
        // setTimeout(() => this.visiblepagination = true, 0);
        if (this.selectedFacility.id != "ALL") {
            if (this.selectedCenter.id != "ALL") {
                this.searchUserByCenter(this.selectedFacility.id, this.selectedCenter.id, event.id, this.pageNo, this.size, new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime())
            }
            else {
                this.searchUserByFacility(this.selectedFacility.id, event.id, this.pageNo, this.size, new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime());
            }
        }
        else {
            this.searchUserByAll(event.id, this.pageNo, this.size, new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime())
        }
        this.searchNameKey = event;
    }

    testChange(event) {
        this.searching = true
        // this.visiblepagination = false;        
        // setTimeout(() => this.visiblepagination = true, 0);
        if (this.selectedFacility.id != "ALL") {
            if (this.selectedCenter.id != "ALL") {
                this.searchTestDoneByCenter(this.selectedFacility.id, this.selectedCenter.id, event.testType, event.testName, this.pageNo, this.size, new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime())
            }
            else {
                this.searchTestDoneByFacility(this.selectedFacility.id, event.testType, event.testName, this.pageNo, this.size, new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime());
            }
        }
        else if(this.userType == 'User'){
            this.searchTestDoneByUser(event.testType, event.testName, this.pageNo, this.size, new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime())
        }
        else {
            this.searchTestByAll(event.testType, event.testName, this.pageNo, this.size, new Date(window.localStorage.getItem('patientStartDate')).getTime(), new Date(window.localStorage.getItem('patientEndDate')).getTime())
        }
        this.searchTestKey = event;
    }

    searchUserByAll(userId, pageNo, size, startDate, endDate) {
        this.visits = [];
        this.tests = [];
        this.resNull = false
        this._visitsService.searchByUserBasedOnAll(userId, pageNo, size, startDate, endDate)
            .then(visits => {
                if (visits.length == 0) {
                    this.resNull = true;
                }else{
                    this.resNull = false
                }
                this.commonResponse(visits);
            })
    }

    searchTestByAll(type, name, pageNo, size, startDate, endDate) {
        this.visits = [];
        this.tests = [];
        this.resNull = false
        this._visitsService.searchByTestBasedOnAll(type, name, pageNo, size, startDate, endDate)
            .then(visits => {
                if (visits.length == 0) {
                    this.resNull = true;
                }else{
                    this.resNull = false
                }
                this.commonResponse(visits);
            })
    }

    searchUserByFacility(facilityId, userId, pageNo, size, startDate, endDate) {
        this.visits = [];
        this.tests = [];
        this.resNull = false
        this._visitsService.searchByUserBasedonFacilty(facilityId, userId, pageNo, size, startDate, endDate)
            .then(visits => {
                if (visits.length == 0) {
                    this.resNull = true;
                }else{
                    this.resNull = false
                }
                this.commonResponse(visits);
            })
    }

    searchTestDoneByFacility(facilityId, type, name, pageNo, size, startDate, endDate) {
        this.visits = [];
        this.tests = [];
        this.resNull = false
        this._visitsService.searchByTestBasedonFacilty(facilityId, type, name, pageNo, size, startDate, endDate)
            .then(visits => {
                if (visits.length == 0) {
                    this.resNull = true;
                }else{
                    this.resNull = false
                }
                this.commonResponse(visits);
            })
    }

    searchUserByCenter(facilityId, centerId, userId, pageNo, size, startDate, endDate) {
        this.visits = [];
        this.tests = [];
        this.resNull = false
        this._visitsService.searchByUserBasedonCenter(facilityId, centerId, userId, pageNo, size, startDate, endDate)
            .then(visits => {
                if (visits.length == 0) {
                    this.resNull = true;
                }else{
                    this.resNull = false
                }
                this.commonResponse(visits);
            })
    }

    searchTestDoneByCenter(facilityId, centerId, type, name, pageNo, size, startDate, endDate) {
        this.visits = [];
        this.tests = [];
        this.resNull = false
        this._visitsService.searchByTestBasedonCenter(facilityId, centerId, type, name, pageNo, size, startDate, endDate)
            .then(visits => {
                if (visits.length == 0) {
                    this.resNull = true;
                }else{
                    this.resNull = false
                }
                this.commonResponse(visits);
        })
    }

    searchTestDoneByUser(type,name, pageNo, size, startDate, endDate){
        //searchByTestBasedOnUser
        this.visits = [];
        this.tests = [];
        this.resNull = false
        this._visitsService.searchByTestBasedOnUser(type, name, this.userInfo.id, pageNo, size, startDate, endDate)
        .then(visits => {
            if (visits.length == 0) {
                this.resNull = true;
            }else{
                this.resNull = false
            }
            this.commonResponse(visits);
        })
    }

    commonResponse(visits) {
        this.visits = [];
        this.tests = [];
        if (visits.results) {
            this.visits = visits.results;
            this.visitCount = visits.totalHits;
            this.visiblepagination = true;
        } else {
            this.visits = [];
            this.visitCount = 0;
        }
        var userIds = [];
        if (this.visits !== undefined && this.visits !== null && this.visits.length > 0) {
            this.visits.forEach((val, i) => {
                userIds.push(val.test.visitId);
                // val.imei = (val.imei !== null) ? val.imei : 'No Data'
                // val.serialNumber = (val.serialNumber !== null) ? val.serialNumber : 'No Data'
                var newObject = val;
                this.testResultCalculation(newObject)
            });
            this.tests = [...this.tests]
        }
        Helpers.setLoading(false);
    }

    downloadCopy() {
        var a = document.createElement("a");
        a.href = this.testData.test.imageUrl;
        a.download = "Image";
        document.body.appendChild(a);
        a.click();
    }

    downloadECG() {
        var objectURL;
        this.showSpinner = true;
        this._visitsService.downloadECG(this.testData.test.ecgReport).then(data => {
            this.showSpinner = false;
            this.showsucc = true;
            objectURL = URL.createObjectURL(data);
            var a = document.createElement("a");
            a.href = objectURL;
            a.download = "ECG Report";
            document.body.appendChild(a);
            a.click();
        })
    }

    facilityChange(value) {
        this.selectedFacility = value;
        this.selectedFacility.organizationCode = value.organizationCode;
        this.centers = value.centers;
        this.dropChange = false;
        if (this.centers[0].id !== "ALL") {
            let allCenter = new Center();
            allCenter.name = "ALL";
            allCenter.id = "ALL";
            this.centers.splice(0, 0, allCenter);
        }
        this.selectedCenter.id = this.centers[0].id;
        this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
    }

    onChangeSeachBy(event) {
        this.searchNameKey = undefined;
        this.searchTestKey = undefined;
        
        if (event.keytoSend == 'TYPE') {
            this.showType = true
            this.showUser = false;
            // this.pageNo=1;
         
        }
        else if (event.keytoSend == 'ALL') {
            this.showUser = false;
            this.showType = false;
            this.pageOne();
            this.onFilterClick();
        }
        else {
            this.showType = false
            this.showUser = true;
            // this.pageNo=1;
           
        }
    }

    searchByType() {

    }

    pageOne(){         
        this.pageNo=1;
        this.visiblepagination = false;       
        setTimeout(() => this.visiblepagination = true, 0);               
    }



}
