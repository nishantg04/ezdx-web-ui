import { Component, OnInit, ViewEncapsulation, AfterViewInit, Input } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';

import { User } from '../user';
import { UserChild } from '../userChild';
import { Role } from '../role';
import { Center } from '../../facilities/center';
import { Facility } from '../../facilities/facility';
import { Address } from '../../facilities/address';
import { Country } from '../../facilities/country';
import { FacilitiesService } from '../../facilities/facilities.service';
import { UsersService } from '../users.service';
import { Observable } from 'rxjs/Observable';
import { Utils } from '../../utils/utils';
import { Ng4FilesStatus, Ng4FilesSelected, Ng4FilesService, Ng4FilesConfig } from "angular4-files-upload"
import { environment } from '../../../../../../../environments/environment';
import { CommonService } from '../../../../../../_services/common.service'
import { element } from 'protractor';
import { StateChangeService } from '../state-change.service';
import { setTimeout } from 'timers';
import { SelectItem } from 'primeng/primeng';
import { CenterModal } from '../center.modal';
import { NgForm } from '@angular/forms';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./users-add.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class UsersAddComponent implements OnInit, AfterViewInit {
    cfpasscode: string;
    passcode: string = '';
    isSignatureUpload: boolean = false;
    showSignatureUpload: Boolean = false;
    //centerLables: {label: string; value: CenterModal}[] = [];
    isIpan: boolean = false;
    invalidFrom: boolean = false;
    //selectedCenterspng: any;

    currentUserType: string;
    userInfo: any;
    user = new User();
    userId: string;
    facilities: Facility[];
    centers: Center[];
    selectedFacility: Facility;
    selectedCenter: Center;
    userType: string;
    countires: Country[];
    hcRoles: Role[];
    facilityRoles: Role[];
    ipanRoles: Role[];
    loading: boolean;
    showsuccess: boolean = false;
    msg: string;
    errorEmail: string;
    errorPhone: string;
    zeroPhone: string;
    digitLength: string;
    fieldError: string;
    country_code: string;
    emailID: boolean = false;
    selectedType: boolean = false;
    HCfacilities: any[];
    facilityName: string;
    profilePictureUrl: any;
    public selectedFiles;
    formData: FormData = new FormData();
    formSign: FormData = new FormData();
    imgLoad: boolean = false;
    submitMsg: boolean = false;
    userPresent: boolean = false;
    userPhonePresent: boolean = false;
    disableCustomer: boolean = false;
    activeUser: boolean = false;
    selectedFacilityId: any;
    tempPassword: any;
    showConfirmMsg: boolean = false;
    showpathologistSignature: boolean = false;
    updated: boolean = false;
    panName: any;

    public selectedSignature;
    private signatureImage: Ng4FilesConfig = {
        acceptExtensions: ["jpg", "jpeg", "png"],
        maxFilesCount: 1,
        maxFileSize: 10000000,
        totalFilesSize: 10000000
    };

    constructor(private _script: ScriptLoaderService, private _usersService: UsersService, private _facilitiesService: FacilitiesService, private _router: Router,
        private _activeroute: ActivatedRoute, private CommonService: CommonService, private ng4FilesService: Ng4FilesService, private _stateChangeService: StateChangeService) {
        this.loading = false;
        if (window.localStorage.getItem('userType') == 'Facility') {
            this.currentUserType = 'Facility';
            this.userType = "facility";
        }
        else {
            this.currentUserType = 'Admin';
            this.userType = "HC";
        }
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
        this.user.user = new UserChild();
        this.user.user.userRoles = [];
        this.user.user.addressInfo = new Address();
        this.user.user.addressInfo.city = "Bangalore";
        this.user.user.addressInfo.country = "India";
        this.user.user.dial_code = "+91";
        this.country_code = "in"
        this.hcRoles = [
            { name: 'ADMIN', type: 'HC_ADMIN', selected: false },
            { name: 'OPERATIONS', type: 'HC_OPERATIONS', selected: false },
            { name: 'MANAGER', type: 'HC_MANAGER', selected: false },
        ];
        this.facilityRoles = [
            { name: 'HEALTHWORKER/OPERATOR', type: 'OPERATOR', selected: false },
            { name: 'DOCTOR', type: 'DOCTOR', selected: false },
            { name: 'CENTER ADMIN', type: 'CENTER_ADMIN', selected: false },
            { name: 'CUSTOMER_ADMIN/MANAGER/TECHNICIAN', type: 'TECHNICIAN', selected: false }];
        this.ipanRoles = [
            { name: 'PATHOLOGIST', type: 'PATHOLOGIST', selected: false },
            { name: 'CLINICAL_USER', type: 'CLINICAL_USER', selected: false }
        ];
        this.getCountries();
        this.clearMsg();
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/forms/widgets/select2.js');
    }


    clearMsg() {
        this.zeroPhone = "";
        this.digitLength = "";
        this.errorEmail = "";
        this.errorPhone = "";
        this.fieldError = "";
    }

    getCountries(): void {
        this.CommonService.getCountries().then(
            countries => {
                this.countires = countries
            }
        );
    }

    /*     clearSelectedCenter(){
            console.log("inside clear:: selected centers", this.selectedCenterspng);
            //this.selectedCenterspng = [];
        } */
    getFlag() {
        this.countires.forEach(item => {
            if (item.name == this.user.user.addressInfo.country)
                this.country_code = item.code;
        })
    }

    getDialCode() {
        this.countires.forEach(item => {
            if (item.name == this.user.user.addressInfo.country)
                this.user.user.dial_code = item.dial_code;
        })
    }

    countrySelect(event) {
        //alert(JSON.stringify(this.countires[event.target.selectedIndex]))
        var selectedCountryObject = this.countires[event.target.selectedIndex];
        this.user.user.addressInfo.country = selectedCountryObject.name;
        this.user.user.dial_code = selectedCountryObject.dial_code;
        this.country_code = selectedCountryObject.code;
    }

    onSelectRole(role: Role): void {
        role.selected = !role.selected;
        // if (role.type == 'HC_OPERATIONS'){
        //     this.selectedType = true;
        //    // this.hcRoles[0].selected = true;  
        // }   
        this.hcRoles.forEach(element => {
            if (element.type === 'HC_OPERATIONS') {
                if (element.selected) {
                    this.selectedType = true;
                    this.getHCFacility();
                }
                else
                    this.selectedType = false;
            }
        })
    }


    getHCFacility() {
        this.HCfacilities = [];
        this.facilities.forEach(org => {
            if (org.organizationCode === "HCB") {
                this.HCfacilities.push(org);
                //this.selectedType = true;
                this.facilityName = org.name;
                this.user.user.facilityId = org.id;
                this.user.user.facilityCode = org.organizationCode;
                this.centers = org.centers;
                this.user.user.centerId = org.centers[0].id;
                if (org.centers[0].centerCode)
                    this.user.user.centerCode = org.centers[0].centerCode;
                this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                    'assets/demo/default/custom/components/forms/widgets/bootstrap-select.js');
            }

        })
    }

    ngOnInit() {
        this.ng4FilesService.addConfig(this.signatureImage, "sign-config");
        document.getElementById('user_fname').focus();
        this.disableFun();
        this.ng4FilesService.addConfig(this.FileConfig);
        this.userId = this._activeroute.snapshot.queryParams['userId'] || '/';
        if (this.userId != '/') {
            Helpers.setLoading(true);
            this.user.user.id = this.userId;
            this._usersService.getUser(this.user.user.id)
                .then(user => this.user.user = user)
                .then(() => {
                    // console.log('role', this.user.user.userRoles, 'user', this.user.user)
                    if (this.user.user.age == 0)
                        this.user.user.age = null;
                    if (this.user.user.active == true) {
                        this.disableCustomer = true;
                    } else {
                        this.disableCustomer = false;
                    }
                    if(!this.user.user.email){
                        this.emailID = true
                    }else{
                        this.emailID = false;
                    }
                    this.user.user.userRoles.forEach((role, i) => {
                        this.hcRoles.forEach((hr, i) => {
                            if (hr.type === role) {
                                hr.selected = true;
                            }
                            if (role == 'HC_OPERATIONS') {
                                this.selectedType = true;
                            }
                        });
                        this.facilityRoles.forEach((fr, i) => {
                            this.selectedType = false;
                            if (fr.type === role) {
                                fr.selected = true;
                                this.userType = "facility";
                            }
                        });
                        this.ipanRoles.forEach((ir, i) => {
                            this.selectedType = false;
                            if (ir.type === role) {
                                ir.selected = true;
                                this.userType = "IAP";
                            }
                        });
                        if (!this.user.user.addressInfo)
                            this.user.user.addressInfo = new Address();

                        this.getFlag();
                        this.getDialCode();
                        this.getFacilities();
                    });
                    Helpers.setLoading(false);
                });
        }
        else {
            this.generatePassword();
            this.getFacilities();
        }
    }
    ngAfterViewInit() {
    }

    private FileConfig: Ng4FilesConfig = {
        acceptExtensions: ['png', 'jpeg', 'jpg'],
        maxFilesCount: 1,
        maxFileSize: 5120000,
        totalFilesSize: 10120000
    };

    disableFun() {
        if (this._activeroute.snapshot.queryParams['userId'] == undefined)
            this.emailID = true;
        else
            this.emailID = false;
    }

    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.user.user.facilityCode = org.organizationCode;
                this.selectedFacilityId = org.id;
                this.user.user.facilityId = this.selectedFacilityId;
                this.centers = org.centers;
                this.user.user.centerId = org.centers[0].id;
                if (org.centers[0].centerCode)
                    this.user.user.centerCode = org.centers[0].centerCode;
                this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                    'assets/demo/default/custom/components/forms/widgets/bootstrap-select.js');
            }
        });
    }

    facilityChange(value) {
        this.centers = [];
        this.selectedFacility = value;
        this.selectedFacility.organizationCode = value.organizationCode;
        this.selectedFacilityId = value.id;
        this.user.user.facilityId = this.selectedFacilityId;
        this.user.user.facilityCode = this.selectedFacility.organizationCode;
        this.centers = value.centers;
        if (this.selectedFacility.centers) {
            this.centers = this.selectedFacility.centers;
            this.user.user.centerId = this.centers[0].id;
            if (this.centers[0].centerCode)
                this.user.user.centerCode = this.centers[0].centerCode;
        } else {
            this.centers = null;
            this.user.user.centerId = null;
        }
    }

    valEmail() {
        this.userPresent = false;
        this.errorEmail = "";
        this.fieldError = "";
    }

    smallCase(email) {
        this.user.user.email = email.toLowerCase()
        return this.user.user.email;
    }

    checkEmail(email) {
        
            this.errorEmail = "";
            document.getElementById('loading_icon').style.display = 'block'
            this._usersService.userExistsSearch('email', email)
                .then(data => {
                    document.getElementById('loading_icon').style.display = 'none'
                    if (data != null)
                        this.userPresent = true;
                    else
                        this.userPresent = false;

                })
        

    }

    checkPhone(phone) {
        if (phone) {
            if (Utils.returnNumbers(phone) === false) {
                this.errorPhone = "ENTER_VALID_PHONE_NUMBER"
            } else {
                document.getElementById('loading_icon_phone').style.display = 'block'
                this._usersService.userExistsSearch('phone', phone)
                    .then(data => {
                        document.getElementById('loading_icon_phone').style.display = 'none'
                        if (data != null)
                            this.userPhonePresent = true;
                        else
                            this.userPhonePresent = false;
                    })

            }

        }

    }

    valPhone() {
        this.digitLength = ""
        this.fieldError = "";
        this.errorPhone = "";
        this.userPhonePresent = false;
    }

    numberChange(Pnumber) {
        if (Pnumber) {
            if (Pnumber.length !== 10) {
                // console.log(Pnumber.length);
                this.digitLength = "ENTER_10_DIGIT_NUMBER"
            } else
                this.digitLength = ""
            if (!Utils.numberZeroCheck(Pnumber)) {
                this.digitLength = ""
                this.errorPhone = "ENTER_VALID_PHONE_NUMBER"
                document.getElementById('user_phone').focus();
            } else {
                this.errorPhone = "";
                this.checkPhone(Pnumber)
            }

        }
    }

    public filesSelect(selectedFiles: Ng4FilesSelected): void {
        var FileUploadPath = selectedFiles.files[0].name
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
        if (Extension === "png" || Extension === "jpeg" || Extension === "jpg" || Extension === "gif") {
            if (selectedFiles.status === Ng4FilesStatus.STATUS_SUCCESS) {
                this.selectedFiles = Array.from(selectedFiles.files).map(file =>
                    this.formData.append('picture', file));
                this.submitMsg = true;
            }
        } else {
            alert('upload valid image')
        }
    }

    uploadUserImage() {
        this._usersService.uploadImage(this.user.user.id, this.formData).then(data => {
            this.user.user.profilePictureUrl = data.profilePictureUrl;
            Helpers.setLoading(false);
        });
    }

    selectedRoleipan(role): void {
        role.selected = !role.selected
        if (role.type == 'PATHOLOGIST') {
            if (role.selected == true) {
                this.showpathologistSignature = true;
            }
            else if (role.selected == false) {
                this.showpathologistSignature = false;
            }
        }
    }


    onUserTypeChange(type: string): void {
        this.showpathologistSignature = false;
        if (type == "IAP") {
            this.selectedType = false;
            this.hcRoles.forEach((h, i) => {
                h.selected = false;
            });
            this.facilityRoles.forEach((f, i) => {
                f.selected = false;
            });
            // console.log('facility: ', this.facilities);
            this.facilities.forEach((facility) => {
                if (facility.organizationCode === "IAP") {
                    this.panName = facility.name
                    this.user.user.facilityId = facility.id;
                    this.centers = facility.centers;
                    this.user.user.centerId = this.centers[0].id;
                    this.user.user.centerCode = this.centers[0].centerCode;
                    // this.user.user.facilityId = this.facilities[0].id;
                    this.user.user.facilityCode = facility.organizationCode;

                }
            })
        } else if (type !== "HC") {
            this.selectedType = false;
            this.hcRoles.forEach((h, i) => {
                h.selected = false;
            });
            this.ipanRoles.forEach((f, i) => {
                f.selected = false;
            });
            this.facilities.sort(function(a, b) {
                if (a.name > b.name) {
                    return 1;
                } else if (a.name < b.name) {
                    return -1;
                }
                return 0;
            });
            if (this.facilities[0].organizationCode == 'HCB') {
                this.centers = this.facilities[1].centers;
                this.user.user.centerId = this.centers[1].id;
                this.user.user.centerCode = this.centers[1].centerCode;
                this.user.user.facilityId = this.facilities[1].id;
                this.user.user.facilityCode = this.facilities[1].organizationCode;
            } else {
                this.centers = this.facilities[0].centers;
                this.user.user.centerId = this.centers[0].id;
                this.user.user.centerCode = this.centers[0].centerCode;
                this.user.user.facilityId = this.facilities[0].id;
                this.user.user.facilityCode = this.facilities[0].organizationCode;
            }

            this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                'assets/demo/default/custom/components/forms/widgets/bootstrap-select.js');
        }
        else {
            //  this.getHCFacility();
            this.facilityRoles.forEach((f, i) => {
                f.selected = false;
            });
            this.ipanRoles.forEach((f, i) => {
                f.selected = false;
            });
        }
    }

    getAllFacilities() {
        this.facilities = [];
        this._facilitiesService.getFacilities(0, 0)  // for get All facility we should pass page and size as zero
            .then(facilities => {
                localStorage.setItem('facilityTable', JSON.stringify(facilities.organizations)); // for save all facility details              
            })
    }


    getFacilities(): void {
        this.getAllFacilities();
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            } else if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        if (!this.user.user.id) {
            if (this.currentUserType === "Admin") {
                this.centers = this.facilities[0].centers;
                this.user.user.centerId = this.centers[0].id;
                this.user.user.centerCode = this.centers[0].centerCode;
                this.user.user.facilityId = this.facilities[0].id;
                this.user.user.facilityCode = this.facilities[0].organizationCode;
                this.getHCFacility();
            } else {
                this.user.user.facilityId = this.userInfo.facilityId;
                this.user.user.facilityCode = this.userInfo.facilityCode;
                this.facilities.forEach(fclt => {
                    if (fclt.id === this.user.user.facilityId) {
                        this.centers = fclt.centers;
                        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                            'assets/demo/default/custom/components/forms/widgets/bootstrap-select.js');
                    }
                });
            }
        }
        else {
            // this.selectedType =true;
            this.HCfacilities = [];
            var userRoleString = this.user.user.userRoles.toString()
            if (userRoleString.includes('HC_OPERATIONS')) {
                this.selectedType = true;
            }
            this.user.user.userRoles.forEach(role => {
                this.hcRoles.forEach(hc => {
                    if (hc.type == role) {
                        this.facilities.forEach((org, i) => {
                            if (org.id === this.user.user.facilityId) {
                                this.HCfacilities.push(org);
                                this.facilityName = org.name;
                                this.user.user.facilityId = org.id;
                                this.user.user.facilityCode = org.organizationCode;
                                this.centers = org.centers;
                                this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                                    'assets/demo/default/custom/components/forms/widgets/bootstrap-select.js');
                            }
                        });
                    } else {
                        this.facilities.forEach((org, i) => {
                            if (org.id === this.user.user.facilityId) {
                                this.centers = org.centers;
                                this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                                    'assets/demo/default/custom/components/forms/widgets/bootstrap-select.js');
                            }
                        });
                    }
                })
            })
        }
    }

    isHcOperations(roles) {
        roles.forEach(element => {
            if (element == "HC_OPERATIONS") {
                return true;
            }
        });
        return false;
    }

    activateUser(userId) {
        Helpers.setLoading(true);
        this._stateChangeService.setUserActive(userId)
            .then((data) => {
                Helpers.setLoading(false);
                this.activeUser = true;
                setTimeout(() => {
                    this.activeUser = false;
                }, 1000)
                setTimeout(() => {
                    this._router.navigate(['/users/list']);
                }, 2000)
            });
    }



    save(formValid): void {

        this.submitMsg = false;

        this.msg = null;
        if (formValid) {
            // console.log('form valid');
            if (this.user.user.phone)
                this.user.user.phone = this.user.user.phone.toString();
            if (!this.loading) {
                // console.log('form loading');
                if (this.zeroPhone === "" && this.errorEmail === "" && this.errorPhone === "" && this.userPhonePresent === false && this.userPresent === false) {
                    this.fieldError = "";
                    Helpers.setLoading(true);
                    this.loading = true;
                    try {
                        var self = this;
                        this.user.user.userRoles = [];
                        if (this.userType === 'HC') {
                            this.hcRoles.forEach((h, i) => {
                                if (h.selected) {
                                    if (h.type == "HC_OPERATIONS") {
                                    }
                                    this.user.user.userRoles.push(h.type);
                                }
                            });
                            this.user.user.userRoles.sort();
                            let len = this.user.user.userRoles.length;
                            if (this.user.user.userRoles[len - 1] != "HC_OPERATIONS") {
                                this.user.user.facilityId = null;
                                this.user.user.facilityCode = null;
                                this.user.user.centerId = null;
                                this.user.user.centerCode = null;
                            }
                        }
                        else {
                            this.facilityRoles.forEach((f, i) => {
                                if (f.selected) {
                                    this.user.user.userRoles.push(f.type);
                                }
                            });
                            this.ipanRoles.forEach((f, i) => {
                                if (f.selected) {
                                    this.user.user.userRoles.push(f.type);
                                }
                            });
                        }
                        if (this.user.user.userRoles.length > 0) {
                            if (!this.user.user.id) {
                                this._usersService.create(this.user)
                                    .then((data) => {
                                        this.user.user.id = data['id'];
                                        if (this.user.user.id) {
                                            if (this.selectedFiles)
                                                this.uploadUserImage();
                                            if (this.isSignatureUpload) {
                                                if (this.passcode !== '') {
                                                    this.formSign.append('passcode', this.passcode);
                                                } else {
                                                    this.formSign.append('passcode', '');
                                                }
                                                this.uploadSignature(this.user.user.id);
                                            }
                                        }
                                    })
                                    .then(() => {
                                        Helpers.setLoading(false);
                                        this.loading = false;
                                        this.showsuccess = true;
                                        setTimeout(() => {
                                            this._router.navigate(['/users/list']);
                                        }, 2000);

                                    }, (e: any) => {
                                        // console.log(e);
                                        let res = JSON.parse(e._body);
                                        if (res.error) {
                                            this.msg = res.error;
                                        }
                                        Helpers.setLoading(false);
                                        this.loading = false
                                    });
                            }
                            else {
                                var isvalid = true;
                                if (this.userType != 'HC' && !this.user.user.facilityId) {
                                    isvalid = false;
                                }
                                if (isvalid) {
                                    this.user.password = undefined;
                                    this._usersService.update(this.user)
                                        .then((data) => {
                                        })
                                        .then(() => {
                                            Helpers.setLoading(false);
                                            this.loading = false;
                                            this.updated = true;
                                            if (this.selectedFiles)
                                                this.uploadUserImage();
                                            if (this.selectedFacilityId != undefined) {
                                                if (this.user.user.active == false) {
                                                    this.user.user.active = true;
                                                    this.activateUser(this.user.user.id)
                                                }
                                            } else {
                                                this.updated = true;
                                                setTimeout(() => {
                                                    this._router.navigate(['/users/list']);
                                                }, 2000)
                                            }

                                        }, (e: any) => {
                                            // console.log(e);
                                            if (e._body) {
                                                let res = JSON.parse(e._body);
                                                if (res.error) {
                                                    this.fieldError = "";
                                                    this.msg = res.error;
                                                }
                                            }
                                            else {
                                                this.fieldError = "";
                                                this.msg = e;
                                            }
                                            Helpers.setLoading(false);
                                            this.loading = false;
                                        });
                                }
                                else {
                                    this.msg = "PLEASE_SELECT_CUSTOMER_AND_CENTER";
                                    Helpers.setLoading(false);
                                    this.loading = false;
                                }
                            }
                        }
                        else {
                            this.msg = "PLEASE_SELECT_AT_LEAST_ONE_USER_ROLE";
                            Helpers.setLoading(false);
                            this.loading = false;
                        }
                    }
                    catch (e) {
                        Helpers.setLoading(false);
                        this.loading = false;
                    }
                } else {
                    this.fieldError = "CORRECT_THE_FIELD_ERROR_THEN_TRY_TO_SUBMIT"
                }
            }
        } else {
            this.invalidFrom = true;
            window.setTimeout(() => {
                this.invalidFrom = false;
            }, 4000);
        }
    }

    cancelClick() {
        this.fieldError = null;
    }

    showPassword(text) {
        if (text == 'hide') {
            document.getElementById('eye').style.display = 'block';
            document.getElementById('eyehide').style.display = 'none';
            document.getElementById('user_password').setAttribute("type", "password")
        }
        if (text == 'show') {
            document.getElementById('eyehide').style.display = 'block';
            document.getElementById('user_password').setAttribute("type", "text")
            document.getElementById('eye').style.display = 'none';
        }
    }

    generatePassword() {
        this._usersService.getPassword()
            .then(data => {
                this.user.password = data;
                this.tempPassword = data;
            })
    }

    keyupPassowrd() {
        document.getElementById('user_password').setAttribute("type", "password")
    }

    handlePassword(password) {
        this.tempPassword = password
        document.getElementById('confirm_passowrd').style.display = 'block'
    }

    pathologistSignature() {
        this.showSignatureUpload = true;
    }

    handleConfirm(value) {
        if (this.tempPassword != value) {
            this.showConfirmMsg = true
        } else {
            this.showConfirmMsg = false
        }
    }

    public signatureUpload(selectedSignature: Ng4FilesSelected): void {
        // console.log("signatuer: ", selectedSignature);
        if (selectedSignature.status === Ng4FilesStatus.STATUS_SUCCESS) {
            this.isSignatureUpload = true;
            this.selectedSignature = Array.from(selectedSignature.files).map(file => {
                this.formSign.append('signature', file);
                let ext = file.name.split('.');
                this.formSign.append('ext', ext[ext.length - 1]);
            });
        } else {
            this.isSignatureUpload = false;
            this.formSign.delete('signature');
            this.formSign.delete('ext');
        }
    }

    /* 
        submitSignature(form: NgForm) {
            Helpers.setLoading(true);
            if (form.valid) {
                this.formSign.append('passcode', form.value.passcode);
            }
            this.showSignatureUpload = false;
        }
     */
    uploadSignature(userId) {
        this._usersService.signatureUpload(userId, this.formSign).then(res => {
            this.signatureFormClear();
            Helpers.setLoading(false);
        }, rej => {
            Helpers.setLoading(false);
        });
    }

    signatureFormClear() {
        this.formSign.delete('signature');
        this.formSign.delete('ext');
        this.formSign.delete('passcode');
        this.isSignatureUpload = false;
        this.passcode = '';
        this.cfpasscode = '';
    }





}