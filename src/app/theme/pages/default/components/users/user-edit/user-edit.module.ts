import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { Ng4FilesModule } from "angular4-files-upload"
import { UsersService } from '../users.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { CommonService } from '../../../../../../_services/common.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { AutoCompleteModule, ButtonModule, DialogModule, GrowlModule, TabViewModule, InputMaskModule, InputTextModule, MessageModule, MessagesModule } from 'primeng/primeng';
import { UserEditComponent } from './user-edit.component';


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": UserEditComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), DialogModule, LayoutModule, Ng4FilesModule,
        TranslateModule.forChild({ isolate: false }), InputTextModule, GrowlModule, MessagesModule, MessageModule
    ], exports: [
        RouterModule
    ], declarations: [
        UserEditComponent,

    ],
    providers: [
        UsersService,
        FacilitiesService,
        CommonService, TranslateService
    ]

})
export class UserEditModule {



}