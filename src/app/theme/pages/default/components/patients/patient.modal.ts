export interface Patient {
    id?: String;
    patientId: String;
    userId: String;
    firstName: String;
    lastName: String;
    organizationId: String;
    centerId: String;
    gender: String;
    age: Number;
    dateOfBirth: Number;
    address: Address;
    email: String;
    phone: String;
    profilePictureUrl: String;
    profilePicturePath: String;
    signatureUrl: String;
    signaturePath: String;
    startTime: Number;
    endTime: Number;
    geopoint: GeoLocation;
    printHistory: PrintHistory[];
    createTime: String;
    updateTime: String;
    syncTime: String;
    mrn: String;
    dial_code: String;
}

export interface Address {
    city: String,
    postcode: String,
    address: String,
    country: String,
    district?: String,
    state: String

}

export interface PrintHistory {
    operatorId: String;
    operatorName: String;
    timeofPrinting: Number;
}

export interface GeoLocation {
    lat: Number;
    lng: Number;
}