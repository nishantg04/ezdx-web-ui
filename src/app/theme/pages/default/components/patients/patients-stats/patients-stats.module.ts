import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { PatientsStatsComponent } from './patients-stats.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { PatientsService } from '../patients.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { UsersService } from '../../users/users.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": PatientsStatsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule,
        TranslateModule.forChild({ isolate: false }),

    ], exports: [
        RouterModule
    ], declarations: [
        PatientsStatsComponent
    ],
    providers: [
        PatientsService,
        FacilitiesService,
        UsersService,
        TranslateService
    ]

})
export class PatientsStatsModule {



}