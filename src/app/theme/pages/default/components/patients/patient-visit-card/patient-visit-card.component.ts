import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-patient-visit-card',
    templateUrl: './patient-visit-card.component.html',
    styleUrls: ['./patient-visit-card.component.scss']
})
export class PatientVisitCardComponent implements OnInit {

    @Input() visit: any;
    @Output() getImage = new EventEmitter<String>();
    nowTime: Date = new Date();
    constructor() { }

    ngOnInit() {
        console.log('visit: ', this.visit);
    }

    toShowImage(imageUrl) {
        console.log('image: ', imageUrl);
        this.getImage.emit(imageUrl);
    }

}
