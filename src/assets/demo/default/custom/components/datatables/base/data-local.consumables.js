//== Class definition

var DatatableDataLocalDemo = function () {
    //== Private functions

    // demo initializer
    var demo = function () {

        var dataJSONArray = JSON.parse(localStorage.getItem('consumablesTable'));

        //Refresh table by remove html tag
        $(".m_datatable").remove();
        var $newdiv1 = $("<div class='m_datatable'></div>");
        $(".m-portlet__body").append($newdiv1);

        var datatable = $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: dataJSONArray,
                pageSize: 10,
                saveState: {
                    cookie: false,
                    webstorage: false
                }, // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
            },

            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 450, // datatable's body's fixed height
                footer: false // display/hide footer
            },

            // column sorting(refer to Kendo UI)
            sortable: true,

            // column based filtering(refer to Kendo UI)
            filterable: false,

            pagination: true,

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition

            columns: [{
                field: "",
                title: "Sr no",
                width: 60,
                sortable: false,
                template: function (row) {
                    return '\
						<span>\
                            ' + (((row.getDatatable().getCurrentPage() - 1) * 10) + (row.getIndex() + 1)) + '\
                        </span>\
					';
                }
            },
            {
                field: "txn",
                title: "TXN ID",
                sortable: true
            },

            {
                field: "reorder",
                title: "Reorder Request",
                sortable: true
            },

            {
                field: "name",
                title: "Consumable",
                sortable: true
            },

            {
                field: "quantity",
                title: "Packs",
                width: 50,
                sortable: true
            },
            {
                field: "user",
                title: "Operator",
                width: 200,
                sortable: true
            },
            {
                field: "customer",
                title: "Customer",
                sortable: true
            },
            {
                field: "center",
                title: "Center",
                sortable: true
            },
            {
                field: "createtime",
                title: "Created Date",
                sortable: true
            },
            {
                field: "status",
                width: 110,
                title: "Staus",
                sortable: false,
                overflow: 'visible',
                titleAlign: 'center',
                template: function (row) {
                    var statusBtn = "";
                    if (row.status == "dispatched") {
                        statusBtn = '<div class="text-warning">\
                            <i class="fa fa-cubes"></i> Dispatched\
                        </div>';
                    }
                    else if (row.status == "shipped") {
                        statusBtn = '<div class="text-info">\
                            <i class="fa fa-truck"></i> Shipped\
                        </div>';
                    }
                    else if (row.status == "delivered") {
                        statusBtn = '<div class="text-success">\
                            <i class="fa fa-check"></i> Delivered\
                        </div>';
                    }
                    else {
                        statusBtn = '<div class="text-danger">\
                            <i class="fa fa-times"></i> Rejected\
                        </div>';
                    }

                    return statusBtn;
                }
            },
            {
                field: "action",
                width: 110,
                title: "Action",
                sortable: false,
                overflow: 'visible',
                template: function (row) {
                    var statusBtn = "";
                    if (row.status == "dispatched") {
                        statusBtn = '<a class="btn btn-success m-btn text-white" title="Mark as Shipped">\
                            <i class="fa fa-truck"></i>\
                        </a>';
                    }
                    else if (row.status == "rejected") {
                        statusBtn = '<a class="btn btn-danger text-white" title="Try Again">\
                            <i class="fa fa-repeat"></i>\
                        </a>';
                    }
                    else {
                    }

                    return statusBtn;
                }
            }
            ]
        });

        var query = datatable.getDataSourceQuery();
        datatable.sort('createtime', 'desc');

        $('#m_form_search').on('keyup', function (e) {
            datatable.search($(this).val().toLowerCase());
        }).val(query.generalSearch);

        // $('#m_form_status').on('change', function () {
        // 	datatable.search($(this).val(), 'Status');
        // }).val(typeof query.Status !== 'undefined' ? query.Status : '');

        // $('#m_form_type').on('change', function () {
        // 	datatable.search($(this).val(), 'Type');
        // }).val(typeof query.Type !== 'undefined' ? query.Type : '');
        // $('#m_form_status, #m_form_type').selectpicker();

    };

    return {
        //== Public functions
        init: function () {
            // init dmeo
            demo();
        }
    };
}();

jQuery(document).ready(function () {
    DatatableDataLocalDemo.init();
});