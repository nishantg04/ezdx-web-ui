export class Files {
    "id": any;
    "path": any;
    "fileType": any;
    "filename": any;
    "checksum": any;
    "facilityId": any[];
    "centerId": any[];
    "deviceId": any[];
    "assignmentStatus": any;
    "userId": any;
    "version": any;
    "deleted": false;
    "uploadTime": any;
    "updateTime": any;
}