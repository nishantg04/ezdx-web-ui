import * as _ from 'lodash';

export class Utils {


    static validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    static validateUrl(url) {
        var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
        return re.test(url);
    }

    static returnNumbers(no) {
        const _10dig = /^\d{10}$/;
        var value = /^[0-9 ,+-]*$/.test(no);
        //phone_number.match(/^(1-?)?(\\([2-9]\\d{2}\\)|[2-9]\\d{2})-?[2-9]\\d{2}-?\\d{4}$/);
        return _10dig.test(no) || value;
    }

    static validatePhoneNumber(number) {
        const _10dig = /^\d{10}$/; // XXXXXXXXXX
        const _charspaces = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;  // XXX-XXX-XXXX | XXX.XXX.XXXX | XXX XXX XXXX
        const _withcountrycode = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/; // +XX XXXX XXXX | +XX-XXXX-XXXX | +XX.XXXX.XXXX

        return _10dig.test(number) || _charspaces.test(number) || _withcountrycode.test(number);
    }

    static validatePanNumber(panval) {
        // const _10dig = /^\d{10}$/;
        var value = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/.test(panval);
        return value;
    }

    static numberZeroCheck(num) {
        var zerosReg = /[1-9]/g.test(num);
        var value = num.match(/^(1-?)?(\\([2-9]\\d{2}\\)|[2-9]\\d{2})-?[2-9]\\d{2}-?\\d{4}$/);
        return zerosReg;
    }

    static specialCharacter(val) {
        var value = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(val);
        return value;
    }

    static getExtension(name) {
        return name.slice((name.lastIndexOf(".") - 1 >>> 0) + 2);
    }

    static returnImages(files) {
        return _.filter(files, function(file) {
            return _.includes(['jpeg', 'png', 'jpg', 'gif'], Utils.getExtension(file.name))
        })
    }

    static checkLimit(lowValue,upValue){
       if(lowValue<upValue)
         return true;
       else
         return false;
    }

}