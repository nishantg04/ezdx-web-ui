import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { UsersService } from '../users.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { UsersDetailsComponent } from './users-details.component';
import { CommonService } from '../../../../../../_services/common.service';
import { PatientsService } from '../../patients/patients.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TestsService } from "./../users-tests/tests.service";
import { DataTableModule, SharedModule, PaginatorModule } from 'primeng/primeng';
import { AmchartModule } from '../../../../../amchartcomponent/amchart.module';


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": UsersDetailsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, DataTableModule, SharedModule, PaginatorModule,
        TranslateModule.forChild({ isolate: false }), AmchartModule
    ], exports: [
        RouterModule
    ], declarations: [
        UsersDetailsComponent
    ],
    providers: [
        UsersService,
        FacilitiesService,
        CommonService,
        PatientsService,
        TranslateService, TestsService
    ]

})
export class UsersDetailsModule { }
