import { Injectable } from "@angular/core";
import { Headers, Http } from "@angular/http";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";

import "rxjs/add/operator/toPromise";
import { Helpers } from "./../../../../../helpers";

import { environment } from "../../../../../../environments/environment";

@Injectable()
export class MasterTestService {
    private headers = new Headers({ "Content-Type": "application/json", Authorization: "Bearer " + JSON.parse(localStorage.getItem("currentUser")).token });
    private serviceUrl = environment.BaseURL + environment.TestMasterURL; //'http://dev.ezdx.healthcubed.com/ezdx-device-command/api/v1/devices';  // URL to web api
    private testSearch = environment.BaseURL + environment.testNameSearch;

    constructor(
        private http: Http,
        private _router: Router,
        private _script: ScriptLoaderService
    ) { }

    getAllTests(): Promise<any[]> {
        return this.http.get(this.serviceUrl + "/search?searchCriteria=ALL", { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    updateTest(test): Promise<any> {
        return this.http
            .put(this.serviceUrl + "", test, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    testNameSearching(test): any {
        var url = this.testSearch + '?searchCriteria=TEST_NAME&testName=' + test;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load(
                "body",
                "assets/demo/default/custom/components/utils/redirect-cidaas-logout.js"
            );
        }
        return Promise.reject(error.message || error);
    }
}
