import { Injectable } from '@angular/core';
const url = 'https://maps.google.com/maps/api/js?key=AIzaSyDBGVDv5fOFgfW4ixNZL_2krgkriGu6vvc&libraries=places,drawing&callback=__onGoogleLoaded';


@Injectable()
export class MapLoaderService {

    constructor() { }
    private static promise;

    public static load() {
        // First time 'load' is called?
        if (!MapLoaderService.promise) {

            // Make promise to load
            MapLoaderService.promise = new Promise(resolve => {
                // console.log('Resolved');
                // Set callback for when google maps is loaded.
                window['__onGoogleLoaded'] = (ev) => {
                    // console.log('Maps Ready');
                    resolve('google maps api loaded');
                };

                let node = document.createElement('script');
                node.src = url;
                node.type = 'text/javascript';
                document.getElementsByTagName('head')[0].appendChild(node);
            });
        }

        // Always return promise. When 'load' is called many times, the promise is already resolved.
        return MapLoaderService.promise;
    }

}
