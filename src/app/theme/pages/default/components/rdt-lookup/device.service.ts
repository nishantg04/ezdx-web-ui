import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';

import 'rxjs/add/operator/toPromise';
import { Helpers } from './../../../../../helpers';

import { Device } from './device';
import { environment } from '../../../../../../environments/environment'
import { RDTEntity } from './RDTEntity';
import { RDTLookupJson } from './rdtFile';
import { RdtLookup } from './rdtLookup';

@Injectable()
export class DeviceService {
    private headers = new Headers({
        "Content-Type": "application/json",
        Authorization:
            "Bearer " + JSON.parse(localStorage.getItem("currentUser")).token
    });
    private serviceUrl = environment.BaseURL + environment.DeviceQueryURL; //'http://dev.ezdx.healthcubed.com/ezdx-device-command/api/v1/devices';  // URL to web api
    private serviceUrlRdt = environment.BaseURL + environment.FileSrvURL;

    constructor(
        private http: Http,
        private _router: Router,
        private _script: ScriptLoaderService
    ) { }

    getDeviceByCenter(centerId, page, limit): Promise<any> {
        var deviceUrl = this.serviceUrl + "/search?searchcriteria=CENTER&center=" + centerId + "&page=" + page + "&limit=" + limit;
        return this.http
            .get(deviceUrl, { headers: this.headers })
            .toPromise()
            .then(res => {
                // console.log("response: ", res.json());
                return res.json();
            })
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load(
                "body",
                "assets/demo/default/custom/components/utils/redirect-cidaas-logout.js"
            );
        }
        return Promise.reject(error.message || error);
    }
}