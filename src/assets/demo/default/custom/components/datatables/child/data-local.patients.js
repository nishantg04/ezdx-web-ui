//== Class definition
var DatatableChildDataLocalDemo = function() { //== Private functions

    var subTableInit = function(e) {
        var datatableCenter = $('<div/>').attr('id', 'child_data_local_' + e.data.id).appendTo(e.detailCell)
            .mDatatable({
                data: {
                    type: 'local',
                    source: e.data.centers,
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    }
                },

                // layout definition
                layout: {
                    theme: 'default',
                    scroll: false,
                    height: null,
                    footer: false,
                    spinner: {
                        type: 1,
                        theme: 'default'
                    }
                },

                sortable: true,

                // columns definition
                columns: [{
                    field: "name",
                    title: "Centers",
                    width: 300,
                    template: function(row) {
                        return '\
                            <a id="'+ row.id +'" class="csr-hand" onclick="my.namespace.navCenterDetails(\'' + row.id+'\',\''+row.facilityId+'\')">\
                                ' + row.name +'\
                            </a>\
                        ';
                    }
                }, {
                    field: "count",
                    title: "Patient registrations",
                    width: 300,
                }]
            });

        // datatableCenter.sort('count');
        // setTimeout(function() {
        //     datatableCenter.sort('count');
        // }, 500);
    }

    // demo initializer
    var mainTableInit = function() {
        //Refresh table by remove html tag
        $(".m_datatable").remove();
        var $newdiv1 = $("<div class='m_datatable'></div>");
        $(".m-portlet__body").append($newdiv1);

        var dataArray = JSON.parse(localStorage.getItem('patientsStatsTable'));
        var dataJSONArray = [];
        dataArray.forEach(element => {
            if (element.name != "ALL") {
                dataJSONArray.push(element);
            }
        });

        var datatable = $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: dataJSONArray,
                pageSize: 10,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },

            // layout definition
            layout: {
                theme: 'default',
                scroll: false,
                height: null,
                footer: false
            },

            sortable: true,

            filterable: false,

            pagination: true,

            detail: {
                title: 'Centers',
                content: subTableInit
            },

            // columns definition
            columns: [{
                field: "id",
                title: "",
                sortable: false,
                width: 20,
                textAlign: 'center' // left|right|center,
            }, {
                field: "name",
                width: 350,
                title: "Customers",
                template: function(row) {
                    return '\
						<a id="'+ row.id +'" class="csr-hand" onclick="my.namespace.navFacilityDetails(\'' + row.facilityId+'\')">\
                            ' + row.name +'\
                        </a>\
					';
                }
            }, {
                field: "count",
                width: 350,
                title: "Patient registrations"
            }]
        });

        var query = datatable.getDataSourceQuery();
        datatable.sort('count');
        setTimeout(function() {
            datatable.sort('count');
        }, 500);

        $('#m_form_search').on('keyup', function(e) {
            datatable.search($(this).val().toLowerCase());
        }).val(query.generalSearch);

        $('#m_form_status, #m_form_type').selectpicker();

    };

    return {
        //== Public functions
        init: function() {
            // init dmeo
            mainTableInit();
        }
    };
}();

jQuery(document).ready(function() {
    DatatableChildDataLocalDemo.init();
});