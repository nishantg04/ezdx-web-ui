import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkinconfigListComponent } from './skinconfig-list.component';

import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { InputTextModule, InputTextareaModule, DataTableModule, SharedModule, PaginatorModule, GrowlModule } from 'primeng/primeng';

import { ConfigService } from '../tests.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": SkinconfigListComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, InputTextModule, InputTextareaModule, DataTableModule, SharedModule, PaginatorModule, GrowlModule,
        TranslateModule.forChild({ isolate: false }),
    ], exports: [
        RouterModule
    ], declarations: [
        SkinconfigListComponent
    ],
    providers: [
        TranslateService,
        ConfigService
    ]

})
export class SkinconfigListModule { }


