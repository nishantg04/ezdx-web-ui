
export class Country {
    code: string = "";
    name: string = "";
    dial_code: string = "";
}