import { Component, OnInit, ViewEncapsulation, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { Ng4FilesStatus, Ng4FilesSelected, Ng4FilesService, Ng4FilesConfig } from "angular4-files-upload"
import { Facility } from '../facility';
import { Country } from '../country';
import { Center } from '../center';
import { Address } from '../address';
import { FacilitiesService } from '../facilities.service';
import { Observable } from 'rxjs/Observable';
import { Utils } from '../../utils/utils'
import { setTimeout } from 'timers';
import { CommonService } from '../../../../../../_services/common.service'
import { concat } from 'rxjs/operator/concat';
import { MapLoaderService } from '../../utils/map-loader.service';
import { Geopoint } from '../geopoint';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./facilities-add.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class FacilitiesAddComponent implements OnInit, AfterViewInit {
    autocomplete: google.maps.places.AutocompleteService;
    placedetail: google.maps.places.PlacesService;
    geoCoderDetail: google.maps.Geocoder;
    map: google.maps.Map;
    results: any;
    options: any = {
        center: { lat: 36.890257, lng: 30.707417 },
        zoom: 12,
    };
    overlays: any[] = [];
    facility = new Facility();
    selectedCenter = new Center();
    centerEditMode = false;
    facilityId: string;
    countires: Country[];
    loading: boolean;
    msg: string;
    errorPan: string;
    errorPhone: string;
    digitLength: string;
    userType: string;
    msgStatus: string;
    fieldError: string;
    errorEmail: string;
    zeroPhone: string;
    cusCode: boolean = false;
    centerNull: string;
    country_code: string;
    showError: boolean = false;
    uploadMsg: any;
    submitMsg: boolean = false;
    public selectedFiles;
    formData: FormData = new FormData();
    icon: any
    showDialog: boolean = false;
    showName: boolean = false;
    showAddress: boolean = false;
    backup: any = null;
    showMsg: boolean = false;
    centerCount: any;
    centerList: any[] = [];
    text: any;
    editMode: boolean = false;
    showAddError: boolean = false;
    singleClick: boolean = false;
    showNo : boolean = false;
    dontUpload : boolean = false;

    constructor(private _script: ScriptLoaderService, private _facilitiesService: FacilitiesService, private _router: Router, private _activeroute: ActivatedRoute,
        private CommonService: CommonService, private ng4FilesService: Ng4FilesService, private changed: ChangeDetectorRef, private mapLoaderService: MapLoaderService) {
        this.loading = false;
        this.selectedCenter = new Center();
        this.selectedCenter.address = new Address();
        this.selectedCenter.geopoint = new Geopoint();
        this.facility.centers = [];
        this.facility.address = new Address();
        this.facility.address.country = "India";
        this.facility.dial_code = "+91"
        this.country_code = "in"
        if (window.localStorage.getItem('userType') == 'Facility')
            this.userType = 'Facility';
        else
            this.userType = 'Admin';
        this.icon = 'https://maps.google.com/mapfiles/ms/icons/red-dot.png';
    }

    setMap(event) {
        this.map = event.map;
        // console.log('map ready', this.map);
    }

    getLocation() {
        this.overlays = [];
        if (navigator.geolocation) {
            // console.log('Getting location');
            navigator.geolocation.getCurrentPosition(position => {
                // console.log(position);
                this.options = {
                    center: { lng: position.coords.longitude, lat: position.coords.latitude },
                    zoom: 14
                };
                setTimeout(() => {
                    this.overlays.push(new google.maps.Marker({ position: { lat: position.coords.latitude, lng: position.coords.longitude }, map: this.map, draggable: true, icon: this.icon }));
                    this.map.panTo(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
                    this.selectedCenter.geopoint.lat = position.coords.latitude;
                    this.selectedCenter.geopoint.lon = position.coords.longitude;
                    const latlng = { lat: this.selectedCenter.geopoint.lat, lng: this.selectedCenter.geopoint.lon };
                    this.geoCoderDetail.geocode({ 'location': latlng }, (results, status) => {
                        if (results[0]) {
                            this.loadAddress(results[0]);
                        }
                    });
                }, 500)
            });
        };
    }

    ngOnInit() {
        MapLoaderService.load().then(() => {
            this.autocomplete = new google.maps.places.AutocompleteService();
            this.placedetail = new google.maps.places.PlacesService(document.createElement('div'))
            this.geoCoderDetail = new google.maps.Geocoder();
            //this.showMap = true;
        })
        this.getCountries();
        this.disableFun();
        this.ng4FilesService.addConfig(this.FileConfig);
        this.facilityId = this._activeroute.snapshot.queryParams['facilityId'] || '/';
        if (this.facilityId != '/') {
            Helpers.setLoading(true);
            this.facility.id = this.facilityId;
            this._facilitiesService.getFacility(this.facility.id)
                .then(facility => this.facility = facility)
                .then(() => {
                    Helpers.setLoading(false);
                    this.getFlag()
                    if (this.facility.phone === '0') {
                        this.facility.phone = null
                    }
                    if (this.facility.mobile === '0') {
                        this.facility.mobile = null
                    }
                    if (!this.facility.address) {
                        this.facility.address = new Address();
                        this.facility.address.country = "India";
                    }
                    if (!this.facility.centers) {
                        this.facility.centers = [];
                        this.centerCount = '0'
                    } else {
                        this.centerCount = this.facility.centers.length;
                        this.centerCount = this.centerCount.toString();
                    }
                });
        }

        document.getElementById('facility_name').focus();
        //  document.getElementById('facility_name').select();
    }

    private FileConfig: Ng4FilesConfig = {
        acceptExtensions: ['png', 'jpeg', 'jpg'],
        maxFilesCount: 1,
        maxFileSize: 5120000,
        totalFilesSize: 10120000
    };


    disableFun() {
        if (this._activeroute.snapshot.queryParams['facilityId'] == undefined)
            this.cusCode = true;
        else
            this.cusCode = false;
    }

    ngAfterViewInit() {
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/forms/validation/form-controls.js');
    }

    selected(event) {
        this.overlays = [];
        $('.ui-autocomplete-loader').remove();
        this.placedetail.getDetails({ placeId: event.place_id }, (results, status) => {
            if (results) {
                this.options.center.lat = results.geometry.location.lat();
                this.options.center.lng = results.geometry.location.lng();
                this.options.zoom = 12;
                this.overlays.push(new google.maps.Marker({ position: { lat: this.options.center.lat, lng: this.options.center.lng }, map: this.map, draggable: true, icon: this.icon }));
                this.map.panTo(new google.maps.LatLng(this.options.center.lat, this.options.center.lng));
                this.selectedCenter.geopoint.lat = this.options.center.lat;
                this.selectedCenter.geopoint.lon = this.options.center.lng;
                this.showAddError = false;
                // console.log(this.selectedCenter.geopoint)
                this.loadAddress(results)
                this.changed.detectChanges();
            }
        });
    }

    search(event) {
        // console.log(event) 

        this.autocomplete.getPlacePredictions({ input: event.query }, res => {
            // console.log('Places search', res);
            this.results = res;
        })
    }

    clearPoint() {
        this.overlays = [];
        this.clearAddress();
        this.options = { center: { lat: 36.890257, lng: 30.707417 }, zoom: 12 };
    }

    handleDragEnd(event, overlays) {
        const latlng = { lat: event.originalEvent.latLng.lat(), lng: event.originalEvent.latLng.lng() };
        this.selectedCenter.geopoint.lat = event.originalEvent.latLng.lat();
        this.selectedCenter.geopoint.lon = event.originalEvent.latLng.lng();
        this.geoCoderDetail.geocode({ 'location': latlng }, (results, status) => {
            if (results[0]) {
                if (event != null && event.overlay != null) {
                    this.loadAddress(results[0]);
                }
            }
        });
    }
    clearAddress() {
        this.selectedCenter.address.address = ''
        this.selectedCenter.address.state = ''
        this.selectedCenter.address.district = ''
        this.selectedCenter.address.postcode = ''
        this.selectedCenter.address.city = ''
        this.selectedCenter.address.country = ''
        //  this.getLocation();
    }

    loadAddress(place) {
        this.clearAddress();
        // console.log(place.address_components)
        place.address_components
            .forEach(function(comp, index) {
                switch (comp.types[0]) {
                    case "premise":
                        this.selectedCenter.address.address = comp.long_name;
                        break;
                    case "administrative_area_level_1":
                        this.selectedCenter.address.state = comp.long_name;
                        break;
                    case "administrative_area_level_2":
                        this.selectedCenter.address.district = comp.long_name;
                        break;
                    case "postal_code":
                        this.selectedCenter.address.postcode = parseFloat(comp.long_name);
                        break;
                    case "locality":
                        this.selectedCenter.address.city = comp.long_name;
                        break;
                    case "route":
                        if (this.selectedCenter.address.address != "") {
                            this.selectedCenter.address.address = this.selectedCenter.address.address + ' ' + comp.long_name
                        } else
                            this.selectedCenter.address.address = comp.long_name
                        break;
                    case "country":
                        this.selectedCenter.address.country = comp.long_name;
                        break;
                    default:
                        break;
                }
            }, this);
        // console.log(this.selectedCenter.geopoint)

    }

    clearMsg() {
        this.digitLength = null;
        this.errorEmail = null;
        this.errorPhone = null;
        this.fieldError = null;
        this.zeroPhone = null;
        this.msgStatus = null;
        this.msg = null;
        this.centerNull = null;
    }

    getCountries(): void {
        this.CommonService.getCountries().then(
            countries => {
                this.countires = countries
            }
        );
    }
    countrySelect(event) {
        //alert(JSON.stringify(this.countires[event.target.selectedIndex]))
        var selectedCountryObject = this.countires[event.target.selectedIndex];
        this.facility.address.country = selectedCountryObject.name;
        this.facility.dial_code = selectedCountryObject.dial_code;
        this.country_code = selectedCountryObject.code;
    }

    getFlag() {
        this.countires.forEach(item => {
            if (item.name == this.facility.address.country)
                this.country_code = item.code;
        })
    }


    valPan() {
        this.fieldError = null;
        if (this.facility.panNumber) {
            if (Utils.validatePanNumber(this.facility.panNumber) === false) {
                this.errorPan = "ENTER_VALID_PAN_NUMBER"
            } else
                this.errorPan = null;
        } else {
            this.errorPan = null;
            return true;
        }
    }

    charCheck(event) {
        return !Utils.specialCharacter(event.key);
    }

    checkCenter() {
        this.centerNull = null;
        this.fieldError = null;
        if (this.facility.centers.length === 0) {
            this.centerNull = "ADD_ONE_OR_MORE_CENTER_FOR_CUSTOMER"
        } else {
            this.centerNull = null;
        }
    }

    checkOrgCode(code) {
        if (code) {
            this.fieldError = null;
            document.getElementById('loading_icon').style.display = 'block'
            this._facilitiesService.getOrgCode(code).then(data => {
                document.getElementById('loading_icon').style.display = 'none'
                if (data == null) {
                    this.showError = false;
                } else {
                    this.facility.organizationCode = code;
                    this.showError = true;
                }
            })
        }
    }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    save(): void {
        //this.submitMsg = false;
        this.msg = null;
        this.fieldError = null;
        this.centerNull = null;
        if (!this.loading) {
            this.checkCenter();
            if (this.errorEmail == null && this.errorPhone == null && this.centerNull == null && this.showError == false && this.errorPan == null) {
                Helpers.setLoading(true);
                this.loading = true;
                if (!this.facility.id) {
                    this.facility.name = this.capitalizeFirstLetter(this.facility.name)
                    this._facilitiesService.create(this.facility)
                        .then((data) => {
                            // console.log(data)
                            this.facility = data;
                            if (this.facility.id && this.dontUpload == true)
                                this.uploadFacilityLogo();
                        })
                        .then(() => {
                            this.getAllFacilities();
                            Helpers.setLoading(false);

                        }, (e: any) => {
                            // console.log(e);
                            let res = JSON.parse(e._body);
                            if (res.error) {
                                this.msg = res.error;
                            }
                            Helpers.setLoading(false);
                            this.loading = false
                        });

                }
                else {
                    if (this.submitMsg == true && this.dontUpload == true) {
                        this.facility.logo = ''
                    }
                    this._facilitiesService.update(this.facility)
                        .then((data) => {
                        })
                        .then(() => {
                            Helpers.setLoading(false);
                            this.msgStatus = "CUSTOMER_DETAILS_UPDATED_SUCCCESSFULLY";
                            if (this.facility.id && this.dontUpload == true)
                                this.uploadFacilityLogo();
                            setTimeout(() => {
                                this.getAllFacilities();
                                this.msgStatus = null;
                            }, 1000);
                        }, (e: any) => {
                            // console.log(e);
                            let res = JSON.parse(e._body);
                            if (res.error) {
                                this.msg = res.error;
                            }
                            Helpers.setLoading(false);
                            this.loading = false
                        });
                }
            } else {
                this.fieldError = "CORRECT_THE_FIELD_ERROR_THEN_TRY_TO_SUMBIT"
            }
        }
    }

    public filesSelect(selectedFiles: Ng4FilesSelected): void {
        this.showNo = false;
        this.uploadMsg = null;
        if (selectedFiles.status === Ng4FilesStatus.STATUS_MAX_FILES_COUNT_EXCEED) {
            this.uploadMsg = 'File Count Exceed';
        }
        if (selectedFiles.status === Ng4FilesStatus.STATUS_MAX_FILES_TOTAL_SIZE_EXCEED) {
            this.uploadMsg = 'Max Total File Size Exceed';
        }
        if (selectedFiles.status === Ng4FilesStatus.STATUS_MAX_FILE_SIZE_EXCEED) {
            this.uploadMsg = 'Max File Size Exceed';
        }
        if (selectedFiles.status === Ng4FilesStatus.STATUS_NOT_MATCH_EXTENSIONS) {
            this.uploadMsg = 'Extension Not Matched';
        }
        var FileUploadPath = selectedFiles.files[0].name
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
        if (Extension === "png" || Extension === "jpeg" || Extension === "jpg" || Extension === "gif") {
            if (selectedFiles.status === Ng4FilesStatus.STATUS_SUCCESS) {
                console.log(selectedFiles)                
                this.selectedFiles = Array.from(selectedFiles.files).map(file =>
                    this.readImageFile(file)                    
                );

                this.submitMsg = true;
            }
        } else {
            //alert('upload valid image')
        }
    }

    readImageFile(file) {
        var width
        var height
        var v 
        var h
        var reader = new FileReader();        
        reader.onload = function (e) {
            console.log(e)
            var img = new Image();      
            img.src = e.target['result'];
            img.onload = function () {
                v = img.width;
                h = img.height;                   
            }
        };
        reader.readAsDataURL(file); 
        this.formData.append('uploadfile', file)
        setTimeout(()=>{              
        if( v >= 100 && v <= 512 && h >= 100 && h <=512){  
            this.dontUpload  = true;      
            console.log('enter',this.dontUpload)
            this.formData.append('uploadfile', file)  
        }else{
           this.showNo = true;
           this.dontUpload  = false;   
        }           
        },100)        
    }


    uploadFacilityLogo() {
        if (this.selectedFiles) {
            Helpers.setLoading(true);
            this._facilitiesService.uploadLogo(this.facility.organizationCode, this.formData)
                .then(data => {
                    this.facility.logo = data.logo;
                    this.submitMsg = false;
                    Helpers.setLoading(false);
                })
        } else {
            setTimeout(() => {
                if (this.userType == 'Admin')
                    this._router.navigate(['/facilities/list']);
                else
                    this._router.navigate(['/facilities/details'], { queryParams: { facilityId: this.facility.organizationCode } });
            }, 1000);
        }
    }

    getAllFacilities() {
        Helpers.setLoading(true);
        this._facilitiesService.getFacilities(0, 0)  // for get All facility we should pass page and size as zero
            .then(facilities => {

                localStorage.setItem('facilityTable', JSON.stringify(facilities.organizations)); // for save all facility details
                // setTimeout(() => {
                //     this._router.navigate(['/facilities/list']);
                // }, 600);
                if (localStorage.getItem('facilityTable') != undefined || localStorage.getItem('facilityTable') != null) {
                    Helpers.setLoading(false);
                    if (this.userType == 'Admin')
                        this._router.navigate(['/facilities/list']);
                    else
                        this._router.navigate(['/facilities/details'], { queryParams: { facilityId: this.facility.organizationCode } });
                }
            })
    }

    checkRoute() {
        if (this.userType == 'Admin')
            this._router.navigate(['/facilities/list']);
        else
            this._router.navigate(['/facilities/details'], { queryParams: { facilityId: this.facility.organizationCode } });
    }

    addCenter(): void {
        if (!this.centerEditMode) {
            var centerToPush = jQuery.extend(true, {}, this.selectedCenter);
            this.facility.centers.push(centerToPush);
        }
        this.centerEditMode = false;
        // this.selectedCenter = new Center();
        // this.selectedCenter.address = new Address();
        // this.selectedCenter.address = jQuery.extend(true, {}, this.facility.address);       
        jQuery('#close_center_modal').click();
    }

    editCenter(center): void {
        this.selectedCenter = center;
        this.centerEditMode = true;
        if (this.selectedCenter.geopoint == null || this.selectedCenter.geopoint == undefined)
            this.selectedCenter.geopoint = new Geopoint();
        jQuery('#m_modal_center_add_button').click();
    }

    saveCenter(center) {
        // console.log(center)
        this.centerEditMode = false;
    }

    openAddCenterModal(): void {
        this.centerEditMode = true;
        this.selectedCenter = new Center();
        this.clearAddress();
        this.selectedCenter.address = jQuery.extend(true, {}, this.facility.address);
        jQuery('#m_modal_center_add_button').click();
    }

    copyAddressForCenter(): void {
        if (!this.selectedCenter.name) {
            this.centerNull = null;
            this.fieldError = null;
            this.selectedCenter = new Center();
            this.selectedCenter.address = new Address();
            this.selectedCenter.geopoint = new Geopoint();
            //this.selectedCenter.address = jQuery.extend(true, {}, this.facility.address);
        }
    }

    closeCenterModal(): void {
        if (this.selectedCenter.centerCode && this.selectedCenter != null) {
            var backup;
            if (window.localStorage.getItem('backup')) {
                backup = JSON.parse(window.localStorage.getItem('backup'))
                this.selectedCenter.name = backup.name;
                this.showDialog = false;
                this.selectedCenter = new Center();
                this.selectedCenter.address = new Address();
            }
        } else {
            this.showDialog = false;
        }

    }

    removeCenter(center): void {
        const index: number = this.facility.centers.indexOf(center);
        if (index !== -1) {
            this.facility.centers.splice(index, 1);
            this.centerCount = this.facility.centers.length;
        }
        this.checkCenter();
    }

    valEmail() {
        this.fieldError = "";
        if (this.facility.email) {
            if (Utils.validateEmail(this.facility.email) === false) {
                this.errorEmail = "";
            } else
                this.errorEmail = null;
        }
        else {
            this.errorEmail = null;
            return true;
        }
    }


    valPhone() {
        this.digitLength = null
        this.fieldError = null;
        if (this.facility.mobile) {
            if (Utils.returnNumbers(this.facility.mobile) === false) {
                this.errorPhone = "ENTER_VALID_PHONE_NUMBER"
            } else
                this.errorPhone = null;
        } else {
            this.errorPhone = null
            return true;
        }
    }

    numberChange(Pnumber) {
        if (Pnumber) {
            if (Pnumber.length !== 10) {
                // console.log(Pnumber.length);
                this.digitLength = "ENTER_10_DIGIT_NUMBER"
            } else
                this.digitLength = null
            if (!Utils.numberZeroCheck(Pnumber)) {
                this.digitLength = null
                this.errorPhone = "ENTER_VALID_PHONE_NUMBER"
                document.getElementById('user_phone').focus();
            } else
                this.errorPhone = null;
        }
    }

    updateCenter(center) {
        this.overlays = [];
        this.backup = center;
        this.dirtyRemove();
        this.editMode = true;
        window.localStorage.setItem('backup', JSON.stringify(this.backup))
        this.selectedCenter = this.backup;
        if (this.selectedCenter.geopoint) {
            this.options.center.lat = this.selectedCenter.geopoint.lat
            this.options.center.lng = this.selectedCenter.geopoint.lon
            this.overlays.push(new google.maps.Marker({ position: { lat: this.options.center.lat, lng: this.options.center.lng }, map: this.map, draggable: true, icon: this.icon }));
            // console.log(this.map)
            setTimeout(() => {
                this.map.setCenter(this.options.center)
            }, 500)

        } else {
            this.showMsg = true;
            this.selectedCenter.geopoint = new Geopoint();
        }
        this.showDialog = true;
    }

    addNewCenter() {
        this.getLocation();
        this.showDialog = true;
        this.showAddress = true;
        this.showMsg = false;
        this.text = "";
        this.selectedCenter = new Center()
        this.selectedCenter.address = new Address()
        this.selectedCenter.geopoint = new Geopoint()
        this.dirtyRemove();
    }

    dirtyRemove() {
        if (document.getElementById('c_name').classList['value'] == 'form-group has-danger') {
            document.getElementById('c_name').classList['value'] = 'form-group'
            document.getElementById('recipient-name-error').style.display = 'none';
        }
    }

    close() {
        if (this.backup) {
            if (this.selectedCenter.name != "") {
                this.showAddress = false;
                this.selectedCenter = this.backup;
            }
        } else {
            this.showAddress = true;
        }
    }

    saveNewCenter(center) {
        if (this.overlays.length == 0) {
            if (center.name == '')
                this.showName = true;
            this.showAddress = true;
            this.showAddError = true;
            if (this.backup)
                this.selectedCenter = this.backup;
        } else {
            this.showName = false;
            this.showAddress = false;
            if (this.facility.id) {
                this.singleClick = true;
                this.creatingCenter(center)
            } else {
                if (!center.centerCode && this.editMode == false) {
                    this.facility.centers.push(center)
                    this.centerCount = this.facility.centers.length
                    this.checkCenter();
                }
                this.showDialog = false;
                this.editMode = false;
            }
        }
    }


    creatingCenter(centerList) {
        this.facility.centers = [];
        this._facilitiesService.createCenter(this.facility.id, centerList)
            .then(data => {
                this.facility.centers = data.centers;
                this.checkCenter();
                this.showDialog = false;
                this.singleClick = false;
            })
    }


    expand() {
        if (document.getElementById('center_expand').classList['value'] == 'm-portlet__body') {
            document.getElementById('center_expand').style.display = 'block'
            document.getElementById('center_expand').classList['value'] = 'm-portlet__body a'
            document.getElementById('exicon').classList['value'] = "la la-chevron-up"
            window.scroll({ top: 2500, left: 0, behavior: 'smooth' });
        }
        else {
            document.getElementById('center_expand').style.display = 'none'
            document.getElementById('center_expand').classList['value'] = 'm-portlet__body'
            document.getElementById('exicon').classList['value'] = "la la-chevron-down"
            window.scroll({ top: 50, left: 0, behavior: 'smooth' });
        }
    }

    paginate(e) {

    }


}