import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ConsumablesMapComponent } from './consumables-map.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { ConsumablesService } from '../consumables.service';
import { OrderModule } from 'ngx-order-pipe';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ConsumablesMapComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule,
        TranslateModule.forChild({ isolate: false }),

    ], exports: [
        RouterModule
    ], declarations: [
        ConsumablesMapComponent
    ],
    providers: [
        ConsumablesService,
        TranslateService
    ]

})
export class ConsumablesMapModule {



}