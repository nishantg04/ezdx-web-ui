import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { RdtEditComponent } from './rdt-edit.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { RdtService } from '../rdt.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { OrderModule } from 'ngx-order-pipe';
import { InputTextModule, InputTextareaModule, DataTableModule, SharedModule, PaginatorModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": RdtEditComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule, InputTextModule, InputTextareaModule,
        DataTableModule, SharedModule, PaginatorModule, TranslateModule.forChild({ isolate: false }),
    ], exports: [
        RouterModule
    ], declarations: [
        RdtEditComponent
    ],
    providers: [
        RdtService,
        FacilitiesService,
        TranslateService
    ]

})
export class RdtEditModule {



}