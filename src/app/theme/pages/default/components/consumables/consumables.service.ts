import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { Helpers } from './../../../../../helpers';
import { environment } from '../../../../../../environments/environment'

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ConsumablesService {

    private headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private serviceUrl = environment.BaseURL + environment.ConsumableCommandURL;
    private serviceUrl2 = environment.BaseURL + environment.ConsumableQueryURL;
    private serviceUrlOrderCommand = environment.BaseURL + environment.OrderCommandURL;
    private serviceUrlOrderQuery = environment.BaseURL + environment.OrderQueryURL;

    constructor(private http: Http, private _router: Router, private _script: ScriptLoaderService) { }

    create(consumable: any): Promise<any> {
        return this.http
            .post(this.serviceUrl + "/master", JSON.stringify(consumable), { headers: this.headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError.bind(this));
    }

    update(consumable: any): Promise<any> {
        return this.http
            .put(this.serviceUrl + "/master", JSON.stringify(consumable), { headers: this.headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError.bind(this));
    }

    list(pageno, pagesize, search, searchterm, orderby, sortby): Promise<any> {
        var url = this.serviceUrl2 + "/master/search?searchcriteria=ALL&page=" + pageno + "&limit=" + pagesize + "&orderBy=" + orderby + "&sort=" + sortby;
        if (search === "TEST_TYPE")
            url = this.serviceUrl2 + "/master/search?searchcriteria=TEST_TYPE&page=" + pageno + "&limit=" + pagesize + "&orderBy=" + orderby + "&sort=" + sortby + "&testType=" + searchterm;
        if (search === "TEST_NAME")
            url = this.serviceUrl2 + "/master/search?searchcriteria=TEST_NAME&page=" + pageno + "&limit=" + pagesize + "&orderBy=" + orderby + "&sort=" + sortby + "&testName=" + searchterm;
        if (search === "CONSUMABLE_TYPE")
            url = this.serviceUrl2 + "/master/search?searchcriteria=CONSUMABLE_TYPE&page=" + pageno + "&limit=" + pagesize + "&orderBy=" + orderby + "&sort=" + sortby + "&consumableType=" + searchterm;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    listForCenter(pageno, pagesize, search, searchterm, orderby, sortby, centerId): Promise<any> {
        var url = this.serviceUrl2 + "/center/" + centerId + "/search?searchcriteria=ALL&page=" + pageno + "&limit=" + pagesize + "&orderBy=" + orderby + "&sort=" + sortby;
        if (search === "TEST_TYPE")
            url = this.serviceUrl2 + "/center/" + centerId + "/search?searchcriteria=TEST_TYPE&page=" + pageno + "&limit=" + pagesize + "&orderBy=" + orderby + "&sort=" + sortby + "&testType=" + searchterm;
        if (search === "TEST_NAME")
            url = this.serviceUrl2 + "/center/" + centerId + "/search?searchcriteria=TEST_NAME&page=" + pageno + "&limit=" + pagesize + "&orderBy=" + orderby + "&sort=" + sortby + "&testName=" + searchterm;
        if (search === "CONSUMABLE_TYPE")
            url = this.serviceUrl2 + "/center/" + centerId + "/search?searchcriteria=CONSUMABLE_TYPE&page=" + pageno + "&limit=" + pagesize + "&orderBy=" + orderby + "&sort=" + sortby + "&consumableType=" + searchterm;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    listOrder(pageno, pagesize, search, searchterm, orderby, sortby): Promise<any> {
        var url = this.serviceUrlOrderQuery + "/search?searchcriteria=ALL&page=" + pageno + "&limit=" + pagesize + "&sortBy=" + orderby + "&sort=" + sortby;
        if (search === "FACILITY")
            url = this.serviceUrlOrderQuery + "/search?searchcriteria=FACILITY&page=" + pageno + "&limit=" + pagesize + "&sortBy=" + orderby + "&sort=" + sortby + "&facilityId=" + searchterm;
        if (search === "CENTER")
            url = this.serviceUrlOrderQuery + "/search?searchcriteria=CENTER&page=" + pageno + "&limit=" + pagesize + "&sortBy=" + orderby + "&sort=" + sortby + "&centerId=" + searchterm;
        if (search === "USER")
            url = this.serviceUrlOrderQuery + "/search?searchcriteria=USER&page=" + pageno + "&limit=" + pagesize + "&sortBy=" + orderby + "&sort=" + sortby + "&userId=" + searchterm;
        if (search === "ORDER_STATUS")
            url = this.serviceUrlOrderQuery + "/search?searchcriteria=ORDER_STATUS&page=" + pageno + "&limit=" + pagesize + "&sortBy=" + orderby + "&sort=" + sortby + "&orderStatus=" + searchterm;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this))
    }

    createReorder(reorder: any): Promise<any> {
        return this.http
            .post(this.serviceUrlOrderCommand, JSON.stringify(reorder), { headers: this.headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError.bind(this));
    }

    updateReorder(reorder: any): Promise<any> {
        return this.http
            .put(this.serviceUrlOrderCommand + "/" + reorder.id + "/dispatch", JSON.stringify(reorder), { headers: this.headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError.bind(this));
    }

    acceptOrder(reorder: any): Promise<any> {
        return this.http
            .put(this.serviceUrlOrderCommand + "/" + reorder.id + "/receive", {}, { headers: this.headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError.bind(this));
    }

    rejectOrder(reorder: any): Promise<any> {
        return this.http
            .put(this.serviceUrlOrderCommand + "/" + reorder.id + "/reject", { headers: this.headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError.bind(this));
    }

    allocateOrder(reorder: any): Promise<any> {
        return this.http
            .put(this.serviceUrlOrderCommand + "/" + reorder.id + "/allocate", JSON.stringify(reorder.orderShipments), { headers: this.headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load('body',
                'assets/demo/default/custom/components/utils/redirect-cidaas-logout.js');
        }
        return Promise.reject(error.message || error);
    }

}