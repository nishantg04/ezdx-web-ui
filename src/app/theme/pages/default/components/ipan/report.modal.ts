import { Test } from "./test.modal";
export interface Report {
    id?: string;
    test: Test;
    mrn: string;
    facilityName: string;
    centerName: string;
    serialNumber: string;
    patientId: string;
    imei: string;
    operatorName: string;
    deviceFirmwareVersion: string;
    devicePyVersion: string;
    deviceFirmwareLastUpdated: number;
    devicePyLastUpdated: number;
    ezdxAppVersion: string;
    androidVersion: string;
    visitId?: string;
}
