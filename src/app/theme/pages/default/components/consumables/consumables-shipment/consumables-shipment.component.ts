import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { ConsumablesService } from '../consumables.service';
import { Facility } from "../../facilities/facility";
import { Center } from "../../facilities/center";
import { FacilitiesService } from "../../facilities/facilities.service";


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./consumables-shipment.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class ConsumablesShipmentComponent implements OnInit, AfterViewInit {
    shipments: any[];
    facilities: any[];
    selectedFacility: any;
    centers: any[];
    selectedCenter: any;
    constructor(private _facilitiesService: FacilitiesService, private _ngZone: NgZone, private _script: ScriptLoaderService, private _consumablesServiceService: ConsumablesService, private _router: Router, private _activeroute: ActivatedRoute) {
        this.shipments = [
            { id: 'asdd', name: "Cholesterol strips", lotno: "H17006057", manufacturer: "Online Surgicals", createtime: "20/12/2017 18:50 PM", reorder: "N/A", txn: "84273645", quantity: 200, status: 'dispatched', customer: "Healthcubed", center: "Bangalore", selected: false, user: "hemant.gogia@widas.in" },
            { id: '123', name: "Urine strips", lotno: "H17006057", manufacturer: "Online Surgicals", createtime: "20/12/2017 16:00 PM", reorder: "HLT57342", txn: "84273643", quantity: 500, status: 'shipped', customer: "Healthcubed", center: "Bangalore", selected: false, user: "sanjay.maniangode@widas.in" },
            { id: '532', name: "Hemoglobin strips", lotno: "H17006056", manufacturer: "ABC Surgicals", createtime: "20/12/2017 14:00 PM", reorder: "HLT57340", txn: "84273641", quantity: 250, status: 'delivered', customer: "Healthcubed", center: "Bangalore", selected: false, user: "sanjay.maniangode@widas.in" },
            { id: '3343', name: "Cholesterol strips", lotno: "H17006054", manufacturer: "Online Surgicals", createtime: "20/12/2017 12:20 PM", reorder: "HLT57345", txn: "84273639", quantity: 100, status: 'rejected', customer: "Healthcubed", center: "Bangalore", selected: false, user: "pavan.g@widas.in" }
        ];
    }
    ngOnInit() {
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
    }
    ngOnDestroy() {
        window.my.namespace.publicFunc = null;
    }
    ngAfterViewInit() {
        this.list();
        //this.getFacilities();
        // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
        //     'assets/demo/default/custom/components/forms/validation/form-controls.js');
    }

    list(): void {
        localStorage.setItem('consumablesTable', JSON.stringify(this.shipments));
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/datatables/base/data-local.consumables.js');
    }

    getFacilities(): void {
        Helpers.setLoading(true);
        this._facilitiesService
            .getFacilities(1, 10)
            .then(facilities => {
                this.facilities = facilities.organizations;
            })
            .then(() => {
                this.facilities.sort(function(a, b) {
                    return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
                });

                this.selectedFacility = jQuery.extend(true, {}, this.facilities[0]);
                this.centers = this.selectedFacility.centers;
                if (this.centers) {
                    this.centers.sort(function(a, b) {
                        return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
                    });
                    this.selectedCenter = jQuery.extend(true, {}, this.centers[0]);
                }
                // let allFacility = new Facility();
                // allFacility.name = "ALL";
                // allFacility.id = "ALL";
                // this.facilities.splice(0, 0, allFacility);
                //this.selectedFacility.id = this.facilities[0].id;
            });
    }

    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers) {
                    this.centers.sort(function(a, b) {
                        return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
                    });
                    this.selectedCenter = jQuery.extend(true, {}, this.centers[0]);
                }
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }

    onCenterChange(centerId: string): void {
    }

}