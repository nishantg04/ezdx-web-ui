import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { UserChild } from '../../pages/default/components/users/userChild';
import { UsersService } from '../../pages/default/components/users/users.service';
import { FacilitiesService } from '../../pages/default/components/facilities/facilities.service';
import { Facility } from '../../pages/default/components/facilities/facility';
import { ConfigService } from '../../pages/default/components/skinconfig/tests.service';
import { setTimeout } from 'timers';
import { TranslateService } from '@ngx-translate/core';

declare let mLayout: any;
@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {
    lang: any;
    user: UserChild;
    showSidebar: boolean;
    languageList: any[];
    selectedLanguage: string;
    Selectedcountry_code: any;
    sidebarToggle: boolean = false;
    sidebarToggleValue: string;
    facilities: Facility[];
    currentFacilityName: string;
    isAdmin: string;
    showLang: boolean = false;
    facLogo: any;
    constructor(private _usersService: UsersService, private _facilitiesService: FacilitiesService, private translate: TranslateService, private _configService: ConfigService) {
        this.translate.setDefaultLang('lang-en');
        this.lang = window.localStorage.getItem('lang');
        this.lang = this.lang ? this.lang : 'lang-en';
        // console.log('language locally stored', this.lang);
        window.localStorage.setItem('lang', this.lang)
        this.languageList = [
            {
                name: 'English',
                value: 'lang-en',
                country_code: 'gb'

            },
            {
                name: 'Portuguese',
                value: 'lang-pt',
                country_code: 'pt'

            },
            {
                name: 'Spanish',
                value: 'lang-es',
                country_code: 'es'

            },
            {
                name: 'Bangla',
                value: 'lang-bd',
                country_code: 'bd'

            }
        ];
        if (this.lang === 'lang-pt') {
            this.selectedLanguage = this.languageList[1].name;
            this.Selectedcountry_code = this.languageList[1].country_code;
        }
        else if (this.lang === 'lang-es') {
            this.selectedLanguage = this.languageList[2].name;
            this.Selectedcountry_code = this.languageList[2].country_code;
        }
        else if (this.lang === 'lang-bd') {
            this.selectedLanguage = this.languageList[3].name;
            this.Selectedcountry_code = this.languageList[3].country_code;
        }
        else {
            this.Selectedcountry_code = this.languageList[0].country_code;
            this.selectedLanguage = this.languageList[0].name;
        }

        this.showSidebar = true;

        // if (window.localStorage.getItem('userInfo'))
        //     this.user = JSON.parse(window.localStorage.getItem('userInfo'));

        if (window.localStorage.getItem('userChanged') === "true") {

            this._usersService.getCurrentUser()
                .then(user => this.user = user)
                .then(() => {
                    window.localStorage.setItem('userInfo', JSON.stringify(this.user));
                    window.localStorage.setItem('userChanged', "false");
                    let criteria = 'FACILITY_ID&facilityId=' + this.user.facilityId;
                    this._configService.getconfigfileForUser(criteria).then(res => {
                        localStorage.setItem('config', JSON.stringify(res.configs[0]));
                        console.log(res);
                        function capitalize(s) {
                            var str = s.toLowerCase();
                            return str && str[0].toUpperCase() + str.slice(1);
                        }
                        this.languageList.map((lang) => {
                            if (capitalize(res.configs[0].language) == 'Portugese') {
                                this.change({
                                    name: 'Portuguese',
                                    value: 'lang-pt',
                                    country_code: 'pt'
                                });
                            } else {
                                if (lang.name == capitalize(res.configs[0].language)) {
                                    this.change(lang)
                                }
                            }

                        })
                    });
                    this.setUserDetails();

                });
        }
        else {
            this.user = JSON.parse(window.localStorage.getItem('userInfo'));
            this.setUserDetails();
        }
    }



    setUserDetails(): void {
        var isadmin = true;
        this.user.userRoles.forEach(role => {
            if (role === "OPERATOR" || role === "DOCTOR" || role === "TECHNICIAN" || role === "CENTER_ADMIN")
                isadmin = false;
        });
        if (!isadmin) {
            this.isAdmin = "Facility";
        }
        else {
            this.isAdmin = "Admin";
        }
        if (this.isAdmin === "Facility") {
            this.getFacilities();
        }

    }

    get currentCountryValue(): string {
        return this.translate.currentLang;
    }

    ngOnInit() {

        this.selectLang(this.lang);
        if (window.location.href.indexOf("privacy") !== -1) {
            this.showSidebar = false;
        }
        if (localStorage.getItem('Sidebar')) {
            $('#m_aside_left_minimize_toggle').addClass('m-brand__toggler—active');
        }

    }

    ngAfterViewInit() {
        mLayout.initHeader();
    }

    change(lang) {
        this.selectLang(lang.value);
        this.lang = lang.value;
        window.localStorage.setItem('lang', this.lang);
        this.selectedLanguage = lang.name;
        this.Selectedcountry_code = lang.country_code;
        // console.log('language stored', this.lang);
    }

    selectLang(lang: string) {
        this.translate.use(lang);
    }

    getRoleName(role): any {
        var roleName = "Admin";
        switch (role) {
            case "HC_OPERATIONS":
                roleName = "OPERATIONS"
                break;
            case "HC_MANAGER":
                roleName = "MANAGER"
                break;
            case "OPERATOR":
                roleName = "OPERATOR"
                break;
            case "DOCTOR":
                roleName = "DOCTOR"
                break;
            case "TECHNICIAN":
                roleName = "CUSTOMER_ADMIN"
                break;
            case "PATHOLOGIST":
                roleName = "PATHOLOGIST"
                break;
            case "CENTER_ADMIN":
                roleName = "CENTER ADMIN"
                break;
            default:
                break;
        }
        return roleName;
    }

    getFacilities(): void {
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.forEach(fclt => {
            if (fclt.id === this.user.facilityId) {
                window.localStorage.setItem('facilityName', fclt.name);
                this.currentFacilityName = fclt.name;
                this.facLogo = fclt.logo;
                // console.log('fac', this.facLogo)
            }
        });
    }


    // $('m_aside_left_minimize_toggle').click()['prevObject'][0].activeElement 
    //$('#m_aside_left_minimize_toggle').addClass('m-brand__toggler--active')

    toggle(e) {

        var tog = document.getElementsByClassName('m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block m-brand__toggler—active m-brand__toggler--active')[0];

        if (tog == undefined) {
            setTimeout(() => {
                document.getElementById('user-role').style.left = "100px";
            }, 100);
        } else {
            document.getElementById('user-role').style.left = "";
        }
    }




}