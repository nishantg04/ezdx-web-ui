import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';

import 'rxjs/add/operator/toPromise';
import { Helpers } from './../../../../../helpers';

import { Device } from './device';
import { environment } from '../../../../../../environments/environment'
@Injectable()
export class DevicesService {

    private headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });
    private serviceUrl2 = environment.BaseURL + environment.DeviceCommandURL; //'http://dev.ezdx.healthcubed.com/ezdx-device-command/api/v1/devices';  // URL to web api

    private serviceUrl = environment.BaseURL + environment.DeviceQueryURL; //'http://dev.ezdx.healthcubed.com/ezdx-device-query/api/v1/devices';
    private serialDeviceUrl = environment.BaseURL + environment.DeviceQueryURL + '/serial'
    private statusUrl = environment.BaseURL + environment.DeviceStatus;
    private multipartHeader = new Headers({ 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token });

    constructor(private http: Http, private _router: Router, private _script: ScriptLoaderService) { }

    getDevices(): Promise<any> {
        return this.http.get(this.serviceUrl + "/search?searchcriteria=ALL", { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }
    getDevicesDate(fromDate, toDate, searchcriteria, id): Promise<any> {
        var getUrl = this.serviceUrl + "/search/date?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/search/date?searchcriteria=ORGANIZATION&organization=" + id + "&from=" + fromDate + "&to=" + toDate;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/search/date?searchcriteria=CENTER&center=" + id + "&from=" + fromDate + "&to=" + toDate;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() : [] })
            .catch(this.handleError.bind(this));
    }

    getDevicesPaginated(pageno, size): Promise<any> {
        return this.http.get(this.serviceUrl + "/search?searchcriteria=ALL" + "&page=" + pageno + "&limit=" + size, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }
    getDevicesDatePaginated(fromDate, toDate, searchcriteria, id, pageno, size, status): Promise<any> {
        var getUrl = this.serviceUrl + "/search/date?searchcriteria=ALL&from=" + fromDate + "&to=" + toDate + "&page=" + pageno + "&limit=" + size + "&status=" + status;
        if (searchcriteria == "FACILITY")
            getUrl = this.serviceUrl + "/search/date?searchcriteria=ORGANIZATION&organization=" + id + "&from=" + fromDate + "&to=" + toDate + "&page=" + pageno + "&limit=" + size + "&status=" + status;
        if (searchcriteria == "CENTER")
            getUrl = this.serviceUrl + "/search/date?searchcriteria=CENTER&center=" + id + "&from=" + fromDate + "&to=" + toDate + "&page=" + pageno + "&limit=" + size + "&status=" + status;
        return this.http.get(getUrl, { headers: this.headers })
            .toPromise()
            .then(response => { return (response.status === 200) ? response.json() : [] })
            .catch(this.handleError.bind(this));
    }

    getDeviceCount(): Promise<any> {
        return this.http.get(this.serviceUrl + "/search?searchcriteria=ALL&page=1&limit=0", { headers: this.headers })
            .toPromise()
            .then(response => { return response.json().count })
            .catch(this.handleError.bind(this));
    }

    getNewDeviceCount(startDate, endDate, searchcriteria, facilityid, centerid): Promise<any> {
        //if (searchcriteria === "FACILITY")
        // searchcriteria = "ORGANIZATION";
        var url = this.serviceUrl + "/count?searchcriteria=" + searchcriteria + "&from=" + startDate + "&to=" + endDate + "&page=1&limit=10";
        if (searchcriteria == "FACILITY")
            url += "&facilityId=" + facilityid;
        if (searchcriteria == "CENTER")
            url += "&centerId=" + centerid + "&facilityId=" + facilityid;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    getDeviceCountDashboard(from, to, search, id): Promise<any> {
        var url = this.serviceUrl + "/count?searchcriteria=ALL&from=" + from + "&to=" + to;
        if (search === "FACILITY")
            url = this.serviceUrl + "/count?searchcriteria=FACILITY&from=" + from + "&to=" + to + "&facilityId=" + id;
        if (search === "CENTER")
            url = this.serviceUrl + "/count?searchcriteria=CENTER&from=" + from + "&to=" + to + "&centerId=" + id;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.handleError.bind(this))
    }

    getSearch(searchby, text, status, page, size): Promise<any> {
        var url = this.serviceUrl + '/search?searchcriteria=' + searchby + '&serialNumber=' + text + '&status=' + status + '&page=' + page + '&limit=' + size
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.handleError.bind(this))
    }

    getSearchByCenter(searchby, text, status, id, page, size) {
        var url;
        if (searchby == 'DEVICE_SERIAL_NUMBER_WITH_STATUS_AND_FACILITY') {
            url = this.serviceUrl + '/search?searchcriteria=' + searchby + '&serialNumber=' + text + '&status=' + status + '&organization=' + id + '&page=' + page + '&limit=' + size
        }
        if (searchby == 'DEVICE_SERIAL_NUMBER_WITH_STATUS_AND_CENTER') {
            url = this.serviceUrl + '/search?searchcriteria=' + searchby + '&serialNumber=' + text + '&status=' + status + '&center=' + id + '&page=' + page + '&limit=' + size
        }
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.handleError.bind(this))
    }


    getDevice(id: string): Promise<Device> {
        const url = '${this.serviceUrl}/code/${id}';
        return this.http.get(this.serviceUrl + "/" + id, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() as Device })
            .catch(this.handleError.bind(this));
    }

    delete(id: number): Promise<void> {
        const url = `${this.serviceUrl}/${id}`;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(() => null)
            .catch(this.handleError.bind(this));
    }

    create(facility: Device): Promise<Device> {
        return this.http
            .post(this.serviceUrl2, JSON.stringify(facility), { headers: this.headers })
            .toPromise()
            .then(res => res.json().data as Device)
            .catch(this.handleError.bind(this));
    }

    bulkCreateHub(file): Promise<Device> {
        var url = this.serviceUrl2 + '/bulk'
        return this.http
            .post(url,file, { headers: this.multipartHeader })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError.bind(this));
    }

    update(facility: Device): Promise<Device> {
        const url = `${this.serviceUrl}/${facility.id}`;
        return this.http
            .put(this.serviceUrl2, JSON.stringify(facility), { headers: this.headers })
            .toPromise()
            .then(() => facility)
            .catch(this.handleError.bind(this));
    }

    deviceAssignsingle(device: Device): Promise<Device> {
        return this.http.put(this.serviceUrl2 + '/' + device.id + '/assign', JSON.stringify(device), { headers: this.headers })
            .toPromise()
            .then(() => device)
            .catch(this.handleError.bind(this));
    }
    deviceAssign(device): Promise<Device> {
        return this.http.put(this.serviceUrl2 + '/assign/bulk', JSON.stringify(device), { headers: this.headers })
            .toPromise()
            .then(() => device)
            .catch(this.handleError.bind(this));
    }



    getDeviceSerial(code): Promise<any> {
        //  const url = this.serviceUrl + "/serial/" + code;
        const url = `${this.serialDeviceUrl}/${code}`;
        return this.http.get(url, { headers: this.headers })
            .toPromise()
            .then(response => { return response.json() })
            .catch(this.handleError.bind(this));
    }

    updateDeviceStatus(device, status): Promise<any> {
        var url
        if (status == 'Activate') {
            url = this.statusUrl + device.id + '/activate'
        }
        if (status == 'Deactivate') {
            url = this.statusUrl + device.id + '/deactivate'
        }
        return this.http
            .put(url, JSON.stringify(device), { headers: this.headers })
            .toPromise()
            .then(() => device)
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            //alert('Session has expired or invalid we will redirect you to login page');
            this._script.load('body',
                'assets/demo/default/custom/components/utils/redirect-cidaas-logout.js');
        }
        return Promise.reject(error.message || error);
    }
}