import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { Ng4FilesStatus, Ng4FilesSelected, Ng4FilesService, Ng4FilesConfig } from "angular4-files-upload"
import { Device } from '../device';
import { Center } from '../../facilities/center';
import { Facility } from '../../facilities/facility';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DevicesService } from '../devices.service';
import { Observable } from 'rxjs/Observable';
import { Utils } from '../../utils/utils'
import { concat } from 'rxjs/observable/concat';
import { combineAll } from 'rxjs/operator/combineAll';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./devices-add1.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})

export class DeviceAdd1Component implements OnInit, AfterViewInit {

    device = new Device();
    deviceId: string;
    facilities: Facility[];
    centers: Center[];
    devices: Device[];
    allDevices: Device[];
    showOrgCenter: boolean;
    selectedFacility: Facility;
    selectedCenter: Center;
    checkAll: boolean;
    sortType: string;
    sort: boolean;
    loading: boolean;
    msg: string;
    successMsg: string;
    successAssignMsg: string;
    assignurl: any;
    assignHub: boolean = false;
    centerSelect: boolean = false;
    noHub: boolean = false;
    successHubAssign: string;
    undevices: any[];
    pageNo1: any;
    pageSize1: any;
    unDevicesCount: any;
    showError: boolean = false;
    idList: any[] = [];
    countNo: any = 0;
    statusDev: any;
    showDialog: boolean = false
    deviceType: any;
    dataToSend: any;
    selectedFacilityid: any = '';
    selectedCenterid: any = '';
    chosen: any;
    public selectedFiles;
    formData: FormData = new FormData();
    fileName : any;
    resultArray : any[] =[];
    skippedArray : any[] = [];
    errorArray : any[] = [];
    showpop : boolean = false;
    hubEntity : any = {}
    skippedCount : any;
    insertedCount : any;
    errorCount : any;

    constructor(private _script: ScriptLoaderService, private _devicesService: DevicesService, private _facilitiesService: FacilitiesService,
         private _router: Router, private _activeroute: ActivatedRoute, private ng4FilesService: Ng4FilesService) {
        this.loading = false;
        this.sortType = "serialNumber";
        this.sort = false;
        this.showOrgCenter = false;
        this.checkAll = false;
        this.selectedFacility = new Facility();
        this.selectedCenter = new Center();
        this.pageNo1 = 1;
        this.pageSize1 = 10;
        this.hubEntity ={
            customerCode : '',
            centerCode : '',
            deviceType : '',
            serialNumber : '',
            result : ''
        }
    }
    ngOnInit() {
        // document.getElementById('device_serialnumber_add').focus();
          
        this.ng4FilesService.addConfig(this.FileConfig);
        this.assignurl = this._activeroute.snapshot['_routerState'].url;
        if (this.assignurl.includes('assign')) {
            this.assignHub = true;
        } else
            this.assignHub = false;
        this.deviceId = this._activeroute.snapshot.queryParams['deviceId'] || '/';
        if (this.deviceId != '/') {
            Helpers.setLoading(true);
            this.device.id = this.deviceId;
            this._devicesService.getDevice(this.device.id)
                .then(device => this.device = device)
                .then(() => {
                    var d = new Date(this.device.activationDate),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                    this.device.activationDate = [month, day, year].join('/');
                    Helpers.setLoading(false);
                });
        }
        this.getFacilities();
        // this.list();
        this.unAssignedlist();
        this.clearMsg()
    }
    ngAfterViewInit() {
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/forms/validation/form-controls.js');
    }

    clearMsg() {
        this.successAssignMsg = null;
        this.successHubAssign = null;
        this.successMsg = null;
    }

    onOrgChange(orgId: string): void {
        this.centerSelect = false;
        this.successAssignMsg = null;
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.device.organizationCode = org.organizationCode;
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                this.device.centerId = this.centers[0].id;
            }
        });
    }

    oncheckAll(): void {
        this.noHub = false;
        this.successAssignMsg = null;
        this.checkAll = !this.checkAll;
        this.undevices.forEach((device, i) => {
            device.selected = this.checkAll;
        })
    }

    charCheck(event) {
        return !Utils.specialCharacter(event.key);
    }

    getDevicebySerial(code) {
        document.getElementById('add_icon').style.display = 'block'
        if (code && code!='') {
            this._devicesService.getDeviceSerial(code)
                .then(device => {
                    document.getElementById('add_icon').style.display = 'none'
                    if (device != null)
                        this.showError = true;
                    else
                        this.showError = false;
                })
        }else{
            document.getElementById('add_icon').style.display = 'none'
        }
    }

    onDeviceCheck(device): void {
        this.noHub = false;
        this.successAssignMsg = null;
        device.selected = !device.selected;
        let allDevicesSelected = true;
        if (device.selected == true) {
            this.idList.push(device);
        }
        if (device.selected == false) {
            var index = this.idList.indexOf(device.id);
            this.idList.splice(index, 1);
        }
        if (this.idList.length != 0) {
            this.countNo = this.idList.length;
        } else
            this.countNo = 0;
        this.undevices.forEach((device, i) => {
            if (!device.selected)
                allDevicesSelected = false;
        });
        if (allDevicesSelected)
            this.checkAll = true;
        else
            this.checkAll = false;
    }

    list(): void {
        Helpers.setLoading(true);
        this.devices = [];
        this.allDevices = [];
        this.undevices = [];
        this._devicesService.getDevices()
            .then(
            devices => this.allDevices = devices)
            .then(() => {
                this.undevices = [];
                this.allDevices.forEach((device, i) => {
                    if (!device.organizationId) {
                        this.undevices.push(device);
                    }
                });
                this.undevices.forEach(function(val, i) {
                    var d = new Date(val.activationDate),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                    val.activationDate = [month, day, year].join('/');
                });
                // localStorage.setItem('deviceTable', JSON.stringify(this.devices));
                // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                //     'assets/demo/default/custom/components/datatables/base/data-local.devices.js');
                Helpers.setLoading(false);
                localStorage.setItem('unAssingedDeviceTable', JSON.stringify(this.undevices));
                if (localStorage.getItem('unAssingedDeviceTable')) {
                    this.undevices = JSON.parse(localStorage.getItem('unAssingedDeviceTable'))
                }
            });
    }


    unAssignedlist(): void {
        Helpers.setLoading(true);
        this.devices = [];
        this.allDevices = [];
        this.undevices = [];
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 1);
        endDate.setHours(23);
        endDate.setMinutes(23);
        endDate.setSeconds(23);
        endDate.setMilliseconds(0);
        this._devicesService.getDevicesDatePaginated(0, new Date(endDate).getTime(), "ALL", 0, this.pageNo1, this.pageSize1, "UNASSIGNED")
            .then(devices => {
                Helpers.setLoading(false);
                if (devices.devices) {
                    this.undevices = devices.devices;
                    this.unDevicesCount = devices.count.toString();
                } else {
                    this.undevices = [];
                    this.unDevicesCount = '0';
                }
            })
            .then(() => {
                if (this.undevices != undefined && this.undevices.length != 0) {
                    this.undevices.forEach((device, i) => {
                        this.idList.forEach(dev => {
                            if (dev.id == device.id) {
                                if (dev.selected == true)
                                    device.selected = true;
                                else
                                    device.selected = false;
                            }
                        })
                    });
                    this.undevices.forEach(function(val, i) {
                        if (val.deviceStatus == 'ACTIVE') {
                            val.status = true
                        }
                        if (val.deviceStatus == 'INACTIVE') {
                            val.status = false
                        }
                        var d = new Date(val.activationDate),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                        if (month.length < 2) month = '0' + month;
                        if (day.length < 2) day = '0' + day;
                        val.activationDate = [month, day, year].join('/');
                    });
                }

                localStorage.setItem('unAssingedDeviceTable', JSON.stringify(this.undevices));
                if (localStorage.getItem('unAssingedDeviceTable')) {
                    this.undevices = JSON.parse(localStorage.getItem('unAssingedDeviceTable'))
                }
            });
    }

    private FileConfig: Ng4FilesConfig = {
        acceptExtensions: ['csv'],
        maxFilesCount: 1,
        maxFileSize: 5120000,
        totalFilesSize: 10120000
    };

    // assignDevice(): void {
    //     this.successHubAssign = null;
    //     this.successAssignMsg = null;
    //     if (this.idList.length == 0 || !this.selectedCenter.id || !this.selectedFacility.id) {
    //         this.successAssignMsg = "SELECT_BOTH_CUSTOMER_AND_HUB_THEN_TRY_TO_ASSIGN";
    //     }
    //     this.idList.forEach((device, i) => {
    //         if (device.selected) {
    //             this.successMsg = null;
    //             this.successHubAssign = null;
    //             this.successAssignMsg = null
    //             this.noHub = false;
    //             device.organizationId = this.selectedFacility.id;
    //             device.organizationCode = this.selectedFacility.organizationCode;
    //             device.centerId = this.selectedCenter.id;
    //             device.assignmentStatus = "ASSIGNED";
    //             //device.selected = undefined;
    //             delete device["selected"]
    //             if (device.activationDate != "NaN") {
    //                 var myDate = new Date(device.activationDate);
    //                 var offset = myDate.getTimezoneOffset() * 60 * 1000;
    //                 var withOffset = myDate.getTime();
    //                 var withoutOffset = withOffset - offset;
    //                 device.activationDate = withOffset.toString();
    //             } else {
    //                 var myDate = new Date();
    //                 var offset = myDate.getTimezoneOffset() * 60 * 1000;
    //                 var withOffset = myDate.getTime();
    //                 var withoutOffset = withOffset - offset;
    //                 device.activationDate = myDate.getTime().toString();
    //             }
    //             if (device.organizationId && device.centerId) {
    //                 if (device.activationDate == "NaN") {
    //                     var myDate = new Date();
    //                     var offset = myDate.getTimezoneOffset() * 60 * 1000;
    //                     var withOffset = myDate.getTime();
    //                     var withoutOffset = withOffset - offset;
    //                     device.activationDate = myDate.getTime().toString();
    //                 }
    //                 this._devicesService.deviceAssign(device)
    //                     .then((data) => // console.log(data))
    //                     .then(() => {
    //                         this.successAssignMsg = null;
    //                         this.successHubAssign = "HUB_ASSIGNED_SUCCESSFULLY";
    //                         this.countNo = 0;
    //                         this.pageNo1 = 1;
    //                         this.idList = [];
    //                         this.unAssignedlist();
    //                         setTimeout(() => {
    //                             this.successHubAssign = null;
    //                         }, 3000);
    //                         // this.list();
    //                     }),
    //                     (e: any) => {
    //                         // console.log(e);
    //                         if (e._body) {
    //                             let res = JSON.parse(e._body);
    //                             // console.log(res)
    //                             if (res.error) {
    //                                 this.successAssignMsg = "Hub assiging Failed";
    //                                 setTimeout(() => {
    //                                     this.successAssignMsg = null;
    //                                 }, 3000);
    //                             }
    //                         }
    //                     }
    //             } else {
    //                 device.selected = true;
    //                 this.successAssignMsg = "SELECT_BOTH_CUSTOMER_AND_HUB_THEN_TRY_TO_ASSIGN";
    //             }

    //         } else {
    //             this.noHub = true;
    //             // this.successAssignMsg = "SELECT_BOTH_CUSTOMER_AND_HUB_THEN_TRY_TO_ASSIGN";
    //         }
    //     });
    // }

    assignDevice(customer, center): void {
        this.successHubAssign = null;
        this.successAssignMsg = null;
        if (this.idList.length == 0 || customer == '' || center == '') {
            this.successAssignMsg = "SELECT_BOTH_CUSTOMER_AND_HUB_THEN_TRY_TO_ASSIGN";
        }
        else {
            Helpers.setLoading(true);
            let deviceIds = []
            this.idList.map((device, i) => {
                if (device.selected) {
                    this.successMsg = null;
                    this.successAssignMsg = null;
                    this.noHub = false;
                    deviceIds.push(device.id);
                }
            })
            let assignObj = {
                deviceIds: deviceIds,
                facilityId: customer,
                centerId: center
            }
            // console.log(assignObj);
            this._devicesService.deviceAssign(assignObj)
                .then((data) => console.log(data))
                .then(() => {
                    this.successAssignMsg = null;
                    this.successHubAssign = "HUB_ASSIGNED_SUCCESSFULLY";
                    this.countNo = 0;
                    this.pageNo1 = 1;
                    this.idList = [];
                    this.unAssignedlist();
                    setTimeout(() => {
                        this.successHubAssign = null;
                    }, 3000);
                    // this.list();
                }),
                (e: any) => {
                    // console.log(e);
                    if (e._body) {
                        let res = JSON.parse(e._body);
                        // console.log(res)
                        if (res.error) {
                            this.successAssignMsg = "Hub assiging Failed";
                            setTimeout(() => {
                                this.successAssignMsg = null;
                            }, 3000);
                        }
                    }
                }
        }
    }

    onDeviceRowclick(selectedDevice): void {
        selectedDevice.selected = !selectedDevice.selected;
    }

    getFacilities(): void {
        this.centerSelect = true;
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            } else if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        // this.selectedFacility.id = this.facilities[0].id;
        // this.centers = this.facilities[0].centers;
        // this.selectedCenter.id = this.centers[0].id;
        // this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
        if (this.device.id === "") {
            this.device.organizationId = this.facilities[0].id;
            this.centers = this.facilities[0].centers;
            this.device.centerId = this.centers[0].id;
        }
        else {
            if (this.device.organizationId && this.device.centerId) {
                this.facilities.forEach((org, i) => {
                    if (org.id === this.device.organizationId) {
                        this.centers = org.centers;
                    }
                });
            }
        }
    }

    clearForm(): void {
        this.device.serialNumber = "";
        this.device.deviceType = "HC_SE";
    }


    save(): void {
        // var myDate = new Date(this.device.activationDate);
        // var offset = myDate.getTimezoneOffset() * 60 * 1000;

        // var withOffset = myDate.getTime();
        // var withoutOffset = withOffset - offset;
        // this.device.activationDate = withOffset.toString();
        this.msg = null;
        if (!this.loading) {
            Helpers.setLoading(true);
            this.loading = true;
            try {
                if (!this.device.id) {
                    var myDate = new Date();
                    var offset = myDate.getTimezoneOffset() * 60 * 1000;

                    var withOffset = myDate.getTime();
                    var withoutOffset = withOffset - offset;
                    this.device.activationDate = withOffset.toString();
                    this.device.assignmentStatus = "UNASSIGNED";
                    this.device.deviceStatus = 'ACTIVE'
                    this._devicesService.create(this.device)
                        .then((data) => console.log(data))
                        .then(() => {
                            this.clearForm();
                            setTimeout(() => {
                                //  this.list();
                                this.unAssignedlist();
                                Helpers.setLoading(false);
                                this.loading = false;
                            }, 1000);
                            this.successMsg = "HUB_SAVED_SUCCESSFULLY";
                            setTimeout(() => {
                                this.successMsg = "";
                            }, 1000);
                        }, (e: any) => {
                            // console.log(e);
                            let res = JSON.parse(e._body);
                            if (res.error) {
                                this.msg = res.error;
                                setTimeout(() => {
                                    this.msg = "";
                                }, 1000);
                            }
                            Helpers.setLoading(false);
                            this.loading = false
                        });
                }
                else {
                    this._devicesService.update(this.device)
                        .then((data) => console.log(data))
                        .then(() => {
                            Helpers.setLoading(false);
                            this.loading = false;
                            this.clearForm();
                            this.list();
                            this.successAssignMsg = null;
                            this.successMsg = "HUB_SAVED_SUCCESSFULLY";
                            setTimeout(() => {
                                this.successMsg = "";
                            }, 1000);
                        }, (e: any) => {
                            // console.log(e);
                            let res = JSON.parse(e._body);
                            if (res.error) {
                                this.msg = res.error;
                                setTimeout(() => {
                                    this.msg = "";
                                }, 1000);
                            }
                            Helpers.setLoading(false);
                            this.loading = false
                        });
                }
            }
            catch (e) {
                Helpers.setLoading(false);
                this.loading = false;
            }
        }
    }

    paginateUnDevices(event) {
        this.pageNo1 = event.page + 1;
        this.pageSize1 = parseInt(event.rows);
        this.unAssignedlist();
    }

    conformStatus() {
        Helpers.setLoading(true)

        if (this.dataToSend.status == false) {
            this.dataToSend.deviceStatus = 'INACTIVE'
            // delete this.device["status"]
        }
        if (this.dataToSend.status == true) {
            this.dataToSend.deviceStatus = 'ACTIVE'
            this.dataToSend.status = true
            //delete this.device["status"]
        }

        this._devicesService.updateDeviceStatus(this.dataToSend, this.statusDev)
            .then(data => {
                // this.clearForm();  
                Helpers.setLoading(false)
                this.showDialog = false;

            })
    }

    close() {
        this.showDialog = false;
        this.dataToSend.status = !this.dataToSend.status
    }

    statusChange(device) {
        this.dataToSend = device
        this.dataToSend.status = !this.dataToSend.status;
        if (this.dataToSend.deviceType.includes('_'))
            this.deviceType = this.device.deviceType.replace('_', ' ')
        if (this.dataToSend.deviceType.includes('_'))
            this.deviceType = this.device.deviceType.replace('_', ' ')
        if (this.dataToSend.status == false) {
            this.statusDev = 'Deactivate'
        }
        if (this.dataToSend.status == true) {
            this.statusDev = 'Activate'
        }
        this.showDialog = true;

    }

    facilityChange(value) {
        this.selectedFacility = value;
        this.centerSelect = false;
        this.successAssignMsg = null;
        this.device.organizationCode = this.selectedFacility.organizationCode;
        this.device.organizationId = this.selectedFacility.id;
        this.selectedFacilityid = this.selectedFacility.id;
        if (this.selectedFacility.centers) {
            this.centers = this.selectedFacility.centers;
            // this.selectedCenterid = this.centers[0].id;
        } else {
            this.centers = null;
            this.device.centerId = null;
        }

    }

    public filesSelect(selectedFiles: Ng4FilesSelected): void {
        var FileUploadPath = selectedFiles.files[0].name
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
        if (Extension === "csv") {
            if (selectedFiles.files[0].name) {
                // console.log(selectedFiles.files)
                this.fileName = selectedFiles.files[0].name
            }
            if (selectedFiles.status === Ng4FilesStatus.STATUS_SUCCESS) {
                this.selectedFiles = Array.from(selectedFiles.files).map(file =>
                this.formData.append('upfile', file));
                
            }
        } else {
            alert('upload valid image')
        }
    }

    closePop(){
        this.resultArray = [];
        this.skippedArray = [];
        this.formData = new FormData;
        this.showpop = false;
    }

    uploadHub(){
        this.resultArray = [];
        this.skippedArray = [];
        this.errorArray = [];
        if(this.formData){
            Helpers.setLoading(true);
            this._devicesService.bulkCreateHub(this.formData)
            .then(data=>{
                Helpers.setLoading(false);
                this.clearFormData();
              //  this.successMsg = "HUB_ASSIGNED_SUCCESSFULLY";
                if(data['overallResult']){  
                    console.log(data['overallResult'])  
                   var a = data['overallResult']
                   console.log(a['inserted'], a['inserted'].length)
                   if(a['inserted'].length !=2){
                    var n = a['inserted']  
                    this.resultArray = JSON.parse(n)                   
                   }else{
                       this.resultArray = [];
                   }                   
                   if(a['skipped'].length != 2){
                    var s = a['skipped'] 
                    this.skippedArray = JSON.parse(s)
                    console.log(this.skippedArray)
                   }else{
                       this.skippedArray = [];
                   }  
                   if(a['_error']) {
                        if(a['_error'].length != 2){
                            var e = a['_error'] 
                            this.errorArray = JSON.parse(e)                        
                    }  
                   }else
                      this.errorArray = [];
                                      
                   this.showpop = true;
                   this.skippedCount = data['skippedCount']
                   this.insertedCount = data['insertedCount'] 
                   if(data['errorCount']) 
                     this.errorCount = data['errorCount']     
                   else
                      this.errorCount = 0       
                   
                }                
                this.countNo = 0;
                this.pageNo1 = 1;
                this.idList = [];
                this.unAssignedlist();
                setTimeout(() => {
                    this.successMsg = null;
                }, 3000);
            }, (e: any) => {
                // console.log(e);
                let res = JSON.parse(e._body);
                if (res.error) {
                    this.msg = 'Please check the uploaded file';    
                    this.clearFormData()               
                    setTimeout(() => {
                        this.msg = "";
                    }, 2000);
                }
                Helpers.setLoading(false);
                this.loading = false
            });
        }       
    }

    clearFormData(){
        this.fileName = null;
        this.formData= new FormData();
    }

    goToDeviceView(deviceId) {
        this._router.navigate(['/devices/details'], { queryParams: { deviceId: deviceId } });
    }

}