//== Class definition
var amChartsChartsDemo = function() {


    var pieChartBGFast = function() {
        var BLOOD_GLUCOSE = JSON.parse(localStorage.getItem('BLOOD_GLUCOSE_FASTING'))
        AmCharts.addInitHandler(function(chart) {
            if (chart.dataProvider[0]['column-1'] === 0 && chart.dataProvider[1]['column-1'] === 0 && chart.dataProvider[2]['column-1'] === 0) {
                // add label to let users know the chart is empty
                chart.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
            }

        }, ["pie"]);
        AmCharts.makeChart("pie_chart_blood_glucose_fast", {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "colorField": "color",
            "titleField": "category",
            "valueField": "column-1",
            "theme": "light",
            "labelsEnabled": false,
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Blood Glucose (Fasting)"
            }],
            "dataProvider": [{
                    "category": "Pre-Diabetic",
                    "column-1": parseInt(BLOOD_GLUCOSE.low)
                },
                {
                    "category": "Normal",
                    "column-1": parseInt(BLOOD_GLUCOSE.normal)
                },
                {
                    "category": "Diabetic",
                    "column-1": parseInt(BLOOD_GLUCOSE.high),
                    "color": "red"
                }
            ]
        });
    }
    var pieChartBGNonFast = function() {
        var BLOOD_GLUCOSE = JSON.parse(localStorage.getItem('BLOOD_GLUCOSE_NON_FASTING'))
        AmCharts.makeChart("pie_chart_blood_glucose_non_fast", {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "colorField": "color",
            "titleField": "category",
            "valueField": "column-1",
            "theme": "light",
            "labelsEnabled": false,
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Blood Glucose (Postprandial)"
            }],
            "dataProvider": [{
                    "category": "Pre-Diabetic",
                    "column-1": parseInt(BLOOD_GLUCOSE.low)
                },
                {
                    "category": "Normal",
                    "column-1": parseInt(BLOOD_GLUCOSE.normal)
                },
                {
                    "category": "Diabetic",
                    "column-1": parseInt(BLOOD_GLUCOSE.high),
                    "color": "red"
                }
            ]
        });
    }

    var pieChartBP = function() {
        var BLOOD_PRESSURE = JSON.parse(localStorage.getItem('BLOOD_PRESSURE'))
        AmCharts.makeChart("pie_chart_blood_pressure", {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "colorField": "color",
            "titleField": "category",
            "valueField": "column-1",
            "theme": "light",
            "labelsEnabled": false,
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Blood Pressure"
            }],
            "dataProvider": [{
                    "category": "Low",
                    "column-1": parseInt(BLOOD_PRESSURE.low)
                },
                {
                    "category": "Normal",
                    "column-1": parseInt(BLOOD_PRESSURE.normal)
                },
                {
                    "category": "High",
                    "column-1": parseInt(BLOOD_PRESSURE.high),
                    "color": "red"
                }
            ]
        });
    }
    var pieChartHemoglobin = function() {
        var HEAMOGLOBIN = JSON.parse(localStorage.getItem('HEAMOGLOBIN'))
        AmCharts.makeChart("pie_chart_hemoglobin", {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "colorField": "color",
            "titleField": "category",
            "valueField": "column-1",
            "theme": "light",
            "labelsEnabled": false,
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Hemoglobin"
            }],
            "dataProvider": [{
                    "category": "Low",
                    "column-1": parseInt(HEAMOGLOBIN.low)
                },
                {
                    "category": "Normal",
                    "column-1": parseInt(HEAMOGLOBIN.normal)
                },
                {
                    "category": "High",
                    "column-1": parseInt(HEAMOGLOBIN.high),
                    "color": "red"
                }
            ]
        });
    }
    var pieChartCholesterol = function() {
        var CHOLESTEROL = JSON.parse(localStorage.getItem('CHOLESTEROL'))
        AmCharts.makeChart("pie_chart_cholesterol", {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "colorField": "color",
            "titleField": "category",
            "valueField": "column-1",
            "theme": "light",
            "labelsEnabled": false,
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Cholesterol"
            }],
            "dataProvider": [{
                    "category": "Low",
                    "column-1": parseInt(CHOLESTEROL.low)
                },
                {
                    "category": "Normal",
                    "column-1": parseInt(CHOLESTEROL.normal)
                },
                {
                    "category": "High",
                    "column-1": parseInt(CHOLESTEROL.high),
                    "color": "red"
                }
            ]
        });
    }
    var pieChartTemperature = function() {
        var temperature = JSON.parse(localStorage.getItem('TEMPERATURE'))
        AmCharts.makeChart("pie_chart_temperature", {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "colorField": "color",
            "titleField": "category",
            "valueField": "column-1",
            "theme": "light",
            "labelsEnabled": false,
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Temperature"
            }],
            "dataProvider": [{
                    "category": "Low",
                    "column-1": parseInt(temperature.low)
                },
                {
                    "category": "Normal",
                    "column-1": parseInt(temperature.normal)
                },
                {
                    "category": "High",
                    "column-1": parseInt(temperature.high),
                    "color": "red"
                }
            ]
        });
    }

    var pieChartMalaria = function() {
        var malaria = JSON.parse(localStorage.getItem('MALARIA'))
        AmCharts.makeChart("pie_chart_malaria", {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "colorField": "color",
            "titleField": "category",
            "valueField": "column-1",
            "theme": "light",
            "labelsEnabled": false,
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Malaria"
            }],
            "dataProvider": [{
                    "category": "Positive",
                    "column-1": parseInt(malaria.positive),
                    "color": "red"
                },
                {
                    "category": "Negative",
                    "column-1": parseInt(malaria.negative),
                    "color": "green"
                },
                {
                    "category": "Invalid",
                    "column-1": parseInt(malaria.invalid),
                    "color": "yellow"
                }
            ]
        });
    }

    var pieChartHepB = function() {
        var hepB = JSON.parse(localStorage.getItem('HEP_B'))
        AmCharts.makeChart("pie_chart_hepb", {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "colorField": "color",
            "titleField": "category",
            "valueField": "column-1",
            "theme": "light",
            "labelsEnabled": false,
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Hepatitis B"
            }],
            "dataProvider": [{
                    "category": "Positive",
                    "column-1": parseInt(hepB.positive),
                    "color": "red"
                },
                {
                    "category": "Negative",
                    "column-1": parseInt(hepB.negative),
                    "color": "green"
                },
                {
                    "category": "Invalid",
                    "column-1": parseInt(hepB.invalid),
                    "color": "yellow"
                }
            ]
        });
    }

    var pieChartTypoid = function() {
        var typhoid = JSON.parse(localStorage.getItem('TYPHOID'))
        AmCharts.makeChart("pie_chart_typoid", {
            "type": "pie",
            "angle": 12,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "depth3D": 15,
            "colorField": "color",
            "titleField": "category",
            "valueField": "column-1",
            "theme": "light",
            "labelsEnabled": false,
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Typhoid"
            }],
            "dataProvider": [{
                    "category": "Positive",
                    "column-1": parseInt(typhoid.positive),
                    "color": "red"
                },
                {
                    "category": "Negative",
                    "column-1": parseInt(typhoid.negative),
                    "color": "green"
                },
                {
                    "category": "Invalid",
                    "column-1": parseInt(typhoid.invalid),
                    "color": "yellow"
                }
            ]
        });
    }


    var daterangepickerInit = function() {
        if ($('#m_dashboard_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#m_dashboard_daterangepicker');
        var start = moment();
        var end = moment();

        function cb(start, end, label) {
            var title = '';
            var range = '';

            if ((end - start) < 100) {
                title = 'Today:';
                range = start.format('MMM D');
            } else if (label == 'Yesterday' || label == 'Today') {
                title = label + ':';
                range = start.format('MMM D');
            } else {
                range = start.format('MMM D') + ' - ' + end.format('MMM D YYYY');
            }

            window.localStorage.setItem('dashStartDate', start);
            window.localStorage.setItem('dashEndDate', end);
            picker.find('.m-subheader__daterange-date').html(range);
            picker.find('.m-subheader__daterange-title').html(title);
        }

        picker.daterangepicker({
            startDate: start,
            endDate: end,
            opens: 'left',
            autoApply: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This year': [moment().startOf('year'), moment().endOf('year')]
            }
        }, cb);
        $(".range_inputs").hide();
        cb(start, end, '');
    }


    return {
        // public functions
        init: function() {
            pieChartBGFast();
            pieChartBGNonFast();
            pieChartBP();
            pieChartHemoglobin();
            pieChartCholesterol();
            pieChartTemperature();
            pieChartMalaria();
            pieChartHepB();
            pieChartTypoid();
            daterangepickerInit();
        }
    };
}();

jQuery(document).ready(function() {
    amChartsChartsDemo.init();
    $("a").removeAttr("href");
    $(".amcharts-chart-div a").hide();
});