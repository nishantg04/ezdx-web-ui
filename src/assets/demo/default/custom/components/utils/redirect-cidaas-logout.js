// console.log("logout started");
doLogout(JSON.parse(localStorage.getItem('currentUser')).token, function() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('facilityName');
    localStorage.removeItem('userInfo');
    // console.log("loout done");
    doLogin();
});