import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { OrderModule } from 'ngx-order-pipe';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DataTableModule, SharedModule, PaginatorModule, DialogModule, DropdownModule, MultiSelectModule, CheckboxModule } from 'primeng/primeng';
import { RdtService } from '../rdt.service';
import { DeviceService } from '../device.service';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { RdtDetailsComponent } from './rdt-details.component';
import { CommonService } from '../../../../../../_services/common.service';
import { UsersService } from '../../users/users.service';
import { UploadFileService } from '../../upload/upload-file.service';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": RdtDetailsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DataTableModule, SharedModule, PaginatorModule,
        RouterModule.forChild(routes),
        LayoutModule,
        TranslateModule.forChild({ isolate: false }),
        MultiSelectModule,
        DropdownModule,
        OrderModule,
        DialogModule,
        CheckboxModule
    ],
    exports: [RouterModule],
    declarations: [RdtDetailsComponent],
    providers: [RdtService, FacilitiesService, DeviceService, TranslateService, CommonService, UsersService, UploadFileService]
})
export class RdtDetailsModule { }