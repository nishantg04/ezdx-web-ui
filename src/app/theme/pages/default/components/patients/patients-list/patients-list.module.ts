import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { PatientsListComponent } from './patients-list.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { PatientsService } from '../patients.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { UsersService } from '../../users/users.service';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { DataTableModule, SharedModule, CheckboxModule, PaginatorModule, CalendarModule, DialogModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { DropdownModule } from 'primeng/primeng';
import { FacDropdownModule } from '../../../../../fac-dropdown/fac-dropdown.module'


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": PatientsListComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, DataTableModule, SharedModule, CheckboxModule, PaginatorModule, CalendarModule, DialogModule,
        PdfViewerModule, DropdownModule, FacDropdownModule,
        TranslateModule.forChild({ isolate: false }),

    ], exports: [
        RouterModule
    ], declarations: [
        PatientsListComponent
    ],
    providers: [
        PatientsService,
        FacilitiesService,
        UsersService,
        TranslateService
    ]

})
export class PatientsListModule {



}