import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { RdtHistoryComponent } from '../rdt-history/rdt-history.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { UploadFileService } from '../../upload/upload-file.service';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": RdtHistoryComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule,
        TranslateModule.forChild({ isolate: false }),
    ], exports: [
        RouterModule
    ], declarations: [
        RdtHistoryComponent
    ],
    providers: [TranslateService, UploadFileService]
})
export class RdtHistoryModule { }
