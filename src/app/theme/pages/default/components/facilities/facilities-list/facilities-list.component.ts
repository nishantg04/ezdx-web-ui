import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';

import { Facility } from '../facility';
import { FacilitiesService } from '../facilities.service';
import * as _ from 'lodash';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./facilities-list.component.html",
    styleUrls: ["../../../primeng.component.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class FacilitiesListComponent implements OnInit, AfterViewInit {

    facilities: Facility[];
    visiblepagination: boolean = false;
    row: number;
    facilityCount: string;
    pageNo: number;
    pageSize: number;
    isSearch: boolean;
    searchValue: string;
    canAddFacility: boolean = false;
    ranges: any[];
    constructor(private _ngZone: NgZone, private _facilitiesService: FacilitiesService, private _script: ScriptLoaderService, private _router: Router) {

    }
    ngOnInit() {
        let showAddTo = ['HC_ADMIN', 'HC_OPERATION'];
        let userRoles = JSON.parse(window.localStorage.getItem('userInfo')).userRoles;
        this.ranges = [
            { label: "10", value: 10 },
            { label: "25", value: 25 },
            { label: "50", value: 50 },
            { label: "100", value: 100 },
        ];
        this.pageNo = 1;
        this.pageSize = 10;
        this.row = 10;
        _.forEach(showAddTo, (role) => {
            if (_.indexOf(userRoles, role) > -1) {
                this.canAddFacility = true;
            }
        });
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
        window.my.namespace.facilityUpdatePublicFunc = this.facilityUpdatePublicFunc.bind(this);
        window.my.namespace.facilityViewPublicFunc = this.facilityViewPublicFunc.bind(this);
    }
    ngOnDestroy() {
        window.my.namespace.publicFunc = null;

    }
    select(e) {
        this.visiblepagination = false;
        // console.log(e);
        this.pageSize = e.value;
        this.pageNo = 1;
        this.row = e.value;
        if (this.isSearch) {
            this.filteredCustomer(this.searchValue)
        }
        else {
            this.list();
        }
    }

    facilityUpdatePublicFunc(facilityId) {
        this._ngZone.run(() => this.goToFacilityEdit(facilityId));
    }

    facilityViewPublicFunc(facilityId) {
        this._ngZone.run(() => this.goToFacilityView(facilityId));
    }

    goToFacilityEdit(facilityId) {
        this._router.navigate(['/facilities/update'], { queryParams: { facilityId: facilityId } });
    }
    goToFacilityView(facilityId) {
        this._router.navigate(['/facilities/details'], { queryParams: { facilityId: facilityId } });
    }
    ngAfterViewInit() {
        this.list();
    }

    list(): void {
        Helpers.setLoading(true);
        this._facilitiesService.getFacilities(this.pageNo, this.pageSize)
            .then(facilities => {
                this.facilities = facilities.organizations;
                this.facilityCount = facilities.count.toString();
                this.visiblepagination = true;
                // localStorage.setItem('facilityTable', JSON.stringify(this.facilities));
            })
            .then(() => {
                this.facilities.forEach(facility => {
                    if (facility.type === "INDIVIDUAL_HOME") {
                        facility.type = "INDIVIDUAL/HOME";
                    }
                    else if (facility.type === "LABORATORY_SOCIALENTREPRENEUR") {
                        facility.type = "LABORATORY/SOCIAL_ENTREPRENEUR";
                    }
                    else if (facility.type === "HOSPITAL_PHC") {
                        facility.type = "Hospital/PHC";
                    }
                    else if (facility.type === "ORGANIZATION") {
                        facility.type = "Organization";
                    }
                    else {
                        facility.type = "HEALTHWORKER";
                    }
                });
                Helpers.setLoading(false);

            });
    }


    searchCustomer(event) {
        this.searchValue = event.target.value;
        if (this.searchValue == '') {
            this.list();
        }
        if (this.searchValue.length >= 3) {
            this.pageNo = 1;
            this.isSearch = true;
            this.filteredCustomer(this.searchValue)
        }
    }

    filteredCustomer(value) {
        Helpers.setLoading(true);
        this._facilitiesService.searchFacilities('NAME', value, this.pageNo, this.pageSize, 'name', 'asc')
            .then(response => {
                if (response !== null) {
                    this.facilities = response.organizations;
                    this.facilityCount = response.count;
                    this.visiblepagination = true;
                }
                else {
                    this.facilities = [];
                    this.facilityCount = '0';
                }
            })
            .then(() => {
                this.facilities.forEach(facility => {
                    if (facility.type === "INDIVIDUAL_HOME") {
                        facility.type = "Individual/Home";
                    }
                    else if (facility.type === "LABORATORY_SOCIALENTREPRENEUR") {
                        facility.type = "Laboratory/Social Entrepreneur";
                    }
                    else if (facility.type === "HOSPITAL_PHC") {
                        facility.type = "Hospital/PHC";
                    }
                    else if (facility.type === "ORGANIZATION") {
                        facility.type = "Organization";
                    }
                    else {
                        facility.type = "Healthworker";
                    }
                });
                Helpers.setLoading(false);

            });
    }


    paginate(event) {
        this.pageNo = event.page + 1;
        this.pageSize = parseInt(event.rows);
        if (this.isSearch) {
            this.filteredCustomer(this.searchValue)
        }
        else {
            this.list();
        }
    }

    pageOne(type){  
        if(type == 'customer'){
           if(this.searchValue.length >=3){
            this.pageNo=1;
            this.visiblepagination = false;       
            setTimeout(() => this.visiblepagination = true, 0);  
           }
        } else{
            this.pageNo=1;
            this.visiblepagination = false;       
            setTimeout(() => this.visiblepagination = true, 0);
        }        
                    
      
    }

}