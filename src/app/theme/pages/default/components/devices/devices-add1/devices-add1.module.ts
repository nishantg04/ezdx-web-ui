import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { OrderModule } from 'ngx-order-pipe';
import { DeviceAdd1Component } from './devices-add1.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { DevicesService } from '../devices.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DataTableModule, SharedModule, PaginatorModule, DialogModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { FacDropdownModule } from '../../../../../fac-dropdown/fac-dropdown.module'
import { Ng4FilesModule } from "angular4-files-upload"


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": DeviceAdd1Component
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule, FacDropdownModule,
        DataTableModule, SharedModule, DialogModule, PaginatorModule,Ng4FilesModule,
        TranslateModule.forChild({ isolate: false }),

    ], exports: [
        RouterModule
    ], declarations: [
        DeviceAdd1Component
    ],
    providers: [
        DevicesService,
        FacilitiesService,
        TranslateService
    ]

})
export class DevicesAdd1Module {



}