import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';

import { Device } from '../device';
import { Center } from '../../facilities/center';
import { Facility } from '../../facilities/facility';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DevicesService } from '../devices.service';
import { Observable } from 'rxjs/Observable';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./devices-add.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class DeviceAddComponent implements OnInit, AfterViewInit {

    currentUserType: string;
    userInfo: any;
    device = new Device();
    deviceId: string;
    facilities: Facility[];
    centers: Center[];
    loading: boolean;
    msg: string;
    center: any;
    deviceDetail: any;
    disableCustomer: boolean = false;
    selectedFacilityId: any;
    hubActive: boolean = false;
    selectedFacility: Facility;
    selectedCenter: Center;
    disable: boolean = false;
    constructor(private _script: ScriptLoaderService, private _devicesService: DevicesService, private _facilitiesService: FacilitiesService, private _router: Router, private _activeroute: ActivatedRoute) {

        if (window.localStorage.getItem('userType') == 'Facility') {
            this.currentUserType = 'Facility';
        }
        else {
            this.currentUserType = 'Admin';
        }
        this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
    }
    ngOnInit() {
        this.loading = false;
        this.deviceId = this._activeroute.snapshot.queryParams['deviceId'] || '/';
        if (this._activeroute.snapshot.queryParams['from'] == 'unassign') {
            this.disableCustomer = false;
        }
        if (this.deviceId != '/') {
            Helpers.setLoading(true);
            this.device.id = this.deviceId;
            this._devicesService.getDevice(this.device.id)
                .then(device => {
                    this.device = device;
                })
                .then(() => {
                    if (this.device.deviceStatus == 'ACTIVE' && this.device.organizationId != undefined)
                        this.disableCustomer = true;
                    else
                        this.disableCustomer = false;
                    if (this.device.assignmentStatus == 'UNASSIGNED') {
                        if (this.device.organizationId != null || this.device.organizationId == undefined)
                            this.device.organizationId = null
                        if (this.device.centerId != null || this.device.centerId == undefined)
                            this.device.centerId = null;
                    }
                    var d = new Date(this.device.activationDate),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (this.device.centerId) {
                        this.facilities.forEach((org, i) => {
                            if (org.id === this.device.organizationId) {
                                this.device.organizationCode = org.organizationCode;
                                if (org.centers) {
                                    this.centers = org.centers;
                                    this.device.centerId = this.device.centerId;
                                } else {
                                    this.centers = null;
                                    this.device.centerId = null;
                                }
                            }
                        });
                    }

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                    this.device.activationDate = [month, day, year].join('/');
                    window.localStorage.setItem('deviceActivationDate', this.device.activationDate);
                    window.localStorage.setItem('deviceDetail', JSON.stringify(this.device));
                    this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                        'assets/demo/default/custom/components/forms/widgets/device-datepicker.js');
                    Helpers.setLoading(false);
                });
        }
        this.getFacilities();
    }
    ngAfterViewInit() {
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
            'assets/demo/default/custom/components/forms/validation/form-controls.js');
    }

    dateCheck(orgId) {
        this.deviceDetail = JSON.parse(window.localStorage.getItem('deviceDetail'))
        if (this.deviceDetail.organizationId !== orgId) {
            var d = new Date(),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            this.device.activationDate = [month, day, year].join('/');
            window.localStorage.setItem('deviceActivationDate', this.device.activationDate);
            this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                'assets/demo/default/custom/components/forms/widgets/device-datepicker.js');
        } else {
            this.device.activationDate = this.deviceDetail.activationDate;
            window.localStorage.setItem('deviceActivationDate', this.device.activationDate);
            this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
                'assets/demo/default/custom/components/forms/widgets/device-datepicker.js');
        }
    }

    onOrgChange(orgId: string): void {
        this.dateCheck(orgId);
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.device.organizationCode = org.organizationCode;
                this.selectedFacilityId = org.id;
                this.device.organizationId = this.selectedFacilityId;
                if (org.centers) {
                    this.centers = org.centers;
                    this.device.centerId = this.centers[0].id;
                } else {
                    this.centers = null;
                    this.device.centerId = null;
                }
            }
        });
    }

    getFacilities(): void {
        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            } else if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        if (this.device.id === "") {
            this.device.organizationId = this.facilities[0].id;
            this.centers = this.facilities[0].centers;
            this.device.centerId = this.centers[0].id;
        }
        else {
            if (this.device.organizationId && this.device.centerId) {
                this.facilities.forEach((org, i) => {
                    if (org.id === this.device.organizationId) {
                        this.centers = org.centers;
                    }
                });
            }
        }
    }

    selectDate(event) {
        window.localStorage.setItem('deviceActivationDate', event.target.value);
    }

    save(): void {
        this.msg = null;
        if (!this.loading) {
            Helpers.setLoading(true);
            this.loading = true;
            try {
                var myDate = new Date(window.localStorage.getItem('deviceActivationDate'));
                var offset = myDate.getTimezoneOffset() * 60 * 1000;

                var withOffset = myDate.getTime();
                var withoutOffset = withOffset - offset;
                this.device.activationDate = withOffset.toString();
                if (this.device.id === '') {
                    this._devicesService.create(this.device)
                        .then((data) => console.log(data))
                        .then(() => {
                            Helpers.setLoading(false);
                            this.loading = false;

                        }, (e: any) => {
                            console.log(e);
                            let res = JSON.parse(e._body);
                            if (res.error) {
                                this.msg = res.error;
                            }
                            Helpers.setLoading(false);
                            this.loading = false
                        });
                }
                else {
                    if (this.device.organizationId) {
                        this._facilitiesService.getCenter(this.device.organizationId)
                            .then(facilities => {
                                this.center = facilities;
                                if (this.center.centers === undefined) {
                                    this.device.centerId = null;
                                }
                                if (this.device.organizationCode) {
                                    this.device.assignmentStatus = "ASSIGNED"
                                }
                                this._devicesService.deviceAssignsingle(this.device)
                                    .then((data) => console.log(data))
                                    .then(() => {
                                        Helpers.setLoading(false);
                                        this.loading = false;
                                        if (this.selectedFacilityId != undefined) {
                                            if (this.device.deviceStatus == 'INACTIVE') {
                                                this.device.deviceStatus = 'ACTIVE';
                                                this.activateHub()
                                            } else {
                                                this._router.navigate(['/devices/list']);
                                            }
                                        } else {
                                            this._router.navigate(['/devices/list']);
                                        }
                                    }, (e: any) => {
                                        let res = JSON.parse(e._body);
                                        if (res.error) {
                                            this.msg = res.error;
                                        }
                                        Helpers.setLoading(false);
                                        this.loading = false
                                    });
                            })
                    } else {
                        this._devicesService.deviceAssign(this.device)
                            .then((data) => console.log(data))
                            .then(() => {
                                Helpers.setLoading(false);
                                this.loading = false;
                                this._router.navigate(['/devices/list']);
                            }, (e: any) => {
                                console.log(e);
                                let res = JSON.parse(e._body);
                                if (res.error) {
                                    this.msg = res.error;
                                }
                                Helpers.setLoading(false);
                                this.loading = false
                            });
                    }

                }
            }
            catch (e) {
                Helpers.setLoading(false);
                this.loading = false;
            }
        }
    }


    activateHub() {
        this.hubActive = false;
        this._devicesService.updateDeviceStatus(this.device, 'Activate')
            .then(data => {
                this.hubActive = true;
                setTimeout(() => {
                    this.hubActive = false;
                }, 500)
                Helpers.setLoading(false)
                setTimeout(() => {
                    this._router.navigate(['/devices/list']);
                }, 600)

            })
    }

    facilityChange(value) {
        this.selectedFacility = value;
        this.device.organizationCode = this.selectedFacility.organizationCode;
        this.selectedFacilityId = this.selectedFacility.id;
        this.device.organizationId = this.selectedFacilityId;
        if (this.selectedFacility.centers) {
            this.centers = this.selectedFacility.centers;
            this.device.centerId = this.centers[0].id;
        } else {
            this.centers = null;
            this.device.centerId = null;
        }

    }

}