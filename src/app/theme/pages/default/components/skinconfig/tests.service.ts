import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Helpers } from "../../../../../helpers";
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";
import { Test } from "../visits/test";

import "rxjs/add/operator/toPromise";
import { environment } from '../../../../../../environments/environment'
@Injectable()
export class ConfigService {
    private headers = new Headers({
        "Content-Type": "application/json",
        "Authorization":
            "Bearer " + JSON.parse(localStorage.getItem("currentUser")).token
    });
    private serviceUrl = environment.BaseURL + environment.TestMasterURL + "/search"; //"http://dev.ezdx.healthcubed.com/ezdx-test-master-srv/api/v1/testmaster/search";
    private serviceConfigUrl = environment.BaseURL + environment.config; //"http://dev.ezdx.healthcubed.com/ezdx-test-master-srv/api/v1/testmaster/search";

    constructor(private _http: Http, private _script: ScriptLoaderService) { }

    getAllTests(): Promise<any[]> {
        return this._http
            .get(this.serviceUrl + "?searchCriteria=ALL", { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    createconfigfile(config): Promise<any> {
        //Helpers.setLoading(true);
        var postUrl = this.serviceConfigUrl + "/configs";
        return this._http
            .post(postUrl, config, { headers: this.headers })
            .toPromise()
            .then(respose => {
                return respose;
            })
            .catch(this.handleError.bind(this));
    }

    updateconfigfile(config): Promise<any> {
        //Helpers.setLoading(true);
        var postUrl = this.serviceConfigUrl + "/configs";
        return this._http
            .put(postUrl, config, { headers: this.headers })
            .toPromise()
            .then(respose => {
                return respose;
            })
            .catch(this.handleError.bind(this));
    }

    Assignconfigfile(config): Promise<any> {       
        var postUrl = this.serviceConfigUrl + "/configs/assign";
        return this._http
            .put(postUrl, config, { headers: this.headers })
            .toPromise()
            .then(respose => {
                return respose;
            })
            .catch(this.handleError.bind(this));
    }

    getconfigfile(criteria, page, limit): Promise<any> {
        return this._http
            .get(this.serviceConfigUrl + "/configs/search?searchCriteria=" + criteria + "&page=" + page + "&limit=" + limit, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getconfigfileForUser(criteria): Promise<any> {
        return this._http
            .get(this.serviceConfigUrl + "/configs/search?searchCriteria=" + criteria, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }



    getconfigfiledetail(id): Promise<any> {
        return this._http
            .get(this.serviceConfigUrl + "/configs/" + id, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }

    getdefaultconfig(): Promise<any> {
        return this._http
            .get(this.serviceConfigUrl + "/configs/default", { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(this.handleError.bind(this));
    }


    private handleError(error: any): Promise<any> {
        Helpers.setLoading(false);
        if (error.status === 401 || error.status === 403) {
            alert(
                "Session has expired or invalid we will redirect you to login page"
            );
            this._script.load(
                "body",
                "assets/demo/default/custom/components/utils/redirect-cidaas-logout.js"
            );
        }
        return Promise.reject(error.message || error);
    }
}
