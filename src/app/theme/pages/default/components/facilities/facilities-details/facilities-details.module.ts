import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FacilitiesDetailsComponent } from './facilities-details.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { FacilitiesService } from '../facilities.service';
import { UsersService } from '../../users/users.service';
import { VisitsService } from '../../visits/visits.service';
import { PatientsService } from '../../patients/patients.service';
import { DevicesService } from '../../devices/devices.service';
import { DataTableModule, SharedModule, PaginatorModule, GMapModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { MapLoaderService } from '../../utils/map-loader.service';


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": FacilitiesDetailsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule,
        DataTableModule, SharedModule, PaginatorModule, GMapModule,
        TranslateModule.forChild({ isolate: false }),

    ], exports: [
        RouterModule
    ], declarations: [
        FacilitiesDetailsComponent
    ],
    providers: [
        FacilitiesService,
        UsersService,
        VisitsService,
        PatientsService, MapLoaderService,
        DevicesService, TranslateService
    ]

})
export class FacilitiesDetailsModule {



}