
export class Role {
    name: string;
    type: string;
    selected: boolean;
}