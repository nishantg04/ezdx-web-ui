//== Class definition

var DatatableDataLocalDemo = function () {
	//== Private functions

	var daterangepickerInit = function () {
		if ($('#m_analytics_visits_daterangepicker').length == 0) {
			return;
		}

		var picker = $('#m_analytics_visits_daterangepicker');
		var DTLabel = '';
		if (window.localStorage.getItem('analyticsStartDate') && window.localStorage.getItem('analyticsEndDate')) {
			var start = moment(new Date(window.localStorage.getItem('analyticsStartDate')));
			var end = moment(new Date(window.localStorage.getItem('analyticsEndDate')));
			var today = moment();
			// console.log(start.date());
			// console.log(end.date());
			// console.log(today.date());
			// console.log((start.date() == today.date()) && (end.date() == today.date()));
			if (((start.date() == today.date()) && (end.date() == today.date())) && ((start.month() == today.month()) && (end.month() == today.month())) && ((start.year() == today.year()) && (end.year() == today.year())))
				DTLabel = "Today";
			if (((start.date() == today.date() - 1) && (end.date() == today.date() - 1)) && ((start.month() == today.month()) && (end.month() == today.month())) && ((start.year() == today.year()) && (end.year() == today.year())))
				DTLabel = "Yesterday";
		}
		else {
			var start = moment().subtract(29, 'days');
			var end = moment();
		}

		function cb(start, end, label) {
			var title = '';
			var range = '';

			if ((end - start) < 100) {
				title = 'Today:';
				range = start.format('MMM D');
			} else if (label == 'Yesterday' || label == 'Today') {
				title = label + ':';
				range = start.format('MMM D');
			} else {
				range = start.format('MMM D') + ' - ' + end.format('MMM D YYYY');
			}

			window.localStorage.setItem('analyticsStartDate', start);
			window.localStorage.setItem('analyticsEndDate', end);
			picker.find('.m-subheader__daterange-date').html(range);
			picker.find('.m-subheader__daterange-title').html(title);
		}

		picker.daterangepicker({
			startDate: start,
			endDate: end,
			opens: 'left',
			autoApply: true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
				'This year': [moment().startOf('year'), moment().endOf('year')]
			}
		}, cb);
		$(".range_inputs").hide();
		cb(start, end, DTLabel);
	}

	return {
		//== Public functions
		init: function () {
			// init dmeo
			daterangepickerInit();
		}
	};
}();

jQuery(document).ready(function () {
	DatatableDataLocalDemo.init();
});