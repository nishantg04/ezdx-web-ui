import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { UsersListComponent } from './users-list.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { UsersService } from '../users.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DataTableModule, SharedModule, PaginatorModule, DialogModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { StateChangeService } from '../state-change.service';
import { DropdownModule } from 'primeng/primeng';
import { FacDropdownModule } from '../../../../../fac-dropdown/fac-dropdown.module'


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": UsersListComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, FacDropdownModule, DataTableModule, SharedModule, PaginatorModule,
        TranslateModule.forChild({ isolate: false }), DialogModule, DropdownModule
    ], exports: [
        RouterModule
    ], declarations: [
        UsersListComponent
    ],
    providers: [
        UsersService,
        FacilitiesService,
        TranslateService,
        StateChangeService
    ]

})
export class UsersListModule {
}