import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { UserService } from "../_services/user.service";
import { Observable } from "rxjs/Rx";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private _router: Router, private _userService: UserService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        return this._userService.verify().map(
            data => {
                if (data !== null) {
                    // logged in so return true
                    return true;
                }
                function getUrlVars() {
                    var vars = [], hash;
                    var hashes = window.location.href.slice(window.location.href.indexOf('#') + 1).split('&');
                    for (var i = 0; i < hashes.length; i++) {
                        hash = hashes[i].split('=');
                        vars.push(hash[0]);
                        vars[hash[0]] = hash[1];
                    }
                    return vars;
                }
                if (state.url.indexOf('/index') !== -1) {
                    if ((getUrlVars()['access_token'])) {
                        localStorage.setItem('currentUser', JSON.stringify({ "fullName": "Nishant", "email": "nishant@healthcubed.com", "token": getUrlVars()['access_token'] }));
                        localStorage.setItem('userChanged', "true");
                        this._router.navigate(['/index']);
                    }
                    else {
                        if (localStorage.getItem('currentUser')) {
                            return true;
                        }
                        else {
                            // error when verify so redirect to login page with the return url
                            this._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
                            return false;
                        }
                    }
                    return true;
                }
                else {
                    if (localStorage.getItem('currentUser')) {
                        return true;
                    }
                    else if (state.url.indexOf('/privacy') !== -1) {
                        return true;
                    }
                    else {
                        // error when verify so redirect to login page with the return url
                        this._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
                        return false;
                    }
                }
            },
            error => {
                // error when verify so redirect to login page with the return url
                this._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
                return false;
            });
    }
}