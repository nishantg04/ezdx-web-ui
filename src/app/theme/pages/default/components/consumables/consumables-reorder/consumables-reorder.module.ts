import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ConsumablesReorderComponent } from './consumables-reorder.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { ConsumablesService } from '../consumables.service';
import { OrderModule } from 'ngx-order-pipe';
import { FacilitiesService } from "../../facilities/facilities.service";
import { DataTableModule, SharedModule, MessagesModule, MessageModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ConsumablesReorderComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule, DataTableModule, SharedModule, MessagesModule, MessageModule,
        TranslateModule.forChild({ isolate: false }),

    ], exports: [
        RouterModule
    ], declarations: [
        ConsumablesReorderComponent
    ],
    providers: [
        ConsumablesService,
        FacilitiesService,
        TranslateService
    ]

})
export class ConsumablesReorderModule {



}