import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CenterDetailsComponent } from './center-details.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { UsersService } from '../../users/users.service';
import { PatientsService } from '../../patients/patients.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { DevicesService } from '../../devices/devices.service';
import { VisitsService } from '../../visits/visits.service';
import { TestsReportService } from '../../home/tests-report/tests-report.service';
import { DialogModule } from 'primeng/primeng';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

import { AmchartModule } from '../../../../../amchartcomponent/amchart.module';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": CenterDetailsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, DialogModule, PdfViewerModule,
        TranslateModule.forChild({ isolate: false }), AmchartModule
    ], exports: [
        RouterModule
    ], declarations: [
        CenterDetailsComponent
    ],
    providers: [
        UsersService,
        PatientsService,
        FacilitiesService,
        DevicesService,
        VisitsService,
        TestsReportService,
        TranslateService
    ]

})
export class CenterDetailsModule { }
