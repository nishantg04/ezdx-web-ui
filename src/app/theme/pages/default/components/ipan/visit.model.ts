import { Test } from "./test.modal";

export interface Visit {
    androidVersion?: String;
    centerId?: String;
    centerName?: String;
    createTime?: Number;
    ezdxAppVersion?: String;
    id?: String;
    organizationId?: String;
    patientId?: String;
    status?: String;
    syncTime?: Number;
    tabletInfo?: String;
    tests?: Test[],
    updateTime?: Number;
    userId?: String;
    rejected?: boolean;
}