//== Class definition
var amChartsChartsDemo = function() {

    AmCharts.addInitHandler(function(testChart) {
        // console.log('data present', testChart.dataProvider)
        if (testChart.dataProvider.length === 0 || testChart.dataProvider === undefined) {
            // add label to let users know the chart is empty
            testChart.addLabel("50%", "50%", "No record available", "middle", 15, "#f4516c");
        }

    }, ["serial"]);

    //== Private functions
    var testIndividualChart = function() {
            var testsForMonth = JSON.parse(window.localStorage.getItem("testsForMonth"));
            var selectedYear = window.localStorage.getItem('SelectedYearDiagnosis');
            var selectedMonth = window.localStorage.getItem('SelectedMonthDiagnosis');
            var testChart = AmCharts.makeChart("test_individual_chart", {
                "type": "serial",
                "categoryField": "Type",
                "maxSelectedTime": -2,
                "startDuration": 1,
                "startEffect": "easeOutSine",
                "processCount": 998,
                "processTimeout": -2,
                "rotate": true,
                "theme": "light",
                "categoryAxis": {
                    "gridPosition": "start"
                },
                "trendLines": [],
                "graphs": [{
                    "balloonText": "[[category]]: [[value]] diagnostics",
                    "fillAlphas": 1,
                    "id": "AmGraph-1",
                    "title": "Diagnostics - " + selectedMonth + " " + selectedYear,
                    "type": "column",
                    "valueField": "Tests",
                    "labelText": "[[value]]"
                }],
                "guides": [],
                "valueAxes": [{
                    "id": "ValueAxis-1",
                    "stackType": "regular"
                }],
                "allLabels": [],
                "balloon": {},
                "legend": {
                    "enabled": true,
                    "useGraphSettings": true
                },
                "titles": [{
                    "id": "Title-1",
                    "size": 15,
                    "text": "Diagnostics done by user"
                }],
                "dataProvider": testsForMonth
            });
        }
        //== Private functions
    var testConsumableChart = function() {
        var selectedYear = window.localStorage.getItem('SelectedYearDiagnosis');
        var selectedMonth = window.localStorage.getItem('SelectedMonthDiagnosis');
        var chart = AmCharts.makeChart("test_consumable_chart", {
            "type": "serial",
            "categoryField": "Type",
            "maxSelectedTime": -2,
            "startDuration": 1,
            "startEffect": "easeOutSine",
            "processCount": 998,
            "processTimeout": -2,
            "rotate": true,
            "theme": "light",
            "categoryAxis": {
                "gridPosition": "start"
            },
            "trendLines": [],
            "graphs": [{
                "balloonText": "[[category]]: [[value]] consumable",
                "fillAlphas": 1,
                "id": "AmGraph-1",
                "title": "Consumables used - " + selectedMonth + " " + selectedYear,
                "type": "column",
                "valueField": "Consumable",
                "labelText": "[[value]]"
            }],
            "guides": [],
            "valueAxes": [{
                "id": "ValueAxis-1",
                "stackType": "regular"
            }],
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "useGraphSettings": true
            },
            "titles": [{
                "id": "Title-1",
                "size": 15,
                "text": "Consumables used(" + selectedMonth + " " + selectedYear + ")"
            }],
            "dataProvider": [{
                    "Consumable": "20",
                    "Type": "Glucose strips"
                },
                {
                    "Consumable": "13",
                    "Type": "Haemoglobin Strips "
                },
                {
                    "Consumable": "65",
                    "Type": "Cholesterol strips"
                },
                {
                    "Consumable": "83",
                    "Type": "Uric Acid Strips"
                },
                {
                    "Consumable": "42",
                    "Type": "Urine 2 colorvs Urine 10 colorstrips"
                },
                {
                    "Consumable": "53",
                    "Type": "Lancets"
                },
                {
                    "Consumable": "26",
                    "Type": "Alcohol Swabs"
                }
            ]
        });
    }



    return {
        // public functions
        init: function() {
            testIndividualChart();
            testConsumableChart();
            $("#test_individual_chart .amcharts-chart-div a").hide();
            $("#test_consumable_chart .amcharts-chart-div a").hide();
        }
    };
}();

jQuery(document).ready(function() {
    amChartsChartsDemo.init();
});