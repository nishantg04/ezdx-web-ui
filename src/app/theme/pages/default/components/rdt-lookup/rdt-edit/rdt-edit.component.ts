import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, RouterLink } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';

import { Device } from '../device';
import { Center } from '../../facilities/center';
import { Facility } from '../../facilities/facility';
import { FacilitiesService } from '../../facilities/facilities.service';
import { RdtService } from '../rdt.service';
import { Observable } from 'rxjs/Observable';
import { RDTEntity } from '../RDTEntity';
import { RdtLookup } from '../rdtLookup';
import { RDTLookupJson } from '../rdtFile';
import { connectableObservableDescriptor } from 'rxjs/observable/ConnectableObservable';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./rdt-edit.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class RdtEditComponent implements OnInit, AfterViewInit {
    successMsg: boolean = false;
    RDTId: any;
    rdtJson = new RDTLookupJson();
    rdts: RDTEntity[] = [];
    currentUserType: string;
    userInfo: any;
    device = new Device();
    deviceId: string;
    facilities: Facility[];
    centers: Center[];
    loading: boolean;
    msg: string;
    center: any;
    deviceDetail: any;
    filename: string;
    reason: string;
    constructor(private _script: ScriptLoaderService, private _rdtService: RdtService, private _facilitiesService: FacilitiesService, private _router: Router, private _activeroute: ActivatedRoute) {

        if (window.localStorage.getItem('userType') == 'Facility') {
            this.currentUserType = 'Facility';
        }
        else {
            this.currentUserType = 'Admin';
        }
        this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
    }
    ngOnInit() {
        this.RDTId = this._activeroute.snapshot.queryParams['rdtId'] || '/';
        this.getFileById();
        this.getJsonDataById();
        Helpers.setLoading(false);
    }
    ngAfterViewInit() { }

    getJsonDataById() {
        this._rdtService.downloadFile(this.RDTId).then(res => {
            // console.log("rdt json: ", res);
            this.rdtJson.rdtLookups = res;
        })
    }

    getFileById() {
        Helpers.setLoading(true);
        this._rdtService.getRdtLookupById(this.RDTId).then(res => {
            // console.log("RDT json: ", res);
            this.rdtJson.fileName = res.filename;
            this.rdtJson.centerId = res.centerId;
            this.rdtJson.facilityId = res.facilityId;
            this.rdtJson.deviceId = res.deviceId;
        });
    }

    updateRdt() {
        this._rdtService.updateRdtJson(this.RDTId, this.rdtJson).then(res => {
            this.successMsg = true;
            this.goToList();
        })
    }

    goToList() {
        this._router.navigate(['rdt/list'], { queryParams: { type: "RL" } });
    }

}