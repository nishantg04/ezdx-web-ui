import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { FacilitiesService } from '../../facilities/facilities.service';
import { OrderModule } from 'ngx-order-pipe';
import { InputTextModule, InputTextareaModule, DataTableModule, SharedModule, PaginatorModule, GrowlModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { MasterTestEditComponent } from './master-test-edit.component';
import { MasterTestService } from '../master-test.service';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": MasterTestEditComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule, InputTextModule, InputTextareaModule,
        DataTableModule, SharedModule, PaginatorModule, TranslateModule.forChild({ isolate: false }), GrowlModule
    ], exports: [
        RouterModule
    ], declarations: [
        MasterTestEditComponent
    ],
    providers: [
        MasterTestService,
        FacilitiesService,
        TranslateService
    ]

})
export class MasterTestEditModule {
}