import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';

import { Facility } from '../../facilities/facility';
import { } from '@types/googlemaps';
import { Patient } from '../../patients/patient';
import { PatientsService } from '../../patients/patients.service';
import { MapLoaderService } from '../../utils/map-loader.service';
import { log } from 'util';
import { ICenterData } from './map-center.entity';
import { VisitsMapService } from './visits-map.service';
import { setTimeout } from 'timers';
declare var MarkerClusterer: any

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./visits-map.component.html",
    styleUrls: ['./visits-map.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class VisitsMapComponent implements OnInit, AfterViewInit {
    centersData: ICenterData[] = [];
    mapLoaded: boolean = false;
    patients: any[];
    userType: string;
    userInfo: any;
    visits: any[];
    tests: any[];
    facilities: any[];
    selectedFacility: Facility;
    testTypeList: any;
    patientsOptions: any;
    patientsOverlays: any[];
    centersOptions: any;
    centersOverlays: any[] = [];
    testsOptions: any;
    testsOverlays: any[] = [];
    infoWindow: any;
    overlays: any[] = [];
    map: google.maps.Map;
    centermap: google.maps.Map;
    testmap: google.maps.Map;
    status: any;
    showmap: boolean = true;

    constructor(private _script: ScriptLoaderService, private _router: Router, private _activeroute: ActivatedRoute, private _patientService: PatientsService, private _centerService: VisitsMapService) {

        MapLoaderService.load().then(() => {
            this.mapLoaded = true;
            this.infoWindow = new google.maps.InfoWindow();
        });

        this.patientsOptions = {
            center: { lat: 21.7679, lng: 78.8718 },
            zoom: 3
        };
        this.centersOptions = {
            center: { lat: 21.7679, lng: 78.8718 },
            zoom: 3
        };
        this.testsOptions = {
            center: { lat: 21.7679, lng: 78.8718 },
            zoom: 3
        };

        this._centerService.getCenterDetails()
            .then((res) => {
                //console.log("response: ", res);
                this.centersData = res;

                this.centersData.forEach(center => {
                    if (center.geopoint != null) {
                        let tempCenter = JSON.stringify({
                            "customer": center.facilityName,
                            "center": center.centerName,
                            "hubs": center.totalDevices
                        });
                        let tempTest = JSON.stringify({
                            "customer": center.facilityName,
                            "center": center.centerName,
                            "tests": center.totalTests
                        });
                        this.testsOverlays.push(new google.maps.Marker({
                            position: {
                                lat: center.geopoint.lat,
                                lng: center.geopoint.lon
                            },
                            title: tempTest,
                            icon: 'assets/app/media/img/icons/test-32.png'
                        }));
                        this.centersOverlays.push(new google.maps.Marker({
                            position: {
                                lat: center.geopoint.lat,
                                lng: center.geopoint.lon
                            },
                            title: tempCenter,
                            icon: 'assets/app/media/img/icons/center32.png'
                        }));
                    }
                })
            });

        this._patientService.getAllPatients()
            .then((res) => {
                this.patients = res.patients;
                // consolee.log("patient", this.patients[0]);
            }).then(() => {
                this.patientsOverlays = [];
                this.patients.forEach(patient => {
                    if (patient.geopoint)
                        this.patientsOverlays.push(new google.maps.Marker({ position: { lat: patient.geopoint.lat, lng: patient.geopoint.lon }, icon: 'assets/app/media/img/icons/user-32.png', title: patient.mrn }));
                    if (this.patientsOverlays.length != 0) {
                        this.overlays = this.patientsOverlays;
                        this.status = 'patient'
                        this.fitBoundMap(this.overlays);
                    }
                })
            });
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
    }

    /* Patiens map */
    setMapPatients(event) {
        this.map = event.map;
    }

    handleMapClickPatients(event) { };

    handleOverlayClick(event) {

        if (this.status == 'patient') {
            this.handleOverlayClickPatients(event);
        }
        if (this.status == 'center') {

            this.handleOverlayClickCenters(event);
        }
        if (this.status == 'tests') {
            this.handleOverlayClickTests(event);
        }
    }

    handleMapClick(event) {
        console.log(event)
    }

    handleOverlayClickPatients(event) {
        var markerCluster = new MarkerClusterer(event.map, this.patientsOverlays,
            { imagePath: 'assets/app/media/img/clustor/' });

        let isMarker = event.overlay.getTitle != undefined;
        if (isMarker) {
            let title = event.overlay.getTitle();
            var content = `<h5>${title}</h5>`;
            this.infoWindow.setContent(content);
            this.infoWindow.open(event.map, event.overlay);
        }
    }

    /* Centers Map */
    /*     loadCenters(): void {
           this.centersData.forEach(center => {
               if(center.geopoint)
               this.centersOverlays.push(new google.maps.Marker({ position: { lat: center.geopoint.lat, lng: center.geopoint.lon }, title: center.centerName }));
           })
        } */

    setMapCenters(event) {
        this.centermap = event.map;
    }

    handleMapClickCenters(eventc) { };

    handleOverlayClickCenters(eventc) {

        let isMarker = eventc.overlay.getTitle != undefined;

        if (isMarker) {
            let title = JSON.parse(eventc.overlay.getTitle());
            var content = `<h6>Customer: ${title.customer}</h6> <h6>Center: ${title.center}</h6> <span>Hubs: ${title.hubs}</span>`;
            this.infoWindow.setContent(content);
            this.infoWindow.open(eventc.map, eventc.overlay);
        }
    }

    /* Tests map */
    loadTests(): void {

    }


    tabClick(type, e) {
        let bounds = new google.maps.LatLngBounds();
        this.overlays = [];
        this.status = type;
        this.showmap = false;
        if (this.status == 'patient') {
            setTimeout(() => {
                this.showmap = true;
            }, 500);
            this.overlays = this.patientsOverlays
            this.fitBoundMap(this.overlays)
        }
        if (this.status == 'center') {
            setTimeout(() => {
                this.showmap = true;
            }, 500);
            this.overlays = this.centersOverlays;
            this.fitBoundMap(this.overlays)

        }
        if (this.status == 'tests') {
            setTimeout(() => {
                this.showmap = true;
            }, 500);
            this.overlays = this.testsOverlays;
            this.fitBoundMap(this.overlays)
        }
    }

    fitBoundMap(overlay) {
        let bounds = new google.maps.LatLngBounds();
        if (overlay.length != 0) {
            overlay.forEach(marker => {
                bounds.extend(marker.getPosition());
            });
            setTimeout(() => { // map will need some time to load               
                this.map.fitBounds(bounds); // Map object used directly
            }, 600);
        }
    }

    setMapTests(event) {
        this.testmap = event.map;
    }

    handleMapClickTests(tevent) { };

    handleOverlayClickTests(tevent) {
        // consolee.log("Event of click: ", event)
        let isMarker = tevent.overlay.getTitle != undefined;

        // consolee.log("marker: ", isMarker);

        if (isMarker) {
            let title = JSON.parse(tevent.overlay.getTitle());
            var content = `<h6>Customer: ${title.customer}</h6> <h6>Center: ${title.center}</h6> <span>Test count: ${title.tests}</span>`;
            this.infoWindow.setContent(content);
            this.infoWindow.open(tevent.map, tevent.overlay);
            //event.map.setCenter(event.overlay.getPosition());
        }
    }



}

