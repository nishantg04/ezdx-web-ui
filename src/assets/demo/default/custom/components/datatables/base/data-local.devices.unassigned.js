//== Class definition

var DatatableDataLocalDemo = function() {
    //== Private functions

    // demo initializer
    var demo = function() {

        var dataJSONArray = JSON.parse(localStorage.getItem('unAssingedDeviceTable'));
        var facilityJSONArray = JSON.parse(localStorage.getItem('facilityTable'));

        for (i = 0; i < dataJSONArray.length; i++) {
            if (dataJSONArray[i].centerId !== undefined) {
                for (k = 0; k < facilityJSONArray.length; k++) {
                    if (facilityJSONArray[k].centers !== undefined) {
                        for (var j = 0; j < facilityJSONArray[k].centers.length; j++) {
                            if (facilityJSONArray[k].centers[j].id === dataJSONArray[i].centerId) {
                                dataJSONArray[i].centerName = facilityJSONArray[k].centers[j].name;
                            }
                        }
                    }
                }
            }


        }

        //Refresh table by remove html tag
        $(".m_datatable").remove();
        var $newdiv1 = $("<div class='m_datatable'></div>");
        $(".m-portlet__body").append($newdiv1);

        var datatable = $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: dataJSONArray,
                pageSize: 10,
                saveState: {
                    cookie: false,
                    webstorage: false
                }, // save datatable state(pagination, filtering, sorting, etc) in cookie or browser webstorage
            },

            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 450, // datatable's body's fixed height
                footer: false // display/hide footer
            },

            // column sorting(refer to Kendo UI)
            sortable: true,

            // column based filtering(refer to Kendo UI)
            filterable: false,

            pagination: true,

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition

            columns: [{
                    field: "",
                    title: "Sr no",
                    sortable: false,
                    template: function(row) {
                        return '\
						<span>\
                            ' + (((row.getDatatable().getCurrentPage() - 1) * 10) + (row.getIndex() + 1)) + '\
                        </span>\
					';
                    }
                },
                {
                    field: "organizationName",
                    title: "Customer",
                    sortable: true
                },

                {
                    field: "centerName",
                    title: "Center",
                    sortable: true
                },

                {
                    field: "deviceType",
                    title: "Hub Type",
                    sortable: true
                },

                {
                    field: "serialNumber",
                    title: "Serial Number",
                    sortable: true
                }, {
                    field: "assignmentStatus",
                    title: "Status",
                    sortable: true
                }, {
                    field: "Actions",
                    width: 110,
                    title: "Actions",
                    sortable: false,
                    overflow: 'visible',
                    template: function(row) {
                        // console.log(row);
                        return '\
						<a id="' + row.id + '" onclick="my.namespace.deviceUpdatePublicFunc(\'' + row.id + '\')" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill edit-facility" title="View ">\
                            <i class="la la-pencil"></i>\
                        </a>\
					';
                    }
                }
            ]
        });

        var query = datatable.getDataSourceQuery();
        datatable.sort('organizationName', 'asc');

        $('#m_form_search').on('keyup', function(e) {
            datatable.search($(this).val().toLowerCase());
        }).val(query.generalSearch);

        // $('#m_form_status').on('change', function () {
        // 	datatable.search($(this).val(), 'Status');
        // }).val(typeof query.Status !== 'undefined' ? query.Status : '');

        // $('#m_form_type').on('change', function () {
        // 	datatable.search($(this).val(), 'Type');
        // }).val(typeof query.Type !== 'undefined' ? query.Type : '');
        // $('#m_form_status, #m_form_type').selectpicker();

    };

    return {
        //== Public functions
        init: function() {
            // init dmeo
            demo();
        }
    };
}();

jQuery(document).ready(function() {
    DatatableDataLocalDemo.init();
});