import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { RdtListComponent } from './rdt-list.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { DefaultComponent } from '../../../default.component';
import { RdtService } from '../rdt.service';
import { FacilitiesService } from '../../facilities/facilities.service';
import { OrderModule } from 'ngx-order-pipe';
import { DataTableModule, SharedModule, PaginatorModule, TabViewModule, DialogModule } from 'primeng/primeng';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { UrineService } from '../../urine-lookup/urine.service';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": RdtListComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, OrderModule, DataTableModule, SharedModule, PaginatorModule, DialogModule,
        TabViewModule, DialogModule,
        TranslateModule.forChild({ isolate: false }),
    ], exports: [
        RouterModule
    ], declarations: [
        RdtListComponent
    ],
    providers: [
        RdtService,
        UrineService,
        FacilitiesService,
        TranslateService
    ]

})
export class RdtListModule {
}