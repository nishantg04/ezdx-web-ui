import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Helpers } from '../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { ConsumablesService } from '../consumables.service';
import { Facility } from "../../facilities/facility";
import { Center } from "../../facilities/center";
import { FacilitiesService } from "../../facilities/facilities.service";
import { VisitsService } from '../../visits/visits.service';


@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./consumables-reorder-add.component.html",
    styleUrls: ['../../../primeng.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ConsumablesReorderAddComponent implements OnInit, AfterViewInit {
    msgComsumableAdd: { severity: string; summary: string; detail: string; }[] = [];
    dateExpired: boolean = false;
    facilities: any[];
    selectedFacility: any;
    centers: any[];
    selectedCenter: any;
    consumableToAdd: any;
    testList: any[];
    tests: any[];
    consumableList: any[];
    consEditMode: boolean;
    errMsg: any;
    loading: boolean;
    userInfo: any;
    userType: string;
    comment: any;
    constructor(private _facilitiesService: FacilitiesService, private _visitsService: VisitsService, private _ngZone: NgZone, private _script: ScriptLoaderService, private _consumablesServiceService: ConsumablesService, private _router: Router, private _activeroute: ActivatedRoute) {

        this.consumableToAdd = {
            id: null,
            quantity: 1,
            manufacturer: null,
            description: '',
            testType: null,
            testName: null,
            itemsPerPackage: 1,
            items: 0,
            expiryDate: null,
            lotNumber: null,
            consumableType: null,
            name: null
        }
        this.consumableList = [];
        this.comment = "";
        this.consEditMode = false;
        this.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
        this.userType = window.localStorage.getItem('userType');
    }

    humanize(str) {
        var frags = str.split('_');
        for (var i = 0; i < frags.length; i++) {
            frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
        }
        return frags.join(' ');
    }

    ngOnInit() {
        window.my = window.my || {};
        window.my.namespace = window.my.namespace || {};
    }
    ngOnDestroy() {
        window.my.namespace.publicFunc = null;
    }
    ngAfterViewInit() {
        this.getFacilities();
        // this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
        //     'assets/demo/default/custom/components/forms/validation/form-controls.js');
    }

    onTestChange() {
        //Getting selected test type to display
        this.testList.forEach(test => {
            if (test.testName === this.consumableToAdd.testName) {
                this.consumableToAdd.consumableType = test.consumableType;
                this.consumableToAdd.testType = test.testType;
                this.consumableToAdd.name = this.humanize(test.testName);
            }
        });
    }

    onQuantityChange() {
        this.consumableToAdd.items = this.consumableToAdd.quantity * this.consumableToAdd.itemsPerPackage;
    }
    addCosumable(): void {
        if (this.consumableToAdd.quantity <= 0) {
            this.msgComsumableAdd = [{ severity: 'error', summary: 'Invalid total quantity', detail: 'Add valid totoal quantity!' }];
            setTimeout(() => {
                this.msgComsumableAdd = [];
            }, 3 * 1000);
            return;
        }
        // console.log("this.consumableToAdd.testName: ", this.consumableToAdd.testName);
        if (this.consumableToAdd.testName === undefined || this.consumableToAdd.testName === null || this.consumableToAdd.testName === '') {
            this.msgComsumableAdd = [{ severity: 'error', summary: 'Invalid Test', detail: 'Select valid Test!' }];
            setTimeout(() => {
                this.msgComsumableAdd = [];
            }, 3 * 1000);
            return;
        }
        if (!this.consEditMode) {
            var consumableToAdd = jQuery.extend(true, {}, this.consumableToAdd);
            this.consumableList.push(consumableToAdd);
        }
        this.consEditMode = false;
        this.consumableList = this.consumableList.slice();
        this.consumableToAdd = {
            id: null,
            quantity: 0,
            manufacturer: null,
            description: '',
            testType: null,
            testName: null,
            itemsPerPackage: 1,
            items: 0,
            expiryDate: null,
            lotNumber: null,
            consumableType: null,
            name: null
        }
        jQuery('#close_consumable_modal').click();
    }

    removeCosumable(selectedConsumable): void {
        const index: number = this.consumableList.indexOf(selectedConsumable);
        if (index !== -1) {
            this.consumableList.splice(index, 1);
        }
        this.consumableList = this.consumableList.slice();
    }

    editCosumable(selectedConsumable): void {
        this.consumableToAdd = selectedConsumable;
        this.consEditMode = true;
        jQuery('#open_consumable_modal').click();
    }

    closeConsumableModal() {
        this.consumableToAdd = {
            id: null,
            quantity: 1,
            manufacturer: null,
            description: '',
            testType: null,
            testName: null,
            itemsPerPackage: 1,
            items: 0,
            expiryDate: null,
            lotNumber: null,
            consumableType: null,
            name: null
        }
    }

    getTests() {
        this._visitsService.getTests()
            .then((data) => {
                this.testList = data;
                // console.log("Tests List: ", this.testList);
            })
            .then(() => {
                this.tests = [];
                var tempTest = [];
                // console.log('got', this.testList)
                this.testList.forEach(test => {
                    test.name = this.humanize(test.testName);
                    if (test.testType != "PHYSICAL") {
                        tempTest.push(test);
                    }

                });
                tempTest.sort(function(a, b) { return (a.testName > b.testName) ? 1 : ((b.testName > a.testName) ? -1 : 0); });
                // console.log('all test', tempTest)
                tempTest.forEach(test => {
                    //  if(test.testName != 'BILIRUBIN' && test.testName != 'BLOOD'  &&
                    //   test.testName !='KETONE' && test.testName != 'LEUKOCYTES' && test.testName != 'NITRITE' && test.testName != 'PH' &&
                    // test.testName != 'SPECIFIC_GRAVITY'   && test.testName != 'URINE'&&
                    // test.testName != 'UROBILINOGEN'){
                    //      this.tests.push(test)
                    //  }       
                    if (test.active)
                        this.tests.push(test);
                })
                this.tests.sort(function(a, b) { return (a.testName > b.testName) ? 1 : ((b.testName > a.testName) ? -1 : 0); });
                /*  this.consumableToAdd.testType = this.tests[1].testType;
                 this.consumableToAdd.testName = this.tests[1].testName;
                 this.consumableToAdd.consumableType = this.tests[1].consumableType; */
                Helpers.setLoading(false);

                //this._router.navigate(['/devices/list']);
            }, (e: any) => {
                // console.log(e);
                Helpers.setLoading(false);
            });
    }
    getFacilities(): void {

        this.facilities = JSON.parse(localStorage.getItem('facilityTable'));
        this.facilities.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            } else if (a.name < b.name) {
                return -1;
            }
            return 0;
        });
        Helpers.setLoading(false);
        this.facilities.sort(function(a, b) {
            return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
        });
        if (window.localStorage.getItem('userType') == 'Facility') {
            this.facilities.forEach(fclt => {
                if (fclt.id == this.userInfo.facilityId) {
                    this.selectedFacility = jQuery.extend(true, {}, fclt);
                }
            });
        }
        else {
            this.selectedFacility = jQuery.extend(true, {}, this.facilities[0]);
        }
        this.centers = this.selectedFacility.centers;
        if (this.centers) {
            this.centers.sort(function(a, b) {
                return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
            });
            this.selectedCenter = jQuery.extend(true, {}, this.centers[0]);
        }
        this.getTests();
    }

    onOrgChange(orgId: string): void {
        this.facilities.forEach((org, i) => {
            if (org.id === orgId) {
                this.selectedFacility.organizationCode = org.organizationCode;
                this.centers = org.centers;
                if (this.centers) {
                    this.centers.sort(function(a, b) {
                        return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
                    });
                    this.selectedCenter = jQuery.extend(true, {}, this.centers[0]);
                }
                this.selectedFacility.organizationCode = this.facilities[0].organizationCode;
            }
        });
    }

    onCenterChange(centerId: string): void {
    }

    createRequest(): void {
        if (this.consumableList.length > 0) {
            Helpers.setLoading(true);
            var reorderReq = {
                facilityId: this.selectedFacility.id,
                centerId: this.selectedCenter.id,
                orderItems: [],
                orderStatus: "ORDER_PLACED",
                comment: this.comment
            }
            this.consumableList.forEach(test => {
                reorderReq.orderItems.push({ testType: test.testType, testName: test.testName, quantity: test.quantity, consumableType: test.consumableType, orderItemStatus: "ORDER_PLACED", fulfilledQuantity: 0 })
            });

            this._consumablesServiceService.createReorder(reorderReq)
                .then((data) => console.log(data))
                .then(() => {
                    Helpers.setLoading(false);
                    this.loading = false;
                    this._router.navigate(['/consumables/reorder']);
                }, (e: any) => {
                    // console.log(e);
                    let res = JSON.parse(e._body);
                    if (res.error) {
                        this.errMsg = res.error;
                    }
                    Helpers.setLoading(false);
                    this.loading = false
                });
        }
    }

}