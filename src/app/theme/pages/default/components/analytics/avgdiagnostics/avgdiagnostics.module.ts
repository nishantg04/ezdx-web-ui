import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { AmchartModule } from '../../../../../amchartcomponent/amchart.module';
import { DefaultComponent } from '../../../default.component';
import { AvgdiagnosticsComponent } from './avgdiagnostics.component';
import { InputTextModule, InputTextareaModule, DataTableModule, SharedModule, PaginatorModule, GrowlModule } from 'primeng/primeng';
import { AnalyticsService } from '../analytics.service';

import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": AvgdiagnosticsComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule.forChild(routes), LayoutModule, InputTextModule, InputTextareaModule, DataTableModule, SharedModule, PaginatorModule, GrowlModule,
        TranslateModule.forChild({ isolate: false }),
        AmchartModule
    ], exports: [
        RouterModule
    ], declarations: [
        AvgdiagnosticsComponent
    ],
    providers: [
        TranslateService,
        AnalyticsService
    ]

})
export class AvgdiagnosticsModule { }
