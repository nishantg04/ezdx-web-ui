import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UploadFileService } from '../upload-file.service'
import { FacilitiesService } from '../../facilities/facilities.service';
import { CommonService } from '../../../../../../_services/common.service';
import { DevicesService } from '../../devices/devices.service';
import { Facility } from '../../facilities/facility';
import { Center } from '../../facilities/center';
import { UsersService } from '../../users/users.service';
import { Files } from './file-details';
import { User } from '../../users/user';
import { Helpers } from '../../../../../../helpers';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./file-details.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['../../../primeng.component.scss'],

})
export class FileDetailsComponent implements OnInit {
    fileId: any;
    facility = new Facility();
    selectedFacility = new Facility();
    center = new Center();
    selectedCenter = new Center();
    file = new Files();
    user = new User();
    centers: any[];
    viewCustomer: boolean = false;
    countires: any[];
    country_code: any;
    facilityArray: any[];
    facilityJson: any;
    deviceArray: any[];
    deviceJson: any;
    centerDevices: any[];
    deviceList: any[];
    fullDetail: any;
    facilities: any[];
    facCenters: any[];
    deviceEntity: any[];

    constructor(private _router: Router, private _activeroute: ActivatedRoute, private _uploadFileService: UploadFileService, private _devicesService: DevicesService,
        private _facilitiesService: FacilitiesService, private _usersService: UsersService, private CommonService: CommonService) {
        this.facilityJson = { 'facilityId': '', 'facilityName': '', 'facilityCode': '', 'centerId': '', 'centerName': '', 'centerCode': '' }
        this.deviceJson = { 'deviceType': '', 'serialNumber': '', 'synchronizedDate': '', 'updatedStatus': '' }
        this.getCountries();
    }

    ngOnInit() {
        this.fileId = this._activeroute.snapshot.queryParams['fileId'] || '/';
        if (this.fileId)
            this.getFullDetail();
        //this.getFile();
    }


    getCountries(): void {
        this.CommonService.getCountries().then(
            countries => {
                this.countires = countries
            }
        );
    }

    getFlag() {
        this.countires.forEach(item => {
            if (item.name == this.user.user.addressInfo.country)
                this.country_code = item.code;
        })
    }

    getFullDetail() {
        Helpers.setLoading(true);
        this.facilities = [];
        this._uploadFileService.getFileFullDetail(this.fileId).then(data => {
            Helpers.setLoading(false);
            this.fullDetail = data;
            this.file = this.fullDetail.file;
            this.facilities = this.fullDetail.organizations;
            this.deviceEntity = this.fullDetail.devices;
            this.getFacilityList();
            if (this.file.userId) {
                this.getUserDetails();
            }
            if (this.file.uploadTime) {
                var d = new Date(this.file.uploadTime),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;
                this.file.uploadTime = [day, month, year].join('-') + ' (' + d.getHours() + ':' + d.getMinutes() + ')';
            }
        })
    }


    getFacilityList() {
        this.facilityArray = [];
        this.file.centerId.forEach(center => {
            this.facilities.forEach(data => {
                this.facCenters = data.centers;
                this.facCenters.forEach(centerData => {
                    if (centerData.id == center) {
                        this.facilityArray.push(
                            {
                                'facilityId': data.id,
                                'facilityName': data.name,
                                'facilityCode': data.organizationCode,
                                'centerId': centerData.id,
                                'centerName': centerData.name,
                                'centerCode': centerData.centerCode
                            });
                    }
                })
            })
        })
        // console.log('pushed faci', this.facilityArray)
    }

    getFullCenter() {
        this.facCenters = [];
        this.facilities.forEach(data => {
            this.facCenters = this.facCenters.concat(data.centers)
        })
    }

    getDialCode() {
        this.countires.forEach(item => {
            if (item.name == this.user.user.addressInfo.country)
                this.user.user.dial_code = item.dial_code;
        })
    }


    getFile() {
        this._uploadFileService.getFileDetails(this.fileId).then(data => {
            this.file = data;
            // console.log('file', this.file)
            if (this.file.facilityId) {
                this.getFaclityDetail(this.file.facilityId)
                if (this.file.userId) {
                    this.getUserDetails();
                }
            }
        }).then(() => {
            if (this.file.fileType == "PYTHON_SCRIPT")
                this.file.fileType = 'Python Script'
            if (this.file.fileType == undefined || this.file.fileType == null)
                this.file.fileType = 'FIRMWARE'
            if (this.file.uploadTime) {
                var d = new Date(this.file.uploadTime),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;
                this.file.uploadTime = [day, month, year].join('-') + ' (' + d.getHours() + ':' + d.getMinutes() + ')';
            }
        })
    }

    getFaclityDetail(facilityId) {
        this.facilityArray = [];
        facilityId.forEach(id => {
            this._facilitiesService.getFacilityById(facilityId)
                .then(facility => this.facility = facility)
                .then(() => {
                    // this.facilityJson.facilityId = this.facility.id;
                    // this.facilityJson.facilityName = this.facility.name; 
                    // this.facilityJson.facilityCode = this.facility.organizationCode;     
                    this.centers = this.facility.centers;
                    if (this.centers) {
                        this.getCenterDetails(this.centers)
                    }
                })
        });
    }

    getUserDetails() {
        Helpers.setLoading(true);
        this._usersService.getUser(this.file.userId)
            .then(user => this.user.user = user)
            .then(() => {
                Helpers.setLoading(false);
                this.getFlag();
            })
    }

    getCenterDetails(centers) {
        centers.forEach(center => {
            this.file.centerId.forEach(id => {
                if (id == center.id) {
                    this.center = center
                    //  this.facilityJson.centerId = this.center.id;
                    //  this.facilityJson.centerName = this.center.name;
                    //  this.facilityJson.centerCode = this.center.centerCode;              
                    this.facilityArray.push(
                        {
                            'facilityId': this.facility.id,
                            'facilityName': this.facility.name,
                            'facilityCode': this.facility.organizationCode,
                            'centerId': this.center.id,
                            'centerName': this.center.name,
                            'centerCode': this.center.centerCode
                        });
                }
            })
        });
    }

    showDetail(facilityId, centerId) {
        if (facilityId && centerId) {
            //  document.getElementById('facDetails'+ centerId).style.background = 'white'
            this.viewCustomer = true;
            // this.file.centerId.forEach(id=>{
            //   if(id == centerId){
            //     document.getElementById('facDetails'+ centerId).style.background = '#dfdff9'
            //   }           
            // })                
            this.getParticularFaciltyCenter(facilityId, centerId);
            this.getDeviceDetails(centerId);
        }
    }

    getParticularFaciltyCenter(facilityId, centerId) {
        this.facilities.forEach(facility => {
            if (facility.id == facilityId) {
                this.selectedFacility = facility;
            }
        })
        this.selectedFacility.centers.forEach(center => {
            if (center.id == centerId) {
                this.selectedCenter = center
            }
        })
        // console.log('data', this.selectedFacility, this.selectedCenter)
    }

    getFacility(facilityId) {
        this._facilitiesService.getFacilityById(facilityId)
            .then(facility => this.selectedFacility = facility)
            .then(() => {
                // console.log('faci', this.selectedFacility)
            })
    }

    getCenter(centerId) {
        this._facilitiesService.getCenterDetail(centerId)
            .then(center => this.selectedCenter = center)
            .then(() => {
                if (this.selectedCenter.id) {
                    //this.getDeviceDetails();
                }
            })
    }

    getDeviceDetails(centerId) {
        this.centerDevices = [];
        this._devicesService.getDevicesDate(0, new Date().getTime(), 'CENTER', centerId)
            .then(device => {
                this.centerDevices = device.devices;
                // console.log('center data', this.centerDevices)
                if (this.centerDevices) {
                    this.getUploadDevice()
                }
            })
    }

    getUploadDevice() {
        this.deviceArray = [];
        this.centerDevices.forEach(device => {
            for (var i = 0, l = this.file.deviceId.length; i < l; i++) {
                if (device.id == this.file.deviceId[i]) {
                    var slider = this.file.deviceId[i];
                    if (this.file.fileType = "Python Script") {
                        var syn = device.pythonDownloadTime;
                    }
                    if (this.file.fileType = "FIRMWARE") {
                        syn = device.firmwareDownloadTime;
                    }
                    if (syn == null || syn == undefined) {
                        var status = 'unassigned'
                    } else
                        status = 'assigned'
                    this.deviceArray.push({ 'deviceType': device.deviceType, 'serialNumber': device.serialNumber, 'synchronizedDate': syn, 'updatedStatus': status });
                    this.deviceList = this.deviceArray;
                }

            }
        })
    }

    getUploadedDeviceList() {
        this.deviceArray = [];
        if (this.file.deviceId.length != 0) {
            this._devicesService.getDevicesDate(0, new Date().getTime(), 'CENTER', this.selectedCenter.id)
                .then(device => {
                    this.centerDevices = device.devices
                    this.centerDevices.forEach(device => {
                        for (var i = 0, l = this.file.deviceId.length; i < l; i++) {
                            if (device.id == this.file.deviceId[i]) {
                                var slider = this.file.deviceId[i];
                                if (this.file.fileType = "Python Script") {
                                    var syn = device.pythonDownloadTime;
                                }
                                if (this.file.fileType = "FIRMWARE") {
                                    syn = device.firmwareDownloadTime;
                                }
                                if (syn == null || syn == undefined) {
                                    var status = 'unassigned'
                                } else
                                    status = 'assigned'
                                this.deviceArray.push({ 'deviceType': device.deviceType, 'serialNumber': device.serialNumber, 'synchronizedDate': syn, 'updatedStatus': status });
                                this.deviceList = this.deviceArray;
                            }

                        }
                    })
                })

        }

    }

}                                   
