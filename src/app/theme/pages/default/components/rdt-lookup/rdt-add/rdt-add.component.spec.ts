import {
    TestBed,
    async,
    inject,
    fakeAsync,
    ComponentFixture,
    tick
} from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { RdtAddComponent } from "./rdt-add.component";
import {
    TranslateModule,
    TranslateService,
    TranslateLoader,
    TranslateFakeLoader
} from "@ngx-translate/core";
import { Http, BaseRequestOptions, ConnectionBackend } from "@angular/http";
import { LayoutModule } from "../../../../../layouts/layout.module";
import { OrderModule } from "ngx-order-pipe";
import {
    SharedModule,
    DataTableModule,
    PaginatorModule,
    DialogModule
} from "primeng/primeng";
import { FacilitiesService } from "../../facilities/facilities.service";
import { RdtService } from "../rdt.service";
import { MockBackend } from "@angular/http/testing";
import { RDTEntity } from "../RDTEntity";
import { RDTLookupJson } from "../rdtFile";
import { RdtLookup } from "../rdtLookup";
import { setTimeout } from "timers";
import { fail } from "assert";

class MockRdtService {
    constructor() { }
    getDefaultLookup(): Promise<any> {
        let rdtJson: RdtLookup = new RdtLookup();
        rdtJson.centerId = ["abc"];
        rdtJson.deviceId = ["abd"];
        rdtJson.facilityId = ["abf"];
        rdtJson.id = "asdfjklp";
        return Promise.resolve({ files: [rdtJson] });
    }

    downloadFile(id: string): Promise<any> {
        let rdt = new RDTEntity();
        rdt.testName = "MALARIA";
        return Promise.resolve([rdt]);
    }
}

describe("RDTComponent", () => {
    let component: RdtAddComponent;
    let fixture: ComponentFixture<RdtAddComponent>;
    let rdtService: MockRdtService;
    let originalTimeout: any;
    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                imports: [
                    RouterTestingModule,
                    LayoutModule,
                    OrderModule,
                    DataTableModule,
                    SharedModule,
                    PaginatorModule,
                    DialogModule,
                    TranslateModule.forRoot({
                        loader: { provide: TranslateLoader, useClass: TranslateFakeLoader }
                    })
                ],
                declarations: [RdtAddComponent],
                providers: [
                    { provide: RdtService, useClass: MockRdtService },
                    TranslateService
                ]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        fixture = TestBed.createComponent(RdtAddComponent);
        component = fixture.componentInstance;
        rdtService = fixture.debugElement.injector.get(RdtService);
        // console.log("rdt service: ", rdtService);
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    })

    it("Should create a component", () => {
        // console.log(component);
        expect(component).toBeTruthy();
    });

    xit("Should have tests empty(undefined -ve test)", () => {
        expect(component.rdts).toBeUndefined();
    });

    it("Should have rdt test", async(() => {
        fixture.detectChanges();
        setTimeout(() => {
            // console.log("rdts res: ", component.rdts);
            expect(component.rdts).toBeDefined();
            expect(component.rdts.length).toBeGreaterThanOrEqual(1);
        }, 3000);
    }));

    it("Should have expected rdt test MALARIA", async(() => {
        fixture.detectChanges();
        setTimeout(() => {
            expect(component.rdts[0].testName).toBe("MALARIA");
        }, 3000);
    }));

    it('should be true for has underscore', () => {
        fixture.detectChanges();
        fixture.componentInstance.rdtLookupJson.fileName = "abc_def"
        fixture.componentInstance.onKeyUpEvent();
        fixture.detectChanges();
        expect(component.hasUnderscore).toBeTruthy();
    })

    it('should be false for has underscore', () => {
        fixture.detectChanges();
        fixture.componentInstance.rdtLookupJson.fileName = "abcdef";
        fixture.componentInstance.onKeyUpEvent();
        fixture.detectChanges();
        expect(component.hasUnderscore).toBeFalsy();
    })

});
