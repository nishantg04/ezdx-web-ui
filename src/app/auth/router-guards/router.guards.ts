import { Injectable } from "@angular/core";
import {
    ActivatedRouteSnapshot,
    CanActivate,
    RouterStateSnapshot,
    Router
} from "@angular/router";
import { UserSessionService } from "./user-session.service";
import { Observable } from "rxjs/Observable";

import * as _ from "lodash";

@Injectable()
export class RouteGuard implements CanActivate {
    constructor(private authUser: UserSessionService, private router: Router) { }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | boolean {


        //// console.log("Route Guards:: route: ", next, " state: ", state);
        let routePermiteRoles: String[] = _.get(next, "data.roles");
        let userRoles: String[] = this.authUser.getUserRoles();
        let shallNavigate = false;

        if (userRoles.length <= 0 || userRoles === undefined) {
            // if the user does not have the roles navigate to the 401(Permision denied) page.
            shallNavigate = false;
        } else if (routePermiteRoles === undefined) {
            // If the router is not assigned to the path, Assuming by default to all the users grant.
            shallNavigate = true;
        } else {
            _.forEach(userRoles, role => {
                _.forEach(routePermiteRoles, routePermited => {
                    if (role == routePermited || routePermited == "ALL")
                        shallNavigate = true;
                });
            });
        }
        if (!shallNavigate) {
            this.router.navigate(["/401"]);
        } else {
            return shallNavigate;
        }
    }
}
