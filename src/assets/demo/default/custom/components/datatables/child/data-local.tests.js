//== Class definition
var DatatableChildDataLocalDemo = function () {//== Private functions
	var subTableTestInit = function (e) {
		var datatableTest = $('<div/>').attr('id', 'child_data_local_' + e.data.id).appendTo(e.detailCell)
			.mDatatable({
				data: {
					type: 'local',
					source: e.data.tests,
					pageSize: 10
				},

				// layout definition
				layout: {
					theme: 'default',
					scroll: false,
					height: null,
					footer: false,

					// enable/disable datatable spinner.
					spinner: {
						type: 1,
						theme: 'default'
					}
				},

				sortable: true,

				// columns definition
				columns: [{
					field: "name",
					title: "Tests"
				}, {
					field: "count",
					title: "Diagnostics done"
				}]
			});
		// datatableTest.sort('count');
		// setTimeout(function () {
		// 	datatableTest.sort('count');
		// }, 500);
	}

	//== Private functions
	var subTableTestTypeInit = function (e) {
		var datatableTestType = $('<div/>').attr('id', 'child_data_local_' + e.data.id).appendTo(e.detailCell)
			.mDatatable({
				data: {
					type: 'local',
					source: e.data.tests,
					pageSize: 10
				},

				// layout definition
				layout: {
					theme: 'default',
					scroll: false,
					height: null,
					footer: false,

					// enable/disable datatable spinner.
					spinner: {
						type: 1,
						theme: 'default'
					}
				},

				detail: {
					title: 'Tests',
					content: subTableTestInit
				},

				sortable: true,

				// columns definition
				columns: [{
					field: "id",
					title: "",
					sortable: false,
					width: 20,
					textAlign: 'center' // left|right|center,
				}, {
					field: "name",
					title: "Test types"
				}, {
					field: "count",
					title: "Diagnostics done"
				}]
			});
		// datatableTestType.sort('count');
		// setTimeout(function () {
		// 	datatableTestType.sort('count');
		// }, 500);
	}

	var subTableInit = function (e) {
		var datatableCenter = $('<div/>').attr('id', 'child_data_local_' + e.data.id).appendTo(e.detailCell)
			.mDatatable({
				data: {
					type: 'local',
					source: e.data.centers,
					pageSize: 10,
					saveState: {
						cookie: false,
						webstorage: false
					}
				},

				// layout definition
				layout: {
					theme: 'default',
					scroll: false,
					height: null,
					footer: false
				},

				detail: {
					title: 'Test types',
					content: subTableTestTypeInit
				},

				sortable: true,

				// columns definition
				columns: [{
					field: "id",
					title: "",
					sortable: false,
					width: 20,
					textAlign: 'center' // left|right|center,
				}, {
					field: "name",
					title: "Centers",
					template: function(row) {
                        return '\
                            <a id="'+ row.id +'" class="csr-hand" onclick="my.namespace.navCenterDetails(\'' + row.id+'\',\''+row.facilityId+'\')">\
                                ' + row.name +'\
                            </a>\
                        ';
                    }
				}, {
					field: "count",
					title: "Diagnostics done"
				}]
			});

		// datatableCenter.sort('count');
		// setTimeout(function () {
		// 	datatableCenter.sort('count');
		// }, 500);
	}

	// demo initializer
	var mainTableInit = function () {
		//Refresh table by remove html tag
		$(".m_datatable").remove();
		var $newdiv1 = $("<div class='m_datatable'></div>");
		$(".m-portlet__body").append($newdiv1);

		var dataJSONArray = JSON.parse(localStorage.getItem('diagnosisStatsTable'));

		var datatable = $('.m_datatable').mDatatable({
			// datasource definition
			data: {
				type: 'local',
				source: dataJSONArray,
				pageSize: 10,
				saveState: {
					cookie: false,
					webstorage: false
				}
			},

			// layout definition
			layout: {
				theme: 'default',
				scroll: false,
				height: null,
				footer: false
			},

			sortable: true,

			filterable: false,

			pagination: true,

			detail: {
				title: 'Centers',
				content: subTableInit
			},

			// columns definition
			columns: [{
				field: "id",
				title: "",
				sortable: false,
				width: 20,
				textAlign: 'center' // left|right|center,
			}, {
				field: "name",
				title: "Customers",
				template: function(row) {
                    return '\
						<a id="'+ row.id +'" class="csr-hand" onclick="my.namespace.navFacilityDetails(\'' + row.facilityId+'\')">\
                            ' + row.name +'\
                        </a>\
					';
                }
			}, {
				field: "count",
				title: "Diagnostics done"
			}]
		});

		var query = datatable.getDataSourceQuery();
		datatable.sort('count');
		setTimeout(function () {
			datatable.sort('count');
		}, 500);

		$('#m_form_search').on('keyup', function (e) {
			datatable.search($(this).val().toLowerCase());
		}).val(query.generalSearch);

		$('#m_form_status, #m_form_type').selectpicker();

	};

	return {
		//== Public functions
		init: function () {
			// init dmeo
			mainTableInit();
		}
	};
}();

jQuery(document).ready(function () {
	DatatableChildDataLocalDemo.init();
});